'use strict';

var gulp = require('gulp'),
    gulpLoadPlugins = require('gulp-load-plugins'),
    through = require('through'),
    gutil = require('gulp-util'),
    ts = require('gulp-typescript'),
    plugins = gulpLoadPlugins(),
    paths = {
        js: ['./*.js', 'config/**/*.js', 'gulp/**/*.js', 'packages/*/*/*.js', //'tools/**/*.js', 
            'packages/*/*/server/*/*.js', 'packages/*/*/public/!(assets)/*.js', 
            'packages/*/*/public/assets/js/**/*.js'],
        ts: ['packages/*/*/*.ts', 'packages/*/*/server/**/*.ts'],
        html: ['packages/*/*/server/**/*.html', 'packages/*/*/public/!(assets)/*.html'],
        css: ['packages/*/*/server/**/*.css', 'packages/*/*/public/!(assets)/*.css', 
            'packages/*/*/public/assets/css/**/*.css', '!packages/**/assets/css/styles.css'],
        less: ['packages/*/*/server/**/*.less', 'packages/*/*/public/!(assets)/*.less', 
            'packages/*/*/public/assets/less/**/*.less'],
        sass: ['packages/**/*.scss', '!packages/**/node_modules/**', '!packages/**/assets/**/lib/**']
    };

var defaultTasks = ['tsc', 'clean', 'less', 'csslint', 'devServe', 'watch'];

function count(taskName, message) {
  var fileCount = 0;

  function countFiles(file) {
    fileCount++; // jshint ignore:line
  }

  function endStream() {
    gutil.log(gutil.colors.cyan(taskName + ': ') + fileCount + ' ' + message || 'files processed.');
    this.emit('end'); // jshint ignore:line
  }
  return through(countFiles, endStream);
}

gulp.task('env:development', function () {
  process.env.NODE_ENV = 'development';
});

gulp.task('jshint', function () {
  return gulp.src(paths.js)
    .pipe(plugins.jshint())
    .pipe(plugins.jshint.reporter('jshint-stylish'))
    // .pipe(plugins.jshint.reporter('fail')) to avoid shutdown gulp by warnings
    .pipe(count('jshint', 'files lint free'));
});

gulp.task('csslint', function () {
  return gulp.src(paths.css)
    .pipe(plugins.csslint('.csslintrc'))
    .pipe(plugins.csslint.reporter())
    .pipe(count('csslint', 'files lint free'));
});

gulp.task('less', function() {
  return gulp.src(paths.less)
    .pipe(plugins.less())
    .pipe(gulp.dest(function (vinylFile) {
      return vinylFile.cwd;
    }));
});

gulp.task('tsc', function() {
    return gulp.src(paths.ts)
        .pipe(ts({
            target: 'ES5'
        }))
        .pipe(gulp.dest('./packages'));
});

gulp.task('devServe', ['env:development'], function () {

  plugins.nodemon({
    script: 'server.js',
    ext: 'html js',
    env: { 
      'NODE_ENV': 'development', 
      'DATABASE': 'mongo'
    },
    ignore: ['node_modules/', 'bower_components/', 'logs/', 'packages/*/*/public/assets/lib/', 'packages/*/*/node_modules/', '.DS_Store', '**/.DS_Store', '.bower-*', '**/.bower-*'],
    nodeArgs: ['--debug=' + (process.env.DEBUG_PORT || 5858 ), '--harmony','--max_old_space_size=30720'],
    stdout: false
  }).on('readable', function() {
    this.stdout.on('data', function(chunk) {
      if(/Mean app started/.test(chunk)) {
        setTimeout(function() { plugins.livereload.reload(); }, 500);
      }
      process.stdout.write(chunk);
    });
    this.stderr.pipe(process.stderr);
  });
});

gulp.task('watch', function () {
  console.log('**********************watch')
  console.log('**************************',process.env.DB_PORT_27017_TCP_ADDR)

  plugins.livereload.listen({
      interval:500,
      port: process.env.LIVERELOAD_PORT ? parseInt(process.env.LIVERELOAD_PORT) : 35729
  });

  gulp.watch(paths.ts, ['tsc']);
  gulp.watch(paths.js, ['jshint']);
  gulp.watch(paths.css, ['csslint']).on('change', plugins.livereload.changed);
  gulp.watch(paths.less, ['less']);
});

gulp.task('development', defaultTasks);
