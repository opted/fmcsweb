FROM collinestes/docker-node-oracle
# node:6.10.3
MAINTAINER Zac Chung

RUN apt-get update && \
	apt-get install -y ssh vim git python2.7 python-pip

RUN pip install pymongo
RUN pip install XlsxWriter
RUN pip install python-dateutil

#RUN npm install -g npm
RUN npm install -g bower gulp
RUN useradd -m mean

RUN unlink /etc/localtime && ln -s /usr/share/zoneinfo/Asia/Taipei /etc/localtime

ADD . /home/mean
ADD . /home/mean/.ssh

RUN chown mean:mean -R /home/mean
RUN chmod 700 /home/mean/.ssh

USER mean

WORKDIR /home/mean

RUN npm install

