'use strict';

/*
 * Defining the Package
 */
var meanio = require('meanio');
var Module = meanio.Module,
  config = meanio.loadConfig(),
  favicon = require('serve-favicon');

var SystemPackage = new Module('system');
/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
SystemPackage.register(function(app, auth, database) {
  // Set user authenzication variables
  // Iinitilize passport
  var session = require('express-session');
  var passportOriginal = require('passport');
  // Need to initial a passport.js first
  var passport = require('../../core/users/server/config/passport.js')(passportOriginal);
  app.use(session({ secret: 'biis'}));
  app.use(passport.initialize());
  app.use(passport.session());
  //We enable routing. By default the Package Object is passed to the routes
  SystemPackage.routes(app, auth, database);

  SystemPackage.aggregateAsset('css', 'common.css');
  SystemPackage.aggregateAsset('js', 'system.js', {weight: -10});
  SystemPackage.aggregateAsset('js', 'ag-grid.js', {weight: -10});
  SystemPackage.aggregateAsset('js', 'dropdown.js');
  SystemPackage.angularDependencies(['mean-factory-interceptor', 'agGrid']);
  

  // The middleware in config/express will run before this code

  // Set views path, template engine and default layout
  app.set('views', __dirname + '/server/views');

  // Setting the favicon and static folder
  if(config.favicon) {
    app.use(favicon(config.favicon));
  } else {
    app.use(favicon(__dirname + '/public/assets/img/kyec.ico'));
  }

  // Adding robots and humans txt
  app.useStatic(__dirname + '/public/assets/static');
  
  SystemPackage.menus.add({
    title: 'Log Out',
    link: 'Log Out',
    roles: ['authenticated'],
    menu: 'account'
  });
  

  return SystemPackage;

});
