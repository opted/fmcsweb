(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;

    var DUTSchema = new Schema({
        'FILEPATH': String,
        'CREATE_TIME': Date,
        'LOTNO': String,
        'OVENID': String,
        'CUSTOMERLOTNO': String,
        'SITENO': String,
        'SLOTINFO':[{
            'BOARD': String,
            'DATETIME': Date,
            'ZONE': String,
            'DUT_LIST':[{
                "DUT": String,
                "ERRCODE": String,
                "DUTV": String,
                "TJ": String,
                "DUTA": String,
                "CAL_TJ": String,
                "TC": String
            }]
        }]
    }, {
        'collection': 'DUT_logs'
    });

    mongoose.model('DUTLOG', DUTSchema);
})();


// (function(){
//     'use strict';
//     var mongoose  = require('mongoose');
//     var Schema    = mongoose.Schema;

//     var ECID2Schema = new Schema({
//         'FILEPATH': String,
//         'OVEN': String,
//         'DATE': Date,
//         'LOTNO': String,
//         'SITENO': String,
//         'OVENID': String,
//         'BTS': String,
//         'CUSTOMERNO': String,
//         'DB': String,
//         'BILOAD': {
//             'CHECKOUTTIME': Date,
//             'EQUIPMENTNO': String,
//             'CHECKOUTEMPLOYEENO': String
//         },
//         'BURNIN': {
//             'CHECKOUTTIME': Date,
//             'EQUIPMENTNO': String,
//             'CHECKOUTEMPLOYEENO': String
//         },
//         'BIUNLOAD': {
//             'CHECKOUTTIME': Date,
//             'EQUIPMENTNO': String,
//             'CHECKOUTEMPLOYEENO': String
//         },
//         'ZONES':[{
//             'ZONE': String,
//             'BOARDS':[{
//                 "Y": Number,
//                 "X": Number,
//                 "BID": String,
//                 "BOARD": String,
//                 "DUTS": [{
//                     "DUT": String,
//                     "ECID": String
//                 }]
//             }]
//         }]
//     }, {
//         'collection': 'ECID_logs_new'
//     });

//     mongoose.model('ECID2', ECID2Schema);
// })();
