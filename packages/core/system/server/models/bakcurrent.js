'use strict';

/************
 *  * Module dependencies.
 *  ************/
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
          _   = require('lodash');

/***********
 *  * BakCurrent Schema
 *  ************/

var BakCurrentSchema = new Schema({
    '_id': {OVENID:String},
    DATETIME: Date,
    SENSOR0: Number,
    SENSOR1: Number,
    SENSOR2: Number,
    SENSOR3: Number,
    SENSOR4: Number,
    SENSOR5: Number,
    LOTNO : [String]
    },{
        collection: 'BakCurrent'
});

mongoose.model('BakCurrent', BakCurrentSchema);

