(function(){
  'use strict';
  var mongoose  = require('mongoose');
  var Schema  = mongoose.Schema;

  var OmsLayoutSchema = new Schema({
    'name': String,
    'x' : Number,
    'y' : Number
  }, {
    'collection': 'OmsLayout'
  });

  mongoose.model('OmsLayout', OmsLayoutSchema);
})();
