
'use strict';

var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
          _   = require('lodash');

/**
 * Self-Test Schema
 */

var DORDailyReportschema = new Schema({
    ASSSERIALNO : String,
    NEW_ASSSERIALNO : String,
    PACKAGETYPE : String,
    DUTOFFRATE : Number,
    DUTONRATE : Number,
    CUSTNAME : String,
    BOARDNO : String,
    TOTAL_BIB : Number,
    TOTAL_SOCKET : Number,
    TOTAL_USED_SOCKET : Number,
    DUTQTY : Number,
    BIB_STATUS : { '0':Number , '1':Number ,'2':Number ,'3':Number ,'4':Number ,'5':Number ,'6':Number ,  },
    FAMILYS: [String],
    DATETIME: Date,
    SMAP_P: Number,
    SMAP_X: Number,
    SMAP_E: Number,
    SMAP_T: Number,
    SMAP_D: Number
}, {
  collection: 'DORDailyReport'
//   collection: 'ST'
});

mongoose.model('DORDailyReport', DORDailyReportschema);