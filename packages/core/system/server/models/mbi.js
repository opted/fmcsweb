(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;
    var mbi_coll_name = 'MBI';

    var MBISchema = new Schema({
      OVENID:String,
      DATETIME:Date,
      ZONE:Number,
      SLOT:Number,
      YIELD:{PASSRATE:Number,BIN:{PASS:Number,ERROR:Number,EMPTY:Number},SOCKET:{P:Number}}
      }, {
      collection: mbi_coll_name
    });

    mongoose.model('MBI', MBISchema);
})();