(function(){
  'use strict';
  var mongoose  = require('mongoose');
  var Schema  = mongoose.Schema;

  var OmsStatusSchema = new Schema({
    'OVENID': String,
    'STATE' : String,
    'UPDATETIME' : Date
  }, {
    'collection': 'OmsStatus'
  });

  mongoose.model('OmsStatus', OmsStatusSchema);
})();
