(function(){
  'use strict';
  var mongoose  = require('mongoose');
  var Schema  = mongoose.Schema;

  var VOLTAGESchema = new Schema({
    'FILEPATH': String,
    'DATETIME' : Date,
    'LOTNO': String,
    'OVENID': String,
    'OVENTYPE': String,
    'DPSIN1' : Number,
    'DPSIN2' : Number,
    'DPSIN3' : Number,
    'DPSIN4' : Number,
    'DPSIN5' : Number,
    'DPSIN6' : Number,
    'DPSIN7' : Number,
    'DPSIN8' : Number,
    'DPSIN9' : Number,
    'DPSIN10' : Number,
    'DPSIN11' : Number,
    'DPSIN12' : Number,
    'DPSIN13' : Number,
    'DPSIN14' : Number,
    'DPSIN15' : Number,
    'DPSIN16' : Number,
    'ZB_Vplus' : Number,
    'ZB_Vminus' : Number,
    'ZB_5V' : Number,
    'VCI' : Number,
    'OVEN_Vplus' : Number,
    'OVEN_Vminus' : Number,
    'ZONE0_5V' : Number,
    'ZONE1_5V' : Number,
    'ZONE2_5V' : Number,
    'ZONE3_5V' : Number
  }, {
    'collection': 'Voltage_logs'
  });

  mongoose.model('VOLTAGE', VOLTAGESchema);
})();
