(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;

    var STDailyStatusschema = new Schema({
        OVENID : String,
        ST_STATUS : Number,
        DATE : Date,
    }, {
      collection: 'STDailyStatus'
    //   collection: 'ST'
    });

    mongoose.model('STDailyStatus', STDailyStatusschema);
})();