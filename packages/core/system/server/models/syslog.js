(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;

    var schema = new Schema({
        'FILEPATH': String,
        'END': Date,
        'START': Date,
        'LOTNO': String,
        'OVENID': String,
        'TYPE': String
    }, {
        'collection': 'SysLog'
    });

    mongoose.model('SysLog', schema);
})();