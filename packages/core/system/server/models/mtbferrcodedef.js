'use strict';

/************
 *  * Module dependencies.
 *  ************/
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema;

/***********
 *  * MTBFERRCODEDEF Schema
 *  ************/

var MTBF_ERRCODEDEF_SCHEMA = new Schema({
    RAW: String,
    DEF: String,
    PRESENT: String
    },{
        collection: 'MTBF_ERRCODEDEF',
});

mongoose.model('MTBF_ERRCODEDEF', MTBF_ERRCODEDEF_SCHEMA);

