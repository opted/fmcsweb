'use strict';

/**
 *  * Module dependencies.
 *   */
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
          _   = require('lodash');

/**
 *  * ET Schema
 *   */

var BakingSchema = new Schema({
    FILEPATH: String,
    STVER: String,
    DATETIME: Date,
    STARTTIME: Date,
    MINUTES: [new Schema({
          SECONDS: [new Schema({
                SENSOR0: Number,
                SENSOR1: Number,
                SENSOR2: Number,
                SENSOR3: Number,
                SENSOR4: Number,
                SENSOR5: Number,
                TIME: Number,
                RANGE: Number,
                MIN: Number,
                MAX: Number
          })],
          TIME: Number
    })],
    LOTNO: [String],
    OVENID: String
}, {
  shardKey: {OVENID: 'hashed'},
  collection: 'BakDetail_logs'
//   collection: 'BAK'
});

mongoose.model('Baking', BakingSchema);
