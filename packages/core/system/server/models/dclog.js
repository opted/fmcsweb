(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;
    var DCLOGSchema = new Schema({
        'Running Mode': String,
        'Right_Oven': Number,
        'Left_Oven': Number,
        'DATETIME': Date,
        'OVENID': String,
        'DPSIN': Number,
        'ZONES':[{
            'BOARD_INIT_STATUS': String,
            'BOARDS':[{
                "DPS3CUR": Number,
                "VDPS2": Number,
                "DPS2LIM": Number,
                "DPS4CUR":Number,
                "VDPS1": Number,
                "VDPS4": Number,
                "DPS2EXP": Number,
                "DPS3LIM": Number,
                "BOARD": Number,
                "VCMP2": Number,
                "OCP": String,
                "VCMP1": Number,
                "VVCLL": Number,
                "VVCLH": Number,
                "DPS1SEN": Number,
                "VOH2": Number,
                "DPS4EXP": Number,
                "DPS2SEN": Number,
                "DPS1LIM": Number,
                "VDPS3": Number,
                "VOH1": Number,
                "VVOHL": Number,
                "DPS3SEN": Number,
                "Vref": Number,
                "DPS1": Number,
                "VVOH2": Number,
                "VOHL": Number,
                "DPS4LIM": Number,
                "VVOH1": Number,
                "DPS3": Number,
                "DPS1CUR": Number,
                "VCLL": Number,
                "DPS3EXP": Number,
                "VCLH": Number,
                "OVP": String,
                "DPS2": Number,
                "DPS4": Number,
                "DPS2CUR": Number,
                "VVCMP2": Number,
                "DPS4SEN": Number,
                "VVCMP1": Number,
                "DPS1EXP": Number,
                "reserve": Number
            }],
            "ZONE": Number,
            "BOARD_DC_READ_RESULT": String,
            "DPSIN_OUTPUT_STATE": Number,
            "TOTAL_CURRENT_DPS1": Number,
            "TOTAL_CURRENT_DPS2": Number,
            "MAX_BOARD": Number
        }],
        'POWER_STATUS': {
            "Heater": Number,
            "Over": Number,
            "Stop": Number,
            "DC": Number,
            "Blower": Number,
            "PC": Number,
            "Desmoke": Number,
            "Motor": Number,
            "Phase": Number
        }
    }, {
        'collection': 'DC_logs'
    });

    mongoose.model('DCLOG', DCLOGSchema);
})();
