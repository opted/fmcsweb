'use strict';

var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
          _   = require('lodash');

/**
 * Self-Test Schema
 */

var ALschema = new Schema({
    ERRORS: [Schema.Types.Mixed],
    LOTNO: String,
    DATETIME: Date,
    OVENID: String
}, {
  collection: 'AutoLoader_logs'
});

mongoose.model('AL', ALschema);
