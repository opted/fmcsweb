(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;

    var ALErrorConfigueschema = new Schema({
        TYPE : String,
        ERR_DESC : String,
        ERR_NO: String,
        MSG_TYPE: String,
        MSG_CLASS: String
    }, {
      collection: 'ALErrorConfig'
    //   collection: 'ST'
    });

    mongoose.model('ALErrorConfig', ALErrorConfigueschema);
})();