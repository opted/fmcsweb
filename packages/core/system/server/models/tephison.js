(function(){
  'use strict';
  var mongoose  = require('mongoose');
  var Schema  = mongoose.Schema;

  var TEPHISONSchema = new Schema({
    'HOLDDISPOSITION': String,
    'CREATEDATE' : Date,
    'LOTNO': String,
    'INCOMINGNO': String,
    'FAMILY': String,
    'DIEQTY': Number,
    'REVISEDATE': Date,
    'RANGE':{
      'FROM': Date,
      'TO': Date,
      'CREATE': Date
    },
  }, {
    'collection': 'te_phison'
  });

  mongoose.model('TEPHISON', TEPHISONSchema);
})();
