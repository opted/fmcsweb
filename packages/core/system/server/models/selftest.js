'use strict';

var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
          _   = require('lodash');

/**
 * Self-Test Schema
 */

var STschema = new Schema({
    TESTITEM : String,
    RESULTS: [Schema.Types.Mixed],
    DLLVER: String,
    TOTALFAILDRV: Number,
    STVER: String,
    DATETIME: Date,
    OVENID: String
}, {
  collection: 'ST_logs'
//   collection: 'ST'
});

mongoose.model('ST', STschema);
