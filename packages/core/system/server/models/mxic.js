'use strict';

var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
          _   = require('lodash');

/**
 * Self-Test Schema
 */

var MXICschema = new Schema({
    SLOTS: [Schema.Types.Mixed],
    LOTNO: String,
    SUB_LOT_ID: String,
    DATETIME: Date,
    START_TEST_TIME: Date,
    // TOTAL_PASS_IC:Number,
    // TOTAL_FAIL_IC:Number,
    // TOTAL_EMPTY_IC:Number,
    OVENID: String
}, {
  collection: 'MXIC_logs'
});

mongoose.model('MXIC', MXICschema);
