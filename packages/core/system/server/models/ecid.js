(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;

    var ECIDSchema = new Schema({
        'FILEPATH': String,
        'OVEN': String,
        'DATE': Date,
        'LOTNO': String,
        'SITENO': String,
        'OVENID': String,
        'BTS': String,
        'CUSTOMERNO': String,
        'DB': String,
        'BILOAD': {
            'CHECKOUTTIME': Date,
            'EQUIPMENTNO': String,
            'CHECKOUTEMPLOYEENO': String
        },
        'BURNIN': {
            'CHECKOUTTIME': Date,
            'EQUIPMENTNO': String,
            'CHECKOUTEMPLOYEENO': String
        },
        'BIUNLOAD': {
            'CHECKOUTTIME': Date,
            'EQUIPMENTNO': String,
            'CHECKOUTEMPLOYEENO': String
        },
        'ZONES':[{
            'ZONE': String,
            'BOARDS':[{
                "Y": Number,
                "X": Number,
                "BID": String,
                "BOARD": String,
                "DUTS": [{
                    "DUT": String,
                    "ECID": String
                }]
            }]
        }]
    }, {
        'collection': 'ECID_logs'
    });

    mongoose.model('ECID', ECIDSchema);
})();
