(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;

    var StateSchema = new Schema({
        'isUsing': Boolean,
        'userName': String,
        'address': String,
        'optime': Number,
        'service': String
    }, {
        'collection': 'ServiceState'
    });

    mongoose.model('ServiceState', StateSchema);
})();
