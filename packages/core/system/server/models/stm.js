(function(){
  'use strict';
  var mongoose  = require('mongoose');
  var Schema  = mongoose.Schema;
  var STMSchema = new Schema({
    "XSOCK" : Number,
    "FAMILY" : String,
    "KYEC_BOARD_ID" : String,
    "YSOCK" : Number,
    "YIELD" : Number,
    "BINMAP" : String,
    "PASS" : Number,
    "FAIL" : Number,
    "LOTNO" : String,
    "CUSTOMER_BOARD_ID" : String,
    "FLOW_NAME" : String,
    "EMPTY" : Number,
    "SLOT" : Number,
    "FILEPATH" : String,
    "BI_START_TIME" : Date,
    "BI_END_TIME" : Date,
    "EXP_DATE" : Date,
    "TOTAL" : Number,
    "OVENID" : String,
    "COLUMN_FAIL": [{
      "INDEX" : Number,
      "DATA": String
    }]
  }, {
    'collection': 'STM_logs'
  });

  mongoose.model('STM', STMSchema);
})();
