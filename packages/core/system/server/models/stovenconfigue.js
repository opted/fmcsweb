(function(){
    'use strict';
    var mongoose  = require('mongoose');
    var Schema    = mongoose.Schema;

    var STOvenConfigueschema = new Schema({
        OVENID : String,
        TYPE : String,
        SLOT : String,
    }, {
      collection: 'STOvenConfigue'
    //   collection: 'ST'
    });

    mongoose.model('STOvenConfigue', STOvenConfigueschema);
})();