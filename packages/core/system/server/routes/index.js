'use strict';

var mean = require('meanio');
var userAuthConfig = require('../../../../../config/userauth-config.js'),
  userAuth = undefined;

module.exports = function(System, app, auth, database) {

  // Home route
  var index = require('../controllers/index')(System);
  var definedUserAuth = function(){
    if(userAuth==undefined)
      userAuth = require('../../../../../packages/core/users/server/controllers/userauth.js');
  };
  var userAuthConfig = require('../../../../../config/userauth-config.js');
  var authIgnoreApi = userAuthConfig.authIgnoreApi;
  var getNotAuthApi = authIgnoreApi.get, 
    postNotAuthApi = authIgnoreApi.post,
    getStwNotAuthApi = authIgnoreApi.getStartsWith;

  app.route('/')
    .get(index.render);
  app.route('/api/aggregatedassets')
    .get(index.aggregatedList);

  app.get('/*', function(req,res,next){ 
    res.header('workerID' , JSON.stringify(mean.options.workerid) );
    next(); // http://expressjs.com/guide.html#passing-route control
  });
  app.get('/api/get-public-config', function(req, res){
    var config = mean.loadConfig();
    return res.send(config.public);
  });

  var checkStartWithInArray = function(msg,arr){
    var check = false;
    arr.forEach(function(path){
      if(msg.startsWith(path))
        check = true;
    });
    return check;
  };
  app.get('/api/*', function(req,res,next){ 
    definedUserAuth();
    if(getNotAuthApi.indexOf(req.url)>=0)
      next();
    else
      userAuth.checkAuth(req,res,next);
  });
  app.post('/api/*', function(req,res,next){
    definedUserAuth();
    if(postNotAuthApi.indexOf(req.url)>=0)
      next();
    else{
      userAuth.checkAuth(req,res,next);
    }
  });

};
