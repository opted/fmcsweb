'use strict';

angular.module('mean-factory-interceptor', [])
  .factory('httpInterceptor', ['$q', '$location', '$window','$cookies',
    function($q, $location, $window, $cookies) {
      return {
      request: function($config) {
        //Fetch token from cookie
        var token=$cookies.get('auth-token');
        //set authorization header
        if(token){
          $config.headers['User-Authorization'] = token;
        }
        return $config;
      },
      'response': function(response) {
          if (response.status === 401) {
            $location.path('/login');
            return $q.reject(response);
          }
          else if(response.headers('AuthToken')){
            $cookies.put('auth-token', response.headers('AuthToken'));
          }
          return response || $q.when(response);
      },
      'responseError': function(rejection) {
        if (rejection.status === 401) {
          $location.url('/login');
          return $q.reject(rejection);
        }
        return $q.reject(rejection);
      }
      };
    }
  ])
//Http Interceptor to check auth failures for XHR requests
.config(['$httpProvider',
  function($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
  }
]);
