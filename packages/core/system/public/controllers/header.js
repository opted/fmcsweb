'use strict';
var wss;
angular.module('mean.system')
.controller('HeaderController', function($scope, $state, $http, $window, MeanUser) {
	wss = $window.sessionStorage;
	$scope.hdrvars = MeanUser.headerAuthInfo;//{authenticated:false,user:{name:""}};
	$scope.logout = function(){
		MeanUser.logOut(function(){
			$scope.hdrvars.authenticated = false;
			$scope.hdrvars.user.name = "";
		});
	};
	MeanUser.loadUserInfo(MeanUser);
});
