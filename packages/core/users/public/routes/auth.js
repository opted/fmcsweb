'use strict';

//Setting up route
angular.module('mean.users').config(['$meanStateProvider', '$httpProvider', 'jwtInterceptorProvider',
  function($meanStateProvider, $httpProvider, jwtInterceptorProvider) {    
        
    jwtInterceptorProvider.tokenGetter = function() {
      return localStorage.getItem('JWT');
    };

    $httpProvider.interceptors.push('jwtInterceptor');

    // states for my app
    $meanStateProvider
	.state('usermanagement', {
		url: '/usermanagement',
		templateUrl: 'users/views/userManagement.html',
		controller: 'userManagementCtrl',
		resolve: {
			loggedin: function(MeanUser) {
				return MeanUser.checkLoggedIn();
			}
		}
    })
	.state('login', {
		url: '/login',
		templateUrl: 'users/views/login.html',
		controller: 'loginCtrl'
    })
	.state('accountsetting', {
		url: '/accountsetting',
		templateUrl: 'users/views/accountSetting.html',
		controller: 'accountSettingCtrl',
		resolve: {
			loggedin: function(MeanUser) {
				return MeanUser.checkLoggedIn();
			}
		}
	})
	.state('userrequestlog', {
		url: '/userrequestlog',
		templateUrl: 'users/views/userRequestLog.html',
		controller: 'userRequestLogCtrl',
		resolve: {
			loggedin: function(MeanUser) {
				return MeanUser.checkLoggedIn();
			}
		}
	});
  }
]);