/**
 * Created by geja on 8/29/16.
 */
angular.module('mean.users')
.factory('floatingToolTip', function() {
	var supportedType = ['success', 'info', 'warning', 'danger'];
	function removeToolTip(randomnum){
		$('#ftt-'+randomnum).fadeOut();
		setTimeout(function(){
			$('#ftt-'+randomnum).remove();
		},500);
	};
	return {
		createToolTip: function(type,message,rmtimeout){
			// Support: success, info, warning, danger four types, that contextual equal to bootstrap alert class
			if(_.indexOf(supportedType,type)<0){
				console.log('Error Type!');
				return;
			}
				
			var randomnum = Math.round(Math.random()*1000);
			$("body").append('<div class="alert alert-'+type+' fade in" id="ftt-'+randomnum
				+'" style="z-index:1;position:absolute;width:30%;">'
				//+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'
				+'<div style="text-align:center;">'
				+message+'</div></div>');
			var left = (document.body.clientWidth - $('#ftt-'+randomnum).width())/2;
			var top = window.pageYOffset+80;
			$('#ftt-'+randomnum).css({'left':left+'px','top':top+'px'});
			
			if(_.isNumber(rmtimeout))
				setTimeout(function(){
					removeToolTip(randomnum);
				},rmtimeout);
			return randomnum;
		},
		removeToolTip: removeToolTip,
		easyTemporaryToolTip: function(type,message,rmtimeout){
			if(rmtimeout==undefined)
				rmtimeout = 2000;	
			message = '<strong>'+type.toUpperCase()+'! </strong>'+message;
			this.createToolTip(type,message,rmtimeout);
		}
	};
});
