'use strict';

angular.module('mean.users').factory('MeanUser', [ '$rootScope', '$http', '$location', '$stateParams', 
'$cookies', '$q', '$timeout', '$window',
  function($rootScope, $http, $location, $stateParams, $cookies, $q, $timeout, $window) {
	function GenerateFunc(name, options) {
		return function(value) {
			if (value) {
				$window.sessionStorage.setItem('datalog.'+name, JSON.stringify(value));
				if (options) {
					_.forEach(options.removeItems, function(item) {
						$window.sessionStorage.removeItem('datalog.'+item);
					});
				}
			} else {
				var rr = $window.sessionStorage.getItem('datalog.'+name)
				if (options && options.transOutput) rr = options.transOutput(rr);
				if (rr === null && options && options.default) rr = options.default;
				return rr;
			}
		};
	};
	var logIn = function(loginparams,callback){
    // User want to ignore uppercase or lowercase of id for login, 
    // so we uniformly turn id into lowercase.
    loginparams.name = loginparams.name.toLowerCase();
		$http.post('/api/login', loginparams)
		.then(function (res) {
			if(typeof callback=="function")
				callback(res);
		},
		// handel error
		function(err){
			console.log(err);
		});
	};
	var loadUserInfo = function(meanUser,callback){
		$http.post('/api/logincheckout') // checkout only
		.then(function (res) {
			if(typeof res.data=="string"){ // not log in
				meanUser.userInfo({local:undefined});
				console.log("not log in");
				// meanUser.clearlocalStorage();
			}
			else{ // log in then load user information
				// load user profile
				$http.post('/api/logincheckout',{userinfo:true}) // get user information
				.then(function (res2) {
					delete res2.data.SETTINGS;
					meanUser.userInfo(res2.data);
					meanUser.headerAuthInfo.authenticated = true;
					// If full name is defined, using it
					if(meanUser.userInfo().local.fullname)
						meanUser.headerAuthInfo.user.name = meanUser.userInfo().local.fullname;
					else
						meanUser.headerAuthInfo.user.name = meanUser.userInfo().local.name;
					// Set authentication of admin
					if(meanUser.userInfo().local.name==='admin')
						meanUser.headerAuthInfo.adminAuth = true;
					else
						meanUser.headerAuthInfo.adminAuth = false;
					$window.localStorage.setItem('userProfile',{});//,JSON.stringify(meanUser.userInfo()));
					$window.localStorage.setItem('userName',meanUser.userInfo().local.name);
					if(typeof callback=="function")
						callback();
				});
			}
		});
	}
	var checkLoggedIn = function(){
		var deferred = $q.defer();
		$timeout(deferred.resolve);
		return deferred.promise;
	};
	var logOut = function(callback){
		var meanUser = this;
		$http.get('/api/logout')
		.then(function (res) {
		if(res.data == 'logout'){
			meanUser.userInfo({local:undefined});
			meanUser.clearlocalStorage();
			$cookies.remove('auth-token');
			$location.path("/login");
			callback();
		}
		});
	};
	var clearlocalStorage = function(keepStorageList){
		if(!keepStorageList)
			keepStorageList = [];
		for(var key in $window.localStorage){
			if(_.indexOf(keepStorageList,key)<0)
			$window.localStorage.removeItem(key);
		}
	};

    return {logIn:logIn,
	loadUserInfo:loadUserInfo,
	userInfo:new GenerateFunc('userInfo',{
	  	transOutput: function(str){
	  	  return JSON.parse(str);
	  	}
	  }),
	checkLoggedIn:checkLoggedIn,
	logOut:logOut,
	clearlocalStorage:clearlocalStorage,
	headerAuthInfo:{authenticated:false,adminAuth:false,user:{name:""}}};
  }
]);
