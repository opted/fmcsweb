'use strict';
angular.module('mean.users')
  .controller('userManagementCtrl', function($scope, $state, $http, $uibModal, Upload) {
	  $scope.$on('$viewContentLoaded', function(){
		  $('body').removeAttr("style");
		  $('.sk-fading-circle').empty();
	  });
	$scope.init = function(){
		$scope.loadUserData();
	};
  $scope.inputData = {};
	$scope.updateInfo = {};	
	$scope.errorMessage = "";
	$scope.successMessage = "";
	$scope.updateErrorMessage = "";
	$scope.companySelection = {companies:['ABC','DEF','GGG']};	
	/**
	 * Register function
	**/
    $scope.register = function(){
      if($scope.inputData.name.length<1 || $scope.inputData.password.length<1){
        $scope.errorMessage = "Please input username and password.";
      }
      else if($scope.inputData.password!=$scope.inputData.repassword)
      {
        $scope.errorMessage = "Password is different to repeat password.";
      }
      else{
      // User want to ignore uppercase or lowercase of id for login, 
      // so we uniformly turn id into lowercase.
      $scope.inputData.name = $scope.inputData.name.toLowerCase(); 
		  $http.post('/api/register', $scope.inputData)
		  .then(function (res) {
			if(res.data == 'done'){
			  cleanMessage();
			  $scope.successMessage = "Registeration Success!";
			  $scope.loadUserData();
			  setTimeout(function(){$scope.successMessage="";},3000);
			}else{
			  $scope.errorMessage = res.data;
			}
		  });
	  }
    };
	// Tab Control
	$scope.tabShow = [true,false];
	$scope.switchTab = function(idx){
		for(var i in $scope.tabShow)
			$scope.tabShow[i]=false;
		if(idx==0){
		  $('#umtab #tab0').attr('class','active')
		  $('#umtab #tab1').attr('class','inactive')
		}
		else{
		  $('#umtab #tab0').attr('class','inactive')
		  $('#umtab #tab1').attr('class','active')
		}
		$scope.tabShow[idx]=true;
	}
		
	/**
		User Information Table
	**/	
	var columnDefs = [
		{headerName: '', width: 30, checkboxSelection: true, suppressSorting: true,suppressMenu: true, pinned: true},
    {headerName: "ID", field: "name", filter: 'text'},
		{headerName: "FullName", field: "fullname", filter: 'text'},
    {headerName: "DepartmentID", field: "depid", filter: 'text'},
    {headerName: "E-Mail", field: "email", filter: 'text'},
    {headerName: "OMS Editable", field: "omsAuthority"}
    ];
    var testdata = [];

  $scope.gridOptions = {
		columnDefs: columnDefs,
		enableFilter: true,
		rowSelection: 'multiple',
		enableColResize: true,
		rowData: testdata,
		enableSorting: true,
		angularCompileRows: true,
		rowHeight: 35,
		enableRangeSelection: true
    };
  // Export user information
  $scope.callExportUserCsvModal = function(){
    $scope.modalInstance = $uibModal.open({
      templateUrl: '/users/views/modal/exportUserCsv.html',
      size: 'md',
      scope: $scope,
    });
	};
  $scope.exportUserCsv = function(){
    var params = {suppressQuotes:true,columnKeys:['name','fullname','depid','email']};
    $scope.gridOptions.api.exportDataAsCsv(params);
    $scope.close();
  };
  
	// Load User Data
	$scope.loadUserData = function(){
		$http.post('/api/usersdata')
		  .then(function (res) {
			testdata = res.data;
			$scope.gridOptions.api.setRowData(testdata);
		});
    };
	/**
	* Delete, Update modal function
	**/
	// Delete selected user
	$scope.callDeleteUserModal = function(){
		$scope.selectedRows = $scope.gridOptions.api.getSelectedRows();
    $scope.namelist = _.map($scope.selectedRows,'name');
    $scope.modalInstance = $uibModal.open({
      templateUrl: '/users/views/modal/deleteUser.html',
      size: 'lg',
      scope: $scope,
      //controller: 'userModifiedCtrl'
    });
	};
	$scope.deleteUser = function(){
		$http.post('/api/deluser',{namelist:$scope.namelist})
		  .then(function (res) {
			console.log(res.data);
			$scope.loadUserData();
      $scope.namelist = '';
			$scope.close();
		});
	};
	
	$scope.callUpdateUserModal = function(){
		$scope.selectedRows = $scope.gridOptions.api.getSelectedRows();
    if($scope.selectedRows.length<=0)
      alert('Please select at least one row.');
		else if($scope.selectedRows.length>1)
			alert('You can only edit one user\'s profile at once!');
		else if($scope.selectedRows.length==1){
			$scope.updateInfo.oldname = $scope.selectedRows[0].name;
			$scope.modalInstance = $uibModal.open({
				templateUrl: '/users/views/modal/updateUser.html',
				size: 'lg',
				scope: $scope,
			});
		}
	};
	// Edit selected Row
	$scope.updateUser = function(){
		var updateoptions = {};
		for(var key in $scope.updateInfo){
			if(key!='oldname' && key!='repassword')
				if($scope.updateInfo[key].length>0)
					updateoptions['local.'+key]=$scope.updateInfo[key];
		}
		if($scope.updateInfo.password!=undefined)
			if($scope.updateInfo.password!=$scope.updateInfo.repassword){
				alert("Password is different to repeat password.");
				return;
			}
		
		if(!_.isEmpty(updateoptions)){
			console.log(updateoptions);
			$http.post('/api/updateuser',{name:$scope.updateInfo.oldname,updatedoc:updateoptions})
			  .then(function (res) {
				if(res.data.status>1)
					$scope.updateErrorMessage = res.data.desc;
				else{
					console.log(res.data.desc);
					$scope.loadUserData();
					$scope.close();
				}
		});}
	};
	$scope.close = function () {
    $scope.modalInstance.dismiss('cancel');
		clearUpdateMessage();
  };
	/**
	*	Clear input text box
	**/
	$scope.clearMessage = function(){
		cleanMessage();
	};	
	var cleanMessage = function(){		
		for(var key in $scope.inputData)
			$scope.inputData[key] = '';			
		$scope.errorMessage = "";
		$scope.successMessage = "";
	};
	var clearUpdateMessage = function(){
		$scope.updateInfo = {};
		$scope.updateErrorMessage = "";
	};
	cleanMessage();
  /**
  * Upload User Data
  */
  var uploadLimitSize = 1024*1024*10; // 10 MB
  $scope.uploadDataHint = '<h3>CSV上傳格式</h3>'+
  '<ol>'+
  '<li>檔案必須為UTF-8編碼(否則上傳中文會亂碼)</li>'+
  '<li>必須要有欄位header</li>'+
  '<li>欄位格式: id,fullname,departmentid,e-mail,password</li>'+
  '<li>欄位header無視大小寫且無須排列順序，但是文字必須正確 (ex: e-mail寫成E-Mail正確，寫成email則錯誤)</li>'+
  '<li>附檔名必須為 ".csv"</li>'+
  '<li>上傳檔案限制 10 MB</li>'+
  '<li>更新(Update)功能附註'+
      '<ol type="a"><li>無法更新password(此欄位會被無視)</li>'+
      '</ol></li>'+
  '</ol>'+
  '<h3>CSV範例</h3><dl>'+
  '<dd>id,fullname,departmentid,e-mail,password'+
  '<dd>test123,TTT123,AB01,test123@tmail.com,test123'+
  '<dd>test456,TTT456,DD03,test456@tmail.com,test456</dl>';
  
  $scope.modalParams = {selectedfile:undefined};
  $scope.uploadStatus = {'status':undefined,'importedCount':0,'errorCount':0,errorLog:[],updateCheck:false};
  $scope.uploading = false;
  $scope.resetUploadStatus = function(){
    $scope.uploadStatus = {'status':undefined,'importedCount':0,'errorCount':0,errorLog:[],updateCheck:false};
  };
  $scope.uploadClose = function () {
    $scope.resetUploadStatus();
    $scope.modalParams.selectedfile = undefined;
    $scope.modalInstance.dismiss('cancel');
  };

  $scope.changeOmsAuthority = function(){
    $scope.selectedRows = $scope.gridOptions.api.getSelectedRows();
    $scope.namelist = _.map($scope.selectedRows,'name');
    $http.post('/api/changeOmsAuthority',{namelist:$scope.namelist})
      .then(function (res) {
        $scope.loadUserData();
        $scope.namelist = '';
      });
  };
  
  $scope.callUploadUserCsvModal = function(){
    $scope.modalInstance = $uibModal.open({
      templateUrl: '/users/views/modal/uploadUserCsv.html',
      size: 'lg',
      scope: $scope
    });
	};
	$scope.uploadUserData = function(){
    if($scope.modalParams.selectedfile!=undefined){
      if($scope.modalParams.selectedfile.size>uploadLimitSize){ // file size is too large
        alert('The size of file is more than 10 MB, please reduce the size.');
        return;
      }
      else if(!$scope.modalParams.selectedfile.name.includes('.csv')){
        alert('This is not a csv file.');
        return;
      }
      $scope.uploading=true;
      // register of update
      var uploadUrl = '/api/bulkregister';
      if($scope.uploadStatus.updateCheck)
        uploadUrl='/api/bulkupdate';
      
      Upload.upload({
        url: uploadUrl,
        data: {file: $scope.modalParams.selectedfile}
      }).then(function (resp) {
        $scope.uploading = false;
        $scope.uploadStatus.importedCount = resp.data.importedCount;
        $scope.uploadStatus.errorCount = resp.data.errorCount;
        $scope.uploadStatus.errorLog = resp.data.errorLog;
        $scope.uploadStatus.status='success';
        $scope.loadUserData();
      }, function (resp) {
        console.log('Error status: ' + resp.status);
        $scope.uploading = false;
        $scope.uploadStatus.status='failed';
        $scope.uploadStatus.errorLog = resp.data.errorLog;
      }, function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
      });
    }
    else{
      alert('Pelease select a file.');
    }
  };
});

