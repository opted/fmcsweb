'use strict';
var debug = undefined;
angular.module('mean.users')
  .controller('userRequestLogCtrl', function($scope, $state, $http) {
	  $scope.$on('$viewContentLoaded', function(){
		  $('body').removeAttr("style");
		  $('.sk-fading-circle').empty();
	  });
    debug = $scope;
	$scope.init = function(){
		$scope.loadUserData();
	};
	$scope.windowHeight = document.body.clientHeight*0.6;
	$scope.showingRows = 0;
	/**
		Table of Request Log of User
	**/		
	var columnDefs = [
		{headerName: '', width: 30, checkboxSelection: true, suppressSorting: true,suppressMenu: true, pinned: true},
    {headerName: "ID", field: "USER_NAME"},
		{headerName: "Request Path", field: "REQ_PATH", filter: 'text'},
    {headerName: "Full Name", field: "FULL_NAME", filter: 'text'},
    {headerName: "Host URL", field: "CONN_URL",hide:true},
    {headerName: "Department ID", field: "DEPARTMENT_ID",hide:true},
    {headerName: "Date Time", field: "DATETIME", filter: 'text'}
    ];
	var reqdata = [];

    $scope.gridOptions = {
		columnDefs: columnDefs,
		enableFilter: true,
		rowSelection: 'multiple',
		enableColResize: true,
		rowData: reqdata,
		enableSorting: true,
		angularCompileRows: true,
		enableRangeSelection: true
    };
	
	var hr8 = 60*60*8*1000;
	// Load Request Data
	$scope.loadUserData = function(days,callback){
		if($scope.gridOptions.api!=undefined)
			$scope.gridOptions.api.showLoadingOverlay();
		$http.post('/api/reqlogdata',{trange:days})
		.then(function (res) {
			reqdata = res.data;
			if(callback)
				callback();
			_.forEach(reqdata, doc => doc["DATETIME"] = moment(doc["DATETIME"]).format('YYYY/MM/DD HH:mm:ss'));
			$scope.gridOptions.api.setRowData(reqdata);
			$scope.gridOptions.api.hideOverlay();
		});
    };
	/**
		Time Range of Request Log
	**/
	var trangeMapDay={'1 day':1,'2 days':2,'3 days':3,'5 days':5,'7 days':7,'14 days':14,'30 days':30,'60 days':60,'90 days':90};
	$scope.trangeList=_.keys(trangeMapDay);
	$scope.gridTimeRange='7 days';	
	$scope.changeTimeRange = function(idx){
		$scope.gridTimeRange=$scope.trangeList[idx];
		$scope.loadUserData(trangeMapDay[$scope.gridTimeRange]);
	};
	// Call initial function	
	$scope.loadUserData(trangeMapDay[$scope.gridTimeRange],function(){
		// Add event listener
		$scope.gridOptions.api.addEventListener('modelUpdated', function(){
		if(debug.gridOptions.api.rowModel!=undefined)
			$scope.showingRows = debug.gridOptions.api.rowModel.rowsToDisplay.length;
		});
	});
	
	$scope.downloadCsv = function(){
		$scope.gridOptions.api.exportDataAsCsv({fileName:'requestlog_'+(new Date()).toLocaleDateString() +".csv"});
	}
});

