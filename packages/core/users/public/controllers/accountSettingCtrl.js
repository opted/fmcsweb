'use strict';

angular.module('mean.users')
  .controller('accountSettingCtrl', function($scope, $state, $http, $uibModal,$window, MeanUser, floatingToolTip) {

	  $scope.$on('$viewContentLoaded', function(){
		  $('body').removeAttr("style");
		  $('.sk-fading-circle').empty();
	  });
    /**
	 Tab Control
	*/
	$scope.tabShow = [true,false];
	$scope.switchTab = function(idx){
		if(idx==0){
		  $('#umtab #tab0').attr('class','active')
		  $('#umtab #tab1').attr('class','inactive')
		}
		else{
		  $('#umtab #tab0').attr('class','inactive')
		  $('#umtab #tab1').attr('class','active')
		}
		for(var i in $scope.tabShow)
			$scope.tabShow[i]=false;
		$scope.tabShow[idx]=true;
	}
	
	
	/**
	 Account Settings
	*/
	$scope.user = {};
	$scope.id = "";
	$scope.as_edit_disabled = true;	
	$scope.init = function(){
		MeanUser.loadUserInfo(MeanUser,function(){
			$scope.id = MeanUser.userInfo().local.name || "";
			$scope.user.fullname = MeanUser.userInfo().local.fullname || "";
			$scope.user.email = MeanUser.userInfo().local.email || "";
      $scope.user.depid = MeanUser.userInfo().local.depid || "";
		});
	};
	
	$scope.editAccountSettings  = function(){
		$scope.as_edit_disabled = !$scope.as_edit_disabled;
	};
	$scope.saveAccountSettings = function(){
		if(!$scope.as_edit_disabled){
			var updateoptions = {};
			for(var key in $scope.user)
				updateoptions['local.'+key] = $scope.user[key];			
			$http.post('/api/updateuser',{name:$scope.id,updatedoc:updateoptions})
				.then(function (res) {
				if(res.data.status>1)
					alert(res.data.desc);
				else{
					console.log(res.data.desc);
					floatingToolTip.easyTemporaryToolTip('success','Update Success!');
					$window.localStorage.removeItem("userProfile");//force to reload user information from server
				}
			});
			$scope.as_edit_disabled = true;
		}		
	};
	/**
	 Change Password
	*/
	$scope.changepw = {curpassword:"",password:"",repassword:""};
	$scope.changePassword = function(){
		
		if($scope.changepw.password.length<4){
			alert("Your password is too short, at least enter more than 4 characters.");
			return;
		}
		
		if($scope.changepw.password!=undefined)
			if($scope.changepw.password!=$scope.changepw.repassword){
				alert("Password is different to repeat password.");
				return;
			}
		
		
		var updateoptions = {};
		for(var key in $scope.changepw){
			if(key!='repassword')
				if($scope.changepw[key].length>0){
					updateoptions['local.'+key]=$scope.changepw[key];
				}
		}
		$http.post('/api/updateuser',{name:$scope.id,updatedoc:updateoptions})
			.then(function (res) {
			if(res.data.status>1) // failed
				alert(res.data.desc);
			else{
				floatingToolTip.easyTemporaryToolTip('success','Update Success!');
				console.log(res.data.desc);
				$scope.clearPassword();
			}
		});
		
	};
	$scope.clearPassword = function(){
		for(var key in $scope.changepw)
			$scope.changepw[key]="";
	};
});

