'use strict';

angular.module('mean.users')
  .controller('loginCtrl', function($scope, $state, $http, $rootScope, $rootElement ,MeanUser) {
    $scope.$on('$viewContentLoaded', function(){
      $('body').css({position: '', top:'', left:'', width: '', height:'', zIndex:'', opacity:'', filter: ''});
      $('.sk-fading-circle').empty();
    });
    var rootBody = $rootElement[0].body;
    rootBody.style.backgroundRepeat = "no-repeat";
    rootBody.style.backgroundSize = "cover";
    //rootBody.style.backgroundImage = 'url("datalog/assets/img/login_background.jpg")';
    $('div#header').css('display', 'none');
    $('div.color-line').css('display', 'none');
    $scope.inputData = {name:undefined,password:undefined};
  $scope.errorMessage = "";
    $scope.login = function(){    
    var isChromium = window.chrome,
      winNav = window.navigator,
      vendorName = winNav.vendor,
      isOpera = winNav.userAgent.indexOf("OPR") > -1,
      isIEedge = winNav.userAgent.indexOf("Edge") > -1,
      isIOSChrome = winNav.userAgent.match("CriOS");
    // is Google Chrome on IOS
    // is Google Chrome
    if (isIOSChrome || (isChromium !== null && isChromium !== undefined && vendorName === "Google Inc." && isOpera == false && isIEedge == false)) {
      if($scope.inputData.name==undefined){
        $scope.errorMessage = "Please enter your user name.";
        return;
      }
      else if($scope.inputData.password==undefined){
        $scope.errorMessage = "Please enter your password.";
        return;
      }
      $scope.inputData.name = $scope.inputData.name.toLowerCase();
      MeanUser.logIn($scope.inputData, function (res) {
        //MeanUser.clearlocalStorage();
        if (res.data.status > 1) {
          $scope.errorMessage = res.data.desc;
        } else {
          $scope.errorMessage = "";
          MeanUser.loadUserInfo(MeanUser , function () {
            $state.go('home');
            $('div#header').css('display', '');
            $('div.color-line').css('display', '');
          });
        }
      });
    } else {
      // not Google Chrome
      window.alert("Please use Chrome to login FTEDA!")
    }
    };
  });

