'use strict';

var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var request = require('request');

module.exports = function(MeanUser, app) {
  
  var userauth = require('../controllers/userauth.js');

  MeanUser.ctrls = {
  };
  
  app.post('/api/login', userauth.login);
  app.post('/api/logincheckout', userauth.logincheckout);
  app.post('/api/register', userauth.register);
  app.get('/api/logout', userauth.logout);
  app.post('/api/usersdata', userauth.usersdata);
  app.post('/api/deluser', userauth.deluser);
  app.post('/api/changeOmsAuthority', userauth.changeOmsAuthority);
  app.post('/api/updateuser', userauth.updateuser);
  app.post('/api/updateusersettings', userauth.updateuserSettings);
  app.post('/api/updateuserprofile', userauth.updateUserProfile);
  app.post('/api/loaduserprofile', userauth.loadUserProfile);
  app.post('/api/removeuserprofile', userauth.removeUserProfile);
  app.post('/api/reqlogdata', userauth.requestLogData);
  app.post('/api/bulkregister', userauth.bulkRegister);
  app.post('/api/bulkupdate', function(req,res,next){
    req.body.update = true;
    userauth.bulkRegister(req,res,next);
  });
  app.get('/api/test', function(req, res, next) {
    res.json("test successed!");
  });
};
