'use strict';

/**
 * Module dependencies.
 */
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema,
    bcrypt    = require('bcrypt-nodejs');

/**
 * ET Schema
 */

var UserSchema = new Schema({
    local:{
      email: String,
      password: String,
      name: String,
      fullname: String,
      depid: String,
      omsAuthority: Boolean
    },
    window: {
      id: String,
      token: String,
      email: String,
      name: String
    },
    LABEL:[String],
    UPLOAD:[String]	
}, {
  collection: 'User',
  read: 'secondaryPreferred'
});

UserSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

UserSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

mongoose.model('User', UserSchema);


