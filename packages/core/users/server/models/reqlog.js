'use strict';

/**
 * Module dependencies.
 */
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema;

/**
 * Request Log Schema
 */

var reqLogSchema = new Schema({
  USER_NAME:String,
	REQ_PATH:String,
	CONN_URL:String,
	HOST_NAME:String,
  FULL_NAME:String,
  DEPARTMENT_ID:String,
	DATETIME:Date
}, {
  collection: 'RequestLog',
  read: 'secondaryPreferred'
});
reqLogSchema.index( { DATETIME: -1 }, { expireAfterSeconds: 60*60*24*93 } );
reqLogSchema.index({USER_NAME:1});
reqLogSchema.index({REQ_PATH:1});
reqLogSchema.index({CONN_URL:1});
reqLogSchema.index({HOST_NAME:1});
reqLogSchema.index({FULL_NAME:1});
reqLogSchema.index({DEPARTMENT_ID:1});

mongoose.model('reqLog', reqLogSchema);