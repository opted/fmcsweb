'use strict';

/**
 * Module dependencies.
 */
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema;

/**
 * UserProfile Schema
 */

var UserProfileSchema = new Schema({
	USERNAME:String,
	TABLE:{STDF_FILE_TABLE:Array,SELECT_TEST_ITEM_TABLE:Array},
	CONFIG:Object
}, {
  collection: 'UserProfile',
  read: 'secondaryPreferred'
});
mongoose.model('UserProfile', UserProfileSchema);