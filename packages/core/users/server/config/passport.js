'use strict';

var mongoose = require('mongoose');
var LocalStrategy = require('passport-local').Strategy,
    User = mongoose.model('User'),
    config = require('meanio').loadConfig();

module.exports = function(passport) {
  // Serialize the user id to push into the session
  passport.serializeUser(function(user, done) {
    done(null, user.id);
  });

  passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
  });

  // local registery
  passport.use('local-signup', new LocalStrategy({
    usernameField : 'name',
    passwordField : 'password',
    passReqToCallback : true 
  },
  function(req, name, password, done) {
    name = name.toLowerCase();
    process.nextTick(function() {
      User.findOne({'local.name' :  name}, function(err, user) {
        if (err)
          return done(err);
        if (user) {
          return done('That id is already taken.' , false);
        }else{
          var newUser            = new User();
		  newUser.local.name = name;
          newUser.local.password = newUser.generateHash(password);
		  newUser.local.email = req.body.email; // extra user information
		  newUser.local.fullname = req.body.fullname; // extra user information
      newUser.local.depid = req.body.depid; // extra user information
          newUser.save(function(err) {
            if (err)
              throw err;
            return done(null, newUser);
          });
        }
      });    
    });
  }));

  passport.use('local-login', new LocalStrategy({
    usernameField : 'name',
    passwordField : 'password',
    passReqToCallback : true
  },
  function(req, name, password, done) {
    User.findOne({ 'local.name' :  name}, function(err, user) {
      if (err)
        return done(err);
      if (!user)
        //return done(null, false, console.log('No user found.'));
        return done('No user found.');
      if (!user.validPassword(password))
        //return done(null, false, console.log('Oops! Wrong password.'));
        return done('Oops! Wrong password.');
      return done(null, user);
    });
  }));
  return passport;
};
