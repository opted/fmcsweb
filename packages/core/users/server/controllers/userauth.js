'use strict';
/**
 * Module dependencies.
 */
var passportOriginal = require('passport'),passport = require('../config/passport.js')(passportOriginal);
var mongoose = require('mongoose');
var User = mongoose.model('User');
var reqLog = mongoose.model('reqLog');
var UserProfile = mongoose.model('UserProfile');
var bcrypt = require('bcrypt-nodejs');
var fs = require('fs');
var jwt = require('jsonwebtoken');
var os = require('os');
var Q = require('q');
var _ = require('lodash');
var uploader = require('./upload.js')

var privateKey = 'privatekeyforbiis';
var exptime = '7d';
exports.privateKey = privateKey;

var userAuthConfig = require('../../../../../config/userauth-config.js');

var noAutentication = function(res){
  return res.status(401).send('no authentication!');
}

exports.login = function(req, res, next) {
  passport.authenticate('local-login', function(err, user, info) {
    if (err) {
      return res.json({status:2,desc:'login error1: '+String(err)});
    } else {
      req.logIn(user, function(err) {
        if (err) {
          return res.json({status:2,desc:'login error2: '+String(err)});
        } else {
          var token =jwt.sign({data:req.body},privateKey,{expiresIn:exptime});
          try{
            res.setHeader('AuthToken',token);
            return res.json({status:1,desc:'login success!'});
          }
          catch(e){
            return res.json({status:1,desc:'login success! setHeader function not defined!'});
          }
        };
      });
    };
  })(req, res, next);
};

exports.register = function(req, res, next) {
  if (checkAdmin(req)) {
    passport.authenticate('local-signup', function(err, user, info) {
      if (err) {
        console.log(err);
        return res.json(err);
      };
      return res.json('done');
    })(req, res, next);
  } else {
    res.json('You are no privilege to use this function.');
  };
};

exports.logout = function(req, res, next) {
  req.logout();
  res.json('logout');
};

exports.logincheckout = function(req, res, next) {
  if (req.isAuthenticated()) {
    if(req.body.userinfo === true) {
      res.json(req.user);
    } else {
      res.json({state:"login"});
    };
  } else{
    res.json('You are not authenticated, log in please.');
  };
};

exports.usersdata = function(req, res, next) {
  if (checkAdmin(req)) {
    User.find({'local.name':{$ne:'admin'}},'local',function(err,docs){
      if (err) {
        res.json(err.stack);
      } else {      
        var newdocs = [];        
        _.forEach(docs, function(doc){
          delete doc.local.password;
          if (doc.local.omsAuthority === undefined) {
            doc.local.omsAuthority = false;
          };
          newdocs.push(doc.local);
        });
        res.json(newdocs);
      };
    });
  } else {
    res.json([]);
  };
};

exports.updateuser = function(req, res, next) {
  if (checkAdmin(req) || req.user.local.name === req.body.name){
    if (req.body.updatedoc['local.curpassword'] !== undefined) {//check current password
      var curpassword = req.body.updatedoc['local.curpassword'];
      if (!bcrypt.compareSync(curpassword, req.user.local.password)) {
        return res.json({status:2,desc:'Current password error!'});
      } else {
        delete req.body.updatedoc['local.curpassword'];
      };
    };
    if(req.body.updatedoc['local.password'] !== undefined) {
      req.body.updatedoc['local.password'] = new User().generateHash(req.body.updatedoc['local.password']);
    };
    function update(){
      User.update({'local.name':req.body.name},{$set:req.body.updatedoc},function(err,docs){
        if (err) {
          res.json({status:2,desc:err});
        } else {
          res.json({status:1,desc:'update success!'});
        };
      });
    };
    if (req.body.updatedoc['local.name'] !== undefined) {
      User.findOne({'local.name':req.body.updatedoc['local.name']}, function(err, user) {
        if (err) {
          res.json({status:2,desc:err});
        };
        if (user) {
          res.json({status:2,desc:'This name is existed.'});
        } else {
          update();
        };
      });
    } else {
      update();
    };
  } else {
    res.json({status:3,desc:'You are no privilege to use this function.'});
  };
};

exports.updateuserSettings = function(req, res, next) {
  if (req.isAuthenticated()) {
    User.update({'local.name':req.body.name},{$set:req.body.updatedoc},function(err,docs){
      if (err) {
        res.json({status:2,desc:err});
      } else {
        res.json({status:1,desc:'update success!'});
      };
    });    
  } else {
    res.json({status:3,desc:'Please log in to use this function.'});
  };
};

exports.changeOmsAuthority = function(req, res, next) {
  if (checkAdmin(req)) {
    if (typeof(req.body.namelist) != typeof([])) {
      req.body.namelist = [req.body.namelist];
    };
    User.find({'local.name':{'$in':req.body.namelist}},{local: 1},function(err,docs){
      var queryList = [];
      _.forEach(docs, function(doc){
        queryList.push(
          User.update({'local.name': doc.local.name},
                      {'$set': { 'local.omsAuthority': !(doc.local.omsAuthority||false) }})
        );
      });
      Q.all(queryList)
        .then(function(result){       
          res.json({status:1, decs:'change success.'});
        })
        .then(null, function(err){
          console.log(err);
          res.json(err.stack);
        })
    });
  } else {
    res.json({status:3,desc:'You are no privilege to use this function.'});
  };
};

exports.deluser = function(req, res, next) {
  if (checkAdmin(req)) {
    if (typeof(req.body.namelist) !== typeof([])) {
      req.body.namelist = [req.body.namelist];
    };
    User.remove({'local.name':{'$in':req.body.namelist}},function(err,docs){
      if (err) {
        res.json(err.stack);
      } else {
        res.json('delete success!');
      };
    });
  } else {
    res.json({status:3,desc:'You are no privilege to use this function.'});
  };
};

exports.updateUserProfile = function(req, res, next) {
  if (req.isAuthenticated()) {
    var username = req.user.local.name;
    var setting = req.body.SETTING; // TABLE or CHART
    // Check document of user's profile is exist or not
    UserProfile.findOne({USERNAME:username}, function(err, doc) {
      if (err) {
        res.json({status:2,desc:err});
      };
      if (doc === undefined) { // insert one      
        var insertdoc = {USERNAME:username};
        _.forEach(setting, function(key){
          insertdoc[key] = setting[key]; 
        });       
        UserProfile.create(insertdoc,function(err,docs){
          if (err) {
            res.json({status:2,desc:err});
          } else {
            res.json({status:1,desc:'update success!'});
          };
        });
      } else { // update
        var updatedoc = {};
        _.forEach(setting, function(key){
          _.forEach(setting[key], function(key2){
            updatedoc[key+"."+key2] = setting[key][key2];
          });
        });
            
        UserProfile.update({USERNAME:username},{$set:updatedoc},function(err,docs){
          if (err) {
            res.json({status:2,desc:err});
          } else {
            res.json({status:1,desc:'update success!'});
          };
        });
      }
    });
  } else {
    res.json({status:3,desc:'Please log in to use this function.'});
  };
}

exports.loadUserProfile = function(req, res, next) {
  if (req.isAuthenticated()) {
    var username = req.user.local.name;//"admin";
    var type = req.body.TYPE
    console.log(username,type);
    UserProfile.findOne({USERNAME:username}, function(err, doc) {
      if (err) {
        res.json({status:2,desc:err});
      };
      if (doc === undefined) {
        res.json([]);
      } else { //update
        if (type === 'TABLE') {
          res.json({status:1,setting:doc['TABLE']});
        } else if (type=='CHART') {
          res.json({status:1,setting:doc['CHART']});
        } else {
          res.json({status:2,desc:'Error type format!'});
        };
      };
    });
  } else {
    res.json({status:3,desc:'Please log in to use this function.'});
  };
};

exports.removeUserProfile = function(req, res, next) {
  if (req.isAuthenticated()) {
    var username = req.user.local.name;//"admin";
    var type = req.body.TYPE
    var rmprofile = req.body.PROFILENAME;
    
    UserProfile.findOne({USERNAME:username}, function(err, doc) {
      if (err) {
        res.json({status:2,desc:err});
      };
      
      if (doc === undefined) {
        res.json({status:1,desc:'Profile not exist.'});
      } else { //update
        if (type === 'CHART') { // Currently only for CHART type
          var chart_profile = doc['CHART'];
          delete chart_profile[rmprofile];
          UserProfile.update({USERNAME:username},{$set:{CHART:chart_profile}},function(err,docs){
            if (err) {
              res.json({status:2,desc:err});
            } else {
              res.json({status:1,desc:'remove success!'});
            };
          });
        } else {
          res.json({status:2,desc:'Error type format!'});
        };
      };
    });
  } else {
    res.json({status:3,desc:'Please log in to use this function.'});
  };
};

exports.getVersion = function(req, res, next) {
  var versionid=process.env.versionid;
  var commitid=process.env.commitid;
  fs.readFile("version.json", "utf-8", function(err, data) {
    data = JSON.parse(data);
    res.json("version: "+data['versionid']+", "+data['commitid']);
  });
}

var checkAdmin = function(req){
  var isadmin = false;
  if (req.user !== undefined) {
    if (req.user.local.name=='admin') {
      isadmin = true;
    };
  };
  return isadmin;
};

/**
  * If administrator user not exist then create one
**/
User.findOne({'local.name' : 'admin'}, function(err, user) {
  if (err) {
    console.log(err);
  };
  if (user) {
    console.log('administrator was created.');
  } else {
    var newUser = new User();
    var adminuser = {local:{ name:'admin', password:newUser.generateHash('admin'),email:'admin@test.com',depid:'admin'},"__v":0,"SETTINGS":{}};
    User.insertMany([adminuser],function(err, docs){});
  };
});

exports.requestLogData = function(req, res, next) {
    if(checkAdmin(req)){
        var days = req.body.trange;
        var fromdate = new Date();
        var todate = new Date(fromdate-days*24*60*60*1000);
        var notRecordUrls = userAuthConfig.requestLog.notRecordUrls;
        reqLog.find({CONN_URL:{$nin:notRecordUrls},DATETIME:{$lt:fromdate,$gte:todate}})
        .then(function(r){
            res.json(r);
        })
        .then(null, function(err){ console.log('[loadUserProfile ERR]', err); res.status(500).json('[loadUserProfile ERR]');});
    }
    else
        res.json({status:3,desc:'You are no privilege to use this function.'});
};

/*
 * upload user data and register by it
*/
var backendRegister = function(req, res, next,callback) {
  passport.authenticate('local-signup', function(err, user, info) {    
    if(callback)
      callback(err);
  })(req, res, next);
};

/*
 * User import
*/
var basicHeader=['id','fullname','departmentid','e-mail','password'];
exports.bulkRegister = function(req, res, next) {
  var filename = 'register_'+String(new Date().getTime())+'.csv';
  var upload = uploader.definedUpload('./uploads/',filename); // args: filepath, filename
  var isUpdate = req.body.update?true:false;
  upload(req,res,function(err){
    if(err){
      console.log(err);
      res.status(400).json({errorLog:[err]});
    }
    else{
      var importedCount=0;
      var errorCount=0;
      var totalCount=0;
      var errorList = [];
      var importList = [];
      var headerIdxMap = {};
      var filepath = './uploads/'+filename;
      fs.readFile(filepath, 'utf8', function (err,data) {
        var lines = data.replace(new RegExp('\r','g'),'').split('\n');
        
        for(var idx in lines){
          if(idx==0){ // processing header
            var header = lines[idx].toLowerCase().split(',');
            header[0] = header[0].replace('\ufeff',''); // remove bom code
            if(header.length<2)
              return res.status(400).json({errorLog:['header error.']});
            for(var hidx in header){
              if(!basicHeader.includes(header[hidx])) // this header is not defined
                return res.status(400).json({errorLog:['Invaild column name: '+header[hidx]]});
              headerIdxMap[header[hidx]] = hidx;
            }
            if(headerIdxMap['id']==undefined)
              return res.status(400).json({errorLog:['not defined id in header.']});
            else if(headerIdxMap['password']==undefined && !isUpdate) // is update then not check
              return res.status(400).json({errorLog:['not defined password in header.']});
            continue;
          }
          // processing user information rows
          totalCount+=1;
          var line = lines[idx];
          idx = Number(idx);
          var linesplit = line.split(',');
          if(linesplit.length!=_.size(headerIdxMap)){
            errorCount+=1;
            errorList.push('line '+(idx+1)+': column number not matched with header column number');
            continue;
          }
          else if(linesplit[headerIdxMap['id']].length<1){// no id
            errorCount+=1;
            errorList.push('line '+(idx+1)+': No id.');
            continue;
          }
          else if(!isUpdate){
            if(linesplit[headerIdxMap['password']].length<1){// no password
              errorCount+=1;
              errorList.push('line '+(idx+1)+': No password.');
              continue;
            }
          }
          importList.push([idx,linesplit]);
        }
        if(importList.length<=0){
          return res.status(400).json({errorLog:errorList});
        }
        // Start Register
        if(!isUpdate){
          importList.forEach(function(pvalue){
            var rowcount = pvalue[0];
            var linesplit = pvalue[1];
            var newreq = {body:{}};
            newreq.body.name = linesplit[headerIdxMap['id']];
            newreq.body.password = linesplit[headerIdxMap['password']];
            // if non-necessary column undefined assign empty string
            newreq.body.fullname = headerIdxMap['fullname']?linesplit[headerIdxMap['fullname']]:'';
            newreq.body.email = headerIdxMap['e-mail']?linesplit[headerIdxMap['e-mail']]:'';
            newreq.body.depid = headerIdxMap['departmentid']?linesplit[headerIdxMap['departmentid']]:'';
            backendRegister(newreq, res, next,function(err,deferred){
              if(err){
                console.log(err);
                errorCount+=1;
                errorList.push('line '+(rowcount+1)+': '+err);
              }
              else
                importedCount+=1;
              if(errorCount+importedCount==totalCount){ // final count, return result
                return res.json({'importedCount':importedCount,'errorCount':errorCount,'errorLog':errorList});
              }
            });
          });
        }
        // Start Update
        else{
          importList.forEach(function(pvalue){
            var rowcount = pvalue[0];
            var linesplit = pvalue[1];
            var name = linesplit[headerIdxMap['id']];
            var updatedoc = {};
            // update options
            var updateKeyMap = {'fullname':'fullname','e-mail':'email','departmentid':'depid'};
            for(var key in updateKeyMap){
              if(headerIdxMap[key])
                if(headerIdxMap[key].length>0)
                  //if(key=='password') //not handel password
                  //  updatedoc['local.'+updateKeyMap[key]]=new User().generateHash(linesplit[headerIdxMap['password']]);
                  //else
                  updatedoc['local.'+updateKeyMap[key]]=linesplit[headerIdxMap[key]];
            }
            User.update({'local.name':name},{$set:updatedoc},function(err,docs){
              if(err){
                errorCount+=1;
                errorList.push('line '+(rowcount+1)+': '+err);
              }
              else
                importedCount+=1;
              if(errorCount+importedCount==totalCount){ // final count, return result
                return res.json({'importedCount':importedCount,'errorCount':errorCount,'errorLog':errorList});
              }
            });
          });
        }
        
      });
    }
  });
};

var checkAuth = function(req,res,next,callback){
  // Not logged in but has authorized token, do auto login
  // decode token
  var decode=undefined;
  try{
    decode=jwt.verify(req.headers['user-authorization'],privateKey);
  }
  catch(e){
    console.error('token decode error!');
  }
  if(decode) { // has token
    // check expired or not
    if(decode.exp*1000>new Date().getTime()){ // not expired
      if(!req.isAuthenticated()){ // not expired and still in authentication
        // token is authorized, do auto login
        req.body.name = decode.data.name;
        req.body.password = decode.data.password;
        var selfRes = {json:function(loginRes){
          return callback();
        }};
        exports.login(req,selfRes,next);
      }
      else 
        return callback();
    }
    else // expired
      noAutentication(res);
  }
  else{
    return noAutentication(res);
  }
};

exports.checkAuth = function(req, res, next){
  checkAuth(req,res,next,function(){
  if (req.isAuthenticated()){
    saveRequestLog(req);
    return next();
  }
  else
    return noAutentication(res);
  });
}

exports.requestChecking = function(req,res,next){
  saveRequestLog(req);
  next();
};

exports.getUserRole = function(req,res,next){
  var userRole = _.cloneDeep(userAuthConfig.role);
  res.json(userRole);
};

var ignore_path = userAuthConfig.requestLog.ingnoreApis;
var ignore_substr = userAuthConfig.requestLog.ingonreSubString;
function saveRequestLog(req){
  var username = req.user==undefined?"undefined":req.user.local.name;
  var fullname = req.user==undefined?"undefined":req.user.local.fullname;
  var depid = req.user==undefined?"undefined":req.user.local.depid;
  var path = req.url;
  let isIgnore = false;
  if (ignore_path.indexOf(path) < 0 ) {
    ignore_substr.forEach(function(subStr){
      if(path.indexOf(subStr)>=0)
        isIgnore = true;
    });
    if(isIgnore)
      return;
    var newReqLogDoc = new reqLog({
      USER_NAME: username,
      REQ_PATH:path,
      CONN_URL:req.headers.host,
      HOST_NAME:os.hostname(),
      FULL_NAME:fullname,
      DEPARTMENT_ID:depid,
      DATETIME: new Date()
    });
    newReqLogDoc.save(function (err, data) {
    if (err) console.error(err);
    });
  }
};