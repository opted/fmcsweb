'use strict';
/**
 * Module dependencies.
 */
var  multer = require('multer');

/*
 * upload user data and register by it
*/
var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
    /*if(file.originalname.indexOf('.csv') > -1){
      console.log(file.originalname);
      cb(null, './uploads/csvUploads/');
    }*/
    cb(null, './uploads/');
  },
  filename: function (req, file, cb) {
    cb(null, req.user.local.name + '_' +file.originalname)
  }
});

var upload = multer({ //multer settings
  storage: storage
}).single('file');

exports.defaultUpload = function() {
 return upload;
};

exports.definedUpload = function(filepath,filename) {
 var newStorage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
      cb(null, filepath);
    },
    filename: function (req, file, cb) {
      cb(null, filename)
    }
  });

  var newUpload = multer({ //multer settings
    storage: newStorage
  }).single('file');
 return newUpload;
};