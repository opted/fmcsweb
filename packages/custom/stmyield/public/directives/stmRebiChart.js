
(function(){
	'use strict';
	function stmRebiChart(common,StmYield,_){
    var chartName = "stmrebi-chart";
		return {
			restrict: 'E',
			scope: {
				data: '='
			},
			template: 
      // amchart
      '<div id="'+chartName+'" style="width:100%;height: 300px; margin:20px;" ng-show="display"><div></div></div>'
      ,
			link: function (scope, element, attrs) {
        scope.display = false;
        /**
          Defined amchart options
        */
        var chartBallonFunction = common.amChartCustomizeBallonFunction();
        // bar1: #118AB2, bar2: #06D6A0, bar3: #073B4C, line1: #FFD166, line2: #EF476F
        var chartOptions = {
          "type": "serial",
          "addClassNames": true,
          "theme": "light",
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
                "useGraphSettings": true,
                "markerSize":12,
                "valueWidth":0,
                "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [
          {
            "id": "bar1",
            "title": "Qty",
            "stackType": "regular",
            "position": "left",
            "integersOnly": true
          }],
          "startDuration": 1,
          "graphs": [
          {
            "id":"rebiqty",
            "balloonFunction": chartBallonFunction.basic,
            "fillAlphas": 1,
            "title": "重測兩次以上數量",
            "type": "column",
            "valueField": "REBIQTY",
            "valueAxis": "bar1",
            "dashLengthField": "dashLengthColumn",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          }],
          "categoryField": "KEY",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":10
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
           "chartScrollbar": {
          },
          "export": {
            "enabled": true
          }
        };

				var chart = AmCharts.makeChart(chartName, chartOptions);
        StmYield.setZoomChart(chart);
        /**
          Whating the data link of outer controller
        */
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined)
            return;
          var chartData = scope.data;
          if(_.isEmpty(chartData)!=true){
            scope.display = true;
            // set am-chart
            chart.dataProvider = chartData;
            chart.validateData();
            chart.validateNow();
          }
          else
            scope.display = false;
				});
				chart.validateData();
				chart.validateNow();
			}//link
		};
	};

	angular.module('mean.stmyield').directive('stmRebiChart', stmRebiChart);
	stmRebiChart.$inject = ['common','StmYield','_'];
})();
'use strict';