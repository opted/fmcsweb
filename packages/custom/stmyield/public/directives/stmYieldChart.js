
(function(){
	'use strict';
	function stmYieldChart(StmYield,common,_){
    var chartAName = "stmyield-chartA";
    var chartBName = "stmyield-chartB";
		return {
			restrict: 'E',
			scope: {
				data: '=',
        opts: '=' // {loadingChart:bool,loadingRawData:bool}, could change this field from controller at directive
			},
			template: 
      '<div ng-show="display"><uib-tabset active="drillDownTabActive">\
        <uib-tab ng-repeat="tab in drillTablist" ng-click="changeDrillDownTab($index)" index="tab.id">\
        <uib-tab-heading>\
          {{tab.title}}<button type="button" class="close" aria-label="Close" style="font-size:medium" ng-click="closeTab($index)" ng-show="$index>0">\
          <span aria-hidden="true">&times;</span>\
          </button>\
        </uib-tab-heading>\
        </uib-tab>\
      </uib-tabset>'+
      // tab title
      '<h3>{{ curChartParams.tabtitle }}</h3>'+
      // grouping button
      // render all pill tabset and hide unneeded tabset to fix a bug
      '<div ng-repeat="tab in drillTablist"><uib-tabset class="rebi-groupby" type="pills" ng-show="selectedTabItemIndex==$index">\
      <uib-tab index="$index" ng-repeat="item in tab.chartparams.groupby.itemlist" heading="{{GroupByMapping[item]}}" select="changeGroup(item)">\
      </uib-tab>\
      </uib-tabset></div>'+
      // using hint
      '<div class="row">\
      <label class="text-primary control-label col-sm-12 vcenter">* 點擊一次 Drill down</label></div>'+
      // sorting select
      '<div class="row"><label class="control-label col-sm-1 vcenter">排序</label>'+
      '<select ng-show="showChartA" class="col-sm-1 vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglistA" ng-change="changeSorting(0)">\
      </select>'+
      '<select ng-show="showChartB" class="col-sm-1 vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglistB" ng-change="changeSorting(0)">\
      </select>'+
      '<div class="btn-group col-sm-2 vcenter">\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="0" ng-click="changeSorting(1)">降序</label>\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="1" ng-click="changeSorting(2)">升序</label>\
      </div>\
      </div>'+
      // Chart A
      // amchart A
      '<div ng-show="showChartA">'+
      '<div id="'+chartAName+'" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      // chart A grid
      '<div class="row"><div class="col-md-7"><div style="height:200px;padding-left:20px;" class="ag-blue" ag-grid="chartAGridOptions"></div></div>\
      <div class="col-md-5"><button class="btn btn-success" ng-click="exportAgGrid(\'chartdataA\',\'xls\')">下載Excel</button></div></div>'+
      '</div>'+
      // Chart B
      // amchart B
      '<div ng-show="showChartB">'+
      '<div id="'+chartBName+'" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      // chart B grid
      '<div class="row"><div class="col-md-7"><div style="height:200px;padding-left:20px;" class="ag-blue" ag-grid="chartBGridOptions"></div></div>\
      <div class="col-md-5"><button class="btn btn-success" ng-click="exportAgGrid(\'chartdataB\',\'xls\')">下載Excel</button></div></div>'+
      '</div>'+
      // drill path
      // raw data grid
      '<hr><h3>Raw Data</h3>'+
      '<div class="row" style="padding-left:20px;"><div style="height:350px;" class="ag-blue" ag-grid="rawDataGridOptions"></div>\
      <button class="btn btn-success" ng-click="exportAgGrid(\'rawdata\',\'xls\')">下載Excel</button></div>'+
      '</div>'
      , // chart
			link: function (scope, element, attrs) {
        scope.display = false;
        /**
          Chart parameters, use to drilldown, sorting, limit or change group by
        */
        /*
          Chart params structure
          {
          level:,//drill down 等級，初始等級為0
          groupby:{itemlist:["LOTNO","ROOTLOT",...],selected:"LOTNO",active:0}, // 分群用項目
          sorting:{selected:'X軸字典排序',type:0},//可用的排序規則記在外部
          limit:{selected:'X軸字典排序',type:0}, //圖表呈現上限
          drillpath:[],
          data:[],
          showdata:[]
          }
        */
        scope.curChartParams = undefined;
        scope.GroupByMapping = StmYield.groupByMapping();
        scope.changeGroup = function(item){
          if(scope.inDrillDown)
            scope.inDrillDown = false;
          else{
            setCurChart(item);
            StmYield.chartChangeGroup(scope,scope.curChart,item);
            scope.chartGridOptions.api.setRowData(scope.curChartParams.allshowdata);
          }
        };
        // limit selected
        scope.limitlist = [5,10,20,"all"];
        scope.changeLimit = function(){
          StmYield.chartChangeLimit(scope,scope.curChart);
        };
        // sorting selected
        scope.sortinglistA = ['X軸字典排序','burnin1 yield','burnin2 yield','burnin1 qty','burnin2 qty'];
        scope.sortinglistB = ['X軸字典排序','yield','passqty','testqty'];
        scope.changeSorting = function(changeItem){
          StmYield.chartChangeSorting(scope,scope.curChart,changeItem);
        };
        /**
          Drill down control
        */
        scope.drillDownTriggerChangeGroup=false;
        scope.drillTablist = [];
        scope.inDrillDown = false;
        scope.drillDown = function(graphItemCategory){
          scope.inDrillDown = true;
          StmYield.chartDrillDown(scope,scope.curChart,graphItemCategory);
          // drill down 產生新 Tab 時會自動觸發 changeGroup，這會影響 chartA & chartB 顯示的運作
          // 使用變數 scope.inDrillDown 確認是否正在 drill down
        };
        scope.triggerCloseTab = false;
        scope.selectedTabItemIndex = 0;
        scope.changeDrillDownTab = function(index){
          setCurChart(scope.drillTablist[index].chartparams.groupby.selected);
          StmYield.chartChangeDrillDownTab(scope,scope.curChart,index);
          scope.changeGroup(scope.curChartParams.groupby.selected);
        };
        scope.closeTab = function(tabindex){
          StmYield.chartCloseTab(scope,tabindex);
        };
        /**
          Defined amchart options
        */
        var chartGrouping = {'family':'A','date':'A','productno':'A','equipment':'B','rootlotno':'B','lotno':'B'};
        var setCurChart = function(selected){
          if(selected==undefined)
            selected = scope.curChartParams.groupby.selected;
          if(chartGrouping[selected]=='A'){
            scope.showChartA = true;
            scope.showChartB = false;
            scope.curChart = chartA;
            scope.chartGridOptions = scope.chartAGridOptions;
          }
          else if((chartGrouping[selected]=='B')){
            scope.showChartA = false;
            scope.showChartB = true;
            scope.curChart = chartB;
            scope.chartGridOptions = scope.chartBGridOptions;
          }
        };
        var chartBallonFunction = common.amChartCustomizeBallonFunction();
        // ChartA for 產品群組、推移圖、產品型號
        var chartAOptions = {
          "type": "serial",
          "addClassNames": true,
          "theme": "light",
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
                "useGraphSettings": true,
                "markerSize":12,
                "valueWidth":0,
                "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [{
            "id": "line1",
            "title": "Yield",
            "position": "right",
            "maximum":120,
            "labelFunction": function(value) {
              return value+"%";
            }
          },
          {
            "id": "bar1",
            "title": "Qty",
            "stackType": "regular",
            "position": "left",
            "integersOnly": true
          }],
          "startDuration": 1,
          "graphs": [
          {
            "id":"bi1qty",
            "balloonFunction": chartBallonFunction.basic,
            "fillAlphas": 1,
            "title": "burnin1 qty",
            "type": "column",
            "valueField": "BI1TESTQTY",
            "valueAxis": "bar1",
            "dashLengthField": "dashLengthColumn",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },
          {
            "id":"bi2qty",
            "balloonFunction": chartBallonFunction.basic,
            "fillAlphas": 1,
            "title": "burnin2 qty",
            "type": "column",
            "valueField": "BI2TESTQTY",
            "valueAxis": "bar1",
            "dashLengthField": "dashLengthColumn",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },
          {
            "id": "burnin1 yield",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "bi1 yield",
            "valueField": "BI1YIELD",
            "valueAxis": "line1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "labelPosition": "left",
            "showAllValueLabels":true
          },
          {
            "id": "burnin2 yield",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "bi2 yield",
            "valueField": "BI2YIELD",
            "valueAxis": "line1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "labelPosition": "left",
            "showAllValueLabels":true
           }],
          "categoryField": "KEY",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":10
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
          "chartScrollbar":{
          },
          "export": {
            "enabled": true
          }
        };
        // ChartB for 爐號分析、母批分析、內批
        var chartBOptinos = _.cloneDeep(chartAOptions);
        chartBOptinos.graphs = [{
            "id":"sumtestqty",
            "balloonFunction": chartBallonFunction.basic,
            "fillAlphas": 1,
            "title": "加總 - testqty",
            "type": "column",
            "valueField": "TESTQTY",
            "valueAxis": "bar1",
            "dashLengthField": "dashLengthColumn",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },
          {
            "id":"sumpassqty",
            "balloonFunction": chartBallonFunction.basic,
            "fillAlphas": 1,
            "title": "加總 - passqty",
            "type": "column",
            "valueField": "PASSQTY",
            "valueAxis": "bar1",
            "dashLengthField": "dashLengthColumn",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },
          {
            "id": "avgyield",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "平均值 - yield",
            "valueField": "YIELD",
            "valueAxis": "line1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "labelPosition": "left",
            "showAllValueLabels":true
          }];
				var chartA = AmCharts.makeChart(chartAName, chartAOptions);
        StmYield.setZoomChart(chartA);
        var chartB = AmCharts.makeChart(chartBName, chartBOptinos);
        StmYield.setZoomChart(chartB);
        /**
          Add double click handler for amchart listener
        */
        var setClickListener = function(chart){
          chart.clickTimeout = 0;
          chart.lastClick = 0;
          chart.doubleClickDuration = 300;
          function clickHandler(event) {
            var clickItem = scope.GroupByMapping[scope.curChartParams.groupby.selected]+': '+event.item.category;
            var ts = (new Date()).getTime();
            // [[ Double click ]], get raw data, temporarily remove
            if ((ts - chart.lastClick) < chart.doubleClickDuration) {
              /*if (chart.clickTimeout) {
                clearTimeout(chart.clickTimeout);
              }
              chart.lastClick = 0; // reset last click
              var ok = true;//confirm('Get raw data from '+clickItem);
              if(ok){
                StmYield.chartGetRawData(scope,event.item.category);
              }*/
            }
            // [[ Single click ]], drill down
            else {
              chart.clickTimeout = setTimeout(function(){
                // trigger drill down
                var ok = true;//confirm('Drill down from '+clickItem);
                if(ok){
                  scope.drillDownTriggerChangeGroup = true;
                  scope.drillDown(event.item.category);
                }
              }, chart.doubleClickDuration);
            }
            chart.lastClick = ts;
          };
          chart.addListener("clickGraphItem", clickHandler);
        };
        setClickListener(chartA);
        setClickListener(chartB);
        
        /**
          Whating the data link of outer controller
        */
        // current chart initial
        scope.showChartA = true;
        scope.showChartB = false;
        scope.curChart = chartA;
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined)
            return;
          var chartData = scope.data.data;
          var rawdata = scope.data.rawdata;
          var queryParams = scope.data.params; // {match:{},datetype:'',searchtype:'',ddfilter:{}}
          if(_.isEmpty(chartData)!=true){
            scope.display = true;
            // set am-chart
            scope.curChartParams = StmYield.initChartParams(chartData,rawdata,queryParams);
            scope.drillTablist = [];
            scope.drillTablist.push(StmYield.getInitDrillTab(scope.curChartParams));
            setCurChart();
            scope.curChart.dataProvider = scope.curChartParams.showdata;
            scope.curChart.validateData();
            scope.curChart.validateNow();
            // set chart ag-grid
            scope.chartGridOptions.api.setRowData(scope.curChartParams.allshowdata);
            // set rawdata ag-grid
            scope.rawDataGridOptions.api.setRowData(rawdata);
            setTimeout(function(){
              scope.drillDownTabActive = scope.drillTablist[0].id;
              scope.$apply();
            },100);
          }
          else
            scope.display = false;
				});
				scope.curChart.validateData();
				scope.curChart.validateNow();
			},//link
    controller:['$scope',function($scope){
      /**
        Chart Data Ag-Grid
      */
      var chartAGridColumnDefs = [
        {headerName: 'Key', field: 'KEY', filter: 'text', width: 100},
        {headerName: 'BURNIN1 yield', field: 'BI1YIELD', filter: 'text', width: 120,cellRenderer:common.agGridcellRendererAddPercent},
        {headerName: 'BURNIN2 yield', field: 'BI2YIELD', filter: 'text', width: 120,cellRenderer:common.agGridcellRendererAddPercent},
        {headerName: 'BURNIN1 Qty', field: 'BI1TESTQTY', filter: 'text', width: 120},
        {headerName: 'BURNIN2 Qty', field: 'BI2TESTQTY', filter: 'text', width: 120}
      ];
      $scope.chartAGridOptions = {
        columnDefs:chartAGridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true
      };
      var chartBGridColumnDefs = [
        {headerName: 'Key', field: 'KEY', filter: 'text', width: 100},
        {headerName: '平均 - yield', field: 'YIELD', filter: 'text', width: 120,cellRenderer:common.agGridcellRendererAddPercent},
        {headerName: '加總 - passqty', field: 'PASSQTY', filter: 'text', width: 120},
        {headerName: '加總 - testqty', field: 'TESTQTY', filter: 'text', width: 120}
      ];
      $scope.chartBGridOptions = {
        columnDefs:chartBGridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.exportAgGrid = function(gridType,fileType){
        if(gridType=='chartdataA')
          common.exportAgGrid('chart_data',fileType,$scope.chartAGridOptions);
        else if(gridType=='chartdataB')
          common.exportAgGrid('chart_data',fileType,$scope.chartBGridOptions);
        else if(gridType=='rawdata')
          common.exportAgGrid('raw_data',fileType,$scope.rawDataGridOptions);
      };
      /**
        Raw Data Ag-Grid
      */
      var rawDataGridColumnDefs = [
        {headerName: 'ProductFamily', field: 'FAMILY', filter: 'text', width: 120},
        {headerName: 'OpNo', field: 'OPNO', filter: 'text', width: 100},
        {headerName: 'LotNo', field: 'LOTNO', filter: 'text', width: 120},
        {headerName: 'ProductNo', field: 'PRODUCTNOLOG', filter: 'text', width: 120},
        {headerName: 'DeviceNo', field: 'DEVICENOLOG', filter: 'text', width: 120},
        {headerName: 'Tester', field: 'EQUIPMENTNO', filter: 'text', width: 100},
        {headerName: 'CheckInTime', field: 'CHECKINTIME', filter: 'text', width: 120},
        {headerName: 'CheckOutTime', field: 'CHECKOUTTIME', filter: 'text', width: 120},
        {headerName: 'DieQty', field: 'DIEQTY', filter: 'text', width: 100},
        {headerName: 'TestQty', field: 'TESTQTY', filter: 'text', width: 100},
        {headerName: 'PassQty', field: 'PASSQTY', filter: 'text', width: 100}
      ];
      $scope.rawDataGridOptions = {
        columnDefs:rawDataGridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
    }]//controller
		};
	};

	angular.module('mean.stmyield').directive('stmYieldChart', stmYieldChart);
	stmYieldChart.$inject = ['StmYield','common','_'];
})();
'use strict';
