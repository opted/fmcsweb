(function() {
'use strict';
    function StmYield($http,$q,_,Rebi,StmChartData) {
      var groupByNameMapping = {'family':'產品群組','productno':'產品型號','date':'推移圖','equipment':'爐號','rootlotno':'母批','lotno':'內批'};
      var groupByList = _.keys(groupByNameMapping);
      _.remove(groupByList,function(o){return o=='lotno'});// drill down by 母批才呈現 lotno
      
      var getShowData = function(chartParams){
        var chartdata = chartParams.chartdata;
        var selected = chartParams.groupby.selected;
        var limitselected = chartParams.limit.selected;
        
        chartParams.allshowdata = chartdata[selected];
        chartParams.allshowdata = Rebi.sortingAllShowData(chartParams);
        var newChartData = [];
        // handle limit item data
        if(limitselected=='all' || limitselected>=chartParams.allshowdata.length)
          newChartData = chartParams.allshowdata;
        else
          for(var i=0;i<limitselected;i++)
            newChartData.push(chartParams.allshowdata[i]);
        return newChartData;
      };
      var getChartParams = function(level,chartdata,rawdata,groupbylist,params){
        // Iinitial status was defined as following
        var extraParams = {
         chartdata:chartdata,
         searchparams:params // for query of drill down and get rawdata
        };
        var chartParams = Rebi.initBasicChartParams(level,rawdata,groupbylist,undefined,extraParams);
        chartParams.sorting.field='KEY';
        chartParams.showdata = getShowData(chartParams);
        chartParams.tmpgroupresult[chartParams.groupby.selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
        return chartParams;
      };
      var setLoadingChart = function(scope){
        scope.opts.loadingChart = true;
      };
      var clearLoadingChart = function(scope){
        scope.opts.loadingChart = false;
      };
      var stmYieldService = Object.create(Rebi); // StmYield Service inheritance from Rebi Service
      // Override Following Function
      stmYieldService.groupByMapping = function(){
        return groupByNameMapping;
      };
      var setDataPath = function(scope){
        var drillpath = '';
        scope.curChartParams.drillpath.forEach(function(o){
          for(var dpkey in o){
            if(drillpath.length>0)
              drillpath+=' -> ';
            drillpath+=scope.GroupByMapping[dpkey]+':'+o[dpkey];
          }
        });
        scope.dataPath = drillpath;
      };
      stmYieldService.getShowData = getShowData;
      stmYieldService.getChartParams = getChartParams;
      stmYieldService.initChartParams = function(chartdata,rawdata,params){
        return this.getChartParams(0,chartdata,rawdata,groupByList,params);
      };
      stmYieldService.queryRawData = function(chartParams,category,scope){
        var deferred = $q.defer();
        var promise = deferred.promise;
        var params = _.cloneDeep(chartParams.searchparams);
        if(params.ddfilter)
          params.ddfilter = _.assign(params.ddfilter,category);
        else
          params.ddfilter = category;
        params.searchtype = 'rawdata';
        scope.opts.loadingRawData = true;
        $http.post('/api/stmyield/searchdata',params).then(
        function(response){
          scope.opts.loadingRawData = false;
          var key = _.keys(category)[0];
          deferred.resolve(response.data);
        }, function(err){
          console.log(err);
          scope.opts.loadingRawData = false;
          deferred.resolve([]);
        });
        return promise;
      };
      stmYieldService.queryRawDataSingle = function(chartParams,category,scope){
        var params = _.cloneDeep(chartParams.searchparams);
        if(params.ddfilter)
          params.ddfilter = _.assign(params.ddfilter,category);
        else
          params.ddfilter = category;
        params.searchtype = 'rawdata';
        
        var rawdata = StmChartData.getRawData(chartParams.data,params);
        var key = _.keys(category)[0];
        return rawdata;
      };
      stmYieldService.getDrillDownParams = function(chartParams,category){
        // 1. set promise
        var deferred = $q.defer();
        var promise = deferred.promise;
        // 2. set paramse
        var key = _.keys(category)[0];
        var params = _.cloneDeep(chartParams.searchparams);
        if(params.ddfilter)
          params.ddfilter = _.assign(params.ddfilter,category);
        else
          params.ddfilter = category;
        params.searchtype = 'chartdata';
        // 4. get new data
        //$http.post('/api/stmyield/test',params).then(
        $http.post('/api/stmyield/searchdata',params).then(
        function(response){
          var chartdata = response.data.data;
          // 5. get new groupbylist
          var groupbylist = _.filter(chartParams.groupby.itemlist,function(o){return o!=key});
          if(category.rootlotno) // is rootlotno perpare to show lotno chart
            if(_.indexOf(groupbylist,'lotno')<0)
              groupbylist.push('lotno');
          var ddChartParams = getChartParams(chartParams.level+1,chartdata,chartParams.data,groupbylist,params);
          // 6. set drillpath
          ddChartParams.drillpath = _.cloneDeep(chartParams.drillpath);
          ddChartParams.drillpath.push(category);
          deferred.resolve(ddChartParams);
        },function(err){deferred.reject(err)});
        return promise;
      };
      stmYieldService.getDrillDownParamsSingle = function(chartParams,category){
        // 1. set params
        var key = _.keys(category)[0];
        var params = _.cloneDeep(chartParams.searchparams);
        if(params.ddfilter)
          params.ddfilter = _.assign(params.ddfilter,category);
        else
          params.ddfilter = category;
        // 2. get new data
        var chartdata = StmChartData.getChartData(chartParams.data,params);
        // 3. get new groupbylist
        var groupbylist = _.filter(chartParams.groupby.itemlist,function(o){return o!=key});
        if(category.rootlotno) // is rootlotno perpare to show lotno chart
          if(_.indexOf(groupbylist,'lotno')<0)
            groupbylist.push('lotno');
        var ddChartParams = getChartParams(chartParams.level+1,chartdata,chartParams.data,groupbylist,params);
        // 4. set drillpath
        ddChartParams.drillpath = _.cloneDeep(chartParams.drillpath);
        ddChartParams.drillpath.push(category);
        return ddChartParams;
      };
      stmYieldService.changeGroup = function(chartParams,selected){
        chartParams.groupby.selected = selected;
        if(chartParams.tmpgroupresult[selected]){ //defined
          var tmpresult = chartParams.tmpgroupresult[selected];
          chartParams.showdata = tmpresult.showdata;
          chartParams.allshowdata = tmpresult.allshowdata;
          chartParams.sorting = tmpresult.sorting;
          chartParams.limit = tmpresult.limit;
        }
        else{ // undefined
          chartParams.showdata = this.getShowData(chartParams);
          chartParams.sorting = _.cloneDeep(chartParams.sorting);
          chartParams.limit = _.cloneDeep(chartParams.limit);
          // 暫存 groupby 資料, 這部分資料在切換 groupby, limit, sorting 時要確認是否確實置換
          chartParams.tmpgroupresult[selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
        }
        return chartParams;
      };
      stmYieldService.chartChangeGroup = function(scope,chart,item){
        if(scope.drillDownTriggerChangeGroup){
          // drill down cilck will make new groupby tabset and trigger this function, 
          // do not trigger at this time.
          scope.drillDownTriggerChangeGroup = false;
          return;
        }
        scope.curChartParams = this.changeGroup(scope.curChartParams,item);
        chart.dataProvider = scope.curChartParams.showdata;
        chart.validateData();
        chart.validateNow();
      };
      stmYieldService.chartChangeSorting = function(scope,chart,changeItem){
        var chartParams = scope.curChartParams;
        var selected = chartParams.sorting.selected;
        if(selected.indexOf('X軸')>=0)
          chartParams.sorting.field = 'KEY';
        else if(selected.indexOf('burnin1 yield')>=0)
          chartParams.sorting.field = 'BI1YIELD';
        else if(selected.indexOf('burnin2 yield')>=0)
          chartParams.sorting.field = 'BI2YIELD';
        else if(selected.indexOf('burnin1 qty')>=0)
          chartParams.sorting.field = 'BI1TESTQTY';
        else if(selected.indexOf('burnin2 qty')>=0)
          chartParams.sorting.field = 'BI2TESTQTY';
        else if(selected.indexOf('yield')>=0)
          chartParams.sorting.field = 'YIELD';
        else if(selected.indexOf('passqty')>=0)
          chartParams.sorting.field = 'PASSQTY';
        else if(selected.indexOf('testqty')>=0)
          chartParams.sorting.field = 'TESTQTY';
        else
          chartParams.sorting.field = undefined;
        
        scope.curChartParams = Rebi.changeSorting(chartParams);
        chart.dataProvider = scope.curChartParams.showdata;
        chart.validateData();
        chart.validateNow();
      };
      stmYieldService.chartGetRawData = function(scope,graphItemCategory){
        // get new chart params
        var category = {};
        category[scope.curChartParams.groupby.selected] = graphItemCategory;
        //this.queryRawData(scope.curChartParams,category,scope).then(function(result){
          //scope.rawDataGridOptions.api.setRowData(result);
        //});
        var gridData = this.queryRawDataSingle(scope.curChartParams,category,scope);
        scope.rawDataGridOptions.api.setRowData(gridData);
        window.scrollTo(0,document.body.scrollHeight);
      };
      stmYieldService.chartDrillDown = function(scope,chart,graphItemCategory){
        if(scope.curChartParams.groupby.itemlist.length<=1){
          alert("Dose not has any grouping for drilling down.");
          return;
        }
        // get new chart params
        var category = {};
        category[scope.curChartParams.groupby.selected] = graphItemCategory;
        setLoadingChart(scope);
        var newChartParams = this.getDrillDownParamsSingle(scope.curChartParams,category);
        this.setDrillDownTab(scope,newChartParams);
        clearLoadingChart(scope);
        Rebi.chartDrillDownAutoChangeTab(scope);
      };
    return stmYieldService;
  }
  angular
      .module('mean.stmyield')
      .factory('StmYield', StmYield);
  StmYield.$inject = ['$http','$q','_','Rebi','StmChartData'];
})();
