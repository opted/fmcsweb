(function() {
'use strict';
  function StmChartData($http,$q,common,_) {
    var processSearchData = function(rawdata,ddfilter,datetype,groupResult){
      if(ddfilter.rootlotno) // if drill down by rootlotno then provide lotno data
        groupResult.lotno=[];
      // Filter raw data
      for(var key in ddfilter)
        delete groupResult[key];
      for(var key in ddfilter){
        if(key=='family')
          rawdata = _.filter(rawdata,{FAMILY:ddfilter[key]});
        else if(key=='equipment')
          rawdata = _.filter(rawdata,{EQUIPMENTNO:ddfilter[key]});
        else if(key=='productno')
          rawdata = _.filter(rawdata,{PRODUCTNOLOG:ddfilter[key]});
        else if(key=='rootlotno')
          rawdata = _.filter(rawdata,function(o){return _.startsWith(o.LOTNO,ddfilter[key])});
        else if(key=='lotno')
          rawdata = _.filter(rawdata,{LOTNO:ddfilter[key]});
        else if(key=='date'){
          if(datetype=='day')
            rawdata = _.filter(rawdata,function(o){
              return new Date(o.CHECKOUTTIME).toLocaleDateString()==ddfilter[key];
            });
          else if(datetype=='week'){
            var weekfilter=ddfilter[key].replace('W','-'); // ex: trun 1111W22 into 1111-22
            rawdata = _.filter(rawdata,function(o){
              var timestr = new Date(o.CHECKOUTTIME);
              timestr = timestr.getFullYear()+'-'+common.getWeek(timestr);
              return timestr==weekfilter;
            });
          }
          else if(datetype=='month')
            // fitlermonth = YYYYMM
            rawdata = _.filter(rawdata,function(o){
              var timestr = new Date(o.CHECKOUTTIME).toLocaleDateString();
              timestr = timestr.substring(0,timestr.lastIndexOf('/')).replace('/','');
              return timestr==ddfilter[key];
            });
        }
      }
      return rawdata;
    };// processSearchData 
    return {
      getChartData: function(rawdata,params){
        var datacount=0;
        var ddfilter = params.ddfilter;
        var datetype = params.datetype;
        var groupResult = {family:[],equipment:[],date:[],rootlotno:[],productno:[]};
        if(_.isEmpty(ddfilter))
          groupResult['dieqty']=[];
        if(ddfilter)
          rawdata = processSearchData(rawdata,ddfilter,datetype,groupResult);
        // 1. gen data of yield chart
        for(var key in groupResult){
          var group = {};
          // i. groupby different field
          if(key=='family')
            group = _.groupBy(rawdata,'FAMILY');
          else if(key=='equipment')
            group = _.groupBy(rawdata,'EQUIPMENTNO');
          else if(key=='productno')
            group = _.groupBy(rawdata,'PRODUCTNOLOG');
          else if(key=='rootlotno')
            group = _.groupBy(rawdata,function(o){return o.LOTNO.substring(0,7);});
          else if(key=='lotno')
            group = _.groupBy(rawdata,'LOTNO');
          else if(key=='date'){
            if(datetype=='day')
              group = _.groupBy(rawdata,function(o){
                return new Date(o.CHECKOUTTIME).toLocaleDateString();
              });
            else if(datetype=='week')
              group = _.groupBy(rawdata,function(o){
                var timestr = new Date(o.CHECKOUTTIME);
                timestr = timestr.getFullYear()+'W'+common.getWeek(timestr);
                return timestr;
              });
            else if(datetype=='month')
              group = _.groupBy(rawdata,function(o){
                var timestr = new Date(o.CHECKOUTTIME).toLocaleDateString();
                timestr = timestr.substring(0,timestr.lastIndexOf('/')).replace('/','');
                return timestr;
              });
          }
          // ii. cal yield and test qty
          // field: KEY, BI1YIELD, BI2YIELD, BI1TESTQTY, BI2TESTQTY, BI1PASSQTY, PASSQTY, TESTQTY, YIELD
          for(var gkey in group){
            // bi1 testqty
            var bi1tqty = _.sumBy(group[gkey],function(o){
              if(o.OPNO=='BURNIN1')
                return o.TESTQTY;
              else
                return 0;
            });
            // bi2 testqty
            var bi2tqty = _.sumBy(group[gkey],function(o){
              if(o.OPNO=='BURNIN2')
                return o.TESTQTY;
              else
                return 0;
            });
            // bi1 passqty
            var bi1pqty = _.sumBy(group[gkey],function(o){
              if(o.OPNO=='BURNIN1')
                return o.PASSQTY;
              else
                return 0;
            });
            // bi2 passqty
            var bi2pqty = _.sumBy(group[gkey],function(o){
              if(o.OPNO=='BURNIN2')
                return o.PASSQTY;
              else
                return 0;
            });
            var chartItem = {KEY:gkey,BI1YIELD:_.round((bi1pqty/bi1tqty)*100,2),BI2YIELD:_.round((bi2pqty/bi2tqty)*100,2),
              BI1TESTQTY:bi1tqty, BI2TESTQTY:bi2tqty, BI1PASSQTY:bi1pqty, PASSQTY:bi1pqty+bi2pqty, TESTQTY:bi1tqty+bi2tqty
            };
            chartItem['YIELD']=_.round((chartItem['PASSQTY']/chartItem['TESTQTY'])*100,2);
            groupResult[key].push(chartItem);
            datacount+=1;
            if(key=='family' && groupResult['dieqty'])
              groupResult['dieqty'].push({FAMILY:gkey,DIEQTY:bi1tqty});
          };
        }// for groupResult
        // iii. For 重測兩次分析圖
        // level 0, calculate reburn qty by family
        var sumDieQtyData = groupResult['dieqty'];
        if(sumDieQtyData){
          var dieQtyFamilyMap = {};
          sumDieQtyData.forEach(function(doc){
            dieQtyFamilyMap[doc.FAMILY] = doc.DIEQTY;
          });
          var familyData = groupResult['family'];
          var rebidata = [];
          for(var i in familyData){
            var o = familyData[i];
            var rebidoc = {};
            rebidoc.KEY = o.KEY;
            var DIEQTY = dieQtyFamilyMap[o.KEY];
            rebidoc.REBIQTY = o.BI1PASSQTY+o.BI2TESTQTY-DIEQTY;
            if(rebidoc.REBIQTY<0 || _.isNaN(rebidoc.REBIQTY))
              continue;
            else
              rebidata.push(rebidoc);
          }
          groupResult['rebidata']=rebidata;
        }
        return groupResult;
      },
      getRawData: function(rawdata,params){
        var ddfilter = params.ddfilter;
        var datetype = params.datetype;
        var groupResult = {family:[],equipment:[],date:[],rootlotno:[],productno:[]};
        return processSearchData(rawdata,ddfilter,datetype,groupResult);
      }
    };
  };
  angular
      .module('mean.stmyield')
      .factory('StmChartData', StmChartData);
  StmChartData.$inject = ['$http','$q','common','_'];
})();