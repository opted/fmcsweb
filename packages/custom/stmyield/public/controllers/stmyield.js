'use strict';
var openFile = undefined;
var daySeconds = 24*60*60*1000;
function StmYieldController($scope, Global, $stateParams, $window, $http, utilsSrchOpt, common, _, StmChartData) {
  /*
    search panel
  */
  $scope.selected = {datetype:'day'};
  $scope.mesfields = [
    utilsSrchOpt.genField("客戶別(CustomerName)", "CUSTOMERCHISNAME", "string", true, true),
    utilsSrchOpt.genField("客戶型號(DeviceNo)", "DEVICENO", "string", true, true),
    utilsSrchOpt.genField("產品型號(ProductNo)", "PRODUCTNO", "string", true, true),
    utilsSrchOpt.genField("封裝型號(PackageType)", "PACKAGETYPE", "string", true, true),
    utilsSrchOpt.genField("產品群組(ProductFamily)", "FAMILY", "string", true, true),
    utilsSrchOpt.genField("客批(CustomerLotno)", "CUSTOMERLOTNO", "string", true, true),
    utilsSrchOpt.genField("內批(LotNo)", "LOTNO", "string", true, true)
  ];
  $scope.mesfsURL = '/api/stmyield/getsearchfield';
  $scope.filterExtraMatch = {datetime:$scope.selected};
  $scope.resetTime = function(){
    var curtime = new Date();
    curtime.setHours(23,59,59,999);
    $scope.selected.from = new Date(common.getToday().getTime()-daySeconds);
    $scope.selected.to = curtime;
  };
  $scope.resetTime();
  /*
   query summary information
  */
  $scope.requestStatus = undefined;
  $scope.rowNum = undefined;
  $scope.ftypeText = undefined;
  /*
    Am-Chart
  */
  $scope.chartOpts = {loadingChart:false,loadingRawData:false};
  $scope.chartData = [];
  $scope.rebiQtyChartData = [];
  /*
    Query
  */
  $scope.stmyieldQuery = function(){
    $scope.downloadFileName=undefined;
    $scope.ftypeText = undefined;
    //clear previous status
    var params = {match:utilsSrchOpt.getMatch($scope.mesfields)};
    var noFilter = true;
    for(var key in params.match){
      if(key=='CUSTOMERCHISNAME'){
        params.match[key] = params.match[key].split(',')[0];
      }
      if(params.match[key]!=undefined){
        if(params.match[key].length>1)
          noFilter = false;
      }
    }
    if(noFilter)
      return alert('至少篩選一個欄位');
    params.match.DATETIME = {FROM:$scope.selected.from,TO:$scope.selected.to};
    params.datetype = $scope.selected.datetype;
    // check the $scope.rawdat has data or not
    var isSameParams = false;
    $scope.requestStatus = '等待 Server 回應...';
    $scope.waitingResponse = true;
    $scope.chartOpts.loadingChart = true;
    $scope.rowNum = undefined;
    var startTime = new Date();
    $scope.queryTime = "";
    
    var successFunction = function(rawdata){
      $scope.waitingResponse = false;
      $scope.chartOpts.loadingChart = false;
      $scope.requestStatus = '查詢完成';
      $scope.rowNum = rawdata.length;
      $scope.queryTime = new Date() - startTime;
    };
    var errorFunction = function(err){
      $scope.requestStatus = '查詢失敗, '+err.data;
      $scope.waitingResponse = false;
      $scope.chartOpts.loadingChart = false;
      console.log(err);
    };
    params.searchtype = 'rawdata';
    $http.post('/api/stmyield/searchdata',params).then(
    //$http.post('/api/stmyield/test',params).then(
    function(response){
      //console.log(response.data);
      var data = StmChartData.getChartData(response.data,params);//response.data.data;
      // 1. assign data for 重測分析圖
      $scope.rebiQtyChartData = data.rebidata;
      delete data.rebidata;
      // 2. assign data for yield analysis chart
      $scope.chartData = {data:data,params:params,rawdata:response.data};
      successFunction(response.data);
    }
    ,errorFunction);
  };
}

angular
    .module('mean.stmyield')
    .controller('StmYieldController', StmYieldController);

StmYieldController.$inject = ['$scope', 'Global', '$stateParams', '$window', '$http', 'utilsSrchOpt', 'common','_','StmChartData'];
