(function() {
    'use strict';

    function StmYield($stateProvider) {
        $stateProvider.state('stmyield main page', {
            url: '/stmyield',
            templateUrl: 'stmyield/views/index.html',
            controller:StmYieldController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              }
            }
        });
    }

    angular
        .module('mean.stmyield')
        .config(StmYield);

    StmYield.$inject = ['$stateProvider'];

})();
