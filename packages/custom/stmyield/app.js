'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var StmYield = new Module('stmyield');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
StmYield.register(function(app, auth, database, utils) {
  
  StmYield.aggregateAsset('css', '../css/stmyield.css');
  //We enable routing. By default the Package Object is passed to the routes
  StmYield.routes(app, auth, database, utils);
  StmYield.angularDependencies(['agGrid']);
  return StmYield;
});
