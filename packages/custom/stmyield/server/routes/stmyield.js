(function() {
  'use strict';
  /* jshint -W098 */
  var stmyieldCtrl = require('../controllers/stmyield');
  module.exports = function(StmYield, app, auth, database,utils) {
    stmyieldCtrl .initMesConnection(utils); // I can't figure out why class constructor not work, use this function to replace it
    app.post('/api/stmyield/searchdata', stmyieldCtrl.searchSTMYield);
    app.post('/api/stmyield/getsearchfield', stmyieldCtrl.getSearchField);
  };
})();
