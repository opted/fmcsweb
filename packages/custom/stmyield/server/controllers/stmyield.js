'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Q = require('q'),
    _ = require('lodash');
var oqtool = require('../../../../custom/rebi/server/controllers/odbQueryTool.js'),
  rebiCtrl = require('../../../../custom/rebi/server/controllers/rebi.js');
var mesconn = undefined;//new mesclass();
var chtime = function(sdate,tag){console.log(tag+(new Date().valueOf()-sdate)/1000)};
var oneHour = 60*60*1000;

exports.initMesConnection = function(utils){
  mesconn = utils.ctrls.mes;
};

exports.example = function(req, res, next) {
  var qcmd = "select * from HISLFT.TblWipLotData where lotno like :lotno";
  var params = {'lotno':'LH401W5AA1'};
  var qcmd_list = [];
  
  qcmd_list.push(mesconn.exec(qcmd,params));
  Q.all(qcmd_list).then(function(results){
    var outputResult = [];
    results.forEach(function(item){
      outputResult.push(item.rows);
    });
    res.json(outputResult);
  });
};

exports.searchSTMYield = function(req, res, next){
  req.setTimeout(3600000); // 60 min
  var match = req.body.match;
  var datetype = req.body.datetype;
  var searchtype = req.body.searchtype;
  var ddfilter = req.body.ddfilter; // drill down filter
  if(ddfilter==undefined)
    ddfilter = {};
  if(ddfilter.family)
    match.FAMILY = ddfilter.family;
  //match = {LOTNO:'LHA075EAC1'};
  // Example lotno:
  // standard: LHA06AUAC1, customize ls: LHA075EAC1, customize nols: LH80YYCAB1
  /**
   Example of match structure:
   {LOTNO:'LH60U1IAC1',CUSTOMERLOTNO:'PFBY68.M00',CUSTOMERCHISNAME:'nVIDIA',DATETIME:{FROM:,TO:}}
  */
  var missingParams=true;
  for(var key in match)
    if(key!='DATETIME'){
      missingParams=false;
      break;
    }
  if(missingParams)
    return res.status(400).json('Missing parameters for query!');
  
  var fparams = [];
  var lotdata_filter="";
  //test
  if(match.LOTNO)
    lotdata_filter="LOTNO='"+match.LOTNO+"'";
  else if(match.CUSTOMERLOTNO)
    lotdata_filter="CUSTOMERLOTNOLOG='"+match.CUSTOMERLOTNO+"'";
  else{
    lotdata_filter="CHECKOUTTIME between :fromDate and :toDate";
    var from = new Date(match.DATETIME.FROM);
    var to = new Date(match.DATETIME.TO);
    fparams={fromDate:from,toDate:to};
  }
  
  var where_filter="";
  for(var key in match){
    if(key=='DEVICENO' || key=='PRODUCTNO'){
      if(where_filter.length>0)
        where_filter+=' and ';
      where_filter+="A."+key+"LOG='"+match[key]+"'";
    }
    else{
      var tableNo='';
      if(key=='CUSTOMERCHISNAME' || key=='CUSTOMERNAME')
        tableNo='B.';
      else if(key=='FAMILY')
        tableNo='C.';
      else if(key=='PACKAGETYPE')
        tableNo='D.';
      
      if(tableNo.length>0){
        if(where_filter.length>0)
          where_filter+=' and ';
        where_filter+=tableNo+key+"='"+match[key]+"'";
      }
    }
  }
  if(where_filter.length>0)
    where_filter=' where '+where_filter;
  var fstqcmd = "select DISTINCT SUBSTR(A.LOTNO,1,7) as PLOTNO"+
  " from (select LOTNO,CUSTOMERLOTNOLOG,DEVICENOLOG,PRODUCTNOLOG from HISMFT.TBLWIPLOTLOG where "+lotdata_filter+") A";
  if(where_filter.length>0){
    var leftjoin = " left join HISMFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO "+
    "left join Masterdm.DIM_Customer B on D.CUSTOMERNO=B.CUSTNO "+
    "left join HISMFT.TBLENGPRODUCTDATA C on A.PRODUCTNOLOG=C.PRODUCTNO "+
    where_filter;
    fstqcmd+=leftjoin;
  }

  var sql_ddfilter = '';// filter of drill down function
  for(var key in ddfilter){
    if(key=='equipment')
      sql_ddfilter+=" and A.EQUIPMENTNO='"+ddfilter[key]+"'";
    else if(key=='productno')
      sql_ddfilter+=" and A.PRODUCTNOLOG='"+ddfilter[key]+"'";
    else if(key=='rootlotno')
      sql_ddfilter+=" and SUBSTR(A.LOTNO,1,7)='"+ddfilter[key]+"'";
    else if(key=='lotno')
      sql_ddfilter+=" and A.LOTNO='"+ddfilter[key]+"'";
    else if(key=='date'){
      if(datetype=='day')
        sql_ddfilter+=" and to_char(A.CHECKOUTTIME,'YYYY-MM-DD')='"+ddfilter[key]+"'";
      else if(datetype=='week'){
        ddfilter[key]=ddfilter[key].replace('W','-'); // ex: trun 1111W22 into 1111-22
        sql_ddfilter+=" and to_char(A.CHECKOUTTIME,'YYYY-WW')='"+ddfilter[key]+"'";
      }
      else if(datetype=='month')
        sql_ddfilter+=" and to_char(A.CHECKOUTTIME,'YYYYMM')='"+ddfilter[key]+"'";
    }
  }
  if(searchtype=='chartdata')
    queryChartData(res,fstqcmd,fparams,ddfilter,sql_ddfilter,datetype);
  else if(searchtype=='rawdata')
    queryRawData(res,fstqcmd,fparams,sql_ddfilter);
};

var queryChartData = function(res,fstqcmd,fparams,ddfilter,sql_ddfilter,datetype){
  var getTempQcmd = function(inkey){
    return "select KEY,"+
    "max(case when OPNO='BURNIN1' then YIELD*100 end) BI1YIELD,"+
    "max(case when OPNO='BURNIN2' then YIELD*100 end) BI2YIELD,"+
    "max(case when OPNO='BURNIN1' then TESTQTY end) BI1TESTQTY,"+
    "max(case when OPNO='BURNIN2' then TESTQTY end) BI2TESTQTY,"+
    "max(case when OPNO='BURNIN1' then PASSQTY end) BI1PASSQTY,"+
    "sum(PASSQTY) PASSQTY,"+
    "sum(TESTQTY) TESTQTY,"+
    "((sum(PASSQTY)/sum(TESTQTY))*100) YIELD "+
    //"max(case when OPNO='BURNIN2' then PASSQTY end) BI2PASSQTY "+
    "from (SELECT "+inkey+" as KEY,A.OPNO,(SUM(A.GOODQTY)/SUM(A.INPUTQTY)) as YIELD,"+
    "SUM(A.INPUTQTY) as TESTQTY,SUM(A.GOODQTY) as PASSQTY from "+
    "HISMFT.TBLWIPLOTLOG A left join HISMFT.TBLENGPRODUCTDATA B on A.PRODUCTNOLOG=B.PRODUCTNO "+
    "where RegExp_like(A.OPNO,'^BURNIN') and SUBSTR(A.LOTNO,1,7) in ("+fstqcmd+")"+sql_ddfilter+
    " group by "+inkey+",A.OPNO) group by KEY";
  }
  
  var groupqcmd = {family:"",equipment:"",date:"",rootlotno:"",productno:""};
  if(ddfilter.rootlotno) // if drill down by rootlotno then provide lotno data
    groupqcmd.lotno="";
  // remove filter field
  for(var key in ddfilter)
    delete groupqcmd[key];
  
  if(_.isEmpty(ddfilter)){
    var dieqty_qcmd = "select B.FAMILY,SUM(C.DIEQTY) as DIEQTY from "+
    "HISMFT.TBLWIPLOTLOG A left join HISMFT.TBLENGPRODUCTDATA B on A.PRODUCTNOLOG=B.PRODUCTNO "+
    "left join HISMFT.TBLWIPLOTDATA C on A.LOTNO=C.LOTNO "+
    "where A.OPNO='BURNIN1' and SUBSTR(A.LOTNO,1,7) in ("+fstqcmd+") group by B.FAMILY";
    groupqcmd['dieqty'] = dieqty_qcmd;
  }
  
  for(var key in groupqcmd){
    if(key=='family'){
      groupqcmd[key]=getTempQcmd('B.FAMILY');
    }
    else if(key=='date'){
      if(datetype=='day')
        groupqcmd[key]=getTempQcmd("to_char(CHECKOUTTIME,'YYYY-MM-DD')");
      else if(datetype=='week')
        groupqcmd[key]=getTempQcmd("to_char(A.CHECKOUTTIME,'YYYY-WW')");
      else if(datetype=='month')
        groupqcmd[key]=getTempQcmd("to_char(CHECKOUTTIME,'YYYYMM')");
    }
    else if(key=='rootlotno')
      groupqcmd[key]=getTempQcmd('SUBSTR(A.LOTNO,1,7)');
    else if(key=='lotno')
      groupqcmd[key]=getTempQcmd('A.LOTNO');
    else if(key=='equipment')
      groupqcmd[key]=getTempQcmd('A.EQUIPMENTNO');
    else if(key=='productno')
      groupqcmd[key]=getTempQcmd('A.PRODUCTNOLOG');
  }
  var qcmdlist = [];
  var keylist = _.keys(groupqcmd);
  for(var i in keylist){
    //console.log(keylist[i]+': ',groupqcmd[keylist[i]]);
    qcmdlist.push(mesconn.exec(groupqcmd[keylist[i]],fparams));
  }
  Q.all(qcmdlist).then(function(results){
    var groupData = {data:{},length:0};
    for(var i in keylist)
      groupData.data[keylist[i]] = results[i];
    
    // For 重測兩次分析圖
    var sumDieQtyData = groupData.data['dieqty'];
    delete groupData.data['dieqty'];
    
    for(var key1 in groupData.data){
      groupData.data[key1].forEach(function(doc){
        groupData.length+=1;
        if(datetype=='week' && key1=='date'){
          doc.KEY = doc.KEY.replace('-','W'); // turn year-week into yearWweek
        }
        for(var key2 in doc){
          if(key2.endsWith('YIELD'))
            if(_.isNumber(doc[key2]))
              doc[key2] = _.round(doc[key2],2);
        }
      });
    }
    // For 重測兩次分析圖
    // level 0, calculate reburn qty by family
    if(sumDieQtyData){
      var dieQtyFamilyMap = {};
      sumDieQtyData.forEach(function(doc){
        dieQtyFamilyMap[doc.FAMILY] = doc.DIEQTY;
      });
      var familyData = groupData.data['family'];
      var rebidata = [];
      for(var i in familyData){
        var o = familyData[i];
        var rebidoc = {};
        rebidoc.KEY = o.KEY;
        var DIEQTY = dieQtyFamilyMap[o.KEY];
        rebidoc.REBIQTY = o.BI1PASSQTY+o.BI2TESTQTY-DIEQTY;
        if(rebidoc.REBIQTY<0 || _.isNaN(rebidoc.REBIQTY))
          continue;
        else
          rebidata.push(rebidoc);
        //rebidata.push(rebidoc);
      }
      groupData.data['rebidata']=rebidata;
    }
    // key 'rebidata' for 重測兩次分析圖
    // others key for 良率分析圖，key 'lotno' 資料只有 drill down 母批(rootlotno)時會產生
    res.json(groupData);
  })
  .catch(function(err){
    console.log(err.stack);
    return res.status(400).json('summary query error. '+err);
  });
};

var queryRawData = function(res,fstqcmd,fparams,sql_ddfilter){
  var qcmd = "select B.FAMILY,C.DIEQTY,A.OPNO,A.LOTNO,A.INPUTQTY as TESTQTY,A.GOODQTY as PASSQTY,"+
    "A.DEVICENOLOG,A.EQUIPMENTNO,A.CHECKINTIME,A.CHECKOUTTIME,A.PRODUCTNOLOG from "+
    "HISMFT.TBLWIPLOTLOG A left join HISMFT.TBLENGPRODUCTDATA B on A.PRODUCTNOLOG=B.PRODUCTNO "+
    "left join HISMFT.TBLWIPLOTDATA C on A.LOTNO=C.LOTNO "+
    "where RegExp_like(A.OPNO,'^BURNIN') and SUBSTR(A.LOTNO,1,7) in ("+fstqcmd+")"+sql_ddfilter+
    " order by A.LOTNO";
  mesconn.exec(qcmd,fparams).then(function(results){
    // calculate dieqty
    var groupByPlotno = _.groupBy(results,function(o){return o.LOTNO.slice(0,7)});
    for(var key in groupByPlotno){
      var plotnoData = groupByPlotno[key];
      var dieqty = _.sumBy(_.filter(plotnoData,{OPNO:'BURNIN1'}),'DIEQTY');
      for(var i in plotnoData){
        var lotlog = plotnoData[i];
        lotlog['DIEQTY']=dieqty;
        lotlog.CHECKINTIME = new Date(lotlog.CHECKINTIME.getTime()+8*oneHour);
        lotlog.CHECKOUTTIME = new Date(lotlog.CHECKOUTTIME.getTime()+8*oneHour);
        lotlog.CHECKINTIME = lotlog.CHECKINTIME.toISOString().slice(0,19);
        lotlog.CHECKOUTTIME = lotlog.CHECKOUTTIME.toISOString().slice(0,19);
      }
    }
    res.json(results);
  })
  .catch(function(err){
    return res.status(400).json('summary query error. '+err);
  });
};

exports.getSearchField = function(req, res, next) {
  var field = req.body.field,
    match = req.body.match; //{LOTNO:,CUSTOMERCHISNAME:}
  if(field=='CUSTOMERCHISNAME'){
    req.body.fstr = match[field];
    req.body.field = 'customername';
    rebiCtrl.getMesMultiSelection(req,res, next);
  }
  else{
    if(match['CUSTOMERCHISNAME']){
      req.body.match['CUSTOMERCHISNAME'] = match['CUSTOMERCHISNAME'].split(',')[0];
    }
    rebiCtrl.mesGetSearchField(req,res, next);
  }
};