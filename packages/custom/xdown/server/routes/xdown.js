(function() {
    'use strict';

    /* jshint -W098 */
    // The Package is past automatically as first parameter
    var schemaList = ['HISMFT','HISLFT'];
    var Q = require('q');
    module.exports = function(Xdown, app, auth, database,utils) {

        // var requiresAdmin = circles.controller.hasCircle('admin');
        // var requiresLogin = circles.controller.hasCircle('authenticated');


        var connectMssql = require('../../../oms/server/controllers/sqlQuery')
        var XdownManager = require('../controllers/xdownOracle');
        var xdownMssql = require('../controllers/xdownMssql');
        var xdownMgr = new XdownManager(utils);
        // var xdownMssqlMgr = new xdownMssql(utils);
        // var processCtrl= require('../controllers/processData');
        // var userauth = require('../../../../core/users/server/controllers/userauth.js');


        app.post('/api/xdown/getsearchfield', function(req, res, next){
            xdownMgr.getSearchField(req.body)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });

        app.post('/api/xdown/getsearchresult',function(req, res, next){
            req.setTimeout(180000);
            var queryList=[];
            schemaList.forEach(function(dbSchema){
                queryList.push(xdownMgr.getSearchResult(req.body,dbSchema))
            })
            queryList.push(xdownMgr.getOvenIdleInfo(req.body))
            queryList.push(xdownMgr.getOvenRunInfo(req.body))

            Q.all(queryList)
            .then(function(results) { 

                  res.json( {'rows':(results[0].concat(results[1]))
                                     .sort(function(a,b){ return a.CREATEDATE-b.CREATEDATE}),
                             'timeData':results[2],
                             'runLotData':results[3]}) 
              })
            .catch((err) => {
                console.log(err);
                res.status(500).send({'error': err})
            });
        }) ;

        app.post('/api/xdown/getoveninfo',function(req, res, next){
            req.setTimeout(180000);
            var queryList=[];
            queryList.push(xdownMgr.getOvenInfo(req.body,'HISMFT'))
            // queryList.push(xdownMgr.getExcecptionInfo(req.body,dbSchema)
            schemaList.forEach(function(dbSchema){
                queryList.push(xdownMgr.getExcecptionInfo(req.body,dbSchema))         
            })
            queryList.push(xdownMgr.getOvenIdleInfo(req.body))

            Q.all(queryList)
            .then(function(results) { 
                  res.json({'rows': results[0]
                                     .sort(function(a,b){ return a.CREATEDATE-b.CREATEDATE}),
                            'exceptionRows':(results[1].concat(results[2]))
                                     .sort(function(a,b){ return a.CREATEDATE-b.CREATEDATE}),
                            'timeData':results[3] }) 
              })
            .catch((err) => {
                console.log(err);
                res.status(500).send({'error': err})
            });
        }) ;

        app.post('/api/xdown/getdailydata',function(req, res, next){
            req.setTimeout(180000);
            // var queryList=[];
            // // schemaList.forEach(function(dbSchema){
            //     queryList.push(xdownMgr.getDailyData(req.body,dbSchema))
            // // })

            xdownMgr.getDailyData(req.body)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        }) ;

        
       app.post('/api/xdown/getovendowntime', function(req, res, next){
            xdownMgr.getOvenDownTime(req.body)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });
       app.post('/api/xdown/getovenidletime', function(req, res, next){
            xdownMgr.getOvenIdleTime(req.body)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });

        app.post('/api/xdown/getfailedinfo',function(req, res, next){
            req.setTimeout(180000); // 3 min
            // connectMssql.getErrmsg(req)
            xdownMssql.getSearchResult(req.body,utils)
             .then(function(mssqlData){
                    // console.log(mssqlData.length);
                    res.json(mssqlData)
                    })
                    .catch(function(err){
                        res.status(500).send({'error': err})
                    })
        })

    };
})();
