var Q = require('q');
 

var queryfunc = function(utils, qSTR, bindVars){
  var deferred = Q.defer();
  utils.ctrls.mbi.exec(qSTR, bindVars)
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function(err){
      console.log(err);
      deferred.reject(err);

    });
  return deferred.promise;
};


exports.getSearchResult = function(params, utils) {
    // var df = Q.defer();
    console.log(params)
    var query= `SELECT LotNum,Oven_ID,ErrorCode,MsgText,COUNT(*) AS ERRCNT
                FROM 
                   OvenErrorMsgALL 
                WHERE (Log_Time >= @fromDate and Log_Time < @toDate )
                AND ErrorCode_1 != '000'
                GROUP BY LotNum,Oven_ID,ErrorCode,MsgText`

    var fromDate = new Date(params.from);
    var toDate = new Date(params.to);
    fromDate.setHours(fromDate.getHours()-8);
    toDate.setHours(toDate.getHours()-8);
    var bindVars={'fromDate':fromDate,'toDate':toDate}

    return queryfunc(utils,query,bindVars)
}