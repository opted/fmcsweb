var _ = require('lodash');
var oracledb = require('oracledb');
// var dbConfig = require('../config/oracleConfig.js');
var Q = require('q');

function getDistinctField(field){
  switch (field){
    case 'CUSTOMERCHISNAME':
      return 'F.' + field;
      break;
    case 'LOTNO':
    case 'CUSTOMERLOTNO':
    case 'EQUIPMENTNO':
      return 'C.' + field;
      break;
    case 'DEVICENO':
      return 'D.' +field;
      break;
    case 'EQUIPMENTTYPE':
      return 'B.' +field;
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}


function getTableName(field){
  switch (field){
    case 'CUSTOMERCHISNAME':
      return ' FULL JOIN Masterdm.Dim_Customer F ON D.CustomerNo = F.CustNo';
      break;
    case 'LOTNO':
    case 'CUSTOMERLOTNO':
    case 'EQUIPMENTNO':
      return '' ;
      break;
    case 'DEVICENO':
      return '';
      break;
    case 'EQUIPMENTTYPE':
      return ' FULL JOIN HISMFT.tblMstEquipmentType B ON A.EquipmentType = B.EquipmentType ';
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}

function getSingleTableName(field){
  switch (field){
    case 'CUSTOMERCHISNAME':
      return ' Masterdm.Dim_Customer F ';
      break;
    case 'LOTNO':
      return ' HISMFT.tblWIpEquipmentException C ';
      break;
    case 'CUSTOMERLOTNO':
      return ' HISMFT.tblWIpEquipmentException C ';
      break;
    case 'EQUIPMENTNO':
      return ' HISMFT.tblWIpEquipmentException C ';
      break;
    case 'DEVICENO':
      return ' HISMFT.tblWipLotData D ';
      break;
    case 'EQUIPMENTTYPE':
      return ' HISMFT.tblMstEquipmentType B';
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}


function genJoin(field){
  return _.contains(['BOARDNO', 'ASSNO', 'ASSSERIALNO'], field) ? " RIGHT JOIN HISMFT.TBLENGASSDATA b ON c.CUSTNO = b.CUSTOMERNO " : '';
}

/*
* ex: ` AND b.ASSNO = '40010018' 
*       AND b.BOARDNO = '10204194' `
*/
function genMatch(cond){
  return _(cond).pick(['CUSTOMERCHISNAME', 'CUSTOMERLOTNO', 'DEVICENO','LOTNO','EQUIPMENTTYPE','EQUIPMENTNO'])
    .map((v, k) => ` AND ` + getDistinctField(k) + ` = '` + v + `' `)
    .value().join('\n');
}

function genSql(param,shcema){
    //shcema = HISMFT , HISLFT
    // broadcom data in HISLFT
    //
    var sql_head =
    `SELECT C.EXCEPTIONNO,C.LotNo, C.CUSTOMERLOTNO, C.EQUIPMENTNO,
     C.STATUSDESC, C.HANDLEMEMO,C.ERRORCODE, C.CreateDate, 
     D.CustomerNo ,D.DeviceNo, SUM(E.DELTASEC) AS DATEDELTA,E.MAINSTATUSNO, 
     B.EquipmentCategory,B.EquipmentType ,F.CUSTOMERCHISNAME 
     FROM `+ 
      shcema+`.tblMstEquipmentType B ,
       HISMFT.tblWIpEquipmentException  C 
       LEFT JOIN `+shcema+`.tblWipLotData D
                     ON C.LotNo = D.LotNo 
       LEFT JOIN `+shcema+`.tblMSTEquipment A
                     ON C.EquipmentNo = A.EquipmentNo
       LEFT JOIN Masterdm.DIM_Customer F 
                     ON D.CustomerNo = F.CustNo ` +
      ` LEFT JOIN (
       SELECT LotNo, MAINSTATUSNO,BORROWNO,
      ((EVENTENDTIME-EVENTSTARTTIME)*24*60*60) AS DELTASEC
       FROM HISMFT.DTBLWIPEQUIPRTCONTENT
       ) E ON (C.LotNo = E.LotNo AND C.EXCEPTIONNO =E.BORROWNO) 
      WHERE (C.CREATEDATE >= :fromDate AND C.CREATEDATE < :toDate) `;

     var sql_tail =   
            ` AND (B.EquipmentCategory = '上下板機' OR B.EquipmentCategory = '奔應爐' ) 
             AND A.EquipmentType = B.EquipmentType 
             AND F.ORGANIZATION = 'KYE' 
             AND E.MAINSTATUSNO = '2' 
             GROUP BY C.EXCEPTIONNO,C.LotNo, C.STATUSDESC,C.CUSTOMERLOTNO,
             C.EQUIPMENTNO,C.HANDLEMEMO, C.CreateDate,C.ERRORCODE,
             B.EquipmentCategory,B.EquipmentType, D.CustomerNo, D.DeviceNo,
             E.MAINSTATUSNO, F.CUSTOMERCHISNAME 
             ORDER BY C.CreateDate `; 
             
             // oracle need to list all column to group by 

    var sql_filter = (shcema=='HISLFT')?" AND F.CUSTOMERCHISNAME = 'Broadcom' ":" " ;

    _.forOwn(param,function(val,key){
        switch(key) {
          case 'CUSTOMERCHISNAME':
              sql_filter += " AND F.CUSTOMERCHISNAME = :custName ";
            break;            
          case 'CUSTOMERLOTNO':
              sql_filter += " AND C.CUSTOMERLOTNO = :custLotNo ";
            break;
          case 'DEVICENO':
              sql_filter += " AND D.DeviceNo = :deviceNo ";
            break;
          case 'LOTNO':
              sql_filter += " AND C.LotNo = :lotNo ";
            break;
          case 'EQUIPMENTTYPE':
              sql_filter += " AND B.EquipmentType = :equipType ";
            break;
          case 'EQUIPMENTNO':
              sql_filter += " AND C.EQUIPMENTNO = :equipNo ";
            break;         
        }
    })             

    return sql_head + sql_filter + sql_tail
    // return sql_head + sql_tail
}

function getExcecptionInfoSql(param,schema){
    //shcema = HISMFT , HISLFT
    // broadcom data in HISLFT
    //
    var sql_head =
    `SELECT C.EXCEPTIONNO,C.LotNo, C.CUSTOMERLOTNO, C.EQUIPMENTNO,
     C.STATUSDESC, C.HANDLEMEMO,C.ERRORCODE, C.CreateDate, 
     D.CustomerNo ,D.DeviceNo ,F.CUSTOMERCHISNAME 
     FROM `+ 
       `HISMFT.tblWIpEquipmentException  C 
       JOIN `+schema+`.tblWipLotData D
                     ON C.LotNo = D.LotNo 
       LEFT JOIN Masterdm.DIM_Customer F 
                     ON (D.CustomerNo = F.CustNo AND F.ORGANIZATION = 'KYE') 
       LEFT JOIN HISMFT.tblMSTEquipment A
              ON (C.EQUIPMENTNO=A.EQUIPMENTNO AND A.LINENAME ='MFT')
       LEFT JOIN HISMFT.tblMstEquipmentType B
              ON A.EquipmentType=B.EquipmentType
      WHERE (C.CREATEDATE >= :fromDate AND C.CREATEDATE < :toDate) 
      AND (B.EquipmentCategory = '上下板機' OR B.EquipmentCategory = '奔應爐' ) `;


     var sql_tail = ` ORDER BY C.CreateDate `; 
             
             // oracle need to list all column to group by 

    var sql_filter = (schema=='HISLFT')?" AND F.CUSTOMERCHISNAME = 'Broadcom' ":" " ;

    _.forOwn(param,function(val,key){
        switch(key) {
          case 'CUSTOMERCHISNAME':
              sql_filter += " AND F.CUSTOMERCHISNAME = :custName ";
            break;
         case 'EQUIPMENTTYPE':
              sql_filter += " AND B.EquipmentType = :equipType ";
            break;
          case 'CUSTOMERLOTNO':
              sql_filter += " AND C.CUSTOMERLOTNO = :custLotNo ";
            break;
          case 'DEVICENO':
              sql_filter += " AND D.DeviceNo = :deviceNo ";
            break;
          case 'LOTNO':
              sql_filter += " AND C.LotNo = :lotNo ";
            break;
          case 'EQUIPMENTNO':
              sql_filter += " AND C.EQUIPMENTNO = :equipNo ";
            break;         
        }
    })             

    return sql_head + sql_filter + sql_tail
}

function genParam(param){

    var fromDate = new Date(param.from);
    var toDate = new Date(param.to);
    var filterObj={'fromDate':fromDate,'toDate':toDate}

    _.forOwn(param,function(val,key){
        switch(key) {
          case 'CUSTOMERCHISNAME':
              filterObj['custName'] = val
            break;            
          case 'CUSTOMERLOTNO':
              filterObj['custLotNo'] = val
            break;
          case 'DEVICENO':
              filterObj['deviceNo'] = val
            break;
          case 'LOTNO':
              filterObj['lotNo'] = val
            break;
          case 'EQUIPMENTTYPE':
              filterObj['equipType'] = val
            break;
          case 'EQUIPMENTNO':
              filterObj['equipNo'] = val
            break;         
        }
    })             

    return filterObj;
}

function genOvenDownTimeSql(param){
    var downTimeSql=`
      SELECT EQUIPMENTNO,MAINSTATUSNO,CREATEDATE,((EVENTENDTIME-EVENTSTARTTIME)*24*60*60) AS DELTASEC
      FROM HISMFT.DTBLWIPEQUIPRTCONTENT
      WHERE (EVENTSTARTTIME >= :fromDate AND EVENTSTARTTIME < :toDate)
            AND (MAINSTATUSNO='2' OR MAINSTATUSNO='0')
            AND EQUIPMENTNO = :equipNo
    `
    return downTimeSql
}

function genAllOvenDownTimeSql(param){

      var ovenCount=param.OVENLIST.length;
    var ovenStr="";
    if(ovenCount>1){
       ovenStr = ` AND EQUIPMENTNO IN (`
      _.forEach(param.OVENLIST,function(oid,idx){
        ovenStr += ((idx+1)==ovenCount)? `'`+oid+`') `: `'`+oid+`',`
      })
    }
    else if (ovenCount ==1){
      ovenStr = ` AND EQUIPMENTNO = '`+param.OVENLIST[0] +`'`
    }

    var downTimeSql=`
      SELECT EQUIPMENTNO,MAINSTATUSNO,CREATEDATE,((EVENTENDTIME-EVENTSTARTTIME)*24*60*60) AS DELTASEC
      FROM HISMFT.DTBLWIPEQUIPRTCONTENT
      WHERE (EVENTSTARTTIME >= :fromDate AND EVENTSTARTTIME < :toDate)
            AND (MAINSTATUSNO='2' OR MAINSTATUSNO='0')` //  AND EQUIPMENTNO = :equipNo

    return downTimeSql + ovenStr
}

function genAllOvenIdleTimeSql(param){

    var idleTimeSql=`
      SELECT A.EQUIPMENTNO,A.MAINSTATUSNO,A.CREATEDATE,
             ((A.EVENTENDTIME-A.EVENTSTARTTIME)*24*60*60) AS DELTASEC,
             B.EquipmentType, C.EquipmentCategory
      FROM HISMFT.DTBLWIPEQUIPRTCONTENT A
      LEFT JOIN HISMFT.tblMSTEquipment B
              ON A.EQUIPMENTNO=B.EQUIPMENTNO
      LEFT JOIN HISMFT.tblMstEquipmentType C
              ON B.EquipmentType=C.EquipmentType
      WHERE (A.EVENTSTARTTIME >= :fromDate AND A.EVENTSTARTTIME < :toDate)
            AND A.MAINSTATUSNO='0' 
            AND (C.EquipmentCategory = '上下板機' OR C.EquipmentCategory = '奔應爐' ) `

    var sql_filter = " " ;
    _.forOwn(param,function(val,key){
        switch(key) {       
          case 'EQUIPMENTTYPE':
              sql_filter += " AND B.EquipmentType = :equipType ";
            break;  
          case 'LOTNO':
              sql_filter += " AND A.LotNo = :lotNo ";
            break;
          case 'EQUIPMENTNO':
              sql_filter += " AND A.EQUIPMENTNO = :equipNo ";
            break;         
        }
    })  

    return idleTimeSql+sql_filter ;
}

function genAllOvenRunTimeSql(param){

    var runTimeSql=`
      SELECT A.EQUIPMENTNO,A.MAINSTATUSNO,A.CREATEDATE,A.LOTNO,
             B.EquipmentType, C.EquipmentCategory
      FROM HISMFT.DTBLWIPEQUIPRTCONTENT A
      LEFT JOIN HISMFT.tblMSTEquipment B
              ON A.EQUIPMENTNO=B.EQUIPMENTNO
      LEFT JOIN HISMFT.tblMstEquipmentType C
              ON B.EquipmentType=C.EquipmentType
      WHERE (A.EVENTSTARTTIME >= :fromDate AND A.EVENTSTARTTIME < :toDate)
            AND A.MAINSTATUSNO='1' 
            AND (C.EquipmentCategory = '上下板機' OR C.EquipmentCategory = '奔應爐' ) `

    return runTimeSql ;
}

function genAllOvenTimeSql(param,schema){

    var ovenTimeSql=`
      SELECT A.EQUIPMENTNO,A.MAINSTATUSNO,A.CREATEDATE,A.LOTNO,
             B.EquipmentType, C.EquipmentCategory,((A.EVENTENDTIME-A.EVENTSTARTTIME)*24*60*60) AS DELTASEC,
             D.DeviceNo,D.CUSTOMERLOTNO,F.CUSTOMERCHISNAME 
      FROM HISMFT.tblMSTEquipment B
      LEFT JOIN HISMFT.DTBLWIPEQUIPRTCONTENT A
              ON (A.EQUIPMENTNO=B.EQUIPMENTNO AND B.LINENAME ='MFT')
      LEFT JOIN HISMFT.tblMstEquipmentType C
              ON (B.EquipmentType=C.EquipmentType )
      LEFT JOIN `+schema+`.tblWipLotData D
                     ON A.LotNo = D.LotNo 
      LEFT JOIN Masterdm.DIM_Customer F 
                     ON (D.CustomerNo = F.CustNo AND F.ORGANIZATION = 'KYE')

      WHERE (A.EVENTSTARTTIME >= :fromDate AND A.EVENTSTARTTIME < :toDate) 
            AND (C.EquipmentCategory = '上下板機' OR C.EquipmentCategory = '奔應爐' ) `
//AND (A.MAINSTATUSNO='0' OR A.MAINSTATUSNO='1' OR A.MAINSTATUSNO='2') 
    var sql_filter = (schema=='HISLFT')?" AND F.CUSTOMERCHISNAME = 'Broadcom' ":" " ;
    _.forOwn(param,function(val,key){
        switch(key) {
          case 'CUSTOMERCHISNAME':
              sql_filter += " AND F.CUSTOMERCHISNAME = :custName ";
            break;            
          case 'CUSTOMERLOTNO':
              sql_filter += " AND A.CUSTOMERLOTNO = :custLotNo ";
            break;
          case 'DEVICENO':
              sql_filter += " AND D.DeviceNo = :deviceNo ";
            break;
          case 'LOTNO':
              sql_filter += " AND A.LotNo = :lotNo ";
            break;
          case 'EQUIPMENTNO':
              sql_filter += " AND A.EQUIPMENTNO = :equipNo ";
            break;
          case 'EQUIPMENTTYPE':
              sql_filter += " AND B.EquipmentType = :equipType ";
            break;    
        }
    })   

    return ovenTimeSql +sql_filter;
}


class XdownManager {
  constructor(utils) {
    // this.coll = ECID;
    this.utils = utils;
  }

  getSearchField (query) {
    var df = Q.defer();
    try {
      var distFld = getDistinctField(query.field);
      // var joinTable = getTableName(query.field);
      var joinTable = getSingleTableName(query.field)
      var sql = `
      SELECT 
        *
      FROM (
        SELECT
          DISTINCT ` + distFld + `
        FROM
           `+ joinTable + `
        WHERE
          UPPER(` + distFld + `) LIKE :regex

      )
      WHERE
        rownum <= :rowlimit
      `;
      var vars = {
        regex: _.toUpper(query.regex) + '%',
        rowlimit: 100
      };

      this.utils.ctrls.mes.exec(sql, vars)
      .then(result => df.resolve(_.map(result, query.field)))
      .catch(err => df.reject(err));
    } catch (err){
    	console.log(err)
      df.reject(err.stack);
    }
    return df.promise;
  }

  getSearchResult (params,schema) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {};
      // console.log(query)
       var sqlStr = genSql(query,schema);
       var bindvars =genParam(query);
      this.utils.ctrls.mes.exec(sqlStr, bindvars)
      .then(function(result){
        try {
          df.resolve(result);
        } catch (err) {
          df.reject(err.stack);
        }
      })
      .catch(err => {
        console.error(err);
        df.reject(err);
      });

    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };

  getDailyData (params,schema) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      // console.log(query)
       var sqlStr = `        
       SELECT A.MAINSTATUSNO,A.EQUIPMENTNO,A.LOTNO,((A.EVENTENDTIME-A.EVENTSTARTTIME)*24*60*60) AS DELTASEC,C.CUSTOMERCHISNAME
        FROM 
            HISMFT.DTBLWIPEQUIPRTCONTENT A
            LEFT JOIN HISMFT.TblWipLotData B
              ON A.LOTNO = B.LOTNO
            LEFT JOIN Masterdm.DIM_Customer C
              ON (B.CustomerNo = C.CustNo AND C.ORGANIZATION = 'KYE')
        WHERE 
            C.ORGANIZATION ='KYE' AND
            (EVENTSTARTTIME >= :fromDate AND EVENTSTARTTIME < :toDate)
            AND (A.MAINSTATUSNO='2' OR A.MAINSTATUSNO='0')
            ORDER BY A.EVENTENDTIME DESC`;
       var bindvars =genParam(query);
       // console.log(bindvars)
      this.utils.ctrls.mes.exec(sqlStr, bindvars)
      .then(function(result){
        try {
          // console.log(result)
          df.resolve(result);
        } catch (err) {
          df.reject(err.stack);
        }
      })
      .catch(err => {
        console.error(err);
        df.reject(err);
      });

    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };


  getOvenDownTime (params) {
    var df = Q.defer();
    try {
      var query = params;
      // console.log(query)
      var match = {}
        var sqlStr = genAllOvenDownTimeSql(query)
       var bindvars =genParam(query);
      this.utils.ctrls.mes.exec(sqlStr,bindvars)
      .then(function(result){
        try {
           // console.log(result)
          df.resolve(result);
        } catch (err) {
          df.reject(err.stack);
        }
      })
      .catch(err => {
        console.error(err);
        df.reject(err);
      });

    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };

  getOvenIdleInfo (params) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
        var sqlStr = genAllOvenIdleTimeSql(query)
       var bindvars =genParam(_.pick(query,['from','to','EQUIPMENTNO'
                                          ,'EQUIPMENTTYPE','LOTNO']));
       // console.log(sqlStr)
       // console.log(bindvars)
      this.utils.ctrls.mes.exec(sqlStr,bindvars)
      .then(function(result){
        try {
          df.resolve(result);
        } catch (err) {
          df.reject(err.stack);
        }
      })
      .catch(err => {
        console.error(err);
        df.reject(err);
      });

    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };
  getOvenRunInfo (params) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
        var sqlStr = genAllOvenRunTimeSql(query)
       var bindvars =genParam(_.pick(query,['from','to']));
      this.utils.ctrls.mes.exec(sqlStr,bindvars)
      .then(function(result){
        try {
          df.resolve(result);
        } catch (err) {
          df.reject(err.stack);
        }
      })
      .catch(err => {
        console.error(err);
        df.reject(err);
      });

    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };

    getOvenInfo (params,schema) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
        var sqlStr = genAllOvenTimeSql(query,schema)
       var bindvars =genParam(query);
      this.utils.ctrls.mes.exec(sqlStr,bindvars)
      .then(function(result){
        try {
          df.resolve(result);
        } catch (err) {
          df.reject(err.stack);
        }
      })
      .catch(err => {
        console.error(err);
        df.reject(err);
      });

    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };

  getExcecptionInfo (params,schema) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
        var sqlStr = getExcecptionInfoSql(query,schema)
        // console.log(sqlStr)
       var bindvars =genParam(query);
      this.utils.ctrls.mes.exec(sqlStr,bindvars)
      .then(function(result){
        try {
          df.resolve(result);
        } catch (err) {
          df.reject(err.stack);
        }
      })
      .catch(err => {
        console.error(err);
        df.reject(err);
      });

    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };
}


module.exports = XdownManager;
