(function(){
	'use strict';

	function xdownDetailChart(){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				showList:'=',
				titleName:'=',
				groupKey:'=',
				dateRange:'='
			},
			template: '<div style="width: 100%;height: 500px;"><div></div></div>',
			link: function (scope, element, attrs) {



				var chartOptions;

				chartOptions = {
 "type": "serial",
  "theme": "light",
  "autoMargins": false,
  "marginLeft": 60,
  "marginRight": 60,
  "marginTop": 30,
  "marginBottom": 30,
  "dataProvider": [ 

  ],
  "valueAxes": [{
						"id": "v1",
						"axisAlpha": 0,
						"position": "right",
						"ignoreAxisWidth":true,
						"integersOnly":true,
						"title":"Count"
					},{
						"id": "v2",
						"axisAlpha": 0,
						"position": "left",
						"gridAlpha":0,
						"ignoreAxisWidth":true,
						"title":"xDwon",
						"unit":"%"
					}],
  "startDuration": 1,
  "graphs": [ {
    // "alphaField": "alpha",
    // "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
    "fillAlphas": 1,
    // "title": "Income",
    "type": "column",
    "valueField": "count",
    "valueAxis": "v1"
    // "dashLengthField": "dashLengthColumn"
  }, {
    "id": "graph2",
    // "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
    "bullet": "round",
    "lineThickness": 3,
    "bulletSize": 7,
    "fillAlphas": 0,
    "lineAlpha": 1,
    // "title": "Expenses",
    "valueField": "xdownRate",
    "valueAxis": "v2"
    // "dashLengthField": "dashLengthLine"
  } ],
  "categoryField": "MsgText",
  "categoryAxis": {
    "gridPosition": "start",
    "axisAlpha": 0,
    "labelRotation":30
				}
			}

				var target = element[0].children[0];

				var chart = AmCharts.makeChart(target, chartOptions);
				// scope.lineChartData = genHistogramData(scope.siteData, scope.min, scope.range);

				scope.$watch('data', function () {
					updateChart();
				});

				var processData = function(dataArr){
					console.log(dataArr.length)
					var outputArr=[]
					var processRecord={}
					if (scope.dateRange ==7) {
						var currentWeek=-1;
						// var currentWeekStart="";
						// var currentWeekEnd="";
					}
					console.log(scope.groupKey)

					_.forEach(dataArr,function(oInfo){
						var groupName ;
							scope.groupKey = "MsgText";
							groupName = oInfo[scope.groupKey]
													_.update(processRecord,groupName,function(o){
								if(o!=undefined){
									o['count']+=1;
									o['DATEDELTA'] += oInfo['DATEDELTA']
									return o
								}
								else{
									oInfo['count']=1
									return oInfo;
								}
							})
						


					})
					var xAxisCount = _.keys(processRecord).length;
					_.forOwn(processRecord,function(v,k){
						v[scope.groupKey] = k;
						var downSecond = v['DATEDELTA']/(86400*dataArr.length)
						v['xdownRate'] = _.round(downSecond,2)*100;
						outputArr.push(v)

					})
					return outputArr
				}

				var updateChart = function(){
					if(_.isArray(scope.data)){
					// scope.graphList = genGraphs(scope.showList,scope.showTemp)
					// chart.graphs =scope.graphList;
					chart.dataProvider = processData(scope.data);
					console.log(processData(scope.data));
					// chart.titles[0].text = scope.titleName;
					// chartOptions.titles.text = scope.titleName;
					chart.categoryField=scope.groupKey;
					chart.validateData();
					chart.validateNow();
					}	
				}
				scope.getChart = function(){
					return chart;
				}

				updateChart()
				chart.validateData();
				chart.validateNow();
			}
		};
	};

	angular.module('mean.xdown').directive('xdownDetailChart', xdownDetailChart);
	xdownDetailChart.$inject = [];
})();
'use strict';

