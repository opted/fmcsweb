(function(){
	'use strict';

	function xdownChart(_,xqs){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				groupKey:'=',
				secondGroupKey:'=',
				groupValue:'=',
				param:'=',
				mode:'=',
				timeData:'=',
				days:'='
			},
			template: '<div id="xdown" style="width: 100%;height: 500px;"><div></div></div><hr><div id="error" style="width: 100%;height: 500px;"><div></div></div>',
			link: function (scope, element, attrs) {
 				scope.xdownGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "labelText": "[[value]]%",
				    "bulletSize": 7,
				    "fillAlphas": 0,
				    "lineAlpha": 1,
					"maximum": 100,
					"minimum": 0,
				    "valueField": "XDOWNRATE",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

 				scope.failGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "labelText": "[[value]]%",
				    "bulletSize": 7,
				    "fillAlphas": 0,
				    "lineAlpha": 1,
					"maximum": 100,
					"minimum": 0,
				    "valueField": "percentFails",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

				scope.drillLevel = 0;
				var xdownChartOptions ={
					"type": "serial",
					"theme": "light",
					"autoMargins": false,
					"marginLeft": 80,
					"marginRight": 80,
					"marginTop": 10,
					"marginBottom": 80,
					"dataProvider": [],
					"valueAxes": [
							{
							"id": "v1",
							"axisAlpha": 0,
							"position": "left",
							"ignoreAxisWidth":true,
							"integersOnly":true,
							"title":"異常單數"
							},
							{
							"id": "v2",
							"axisAlpha": 0,
							"position": "right",
							"gridAlpha":0,
							"ignoreAxisWidth":true,
							"title":"xDwon Rate",
							"unit":"%"
							}
						],
					"startDuration": 0,
					"graphs": [],
					"titles":[{text: "",size:15}],
					"chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
						"cursorAlpha": 0.1,
						"cursorColor":"#000000",
						"fullWidth":true,
						"accessibleLabel":"",
						"valueBalloonsEnabled": false,
						"zoomable": true
					  },
					  "chartScrollbar": {
					        "autoGridCount": true,
					        "scrollbarHeight": 40
					    },
					"categoryField": scope.groupKey,
					"categoryAxis": {
						"gridPosition": "start",
						"axisAlpha": 1,
						"labelRotation":30
					}
				}

				var errorChartOptions ={
					"type": "serial",
					"theme": "light",
					"autoMargins": false,
					"marginLeft": 80,
					"marginRight": 80,
					"marginTop": 10,
					"marginBottom": 80,
					"dataProvider": [],
					"valueAxes": [
							{
							"id": "v1",
							"axisAlpha": 0,
							"position": "left",
							"ignoreAxisWidth":true,
							"integersOnly":true,
							"title":"異常單數"
							},
							{
							"id": "v2",
							"axisAlpha": 0,
							"position": "right",
							"gridAlpha":0,
							"ignoreAxisWidth":true,
							"title":"Failed Rate",
							"unit":"%"
							}
						],
					"startDuration": 0,
					"graphs": scope.failGraphs,
					"titles":[{text: "",size:15}],
					"chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
						"cursorAlpha": 0.1,
						"cursorColor":"#000000",
						"fullWidth":true,
						"valueBalloonsEnabled": false,
						"zoomable": true
					  },
					  // "chartScrollbar": {
					  //       "autoGridCount": true,
					  //       "scrollbarHeight": 40
					  //   },
					"categoryField": scope.groupKey,
					"categoryAxis": {
						"gridPosition": "start",
						"axisAlpha": 1,
						"labelRotation":30
					},
				    "labelFunction": function( value, dataItem ) {
				    	console.log(dataItem)
				    	if(_.isEmpty(value))
			          		return dataItem.dataContext.ERRORCODE;
			          	else return value;
			        	}
				}
				var target = element[0].children[0];
				var target2 = element[0].children[2];

				var chart = AmCharts.makeChart(target, xdownChartOptions);
				var chart2 = AmCharts.makeChart(target2, errorChartOptions);
                chart.clickTimeout = 0;
                chart.lastClick = 0;
                chart.doubleClickDuration = 300;

				function clickHandler(event) {
	              var ts = (new Date()).getTime();
	              if ((ts - chart.lastClick) < chart.doubleClickDuration) {  // [[ Double click ]]
	                if (chart.clickTimeout) {
	                  clearTimeout(chart.clickTimeout);
	                }
	                chart.lastClick = 0; // reset last click
	                	// drillDown(event);
	              } else {  // [[ Single click ]]
	                chart.clickTimeout = setTimeout(() => genChart(event), chart.doubleClickDuration);
	              }
	              chart.lastClick = ts;
	            }

	            chart.addListener("clickGraphItem", clickHandler);

				scope.$watch('secondGroupKey' ,function(nv,ov){
					if( nv!= ov){

						if(scope.drillLevel>0){
							scope.drillLevel=1;
							chart.titles[0].text = scope.groupValue;
							groupDataByKey(scope.secondGroupKey)

							chart2.titles[0].text = scope.groupValue ;
          					updateDetailChart(scope.filterData,scope.mode);
						}
						else {
							alert('請點選圖表中任一子項目。')
						}
					}

				})

				scope.$watch('mode' ,function(nv,ov){
					if( nv!= ov){
						chart2.validateData();
						chart2.validateNow();
						if(scope.drillLevel==0){

							// updateChart()

							updateDetailChart(scope.data,scope.mode)
						}
						else if(scope.drillLevel==1){
							updateDetailChart(scope.filterData,scope.mode)
						}
						else{
							updateDetailChart(scope.secondFilterData,scope.mode)
						}
					}

				})
				scope.$watch('groupKey', function (nv,ov) {
					if(nv != ov) {
						scope.drillLevel=0;
						chart.titles[0].text="";
						chart2.titles[0].text="";
						scope.filterData =[];
						scope.secondGroupKey=null;
						var idleInfo= {},runInfo={},downInfo={};
						var allOvenList = _(scope.timeData)
											.sortBy('EQUIPMENTNO')
											.groupBy('EQUIPMENTNO')
											.keys().value();

						//統計每爐每天的idle time供後續計算使用

                        _.forEach(allOvenList,function(oid){
 
                        	var filterRows=_.filter(scope.timeData,{'EQUIPMENTNO':oid,'MAINSTATUSNO':'0'})
                        	var ovenRows=_.filter(scope.timeData,{'EQUIPMENTNO':oid});
                        	var downRows=_.filter(scope.timeData,{'EQUIPMENTNO':oid,'MAINSTATUSNO':'2'})

                			if(filterRows.length>0){
	                            var e={};
	                            var totalTime=0;
	                            _(filterRows).groupBy(dateGroupBy)
	                                .forOwn(function(row,d){
	                                    e[d]=_.sumBy(_.filter(row,r=>r['DELTASEC']<=86400)
	                                    	,'DELTASEC')||0;
	                                    e['total']= (e['total']||0) +e[d];
	                                })    
	                            idleInfo[oid]=e;                        				
                			}
                			else{
                				idleInfo[oid]={'total':0};
                			}

                			if(ovenRows.length>0){
	                            var e={};
	                            var totalRunTime=0;
	                            _(ovenRows).groupBy(dateGroupBy)
	                                .forOwn(function(row,d){
	                                    e[d]=_.sumBy(_.filter(row,r=>r['DELTASEC']<=86400)
	                                    	,'DELTASEC')||0;
	                                    e['total']= (e['total']||0) +e[d];
	                                })    
	                            runInfo[oid]=e;                        				
                			}
                			else{
                				runInfo[oid]={'total':0};
                			}

                			if(downRows.length>0){
	                            var d={};
	                            var totalDownTime=0;
	                            _(downRows).groupBy(dateGroupBy)
	                                .forOwn(function(row,day){
	                                	d[day]=_(row).orderBy(['EQUIPMENTTYPE','EQUIPMENTNO']).sumBy('DELTASEC')||0;
	                                    // d[day]=_.sumBy(_.filter(row,r=>r['DELTASEC']<=86400)
	                                    // 	,'DELTASEC')||0;
	                                    d['total']= (d['total']||0) +d[day];
	                                })    
	                            downInfo[oid]=d;                        				
                			}
                			else{
                				downInfo[oid]={'total':0};
                			}
                        		
                        })    
                        scope.idleInfo=idleInfo;
                        scope.runInfo=runInfo;
                        scope.downInfo=downInfo;
						updateChart();
					}
				});



				var processData = function(dataArr){
					//累加各爐每天的down,idle time，異常單數(count)
					var outputArr=[];
					var processRecord={};
					var c=0,n=0;;
					//以稼動中查到的爐號為基底
					_.forOwn(scope.runInfo,function(info,ovenId){
						var filterArr = _.filter(scope.timeData,{'EQUIPMENTNO':ovenId});
	
						_.forEach(filterArr,function(oInfo){
						var groupName;
						//若分類為日期，將日期轉成固定格式 ex: 2018/01/30 15:00:00 -> 1/30            
						if(scope.groupKey=='CREATEDATE' ) {//|| scope.drillLevel==1
							groupName=dateGroupBy(oInfo)
							_.update(processRecord,groupName,function(o){
								if(o!=undefined){
									// o['count']+= oInfo['MAINSTATUSNO']=='2'? 1:0;
									o['DELTASEC'] += oInfo['MAINSTATUSNO']=='2'?oInfo['DELTASEC']:0
									return o
								}
								else{
									var recordObj ={};
									recordObj['count'] = _(scope.data)
															.filter(r=>(dateGroupBy(r)==groupName&&r['MAINSTATUSNO']=='2') )
															.value().length;
									// recordObj['count']=oInfo['MAINSTATUSNO']=='2'? 1:0;
									recordObj['DELTASEC'] = oInfo['MAINSTATUSNO']=='2'?oInfo['DELTASEC']:0	
									return recordObj;
								}
							})
						}
						else{ 
							groupName = oInfo[scope.groupKey];
								_.update(processRecord,groupName,function(o){
								if(o!=undefined){
									// o['count']+=oInfo['MAINSTATUSNO']=='2'? 1:0;
									o['DELTASEC'] += oInfo['MAINSTATUSNO']=='2'?oInfo['DELTASEC']:0;
									return o
								}
								else{
									var recordObj ={};
									recordObj['count'] = _(scope.data)
															.filter(r=>r[scope.groupKey]==groupName &&r['MAINSTATUSNO']=='2')
															.value().length;
									// recordObj['count']=oInfo['MAINSTATUSNO']=='2'? 1:0;
									recordObj['DELTASEC']=oInfo['MAINSTATUSNO']=='2'?oInfo['DELTASEC']:0;
									return recordObj;
								}
							})
						}	
						})
					})
					_.forOwn(processRecord,function(v,k){
						var ovenList=[],ovenCount=0,totalIdleTime=0,totalRunTime=0;
						if(scope.groupKey=='CREATEDATE' || scope.drillLevel==1){

							ovenList= _.keys(scope.runInfo);

							ovenList.forEach(function(oid){ 
								if(scope.idleInfo[oid]){
									totalIdleTime += scope.idleInfo[oid][k]||0;
								}
								if(scope.runInfo[oid]){
									totalRunTime += scope.runInfo[oid][k]||0;
								}
							})
							ovenCount=ovenList.length||0;

							if((totalRunTime-totalIdleTime)>0)
								v['XDOWNRATE'] = (100*v['DELTASEC']/(totalRunTime-totalIdleTime)).toFixed(2);
						}
						else{
							ovenList=_(dataArr).filter(r=> r[scope.groupKey]==k)
												  .map('EQUIPMENTNO').uniq().value()

							ovenList.forEach(function(oid){ 
								totalIdleTime+= scope.idleInfo[oid]==undefined? 0:scope.idleInfo[oid]['total'];
								totalRunTime+= scope.runInfo[oid]==undefined? 0:scope.runInfo[oid]['total']
							})
							ovenCount=ovenList.length||0;
						if((86400*ovenCount*scope.days-totalIdleTime)>0)
							v['XDOWNRATE'] = (100*v['DELTASEC']/(totalRunTime-totalIdleTime)).toFixed(2)
							
						}
						v[scope.groupKey] = k;
						outputArr.push(v)

					})

					chart.dataProvider=_.orderBy(outputArr,scope.groupKey);
					chart.graphs=scope.xdownGraphs;
					chart.categoryField=scope.groupKey;

					chart2.dataProvider= processDetailData(dataArr)
					chart2.categoryField=scope.mode == 'fail'? 'STATUSDESC':'HANDLEMEMO';

					chart.validateData();
					chart.validateNow();
					chart2.validateData();
					chart2.validateNow();
				}

				var processDetailData = function(dataArr){
					var outputArr=[];
					var processRecord={};
					var totalFailCount=0;
					_(dataArr).filter({'MAINSTATUSNO':'2'}).forEach(function(oInfo){
						var groupName ;
						groupName = oInfo['ERRORCODE']
						// groupName = scope.mode == 'fail'?oInfo['ERRORCODE']:oInfo['MSGERRORCODE']
						totalFailCount = (totalFailCount||0) +1;
						var showText = scope.mode == 'fail'? 'STATUSDESC':'HANDLEMEMO';
							_.update(processRecord,groupName,function(o){
							if(o!=undefined){
								o['count']+=1;
								if(oInfo[showText]!=null){
									o[showText] = oInfo[showText];
								}
								return o
							}
							else{
								var obj={}
								obj['count']=1
								return obj;
							}
						})
						

					})

					_.forOwn(processRecord,function(v,k){
						v['ERRORCODE'] = k;
						outputArr.push(v)
					})
					outputArr.sort(function(a, b){return b['count']-a['count']})

					var accumulationPFail = 0;
					_.forEach(outputArr,function(oven){
						accumulationPFail += ((oven['count'] / totalFailCount) * 100);
						oven['percentFails'] = accumulationPFail.toFixed(2);
					})

					return outputArr
				}

				var updateChart = function(filterData){

					if(_.isArray(filterData) && filterData.length>0 && scope.drillLevel<2){
						chart.titles[0].text = scope.groupValue;
						groupDataByKey('CREATEDATE')

					}
					else if(_.isArray(scope.data)){
						processData(_.cloneDeep(scope.data));
						chart.dataProvider.sort(function(a, b){return b[scope.groupKey]-a[scope.groupKey]})
					}
					
					
				}

				var groupDataByKey = function(groupKey){
					if(groupKey=='CREATEDATE'){

						chart.dataProvider = _(scope.filterData)
							.groupBy(dateGroupBy)
							.map(function(r,k){
								var ovenList=[],ovenCount=0,totalIdleTime=0,totalRunTime=0;
								var o =new Object({
							   'count':_.countBy(_.filter(r,{'MAINSTATUSNO':'2'}),function(row){
       								return dateGroupBy(row)
									})[k]
								})
								var timeDataInDay =_(scope.filterTimeData).filter(row=> customGroupBy(row,'CREATEDATE')==k).value()

								ovenList=_(timeDataInDay)
												  .map('EQUIPMENTNO').uniq().value()
								
								ovenList.forEach(function(oid){ 
									totalIdleTime+= scope.idleInfo[oid]==undefined? 0:scope.idleInfo[oid][k]||0;
									totalRunTime+= scope.runInfo[oid]==undefined? 0:scope.runInfo[oid][k]||0;
								})
								ovenCount=ovenList.length||0;
								if((totalRunTime-totalIdleTime)>0){

									o['XDOWNRATE'] = (100*_.sumBy(_.filter(timeDataInDay,{'MAINSTATUSNO':'2'}),'DELTASEC')/(totalRunTime-totalIdleTime)).toFixed(2)
								}
								o[groupKey]=k;
								return o
							})
							.orderBy(groupKey)
							.value()
					}
					else{
					chart.dataProvider = _(scope.filterData)
							.groupBy(groupKey)
							.map(function(r,k){
								var ovenList=[],ovenCount=0,totalIdleTime=0,totalRunTime=0;
								
								var o =new Object({
							   'count':_.countBy(_.filter(r,{'MAINSTATUSNO':'2'})
							   			,groupKey)[k]
								})
								var timeDataInGroup = _(scope.filterTimeData).filter(row=> row[groupKey]==k).value()
								ovenList=_(timeDataInGroup)
										    .map('EQUIPMENTNO').uniq().value()
							    ovenList.forEach(function(oid){ 
									totalIdleTime+= scope.idleInfo[oid]==undefined? 0:scope.idleInfo[oid]['total'];
									totalRunTime+= scope.runInfo[oid]==undefined? 0:scope.runInfo[oid]['total'];
								})
								ovenCount=ovenList.length||0;
								if((totalRunTime-totalIdleTime)>0)
									o['XDOWNRATE'] = (100*_.sumBy(_.filter(timeDataInGroup,{'MAINSTATUSNO':'2'}),'DELTASEC')/(totalRunTime-totalIdleTime)).toFixed(2)
								o[groupKey]=k;
								return o
							})
							.orderBy(groupKey)
							.value()
					}		
					chart.categoryField =groupKey

					chart.graphs=scope.xdownGraphs;
					chart.validateData();
					chart.validateNow();
				}

				var updateDetailChart = function(data,mode){
					if(_.isArray(data)){
						chart2.dataProvider = processDetailData(data);
						chart2.categoryField= mode== 'fail'? 'STATUSDESC':'HANDLEMEMO';
						chart2.validateData();
						chart2.validateNow();
					}	
				}

				scope.getChart = function(){
					return chart;
				}
				scope.getChart2 = function(){
					return chart2;
				}
        function genChart(event){
        	
          
          if(scope.drillLevel==0){
          	scope.drillLevel+=1;
          	scope.filterData = _.filter(scope.data,function(r){
					return customGroupBy(r,scope.groupKey)== event.item.category
				})
          	scope.filterTimeData = _.filter(scope.timeData,function(r){
					return customGroupBy(r,scope.groupKey)== event.item.category
				})
          	scope.groupValue = event.item.category
          	chart.titles[0].text = event.item.category ;
          	updateChart(scope.filterData)
          	chart2.titles[0].text = event.item.category ;
          	updateDetailChart(scope.filterData,scope.mode);
          }
          else if(scope.drillLevel>=1){
          	if(_.isEmpty(scope.secondGroupKey))
          		alert('請選擇第二群集');
          	else{
          		scope.drillLevel+=1;
          		chart2.titles[0].text = scope.groupValue+'_'+event.item.category
          		scope.secondFilterData=_.filter(scope.filterData,function(r){
          									return customGroupBy(r,scope.secondGroupKey)== event.item.category
          								})
          		updateDetailChart(scope.secondFilterData,scope.mode);
          	}
          }
          else 
          	alert('已到分析底層，請重新選取群集。')

        }

        	var customGroupBy = (row,key) => key == 'CREATEDATE' ? dateGroupBy(row): row[key]

        	var dateGroupBy = function(row){
        	    var sd = (new Date(row['CREATEDATE'])).setHours(6,50,0)
        	    var d = new Date(row['CREATEDATE'])
       	    	var dateStr= (sd<d)? d.toLocaleDateString().slice(5,10) 
       	    				: (new Date(sd-86400000)).toLocaleDateString().slice(5,10) 
       	    	// return (sd<d)? strToLocalDate(d).slice(5,10) 
       	    	// 			: strToLocalDate(new Date(sd-86400000)).slice(5,10) 
       	    	return addZero( dateStr.split('/')[0] ) + '/'+addZero( dateStr.split('/')[1] )

        	}
        	 var setXdownDate = function(d){
	            if(_.isDate(d)){
	                var td = new Date(d);
	                var n = td.setHours(6,50,0,0);
	                return new Date(n)
	            }
        	} 


				updateChart()
				chart.validateData();
				chart.validateNow();
				chart2.validateData();
				chart2.validateNow();
			}
		};
	};

	angular.module('mean.xdown').directive('xdownChart', xdownChart);
	xdownChart.$inject = ['_','xdownQueryService'];
})();
'use strict';

