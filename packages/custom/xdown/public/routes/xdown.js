(function() {
    'use strict';

    function Xdown($stateProvider) {
        $stateProvider.state('xdown example page', {
            url: '/xdown',
            controller:'XdownController',
            templateUrl: 'xdown/views/index.html',
                resolve:{
                    loggedin:function(MeanUser){
                        return MeanUser.checkLoggedIn();
                    }
                }
            })
    }

    angular
        .module('mean.xdown')
        .config(Xdown);

    Xdown.$inject = ['$stateProvider'];

})();
