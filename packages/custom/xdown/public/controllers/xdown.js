    var xdebug;
(function() {
    'use strict';

    /* jshint -W098 */
function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function strToLocalDate(dateIsoStr) {
    var dateObj = new Date(dateIsoStr);
    var dateOptions = { year:'numeric',month: '2-digit', day: '2-digit'}
    var timeOptions = {minute:'2-digit',second:'2-digit'};
  return dateObj.toLocaleDateString('zh',dateOptions) + ' '+  addZero(dateObj.getHours())+":"+dateObj.toLocaleTimeString('zh',timeOptions);
};
    var dateGroupBy = function(row){
        var sd = (new Date(row['CREATEDATE'])).setHours(6,50,0)
        var d = new Date(row['CREATEDATE'])

    	var dateStr= (sd<d)? d.toLocaleDateString().slice(5,10) 
    				: (new Date(sd-86400000)).toLocaleDateString().slice(5,10) 
    	// return (sd<d)? strToLocalDate(d).slice(5,10) 
    	// 			: strToLocalDate(new Date(sd-86400000)).slice(5,10) 
    	return addZero( dateStr.split('/')[0] ) + '/'+addZero( dateStr.split('/')[1] )
    }
    var customGroupBy = (row,key) => key == 'CREATEDATE' ? dateGroupBy(row): row[key]

    function XdownController($scope,$window, $stateParams,$http,srchXdownOpt,xqs,_) {
        $scope.selected = {
            "from": getToday(),  // Set default start day
            "to": getToday(),  // Set default end day
            "isLastLot": false
        };
        xdebug = $scope;
        // console.log('Enter xdown')
        $scope.fields = srchXdownOpt.fields;
        $scope.fieldSearchURL = "/api/xdown/getsearchfield";
        // $scope.groupKey= 'CREATEDATE';
        $scope.rowNum=0;
        $scope.dateRange =1;
        $scope.secondGroupKey=null;
        $scope.rangeOffset=0;
        $scope.getFailedMode=false;
        $scope.mode='fail'
        $scope.showSecondGroupOptions=false;
        $scope.changeGroup = function(groupKey){   
            $scope.chartData= $scope.ovenInfo.gridData;
            $scope.varsSeleted.secondVarsSelected =null;
            // $scope.varsSeleted.firstVarsSelected= _.filter($scope.varsSeleted.varsList,opt => opt.id ==groupKey )[0].name
            $scope.varsSeleted.secondList= _.filter($scope.varsSeleted.varsList,opt => opt.id !=groupKey )
            $scope.groupKey= groupKey;
        }

        $scope.setSecondGroup = function(groupKey){
            
            $scope.secondGroupKey= groupKey;
        }

        var setXdownDate = function(d){

            if(_.isDate(d)){
                var td = new Date(d);
                var n = td.setHours(6,50,0,0);
                return new Date(n)
            }
        } 

        $scope.changeShowData = function(groupValue){
            $scope.varsSeleted.groupListSelected = groupValue;
            if(groupValue != null ){
                $scope.chartData = _.filter($scope.ovenInfo.gridData,function(row){
                    return row[$scope.groupKey] == groupValue
                })
                $scope.groupValue= groupValue;
                // setTimeout(function(){ $scope.groupValue= groupValue;}, 3000);
            }

        }        

        $scope.exportXLS = function(){
          // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
          if ($scope.xdownInfoGrid.api.getModel().getRowCount() < 1){
            $window.alert("無資料可供下載");
            return;
          }
      
          var params = {
            fileName: "Xdown_Table_Data.xls"
          };
      
          $scope.xdownInfoGrid.api.exportDataAsExcel(params);
        };

        $scope.varsSeleted = {
            firstVarsSelected: {"name":"日期",id:"CREATEDATE"},
            secondVarsSelected: null,
            varsList: [{"name":"日期",id:"CREATEDATE"},
            {"name":"客戶",id:"CUSTOMERCHISNAME"},
            {"name":"客批",id:"CUSTOMERLOTNO"},
            {"name":"客型",id:"DEVICENO"},
            {"name":"內批",id:"LOTNO"},
            {"name":"機型",id:"EQUIPMENTTYPE"},
            {"name":"爐號",id:"EQUIPMENTNO"},
            ],

            // slotChooseList: [{"name":"board","checked":true},{"name":"dut","checked":false}],
              dateRangeSelected:"日"
        };

        $scope.varsSeleted.firstVarsSelected = {"name":"日期",id:"CREATEDATE"}
      
        $scope.defaultColumnDefs = [
            {headerName: "Time ", field: "CREATEDATE", width: 130},
            {headerName: "異常單號", field: "EXCEPTIONNO", width: 100},
            {headerName: "Customer Name", field: "CUSTOMERCHISNAME", width: 80},
            {headerName: "Customer Lot #", field: "CUSTOMERLOTNO", width: 130},
            {headerName: "Customer Model", field: "DEVICENO", width: 130},
            {headerName: "Lot #", field: "LOTNO", width: 100},
            {headerName: "機型", field: "EQUIPMENTTYPE", width: 80},   
            {headerName: "Equip #", field: "EQUIPMENTNO", width: 80},
            {headerName: "Error Code", field: "ERRORCODE", width: 80},    
            {headerName: "狀態描述", field: "STATUSDESC", width: 150},
            {headerName: "Handle Memo", field: "HANDLEMEMO", width: 250}
              
        ],
        $scope.xdownInfoGrid = {
            columnDefs:  [],
            rowData:[],
            groupUseEntireRow: false,
            enableSorting: true,
            enableColResize: true,
            //groupSuppressAutoColumn: true,
            //suppressAggFuncInHeader: true,
        };
        $scope.dailyXdownGrid = {
            columnDefs:  [
            {headerName: "時間", field: "CREATEDATE", width: 130},
            {headerName: "客戶名稱", field: "CUSTOMERCHISNAME", width: 80},
            {headerName: "機型", field: "EQUIPMENTTYPE", width: 80}, 
            {headerName: "機台型號", field: "EQUIPMENTNO", width: 80},
            {headerName: "xDown Rate", field: "XDOWNRATE", width: 80},
            {headerName: "異常單數", field: "count", width: 70},
        ],
            rowData:[],
            groupUseEntireRow: false,
            enableSorting: true,
            enableColResize: true,

        };

        $scope.getDailyData = function(){
            var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
            queryParam['OVENLIST'] = _($scope.oracleData)
                        .map('EQUIPMENTNO')
                        .uniq()
                        .value();
            queryParam.to.setDate(queryParam.to.getDate() + 1);

            queryParam.from=setXdownDate(queryParam.from);
            queryParam.to = setXdownDate(queryParam.to); 
            $scope.requestStatus = "正在下載資料...";

            $scope.exportData=[]
            _($scope.ovenInfo.gridData).groupBy(dateGroupBy).keys().forEach(
                function(d){
                    var arr = calXdownByOven(d)
                   $scope.exportData =  _.concat($scope.exportData,arr)
                })

                $scope.processResult = _($scope.exportData)
                                    .forEach(function(r){
                                       var match =  _.find($scope.ovenInfo.gridData,function(x){
                                            return (x['EQUIPMENTNO'] == r['EQUIPMENTNO']
                                                    && dateGroupBy(x) == r['CREATEDATE'])
                                        })
                                        if(match){ 
                                            _.assign(r,_.pick(match,['EQUIPMENTTYPE','CUSTOMERCHISNAME']));
                                            // r['count']=errCount[dateGroupBy(r)][r['EQUIPMENTNO'] ]
                                        }
                                    }) 
                $scope.dailyXdownGrid.api.setRowData( _.sortBy($scope.processResult,'CUSTOMERCHISNAME'));
                var exportParams = {
                      fileName: "XdownDailyReport.xls"
                    };

                $scope.dailyXdownGrid.api.exportDataAsExcel(exportParams);
                $scope.requestStatus = "下載完成";

        }


        $scope.changeContext =function(){
        	if($scope.getFailedMode){
              _.forEach($scope.ovenInfo.gridData,function(o){
              	if(o['hasChange']){
	         		var temp=o['HANDLEMEMO']
	         		o['HANDLEMEMO']=o['TEMPRECORD'];
	         		o['TEMPRECORD']=temp;
         		}
				
              })
              $scope.xdownInfoGrid.api.setRowData( $scope.ovenInfo.gridData);
              $scope.chartData =  $scope.ovenInfo.gridData;
              $scope.mode = $scope.mode=='fail'?'error':'fail'
        	}
        	else{
	            var startTime = new Date();
	            $scope.queryTime = "";
	            var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
	            if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
	                $window.alert("請設定日期");
	                return 0;
	            }
	            queryParam.to.setDate(queryParam.to.getDate() + 1);  
	            queryParam.from=setXdownDate(queryParam.from);
	            queryParam.to = setXdownDate(queryParam.to); 

	            $scope.days=(queryParam.to -queryParam.from)/86400000;

	            _.assign(queryParam, srchXdownOpt.getMatch($scope.fields));
	    
	            var config = {
	                eventHandlers: {
	                    progress: function(event){
	                        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
	                        $scope.requestStatus = "正在下載資料...";
	                    },
	                }
	            };
	            $scope.requestStatus = "等待 Server 回應...";
	            $scope.isSearching = true;

        		xqs.getFailedInfo(queryParam).then(function(results){
                    $scope.errMsgData = results

                    _.forEach($scope.ovenInfo.gridData,function(o){

                        $scope.errMsgData.some(function(e){
                           if(e['Oven_ID'] == o['EQUIPMENTNO'] && e['LotNum'] == o['LOTNO'] ){
                           		var temp = o['HANDLEMEMO'];
                                o['HANDLEMEMO'] = e['MsgText'] +" Count: "+ e['ERRCNT'];
                           		o['TEMPRECORD'] = temp;
                                o['hasChange']=true;
                            }                        	
                        	return (e['Oven_ID'] == o['EQUIPMENTNO'] && e['LotNum'] == o['LOTNO'] )
                        })
                        // var errMsgText=""
                        // var filterArr=_($scope.errMsgData).sortBy('ERRCNT')
                        //                     .reverse()
                        //                     .filter(e=> (e['Oven_ID']==o['EQUIPMENTNO'] && e['LotNum'] == o['LOTNO']))
                        //                     .value()
                        // filterArr.forEach(r=> errMsgText+=r['MsgText'] +" Count: "+ r['ERRCNT']+". ")                
                        // if(errMsgText!="" && o['MAINSTATUSNO']=="2"){
                        //     var temp = o['HANDLEMEMO']||"";
                        //     o['HANDLEMEMO'] = _.head(filterArr)['MsgText']
                        //     o['TEMPRECORD'] =temp;
                        //     o['MSGERRORCODE'] = _.head(filterArr)['ErrorCode']
                        //     o['hasChange']=true;                        
                        // }                    

                    })

                    $scope.xdownInfoGrid.api.setRowData( $scope.ovenInfo.gridData);
                    $scope.chartData =  $scope.ovenInfo.gridData;
                    $scope.groupKey= 'CREATEDATE';
                    $scope.getFailedMode=true;	
                    $scope.mode = $scope.mode=='fail'?'error':'fail'
                    $scope.requestStatus = "Done";
                    $scope.isSearching=false;
                    $scope.queryTime = new Date() - startTime;
            })
        	}


              
        }
        $scope.getOvenInfo = function() {
            var startTime = new Date();
            $scope.queryTime = "";
            $scope.getFailedMode=false;
            $scope.rowNum = 0;
            // $scope.mode = (searchType == 'n')? 'fail':'error';
            var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
            if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
                // console.log(queryParam)
                $window.alert("請設定日期");
                return 0;
            }
            queryParam.to.setDate(queryParam.to.getDate() + 1);  
            queryParam.from=setXdownDate(queryParam.from);
            queryParam.to = setXdownDate(queryParam.to); 

            $scope.days=(queryParam.to -queryParam.from)/86400000;

            _.assign(queryParam, srchXdownOpt.getMatch($scope.fields));
    
            $scope.param = queryParam;
            var config = {
                eventHandlers: {
                    progress: function(event){
                        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                        $scope.requestStatus = "正在下載資料...";
                    },
                }
            };
            $scope.requestStatus = "等待 Server 回應...";
            $scope.isSearching = true;
            $scope.xdownInfoGrid.api.setColumnDefs($scope.defaultColumnDefs);
            xqs.getOvenInfo(queryParam).then(function(results){
            	$scope.ovenInfo = results;
            	$scope.rowNum = $scope.ovenInfo.exceptionRows.length
                $scope.timeData=_.filter($scope.ovenInfo.rows,r=>r['DELTASEC']<=86400);

            	$scope.xdownInfoGrid.api.setRowData( $scope.ovenInfo.gridData );
            	$scope.chartData = $scope.ovenInfo.gridData;
                $scope.requestStatus = "Done";
                    $scope.queryTime = new Date() - startTime;
                $scope.isSearching = false;
                $scope.groupKey= null;
                $scope.groupKey= 'CREATEDATE';
            })
        };

        var calXdownByOven = function(day){
            console.log(day)
            // day: day string like: "06/12"
            var scope = angular.element('.amcharts-chart-div').scope();
                    //累加各爐每天的down,idle time，異常單數(count)
                    var outputArr=[];
                    var processRecord={};
                    var c=0,n=0;;
                    //以稼動中查到的爐號為基底

                    _.forOwn(scope.runInfo,function(info,ovenId){
                        var filterArr = _.filter(scope.timeData,function(r){
                            return r['EQUIPMENTNO']==ovenId && customGroupBy(r,'CREATEDATE')==day
                        });
                            // console.log(filterArr)
                        _.forEach(filterArr,function(oInfo){
                        var groupName = oInfo['EQUIPMENTNO'];
                                _.update(processRecord,groupName,function(o){
                                if(o!=undefined){
                                    o['DELTASEC'] += oInfo['MAINSTATUSNO']=='2'?oInfo['DELTASEC']:0;
                                    return o
                                }
                                else{
                                    var recordObj ={};
                                    recordObj['count'] = _(scope.data)
                                                            .filter(r=>r['EQUIPMENTNO']==groupName 
                                                                && r['MAINSTATUSNO']=='2'
                                                                && customGroupBy(r,'CREATEDATE')==day)
                                                            .value().length;
                                    recordObj['DELTASEC']=oInfo['MAINSTATUSNO']=='2'?oInfo['DELTASEC']:0;
                                    return recordObj;
                                }
                            })
                           
                        })
                    })
                    // console.log(processRecord)
                    _.forOwn(processRecord,function(v,k){
                        var ovenList=[],ovenCount=0,totalIdleTime=0,totalRunTime=0;

                            ovenList=_(scope.data).filter(r=> r['EQUIPMENTNO']==k && customGroupBy(r,'CREATEDATE')==day)
                                                  .map('EQUIPMENTNO').uniq().value()

                            ovenList.forEach(function(oid){ 
                                totalIdleTime+= scope.idleInfo[oid]==undefined? 0:scope.idleInfo[oid][day];
                                totalRunTime+= scope.runInfo[oid]==undefined? 0:scope.runInfo[oid][day]
                            })
                            ovenCount=ovenList.length||0;
                        if((totalRunTime-totalIdleTime)>0)
                            v['XDOWNRATE'] = (100*v['DELTASEC']/(totalRunTime-totalIdleTime)).toFixed(2)
                        else
                            v['XDOWNRATE'] = "0.0";
                            
                        
                        v['EQUIPMENTNO'] = k;
                        v['CREATEDATE'] = day;
                        outputArr.push(v)

                    })
                return outputArr;
        }

    }

    angular
        .module('mean.xdown')
        .controller('XdownController', XdownController);

    XdownController.$inject = ['$scope','$window', '$stateParams','$http','srchXdownOpt','xdownQueryService','_'];

})();
