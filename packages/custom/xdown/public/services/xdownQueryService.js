(function(){
  'use strict';
  function xdownQueryService(_, $http, $window){
    var isSearching = false;
    var status = {
      str: "",
      err: ""
    };
    var dateGroupBy = function(row){
        var sd = (new Date(row['CREATEDATE'])).setHours(6,50,0)
        var d = new Date(row['CREATEDATE'])
        var dateStr= (sd<d)? d.toLocaleDateString().slice(5,10) 
              : (new Date(sd-86400000)).toLocaleDateString().slice(5,10) 
        // return (sd<d)? strToLocalDate(d).slice(5,10) 
        //      : strToLocalDate(new Date(sd-86400000)).slice(5,10) 
        return addZero( dateStr.split('/')[0] ) + '/'+addZero( dateStr.split('/')[1] )
    }
    function isoToLocalDate(dateIsoStr) {

      var dateObj = new Date(dateIsoStr);
      var dateOptions = { year:'numeric',month: '2-digit', day: '2-digit'}
      var timeOptions = {minute:'2-digit',second:'2-digit'};
      
      return dateObj.toLocaleDateString('zh',dateOptions) + ' '+  addZero(dateObj.getHours())+":"+dateObj.toLocaleTimeString('zh',timeOptions);
    };

    var config = {
      eventHandlers: {
      progress: function(event){
        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
        status.str = "正在下載資料...";
      },
      // readystatechange: function(event){
      //   console.log("STATUS:", event);
      // }
      }
    };
    return {
      isSearching: () => isSearching,
      status: status,
      query: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/xdown/getovendowntime', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          // console.log(_.flatten(result.data))
          var rows=_.flatten(result.data);

   
          return rows;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
      getDownTime: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/xdown/getovendowntime', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          // var rows=[];
          // _.forEach(result.data,function(ovenInfo){
          //   rows = _.concat(rows,ovenInfo)
          // })
          var rows=_.flatten(result.data);
          return _(rows)
                .sortBy('CREATEDATE')
                .groupBy(dateGroupBy)
                .map(function(rows,d){
                    var xdown_day=_(rows).groupBy('EQUIPMENTNO')
                           .map(function(row,oid){
                           
                           var down = _.sumBy(row,function(r){
                           return   (r['MAINSTATUSNO']=='2')?r['DELTASEC']:0   
                        });
                            var idle = _.sumBy(row,function(r){
                           return   (r['MAINSTATUSNO']=='0')?r['DELTASEC']:0   
                        })  
                           var runTime = 86400-idle; 
                           var o = new Object({
                           'EQUIPMENTNO':oid,
                           //'count':errCount[k]||0,
                           'XDOWNRATE':((runTime<=0) ? 0:((down/runTime)*100) ).toFixed(2)         
                           })
                           if(runTime>0)
                            return o;
                           })
                          .filter(function(r) { return r!=undefined})
                         .forEach(function(r){
                                r['CREATEDATE'] = d ;
                          })

                        return xdown_day;  
                     }).flattenDeep().value()

          // return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
      getDailyData: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/xdown/getdailydata', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          // var rows=[];
          // _.forEach(result.data,function(ovenInfo){
          //   rows = _.concat(rows,ovenInfo)
          // })
          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },

      getFailedInfo:function(queryParam){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/xdown/getfailedinfo', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);

      },
      getOvenInfo: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/xdown/getoveninfo', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";

          var eRows = _.cloneDeep(_.filter(result.data.exceptionRows,t=>t['CUSTOMERNO']!=null));
           _.forEach(eRows,function(r){
            //計算該批號有幾張異常單
              var repeatCount=_.filter(eRows,f=>f['EQUIPMENTNO']==r['EQUIPMENTNO'] 
                            &&  f['LOTNO']==r['LOTNO']).length || 1;
              //並把down time依照重複單數分配，以避免後續多算該批號的down time
              r['DELTASEC']= _(result.data.rows)
                .filter(f=>f['MAINSTATUSNO']=='2' 
                            && f['EQUIPMENTNO']==r['EQUIPMENTNO'] 
                            &&  f['LOTNO']==r['LOTNO'])
                .sumBy('DELTASEC')/repeatCount;
                
              var m = _.find(result.data.rows,f=> f['MAINSTATUSNO']=='2' 
                            &&  f['EQUIPMENTNO']==r['EQUIPMENTNO'] 
                            &&  f['LOTNO']==r['LOTNO'] );
              _.assign(r,_.pick(m,['EQUIPMENTCATEGORY','EQUIPMENTTYPE','MAINSTATUSNO']));
              _.update(r, 'CREATEDATE', isoToLocalDate);
           })

           // console.log(_.cloneDeep(result.data.rows));
           // 以批號+爐號尋找不到異常單的處理，和回傳沒有異常單的爐號資料供圖表顯示overall情況
           var otherOvens  = _(result.data.rows).groupBy(r=>r['EQUIPMENTNO']+'_'+r['LOTNO']).map(function(rows,k){
              if( _(rows).map('MAINSTATUSNO').indexOf('2') <0){
                return _(rows).orderBy(['MAINSTATUSNO'],['desc']).head();
              } else{
                // console.log(rows)
                var oid = k.split('_')[0];
                var lotno = k.split('_')[1];
                // console.log(oid,lotno)
                //有down time的爐號
                if( _(eRows).map('EQUIPMENTNO').indexOf(oid) <0) {
                  var noRecordOven= _.cloneDeep(_(rows).orderBy(['MAINSTATUSNO'],['desc']).head());
                  noRecordOven['DELTASEC'] = _(rows).filter({'MAINSTATUSNO':'2','EQUIPMENTNO':oid}).sumBy('DELTASEC');
                  noRecordOven['MAINSTATUSNO']='2';
                    return noRecordOven;
                }//有down time的批號
                else if(_(eRows).map('LOTNO').indexOf(lotno) <0 && lotno!=null){
                  var noRecordLot= _.cloneDeep(_(rows).orderBy(['MAINSTATUSNO'],['desc']).head());
                  noRecordLot['DELTASEC'] = _(rows).filter({'MAINSTATUSNO':'2','LOTNO':lotno}).sumBy('DELTASEC');
                  noRecordLot['MAINSTATUSNO']='2';  
                    return noRecordLot;
                }
              }
            }).compact().forEach(function(e){
                _.update(e, 'CREATEDATE', isoToLocalDate);
              
            })

            _(result.data.rows).groupBy(dateGroupBy).forOwn(function(rows,day){ 
              var nullLotRows=_(rows).partition(r=>r['MAINSTATUSNO']!='2'&&r['MAINSTATUSNO']!='1').value()[0];
              var hasLotRows= _(rows).partition(r=>r['MAINSTATUSNO']!='2'&&r['MAINSTATUSNO']!='1').value()[1];

              _.forEach(nullLotRows,function(nlr){
                var match=_.find(hasLotRows,function(hlr){
                    return hlr['EQUIPMENTNO']==nlr['EQUIPMENTNO'];
                      })

                if(match){ 
                  nlr['LOTNO']=match['LOTNO'];
                   _.assign(nlr,_.pick(match,['CUSTOMERCHISNAME','CUSTOMERLOTNO','DEVICENO','LOTNO']));
                }
              })
            })

          _.assign(result.data,{'gridData':
                                _.keys(queryParam).length>2?eRows:_.concat(eRows,otherOvens)})
          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      }
    };
  };

  angular.module('mean.xdown').factory('xdownQueryService', xdownQueryService);
  xdownQueryService.$inject = ["_", "$http", "$window"];
})();