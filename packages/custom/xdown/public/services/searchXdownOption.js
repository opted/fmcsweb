(function(){
    'use strict';
    function srchXdownOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("客戶(Siteno)", "CUSTOMERCHISNAME"),
                uOpt.genField("客批(CustomerLotno)", "CUSTOMERLOTNO"),
                uOpt.genField("客型(CustomerModel)", "DEVICENO"),
                uOpt.genField("內批(Lotno)", "LOTNO"),
                uOpt.genField("機型", "EQUIPMENTTYPE"),
                uOpt.genField("爐號", "EQUIPMENTNO"),
                // uOpt.genField("Failure Mode", "FMODE", "string", false, false),
            ],
            getMatch: uOpt.getMatch
        };
    };

    angular.module('mean.xdown').factory('srchXdownOpt', srchXdownOpt);
    srchXdownOpt.$inject = ['utilsSrchOpt'];
})();