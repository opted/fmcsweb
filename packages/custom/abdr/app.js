'use strict';

var Module = require('meanio').Module;
var Abdr = new Module('abdr');

Abdr.register(function(app, auth, database, utils) {
  Abdr.routes(app, auth, database, utils);
  Abdr.aggregateAsset('css', 'abdr.css');
  Abdr.angularDependencies(['agGrid', 'ui.bootstrap']);
  return Abdr;
});
