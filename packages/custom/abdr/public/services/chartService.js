(function(){
  'use strict';
  function abdrChartService(_) {
    var serialOption = {
      "type": "serial",
      "theme": "none",
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "達交率分析", "size": 30}
      ],
      "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 120
      },
      "dataProvider": [],
      "valueAxes": [{
        "id": "numOfRepairAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "minimum": 0,
        "title": "結單/申請 數",
        "titleFontSize": 20
      },{
        "id": "abdrAxis",
        "axisAlpha": 1,
        "gridAlpha": 0,
        "position": "right",
        "minimum": 0,
        "title": "達交率(%)",
        "titleFontSize": 20
      }],
      "graphs": [{
        "alphaField": "alpha",
        "balloonText": "[[value]] 次",
        "dashLengthField": "dashLength",
        "fillAlphas": 0.8,
        "fillColors": "#F0E68C",
        "lineColor": "#D2691E",
        "legendPeriodValueText": "total: [[value.sum]]",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "top",
        "showAllValueLabels": true,
        "color": "#3366CC",
        "fontSize": 15,
        "title": "申請數",
        "type": "column",
        "valueField": "APPLY",
        "valueAxis": "numOfRepairAxis"
      }, {
        "alphaField": "alpha",
        "balloonText": "[[value]] 次",
        "dashLengthField": "dashLength",
        "fillAlphas": 0.8,
        "fillColors": "#99CCFF",
        "lineColor": "#0066CC",
        "legendPeriodValueText": "total: [[value.sum]]",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "top",
        "showAllValueLabels": true,
        "color": "#3366CC",
        "fontSize": 15,
        "title": "結單數",
        "type": "column",
        "valueField": "REPAIR",
        "valueAxis": "numOfRepairAxis"
      }, {
        "bullet": "square",
        "bulletBorderAlpha": 1,
        "bulletBorderThickness": 1,
        "bulletColor": "#FF6600",
        "lineColor": "#FF9933",
        "dashLengthField": "dashLength",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "left",
        "showAllValueLabels": true,
        "color": "#FF0000",
        "fontSize": 15,
        "title": "達交率(%)",
        "fillAlphas": 0,
        "valueField": "ABDR",
        "valueAxis": "abdrAxis"
      }],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "GRP",
      "export": {
        "enabled": true
      },
      "categoryAxis": {
        "autoRotateCount": 8,
        "autoRotateAngle": -45
      }
    };

    /*
      groupchart with sorting
      參考: https://www.amcharts.com/kbase/ordering-columns-within-category-value/
    */
    AmCharts.addInitHandler(function(chart) {
      // Check if enabled
      if (chart.sortColumns != true)
        return;

      /**
       * Iterate through data
       */
      _.forEach(chart.dataProvider, d => {
        // Collect all values for all graphs in this data point
        var values = _.map(chart.graphs, g => new Object({'value': d[g.valueField], 'graph': g}));

        // Drop undefined bar
        values = _.reject(values, v => _.isUndefined(v.value));

        // Sort by value
        values.sort((a, b) => b.value - a.value);

        // Apply `columnIndexField`
        _.forEach(values, (v, indx) => {
          v.graph.columnIndexField = v.graph.valueField + "_index";
          d[v.graph.columnIndexField] = indx;
        });
      });
    }, [ "serial" ] );

    var clusterOption = {
      "type": "serial",
      "theme": "light",
      "sortColumns": true,
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "BIB 維修分析", "size": 30}
      ],
      "legend": {
        "horizontalGap": 2,
        "position": "bottom",
        "useGraphSettings": true,
        "markerSize": 10,
        "equalWidths": false
      },
      "valueAxes": [{
        "id": "numOfRepairAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "title": "維修次數(次)",
        "titleFontSize": 20,
        "minimum": 0
      }],
      "dataProvider": [],
      "graphs": [],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "GRP",
      "categoryAxis": {
        // "autoRotateCount": 6,
        // "autoRotateAngle": -10,
        "labelRotation": -20,
        // "gridPosition": "start",
        "gridAlpha": 0,
        // "position": "left"
      },
      "export": {
        "enabled": true
      },
    };

    var CDFOption = {
      "type": "serial",
      "theme": "light",
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "BIB 維修分析", "size": 30}
      ],
      "legend": {
        "horizontalGap": 2,
        "position": "bottom",
        "useGraphSettings": true,
        "markerSize": 10,
        "equalWidths": false
      },
      "valueAxes": [{
        "id": "numOfRepairAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "title": "維修次數(次)",
        "titleFontSize": 20,
        "minimum": 0
      }, {
        "id": "cdfAxis",
        "axisAlpha": 1,
        "gridAlpha": 0,
        "position": "right",
        "title": "累積分布率(%)",
        "titleFontSize": 20,
        "minimum": 0,
        "maximum": 100
      }],
      "dataProvider": [],
      "graphs": [
      ],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "GRP",
      "categoryAxis": {
        // "autoRotateCount": 6,
        // "autoRotateAngle": -10,
        "labelRotation": -20,
        // "gridPosition": "start",
        "gridAlpha": 0,
        // "position": "left"
      },
      "export": {
        "enabled": true
      },
    };


    function hashCode(str) {
      var hash = 0;
      _.forEach(_.range(str.length), i => hash = str.charCodeAt(i) + ((hash << 5) - hash))
      return hash;
    }

    function intToRGB(i){
      var c = (i & 0x00FFFFFF).toString(16).toUpperCase();
      return "#" + ("00000".substring(0, 6 - c.length) + c);
    }

    function intToRGB(i){
      var c = (i & 0x00FFFFFF).toString(16).toUpperCase();
      return "#" + ("00000".substring(0, 6 - c.length) + c);
    }

    return {
      getSerialOption: () => _.cloneDeep(serialOption),
      genClusterOption: (fields, data) => {
        if (data.length > 1){
          var opt = _.cloneDeep(clusterOption);
          _.forEach(fields, (f, i) => {
             // 採用 hash 讓每次同一名稱的 bar 顏色一致
            var color = intToRGB(hashCode(f));
            opt.graphs.push({
              "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
              "fillAlphas": 0.8,
              "fillColors": color,
              "labelText": "[[value]]",
              "showAllValueLabels": true,
              "lineAlpha": 0.3,
              "title": f,
              "type": "column",
              "color": "#000000",
              "valueField": f
            });
          });
          opt.dataProvider = data;
          return opt;
        } else {
          // 若為單筆，則顯示柏拉圖
          data = _.head(data);
          var opt = _.cloneDeep(CDFOption);
          opt.graphs.push({
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "fillColorsField": "color",
            "labelText": "[[value]]",
            "showAllValueLabels": true,
            "lineAlpha": 0.3,
            "title": "維修次數(次)",
            "type": "column",
            "color": "#000000",
            "valueField": "VAL"
          });
          opt.graphs.push({
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "bullet": "square",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "bulletColor": "#FF6600",
            "lineColor": "#FF9933",
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "labelPosition": "left",
            "showAllValueLabels": true,
            "title": "累積分布率(%)",
            "color": "#FF0000",
            "fillAlphas": 0,
            "valueField": "CDF",
            "valueAxis": "cdfAxis"
          });

          // 拆分 repaire 成各 bar
          var bars = _.map(fields, f => new Object({GRP: f, VAL: data[f], color: intToRGB(hashCode(f))}));
          bars.sort((a, b) => b.VAL - a.VAL);

          // 計算累積分布
          var total = _.sumBy(bars, 'VAL');
          _.forEach(bars, (b, i) => b.CDF = _.round(_.divide(_.sumBy(bars.slice(0, i + 1), 'VAL'), total)*100, 1));
          
          opt.dataProvider = bars;
          return opt;
        }
      },
    };
  };

  angular.module('mean.abdr').factory('abdrChartService', abdrChartService);
  abdrChartService.$inject = ['_'];
})();