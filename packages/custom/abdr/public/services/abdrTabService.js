(function(){
  'use strict';
  function abdrTabService(_, $timeout, mso) {
    var tabs = [];
    return {
      list: tabs,
      getSize: () => tabs.length,
      removeTab: (i) => tabs.splice(i, 1),
      getLastTab: () => _.get(_.last(tabs), 'id'),
      addTab: function(subMatch) {
        var newTab = {
          'id': new Date().getTime(),
          'title': mso.genTabTitle(subMatch),
          'subMatch': subMatch,
          'fields': mso.getRemainField(subMatch)
        };
        $timeout(tabs.push(newTab));
      }
    };
  };

  angular.module('mean.abdr').factory('abdrTabService', abdrTabService);
  abdrTabService.$inject = ['_', '$timeout', 'abdrSrchOpt'];
})();