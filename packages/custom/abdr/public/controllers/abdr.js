'use strict';

function AbdrController(_, $scope, $window, $timeout, $rootScope, aso, aqs, ats) {
  // 選取條件 (含申請日期)
  $scope.selected = {
    "from": moment().subtract(4 ,'day').startOf('day')._d,
    "to": moment().startOf('day')._d
  };

  // 選取條件 (含結單日期)
  $scope.selected2 = {
    "from": moment().subtract(4 ,'day').startOf('day')._d,
    "to": moment().startOf('day')._d
  };

  $scope.isSearching = aqs.isSearching;  // 卡介面操作
  $scope.rowNum = 0;  // 資料筆數
  $scope.fields = aso.fields;  // 網頁的查詢欄位
  $scope.fieldSearchURL = "/api/abdr/getsearchfield";
  $scope.requestStatus = aqs.status; // http Request 當下狀態
  $scope.tabs = ats;
  $scope.searchBy = 'BIB';

  // Tab 數量增減時，跳最後一個
  $scope.$watch('tabs.getSize()', nv => nv > 0 ? $timeout(() => $rootScope.abdr_active = $scope.tabs.getLastTab()) : {});

  $scope.selected.schema = "HISMFT";
  $scope.$watch('schema', nv => {
    if (nv){
      aqs.setSchema(nv);
      $scope.selected.schema = nv;
    }
  });

  $scope.ok = function() {
    $scope.rowNum = 0;
    var params = aso.params; // 搜尋面板條件

    // 設定時間 [申請日期]
    _.assign(params, _.cloneDeep(_.pick($scope.selected, ['from', 'to'])));  // [NOTE] cloneDeep 是因為 _.pick 回傳的是淺層複製
    if (params.to !== null){
      params.to.setDate(params.to.getDate() + 1);  // 讓結束天 = 另一天的起頭  
    }

    // 設定時間 [結單日期]
    var repairPeriod = _.cloneDeep(_.pick($scope.selected2, ['from', 'to']));
    _.assign(params, {'repairFrom': repairPeriod.from, 'repairTo': repairPeriod.to});  // [NOTE] cloneDeep 是因為 _.pick 回傳的是淺層複製
    if (params.repairTo !== null){
      params.repairTo.setDate(params.repairTo.getDate() + 1);  // 讓結束天 = 另一天的起頭  
    }

    // 設定其他參數
    params.match = aso.getMatch($scope.fields);
    params.searchBy = $scope.searchBy;

    if (_.isEmpty(_.pickBy(params, _.identity))){
      $window.alert("請設定至少一種搜尋條件(時間、客戶、BIB Type、板全號、配件/舊配件編號、Failure Mode)...");
      return;
    }

    // 新增 1st Tab
    $scope.tabs.addTab({});
  };
};

angular
  .module('mean.abdr')
  .controller('AbdrController', AbdrController);

AbdrController.$inject = ['_', '$scope', '$window', '$timeout', '$rootScope', 'abdrSrchOpt', 'abdrQueryService', 'abdrTabService'];
