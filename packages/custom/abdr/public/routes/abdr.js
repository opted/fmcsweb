(function() {
  'use strict';

  function Abdr($stateProvider) {
    $stateProvider.state('abdr main page', {
      url: '/abdr',
      templateUrl: 'abdr/views/index.html',
      controller:AbdrController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.abdr')
    .config(Abdr);

  Abdr.$inject = ['$stateProvider'];

})();
