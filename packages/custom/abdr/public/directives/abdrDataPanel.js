(function(){
  'use strict';

  function abdrDataPanel($sce, $uibModal, _, aqs, ats, acs, aso){
    return {
      restrict: 'E',
      scope: {
        tb: "="
      },
      templateUrl: 'abdr/views/abdrDataPanel.html',
      link: function(scope, element, attrs){
        scope.fields = scope.tb.fields;  // 網頁的查詢欄位

        scope.fields = [{'label': "Overall", 'name': "OVERALL"}].concat(scope.fields);  
        var firstTime = true;  // 按下查詢後，首次產生頁面

        scope.groupBy = _.head(scope.fields); // 預設值
        scope.grpLoading = true;  // group loading 畫面
        scope.trdLoading = true;  // trend loading 畫面
        scope.rppLoading = true;  // 維修位置 loading 畫面
        scope.rpiLoading = true;  // 維修項目 loading 畫面
        scope.showTrend = false;  // 預設不顯示 trend chart
        scope.status = aqs.status;  // Query 狀態
        scope.rowNum = null;  // 維修筆數
        scope.groupData = [];
        scope.query = null;
        scope.rawData = null;

        scope.grpTips =$sce.trustAsHtml(`
          <div class="form-group">
            <p><b>[提示]</b> 對Bar或方塊</p>
            <ol>
              <li><b>Single Click</b>: 產生推移圖</li>
              <li><b>Double Click</b>: Drill down 層級</li>
            </ol>
            <p><b>[NOTE]</b> 若切換左側推移圖單位/範圍，需重新點選 Bar</p>
          </div>
        `);

        scope.fmTips =$sce.trustAsHtml(`
          <div class="form-group">
            <p><b>[提示]</b> 對Bar或方塊</p>
            <ol>
              <li><b>Single Click</b>: 顯示該 group 之維修項目分析圖</li>
              <li><b>Double Click</b>: 顯示所有維修項目分析圖</li>
            </ol>
          </div>
        `);

        // 推移圖單位
        scope.periods = [
          {label: "日", name: "DAY"},
          {label: "週", name: "WEEK"},
          {label: "月", name: "MONTH"},
        ];
        scope.period = scope.periods[0];

        // 推移圖範圍
        scope.pRanges = _.map(_.reverse(_.range(1, 13)), r => new Object({idx: r}));
        scope.pRange = scope.pRanges[0];

        var grpChart;
        var trdChart;
        var rppChart;
        var rpiChart;

        // Group 圖 option
        var grpOpt = acs.getSerialOption();
        grpOpt.titles.push({"text": "Group By " + scope.groupBy.label, "size": 15, "bold": false});
        grpOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.grpLoading = false
        }];

        // Trend 圖 option
        var trdOpt = acs.getSerialOption();
        trdOpt.titles[0].text = aso.params.searchBy + "維修 推移分析";
        trdOpt.titles.push({"text": "", "size": 15, "bold": false});
        _.chain(trdOpt.graphs).find({"title": "維修次數"})
          .assign({
            "fillColors": "#00CC66",
            "lineColor": "#006600",
            "color": "#006600",
          }).value();
        _.chain(trdOpt.graphs).find({"title": "BIB & Driver 維修"})
          .assign({
            "fillColors": "#993399",
            "lineColor": "#FF3399",
            "color": "#993399",
          }).value();
        trdOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.trdLoading = false
        }];

        function drillDown(event){
          if (_.keys(scope.tb.subMatch).length == aso.getSize() - 1) {
            alert("已 Drill down 至最後一層");
          } else {
            if (scope.groupBy.name == "OVERALL") {
              scope.$apply(() => scope.groupBy = _.head(_.reject(scope.fields, {name: 'OVERALL'})));
            } else {
              // 根據點選的 bar 更新 sub query
              var subQ = _.cloneDeep(scope.tb.subMatch);
              subQ[scope.groupBy.name] = event.item.category;
              ats.addTab(subQ);  // 新增下一個 tab
            }
          }
        }

        function genTrendChart(event){
          scope.showTrend = true;
          scope.trdLoading = true;
          var qry = _.cloneDeep(aso.params);  // 主搜尋面板條件
          _.assign(qry.match, scope.tb.subMatch);  // 子條件 (drilldown 維度)
          qry.match[scope.groupBy.name] = event.item.category;  // 設定所選的 group
          qry.groupBy = scope.period.name;  // 選擇 週/月
          qry.pRange = scope.pRange.idx;  // 選擇範圍

          aqs.query(qry, 'gettrend', function(data){
            var format = 'YYYY/MM' + (qry.groupBy != "MONTH" ? ('/DD' + (qry.groupBy == "WEEK" ? ' ([w]W)' : '')) : '');
            _.forEach(data, d => d.GRP = moment(d.GRP).format(format));

            if (!trdChart) {
              trdChart = AmCharts.makeChart('abdr-trdcht-' + scope.$id, trdOpt);
            }
            trdChart.titles[1].text = [scope.groupBy.label, event.item.category, qry.pRange, qry.groupBy].join(" ");
            trdChart.dataProvider = data;  
            trdChart.validateData();
            trdChart.validateNow();            
          });
        }

        function genGroupResult(data, group, qry) {
          var rslt = _(data).groupBy(group).map((rd, g) => {
            // [重要] raw data 以項目之分， group 圖必須合併以 "維修單" 呈現
            // (raw data 可能有 單一維修單包含多個項目 的情況)
            var applyNum = _.uniqBy(_.filter(rd, r => {
              return (qry.from ? new Date(r.CREATEDATE) >= qry.from : true) && (qry.to ? new Date(r.CREATEDATE) < qry.to : true);
            }), "REPAIRNO").length;

            var repairNum = _.uniqBy(_.filter(rd, r => {
              return r.STATE == "已結單" && (qry.repairFrom ? new Date(r.REPAIRDATE) >= qry.repairFrom : true) && (qry.repairTo ? new Date(r.REPAIRDATE) < qry.repairTo : true);
            }), "REPAIRNO").length;
            
            var bar = {
              'GRP': g,
              'REPAIR': repairNum,
              'APPLY': applyNum,
              'ABDR': _.round(repairNum*100/applyNum, 2),
              'LABEL': 0
            }
            _.forEach(_.groupBy(rd, "REPAIRPOSITION"), (dd, cond)=> bar[cond] = dd.length);  // 補上 維修位置
            _.forEach(_.groupBy(rd, "REPAIRITEM"), (dd, cond)=> bar[cond] = dd.length);  // 補上 維修項目
            return bar;
          }).value();

          return sort(rslt, 'REPAIR');
        }

        // 處理 維修項目 柏拉圖資料
        function genRepairItemChart(rawData, groupData) {
          scope.repairItemList = _.map(_.uniq(_.map(rawData, "REPAIRITEM")), h => new Object({'name': h, 'checked': true}));
          if (rpiChart) rpiChart.clear();
          var rpiOpt = acs.genClusterOption(_.map(scope.repairItemList, "name"), groupData);
          rpiOpt.titles[0].text = aso.params.searchBy + " 維修項目分析";
          rpiOpt.listeners = [{
            "event": "rendered", 
            "method": (e) => scope.rpiLoading = false
          }];
          rpiChart = AmCharts.makeChart('abdr-rpicht-' + scope.$id, rpiOpt);
        }

        // Overall 時顯示 ASSTYPE，避免接下來 groupby 導致 key 為 undefined
        function getGroupKey() {
          var gb = scope.groupBy.name;
          gb = gb == "OVERALL" ? "ASSTYPE" : gb;
          return gb;
        }

        scope.$watch("groupBy", function(nv, ov){
          if (nv != ov || _.isEmpty(scope.groupData)) {
            scope.grpLoading = true;
            scope.repairGrid.api.showLoadingOverlay();
            scope.query = _.cloneDeep(aso.params);  // 主搜尋面板條件
            _.assign(scope.query.match, scope.tb.subMatch);  // 子條件 (drilldown 維度)
            scope.query.groupBy = nv.name;
            aqs.query(scope.query, 'getgroup', result => {
              scope.rowNum = result.length;
              if (scope.rowNum == 0) {
                alert("查無資料，請給予新條件...");
                scope.grpLoading = false;
                scope.trdLoading = false;
                scope.rppLoading = false;
                return;
              }
              scope.rawData = result;

              var gb = getGroupKey();

              scope.groupData = genGroupResult(scope.rawData, gb, scope.query);

              // 處理 維修位置 柏拉圖資料
              scope.repairPositionList = _.map(_.uniq(_.map(scope.rawData, "REPAIRPOSITION")), h => new Object({'name': h, 'checked': true}));
              if (rppChart) rppChart.clear();
              var rppOpt = acs.genClusterOption(_.map(scope.repairPositionList, "name"), scope.groupData);
              rppOpt.titles[0].text = aso.params.searchBy +  " 維修位置分析";
              rppOpt.listeners = [{
                "event": "rendered", 
                "method": (e) => scope.rppLoading = false
              }];
              rppChart = AmCharts.makeChart('abdr-rppcht-' + scope.$id, rppOpt);
              
              // 維修位置柏拉圖 新增 listener 可篩選 維修項目柏拉圖
              rppChart.clickTimeout = 0;
              rppChart.lastClick = 0;
              rppChart.doubleClickDuration = 500;
              rppChart.addListener("clickGraphItem", event => {
                var ts = (new Date()).getTime();
                if ((ts - rppChart.lastClick) < rppChart.doubleClickDuration) {  // [[ Double click ]]
                  if (rppChart.clickTimeout) clearTimeout(rppChart.clickTimeout);
                  rppChart.lastClick = 0; // reset last click
                  genRepairItemChart(scope.rawData, scope.groupData);
                } else {  // [[ Single click ]]
                  rppChart.clickTimeout = setTimeout(() => {
                    var filterData = _.filter(scope.rawData, {"REPAIRPOSITION": event.item.category})
                    var grpData = genGroupResult(filterData, getGroupKey(), scope.query)
                    genRepairItemChart(filterData, grpData);
                  });
                }
                rppChart.lastClick = ts;
              });

              genRepairItemChart(scope.rawData, scope.groupData);

              // 設定原始資料表
              scope.repairGrid.api.setRowData(result);

            });
          }
        });

        scope.$watch('groupData', function(nv, ov){
          if (nv != ov) {
            if (!_.isEmpty(nv)) {
              // 產生 Group 圖
              grpOpt.titles[0].text = aso.params.searchBy + " 達交率";
              var subtitle = "Group By " + scope.groupBy.label;
              if (!grpChart) {
                grpChart = AmCharts.makeChart('abdr-grpcht-' + scope.$id, grpOpt);

                grpChart.clickTimeout = 0;
                grpChart.lastClick = 0;
                grpChart.doubleClickDuration = 300;

                function clickHandler(event) {
                  var ts = (new Date()).getTime();
                  if ((ts - grpChart.lastClick) < grpChart.doubleClickDuration) {  // [[ Double click ]]
                    if (grpChart.clickTimeout) clearTimeout(grpChart.clickTimeout);
                    grpChart.lastClick = 0; // reset last click
                    drillDown(event);
                  } else {  // [[ Single click ]]
                    grpChart.clickTimeout = setTimeout(() => genTrendChart(event), grpChart.doubleClickDuration);
                  }
                  grpChart.lastClick = ts;
                }

                grpChart.addListener("clickGraphItem", clickHandler);
              }
              grpChart.titles[1].text = subtitle;
              grpChart.dataProvider = nv;
              grpChart.validateNow();  // 觸發 listener
              grpChart.validateData(); // 重畫 data
            } else {
              scope.grpLoading = false;
            }
          }
        }, true);

        function sort(data, sortBy){
          return _.reverse(_.sortBy(data, sortBy));
        }

        // Group 圖 sort 切換
        scope.$watch('sortBy', (nv, ov) => {
          if (nv){
            grpChart.dataProvider = sort(scope.groupData, nv);
            grpChart.validateNow();  // 觸發 listener
            grpChart.validateData(); // 重畫 data
          }
        }); 

      },
      controller: ['$scope', function($scope){
        $scope.repairGrid = {
          columnDefs: [
            {headerName: "申請日期", field: "APPLYDATE", width: 180},
            {headerName: "結單日期", field: "REPAIRDATE", width: 180},
            {headerName: "客戶", field: "CUSTOMERCHISNAME", width: 100},
            {headerName: "BIB Type", field: "BIBTYPE", width: 80},
            {headerName: "板全號", field: "BOARDNO", width: 100},
            {headerName: "維修單號", field: "REPAIRNO", width: 100},
            {headerName: "配件編號", field: "ASSNO", width: 180},
            {headerName: "舊配件編號", field: "ASSSERIALNO", width: 90},
            {headerName: "狀態", field: "STATE", width: 90},
            {headerName: "維修位置", field: "REPAIRPOSITION", width: 180},
            {headerName: "維修項目", field: "REPAIRITEM", width: 180},
          ],
          rowData:[],
          groupUseEntireRow: false,
          enableSorting: true,
          enableColResize: true,
          excelStyles: [{
            id: "greenBackground",
            interior: {color: "#aaffaa", pattern: 'Solid'}
          }]
        };
      }]
    };
  };
  
  angular.module('mean.abdr').directive('abdrDataPanel', abdrDataPanel);
  abdrDataPanel.$inject = ['$sce', '$uibModal', '_', 'abdrQueryService', 'abdrTabService', 'abdrChartService', 'abdrSrchOpt'];
})();