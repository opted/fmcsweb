'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var Q = require('q');
var moment = require('moment');

function numTo2Char(num){
  return ("0" + num).slice(-2);
}

function getFieldStr(field, isCondition){
  switch (field){
    case 'CUSTOMERCHISNAME':
      return 'd.' + field;
      break;
    case 'BOARDNO':
    case 'ASSNO':
    case 'ASSSERIALNO':
      return 'b.' + field;
      break;
    case 'BIBTYPE':
      return isCondition ? "SUBSTR(b.ASSSERIALNO, 2, 3)" : "SUBSTR(b.ASSSERIALNO, 2, 3) as BIBTYPE";
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}

/*
* ex: ` AND r.ASSNO = '40010018' 
*       AND r.BOARDNO = '10204194' `
*/
function genMatch(cond, callback){
  return _(cond).pick(['CUSTOMERCHISNAME', 'BOARDNO', 'ASSNO', 'ASSSERIALNO', 'BIBTYPE'])
    .map((v, k) => callback(k, true) + ` = '` + v + `' `)
    .value().join('\n AND ');
}

function getRepairDataSQL(schema, match, dateObj, searchBy){
  var mtchFld = genMatch(match, getFieldStr);
  var dateSQL = "";
  if (_.some([dateObj.applyFrom, dateObj.applyTo, dateObj.repairFrom, dateObj.repairTo])){
    dateSQL += ` AND ( `;

    var applySQL = ``;
    var applyArr = [];
    if (dateObj.applyFrom) applyArr.push(` a.CREATEDATE >= :applyFrom `);
    if (dateObj.applyTo) applyArr.push(` a.CREATEDATE <= :applyTo `);
    if (applyArr.length > 0) applySQL = (`(` + applyArr.join(` AND `) + `)`);

    var repairSQL = ``;
    var repairArr = [];
    if (dateObj.repairFrom) repairArr.push(` a.REPAIRDATE >= :repairFrom `);
    if (dateObj.repairTo) repairArr.push(` a.REPAIRDATE <= :repairTo `);
    if (repairArr.length > 0) repairSQL = (`(` + repairArr.join(` AND `) + `)`);

    var dateArr = _.reject([applySQL, repairSQL], _.isEmpty);
    dateSQL += (applyArr.length > 0 || repairArr.length > 0) ? dateArr.join(` OR `) : ``;
    dateSQL += ` ) `;
  }
  var sql = `
    SELECT
      a.*,
      b.ASSSerialNo,
      SUBSTR(b.ASSSERIALNO, 2, 3) as BIBTYPE,
      b.AssCustNo,
      b.BoardNo,
      b.PMMAX,
      b.PMMIN,
      c.VENDORSNAME,
      d.CUSTOMERCHISNAME,
      e.REPAIRPOSITION,
      e.REPAIRITEM
    FROM   
      `+ schema + `.TBLWIPASSREPAIR a,
      `+ schema + `.TBLENGASSDATA b,
      (   SELECT
            VENDORNO, 
            VENDORSNAME 
          FROM 
            `+ schema + `.TBLMSTVENDOR) c,
      (   SELECT
            CUSTOMERNAME, 
            MIN (CUSTOMERCHISNAME) AS CUSTOMERCHISNAME
          FROM
            Masterdm.Dim_Customer
          GROUP BY   
            CUSTOMERNAME) d,
      `+ schema + `.TBLWIPASSREPAIRHANDLE e
    WHERE
      ` + (_.isEmpty(mtchFld) ? '' : mtchFld + ' AND ' ) + `
      a.ASSNo = b.ASSNo
      ` + dateSQL + `
      AND a.CUSTOMERNAME = d.CUSTOMERNAME(+)
      AND a.VENDORNO = c.VENDORNO(+)
      AND a.REPAIRNO = e.REPAIRNO(+)
      AND a.AssType = ` + (searchBy == 'DRV' ? `'BI DRIVER'` : `'BURN IN BOARD'`) + `
      
    ORDER BY   
      a.RepairNo
  `;
  return sql;
}

function getTrendSQL(schema, match, prd, dateObj, searchBy){
  var mtchFld = genMatch(match, getFieldStr);
  var dateSQL = "";
  if (_.some([dateObj.applyFrom, dateObj.applyTo, dateObj.repairFrom, dateObj.repairTo])){
    dateSQL += ` AND ( `;

    var applySQL = ``;
    var applyArr = [];
    if (dateObj.applyFrom) applyArr.push(` a.CREATEDATE >= :applyFrom `);
    if (dateObj.applyTo) applyArr.push(` a.CREATEDATE <= :applyTo `);
    if (applyArr.length > 0) applySQL = (`(` + applyArr.join(` AND `) + `)`);

    var repairSQL = ``;
    var repairArr = [];
    if (dateObj.repairFrom) repairArr.push(` a.REPAIRDATE >= :repairFrom `);
    if (dateObj.repairTo) repairArr.push(` a.REPAIRDATE <= :repairTo `);
    if (repairArr.length > 0) repairSQL = (`(` + repairArr.join(` AND `) + `)`);

    var dateArr = _.reject([applySQL, repairSQL], _.isEmpty);
    dateSQL += (applyArr.length > 0 || repairArr.length > 0) ? dateArr.join(` OR `) : ``;
    dateSQL += ` ) `;
  }
  var sql = `
    SELECT
      a.*,
      b.ASSSerialNo,
      SUBSTR(b.ASSSERIALNO, 2, 3) as BIBTYPE,
      b.AssCustNo,
      b.BoardNo,
      b.PMMAX,
      b.PMMIN,
      c.VENDORSNAME,
      d.CUSTOMERCHISNAME,
      e.REPAIRPOSITION,
      e.REPAIRITEM
    FROM   
      `+ schema + `.TBLWIPASSREPAIR a,
      `+ schema + `.TBLENGASSDATA b,
      (   SELECT
            VENDORNO, 
            VENDORSNAME 
          FROM 
            `+ schema + `.TBLMSTVENDOR) c,
      (   SELECT
            CUSTOMERNAME, 
            MIN (CUSTOMERCHISNAME) AS CUSTOMERCHISNAME
          FROM
            Masterdm.Dim_Customer
          GROUP BY   
            CUSTOMERNAME) d,
      HISMFT.TBLWIPASSREPAIRHANDLE e
    WHERE
      ` + (_.isEmpty(mtchFld) ? '' : mtchFld + ' AND ' ) + `
      a.ASSNo = b.ASSNo
      ` + dateSQL + `
      AND a.CUSTOMERNAME = d.CUSTOMERNAME(+)
      AND a.VENDORNO = c.VENDORNO(+)
      AND a.REPAIRNO = e.REPAIRNO(+)
      AND a.AssType = ` + (searchBy == 'DRV' ? `'BI DRIVER'` : `'BURN IN BOARD'`) + `
  `;
  return sql;
}

// 處理時間單位,  ex: "05/12/2017, 11:27:16" => ["05/12/2017", " 11:27:16"] => "2017/12/05 11:27:16"
function formatDateStr(d, field){
  d[field] = d[field] ? moment(d[field]).format("YYYY/MM/DD HH:mm:ss") : null;
}

class AbdrManager {
  constructor(utils) {
    this.mes = utils.ctrls.mes;
    this.mongoQuery = utils.ctrls.mongoQuery;
    this.dt = utils.ctrls.dateTool;
  }

  getSearchField (query) {
    try {
      var mtchFld = genMatch(_.omit(query.match, query.field), getFieldStr);
      var schema = query.exmatch.schema;
      var sql = `
        SELECT 
          *
        FROM (
            SELECT
              DISTINCT ` + getFieldStr(query.field) + `
            FROM
              Masterdm.Dim_Customer d
            FULL JOIN 
              ` + schema + `.TBLENGASSDATA b ON d.CUSTNO = b.CUSTOMERNO
          WHERE
            UPPER(` + getFieldStr(query.field, true) + `) LIKE :regex
            ` + (_.isEmpty(mtchFld) ? '' : 'AND ' + mtchFld ) + `
        )
        WHERE
          rownum <= :rowlimit
      `;

      var vars = {
        regex: _.toUpper(query.regex) + '%',
        rowlimit: 100
      };

      return this.mes.exec(sql, vars)
        .then(rslt => _.map(rslt, query.field));
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getGroup (query) {
    try {
      var dateObj = {
        'applyFrom': query.from ? new Date(query.from) : null,  // 避免直接用 from 作為 sql 的 binding var
        'applyTo': query.to ? new Date(query.to) : null,
        'repairFrom': query.repairFrom ? new Date(query.repairFrom) : null,
        'repairTo': query.repairTo ? new Date(query.repairTo) : null
      }
      dateObj = _.omitBy(dateObj, _.isNull); // 避免 sql var binding 錯誤
      var sql = getRepairDataSQL(query.schema, query.match, dateObj, query.searchBy);
      return this.mes.exec(sql, dateObj).then(rslt => {
        _.forEach(rslt, r => {
          formatDateStr(r, 'CREATEDATE');
          formatDateStr(r, 'REPAIRDATE');
          formatDateStr(r, 'APPLYDATE');
          r.VENDORSNAME = r.VENDORSNAME ? r.VENDORSNAME : 'unknown';
          r.CUSTOMERCHISNAME = r.CUSTOMERCHISNAME ? r.CUSTOMERCHISNAME : 'unknown';
          r.REPAIRPOSITION = r.REPAIRPOSITION ? r.REPAIRPOSITION : 'unknown';
          r.REPAIRITEM = r.REPAIRITEM ? r.REPAIRITEM : 'unknown';
          r.STATE = r.STATE == 0 ? "申請中" : (r.STATE == 1 ? "已結單" : 0);
        });
        return rslt;
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getTrend (query) {
    try {
      var [from, to, range, prd] = this.dt.getPeriodSetting(
        query.from, query.to, query.pRange, query.groupBy, 
        f => `TRUNC(a.` + f + `, 'DD')`,
        f => `TRUNC(a.` + f + `, 'IW')`,
        f => `TO_CHAR(a.` + f + `, 'YYYY-MM')`
        );

      if (query.groupBy === 'MONTH'){
        range = _.map(range, r => moment(r)._d);
      }
      var dateList = _.uniqBy([from, to].concat(range).sort((a, b) => a - b), d => d.getTime());
      var qs = [];
      var range_list = [];
      for (var i = 0; i < (dateList.length - 1); i++) {
        var vars = {
          'applyFrom': dateList[i],
          'applyTo': dateList[i + 1],
          'repairFrom': dateList[i],
          'repairTo': dateList[i + 1]
        };

        var sql = getTrendSQL(query.schema, query.match, prd, vars, query.searchBy);
        qs.push(this.mes.exec(sql, vars)); 
        //   .then(rslt => {
        //   var repairNum = _.uniqBy(_.filter(rslt, {STATE: 1}), "REPAIRNO").length;
        //   var applyNum = _.uniqBy(rslt, "REPAIRNO").length;
        //   var bar = {
        //     'REPAIR': repairNum,
        //     'APPLY': applyNum,
        //     'ABDR': _.round(repairNum*100/applyNum, 2),
        //   }
        //   return bar;
        // }));
        range_list.push(vars);
      }

      return Q.all(qs).then(rslt => {
        rslt = _.map(rslt, (rr, i) => {
          var qry = range_list[i];
          var applyNum = _.uniqBy(_.filter(rr, r => {
            return (qry.applyFrom ? new Date(r.CREATEDATE) >= qry.applyFrom : true) && (qry.applyTo ? new Date(r.CREATEDATE) < qry.applyTo : true);
          }), "REPAIRNO").length;

          var repairNum = _.uniqBy(_.filter(rr, r => {
            return r.STATE == 1 && (qry.repairFrom ? new Date(r.REPAIRDATE) >= qry.repairFrom : true) && (qry.repairTo ? new Date(r.REPAIRDATE) < qry.repairTo : true);
          }), "REPAIRNO").length;
          
          var bar = {
            'GRP': dateList[i],
            'REPAIR': repairNum,
            'APPLY': applyNum,
            'ABDR': _.round(repairNum*100/applyNum, 2),
            'LABEL': 0
          }
          return bar;
        });
        return rslt;
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  };
}

module.exports = AbdrManager;