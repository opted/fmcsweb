(function() {
    'use strict';
    module.exports = function(Abdr, app, auth, database, utils) {
        var AbdrManager = require('../controllers/abdrQuery');
        var abdrMgr = new AbdrManager(utils);
        var checkSchema = utils.ctrls.mes.checkSchema;
        var timeout = 30*60*1000; // 30 min

        app.post('/api/abdr/getsearchfield', function(req, res, next){ // search field too often, do not save requestlog
            abdrMgr.getSearchField(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/abdr/getgroup', checkSchema, function(req, res, next){
            abdrMgr.getGroup(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/abdr/gettrend', checkSchema, function(req, res, next){
            abdrMgr.getTrend(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });
    };

    function handleError(err, res) {
        console.log(err);
        res.status(500).send({'error': err});
    }
})();