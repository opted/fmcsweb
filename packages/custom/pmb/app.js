'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Pmb = new Module('pmb');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Pmb.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Pmb.routes(app, auth, database, utils);

  Pmb.aggregateAsset('css', 'pmb.css');

  Pmb.angularDependencies(['agGrid']);
  return Pmb;
});
