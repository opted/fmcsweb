(function() {
  'use strict';

  function Pmb($stateProvider) {
    $stateProvider.state('pmb main page', {
      url: '/pmb',
      templateUrl: 'pmb/views/index.html',
      controller:PmbController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.pmb')
    .config(Pmb);

  Pmb.$inject = ['$stateProvider'];

})();
