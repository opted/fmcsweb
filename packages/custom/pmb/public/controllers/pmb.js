'use strict';
function PmbController(_, $scope, $http, srchPmbOpt, pmbService, $timeout) {
  $scope.packageName = 'Pmb';
  var today = new Date()
  $scope.selected = {
    "from": new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1 ),
    "to": new Date(today.getFullYear(), today.getMonth(), today.getDate() , today.getHours(), today.getMinutes())
  };
  $scope.selectedToQuery = {'DATETIME': {}} 
  $scope.$watch('selected.from', date => {
    $scope.selectedToQuery['DATETIME']['$gte'] = date;
  });
  $scope.$watch('selected.to', date => {
    $scope.selectedToQuery['DATETIME']['$lte'] = date;
  });

  $scope.fieldSearchURL = "/api/pmb/getSearchField";
  $scope.fields = srchPmbOpt.fields;

  $scope.graphTypes = [
    {'label': 'DPS模組', 'type': 'd'},
    {'label': 'DPS電壓', 'type': 'v'}
  ];
  $scope.nextGraphType = $scope.graphTypes[0];

  $scope.lotData = pmbService;

  $scope.queryData = function(){
    $scope.isSearching = true;
    $scope.showMainGraph = false;
    _.find($scope.lotData.groupTabList.list, {'active': true}).active = false;
    _.find($scope.lotData.groupTabList.list, {'groupKey': 'DATE'}).active = true;
    $scope.queryTime = false;
    $scope.rowNum = false;
    $scope.clearSubGraph();
    $scope.problemOvens = [];
    var startTime = new Date();
    var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
    _.assign(queryParam, srchPmbOpt.getMatch($scope.fields));

    //DEBUG
    /*if (queryParam.OVENID === undefined) {
      //queryParam.from.setDate(queryParam.to.getDate() - 1);
      //queryParam.to.setDate(queryParam.to.getDate());
      queryParam.from = new Date( 2017, 10, 26);
      queryParam.to = new Date( 2017, 10, 28);
      queryParam.OVENID = 'OD-40';
      queryParam.OVENTYPE = 'OD';
      
      //queryParam.from = undefined;
      //queryParam.to = undefined;
      queryParam.LOTNO = 'NHB02PJAB2';
    };*/
    console.log(queryParam);

    if (queryParam.OVENID === undefined && queryParam.LOTNO === undefined && queryParam.OVENTYPE === undefined) {
      $scope.isSearching = false;
      $scope.requestStatus = '查詢失敗, 查詢條件錯誤';
      alert("至少需要輸入一個欄位");
      return;
    };

    if (queryParam.to.getDate() - queryParam.from.getDate() > 7 && queryParam.OVENID === undefined && queryParam.OVENTYPE) {
      $scope.isSearching = false;
      $scope.requestStatus = '查詢失敗, 查詢條件錯誤';
      alert("搜尋OVENTYPE時間區間大於一週, 請重新設定");
      return;
    };

    queryParam.from = new Date(queryParam.from.getTime() + 8*60*60*1000 );
    queryParam.to = new Date(queryParam.to.getTime() + 8*60*60*1000 );    
    $scope.lotData.setQueryParams(queryParam);
    $scope.requestStatus = "等待 Server 回應...";

    $http
      .post('/api/pmb/getSearchResult', queryParam)
      .then(function(res){
        $scope.lotData.failData.data = _.cloneDeep(res.data.failData);
        $scope.lotData.displayFailData.data = res.data.failData;
        $scope.problemOvens = res.data.problemOvens;
        $scope.rowNum = res.data.dataLength;
        if (res.data.dataLength === 0) {
          $scope.rowNum = '0';
        };
        $scope.lotData.mainFailData.data = _.find(res.data.failData, {'groupKey': 'DATE'});
        $scope.showMainGraph = true;
        $scope.requestStatus = '查詢完成';
        $scope.isSearching = false;
        $scope.queryTime = new Date() - startTime;
      },function(err){
        $scope.showMainGraph = false;
        $scope.requestStatus = '查詢失敗, '+err.data
        $scope.isSearching = false;
        $scope.queryTime = new Date() - startTime;
        pmbService.clearLotFailData();
      });
  };

  $scope.redrawLotDetailGraph = function(){
    $scope.lotData.lotDetailData.isQuering = true;
    $timeout(() => { $scope.lotData.redrawLotDetailGraph(); }, 1000)
    $('#legendsSettingModal').modal('hide');   
  };

  $scope.clearSubGraph = function() {
    $scope.lotData.clearLotFailData();
    $scope.lotData.clearLotDetailData();
  };

  $scope.getProblemOvenInfo = function() {
    $http
      .post('/api/pmb/getProblemOvenInfo', {OVENIDS: $scope.problemOvens})
      .then(function(res){
        $scope.problemOvenInfo = res.data;
        console.log($scope.problemOvenInfo);
      });
  };
  
  //DEBUG
  //$scope.queryData();

  //exportXLS
  $scope.lotDetailGrid = {
    columnDefs: [],
    rowData:[],
    groupUseEntireRow: false,
    enableSorting: true,
    enableColResize: true,
    excelStyles: [{
      id: "greenBackground",
      interior: { color: "#aaffaa", pattern: 'Solid' }
    }]
  };

  $scope.exportXLS = function(){
    if ($scope.lotData.lotDetailData.name) {
      var colDefs = [
        {headerName: "時間", field: "DATETIME", width: 100}
          /*valueGetter: function(params){
            return params.data.DATETIME.toString('yyyy/MM/dd tt hh:mm:ss').replace('AM', '上午').replace('PM', '下午')
          }
        }*/
      ];
      _.forEach($scope.lotData.lotDetailData.legends, function(legend){
        colDefs.push({headerName: legend, field: legend, width: 100})
      });
      $scope.lotDetailGrid.api.setColumnDefs(colDefs);
      var copyData = _.cloneDeep($scope.lotData.lotDetailData.data)
      _.forEach(copyData, function(x){
        var tmp = new Date(x.DATETIME.replace('.000Z', ''));
        x.DATETIME = tmp.toLocaleString();
      })
      $scope.lotDetailGrid.api.setRowData(copyData);
      if ($scope.lotDetailGrid.api.getModel().getRowCount() < 1){
        $window.alert("無資料可供下載");
        return;
      };
      var params = {
        fileName: $scope.lotData.lotDetailData.name + "_電壓曲線Raw.xls"
      };
      $scope.lotDetailGrid.api.exportDataAsExcel(params);
    } else {
      var colDefs = [
        {headerName: "項目", field: "item", width: 100},
        {headerName: "Fail Count", field: "value", width: 100},
        {headerName: "Fail Rate", field: "failRate", width: 100}
      ];
      $scope.lotDetailGrid.api.setColumnDefs(colDefs);
      $scope.lotDetailGrid.api.setRowData($scope.lotData.lotFailData.data);
      if ($scope.lotDetailGrid.api.getModel().getRowCount() < 1){
        $window.alert("無資料可供下載");
        return;
      };
      var params = {
        fileName: $scope.lotData.lotFailData.name + "_DPS模組Raw.xls"
      };
      $scope.lotDetailGrid.api.exportDataAsExcel(params);
    }
  };
};

angular
  .module('mean.pmb')
  .controller('PmbController', PmbController);

PmbController.$inject = ['_', '$scope', '$http', 'srchPmbOpt', 'pmbService', '$timeout'];
