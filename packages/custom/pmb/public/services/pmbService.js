(function(){
  'use strict';
  function pmbService(_, $http){ 
    var failData = {
      'data': null
    };
    var displayFailData = {
      'data': null
    };
    var mainFailData = {
      'data': null
    };
    var lotFailData = {
      'name': false,
      'data': []
    };
    var lotDetailData = {
      'name': false,
      'data': [],
      'backupData': [],
      'legends': ['failCount', 'DPSIN1', 'DPSIN2', 'DPSIN3', 'DPSIN4', 'DPSIN5', 'DPSLTL', 'DPSUTL'],
      'limitlines': [
      ],
      'defaultLegends': [
        {'name': 'failCount', 'checked': true},
        {'name': 'DPSIN1', 'checked': true},
        {'name': 'DPSIN2', 'checked': true},
        {'name': 'DPSIN3', 'checked': true},
        {'name': 'DPSIN4', 'checked': true},
        {'name': 'DPSIN5', 'checked': true},
        {'name': 'DPSIN6', 'checked': false},
        {'name': 'DPSIN7', 'checked': false},
        {'name': 'DPSIN8', 'checked': false},
        {'name': 'DPSIN9', 'checked': false},
        {'name': 'DPSIN10', 'checked': false},
        {'name': 'DPSIN11', 'checked': false},
        {'name': 'DPSIN12', 'checked': false},
        {'name': 'DPSIN13', 'checked': false},
        {'name': 'DPSIN14', 'checked': false},
        {'name': 'DPSIN15', 'checked': false},
        {'name': 'DPSIN16', 'checked': false},
        {'name': 'ZB_Vplus', 'checked': false},
        {'name': 'ZB_Vminus', 'checked': false},
        {'name': 'ZB_5V', 'checked': false},
        {'name': 'OVEN_Vplus', 'checked': false},
        {'name': 'OVEN_Vminus', 'checked': false},
        {'name': 'ZONE0_5V', 'checked': false},
        {'name': 'ZONE1_5V', 'checked': false},
        {'name': 'ZONE2_5V', 'checked': false},
        {'name': 'ZONE3_5V', 'checked': false}
      ],
      'defaultLimitlines': [
        {'name': 'DPSLTL', 'checked': true},
        {'name': 'DPSUTL', 'checked': true},
        {'name': 'ZONELTL', 'checked': false, 'value': 4800},
        {'name': 'ZONEUTL', 'checked': false, 'value': 5800},
        {'name': 'OVEN_VplusLTL', 'checked': false, 'value': 14500},
        {'name': 'OVEN_VplusUTL', 'checked': false, 'value': 15500},
        {'name': 'OVEN_VminusLTL', 'checked': false, 'value': -5500},
        {'name': 'OVEN_VminusUTL', 'checked': false, 'value': -4500},
        {'name': 'ZB_5VLTL', 'checked': false, 'value': 4500},
        {'name': 'ZB_5VUTL', 'checked': false, 'value': 5500},
        {'name': 'ZB_VplusLTL', 'checked': false, 'value': 11500},
        {'name': 'ZB_VplusUTL', 'checked': false, 'value': 12500},
        {'name': 'ZB_VminusLTL', 'checked': false, 'value': -12500},
        {'name': 'ZB_VminusUTL', 'checked': false, 'value': -11500}
      ]
    };
    var queryParams = {};
    var drillDown = {
      message: false,
      groupKey: 'OVENID'
    };
    var originGroupTabList = [
      {'groupKey': 'DATE', 'active': false},
      {'groupKey': 'OVENTYPE', 'active': false},
      {'groupKey': 'OVENID', 'active': false},
      {'groupKey': 'LOTNO', 'active': false}
    ];
    var groupTabList = {
      list: [
        {'groupKey': 'DATE', 'active': true},
        {'groupKey': 'OVENTYPE', 'active': false},
        {'groupKey': 'OVENID', 'active': false},
        {'groupKey': 'LOTNO', 'active': false}
      ]
    };
    return {
      failData: failData,
      displayFailData: displayFailData,
      mainFailData: mainFailData,
      lotFailData: lotFailData,
      lotDetailData: lotDetailData,
      queryParams: queryParams,
      drillDown: drillDown,
      groupTabList: groupTabList,
      setTabList: function(select) {
        var newList = [];
        _.forEach(select, function(key){         
          var findResult = _.clone(_.find(originGroupTabList, {'groupKey': key}));
          if (key === drillDown.groupKey){
            findResult.active = true;
          };
          newList.push(findResult);
        });
        groupTabList.list = newList;
      },
      backToFailData: function() {
        displayFailData.data = _.cloneDeep(failData.data);
        mainFailData.data = _.find(failData.data, {'groupKey': drillDown.groupKey});
        drillDown.message = false;
        this.setTabList(_.map(originGroupTabList, 'groupKey'));
      },
      setQueryParams: function(data){
        queryParams = data;
      },
      setLotFailData: function(name, data){
        lotFailData.name = name;
        lotFailData.data = data;
      },
      setLotDetailData: function(name, ovenId){
        lotDetailData.name = name;
        lotDetailData.isQuering = true;
        lotDetailData.rowNum = false;
        lotDetailData.queryTime = false;
        lotDetailData.data = [];
        
        var query = {LOTNO: name, OVENID: ovenId};
        if (queryParams.from !== undefined) {
          query.DATETIME = {'$gte': queryParams.from, '$lte': queryParams.to};
        };
        var startTime = new Date();
        lotDetailData.requestStatus = "等待 Server 回應...";
        $http.post('/api/pmb/getLotDetail', query)
          .then(function(res){
            lotDetailData.isQuering = false;
            lotDetailData.data = res.data;
            lotDetailData.requestStatus = '查詢完成';
            lotDetailData.queryTime = new Date() - startTime;
            lotDetailData.rowNum = res.data.length.toString();
          },function(err){
            lotDetailData.requestStatus = '查詢失敗, '+err.data;
          });
      },
      redrawLotDetailGraph: function(){
        lotDetailData.data = lotDetailData.backupData;
        lotDetailData.backupData = [];
        lotDetailData.isQuering = false;
      },
      clearLotFailData: function(){
        lotFailData.name = false;
        lotFailData.data = [];
      },
      clearLotDetailData: function(){
        lotDetailData.name = false;
        lotDetailData.data = [];
      },     
      backupLotDetailData: function(){
        lotDetailData.backupData = lotDetailData.data;
        lotDetailData.data = [];
      },
      getLegends: function(){
        lotDetailData.legends = _.map(_.filter(lotDetailData.defaultLegends, {'checked': true}), 'name');
        var findDPSlimit = _.find(lotDetailData.defaultLimitlines, {'name': 'DPSLTL', 'checked': true});
        if (findDPSlimit) {
          lotDetailData.legends.push(findDPSlimit.name)
        };
        findDPSlimit = _.find(lotDetailData.defaultLimitlines, {'name': 'DPSUTL', 'checked': true});
        if (findDPSlimit) {
          lotDetailData.legends.push(findDPSlimit.name)
        };
      },
      getLimitlines: function(){
        lotDetailData.limitlines = [];
        _.forEach(_.filter(lotDetailData.defaultLimitlines, {'checked': true}), function(limit){
          if (limit.name !== 'DPSLTL' && limit.name !== 'DPSUTL') {
            lotDetailData.limitlines.push(limit);
          };
        });
      },
      clickGroup: function (groupKey){
        mainFailData.data = _.find(displayFailData.data, {'groupKey': groupKey});
        this.clearLotFailData();
        this.clearLotDetailData();
      },
    };
  };

  angular.module('mean.pmb').factory('pmbService', pmbService);
  pmbService.$inject = ['_', '$http'];
})();
