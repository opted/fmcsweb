(function(){
    'use strict';
    function srchPmbOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("OVENTYPE", "OVENTYPE", "string", false, true),
                uOpt.genField("OVENID", "OVENID", "string", false, true),
                uOpt.genField("內批(Lotno)", "LOTNO", "string", false, true)
            ],
            getMatch: uOpt.getMatch
        };
    };

    angular.module('mean.dutlog').factory('srchPmbOpt', srchPmbOpt);
    srchPmbOpt.$inject = ['utilsSrchOpt'];
})();
