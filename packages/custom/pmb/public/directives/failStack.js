(function(){
  'use strict';
  function lineBarChart(_, pmbService, $http){
    return {
      restrict: 'E',
      scope: {
        source: '=',
        nextType: '=',
      },
      template: '<div id="lineBarChart" style="min-width: 310px; height: 600px; margin: 0 auto"><div></div></div>',
      link: function (scope, element, attrs) {
        var chart = false;
        function genGraph () {
          var graphList = [];
          var defaultLegends = _.map(pmbService.lotDetailData.defaultLegends, 'name');
          _.pull(defaultLegends, 'failCount');
          _.forEach(scope.source.data[0], function(value, key){
            if (defaultLegends.indexOf(key) > -1) {
              var tmpGraph = {
                'valueAxis':'left',
                'balloonText': "[[category]]<br><b><span style='font-size:20px;'>" + key + ": [[value]]</span></b>",
                'alphaField': 'alpha',
                'fillAlphas': 1,
                'type': 'column',
                'valueField': key,
                'title': key,
                'labelFunction': function (item, text) {
                  if (text === "total") {
                    return item.dataContext.allFailRate.toFixed(2) + '%';
                  };
                }
              };
              graphList.push(tmpGraph);
            };
          });
          graphList.push({
            'valueAxis':'right',
            'balloonFunction': function(item, graph) {
              return  item.category + "<br><b><span style='font-size:14px;'>FailRate:" + 
                      item.values.value.toFixed(4) + "%</span></b>"
            },
            'bullet': 'round',
            'bulletSize': 7,
            'lineColor': '#d1655d',
            'lineThickness': 3,
            'bulletBorderAlpha': 1,
            'bulletColor': "#FFFFFF",
            'useLineColorForBulletBorder': true,
            'bulletBorderThickness': 3,
            'fillAlphas': 0,
            'lineAlpha': 1,
            'title': 'Fail %',
            'valueField': 'allFailRate'
          });
          return graphList;
        };
        var genChart = function() {
          if (chart) {
            chart.destroy();
          };
          var graphs = genGraph();
          var chartOption = {
            'type': 'serial',
            'theme': 'none',
            'marginLeft': 20,
            'dataProvider': scope.source.data,
            'legend': {
              'horizontalGap': 10,
              'maxColumns': 10,
              'position': 'top',
              'useGraphSettings': true,
              'markerSize': 10
            },
            'valueAxes': [{
              'id': 'left',
              'stackType': 'regular',
              'axisAlpha': 0.3,
              'gridAlpha': 0.15,
              'position': 'left',
              'title': 'Fail Count',
              'totalText': 'total',
              'integersOnly': true
            }, {
              'id': 'right',
              'axisAlpha': 0,
              'position': 'right',
              'unit': '%',
              'title': 'Fails(%)'
            }],
            'graphs': graphs,
            'chartScrollbar': {},
            'categoryField': scope.source.groupKey,
            'categoryAxis': {
              'gridPosition': 'start',
              'axisAlpha': 0,
              'gridAlpha': 0.15,
              'position': 'left',
              'labelRotation': 60
            }
          };
          var config = scope.config || {};
          var target = element[0].children[0];
          chart = AmCharts.makeChart(target, chartOption); 
        }// end of genChart function
        genChart();
        scope.$watch('source', function () {
          genChart();
          function clickItem(e){            
            if (scope.source.groupKey === 'LOTNO') {
              var tmp = [];
              var lotData = _.find(scope.source.data, {'LOTNO': e.item.category});
              _.forEach(lotData, function(value, key){
                if (key !== scope.source.groupKey && key.indexOf('Fail') < 0) {
                  tmp.push({'item': key, 'value': value, 'failRate': lotData[key+'FailRate']})
                };
              });

              var displayData = {
                'OVENID':_.find(pmbService.displayFailData.data, {'groupKey': 'OVENID'}).data,
              };
              var ovenId;            
              _.forEach(displayData.OVENID, function(group){
                if (group.downList.indexOf(e.item.category) > -1) {
                  ovenId = group.OVENID;
                };
              })

              if (scope.nextType.type === 'd') {
                pmbService.setLotFailData(e.item.category, tmp);
              } else {
                pmbService.setLotDetailData(e.item.category, ovenId);
              };
            } else if (scope.source.groupKey === 'OVENID') {
              var filter = {};
              filter[scope.source.groupKey] = e.item.category;
              var downList = _.find(scope.source.data, filter).downList;
              _.forEach(pmbService.displayFailData.data, function(group){
                if (group.groupKey === 'OVENID') {
                  group.data = _.filter(group.data, filter);
                } else if (group.groupKey === 'LOTNO') {
                  var tmp = [];
                  _.forEach(downList, function(key){
                    tmp.push.apply(tmp, _.filter(group.data, {'LOTNO': key}))
                  });
                  group.data = tmp;
                  pmbService.mainFailData.data = {'groupKey': 'LOTNO', 'data': tmp};
                } else {
                  group.data = [];
                };
              });
              pmbService.drillDown.groupKey = 'LOTNO';
              pmbService.setTabList(['OVENID', 'LOTNO']);
              if (pmbService.drillDown.message === false) {
                pmbService.drillDown.message = ' [OVENID: ' + e.item.category + ' ]';
              } else {
                pmbService.drillDown.message += ' -> [ OVENID: ' + e.item.category + ' ]';
              };
            } else if (scope.source.groupKey === 'OVENTYPE') {
              var filter = {};
              filter[scope.source.groupKey] = e.item.category;
              var downList = _.find(scope.source.data, filter).downList;

              var displayData = {
                'DATE': _.find(pmbService.displayFailData.data, {'groupKey': 'DATE'}).data,
                'OVENTYPE': _.find(pmbService.displayFailData.data, {'groupKey': 'OVENTYPE'}).data,
                'OVENID':_.find(pmbService.displayFailData.data, {'groupKey': 'OVENID'}).data,
                'LOTNO':_.find(pmbService.displayFailData.data, {'groupKey': 'LOTNO'}).data
              };

              pmbService.mainFailData.data = {'groupKey': 'OVENID', 'data': displayData.OVENID};
              
              var lotDownList = []
              _.forEach(['DATE', 'OVENTYPE', 'OVENID', 'LOTNO'], function(groupKey){
                if (groupKey === 'OVENTYPE'){
                  displayData.OVENTYPE = _.filter(displayData.OVENTYPE, filter);
                } else if (groupKey === 'OVENID'){
                  var tmp = [];
                  _.forEach(downList, function(key){
                    tmp.push.apply(tmp, _.filter(displayData.OVENID, {'OVENID': key}))
                    lotDownList.push.apply(lotDownList, _.flatten(_.map(_.filter(displayData.OVENID, {'OVENID': key}), 'downList')));
                  });
                  lotDownList = _.uniq(lotDownList);
                  displayData.OVENID = tmp;
                } else if (groupKey === 'LOTNO') {
                  var tmp = [];
                  _.forEach(lotDownList, function(key){
                    tmp.push.apply(tmp, _.filter(displayData.LOTNO, {'LOTNO': key}))
                  });
                  displayData.LOTNO = tmp;
                } else {
                  displayData[groupKey] = [];
                }
              });
              pmbService.displayFailData.data = [
                { 'groupKey': 'DATE', 'data': displayData.DATE },
                { 'groupKey': 'OVENTYPE', 'data': displayData.OVENTYPE },
                { 'groupKey': 'OVENID', 'data': displayData.OVENID },
                { 'groupKey': 'LOTNO', 'data': displayData.LOTNO }
              ];
              pmbService.drillDown.groupKey = 'OVENID';
              pmbService.setTabList(['OVENTYPE', 'OVENID', 'LOTNO']);
              if (pmbService.drillDown.message === false) {
                pmbService.drillDown.message = ' [ OVENTYPE: ' + e.item.category + ' ]';
              } else {
                pmbService.drillDown.message += ' -> [ OVENTYPE: ' + e.item.category + ' ]';
              };
            };
          };
          chart.addListener("clickGraphItem",clickItem);
        });
      }//end watch
    } 
  };

  angular.module('mean.pmb').directive('failStack', lineBarChart);
  lineBarChart.$inject = ['_', 'pmbService', '$http'];
})();
