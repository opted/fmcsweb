(function(){
  'use strict';
  function lineBarChart(_, pmbService){
    return {
      restrict: 'E',
      scope: {
        source: '='
      },
      template:'<div id="lineBarChart" style="min-width: 310px; height: 600px; margin: 0 auto"><div></div></div>',
      link: function (scope, element, attrs) {
        var chart = false;
        var genChart = function() {
          if (chart) {
            chart.destroy();
          };
          var chartOption = {
            'type': 'serial',
            'theme': 'none',
            'marginLeft': 20,
            'dataProvider': scope.source,
            'legend': {
              'horizontalGap': 10,
              'maxColumns': 10,
              'position': 'top',
              'useGraphSettings': true,
              'markerSize': 10
            },
            'valueAxes': [{
              'id': 'left',
              'stackType': 'regular',
              'axisAlpha': 0.3,
              'gridAlpha': 0,
              'position': 'left',
              'title': 'Fail Count',
              'integersOnly': true
            }, {
              'id': 'right',
              'axisAlpha': 0,
              'position': 'right',
              'unit': '%',
              'title': 'Fails(%)'
            }],
            'graphs': [{
              'valueAxis':'left',
              'balloonText': "[[category]]<br><b><span style='font-size:14px;'>FailCount: [[value]]</span></b>",
              'alphaField': 'alpha',
              'fillAlphas': 1,
              'type': 'column',
              'lineColor': '#0D8ECF',
              'valueField': 'value',
              'title': 'Fail Count',
            }, {
              'valueAxis':'right',
              'balloonFunction': function(item, graph) {
                return  item.category + "<br><b><span style='font-size:14px;'>FailRate:" + 
                        item.values.value.toFixed(4) + "%</span></b>"
              },
              'bullet': 'round',
              'bulletSize': 7,
              'lineColor': '#d1655d',
              'lineThickness': 3,
              'bulletBorderAlpha': 1,
              'bulletColor': "#FFFFFF",
              'useLineColorForBulletBorder': true,
              'bulletBorderThickness': 3,
              'fillAlphas': 0,
              'lineAlpha': 1,
              'title': 'Fail %',
              'valueField': 'failRate'
            }],
            'chartScrollbar': {},
            'categoryField': 'item',
            'categoryAxis': {
              'gridPosition': 'start',
              'axisAlpha': 0,
              'gridAlpha': 0,
              'position': 'left',
              'labelRotation': 60
            }
          };
          var config = scope.config || {};
          var target = element[0].children[0];
          chart = AmCharts.makeChart(target, chartOption); 
        }// end of genChart function
        genChart();
        scope.$watch('source', function () {
          genChart();
        });
      }//end watch
    } 
  };

  angular.module('mean.pmb').directive('lotFailStack', lineBarChart);
  lineBarChart.$inject = ['_', 'pmbService'];
})();
