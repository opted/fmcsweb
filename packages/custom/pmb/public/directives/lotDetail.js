(function(){
  'use strict';
  function lineBarChart(_, pmbService){
    return {
      restrict: 'E',
      scope: {
        source: '=',
        legends: '='
      },
      template:'<div id="lineBarChart" style="min-width: 310px; height: 600px; margin: 0 auto"><div></div></div>',
      link: function (scope, element, attrs) {
        var chart = false;
        function getMaxFailCount(data){
          var max = _.max(_.map(data, 'failCount'));
          return max <= 2 ? 3 : max;
        };
 
        function genGuideLine(){
          var guideLineList = [];
          var lines = pmbService.lotDetailData.limitlines;
          _.forEach(lines, function(line){
            var config = {
              'value': line.value,
              'label': line.name + ': ' + line.value,
              'labelRotation':90,
              'inside': true,
              'lineAlpha': 0.7,
              'lineColor': 'black',
              'color': 'black',
              'lineThickness': 2,
              'position': 'left',
              'dashLength' : 5
            };
            if (line.name.indexOf('LTL') > 0) {
              config.position = 'right';
            };
            guideLineList.push(config);
          });
          return guideLineList;
        }
        function genGraph(data){
          var groupList = [];
          //var keys = scope.legends ? scope.legends : _.keys(data[0]);
          var keys = pmbService.lotDetailData.legends;
          _.forEach(keys, function(key){
            if (key === 'failCount') {
              groupList.push({
                'valueAxis':'right',
                'alphaField': 'alpha',
                'fillAlphas': 1,
                'type': 'column',
                'valueField': 'failCount',
                'lineColor': '#4F80B7',
                'title': 'Fail Count',
                'showBalloon': false
              });
            } else if (key !== 'DATETIME' && key !== 'DC' && key !== 'failCount') {
              var tmpConfig = {
                'valueAxis':'left',
                'lineThickness': 4,
                'fillAlphas': 0,
                'lineAlpha': 1,
                'title': key,
                'valueField': key,
                'showBalloon': false
              };
              if (key === 'DPSUTL' || key === 'DPSLTL') {
                tmpConfig.lineColor = 'red';
                tmpConfig.dashLength = 5;
                tmpConfig.lineThickness = 2;
              };
              groupList.push(tmpConfig)
            };
          });
          return groupList;
        };
        var genChart = function() {
          if (chart) {
            chart.destroy();
          };         
          var chartOption = {
            'type': 'serial',
            'theme': 'none',
            'marginLeft': 20,
            'dataProvider': scope.source,
            'legend': {
              'horizontalGap': 10,
              'maxColumns': 10,
              'position': 'top',
              'useGraphSettings': true,
              'markerSize': 10
            },
            'valueAxes': [{
              'id': 'left',
              'axisAlpha': 0,
              'position': 'left',
              'title': 'mV',
              'balloon': { 'enabled': false },
              'integersOnly': true,
              'guides': genGuideLine()
            }, {
              'id': 'right',
              'stackType': 'regular',
              'axisAlpha': 0.3,
              'gridAlpha': 0,
              'position': 'right',
              'title': 'Fail Count',
              'maximum': getMaxFailCount(scope.source),
              'balloon': { 'enabled': false },
              'integersOnly': true
            }],
            'graphs': genGraph(scope.source),
            'categoryField': 'DATETIME',
            'categoryAxis': {
              'gridPosition': 'start',
              'axisAlpha': 0,
              'gridAlpha': 0,
              'position': 'left',
              'labelRotation': 60,
              'categoryFunction': function (category, dataItem, categoryAxis) {
                return category.replace('\.000Z', '').replace('T', ' ');
              },
              'balloon': { 'enabled': false },
            },
            "chartCursor": {
              "cursorPosition": "mouse"
            }
          };
          var config = scope.config || {};
          var target = element[0].children[0];
          chart = AmCharts.makeChart(target, chartOption); 
        }// end of genChart function
        genChart();
        /*chart.addListener("zoomed", function(event) {
          if (chart.ignoreZoomed) {
            chart.ignoreZoomed = false;
            return;
          };
          chart.zoomStartDate = event.startDate;
          chart.zoomEndDate = event.endDate;
        });*/
        scope.$watch('source', function () {
          genChart();
        }, true);
      }//end watch
    } 
  };

  angular.module('mean.pmb').directive('lotDetailStack', lineBarChart);
  lineBarChart.$inject = ['_', 'pmbService'];
})();
