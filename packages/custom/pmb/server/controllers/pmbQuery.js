'use strict'
var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');

function getAllFailRate(allCount, failCount){
  var rateList = [];
  var failRate = {}
  _.forEach(_.keys(allCount), function(key){
    if (allCount[key] === 0) {
      console.log('No Data at compute getAllFailRate.');
    } else {
      var rate = failCount[key]/allCount[key];
      rateList.push(rate);
      failRate[key+'FailRate'] = rate*100;
    };
  });
  failRate['allFailRate'] = _.sum(rateList)/rateList.length*100;
  return failRate;
};

function getFullDateText(date) {
  return date.getUTCFullYear() + '/' + (date.getUTCMonth()+1) + '/' + date.getUTCDate();
};

function reduceDClog(DcData) {
  if (DcData.length < 1) {
    return [];
  };
  var reducedData = [];
  var dpsin;
  var i;
  for (i = 0; i < DcData.length; i++) {
    if (i === 0 || dpsin !== DcData[i].DPSIN) {
      dpsin = DcData[i].DPSIN;
      reducedData.push(DcData[i]);
    };
  };
  return reducedData;
};

function getDefaultVoltageItem(type) {
  if (type === 'counter') {
    return {
      'DPSIN1' : 0,
      'DPSIN2' : 0,
      'DPSIN3' : 0,
      'DPSIN4' : 0,
      'DPSIN5' : 0,
      'DPSIN6' : 0,
      'DPSIN7' : 0,
      'DPSIN8' : 0,
      'DPSIN9' : 0,
      'DPSIN10' : 0,
      'DPSIN11' : 0,
      'DPSIN12' : 0,
      'DPSIN13' : 0,
      'DPSIN14' : 0,
      'DPSIN15' : 0,
      'DPSIN16' : 0,
      'ZB_Vplus' : 0,
      'ZB_Vminus' : 0,
      'ZB_5V' : 0,
      'OVEN_Vplus' : 0,
      'OVEN_Vminus' : 0,
      'ZONE0_5V' : 0,
      'ZONE1_5V' : 0,
      'ZONE2_5V' : 0,
      'ZONE3_5V' : 0
    };
  } else if (type === 'limiter') {
    return {
      'ZONE0_5V' : {'UTL': 5800, 'LTL': 4800},
      'ZONE1_5V' : {'UTL': 5800, 'LTL': 4800},
      'ZONE2_5V' : {'UTL': 5800, 'LTL': 4800},
      'ZONE3_5V' : {'UTL': 5800, 'LTL': 4800},
      'OVEN_Vplus' : {'UTL': 15500, 'LTL': 14500},
      'OVEN_Vminus' : {'UTL': -4500, 'LTL': -5500},
      'ZB_5V' : {'UTL': 5500, 'LTL': 4500},
      'ZB_Vplus' : {'UTL': 12500, 'LTL': 11500},
      'ZB_Vminus' : {'UTL': -11500, 'LTL': -12500}
    };
    
  } else {
    return;
  }
};

function getOvenInfoFromVoltage(lotno) {
  var deferred = Q.defer();
  var Voltage = mongoose.model('VOLTAGE');
  var match = {'$match': { 'LOTNO': lotno}};
  var sort =  {'$sort' : { 'DATETIME': 1 }};
  var group = {'$group': { '_id': {'LOTNO': '$LOTNO'}, 'START': {'$first': '$DATETIME'},
                           'END': {'$last': '$DATETIME'}, 'OVENID': {'$first': '$OVENID'}}};
  var project = {'$project': { 'START': 1, 'END': 1, 'OVENID':1}};
  Voltage.aggregate([match, sort, group, project])
    .then(function(result){
      deferred.resolve(result[0]);
    })
    .then(null, function(err){ 
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

function getDcData(req) {
  var deferred = Q.defer();
  var DC = mongoose.model('DCLOG');
  var dcMatch = {};
  var dcFilter = {'DATETIME':1, 'DPSIN':1, 'OVENID':1};
  if (req.LOTNO && req.OVENID === undefined) {
    getOvenInfoFromVoltage(req.LOTNO)
      .then(function(info){
        if (info === undefined) {
          deferred.resolve([]);
        };
        dcMatch.OVENID = info['OVENID'];        
        dcMatch.DATETIME = {'$gte': info['START'], '$lte': info['END']};
        DC.find(dcMatch, dcFilter).sort({'DATETIME':1})
          .then(function(result){
            deferred.resolve(reduceDClog(result));
          })
          .then(null, function(err){ 
            console.log('DCfind error:', err);
            deferred.reject(err);
          });
      })
      .then(null, function(err){
        console.log('getOvenInfoFromVoltage error:', err);
      });
  } else {
    if (req.OVENID) {
      dcMatch.OVENID = req.OVENID;
    } else if (req.OVENTYPE) {
      dcMatch.OVENID = {'$regex': req.OVENTYPE};
    };
    if (req.DATETIME) {
      dcMatch['DATETIME'] = req.DATETIME;
    };
    DC.find(dcMatch, dcFilter).sort({'DATETIME':1})
      .then(function(result){
        deferred.resolve(reduceDClog(result));
      })
      .then(null, function(err){ 
        console.log('DCfind2 error:', err);
        deferred.reject(err);
      });
  };
  return deferred.promise;
};

//檢查失效的項目
function computeFailData (voltageData, dcData) {
  var deferred = Q.defer();
  var failData = [];
  if (_.isArray(dcData)) {
    dcData = _.groupBy(dcData, 'OVENID');
  };
  var dcIndex = {};
  _.forEach(_.keys(dcData), function(key){
    dcIndex[key] = 0;
  });

  _.forEach(voltageData, function(voltage){
    var ovenId = voltage.OVENID;
    if (dcData[ovenId] === undefined) {
      console.log(ovenId, 'not have dcData at');
      return;
    };
    if (dcIndex[ovenId] < dcData[ovenId].length-1  && voltage.DATETIME > dcData[ovenId][dcIndex[ovenId]+1].DATETIME ) {
      dcIndex[ovenId]+=1;
    };
    var dpsin = dcData[ovenId][dcIndex[ovenId]].DPSIN;
    var tmpFailData = {
      DATETIME: voltage.DATETIME,
      DATE: getFullDateText(voltage.DATETIME),
      LOTNO: voltage.LOTNO,
      OVENID: voltage.OVENID,
      OVENTYPE: voltage.OVENTYPE,
      DC: dpsin
    };

    //處理對不到LOTNO的原始資料
    if (tmpFailData.LOTNO === undefined) {
      return;
    };

    var limiter = getDefaultVoltageItem('limiter');
    _.forEach(_.keys(getDefaultVoltageItem('counter')), function(key){
      if (key.indexOf('DPSIN') > -1) {
        if (voltage[key] > (dpsin*1000 + 600) || voltage[key] < (dpsin*1000 - 600)) {
          tmpFailData[key] = 1;
        } else {
          tmpFailData[key] = 0;
        };
      } else {
        if (voltage[key] > limiter[key].UTL || voltage[key] < limiter[key].LTL) {
          tmpFailData[key] = 1;
        } else {
          tmpFailData[key] = 0;
        };
      };
    });
    failData.push(tmpFailData);
  });

  //計算四種group下的Fail總數及Fail%
  var failCountByGroup = [];
  var groupKeyList = ['DATE', 'OVENTYPE', 'OVENID', 'LOTNO'];
  _.forEach(groupKeyList, function(groupKey){
    var tmpGroup = [];
    if (groupKey === 'OVENID' || groupKey === 'LOTNO') {
      _.forEach(_.groupBy(failData, groupKey), function(groupData, groupName){
        var allCount = getDefaultVoltageItem('counter');
        var failCount = getDefaultVoltageItem('counter');
        var tmpCount = getDefaultVoltageItem('counter');
        failCount.downList = [];
        _.forEach(groupData, function(data){
          _.forEach(_.keys(getDefaultVoltageItem('counter')), function(key){
            allCount[key] += 1;
            if (data[key] === 1) {
              tmpCount[key] += 1;
              if (tmpCount[key] === 6) {
                allCount[key] -= 5;
                failCount[key] += 1;
                tmpCount[key] = 0;
              };
            } else {
              tmpCount[key] = 0;
            };
          });
          if (groupKey === 'OVENID') {
            failCount.downList.push(data.LOTNO);
          };
        });
        failCount[groupKey] = groupName;
        if (groupKey === 'OVENID') {
          failCount.downList = _.uniq(failCount.downList);
        };
        _.assign(failCount, getAllFailRate(allCount, failCount))
        tmpGroup.push(failCount);
      });
    } else { 
      _.forEach(_.groupBy(failData, groupKey), function(groupData, groupName){
        var ovenGroup = [];
        _.forEach(_.groupBy(groupData, 'OVENID'), function(groupData, groupName){
          var allCount = getDefaultVoltageItem('counter');
          var failCount = getDefaultVoltageItem('counter');
          var tmpCount = getDefaultVoltageItem('counter');
          if (groupKey === 'OVENTYPE') {
            failCount.ovenId = groupName;
          };
          _.forEach(groupData, function(data){
            _.forEach(_.keys(getDefaultVoltageItem('counter')), function(key){
              allCount[key] += 1;
              if (data[key] === 1) {
                tmpCount[key] += 1;
                if (tmpCount[key] === 6) {
                  allCount[key] -= 5;
                  failCount[key] += 1;
                  tmpCount[key] = 0;
                };
              } else {
                tmpCount[key] = 0;
              };
            });
          });
          failCount['allFailRate'] = getAllFailRate(allCount, failCount)['allFailRate'];
          ovenGroup.push(failCount);
        });
        var ovenNum = ovenGroup.length;
        if (ovenNum > 0) {
          var ovenSum = {};
          _.forEach(_.keys(ovenGroup[0]), function(key){
            if (key === 'ovenId'){
              ovenSum.downList = _.union(_.map(ovenGroup, key));
            } else {
              ovenSum[key] = _.sumBy(ovenGroup,key);
            };
          });
          ovenSum['allFailRate'] = ovenSum['allFailRate']/ovenNum;
          ovenSum[groupKey] = groupName;
          tmpGroup.push(ovenSum);
        };
      });
    }
    failCountByGroup.push({groupKey: groupKey , data: tmpGroup});
  });
  deferred.resolve(failCountByGroup);
  return deferred.promise;
};

function getDiffOvens(voltageData, dcData) {
  var deferred = Q.defer();
  var dcOvens = _.uniq(_.map(dcData, 'OVENID'));
  var volOvens = _.uniq(_.map(voltageData, 'OVENID'));
  var diff = _.difference(dcOvens, volOvens);
  diff.push.apply(diff, _.difference(volOvens, dcOvens))
  deferred.resolve(diff);
  return deferred.promise;
};

exports.getBackEndResponse = function(req) {
  var deferred = Q.defer();
  console.log('test query from:', req.packageName, 'package');
  deferred.resolve();
  return deferred.promise;
};

exports.getSearchResult = function(req) {
  var deferred = Q.defer(); 
  var Voltage = mongoose.model('VOLTAGE');
  var queryList = [];
  if (req.from !== undefined && req.to !== undefined) {
    req.DATETIME = {'$gte': req.from, '$lte': req.to}
    delete req.from;
    delete req.to;
  };
  queryList.push(Voltage.find(req, {VCI: 0}).sort({'DATETIME':1}).exec());
  queryList.push(getDcData(req));
  Q.all(queryList)
    .then(function(result){
      var voltageLength = result[0].length;
      Q.all([computeFailData(result[0], result[1]), getDiffOvens(result[0], result[1])])
        .then(function(result){
          var results = {};
          results.failData = result[0];
          results.problemOvens = result[1];
          results.dataLength = voltageLength;
          deferred.resolve(results);
        })
        .then(null, function(err){ 
          console.log(err);
          deferred.reject(err);
        });
    })
    .then(null, function(err){ 
      console.log(err);
      deferred.reject(err);
    });  
  return deferred.promise;
};

exports.getLotDetail = function(req) {
  var deferred = Q.defer();
  var queryList = []; 
  var Voltage = mongoose.model('VOLTAGE');  
  var filter = {EXP_DATE:0, VCI:0, _id:0, FILEPATH:0, LOTNO:0, OVENTYPE:0};
  queryList.push(Voltage.find(req, filter).exec());
  queryList.push(getDcData(req));
  Q.all(queryList)
    .then(function(result){
      var dcData = _.groupBy(result[1], 'OVENID');
      var failData = [];     
      var dcIndex = {};
      _.forEach(_.keys(dcData), function(key){
        dcIndex[key] = 0;
      });
    
      _.forEach(result[0], function(voltage){
        var ovenId = voltage.OVENID;
        if (dcData[ovenId] === undefined) { 
          console.log(ovenId, 'not have dcData at');
          return false;
        };
        if (dcIndex[ovenId] < dcData[ovenId].length-1  && voltage.DATETIME > dcData[ovenId][dcIndex[ovenId]+1].DATETIME ) {
          dcIndex[ovenId]+=1;
        };
        var dpsin = dcData[ovenId][dcIndex[ovenId]].DPSIN;
        var tmpFailData = {
          DATETIME: voltage.DATETIME,
          DC: dpsin,
          failCount: 0
        };
        var limiter = getDefaultVoltageItem('limiter');
        _.forEach(_.keys(getDefaultVoltageItem('counter')), function(key){
          tmpFailData[key] = voltage[key];
          tmpFailData['DPSUTL'] = dpsin*1000 + 600;
          tmpFailData['DPSLTL'] = dpsin*1000 - 600 ;
          if (key.indexOf('DPSIN') > -1) {
            if (voltage[key] > (dpsin*1000 + 600) || voltage[key] < (dpsin*1000 - 600)) {
              tmpFailData.failCount += 1;
            };
          } else {
            if (voltage[key] > limiter[key].UTL || voltage[key] < limiter[key].LTL) {
              tmpFailData.failCount += 1;
            };
          };
        });
        failData.push(tmpFailData);
      });
      deferred.resolve(failData);
    })
    .then(null, function(err){ 
      console.log(err);
      deferred.reject(err);
    });  
  return deferred.promise;
};

exports.getProblemOvenInfo = function (req){
  var deferred = Q.defer();
  var Voltage = mongoose.model('VOLTAGE');
  var match = {'$match': { 'OVENID':{ '$in': req.OVENIDS }}};
  var group = {'$group': { '_id': { 'OVENID': '$OVENID' }, 'DATETIME': { '$last': '$DATETIME' }}}
  var project = {'$project': { 'OVENID': '$_id.OVENID', 'DATETIME':1, '_id':0 }}
  Voltage.aggregate([match, group, project])
    .then(function(ovenInfo){
      var results = [];
      _.forEach(req.OVENIDS, function(ovenId){
        var findResult = _.find(ovenInfo, {'OVENID': ovenId});
        if (findResult === undefined) {
          results.push({OVENID: ovenId, INFO: '此爐號無Voltagelog資料'})
        } else {
          //轉換DB拿出來的時間(拿掉時區, 維持原本的數值)
          var d1 = findResult.DATETIME
          var d2 = new Date( d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate(), d1.getUTCHours(),
                             d1.getUTCMinutes(), d1.getUTCSeconds());
          var timeRange = (new Date() - d2)/(1000*60*60*24)
          if (timeRange > 7) {
            results.push({OVENID: ovenId, INFO: 'Voltagelog大於一週沒有資料', DATETIME: findResult.DATETIME})
          } else {
            results.push({OVENID: ovenId, INFO: '選擇的時間區內, DClog無資料或是Voltagelog無資料', DATETIME: findResult.DATETIME})
          };
        };
      })
      deferred.resolve(results);
    })
    .then(null, function(err){ 
      console.log(err);
      deferred.reject(err);
    });  
  return deferred.promise;
}
