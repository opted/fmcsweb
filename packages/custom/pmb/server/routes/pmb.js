(function() {
  'use strict';
  module.exports = function(Pmb, app, auth, database, utils) {
    var mongoose = require('mongoose');
    var pmbQuery = require('../controllers/pmbQuery');
    var _ = require('lodash');

    app.post('/api/pmb/getSearchField', function(req, res, next){
      req.setTimeout(1800000); // 30 min
      var pathMapping = {
        'OVENID': 'OVENID',
        'OVENTYPE': 'OVENTYPE',
        'LOTNO': 'LOTNO'
      };
      utils.ctrls.mongoQuery.getSearchField(mongoose.model('VOLTAGE'), req.body, pathMapping, [])
        .then((data) => res.json(data))
        .catch((err) => res.status(500).send({'error': err}));
    });

    app.post('/api/pmb/getProblemOvenInfo', function(req, res, next){
      pmbQuery
        .getProblemOvenInfo(req.body)
        .then(function(result){
          res.json(result);
        }, function(err){
          res.status(500).send(err);
        });
    })

    app.post('/api/pmb/getSearchResult', function(req, res, next){
      req.setTimeout(1800000); // 30 min
      pmbQuery
        .getSearchResult(req.body)
        .then(function(result){
          res.json(result);
        }, function(err){
          res.status(500).send(err);
        });
    });   

    app.post('/api/pmb/getLotDetail', function(req, res, next){
      pmbQuery
        .getLotDetail(req.body)
        .then(function(result){
          res.json(result);
        }, function(err){
          res.status(500).send(err);
        });
    });   

    app.post('/api/pmb/getBackEndResponse', function(req, res, next){
      pmbQuery
        .getBackEndResponse(req.body)
        .then(function(){
          res.sendStatus(201);
        }, function(err){
          res.status(500).send(err);
        });
    });
  };
})();
