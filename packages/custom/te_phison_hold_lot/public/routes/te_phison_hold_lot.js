(function() {
  'use strict';

  function Te_phison_hold_lot($stateProvider) {
    $stateProvider.state('te_phison_hold_lot main page', {
      url: '/te_phison_hold_lot',
      templateUrl: 'te_phison_hold_lot/views/index.html',
      controller:Te_phison_hold_lotController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.te_phison_hold_lot')
    .config(Te_phison_hold_lot);

  Te_phison_hold_lot.$inject = ['$stateProvider'];

})();
