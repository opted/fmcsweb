'use strict';

function Te_phison_hold_lotController(_, $scope,$http) {
  $scope.packageName = 'TE_Phison Hold Lot';
  var colDefs = [
    {headerName: "Lot No", field: "INCOMINGNO", width: 100, cellClass: "textFormat"},
    {headerName: "ProductFamily", field: "FAMILY", width: 150, cellClass: "textFormat"},
    {headerName: "內批", field: "LOTNO", width: 100, cellClass: "textFormat"},
    {headerName: "Hold Time", field: "CREATEDATE", width: 180},
    {headerName: "處置方式", field: "HOLDDISPOSITION", width: 300, cellClass: "textFormat"},
    {headerName: "REVISEDATE", field: "REVISEDATE", width: 180},
    {headerName: "Qty", field: "DIEQTY", width: 100},
    {headerName: "BIN2", field: "BIN2", width: 70},
    {headerName: "BIN3", field: "BIN3", width: 70},
    {headerName: "BIN4", field: "BIN4", width: 70},
    {headerName: "BIN5", field: "BIN5", width: 70},
    {headerName: "BIN6", field: "BIN6", width: 70},
    {headerName: "BIN7", field: "BIN7", width: 70},
    {headerName: "BIN8", field: "BIN8", width: 70}
  ];

  $scope.gridOption = {
    columnDefs: colDefs,
    rowData:[],
    groupUseEntireRow: false,
    enableSorting: true,
    enableColResize: true,
    excelStyles: [
      {
        id: "greenBackground",
        interior: {
          color: "#aaffaa", pattern: 'Solid'
        }
      }, {
        id: "textFormat",
        dataType: "string"
      }
    ]
  };
  $scope.dateRanges = getDateRangeList(4);
  $scope.selectedRange = $scope.dateRanges[0];
  getDataByRange($scope.selectedRange.FROM, $scope.selectedRange.TO)

  $scope.selectRange = function(range) {
    $scope.selectedRange = range;
    getDataByRange(range.FROM, range.TO);
  };

  $scope.exportXLS = function(){
    var ws_name = "京元內批";
    var wb = new Workbook();
    var output = [];
    var convertMap = {
      'INCOMINGNO': 'Lot No',
      'FAMILY': 'ProductFamily',
      'LOTNO': '內批',
      'CREATEDATE': 'Hold Time',
      'HOLDDISPOSITION': '處置方式',
      'DIEQTY': 'Qty',
      'REVISEDATE': 'DELETE'
    };
    var binMap = {};
    _.forEach($scope.rawData.data, function(doc){
      var tmp = {};
      _.forEach(doc, function(value, key){        
        if ( convertMap[key] === 'DELETE' ) {
          //Do nothing
        } else if ( convertMap[key] !== undefined ) {
          tmp[convertMap[key]] = value;
        } else {
          binMap[key] = true;
          tmp[key] = value;
        };
      });
      output.push(tmp);
    });
    var header = ['Lot No', 'ProductFamily', '內批', 'Hold Time', '處置方式', 'Qty'];
    header = _.concat(header, _.sortBy(_.keys(binMap)))
    header.push('STATUS')
    var ws = XLSX.utils.json_to_sheet(output, {header: header});
    var wscols = [
      {wch:15},
      {wch:25},
      {wch:15},
      {wch:25},
      {wch:25}
    ];
    ws['!cols'] = wscols;
    wb.SheetNames.push(ws_name);
    wb.Sheets[ws_name] = ws;

    var convertMap = {
      'CUSTOMLOT': 'CUSTOM LOT',
      'LOTNO': '內批',
      'COORDINATE': '座標'
    };
    _.forEach($scope.rawData.bibList, function(data, bibId){
      wb.SheetNames.push(bibId);
      var convertedData = [];
      var sum = {'COORDINATE': []};
      _.forEach(data, function(doc){
        var tmp = {};
        _.forEach(doc, function(value, key){
          if (convertMap[key] !== undefined) {
            tmp[convertMap[key]] = value;
          } else {
            tmp[key] = value;
          };
        });
        convertedData.push(tmp);
      });
      var header = ['CUSTOM LOT', '內批', '座標', ' '];
      var wscols = [{wch:20}, {wch:20}, {wch:20}, {wch:2}];
      _.forEach(_.range(1,9), function(x){
        _.forEach(_.range(1,11), function(y){
          var v;
          if (y === 10) {
            v = x*10;
          } else {
            v = x*10 + y;
          };
          header.push(v.toString())
          wscols.push({wch:2});

          if (data.length > 1) {
            var sumValue = _.sumBy(data, v);
            if (sumValue !== undefined){
              sum[v] = sumValue;
              sum.COORDINATE.push(v)
            };
          };

        });
      });
      if (data.length > 1) {
        sum['座標'] = sum.COORDINATE.join(',');
        delete sum.COORDINATE;        
        convertedData.push({});
        convertedData.push(sum);
      };
      var ws = XLSX.utils.json_to_sheet(convertedData, {header: header});
      ws['!cols'] = wscols;
      wb.Sheets[bibId] = ws;
    });

    $http
      .post('/api/te_phison_hold_lot/getBibGuide')
      .then(function(res){
        var shName = res.data.SheetNames[0];
        wb.SheetNames.splice(1,0,shName);
        wb.Sheets[shName] = res.data.Sheets[shName];
        var wscols = [
          {wch:10},
          {wch:10},
          {wch:30},
        ];
        wb.Sheets[shName]['!cols'] = wscols;
        var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});
        function s2ab(s) {
          var buf = new ArrayBuffer(s.length);
          var view = new Uint8Array(buf);
          for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
          return buf;
        };
        var from = new Date($scope.selectedRange.FROM);
        var to = new Date($scope.selectedRange.TO);
        var fileName = from.toLocaleDateString().replace(/\//g,'-') + '_' + to.toLocaleDateString().replace(/\//g,'-') +
                       '_PHISON_HOLD_LOT_ex.xlsx';
        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), fileName)
      })
  };

  function Workbook() {
    if(!(this instanceof Workbook)) return new Workbook();
    this.SheetNames = [];
    this.Sheets = {};
  };

  function getDataByRange(from, to){
    $http
      .post('/api/te_phison_hold_lot/getReportByRange', {FROM: from, TO: to})
      .then(function(res){
        $scope.gridOption.api.setRowData([]);
        res.data.data = _.sortBy(res.data.data, 'CREATEDATE');
        res.data.originData = _.sortBy(res.data.originData, 'CREATEDATE');
        _.forEach(res.data.data, function(x){
          var datetime = new Date(x.CREATEDATE);
          x.CREATEDATE = datetime.toLocaleDateString() + ' ' + datetime.toLocaleTimeString('en-US',{'hour12': false});
        });
        _.forEach(res.data.originData, function(x){
          var datetime = new Date(x.CREATEDATE);
          x.CREATEDATE = datetime.toLocaleDateString() + ' ' + datetime.toLocaleTimeString('en-US',{'hour12': false});
          datetime = new Date(x.REVISEDATE);
          x.REVISEDATE = datetime.toLocaleDateString() + ' ' + datetime.toLocaleTimeString('en-US',{'hour12': false});
        });
        $scope.gridOption.api.setRowData(res.data.originData);
        $scope.rawData = res.data;          
      });
  };

  function getDateRangeList(times) {
    var today = new Date();
    var rangeList = [];
    _.forEach(_.range(0, times), function(time){
      var fromDate = new Date();
      if (today.getDay() === 0) {
        fromDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 6, 8, 30);
      } else if (today.getDay() === 1) {
        fromDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(),  8, 30);
      } else if (today.getDay() > 1) {
        fromDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 1,  8, 30);
      };
      var endDate = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() + 7, 8, 30);
      rangeList.push({FROM: fromDate, TO: endDate});
      today = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate() - 7, 8, 30);
    });
    return rangeList;
  };
};

angular
  .module('mean.te_phison_hold_lot')
  .controller('Te_phison_hold_lotController', Te_phison_hold_lotController);

Te_phison_hold_lotController.$inject = ['_', '$scope', '$http'];
