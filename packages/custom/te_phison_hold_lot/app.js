'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Te_phison_hold_lot = new Module('te_phison_hold_lot');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Te_phison_hold_lot.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Te_phison_hold_lot.routes(app, auth, database, utils);
  Te_phison_hold_lot.aggregateAsset('css', 'te_phison_hold_lot.css');
  Te_phison_hold_lot.angularDependencies(['agGrid']);
  return Te_phison_hold_lot;
});
