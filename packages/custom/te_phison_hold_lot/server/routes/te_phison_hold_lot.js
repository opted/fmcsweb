(function() {
  'use strict';
  module.exports = function(Te_phison_hold_lot, app, auth, database, utils) {
    var interval;
    var te_phison_hold_lotQuery = require('../controllers/te_phison_hold_lotQuery');
    var mesQuery = require('../controllers/mesQuery');

    app.post('/api/te_phison_hold_lot/getReportByRange', function(req, res, next){
      te_phison_hold_lotQuery
        .getReportByRange(req.body.FROM, req.body.TO)
        .then(function(data){
          var newResult = te_phison_hold_lotQuery.processQDNData(data);
          newResult.originData = data;
          res.send(newResult); 
        })
        .then(null, function(err){
          console.log(err);
          res.status(500).send(err);
        });
    });
    app.post('/api/te_phison_hold_lot/getBibGuide', function(req, res, next){
      var result = te_phison_hold_lotQuery.readBIBGuide();
      res.send(result);
    });

    interval = setInterval(function () {
      var today = new Date();
      if (today.getHours() !== 8 || today.getMinutes() !== 30 ) {
        //console.warn('[te_phison]: not at produce report datetime');
        return;
      };
      var startDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 1, 8, 30);
      var endDate = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 8, 30);
      var startDateStr = startDate.toLocaleString('en-US', {'hour12': false});
      var endDateStr = endDate.toLocaleString('en-US', {'hour12': false});
      te_phison_hold_lotQuery
        .checkDailyData(startDate, endDate)
        .then(function(result){ 
          if (result === 0) {
            mesQuery
              .getReportFromMes(startDateStr, endDateStr, utils)
              .then(function(result){
                te_phison_hold_lotQuery.saveDailyData(result, startDate, endDate)
                  .then(function(result){
                    //console.warn('[te_phison]: ', startDate, '-', endDate, 'saved.');
                  })
                  .then(null, function(err){
                    console.log(err);
                    res.status(500).send(err);
                  });
              }).then(null, function(err){
                console.log(err);
              });
          } else {
            //console.warn('[te_phison]: ', startDate, '-', endDate, 'Data exists.')
          };
        })
    }, 10*1000);
  };
})();
