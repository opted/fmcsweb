'use strict'
var _ = require('lodash');
var Q = require('q');
var XLSX = require('xlsx');
var mongoose = require('mongoose');
var TEPHISON = mongoose.model('TEPHISON');

function filterData(data){
  data = _.filter(data, function(x){
    var isRightFamily = (x.FAMILY.indexOf('MicroSD') > -1) || (x.FAMILY.indexOf('SD') > -1);
    var isRightHoldDisposition = x.HOLDDISPOSITION && (x.HOLDDISPOSITION.match('[0-9_\r\n,]*')[0].length === x.HOLDDISPOSITION.length); 
    return isRightFamily && isRightHoldDisposition;
  })
  _.forEach(data, function(x){
    x.HOLDDISPOSITION = x.HOLDDISPOSITION.split(/\r?\n/);
  })
  return data;
};

exports.readBIBGuide = function(){
  var workbook = XLSX.readFile('packages/custom/te_phison_hold_lot/public/assets/xlsx/BIBGuide.xlsx')
  return workbook;
};

exports.checkDailyData = function(from, to){
  var deferred = Q.defer();
  TEPHISON.count({'RANGE.FROM':from, 'RANGE.TO': to})
    .then(function(result){
       deferred.resolve(result);
    })
    .then(null, function(err){ 
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

exports.saveDailyData = function(data, from, to){
  var deferred = Q.defer();
  var nowTime = new Date();
  _.forEach(data, function(doc){
    doc.RANGE = {FROM: from, TO: to};
  });
  TEPHISON.insertMany(data)
    .then(function(result){
       deferred.resolve('inserted');
    })
    .then(null, function(err){ 
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

exports.getReportByRange = function(from, to, create){
  var deferred = Q.defer();
  var matchFile = {
    '$match': {
      'RANGE.FROM': {'$gte': new Date(from), '$lte': new Date(to)},
      'RANGE.TO': {'$gte': new Date(from), '$lte': new Date(to)}
    }
  };
  var group = {
    '$group': {
      '_id': { 'CREATEDATE': '$CREATEDATE', 'LOTNO': '$LOTNO' },
      'INCOMINGNO': {'$last': '$INCOMINGNO'},
      'HOLDDISPOSITION': {'$last': '$HOLDDISPOSITION'},
      'DIEQTY': {'$last': '$DIEQTY'},
      'FAMILY': {'$last': '$FAMILY'},
      'REVISEDATE': {'$last': '$REVISEDATE'}
    }
  };
  var project = {
    '$project': { 
      CREATEDATE:'$_id.CREATEDATE',
      HOLDDISPOSITION: 1,
      LOTNO:'$_id.LOTNO',
      INCOMINGNO:1,
      DIEQTY:1,
      FAMILY:1,
      REVISEDATE:1,
      _id:0
    }
  }
  TEPHISON.aggregate([matchFile, group, project])
    .then(function(result){
      deferred.resolve(result);
    })
    .then(null, function(err){
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

exports.processQDNData = function(data){
  data = filterData(data);
  var bibList = {};
  _.forEach(data, function(doc){
    var bibHeader;
    if (doc.FAMILY.indexOf('MicroSD') > -1) {
      bibHeader = '1D1';
    } else {
      bibHeader = '1D5';
    };
    _.forEach(doc.HOLDDISPOSITION, function(m){      
      var multiDut = m.split(',');
      var splitors = multiDut[0].split('_');
      var bin = splitors[0];
      var bib = splitors[1];
      var duts = _.flatten([splitors[2], multiDut.slice(1)]);
      if (doc['BIN' + bin] === undefined) {
        doc['BIN' + bin] = 0;
      };
      doc['BIN' + bin] += duts.length;
      while (bib.length <= 4) {
        bib = '0' + bib;
      };
      bib = bibHeader + bib;
      if (bibList[bib] === undefined) {
        bibList[bib] = [];
      };
      var bibData = { 
        CUSTOMLOT: doc.INCOMINGNO,
        LOTNO: doc.LOTNO,
        COORDINATE: duts, 
      };
      _.forEach(duts, function(dut){
        bibData[dut] = 1;
      });
      bibData.COORDINATE = bibData.COORDINATE.join(',');
      bibList[bib].push(bibData);
    });
    doc.HOLDDISPOSITION = doc.HOLDDISPOSITION.join('\n');
  });
  return {data: data, bibList: bibList};
};

