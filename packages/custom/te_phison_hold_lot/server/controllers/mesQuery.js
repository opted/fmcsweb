var oracledb = require('oracledb');
var _ = require('lodash');
var Q = require('q');

exports.getReportFromMes = function(startTime, endTime, utils){
  var sqlStr = 
        "SELECT HOLDDISPOSITION,TWD.CREATEDATE,TWD.LOTNO,INCOMINGNO,DIEQTY,FAMILY,TWD.REVISEDATE from HISMFT.TBLWIPERFDATA TWD " +
        "INNER JOIN HISMFT.TBLWIPERFREASONDISP " +
        "ON TWD.ERFNO=HISMFT.TBLWIPERFREASONDISP.ERFNO " +
        "INNER JOIN HISMFT.TBLWIPLOTDATA " +
        "ON TWD.LOTNO=HISMFT.TBLWIPLOTDATA.LOTNO " +
        "INNER JOIN HISMFT.TBLENGPRODUCTDATA " +
        "ON HISMFT.TBLWIPLOTDATA.PRODUCTNO=HISMFT.TBLENGPRODUCTDATA.PRODUCTNO " + 
        "where TWD.CUSTOMERNO like '5997-2' " +
        "AND PHASE < 10 " +
        "AND OPNO like '%BURNIN%' " + 
        "AND TWD.CREATEDATE BETWEEN TO_DATE('" + startTime +"','MM-DD-YYYY ,hh24:mi:ss') " +
        "AND TO_DATE('" + endTime +"','MM-DD-YYYY ,hh24:mi:ss') "
  return utils.ctrls.mes.exec(sqlStr)
};

function doRelease(connection){
  connection
    .close( function(err) {
      if (err) {
        console.error(err.message);
      };
    });
};
