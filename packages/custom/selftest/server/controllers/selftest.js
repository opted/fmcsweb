'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var ST = mongoose.model('ST');
var dailyStatus = mongoose.model('STDailyStatus');
var STOvenConfigue = mongoose.model('STOvenConfigue');
var Q = require('q');

function replaceAllHyphen(str) {
    return str.replace(new RegExp('-', 'g'), '');
}

function addHours(queryDateTime){
  var tempDate = new Date(queryDateTime)
  return tempDate.setHours(tempDate.getHours()+8);
}
function substractHours(queryDateTime){
  var tempDate = new Date(queryDateTime)
  return tempDate.setHours(tempDate.getHours()-8);
}

function getMBITableName(queryDateTime){
    var queryDate = new Date(queryDateTime)
    var nowYear = queryDate.getUTCFullYear() % 100
    var nowMonth = queryDate.getMonth() + 1;
    var tableName = nowMonth >= 10 ? nowYear.toString() + nowMonth.toString(): nowYear.toString() + '0' +  nowMonth.toString()

    return 'MBI'+tableName;
}

function trans2UTC(date) {
    return new Date(Date.UTC(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      date.getHours(),
      date.getMinutes()
    ));
  }
  function tran2SqlDateFormate(date){
    console.log(date.getMonth())
    return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() ;
    // return date.getFullYear() + "-" + date.getMonth() + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes();
  }

  function modifyDatetimeQuery(obj) {
    if ('$gte' in obj) obj.$gte = trans2UTC(new Date(obj.$gte));
    if ('$lt' in obj) obj.$lt = trans2UTC(new Date(obj.$lt));
    return _.omit(obj, function(val) {
      return _.isNaN(val.valueOf());
    });
  }

  function modifyQuery(query) {
    return _(query)
      .mapValues(function(value, key) {
        return (key === 'DATETIME') ? 
          modifyDatetimeQuery(value) : ( _.isArray(value) ? value : [value] );
      });
  }

  function stQuery(query) {
    var lowerDate = new Date(_.get(query, ['DATETIME', '$gte'])),
        upperDate = new Date(_.get(query, ['DATETIME', '$lt']));
    return  _({})
      .set('DATETIME', _({})
        .set('$gte', _.isNaN(lowerDate.valueOf()) ? undefined : trans2UTC(lowerDate))
        .set('$lt', _.isNaN(upperDate.valueOf()) ? undefined : trans2UTC(upperDate))
        .value())
      .set('OVENID', query.OVENID ? { $in: query.OVENID } : undefined)
      .set('TESTITEM', query.TESTITEM ? { $in: query.TESTITEM } : undefined)
      .omit(function(value) {
        return _.isUndefined(value) || _.isEmpty(value)
      });
  }

// B.oven_ID,B.lotnum,A.min_time,A.max_time
//                             ,B.board_id,B.driver_id,B.Zone_ID,B.Slot_ID,B.bipgm

function getDistinctField(field){
  switch (field){
    // case 'MACHINE':
    //   return 'F.' + field;
    //   break;
    case 'DRIVERID':
      return 'driver_id';
      break;
    case 'LOTNO':
      return 'lotnum';
      break;
    case 'bipgm':
      return 'bipgm';
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}

class SelftestManager {
  constructor(utils) {
    this.coll = ST;
    this.searchColl = STOvenConfigue;
    this.utils = utils;
    this.pathMapping = {

      'OVENID': 'OVENID',
      'ZB': 'ZB',
      'TESTITEM':'TESTITEM'
    };
    this.searchPathMapping = {
      'TYPE':'TYPE',
      'OVENID': 'OVENID'
    };
    this.arrayFields = [];
  }

  getSearchField (query) {
    var df = Q.defer();
    try {
      var now = new Date()
      // var distFld = getDistinctField(query.field);
      // var joinTable = getTableName(query.field);
      var table = getMBITableName(now.toISOString())
      var sql = `
        SELECT
           TOP(100) *
        FROM (
          SELECT DISTINCT  `+ query.field + `
          FROM
             `+ table + `
          WHERE
            ` + query.field + ` LIKE '`+ query.regex+`%' ) AS a
        ORDER BY  `+query.field;

      // var vars = {
      //   'regex': _.toUpper(query.regex) + '%'
      // };
      this.utils.ctrls.mbi.exec(sql)
      .then(result => 
        df.resolve(_(result)
                     .map(function(r){return _.trimEnd(r[query.field])})
                     ))
      // .catch(err => df.reject(err));
      .catch(function(err){
        console.log(err)
        df.reject(err)
      })
    } catch (err){
      console.log(err)
      df.reject(err.stack);
    }
    return df.promise;
  }

  getOvenAggregate (params) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      var dateRange = {'DATETIME': {}};
      if (query.from !== null){
        dateRange.DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATETIME['$lt'] = new Date(addHours(query.to));
      }
      if (!_.isEmpty(dateRange.DATETIME)){
        _.assign(match, dateRange);
      }
      var pipe = [{'$match': match}];
      pipe.push({'$unwind': "$RESULTS"});
      pipe.push(
          { '$group': {
            '_id': { 
              'DATETIME': '$DATETIME', 
              // 'TESTITEM': '$TESTITEM',
              'OVENID': '$OVENID'
            },
            'TOTAL': { '$sum': '$RESULTS.TESTDRV' },
            'PASS': { '$sum': '$RESULTS.PFDRV' }
          }}
        );
      pipe.push({
        '$project': {
          '_id': 0,
          'DATETIME': {
            '$dateToString': { 'format': '%Y-%m-%d %H:%M', 'date': '$_id.DATETIME' }
          },
          // 'TESTITEM': '$_id.TESTITEM',
          'OVENID': '$_id.OVENID',
          'PASS': '$PASS',
          'TOTAL': '$TOTAL'
        }}
      );
      pipe.push({'$sort': { 'DATETIME' : -1 }});
      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(ST, pipe)
      .then(function(result){
        try {
            df.resolve(result);
        } catch (e) {
            df.reject(e.stack);
            console.log(e)
        }
      })
    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };
  
   getDailyStatus (params) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      // console.log(query)
      var dateRange = {'DATE': {}};
      if (query.from !== null){
        dateRange.DATE['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATE['$lt'] = new Date(addHours(query.to));
      }
      if (!_.isEmpty(dateRange.DATE)){
        _.assign(match, dateRange);
      }
       _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['OVENID']), this.pathMapping));
      var pipe = [{'$match': match}];

      pipe.push({
        '$project': {
          '_id': 0,
          // 'DATETIME': {
          //   '$dateToString': { 'format': '%Y-%m-%d %H:%M', 'date': '$DATE' }
          // },
          'DATE':1,
          'OVENID': 1,
          'ST_STATUS': 1
        }}
      );

      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(dailyStatus, pipe)
      .then(function(result){
        try {
            df.resolve(result);
        } catch (e) {
            df.reject(e.stack);
            // console.log(e)
        }
      })
    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };

queryAggregate (params) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      var dateRange = {'DATETIME': {}};
      var matchMode ={}
      if (query.from !== null){
        dateRange.DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATETIME['$lt'] = new Date(addHours(query.to));
      }
      if (!_.isEmpty(dateRange.DATETIME)){
        _.assign(match, dateRange);
      }
      // console.log(dateRange)
      _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['OVENID']), this.pathMapping));
      var pipe = [{'$match': match}];
       
      pipe.push({'$unwind': "$RESULTS"});
      if(query.failedmode !== undefined){
        matchMode={'TESTITEM':query.failedmode};
        pipe.push({'$match': matchMode})
      }
      pipe.push(
          { '$group': {
            '_id': { 
              'DATETIME': '$DATETIME', 
              // 'TESTITEM': '$TESTITEM',
              'OVENID': '$OVENID'
            },
            'TESTITEM_RECORD':{'$push':{
                            'ZB':'$RESULTS.ZB',
                            'TESTITEM':"$TESTITEM",
                            'T_CNT':{ '$sum': '$RESULTS.TESTDRV' },
                            'P_CNT':{ '$sum': '$RESULTS.PFDRV' }, 
                            }},
            'DLLVER':{'$first':"$DLLVER"},
            'STVER':{'$first':"$STVER"},
            'TOTAL': { '$sum': '$RESULTS.TESTDRV' },
            'PASS': { '$sum': '$RESULTS.PFDRV' }
          }}
        );
      pipe.push({
        '$project': {
          '_id': 0,
          'DATETIME': {
            '$dateToString': { 'format': '%Y/%m/%d %H:%M', 'date': '$_id.DATETIME' }
          },
          'ISODATE':'$_id.DATETIME',
          // 'TESTITEM': '$_id.TESTITEM',
          'TESTITEM_RECORD':1,
          'DLLVER':1,
          'STVER':1,
          'OVENID': '$_id.OVENID',
          'PASS': 1,
          'TOTAL': 1,
          'FAIL': {'$subtract':['$TOTAL','$PASS']}
        }}
      );
      pipe.push({'$sort': { 'DATETIME' : -1 }});
      // console.log(pipe  )
      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(ST, pipe)
      .then(function(result){
        try {
            // console.log(result)
            // df.resolve(result);

            _.forEach(result,function(stlog){
               stlog['ST_PASS_RECORD'] =
                  _(stlog['TESTITEM_RECORD'])
                 .groupBy('ZB')
                 .map(function(r,zb){
                    var s = new Object();
                    s['ZB'] = zb
                    _.forEach(r,function(ti){ //testitem
                      var o = new Object();
                      var tiName = (ti['TESTITEM']).replace(/\s+/g,"")    
                          o[tiName] =ti['T_CNT'] - ti['P_CNT'];
                          o['PASS'] = (o[tiName]==0 && ti['T_CNT']>0) ? true:false;
                          _.assign(s,o)     
                      })
                    return s;
                  }).value()
                 stlog['PASS_SLOT'] = _.sumBy(stlog['ST_PASS_RECORD'],'PASS')
               })

              df.resolve(result);
        } catch (e) {
            df.reject(e.stack);
            console.log(e)
        }
      })
    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };

getSlotAggregate (params) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      var matchMode ={}
      var dateRange = {'DATETIME': {}};
      if (query.from !== null){
        dateRange.DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATETIME['$lt'] = new Date(addHours(query.to));
      }

      if (!_.isEmpty(dateRange.DATETIME)){
        _.assign(match, dateRange);
      }
      _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['OVENID']), this.pathMapping));
      var pipe = [{'$match': match}];
       
      pipe.push({'$unwind': "$RESULTS"});
      if(query.failedmode !== undefined){
        matchMode={'TESTITEM':query.failedmode};
        pipe.push({'$match': matchMode})
      }
      pipe.push(
          { '$group': {
            '_id': {
              'DATETIME': '$DATETIME', 
              // 'TESTITEM': '$TESTITEM',
              'OVENID': '$OVENID',
              'ZB': '$RESULTS.ZB'
            },
            'TESTITEM_RECORD':{'$push':{'TESTITEM':"$TESTITEM",
                                        'T_CNT':{ '$sum': '$RESULTS.TESTDRV' },
                                        'P_CNT':{ '$sum': '$RESULTS.PFDRV' }, 
                                        }},
            'DLLVER':{'$first':"$DLLVER"},
            'STVER':{'$first':"$STVER"},
            'TOTAL': { '$sum': '$RESULTS.TESTDRV' },
            'PASS': { '$sum': '$RESULTS.PFDRV' }
          }}
        );
      pipe.push({
        '$project': {
          '_id': 0,
          'DATETIME': {
            '$dateToString': { 'format': '%Y-%m-%d %H:%M', 'date': '$_id.DATETIME' }
          },
          'ISODATE':'$_id.DATETIME',
          // 'TESTITEM': '$_id.TESTITEM',
          'TESTITEM_RECORD':1,
          'OVENID': '$_id.OVENID',
          'ZB': '$_id.ZB',
          'PASS': '$PASS',
          'TOTAL': '$TOTAL',
          'DLLVER':1,
          'STVER':1,
          'FAIL': {'$subtract':['$TOTAL','$PASS']}
        }}
      );


      pipe.push({'$sort': { 'DATETIME' : -1 }});
      // console.log(pipe  )
      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(ST, pipe)
      .then(function(result){
        try {
            // df.resolve(  result);
            df.resolve(  _(result)
                         .filter(r=> r.TOTAL >0)
                         .forEach(function(r){
                            _.forEach(r['TESTITEM_RECORD'],function(ti){
                            var o = new Object();
                            var tiname = (ti['TESTITEM']).replace(/\s+/g,"")    
                                o[tiname] =ti['T_CNT'] - ti['P_CNT']||0;
                              _.assign(r,o)
                         })
                      }) );
        } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
        }
      })
    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };



  getAllOvenConfigue(params){
    var df = Q.defer();
    try {

      STOvenConfigue.find({},{'_id':0}, function(err, stOvenConfigues){
          if(err){
            console.log(err.stack);
            df.reject(err.stack);
          } else{
            df.resolve(stOvenConfigues);
          }
      })
      
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }

  uploadOvenConfigue(params){
    var df = Q.defer();
    try {

      for( var i = 0  ; i < params.length ; i++ ){
        params[i]["OVENID"] = params[i]["Oven ID"]
        delete params[i]["Oven ID"]
        params[i]["TYPE"] = params[i]["Type"]
        delete params[i]["Type"]
        params[i]["SLOT"] = params[i]["Slot"]
        delete params[i]["Slot"]
        var id = params[i].OVENID;
        if (id) {
          STOvenConfigue.collection.findOneAndUpdate({OVENID: id}, params[i], {upsert: true}, function (err2) {
              df.reject(err2);
          });
        } 
      }
      df.resolve();
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }


  getMbiByTime(params,filterColumn){
    var df = Q.defer();
    try {

      var query = params
      var ovenStr = replaceAllHyphen(query.OVENID)  
      var tempTime = new Date(params.ISODATE)
      tempTime.setDate(tempTime.getDate()+7)
      var endTime = new Date(tempTime);
      var filterStr="";
      _.forOwn(filterColumn,function(v,k){
        filterStr+=" AND B." + k + " LIKE '" + v +"%' " 
      })
      var isoDate = new Date( substractHours( query.ISODATE));
        var bindVars = {
        'logDate':isoDate,
        'endTime':endTime
      };

      // console.log(isoDate,endTime,ovenStr)

      var sqlSlotStr = `
            SELECT DISTINCT B.oven_ID,B.lotnum,A.min_time,A.max_time
                            ,B.board_id,B.driver_id,B.Zone_ID,B.Slot_ID,B.bipgm
              FROM
                  (SELECT TOP 100 oven_ID,lotnum,min(log_time) as min_time,max(log_time) as max_time FROM 
                  ` + getMBITableName(query.ISODATE) +` 
                  WHERE  
                  oven_ID LIKE '`+ovenStr+`'
                 
                  GROUP BY oven_ID,lotnum 
                  ORDER BY min_time ) A ,
                  `+ getMBITableName(query.ISODATE) +` B
                   
              WHERE
                  (B.lotnum = A.lotnum 
                  AND B.oven_ID = A.oven_ID)
                   `+ filterStr+`
                  AND (B.log_time > A.min_time AND B.log_time < A.max_time
                  AND B.log_time > @logDate AND @endTime >B.log_time )
                  ORDER BY A.min_time
                   `
        this.utils.ctrls.mbi.exec(sqlSlotStr,bindVars)
        .then(function(result){
          try {

            df.resolve(result);
          } catch (err){
            console.log(err);
            df.reject(err.stack);
          }      
        })
      } catch (err){
        console.log(err);
        df.reject(err.stack);
      }
    return df.promise;
  }

  getMultiMbiOvenByTime(params){
    var df = Q.defer();

      var stModel = this;

      var queryList=[];
      var lotRecord={};

      var groupCount = params.stLog.length;
      console.log(params)
       // console.log(_(params).pick(['DRIVERID','LOTNO','bipgm']).value())
      params.stLog.forEach(function(stInfo){
          queryList.push(stModel.getMbiByTime(stInfo,_.pick(params,['driver_id','lotnum','bipgm']) ))
        
      })  


      // Q.all(queryList)
      this.utils.ctrls.batch.doBatch(queryList)
        .then(function(results){  
            df.resolve(results);
            })
        .catch((err) => {
            console.log(err);
            df.reject(err.stack);
        });

    return df.promise;
  }

  getSingleMbiByTime(params){
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      // var fromDateValue = new Date(query.from);
      // var toDateValue = new Date(query.to);

      
      // var ovenStr = replaceAllHyphen(params.OVENID)  
      var isoDate = new Date( substractHours( params.ISODATE));
        var bindVars = {
        'logDate':isoDate,
      };
      // console.log(params)
      // console.log(ovenStr)
      var sqlStr = `
            SELECT A.oven_ID,A.lotnum,B.board_id,B.log_time,B.Zone_ID,B.Slot_ID,A.min_time,A.max_time
              FROM
                  (SELECT TOP 100 oven_ID,lotnum,min(log_time) as min_time,max(log_time) as max_time FROM 
                  ` + getMBITableName(query.ISODATE) +` 
                  WHERE  
                  oven_ID LIKE 'OC%'
                  GROUP BY oven_ID,lotnum 
                  ORDER BY min_time DESC) A ,
                  `+ getMBITableName(query.ISODATE) +` B
                   
              WHERE
                  B.lotnum = A.lotnum AND @logDate < A.min_time
                  AND (B.log_time > A.min_time AND B.log_time < A.max_time)
                   `
        this.utils.ctrls.mbi.exec(sqlStr,bindVars)
        .then(function(result){
          try {

            df.resolve(result);
              }  catch (err){
            console.log(err);
            df.reject(err.stack);
          }      
        })
      } catch (err){
        console.log(err);
        df.reject(err.stack);
      }
    return df.promise;
  }
}

module.exports = SelftestManager;