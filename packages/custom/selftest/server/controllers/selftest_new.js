'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  ST = mongoose.model('ST'),
  _ = require('lodash'),
  Q = require('q');

exports.getOvenAggregate = function(req, res, next) {
  var findQuery = JSON.parse(req.query.find);
  if (findQuery.DATETIME && findQuery.DATETIME.$gte) {
    findQuery.DATETIME.$gte = new Date(findQuery.DATETIME.$gte);
  }
  if (findQuery.DATETIME && findQuery.DATETIME.$lte) {
    findQuery.DATETIME.$lte = new Date(findQuery.DATETIME.$lte);
  }
  ST.aggregate([
      { $match: findQuery }, 
      { $unwind: '$RESULTS' },
      { $group: {
        _id: { 
          DATETIME: '$DATETIME', 
          TESTITEM: '$TESTITEM',
          OVENID: '$OVENID'
        },
        TOTAL: { $sum: '$RESULTS.TESTDRV' },
        PASS: { $sum: '$RESULTS.PFDRV' }
      }}, { 
        $project: {
        _id: 0,
        DATETIME: {
          $dateToString: { format: '%Y-%m-%d %H:%M', date: '$_id.DATETIME' }},
        TESTITEM: '$_id.TESTITEM',
        OVENID: '$_id.OVENID',
        PASS: '$PASS',
        TOTAL: '$TOTAL'
      }}, {
        $sort: { DATETIME: -1 } 
  }])
  .exec(function(err, data) {
    res.json(data);
  });
};


function trans2UTC(date) {
  return new Date(Date.UTC(
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours(),
    date.getMinutes()
  ));
}

function modifyDatetimeQuery(obj) {
  if ('$gte' in obj) obj.$gte = trans2UTC(new Date(obj.$gte));
  if ('$lt' in obj) obj.$lt = trans2UTC(new Date(obj.$lt));
  return _.omit(obj, function(val) {
    return _.isNaN(val.valueOf());
  });
}

function modifyQuery(query) {
  return _(query)
    .mapValues(function(value, key) {
      return (key === 'DATETIME') ? 
        modifyDatetimeQuery(value) : ( _.isArray(value) ? value : [value] );
    });
}

function stQuery(query) {
  var lowerDate = new Date(_.get(query, ['DATETIME', '$gte'])),
      upperDate = new Date(_.get(query, ['DATETIME', '$lt']));
  return  _({})
    .set('DATETIME', _({})
      .set('$gte', _.isNaN(lowerDate.valueOf()) ? undefined : trans2UTC(lowerDate))
      .set('$lt', _.isNaN(upperDate.valueOf()) ? undefined : trans2UTC(upperDate))
      .value())
    .set('OVENID', query.OVENID ? { $in: query.OVENID } : undefined)
    .set('TESTITEM', query.TESTITEM ? { $in: query.TESTITEM } : undefined)
    .omit(function(value) {
      return _.isUndefined(value) || _.isEmpty(value)
    });
}

exports.queryAggregate = function(req, res, next) {
  var query = modifyQuery(req.query).value();
  ST.aggregate([
      { $match: stQuery(query).value() }, 
      { $unwind: '$RESULTS' },
      { $group: {
        _id: { 
          DATETIME: '$DATETIME', 
          TESTITEM: '$TESTITEM',
          OVENID: '$OVENID'
        },
        TOTAL: { $sum: '$RESULTS.TESTDRV' },
        PASS: { $sum: '$RESULTS.PFDRV' }
      }}, { 
        $project: {
        _id: 0,
        DATETIME: {
          $dateToString: { format: '%Y-%m-%d %H:%M', date: '$_id.DATETIME' }},
        TESTITEM: '$_id.TESTITEM',
        OVENID: '$_id.OVENID',
        PASS: '$PASS',
        TOTAL: '$TOTAL'
      }}, {
        $sort: { DATETIME: -1 } 
  }])
  .exec(function(err, data) {
    res.json(data);
  });
};

exports.getSlotAggregate = function(req, res, next) {
  var findQuery = JSON.parse(req.query.find);
  var lowerDate = new Date(findQuery.DATETIME.$gte);
  var upperDate = new Date(findQuery.DATETIME.$lte);
  findQuery.DATETIME.$gte = lowerDate;
  findQuery.DATETIME.$lte = upperDate;
  ST.aggregate([
      { $match: findQuery }, 
      { $unwind: '$RESULTS' },
      { $group: {
          _id: {
            DATETIME: '$DATETIME', 
            OVENID: '$OVENID',
            ZB: '$RESULTS.ZB'
          },
          TOTAL: { $sum: '$RESULTS.TESTDRV' },
          PASS: { $sum: '$RESULTS.PFDRV' }
      }}, {
        $project: {
          _id: 0,
          DATETIME: {
            $dateToString: { format: '%Y-%m-%d %H:%M', date: '$_id.DATETIME' }},
          OVENID: '$_id.OVENID',
          ZB: '$_id.ZB',
          PASS: '$PASS',
          TOTAL: '$TOTAL'
      }}, {
        $sort: { DATETIME: -1 }
      }
  ])
  .exec(function(err, data) {
    res.json(data);
  });
};
exports.getLastOven = function(req, res, next) {
  ST.aggregate([
      { $group: {
          _id: { OVENID: '$OVENID' },
          lastDate: { $max: '$DATETIME' }
      }}, {
        $project: {
          _id: 0,
          OVENID: '$_id.OVENID',
          DATETIME: '$lastDate'
      }}
  ])
  .exec(function(err, data) {
    ST.aggregate([
        { $match: { $or: data } }, 
        { $unwind: '$RESULTS' },
        { $group: {
            _id: { 
            DATETIME: '$DATETIME', 
            TESTITEM: '$TESTITEM',
            OVENID: '$OVENID'
          },
          TOTAL: { $sum: '$RESULTS.TESTDRV' },
          PASS: { $sum: '$RESULTS.PFDRV' }
        }}, { 
          $project: {
            _id: 0,
            DATETIME: {
              $dateToString: { format: '%Y-%m-%d %H:%M', date: '$_id.DATETIME' }},
            TESTITEM: '$_id.TESTITEM',
            OVENID: '$_id.OVENID',
            PASS: '$PASS',
            TOTAL: '$TOTAL'
        }}, {
          $sort: { DATETIME: -1 } 
    }]).exec(function(err, data) {
      res.json(data);
    });
  });
};

exports.getLastSlot = function(req, res, next) {
  ST.aggregate([
      { $group: {
          _id: { OVENID: '$OVENID' },
          lastDate: { $max: '$DATETIME' }
      }}, {
        $project: {
          _id: 0,
          OVENID: '$_id.OVENID',
          DATETIME: '$lastDate'
      }}
  ])
  .exec(function(err, data) {
    ST.aggregate([
        { $match: { $or: data } }, 
        { $unwind: '$RESULTS' },
        { $group: {
            _id: {
              DATETIME: '$DATETIME', 
              OVENID: '$OVENID',
              ZB: '$RESULTS.ZB'
            },
            RESULTS: { $push: { 
                TESTITEM: '$TESTITEM',
                TESTDRV: '$RESULTS.TESTDRV', 
                PFDRV: '$RESULTS.PFDRV'
            }},
            TOTAL: { $sum: '$RESULTS.TESTDRV' },
            PASS: { $sum: '$RESULTS.PFDRV' }
        }}, {
          $project: {
            _id: 0,
            DATETIME: {
              $dateToString: { format: '%Y-%m-%d %H:%M', date: '$_id.DATETIME' }},
            OVENID: '$_id.OVENID',
            ZB: '$_id.ZB',
            PASS: '$PASS',
            TOTAL: '$TOTAL'
        }}, {
          $sort: { DATETIME: -1 }
        }
    ]).exec(function(err, data) {
      res.json(data);
    });
  });
};

exports.getTestItems = function(req, res, next) {
  ST.distinct('TESTITEM')
  .exec(function(err, data) {
    res.json(data);
  });
};

exports.getOvenIds = function(req, res, next) {
  ST.distinct('OVENID')
  .exec(function(err, data) {
    res.json(data);
  });
}; 

exports.getSelfTestLast = function(req, res, next) {
  var MES = mongoose.model('MES'),
      BIDMap = mongoose.model('BIDMap'),
      query = modifyQuery(req.body).value(),
//       stLastQuery = [{
//         $match: {
//           OVENID: { $in: query.OVENID },
//           TESTITEM: { $in: query.TESTITEM  }
//       }}, {
//         $group: {
//           _id: '$OVENID',
//           DATETIME: { $max: '$DATETIME' }
//       }}],
      stDistinceQuery = {
        OVENID: { $in: query.OVENID },
        TESTITEM: { $in: query.TESTITEM }
      };
//   stLastQuery[0].$match = _.omit(stLastQuery[0].$match, function(value){
//     return _.isUndefined(value.$in);
//   });
  stDistinceQuery = _.omit(stDistinceQuery, function(value) {
    return _.isUndefined(value.$in);
  });
  ST.distinct('OVENID', stDistinceQuery)
  .exec()
  .then(function(OVENIDS) {
    return Q.all(_.map(OVENIDS, function(OVENID) {
      return ST.find({
          OVENID: OVENID
      }, {_id:0, DATETIME:1})
      .sort({DATETIME: -1})
      .limit(1).exec()
      .then(function(STDatetime) {
        return Q.all([BIDMap.find({
            OVENID: OVENID,
            DATETIME: {
              $gte: STDatetime[0].DATETIME
            }
        }, {_id:0, DATETIME:1})
        .sort({DATETIME: 1})
        .limit(1).exec()
        .then(function(BIDDATETIME) {
          return BIDDATETIME.length ? BIDMap.find({
              OVENID: OVENID,
              DATETIME: BIDDATETIME[0].DATETIME
          }, {
            _id: 0, SLOTID: 1, 
            ZONEID: 1, LOTNO: 1
          }).exec() : [];
        })
        .then(function(BIDResults) {
          return BIDResults.length ? MES.find({
              LOTNO: { $in: _.pluck(BIDResults, 'LOTNO')
          }}).exec()
          .then(function(MESResults) {
            var mesMap = _.reduce(MESResults, function(prev, cur) {
              prev[cur.LOTNO] = cur;
              return prev;
            }, {});
            return _.map(BIDResults, function(bidResult) {
              return _({})
                .set('SLOTID', _.get(bidResult, 'SLOTID'))
                .set('ZONEID', _.get(bidResult, 'ZONEID'))
                .set('LOTNO', _.get(bidResult, 'LOTNO'))
                .set('DEVICENO', _.get(mesMap, [bidResult.LOTNO, 'DEVICENO'], null))
                .set('PACKAGETYPE', _.get(mesMap, [bidResult.LOTNO, 'PACKAGETYPE'], null))
                .set('SITENO', _.get(mesMap, [bidResult.LOTNO, 'SITENO'], null))
                .set('CUSTOMERLOTNO', _.get(mesMap, [bidResult.LOTNO, 'CUSTOMERLOTNO'], null))
                .value();
            });
          }) : [];
        }), ST.aggregate([{
              $match: {
                OVENID: OVENID,
                DATETIME: STDatetime[0].DATETIME
            }}, {
              $unwind: '$RESULTS'
            },  {
              $match: {
                'RESULTS.TESTDRV': 1
            }}, {
              $project: {
                _id: 0,
                TESTITEM: 1,
                OVENID: 1,
                DATETIME: {
                  $dateToString: { format: '%Y-%m-%d %H:%M', date: '$DATETIME' }
                },
                SLOTID: '$RESULTS.SLOTID',
                ZONEID: '$RESULTS.ZONEID',
                PFDRV: '$RESULTS.PFDRV',
                TESTDRV: '$RESULTS.TESTDRV',
                STVER: 1,
                DLLVER: 1
            }}, {
              $sort: {
                DATETIME: 1, ZONEID: 1, SLOTID: 1
            }}
        ]).exec()])
        .then(function(results) {
          return _.map(_.last(results), function(result) {
            return _.merge(result, _.find(_.first(results), {
              SLOTID: result.SLOTID, ZONEID: result.ZONEID
            }));
          });
        });
      });
    }))
  })
  .then(function(results) {
    results = _.flatten(results);
    var _id = -1,
        lastIdentify;
    _.forEach(results, function(result) {
      var identify = result.SLOTID + ',' + result.ZONEID + result.DATETIME + result.OVENID + result.LOTNO
      if (lastIdentify !== identify) {
        _id += 1;
        lastIdentify = identify;
      } 
      result._id = _id
    });
    res.json(results);
  })
  .then(null, function(error) {
    console.log(error);
    res.json([]);
  });
};

exports.getSelfTestByTime = function(req, res, next){
  var MES = mongoose.model('MES'),
      BIDMap = mongoose.model('BIDMap'),
      query = modifyQuery(req.body).value(),
      stAgg = [{
        $match: {
          DATETIME: query.DATETIME,
          OVENID: { $in: query.OVENID },
          TESTITEM: { $in: query.TESTITEM }
      }}, {
        $group: {
          _id: {
            OVENID: '$OVENID',
            DATETIME: '$DATETIME'
          }
      }}];
  stAgg[0].$match = _.omit(stAgg[0].$match, function(value, key) {
    return (key === 'DATETIME') ? _.isUndefined(value) : _.isUndefined(value.$in);
  });
  ST.aggregate(stAgg)
  .exec()
  .then(function(results) {
    return Q.all(_.map(results, function(result) {
      return Q.all([BIDMap.find({
          OVENID: result._id.OVENID,
          DATETIME: {
            $gte: result._id.DATETIME
          },
          COMPLETE_BI: true
      }, {_id:0, DATETIME:1}).sort({DATETIME: 1}).limit(1).exec()
      .then(function(BIDDATETIME) {
        return BIDDATETIME.length? BIDMap.find({
            OVENID: result._id.OVENID,
            DATETIME: BIDDATETIME[0].DATETIME
        }, {
          _id: 0, SLOTID: 1, ZONEID:1, LOTNO:1
        }).exec() : [];
      })
      .then(function(BIDResults) {
        return BIDResults.length ? MES.find({
            LOTNO: { $in: _.pluck(BIDResults, 'LOTNO')
        }}).exec()
        .then(function(MESResults) {
          var mesMap = _.reduce(MESResults, function(prev, cur) {
            prev[cur.LOTNO] = cur;
            return prev;
          }, {});
          return _.map(BIDResults, function(bidResult) {
            return _({})
              .set('SLOTID', _.get(bidResult, 'SLOTID'))
              .set('ZONEID', _.get(bidResult, 'ZONEID'))
              .set('LOTNO', _.get(bidResult, 'LOTNO'))
              .set('DEVICENO', _.get(mesMap, [bidResult.LOTNO, 'DEVICENO'], null))
              .set('PACKAGETYPE', _.get(mesMap, [bidResult.LOTNO, 'PACKAGETYPE'], null))
              .set('SITENO', _.get(mesMap, [bidResult.LOTNO, 'SITENO'], null))
              .set('CUSTOMERLOTNO', _.get(mesMap, [bidResult.LOTNO, 'CUSTOMERLOTNO'], null))
              .value();
          });
        }) : [];
      }), ST.aggregate([{
            $match: {
              OVENID: result._id.OVENID,
              DATETIME: result._id.DATETIME
          }}, {
            $unwind: '$RESULTS'
          },  {
            $match: {
              'RESULTS.TESTDRV': 1
          }}, {
            $project: {
              _id: 0,
              TESTITEM: 1,
              OVENID: 1,
              DATETIME: {
                $dateToString: { format: '%Y-%m-%d %H:%M', date: '$DATETIME' }
              },
              SLOTID: '$RESULTS.SLOTID',
              ZONEID: '$RESULTS.ZONEID',
              PFDRV: '$RESULTS.PFDRV',
              TESTDRV: '$RESULTS.TESTDRV',
              STVER: 1,
              DLLVER: 1
          }}, {
            $sort: {
              DATETIME: 1, ZONEID: 1, SLOTID: 1
          }}]).exec()])
      .then(function(results) {
        return _.map(_.last(results), function(result) {
          return _.merge(result, _.find(_.first(results), {
            SLOTID: result.SLOTID, ZONEID: result.ZONEID
          }));
        });
      });
    }));
  })
  .then(function(results) {
    results = _.flatten(results);
    var _id = -1,
        lastIdentify;
    _.forEach(results, function(result) {
      var identify = result.SLOTID + ',' + result.ZONEID + result.DATETIME + result.OVENID + result.LOTNO
      if (lastIdentify !== identify) {
        _id += 1;
        lastIdentify = identify;
      } 
      result._id = _id
    });
    res.json(results);
  })
  .then(null, function(error) {
    console.log(error);
    res.json([]);
  });
};

exports.getSelfTestByLotno = function(req, res, next){
  var MES = mongoose.model('MES'),
      BIDMap = mongoose.model('BIDMap'),
      query = modifyQuery(req.body).value();
  var bidQuery = [{
        $match: {
          LOTNO: { $in: query.LOTNO },
          OVENID: { $in: query.OVENID },
          BID: { $in: query.BID },
          DRIVERID: { $in: query.DRIVERID },
          TESTITEM: { $in: query.TESTITEM },
          COMPLETE_BI: true
      }}, {
        $group: {
          _id: {
            OVENID: '$OVENID',
            LOTNO: '$LOTNO'
          },
          DATETIME: {
            $min: '$DATETIME'
          }, 
          ZBID: {
            $addToSet: {
              SLOTID: '$SLOTID',
              ZONEID: '$ZONEID'
            }
          }
      }}];
  bidQuery[0].$match = _.omit(bidQuery[0].$match, function(value){
    return _.isUndefined(value.$in);
  });
  var mesPromise = MES.find({
      LOTNO: {
        $in: query.LOTNO 
  }}).exec();
  BIDMap.aggregate(bidQuery)
  .exec()
  .then(function(results) {
    return Q.all(_.map(results, function(result) {
      return ST.find({
          OVENID: result._id.OVENID,
          DATETIME: {
            $lte: result.DATETIME
          }
        }, {_id:0, DATETIME:1})
        .sort({DATETIME: -1})
        .limit(1).exec();
    }))
    .then(function(datetimes) {
      results = _.reduce(results, function(prev, current, index) {
        if (datetimes[index].length) {
          current.DATETIME = datetimes[index][0].DATETIME;
          prev.push(current);
        }
        return prev;
      }, []);
      return Q.all(_.map(results, function(result) {
        return ST.aggregate([{
              $match: {
                OVENID: result._id.OVENID,
                DATETIME: result.DATETIME
            }}, {
              $unwind: '$RESULTS'
            }, {
              $project: {
                _id:0,
                TESTITEM: 1,
                OVENID: 1,
                DATETIME: {
                  $dateToString: { format: '%Y-%m-%d %H:%M', date: '$DATETIME' }
                },
                LOTNO: { $literal: result._id.LOTNO },
                SLOTID: '$RESULTS.SLOTID',
                ZONEID: '$RESULTS.ZONEID',
                PFDRV: '$RESULTS.PFDRV',
                TESTDRV: '$RESULTS.TESTDRV',
                STVER: 1,
                DLLVER: 1
            }}, {
              $match: {
                $or: result.ZBID
            }}, {
              $sort: {
                ZONEID: 1, SLOTID: 1
            }}]).exec();
      }).concat(mesPromise))
      .then(function(data) {
        var mesMap = _.reduce(_.last(data), function(prev, cur) {
          prev[cur.LOTNO] = cur;
          delete cur.LOTNO;
          return prev;
        }, {});
        var results = _.flatten(_.map(_.flatten(data.slice(0,-1)), function(obj) {
          var map = mesMap[obj.LOTNO];
          obj.DEVICENO = map.DEVICENO;
          obj.PACKAGETYPE = map.PACKAGETYPE;
          obj.SITENO = map.SITENO;
          obj.CUSTOMERLOTNO = map.CUSTOMERLOTNO;
          obj.PFDRV = obj.TESTDRV ? obj.PFDRV : 0;
          return obj;
        }));
        var _id = -1,
            lastIdentify;
        _.forEach(results, function(result) {
          var identify = result.SLOTID + ',' + result.ZONEID + result.DATETIME + result.OVENID + result.LOTNO
          if (lastIdentify !== identify) {
            _id += 1;
            lastIdentify = identify;
          } 
          result._id = _id
        });
        res.json(results);
      });
    })
  })
  .then(null, function(err) {
    console.log('[ERR]', err);
    res.json([]);
  });
};


