(function() {
    'use strict';
    // var st = require('../controllers/selftest');
    /* jshint -W098 */
    // The Package is past automatically as first parameter
    var Q = require('q');
    module.exports = function(Selftest, app, auth, database, utils) {
        var selftestManager = require('../controllers/selftest');
        var msSql = require('../../../oms/server/controllers/sqlQuery');
        var stInfo = require('../../../oms/server/controllers/stInfo');
        var selftestMgr = new selftestManager(utils);

    var getStatus = function(){
      var _ = require('lodash');
      // var Q = require('q');
      var deferred = Q.defer();
      // var dcQueryList = [];
      // var bakQueryList = [];
      // var aRunList = [];
      // var errQueryList = [];

      function printErr(err) {
        console.log(err);
        deferred.reject(err);
      };

      var queryStartT = new Date();
      var stStatusQueryList = [];
      msSql
        .getAllMachineStatus(utils)
        .then(function(recordSet){
          // recordSet.forEach(function (element, index) {
          //   dcQueryList.push(ovenStatus.checkLogStatus(tools.transOvenId(element.id)));
          // });
          _.forEach(recordSet, function(element, idx){
            var stStatusParams = {
              'query': {"OVENID": element.id},
              'machine_status': element.status,
              'lotnum' : element.lotnum ,
              'start_time': element.start_time
            };
            stStatusQueryList.push(stInfo.getOneOvenStStatus(stStatusParams))
          });
          Q.all(stStatusQueryList).then(function(stStatusResults){
            stStatusResults.forEach(function (item, idx) {
              recordSet[idx].st_status = item['statusStr'];
              if(item['isExpired']){
                recordSet[idx].st_expired= item['isExpired'];
                recordSet[idx].st_status='一週未執行Selftest'
                }
            });
            deferred.resolve(recordSet);
            })
            .then(null, printErr);;
        })
        .then(null,printErr);
      
      return deferred.promise;      
    };

    app.get('/api/selftest/getovenstatus', function(req, res, next) {
      getStatus().then(function(result){
        res.send(result);
      }).then(null, function(err){
        console.log(err);
        res.status(500).send(err);
      });
    });
        app.post('/api/selftest/getsearchfield', function(req, res, next){
            if(req.body.field =='OVENID' || req.body.field =='TYPE'){ // mongo
                utils.ctrls.mongoQuery.getSearchField(selftestMgr.searchColl, req.body, selftestMgr.searchPathMapping, selftestMgr.arrayFields)
                .then((data) => res.json(data))
                .catch((err) => res.status(500).send({'error': err}));
            }
            else{    // mbi 
                selftestMgr.getSearchField(req.body)
                .then((data) => res.json(data))
                .catch((err) => res.status(500).send({'error': err}));
            }
        });


        app.post('/api/selftest/oven', function(req, res, next) {
            selftestMgr.queryAggregate(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });
        app.post('/api/selftest/getslot', function(req, res, next) {
            // console.log(req.body)
            selftestMgr.getSlotAggregate(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });
        
        app.post('/api/selftest/get_all_oven_config', function(req, res, next) {
            selftestMgr.getAllOvenConfigue(req, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });

        app.post('/api/selftest/getdailystatus', function(req, res, next) {
            selftestMgr.getDailyStatus(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });


        app.post('/api/selftest/getmbidata', function(req, res, next) {
            req.setTimeout(180000);
            selftestMgr.getMultiMbiOvenByTime(req.body)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });

        app.post('/api/selftest/getsinglembidata', function(req, res, next) {
            console.log("getsinglembidata")
            console.log(req.body)
            selftestMgr.getSingleMbiByTime(req.body.stLog[0], res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));

        });


        app.post('/api/selftest/uploadOvenConfigue', function(req, res, next) {
            // console.log("#############################")
            selftestMgr.uploadOvenConfigue(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });
    };
})();
