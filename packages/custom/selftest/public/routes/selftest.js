(function() {
    'use strict';

    function Selftest($stateProvider) {
        $stateProvider.state('selftest main page', {
            url: '/selftest/main',
            templateUrl: 'selftest/views/index.html',
            controller:SelftestController
        });
    }

    angular
        .module('mean.selftest')
        .config(Selftest);

    Selftest.$inject = ['$stateProvider'];

})();
