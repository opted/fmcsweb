(function(){
  'use strict';

  function daySummaryChart(_,sqs){
    return {
      restrict: 'E',
      scope: {
        data:"="
      },
      // templateUrl: 'selftest/views/mtbfDataPanel.html',
      template: '<div style="width: 100%;height: 500px;"><div></div></div>',
      link: function(scope, element, attrs){
        scope.status=['waiting','running','fail','complete'];
        scope.testGraphs=[{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#B54434",
            "lineColor": "#B54434",
            "legendValueText": "[[value]]",
            // "labelText": "[[value]]",
            "labelPosition": "top",
            // "color": "#3366CC",
            // "colorField":'color',
            "fontSize": 15,
            "title": "執行Fail",
            "type": "column",
            "valueField": "fail",
            "valueAxis": "testCount"
          },{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#3A8FB7",
            "lineColor": "#3A8FB7",
            "legendValueText": "[[value]]",
            // "labelText": "[[value]]",
            "labelPosition": "top",
            // "color": "#3366CC",
            // "colorField":'color',
            "fontSize": 15,
            "title": "執行中",
            "type": "column",
            "valueField": "running",
            "valueAxis": "testCount"
          },{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#24936E",
            "lineColor": "#24936E",
            "legendValueText": "[[value]]",
            // "labelText": "[[value]]",
            "labelPosition": "top",
            // "color": "#3366CC",
            // "colorField":'color',
            "fontSize": 15,
            "title": "執行完成",
            "type": "column",
            "valueField": "complete",
            "valueAxis": "testCount"
          },{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#6F3381",
            "lineColor": "#6F3381",
            "legendValueText": "[[value]]",
            // "labelText": "[[value]]",
            "labelPosition": "top",
            // "color": "#3366CC",
            // "colorField":'color',
            "fontSize": 15,
            "title": "待執行",
            "type": "column",
            "valueField": "waiting",
            "valueAxis": "testCount"
          },];

        var chartOption = {
          "type": "serial",
          "theme": "none",
          "legend": {
            "equalWidths": false,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 120,
            "position":"right"
          },
          "dataProvider": [],
          "valueAxes": [{
            "id": "testCount",
            "axisAlpha": 1,
            "gridAlpha": 0.1,
            "position": "left",
            "title": "次數",
            "titleFontSize": 20
          }],
          "graphs": scope.testGraphs,
          "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": true
          },
          "categoryField": "DATETIME",
          "export": {
            "enabled": true
          },
          "categoryAxis": {
            "autoRotateAngle": 45,
            "autoRotateCount": 20
          },
          "titles":[{text: "",size:15}],

        };

        var target = element[0].children[0];

        var chart = AmCharts.makeChart(target, chartOption);
        scope.drillLevel=0;
        chart.clickTimeout = 0;
        chart.lastClick = 0;
        chart.doubleClickDuration = 300;

        function clickHandler(event) {
                var ts = (new Date()).getTime();
                if ((ts - chart.lastClick) < chart.doubleClickDuration) {  // [[ Double click ]]
                  if (chart.clickTimeout) {
                    clearTimeout(chart.clickTimeout);
                  }
                  chart.lastClick = 0; // reset last click
                    // drillDown(event);
                } else {  // [[ Single click ]]
                  chart.clickTimeout = setTimeout(() => filterChartData(event), chart.doubleClickDuration);
                }
                chart.lastClick = ts;
              }

        // chart.addListener("clickGraphItem", clickHandler);
        

        scope.$watch('data', function (nv,ov) {
          // console.log(scope.groupBy)
          // scope.drillLevel=0;
          // chart.titles[0].text="";
          // setGraphs(scope.groupBy)
          
          updateChart();
        });


    function processChartData(rows){
        return _(rows)
                .sortBy('DATETIME')
                .groupBy('DATETIME')
                .map(function(r,d){
                    var o={};
                    o['DATETIME']=d;
                    o['waiting']=_.filter(r,{'ST_STATUS':0}).length;
                    o['running']=_.filter(r,{'ST_STATUS':1}).length;
                    o['complete']=_.filter(r,{'ST_STATUS':2}).length;
                    o['fail']=_.filter(r,{'ST_STATUS':3}).length;

                    return o; 
                  }).value()

                
    }

        var updateChart = function(){

          chart.dataProvider = processChartData(scope.data)
          chart.validateData();
          chart.validateNow();
        }

          var customGroupBy = function(row,key){
            if(key=='DATETIME'){ return row[key].slice(5,10)}
            return row[key]
          }
          var dateGroupBy = function(row){
              var sd = (new Date(row['DATETIME'])).setHours(0,0,0)
              var d = new Date(row['DATETIME'])

              return (d>sd)? d.toLocaleDateString().slice(5,10) 
                    : (new Date(sd-86400000)).toLocaleDateString().slice(5,10)  
          }
        scope.getChart = function(){
          return chart;
        }
        chart.validateData();
        chart.validateNow();

      }
    };
  };
  
  angular.module('mean.selftest').directive('daySummaryChart', daySummaryChart);
   daySummaryChart.$inject = ["_","selftestQueryService"];
})();