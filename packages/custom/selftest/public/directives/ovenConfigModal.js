var reportOpenFile = undefined;
(function(){
  'use strict';

  function ovenConfigModal(_,$http,$uibModal,MeanUser){
    return {
        restrict: 'E',
        scope: {
          btnStyle: '@'
        },
        template: '<button type="button" class="btn btn-default" style="{{btnStyle}}" ng-click="ovenConfigModal()">上傳Oven Configure</button>', 
        link: function (scope, element, attrs) {
          
        },//link
      controller:['$scope','$window',function($scope,$window){
        $scope.ovenConfigModal = function(){
          $http.post('/api/selftest/get_all_oven_config', {}, {})
                    .then(function(result){
                        // $scope.requestStatus = "處理回傳資料...";
                        $scope.ovenRows = result.data;
                        $scope.ovenInfoGrid.api.setRowData(result.data);
                    }, function(err){
                        // $window.alert("Server failed...");
                        $scope.uploadStatus = "重新載入失敗";
                    });
          $scope.modalInstance = $uibModal.open({
            templateUrl: 'selftest/views/ovenConfigModal.html',
            size: 'md',
            scope: $scope
          });
        };
        $scope.ovenInfoGrid = {
            columnDefs: [
                {headerName: "OVEN ID", field: "OVENID", width: 160},
                {headerName: "TYPE", field: "TYPE", width: 160},
                {headerName: "SLOT", field: "SLOT", width: 160}
            ],
            rowData:[],
            groupUseEntireRow: false,
            enableSorting: true,
            enableColResize: true,
            suppressExcelExport :false
        };
        $scope.uploadStatus = undefined;
        // $scope.nvUploadHint = '\
        //   <ol>\
        //   <li>上傳csv檔編碼必須為UTF-8</li>\
        //   <li>必定要有Header且Header名稱必須正確，參考以下上傳範例</li>\
        //   <li><p>上傳csv檔格式範例如下:</p>\
        //   CustomerName,Family,Setting<br>\
        //   nVIDIA,T124AKV,Standard<br>\
        //   nVIDIA,GP106AK,Customize</li></ol>\
        //   '
        // $http.post('/api/selftest/get_all_oven_config', {}, {})
        //     .then(function(result){
        //         // console.log(result.data)
        //         // $scope.requestStatus = "處理回傳資料...";
        //         $scope.ovenRows = result.data;
        //         $scope.ovenInfoGrid.rowData = result.data;
        //     }, function(err){
        //         $window.alert("Server failed...");
        //         console.log(err)
        //     });
        $scope.storeOvenConfigue = function(){
          var file = document.getElementById('importFile').files[0],
          reader = new FileReader(); 
          reader.onloadend = function(e){
              var xlsdata = e.target.result;
              var workbook = XLSX.read(xlsdata, {
                type: 'binary'
              });
              workbook.SheetNames.forEach(function(sheetName) {
                // Here is your object
                var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                $scope.uploadXmlData = JSON.stringify(XL_row_object);
                // console.log($scope.uploadXmlData);
              })      
            };
          reader.readAsBinaryString(file);
        };
        $scope.uploadOvenConfigue = function(){
            
            $http.post('/api/selftest/uploadOvenConfigue', $scope.uploadXmlData)
            .then(function(result){
                $scope.uploadStatus = "匯入成功";

                  $http.post('/api/selftest/get_all_oven_config', {}, {})
                  .then(function(result){
                      // $scope.requestStatus = "處理回傳資料...";
                      $scope.ovenRows = result.data;
                      $scope.ovenInfoGrid.api.setRowData(result.data);
                  }, function(err){
                      // $window.alert("Server failed...");
                      $scope.uploadStatus = "重新載入失敗";
                  });
                  // $window.alert("匯入成功")
              }, function(err){
                $scope.uploadStatus = "匯入失敗，請確定格式正確";
                  // $window.alert("Server failed...");
              });
        }
        
        // reportOpenFile = function(event) {
        //   var input = event.target;
        //   // filename = input.value
        //   if(!_.endsWith(input.value,'.csv')){
        //     $scope.$apply(function(){
        //       $scope.uploadStatus = '上傳失敗! 非CSV檔!';
        //     });
        //     return;
        //   }
        //   else{
        //     var reader = new FileReader();
        //     reader.onload = function(){
        //       var csvtext = reader.result.replace('\ufeff',''); // remove bom code
        //       $http.post('/api/rebi/uploadconfig',{'csvtext':csvtext}).then(
        //         function(response){
        //           $scope.uploadStatus = '上傳成功!';
        //           $scope.configText = csvtext;
        //           console.log('success!');
        //         },function(err){
        //           $scope.uploadStatus = '上傳失敗! '+err.data;
        //       });
        //     };
        //     reader.readAsText(input.files[0]);
        //   }
        // };
        $scope.close = function(){
          $scope.uploadStatus = undefined;
          $scope.modalInstance.dismiss('cancel');
        }
        if(MeanUser.userInfo())
          $scope.username = MeanUser.userInfo().local.name;
        else
          $scope.username = undefined;
      }]
    };
  };

  angular.module('mean.selftest').directive('ovenConfigModal', ovenConfigModal);
  ovenConfigModal.$inject = ['_','$http','$uibModal','MeanUser'];
})();