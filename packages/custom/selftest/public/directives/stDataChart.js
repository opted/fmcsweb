(function(){
  'use strict';

  function stDataChart(_,sqs){
    return {
      restrict: 'E',
      scope: {
        data:"=",
        groupBy:"=",
        dailyData:"=",
        queryParam:"="
      },
      template: '<div style="width: 100%;height: 500px;"><div></div></div>',
      link: function(scope, element, attrs){

        scope.completeRateGraphs=[{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#99CCFF",
            "lineColor": "#0066CC",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "labelPosition": "top",
            "colorField":'color',
            "fontSize": 15,
            "title": "Configure Slot",
            "type": "column",
            "valueField": "SLOT",
            "valueAxis": "testCount"
          },{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#FB966E",
            "lineColor": "#CC543A",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "labelPosition": "top",
            "colorField":'color',
            "fontSize": 15,
            "title": "Pass Slot",
            "type": "column",
            "valueField": "PASS_SLOT",
            "valueAxis": "testCount"
          },{
            "bullet": "triangleUp",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "balloonText": "[[value]]%",
            "bulletColor": "#FF6600",
            "lineColor": "#FF9933",
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]%",
            "labelText": "[[value]]%",
            "labelPosition": "left",
            "color": "#FF0000",
            "fontSize": 15,
            "title": "完成度",
            "fillAlphas": 0,
            "valueField": "C_RATE",
            "valueAxis": "completeRate"
          }];

        scope.sumFailGraphs=[{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#FB966E",
            "lineColor": "#CC543A",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "labelPosition": "top",
            "colorField":'color',
            "fontSize": 15,
            "title": "加總 - Fail",
            "type": "column",
            "valueField": "FAIL",
            "valueAxis": "testCount"
          },{
            "bullet": "triangleUp",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "balloonText": "[[value]]%",
            "bulletColor": "#FF6600",
            "lineColor": "#FF9933",
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]%",
            "labelText": "[[value]]%",
            "labelPosition": "left",
            "color": "#FF0000",
            "fontSize": 15,
            "title": "完成度",
            "fillAlphas": 0,
            "valueField": "F_RATE",
            "valueAxis": "completeRate"
          }];
 

          scope.ZBFailGraphs=[{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#FB966E",
            "lineColor": "#CC543A",
            // "legendPeriodValueText": "total: [[value.sum]]",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "labelPosition": "top",
            // "color": "#3366CC",
            "colorField":'color',
            "fontSize": 15,
            "title": "加總 - Fail",
            "type": "column",
            "valueField": "FAIL",
            "valueAxis": "testCount"
          },{
            "bullet": "triangleUp",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "balloonText": "[[value]]%",
            "bulletColor": "#FF6600",
            "lineColor": "#FF9933",
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]%",
            "labelText": "[[value]]%",
            "labelPosition": "left",
            "color": "#FF0000",
            "fontSize": 15,
            "title": "Fail Rate",
            "fillAlphas": 0,
            "valueField": "F_RATE",
            "valueAxis": "failRate"
          }];

        scope.testGraphs=[{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#99CCFF",
            "lineColor": "#0066CC",
            // "legendPeriodValueText": "total: [[value.sum]]",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "labelPosition": "top",
            // "color": "#3366CC",
            "colorField":'color',
            "fontSize": 15,
            "title": "加總 - Total",
            "type": "column",
            "valueField": "TOTAL",
            "valueAxis": "testCount"
          },{
            "alphaField": "alpha",
            "balloonText": "[[value]] 次",
            "dashLengthField": "dashLength",
            "fillAlphas": 0.8,
            "fillColors": "#FB966E",
            "lineColor": "#CC543A",
            "legendValueText": "[[value]]",
            "labelText": "[[value]]",
            "labelPosition": "top",
            "colorField":'color',
            "fontSize": 15,
            "title": "加總 - Fail",
            "type": "column",
            "valueField": "FAIL",
            "valueAxis": "testCount"
          },{
            "bullet": "triangleUp",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            "balloonText": "[[value]]%",
            "bulletColor": "#FF6600",
            "lineColor": "#FF9933",
            "dashLengthField": "dashLength",
            "legendValueText": "[[value]]%",
            "labelText": "[[value]]%",
            "labelPosition": "left",
            "color": "#FF0000",
            "fontSize": 15,
            "title": "Pass Rate",
            "fillAlphas": 0,
            "valueField": "P_RATE",
            "valueAxis": "passRate"
          }];

        var chartOption = {
          "type": "serial",
          "theme": "none",
          "legend": {
            "equalWidths": false,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 120
          },
          "dataProvider": [],
          "valueAxes": [{
            "id": "testCount",
            "axisAlpha": 1,
            "gridAlpha": 0.1,
            "position": "left",
            "title": "Slot 計次",
            "titleFontSize": 20
          },{
            "id": "completeRate",
            "axisAlpha": 1,
            "gridAlpha": 0,
            "position": "right",
            "unit":"%",
            "title": "完成度",
            "titleFontSize": 20
          },{
            "id": "failRate",
            "axisAlpha": 1,
            "gridAlpha": 0,
            "position": "right",
            "unit":"%",
            "title": "Fail Rate",
            "titleFontSize": 20
          },{
            "id": "passRate",
            "axisAlpha": 1,
            "gridAlpha": 0,
            "position": "right",
            "unit":"%",
            "title": "Pass Rate",
            "titleFontSize": 20
          }],
          "graphs": [],
          "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": true
          },
          "categoryField": "DATETIME",
          "export": {
            "enabled": true
          },
          "categoryAxis": {
            "autoRotateAngle": 45,
            "autoRotateCount": 20
          },
          "titles":[{text: "",size:15}],

        };

        var target = element[0].children[0];

        var chart = AmCharts.makeChart(target, chartOption);
        scope.drillLevel=0;
        chart.clickTimeout = 0;
        chart.lastClick = 0;
        chart.doubleClickDuration = 300;

        function clickHandler(event) {
                var ts = (new Date()).getTime();
                if ((ts - chart.lastClick) < chart.doubleClickDuration) {  // [[ Double click ]]
                  if (chart.clickTimeout) {
                    clearTimeout(chart.clickTimeout);
                  }
                  chart.lastClick = 0; // reset last click
                    // drillDown(event);
                } else {  // [[ Single click ]]
                  chart.clickTimeout = setTimeout(() => filterChartData(event), chart.doubleClickDuration);
                }
                chart.lastClick = ts;
              }

        chart.addListener("clickGraphItem", clickHandler);
        

        scope.$watch('groupBy', function (nv,ov) {
          scope.drillLevel=0;
          chart.titles[0].text="";
          setGraphs(scope.groupBy)
          
          updateChart();
        });

        function filterChartData(event){
          
          scope.drillLevel +=1;
          // 設定所選的 group
          chart.titles[0].text += scope.drillLevel==1? event.item.category:'_'+ event.item.category ;
          if(scope.drillLevel ==1){
            chart.categoryField  = 'OVENID'
            scope.filteredData= _.filter(scope.data,function(r){
              return customGroupBy(r,scope.groupBy)== event.item.category
            })
            
            updateFilteredChart(scope.filteredData)
          }
          else if(scope.drillLevel ==2){
              chart.categoryField  = 'ZB'
              scope.ZBData = _(scope.filteredData).filter({'OVENID':event.item.category}).map('TESTITEM_RECORD').flatten().value();
            updateZBChart(scope.ZBData);

          }
          else if(scope.drillLevel==3){
              chart.categoryField ='TESTITEM';
              updateTestChart(_.filter(scope.ZBData,function(r){return r.ZB == event.item.category}))

          }

        }


    function processChartData(rows){
        return _(rows)
                // .filter(row => row['TOTAL']>0)
                .sortBy(scope.groupBy)
                .groupBy(scope.groupBy=='DATETIME'?dateGroupBy:scope.groupBy)
                .map((r,k) => new Object({
                    'GROUPNAME':k,
                    'PASS_SLOT':_.sumBy(r, 'PASS_SLOT'),
                    'SLOT':_.sumBy(r, row=> parseInt(row['SLOT'])||0),
                    'C_RATE': (100*_.sumBy(r, 'PASS_SLOT')/_.sumBy(r, row=> parseInt(row['SLOT']))).toFixed(2) ,
                    })
                
                )
                .value()
    }
    function processFilteredData(rows){
        return _(rows)
                .groupBy('OVENID')
                .map((r,k) => new Object({
                    'OVENID':k,
                    'FAIL':_.sumBy(r, 'FAIL'),
                    'F_RATE': (100*_.sumBy(r, 'FAIL')/_.sumBy(r, 'TOTAL')).toFixed(2) ,
                    }))
                .value()
    }

    function processZBData(rows){
        return _(rows)
                .groupBy('ZB')
                .map((r,zb) => new Object({
                    'ZB':zb,
                    'PASS':_.sumBy(r, 'P_CNT'),
                    'TOTAL':_.sumBy(r, 'T_CNT'),
                    'FAIL':_.sumBy(r, 'T_CNT') - _.sumBy(r, 'P_CNT'),
                    'F_RATE': (100*_.sumBy(r, 'P_CNT')/_.sumBy(r, 'T_CNT')).toFixed(2) ,
                    }))
                .value()
    }

    function processTestData(rows){
        return _(rows)

                .groupBy('TESTITEM')
                .map((r,t) => new Object({
                    'TESTITEM':t,
                    'PASS':_.sumBy(r, 'P_CNT'),
                    'TOTAL':_.sumBy(r, 'T_CNT'),
                    'FAIL':_.sumBy(r, 'T_CNT') - _.sumBy(r, 'P_CNT'),
                    'P_RATE': (100*_.sumBy(r, 'P_CNT')/_.sumBy(r, 'T_CNT')).toFixed(2) ,
                    }))
                .value()
    }

    function groupChartData(rows,groupBy){
        return _(rows)
            .groupBy(groupBy)
            .map(function(a,k){
                   var record  = processChartData(a);
                   var o = new Object({});
                    _.forEach(record,function(t){
                             if(t['FAIL']>0)
                             o[t['TESTITEM']] = t['FAIL']
                   })
                      o[groupBy]=k;            
                return o;
            })
            .filter(group => _.keys(group).length>1)
            .value()
    }

        var updateTestChart = function(data){
          chart.graphs= scope.testGraphs;
          chart.dataProvider = processTestData(data)
     
          chart.validateData();
          chart.validateNow();
        }

        var updateZBChart = function(data){
          chart.graphs= scope.ZBFailGraphs;
          chart.dataProvider = processZBData(data)
          chart.validateData();
          chart.validateNow();
          }

        var updateFilteredChart = function(data){
          chart.graphs= scope.sumFailGraphs;
          chart.dataProvider = processFilteredData(data)
          chart.validateData();
          chart.validateNow();
          }

        var setGraphs = function(groupBy){
          chart.graphs = scope.completeRateGraphs;
        }  

        var updateChart = function(){
          chart.categoryField ='GROUPNAME'
          chart.dataProvider = processChartData(scope.data)
          chart.validateData();
          chart.validateNow();
        }

          var customGroupBy = (row,key) => key == 'DATETIME' ? dateGroupBy(row): row[key];

          var dateGroupBy = function(row){
              var sd = (new Date(row['DATETIME'])).setHours(0,0,0)
              var d = new Date(row['DATETIME'])

              return (sd<d)? d.toLocaleDateString().slice(5,10) 
                    : (new Date(sd-86400000)).toLocaleDateString().slice(5,10)  
          }
        scope.getChart = function(){
          return chart;
        }
        chart.validateData();
        chart.validateNow();

      }
    };
  };
  
  angular.module('mean.selftest').directive('stDataChart', stDataChart);
   stDataChart.$inject = ["_","selftestQueryService"];
})();