(function() {
    'use strict';

    function Selftest($http, $q) {
        return {
            name: 'selftest',
            checkCircle: function(circle) {
                var deferred = $q.defer();

                $http.get('/api/selftest/example/' + circle).success(function(response) {
                    deferred.resolve(response);
                }).error(function(response) {
                    deferred.reject(response);
                });
                return deferred.promise;

            }
        };
    }

    angular
        .module('mean.selftest')
        .factory('Selftest', Selftest);

    Selftest.$inject = ['$http', '$q'];

})();
