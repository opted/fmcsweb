(function(){
    'use strict';
    function srchSelftestOpt(_,utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("機型", "TYPE"),
                uOpt.genField("爐號", "OVENID"),
                uOpt.genField("Driver ID", "driver_id"),
                uOpt.genField("內批(Lotno)", "lotnum"),
                uOpt.genField("程式名稱(Bipgm)", "bipgm"),
                // uOpt.genField("BOARD", "BOARD", "string", false, false),
            ],
            getMatch: uOpt.getMatch
        };
    };

    angular.module('mean.selftest').factory('srchSelftestOpt', srchSelftestOpt);
    srchSelftestOpt.$inject = ['_','utilsSrchOpt'];
})();