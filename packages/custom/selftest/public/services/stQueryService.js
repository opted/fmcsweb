(function(){
  'use strict';
  function selftestQueryService(_, $http, $window){
    var isSearching = false;
    var ovenConfig=[];
    var status = {
      str: "",
      err: ""
    };
    var queryService ;

    function replaceAllHyphen(str) {
    return str.replace(new RegExp('-', 'g'), '');
    }
    var config = {
      eventHandlers: {
      progress: function(event){
        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
        status.str = "正在下載資料...";
      },
      // readystatechange: function(event){
      //   console.log("STATUS:", event);
      // }
      }
    };
    return {
      isSearching: () => isSearching,
      status: status,
      
      queryOvenConfig: function(oven, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/selftest/get_all_oven_config', {}, {})
        .then(function(result){
          status.str = "處理回傳資料...";

          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
      query: function(queryParam, callback){
        isSearching = true;
        queryService = this,
        status.str = "等待 Server 回應...";
        
        return $http.post('/api/selftest/getslot', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          // console.log(result.data)
          var rows=result.data;

          return rows;
      
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
///api/oms/oven/status

      getOvenStatus: function( callback){
        isSearching = true;
        queryService = this,
        status.str = "等待 Server 回應...";
        
        return $http.get('/api/selftest/getovenstatus')
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          console.log(result.data)
          var rows=result.data;

          return rows;
      
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },

      queryOven: function(queryParam, callback){
        isSearching = true;
        queryService = this,
        status.str = "等待 Server 回應...";
        
        return $http.post('/api/selftest/oven', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          // console.log(result.data)
          var rows=result.data;

          return rows;
      
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },

      queryMBI: function(queryParam,stInfos,ovenConfig,isSlotLevel, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        console.log(stInfos)
        queryParam.stLog= _(stInfos)
                            .groupBy('ISODATE')
                            .map((r,k) => new Object({
                                'OVENID':_.head(_.keys(_.groupBy(r,'OVENID'))),
                                'ISODATE':k,
                            }))
                            .value()

        return $http.post('/api/selftest/getmbidata', queryParam, config)
        .then(function(result){
          status.str = "處理Lot資料...";
          // callback(result.data);
          console.log(result.data)
          var mibResult=result.data;
          var group = _.map(queryParam.stLog,'ISODATE');
          _.forEach(stInfos,function(r,idx){
            var logDate = stInfos[idx]['ISODATE']
             var groupIdx = _.indexOf(group,logDate)
             // console.log(groupIdx,r.ISODATE)
             var findGroup=mibResult[groupIdx]
                  var match = _.find(findGroup,function(record){
                    // var mbiTime= new Date(record.min_time);
                    // var logTime= new Date(logDate);
                      if( (record.min_time>r.ISODATE) && _.includes(record.oven_ID,replaceAllHyphen(r.OVENID))  
                           ){
                      console.log(record.min_time,r.ISODATE)
                  // when data is slot-level, compare to z&b
                        if ( isSlotLevel 
                            &&record.Slot_ID ==parseInt(r.ZB.slice(3)) 
                            && record.Zone_ID==parseInt(r.ZB[1]))
                             { return record;}
                        else if(isSlotLevel==false)// else compare to oven
                        { 
                        	return record;
                      }   
                        }
                      
                  })
                  // console.log(match)
                     if(match!=undefined){
                      r['lotnum']= _.trimEnd(match.lotnum);
                      // if(isSlotLevel) 
                      r['driver_id']= _.trimEnd(match.driver_id);
                      r['bipgm']= _.trimEnd(match.bipgm);
                  }
                  else {
                      r['lotnum']="-";
                      // r['driver_id']="-";
                      if(isSlotLevel) r['driver_id']="-";
                      r['bipgm']="-";
                  }

    
                    var ovenMatch =_.find(ovenConfig,
                                        o => o['OVENID'] == r['OVENID'])
                    if(ovenMatch){
                        // r['TYPE'] = ovenMatch['TYPE'];
                        // r['SLOT'] = ovenMatch['SLOT']
                        r['C_RATE'] =  ((r['PASS_SLOT']/ovenMatch['SLOT'])*100).toFixed(2)+'%'
                        _.assign(r,ovenMatch)
                    }
                    else {
                      r['TYPE']="-";
                      r['SLOT']="-"
                  }
          })

          var filterMatch = _.pick(queryParam,['driver_id','lotnum','bipgm','TYPE']);

          return _.filter(stInfos,filterMatch);
          // return stInfos;
          // status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
      queryDailySummary: function(queryParam, callback){
        isSearching = true;
        queryService = this,
        status.str = "等待 Server 回應...";
        
        return $http.post('/api/selftest/getdailystatus', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";

          _.forEach(result.data,function(r){
            var dt = new Date(r['DATE']);
            var nt = new Date(dt.setHours(dt.getHours()-12));
            _.set(r,'DATETIME', nt.toLocaleDateString());
          })
          return result.data;
      
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
    };
  };

  angular.module('mean.selftest').factory('selftestQueryService', selftestQueryService);
  selftestQueryService.$inject = ["_", "$http", "$window"];
})();