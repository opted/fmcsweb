
'use strict';   

    /* jshint -W098 */
function getToday(){
  var cur = new Date();  // Get current time
  cur.setHours(0, 0, 0, 0);  // Set to the start of day
  return cur;
}

// function testDay(){
//   var cur = new Date(2017,11,12);  // Get current time
//   cur.setHours(0, 0, 0, 0);  // Set to the start of day
//   return cur;
// }

var ted;
function SelftestController(_,$scope, Global, Selftest, $stateParams, $window, $http, srchSelftestOpt,sqs) {
    $scope.global = Global;
    ted = $scope;
    $scope.package = {
        name: 'selftest'
    };
    $scope.selected = {
        "from": getToday(),  // Set default start day
        "to": getToday(),
        "cust": null,
        "custlotno": null,
        "dutlog": null
    };

    angular.element(document).ready(function () {
        // document.getElementById('msg').innerHTML = 'Hello';
         sqs.queryOvenConfig().then(function(ovenConfig){
             $scope.ovenConfig=ovenConfig
        })


    });

   
    $scope.boardInfoGrid = {
        columnDefs: [
            {headerName: "Date Time", field: "DATETIME", width: 120},
            {headerName: "LotNo", field: "lotnum", width: 120},
            {headerName: "Driver ID", field: "driver_id", width: 100,hide:true},
            {headerName: "機型", field: "TYPE", width: 80},
            {headerName: "Oven ID", field: "OVENID", width: 80},
            {headerName: "Configure_Slot", field: "SLOT", width: 80},
            {headerName: "完成度", field: "C_RATE", width: 80},
            {headerName: "Pass Slot", field: "PASS_SLOT", width: 80},
            {headerName: "程式名稱", field: "bipgm", width: 240},
            {headerName: "DLL Ver", field: "DLLVER", width: 80},
            {headerName: "ST Ver", field: "STVER", width: 80}
        ],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        suppressExcelExport :false
    };

    $scope.ovenStatusGrid = {
        columnDefs: [
            {headerName: "爐號", field: "id", width: 80},
            {headerName: "狀態", field: "st_status", width: 220},
        ],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        suppressExcelExport :false
    };

    $scope.slotInfoGrid = {
        columnDefs: [
            {headerName: "Date Time", field: "DATETIME", width: 120},
            {headerName: "LotNo", field: "lotnum", width: 120},
            {headerName: "Driver ID", field: "driver_id", width: 100,hide:true},
            {headerName: "機型", field: "TYPE", width: 80},
            {headerName: "Oven ID", field: "OVENID", width: 80},
            {headerName: "Slot", field: "ZB", width: 80},
            {headerName: "程式名稱", field: "bipgm", width: 240},
            {headerName: "DLL Ver", field: "DLLVER", width: 80},
            {headerName: "ST Ver", field: "STVER", width: 80},
            {headerName: "Channel Test Compare", field: "ChannelTestCompare", width: 80},
            {headerName: "Channel Test Drive", field: "ChannelTestDrive", width: 80},
            {headerName: "FPGA Channel", field: "FPGAChannel", width: 80},
            {headerName: "RAM", field: "RAM", width: 80},
            {headerName: "Voh Check", field: "VohCheck", width: 80}
        ],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        suppressExcelExport :false
    };

    $scope.filterSelected;
    $scope.groupListSelected;
    $scope.firstListSelected= {"name":"日期",id:"DATETIME"};
    $scope.failedmodeSelected;
    $scope.varsSeleted = {
        firstList: [
            {"name":"日期",id:"DATETIME"},
            {"name":"LotNo",id:"lotnum"},
            {"name":"Driver ID",id:"driver_id"},
            {"name":"機型",id:"TYPE"},
            {"name":"Oven ID",id:"OVENID"},
            {"name":"程式名稱",id:"bipgm"},
        ],
        groupList:[],
        failedmodeList:[
        {name: 'RAM', id: 'failedmode'},
        {name: 'Channel Test Compare', id: 'failedmode'},
        {name: 'Voh Check', id: 'failedmode'},
        {name: 'Channel Test Drive', id: 'failedmode'},
        {name: 'FPGA Channel', id: 'failedmode'},
    ]
    }



    $scope.filterDate = function(){
        // _.forEach($scope.slotResult, function(r,idx){ 
        //      $scope.slotResult[idx]= _.omit(r,'ISODATE')
        // })

        //  $scope.slotInfoGrid.api.setRowData($scope.slotResult);
         $scope.exportXLS()

    }

    $scope.changeFailedmode = function(mode){
        $scope.failedmodeSelected = mode.name;
    }

  $scope.exportXLS = function(){
    // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
    if ($scope.boardInfoGrid.api.getModel().getRowCount() < 1){
      $window.alert("資料處理中，請稍後...");
      return;
    }
    var params = {
      fileName: "Selftest_Export.xls"
    };
    $scope.boardInfoGrid.api.exportDataAsExcel(params);
  };

    $scope.changeGroup = function(groupBy){

        $scope.varsSeleted.groupList=[];
        $scope.groupBy = groupBy    
    }

    $scope.filterData = function(filterValue){

        if(filterValue != null && filterValue !=""){
            var key = (_.find($scope.varsSeleted.filterList,option=> option.name == filterValue)).id
             $scope.filterRows = _.filter($scope.rows,function(row){
                return row[key] == filterValue
            })

            $scope.chartData = processChartData($scope.filterRows)
            $scope.groupBy ='TESTITEM' 
            $scope.filterValue= filterValue;
        }

    }     


    $scope.changeShowData = function(groupValue){
        $scope.groupListSelected = groupValue;
        if(groupValue != null && groupValue !=""){
            $scope.chartData = processChartData(_.filter($scope.filterRows,function(row){
                                            return row[$scope.groupBy] == groupValue
                                            })
                                )
            $scope.groupValue= groupValue;
            $scope.groupBy ='TESTITEM' 
        }

    }     

    $scope.uploadXmlData = null;
    $scope.fields = srchSelftestOpt.fields;
    $scope.fieldSearchURL = "/api/selftest/getsearchfield";
    $scope.storeOvenConfigue = function(){
      var file = document.getElementById('importFile').files[0],
      reader = new FileReader(); 
      reader.onloadend = function(e){
          var xlsdata = e.target.result;
          var workbook = XLSX.read(xlsdata, {
            type: 'binary'
          });
          workbook.SheetNames.forEach(function(sheetName) {
            var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
            $scope.uploadXmlData = JSON.stringify(XL_row_object);
          })      };
      reader.readAsBinaryString(file);
    };
    $scope.uploadOvenConfigue = function(){
        $http.post('/api/selftest/uploadOvenConfigue', $scope.uploadXmlData)
        .then(function(result){
                $window.alert("匯入成功")
            }, function(err){
                $window.alert("Server failed...");
            });
    }
    function processChartData(rows){
        return _(rows)
                .groupBy('TESTITEM')
                .map((r,k) => new Object({
                    'TESTITEM':k,
                    'TOTAL':_.sumBy(r, 'TOTAL'),
                    'PASS':_.sumBy(r, 'PASS'),
                    'FAIL':_.sumBy(r, 'FAIL'),
                    'FAILRATE': Math.round((_.sumBy(r, 'FAIL')/_.sumBy(r, 'PASS'))*100,2)
                    }))
                .sortBy('FAIL')
                .reverse()
                .value()
    }
    function groupChartData(rows,groupBy){
        return _(rows)
            .groupBy(groupBy)
            .map(function(a,k){
                   var record  = processChartData(a);
                   var o = new Object({});
                    _.forEach(record,function(t){
                            if(t['FAIL']>0)
                             o[t['TESTITEM']] = t['FAIL']
                   })
                      o[groupBy]=k;            
                return o;
            })
            .filter(group => _.keys(group).length>1)
            .value()
    }

    function groupDailyData(rows,groupBy){
        return _(rows)
            .groupBy('DATETIME')
            .map(function(a,k){
                   // var record  = processChartData(a);
                   var o = new Object({});
                   o['ST_STATUS']=_.sumBy(a,'ST_STATUS')
                   o['DATETIME']=k.slice(5,10);            
                return o;
            })
            .value()
    }
    $scope.slotQuery = function() {

        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }

        queryParam.from = queryParam.from.toISOString();

        if($scope.failedmodeSelected!==undefined)
            queryParam.failedmode = $scope.failedmodeSelected;
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        _.assign(queryParam, srchSelftestOpt.getMatch($scope.fields));

        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在處理下載資料...";
                },
            }
        };

        sqs.query(queryParam)
          .then(function(result){

            sqs.queryMBI(_.cloneDeep(queryParam),result,$scope.ovenConfig,true)
            .then(function(lotInfo){

                $scope.slotResult = lotInfo;
                $scope.exportDataNum = $scope.slotResult.length;
                $scope.slotInfoGrid.api.setRowData($scope.slotResult); 
                $scope.requestStatus = "Done";

            },function(mbiErr){
              console.log(mbiErr)
            })
          },function(err){
              console.log(err)
            })

    };

    $scope.getOvenStatus = function(){
        // console.log('getOvenStatus')
        sqs.getOvenStatus().then(function(r){
            $scope.ovenStatusRows=r
            $scope.ovenStatusGrid.api.setRowData($scope.ovenStatusRows);
            // console.log(r);
        },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              $scope.isSearching = false;
              console.log(eErr)
            });
    }  
    $scope.ovenQuery = function() {
        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        $scope.groupBy="";
        if($scope.failedmodeSelected!==undefined)
            queryParam.failedmode = $scope.failedmodeSelected;
        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };

  
        $scope.requestStatus = "等待 Server 回應...";
        $scope.isSearching = true;
        var startTime = new Date();
        $scope.queryTime = "";
        $scope.rowNum = undefined;
        _.assign(queryParam, srchSelftestOpt.getMatch($scope.fields));
        sqs.queryOven(queryParam)
          .then(function(result){

            sqs.queryMBI(_.cloneDeep(queryParam),result,$scope.ovenConfig,false)
            .then(function(lotInfo){

                $scope.result = lotInfo
                $scope.groupBy = 'DATETIME';
                $scope.chartData = _.filter(lotInfo,_.pick(queryParam,['driver_id','lotnum','bipgm','TYPE']))

                $scope.rows = lotInfo;

                $scope.boardInfoGrid.api.setRowData($scope.rows);
                $scope.rowNum = $scope.rows.length;
                $scope.isSearching = false;
                $scope.requestStatus = "查詢完成";
                $scope.queryTime = new Date() - startTime;
                // $scope.slotQuery()
            },function(mbiErr){
              $scope.requestStatus = '查詢失敗, '+err.data;
              $scope.isSearching = false;
              console.log(mbiErr)
            })
          },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              $scope.isSearching = false;
              console.log(err)
          })};

    $scope.dailySummary = function(){
        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };
        $scope.getOvenStatus()
        $scope.requestStatus = "等待 Server 回應...";
        $scope.isSearching = true;
        var startTime = new Date();
        $scope.queryTime = "";
        // _.assign(queryParam, srchSelftestOpt.getMatch($scope.fields));
        sqs.queryDailySummary(queryParam)
          .then(function(result){
            // console.log(result)
                $scope.daySummaryData=result
                $scope.isSearching = false;
                $scope.requestStatus = "查詢完成";
                $scope.queryTime = new Date() - startTime;
          },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              $scope.isSearching = false;
              console.log(err)
          });        
    }
}

angular
    .module('mean.selftest')
    .controller('SelftestController', SelftestController);

SelftestController.$inject = ['_','$scope', 'Global', 'Selftest', '$stateParams', '$window', '$http', 'srchSelftestOpt','selftestQueryService'];

