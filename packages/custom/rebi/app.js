'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Rebi = new Module('rebi');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Rebi.register(function(app, auth, database, utils) {
  // Iinitilize passport
  var session = require('express-session');
  var passportOriginal = require('passport');
  // Need to initial a passport.js first
  var passport = require('../../core/users/server/config/passport.js')(passportOriginal);
  app.use(session({ secret: 'biis'}));
  app.use(passport.initialize());
  app.use(passport.session());
  //We enable routing. By default the Package Object is passed to the routes
  Rebi.routes(app, auth, database, utils);
  
  Rebi.aggregateAsset('css', '../css/rebi.css');
  
  Rebi.angularDependencies(['agGrid']);
  return Rebi;
});
