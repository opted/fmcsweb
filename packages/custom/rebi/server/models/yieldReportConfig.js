'use strict';

/**
 * Module dependencies.
 */
var mongoose  = require('mongoose'),
    Schema    = mongoose.Schema;

/**
 * UserProfile Schema
 */

var reportConfigSchema = new Schema({
	OWNER:String,
	FAMILY:String,
  CUSTOMERNAME:String,
  SETTING:String
}, {
  collection: 'YieldReportConfig',
  read: 'secondaryPreferred'
});
reportConfigSchema.index({OWNER:1});
reportConfigSchema.index({FAMILY:1});
reportConfigSchema.index({CUSTOMERNAME:1});
mongoose.model('YieldReportConfig', reportConfigSchema);