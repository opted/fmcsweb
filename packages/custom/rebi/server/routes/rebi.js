(function() {
  'use strict';
  /* jshint -W098 */
  var rebiCtrl = require('../controllers/rebi');
  var yieldReportCtrl = require('../controllers/yieldreport');
  var shiftCtrl = require('../controllers/rebishift');
  module.exports = function(Rebi, app, auth, database,utils) {
    // I can't figure out why class constructor not work, use this function to replace it
    rebiCtrl.initMesConnection(utils);
    yieldReportCtrl.initMesConnection(utils);
    shiftCtrl.initMesConnection(utils);
    app.post('/api/rebi/test', rebiCtrl.example);
    app.post('/api/rebi/uploadconfig', rebiCtrl.uploadConfig);
    app.get('/api/rebi/loadconfig', rebiCtrl.loadConfig);
    app.post('/api/rebi/shiftmap', shiftCtrl.getShiftMapChart);
    app.post('/api/rebi/mes/getsearchfield', rebiCtrl.mesGetSearchField);
    app.post('/api/rebi/mes/getmesmultiselection', rebiCtrl.getMesMultiSelection);
    app.post('/api/yieldreport/searchyreport', yieldReportCtrl.searchYieldReport);
    app.post('/api/rebi/searchrebi', function(req,res,next){
      req.body.rebionly = true;
      yieldReportCtrl.searchYieldReport(req,res,next);
    });
    app.post('/api/yieldreport/download', yieldReportCtrl.downloadYieldReport);
  };
})();
