'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Q = require('q'),
    _ = require('lodash');
var oqtool = require('./odbQueryTool.js'),
    yieldreport = require('./yieldreport.js');
var chtime = function(sdate,tag){console.log(tag+(new Date().valueOf()-sdate)/1000)};
var UserProfile = mongoose.model('UserProfile');
var ReportConfig = mongoose.model('YieldReportConfig');
var gvar = require('../../../utils/server/controllers/globalVariables');

var mesconn = undefined;
exports.initMesConnection = function(utils){
  mesconn = utils.ctrls.mes;
};
var getBasicWeek = function(date,janone){
  return Math.ceil((((date - janone) / 86400000) - 1 + janone.getDay()) / 7);
}
var getWeek = function(date) {
  // 星期一 00:00:00 會被視為上一週的最後一秒，強制調整秒數為 1
  date  = new Date(new Date(date).setSeconds(1));
  var janone = new Date(date.getFullYear(), 0, 1);
  var addnum = 0;
  // week range: Mon ~ Sun
  // 若當年的1月1日算成 week 0，需要自動加1
  if(getBasicWeek(new Date(date.getFullYear().toString()),janone)==0)
    addnum=1;
  return date.getUTCFullYear()+'W'+_.padStart(getBasicWeek(date,janone)+addnum,2,'0');
}
/*var getWeek = function(date){
  var janone = new Date(date.getFullYear(), 0, 1);
  var week = Math.ceil((((date - janone) / 86400000) + janone.getDay() + 1) / 7);
  return date.getUTCFullYear()+'W'+_.padStart(week,2,'0');
};*/
var catchFuncion = function(err,msg){
  console.log(err.stack);
  return res.status(400).send(msg);
}
exports.getWeek = getWeek;

// 26 英文字母進制
var letterDecimalAdd = function(letters,number){
  letters = letters.toUpperCase();
  var alphabet26 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  // to decimal list
  var dclist = [];
  for(var i=0;i<letters.length;i++){
    dclist.push(alphabet26.indexOf(letters[i]));
  }
  // add number
  dclist[dclist.length-1]+=number;
  // carry
  var olength = dclist.length-1;
  var carrynum = 0;
  for(var i=olength;i>=0;i--){
    dclist[i]+=carrynum;
    carrynum = Math.floor(dclist[i]/26);
    dclist[i] = dclist[i]%26;
  }
  var newdclist = [];
  if(carrynum>0){
    newdclist.push(carrynum);
    var i=0;
    while(carrynum>26){
      newdclist[i]+=carrynum;
      carrynum = Math.floor(newdclist[i]/26);
      newdclist[i] = newdclist[i]%26;
    }
    newdclist = _.reverse(newdclist);
  }
  newdclist.push.apply(newdclist,dclist);
  // dclist to letters
  var newletters = '';
  for(var i in newdclist){
    newletters+=alphabet26[newdclist[i]];
  }
  return newletters;
}

var cutArray = function(list,maxlength){
  var multipleArray = [];
  var sindex =0, eindex = maxlength;
  while(sindex<list.length){
    multipleArray.push(list.slice(sindex,eindex));
    sindex = eindex;
    eindex+=maxlength;
  }
  return multipleArray;
}
exports.cutArray = cutArray;

exports.example = function(req, res, next) {
  //var qcmd = 'select * from HisMFT.TBLENGPRODOPRECIPECONTENT where ATTRIBNO=:attribno order by REVISEDATE desc where rownum=1';
  var qcmd = "select * from HISLFT.TblWipLotData where lotno like :lotno";
  var params = {'lotno':'LH401W5AA1'};
  var qcmd_list = [];
  
  qcmd_list.push(mesconn.exec(qcmd,params));
  Q.all(qcmd_list).then(function(results){
    var outputResult = [];
    results.forEach(function(item){
      outputResult.push(item.rows);
    });
    res.json(outputResult);
  });
};

exports.getLotLogWithPath = function(fstqcmd,fparams,ftype,reportmode,rebionly,res,callback){
  // two report mode: rebi, yreport
  /*
    lotloglist structure:
    [{LOTNO:,OPNO:,GOODQTY:,FAILQTY:,OTHERQTY:},...]
  */
  var lotloglist = [];
  //var pflotno_root_map={};
  var endlotno_list=[];
  var rootlot_dieqty={};
  var testSdate = new Date().valueOf();
  
  // Using lotno find a root and build a tree from MES.TBLWIPLOTSMCONTENT
  
  // 2. Find root lotno (母批) of that bunch of lotno
  // Input: [plotno1,plotno2,...] (sub lotno has first 7 digits)
  // Output: [rootlot1,rootlot2,...]
  var findRoot = function()
  {
    var qcmd_list = [];
    var qcmd = ""
    if(!rebionly){ // query dieqty data will slow down query
      qcmd = "select A.FIRSTLOTNO,A.ENDLOTNO,B.DIEQTY from "+
      "(select MIN(FROMLOTNO) as FIRSTLOTNO,MAX(TOLOTNO) as ENDLOTNO from HISMFT.TBLWIPLOTSMCONTENT where SUBSTR(FROMLOTNO,1,7) in ("+fstqcmd+") group by SUBSTR(FROMLOTNO,1,7)) A "+
      "left join HISMFT.TBLWIPLOTDATA B on A.FIRSTLOTNO=B.LOTNO";
    }
    else
      qcmd = "select MIN(FROMLOTNO) as FIRSTLOTNO,MAX(TOLOTNO) as ENDLOTNO from HISMFT.TBLWIPLOTSMCONTENT where SUBSTR(FROMLOTNO,1,7) in ("+fstqcmd+") group by SUBSTR(FROMLOTNO,1,7)";
    
    mesconn.exec(qcmd,fparams).then(function(root_result){
      for(var i=0;i<root_result.length;i++)
        rootlot_dieqty[root_result[i].FIRSTLOTNO.slice(0,7)] = root_result[i].DIEQTY;
      var flotno_list = _.map(root_result,"FIRSTLOTNO");
      chtime(testSdate,"check 1-1: Root is found ");
      endlotno_list = _.map(root_result,"ENDLOTNO");
      // defined rootlotno is pflotno that is first 7 digits of lotno
      buildTree(flotno_list);
    }).catch(function(err){console.log(err);res.status(500).end(JSON.stringify(err));});// Find Root Lotno
  };
  // 3. Build a tree (recursive function)
  // Input: [rootlot1,rootlot2,...]
  // Output: [rootlot1:tree1,rootlot2:tree2,...]
  var buildTree = function(flotno_list){
    var rootlot_flot_map  = {}; // key is rootlotno, value is flotno
    for(var i in flotno_list)
      rootlot_flot_map[flotno_list[i].slice(0,7)] = flotno_list[i];
    
    var fromto_qcmd = "select FROMLOTNO,TOLOTNO from HISMFT.TBLWIPLOTSMCONTENT where SUBSTR(FROMLOTNO,1,7) in ("+fstqcmd+")";
    mesconn.exec(fromto_qcmd,fparams).then(function(fromto_result){
      chtime(testSdate,"check 1-2: query TBLWIPLOTSMCONTENT ");
      // group fromto_result by flotno_list
      // in: [flotno_list, fromto_result]
      // out: flot_fromto_group = {rootlot1:sub_fromto_result1,rootlot2:sub_fromto_result2}
      var flot_fromto_group = _.groupBy(fromto_result,function(o){
        return rootlot_flot_map[o.FROMLOTNO.slice(0,7)];
      });
      // query TBLWIPLOTLOG by each rootlot group
      var qcmd_list = [];
      var binfield = "";
      for(var binno=1;binno<=8;binno++)
        binfield+="max(case when C.BINNO="+binno+" then C.BINQTY end) BIN"+binno+","
      
      var opno_filter="RegExp_like(A.OPNO,'^BURNIN')";
      if(ftype.yield==1 || ftype.yield==2)
        opno_filter+=" or A.OPNO='MERGE LOT CHECK1'";
      else if(ftype.yield==3)
        opno_filter+=" or A.OPNO='Split-MERGE LOT CHECK1'";

      for(var key in flot_fromto_group){
        var multiLotnoList = cutArray(_.uniq(_.map(flot_fromto_group[key],'TOLOTNO')),500);
        multiLotnoList.forEach(function(lotnolist){
          var find_cmd = "select A.*,B.FTPROGRAMNO,B.PACKAGETYPE,B.DIEQTY,C.CUSTOMERCHISNAME,D.FAMILY"+
            " from (select A.LOTSERIAL,max(A.LOTNO) LOTNO,max(D.CUSTOMERLOTNO) CUSTOMERLOTNOLOG,max(A.OPNO) OPNO,"+
            "max(A.EQUIPMENTNO) EQUIPMENTNO,max(A.CHECKINTIME) CHECKINTIME,max(A.CHECKOUTTIME) CHECKOUTTIME,max(A.INPUTQTY) INPUTQTY,"+
            "max(A.GOODQTY) GOODQTY,max(A.FAILQTY) FAILQTY,max(A.OTHERQTY) OTHERQTY,max(A.SCRAPQTY) SCRAPQTY,max(A.LOSSQTY) LOSSQTY,"+
            "max(D.DEVICENO) DEVICENOLOG,max(D.PRODUCTNO) PRODUCTNOLOG,max(D.RCVSTATUS) RCVSTATUS";
          var find_cmdpost = "group by A.LOTSERIAL) A"+
            " left join HISMFT.TBLWIPLOTDATA B on A.LOTNO=B.LOTNO "+
            " left join Masterdm.DIM_Customer C on B.customerno=C.custno "+
            " left join HISMFT.TBLENGPRODUCTDATA D on B.productno=D.productno";
          if(rebionly){
            if(ftype.rebi==2){
              binfield = _.trimEnd(binfield,',');
              find_cmd = find_cmd +","+ binfield+
              " from HISMFT.TBLWIPLOTLOG A"+
              " left join HisMFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO"+
              " inner join HISMFT.TBLWIPBINCONTENT C on A.LOTSERIAL=C.LOTSERIAL"+
              " where A.LOTNO in :lotnolist"+
              " and ("+opno_filter+") "+find_cmdpost;
            }else{
              find_cmd = find_cmd+
              " from HISMFT.TBLWIPLOTLOG A"+
              " left join HisMFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO"+
              " where A.LOTNO in :lotnolist"+
              " and ("+opno_filter+") "+find_cmdpost;
            }
          }  
          else{
            if(ftype.yield==2)
              opno_filter+=" or RegExp_like(A.OPNO,'^L/S')";
            find_cmd = find_cmd +","+ binfield+
            "max(case when B.ATTRIBNO='BITemp' then B.VALUE end) TEMP,max(case when B.ATTRIBNO='BIhourCycle' then B.VALUE end) BIHOUR"+
            " from HISMFT.TBLWIPLOTLOG A"+
            // where OPNO= 'L/S*' or 'Split-MERGE LOT CHECK1' fields PRODUCTNOLOG,DEVICENOLOG,CUSTOMERLOTNO are null, get these data from TBLWIPLOTDATA
            " left join HisMFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO "+
            " inner join HisMFT.TBLENGPRODOPRECIPECONTENT B on D.PRODUCTNO=B.PRODUCTNO"+
            " inner join HISMFT.TBLWIPBINCONTENT C on A.LOTSERIAL=C.LOTSERIAL"+
            " where A.LOTNO in :lotnolist"+
            " and ("+opno_filter+")"+
            " and (B.ATTRIBNO='BITemp' or B.ATTRIBNO='BIhourCycle') "+find_cmdpost;
          }
          find_cmd = oqtool.insertListToQcmd(find_cmd,{':lotnolist':lotnolist});
          qcmd_list.push(mesconn.exec(find_cmd,[]));
        });
      }
      // above query is faster in query of few days
      if(reportmode=='yreport' && rebionly!=true){
        // HISLFT Table
        //var lftlotnlist = _.uniq(_.map(endlotno_list,function(o){return o.slice(0,7)}));
        var multiLotnoList = cutArray(_.uniq(_.map(endlotno_list,function(o){return o.slice(0,7)})),500);
        multiLotnoList.forEach(function(lftlotnlist){
          var lft_qcmd = "select A.LOTSERIAL,max(A.LOTNO) LOTNO,max(A.CUSTOMERLOTNOLOG) CUSTOMERLOTNOLOG,max(A.OPNO) OPNO,"+
            binfield+
            "max(A.EQUIPMENTNO) EQUIPMENTNO,max(A.CHECKINTIME) CHECKINTIME,max(A.CHECKOUTTIME) CHECKOUTTIME,max(A.INPUTQTY) INPUTQTY,"+
            "max(A.GOODQTY) GOODQTY,max(A.FAILQTY) FAILQTY,max(A.OTHERQTY) OTHERQTY,max(A.SCRAPQTY) SCRAPQTY,max(A.LOSSQTY) LOSSQTY,"+
            "max(A.DEVICENOLOG) DEVICENOLOG,max(A.PRODUCTNOLOG) PRODUCTNOLOG from HISLFT.TBLWIPLOTLOG A"+
            " full outer join HISLFT.TBLWIPBINCONTENT C on A.LOTSERIAL=C.LOTSERIAL where"+
            " SUBSTR(A.LOTNO,1,7) in :lftlotnlist and A.OPNO='Split-MFT'"+
            " group by A.LOTSERIAL";
          lft_qcmd = oqtool.insertListToQcmd(lft_qcmd,{':lftlotnlist':lftlotnlist});
          qcmd_list.push(mesconn.exec(lft_qcmd,[]));
        });
      }
      Q.all(qcmd_list).then(function(lotlog_result){
        chtime(testSdate,"check 1-3: Query build tree data is completed.");
        lotloglist = _.flatten(lotlog_result);
        var lotlog_map = {};
        lotloglist.forEach(function(doc){
          // One lotno maybe has multiple loglog
          // append program name and program ver
          if(doc['FTPROGRAMNO']){
            var lindex = doc['FTPROGRAMNO'].lastIndexOf('-');
            doc['PROGRAMNAME'] = doc['FTPROGRAMNO'].substring(0,lindex);
            doc['PROGRAMVER'] = doc['FTPROGRAMNO'].substring(lindex+1);
          }
          if(lotlog_map[doc.LOTNO]){
            lotlog_map[doc.LOTNO].lotlogs.push(doc);
          }
          else{
            lotlog_map[doc.LOTNO] = {bilvinfo:undefined,lotlogs:[doc]};
          }
        });
        //console.log('### lotloglist',lotloglist);
        buildUpTreeData(flotno_list,flot_fromto_group,lotlog_map);
      })
      .catch(function(err){
        console.log(err.stack);
        return res.status(400).json('MES TBLWIPLOTLOG query error! '+err);
      });
    })//TBLWIPLOTSMCONTENT connection query
    .catch(function(err){
      return res.status(400).json('MES TBLWIPLOTSMCONTENT query error!'+err);
    });
  };
  var buildUpTreeData = function(flotno_list,flot_fromto_group,lotlog_map){
    chtime(testSdate,"check 1-4: Start to build tree ");
    var getFromLotno = function(fromto_data,fromlotno){
      return _.filter(fromto_data,function(o){return o.FROMLOTNO==fromlotno});
    };
    var dfsBuild = function(tolotno_list,bilv,plotno,lotrelation,fromto_data){
      tolotno_list.forEach(function(doc){
        var new_lotrelation = lotrelation+'->'+doc.TOLOTNO.slice(7);
        var lotlog = lotlog_map[doc.TOLOTNO];
        if(lotlog) // the lotno which has burn-in record
        {
          var newplotno = "";
          if(lotlog.bilvinfo!=undefined) // handel the merge problem
          {
            var bilvinfo = lotlog.bilvinfo;
            bilvinfo.bilv = (bilvinfo.bilv<bilv)?bilv:bilvinfo.bilv;
            bilvinfo.plotno = bilvinfo.plotno + plotno;
            // pick the longer one
            lotrelation = (bilvinfo.lotrelation.length>lotrelation.length)?bilvinfo.lotrelation:lotrelation;
            bilvinfo.lotrelation = lotrelation;
          }
          else
          {
            lotlog.bilvinfo = {bilv:bilv,plotno:plotno,lotrelation:lotrelation};
          }
          // set lot relationship
          for(var i in lotlog.lotlogs)
              lotlog.lotlogs[i]['LOTRELATIONSHIP']=lotrelation;
          
          var newplotno = plotno + doc.TOLOTNO + '('+ bilv +'),';
          var tolotno_list = getFromLotno(fromto_data,doc.TOLOTNO);
          dfsBuild(tolotno_list,bilv+1,newplotno,new_lotrelation,fromto_data);
        }
        else{
          var tolotno_list = getFromLotno(fromto_data,doc.TOLOTNO);
          dfsBuild(tolotno_list,bilv,plotno,new_lotrelation,fromto_data);
        }
      });
    };
    flotno_list.forEach(function(flotno){
      var bilv = 1;
      var plotno = "";
      // fromto_data of this rootlotno
      var fromto_data = flot_fromto_group[flotno];
      //Get the first tolotno_list by rootlotno
      var tolotno_list = getFromLotno(fromto_data,flotno);
      var lotrelation = flotno.slice(7);
      dfsBuild(tolotno_list,bilv,plotno,lotrelation,fromto_data);
      // Pull lotlog_map data out
    });
    chtime(testSdate,"check 1-5: Build Tree compelete! ");
    appendMesToLotLog(lotlog_map);
  };
  // 4. Apeend mbi and mes data to lot log data from TBLWIPLOTLOG
  var appendMesToLotLog = function(lotlog_map){
    var rebi_lotlist = _.keys(lotlog_map);
    console.log('Rebi # lotnos: '+rebi_lotlist.length);
    // 4.1 append mes filed: customerchisname, packagetype, deviceno
    // -> move this part to TBLWIPLOTLOG query
    // 4.2 Combine the rebi information from mbi
    // -> we can get OVENID and DATETIME from TBLWIPLOTLOG
    for(var key in lotlog_map){
      var lotlog = lotlog_map[key];
      var lotinfo = lotlog.lotlogs[0];
      if(lotlog.bilvinfo==undefined)
        lotlog.bilvinfo = {bilv:undefined};
      else{
        // set LotRelationShip
        if(lotlog.bilvinfo.lotrelation){
          lotlog['LOTRELATIONSHIP']=lotlog.bilvinfo.lotrelation;
        }
      }
        
      if(lotinfo.OPNO.startsWith('MERGE') || lotinfo.OPNO.startsWith('L/S') || lotinfo.OPNO.startsWith('Split-MERGE')) // ended lot
        lotlog.bilvinfo.bilv = undefined;

      lotlog.group = {OVENID:lotinfo.EQUIPMENTNO,LOTNO:lotinfo.LOTNO,
        ROOTLOT:lotinfo.LOTNO.slice(0,7),BILEVEL:lotlog.bilvinfo.bilv};
    }
    if(callback)
      callback({'lotlogMap':lotlog_map,'lotlogList':lotloglist,'rootLotDieQty':rootlot_dieqty});
  };// appendMesToLotLog function
  findRoot();
};

exports.uploadConfig = function(req,res,next){
  if(req.user.local)
    if(req.user.local.name!='admin') // only admin user can use this function
      return res.status(400).json('Unauthenticated!');
  /*
  csvtext example:
  CustomerName,Family,Setting
  nVIDIA,T124AKV,Standard
  nVIDIA,GP106AK,Customize
  */
  var csvtext = req.body.csvtext;
  if(csvtext==undefined)
    return res.status(400).json('config undefined!');
  if(csvtext.length<1)
    return res.status(400).json('file is empty!');
  
  csvtext = csvtext.replace(/\r/g,'');
  UserProfile.findOne({'USERNAME':'admin'}, function(err, profile) {
    if(err){
      console.log(err);
      return res.status(400).json('update error!');
    }
    try{
      var csvtextlist = csvtext.split('\n');
      csvtextlist[0] = csvtextlist[0].toLowerCase();
      var headerlist = csvtextlist[0].replace('\n','').split(',');
      // check header if valid or not
      if(headerlist.indexOf('customername')<0 || headerlist.indexOf('family')<0 || headerlist.indexOf('setting')<0)//headerlist.indexOf('rebibinlist')<0)
        return res.status(400).json('header error! invalid header: '+csvtextlist[0]);
      var doclist = [];
      for(var i=1;i<csvtextlist.length;i++){
        var linesplit = csvtextlist[i].replace('\n','').split(',');
        if(linesplit[0].length<2) // ignore empty line
            continue;
        var doc = {};
        for(var j in headerlist){
          var value = linesplit[j];
          if(headerlist[j]=='setting')
            value = value.toLowerCase();
          doc[headerlist[j]]=value;
        }
        doclist.push(doc);
      }
    }
    catch(err){
      return res.status(400).json('header match error! '+err);
    };
    // multiple update callback function
    var callbackUpdate = function(doclist,i){
      if(i>=doclist.length)
        return res.send('upload success!');
      else{
        doc = doclist[i];
        ReportConfig.update({'OWNER':'admin','CUSTOMERNAME':doc.customername,'FAMILY':doc.family},
          {'OWNER':'admin','CUSTOMERNAME':doc.customername,'FAMILY':doc.family,'SETTING':doc.setting},
          {upsert:true},
          function(err){
            if(err)
              return res.status(400).json('update error at YieldReportConfig!');
            callbackUpdate(doclist,i+1);
        });
      }
    };
    if(profile)
      UserProfile.update({'USERNAME':'admin'},{$set:{CONFIG:{YREPORT:{TEXT:csvtext}}}},function(err,docs){
        if(err){
          console.log(err);
          return res.status(400).json('update error at UserProfile!');
        }
        callbackUpdate(doclist,0);
      });
    else
      UserProfile.create({USERNAME:'admin',CONFIG:{YREPORT:{TEXT:csvtext}},TABLE:{}},function(err,docs){
        if(err){
          console.log(err);
          return res.status(400).json('update error!');
        }
        callbackUpdate(doclist,0);
      });
  });
};

exports.loadConfig = function(req,res,next){
  UserProfile.findOne({'USERNAME':'admin'}, function(err, profile) {
    if(err){
      return res.status(400).json('query error!');
    }
    if(profile)
      res.send(profile.CONFIG.YREPORT.TEXT);
    else
      res.send('');
  });
};

exports.getMesMultiSelection = function(req, res, next){
  var fstr = req.body.fstr;
  var field = req.body.field;
  var qcmd="";
  if(field=='customername'){
    qcmd="select CUSTOMERCHISNAME,CUSTOMERENGSNAME,CUSTOMERNAME from Masterdm.DIM_Customer where "+
    "RegExp_like(CUSTOMERCHISNAME,'^"+fstr+"','i') or "+
    "RegExp_like(CUSTOMERENGSNAME,'^"+fstr+"','i') or "+
    "RegExp_like(CUSTOMERNAME,'^"+fstr+"','i')";
  }
  else if(field=='family'){
    qcmd="select FAMILY from (select max(FAMILY) FAMILY from HisMFT.TBLENGPRODUCTDATA where "+
    "RegExp_like(FAMILY,'^"+fstr+"','i') group by FAMILY) where rownum<="+gvar.searchFilterMaximumRowNum;
  }
  
  mesconn.exec(qcmd,[]).then(function(result){
    if(field=='customername'){
      result = _.orderBy(result,'CUSTOMERNAME');
      result = _.map(result,function(o){
        return o.CUSTOMERCHISNAME+','+o.CUSTOMERENGSNAME+'('+o.CUSTOMERNAME+')';
      });
    }
    else if(field=='family'){
      result = _.orderBy(result,'FAMILY');
      result=_.map(result,function(o){return o['FAMILY']});
    }
    result = _.uniq(result);
    return res.json(result);
  })
  .catch(function(err){
    console.log(err);
    return res.status(400).json('query error!');
  });
};

exports.mesGetSearchField = function(req, res, next){ // search field too many times, do not save requestlog
  var timerange = req.body.exmatch.datetime;
  var multiSelection = req.body.exmatch.multiselection?req.body.exmatch.multiselection:undefined;
  var to = new Date(timerange.to),
    from = new Date(timerange.from),
    field = req.body.field,
    match = req.body.match; //{LOTNO:,CUSTOMERCHISNAME:}
  // do not modified time zone, oracle db will automatically do it
  // append 'startwtih' regexp '^'
  var matchField = "";
  var keys = _.keys(match);
  var qcmd='';
  var params={};
  
  if(keys.length==1 && keys.indexOf('CUSTOMERCHISNAME')>=0){ // only customer name
    qcmd="select DISTINCT CUSTOMERCHISNAME from Masterdm.DIM_Customer where RegExp_like(CUSTOMERCHISNAME,'^"+match['CUSTOMERCHISNAME']+"','i')";
  }
  else{
    var handelMatchField = function(matchField){
      if(matchField.length<=0)
        matchField+=" where "
      else
        matchField+=" and "
      return matchField;
    };
    for(var key in match){
      match[key]='^'+match[key];
      var table="";
      if(key=='CUSTOMERCHISNAME' || key=='CUSTOMERNAME')
        table='B'
      else if(key=='FAMILY')
        table='C'
      else // LOTNO, CUSTOMERLOTNO
        table='A'
      matchField = handelMatchField(matchField);
      matchField+="RegExp_like("+table+"."+key+",'"+match[key]+"','i')";
    }
    if(multiSelection)
      for(var key in multiSelection){ // customername and family
        if(key=='customername'){
          for(var i in multiSelection[key]){
            matchField = handelMatchField(matchField);
            matchField+="B.CUSTOMERCHISNAME='"+multiSelection[key][i].split(',')[0]+"'";
          }
        }
        else if(key=='family'){
          for(var i in multiSelection[key]){
            matchField = handelMatchField(matchField);
            matchField+="C.FAMILY='"+multiSelection[key][i]+"'";
          }
        }
      }
    
    // Set filed with table
    var fieldWithTable = '';
    if(field=='PRODUCTNO')
      fieldWithTable = 'A.'+field;
    else
      fieldWithTable = field;
    qcmd= "select "+field+" from (select max("+fieldWithTable+") "+field+
        " from HISMFT.TBLWIPLOTDATA A inner join Masterdm.DIM_Customer B"+
        " on A.CUSTOMERNO=B.CUSTNO"+
        " left join HISMFT.TBLENGPRODUCTDATA C on A.PRODUCTNO=C.PRODUCTNO"+
        //" where A.REVISEDATE >= :fromDate and A.REVISEDATE < :toDate and rownum<20"+
        matchField+" group by "+fieldWithTable+") where rownum<="+gvar.searchFilterMaximumRowNum;
    //params['fromDate']=from;
    //params['toDate']=to;
  }
  mesconn.exec(qcmd,params).then(function(result){
    result = _.map(result,field);
    result = _.orderBy(result);
    return res.json(result);
  })
  .catch(function(err){
    return res.status(400).json('query error!');
  });
};