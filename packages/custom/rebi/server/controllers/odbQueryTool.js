
exports.insertListToQcmd = function(qcmd,listmap){
  // Example
  // In: (1) qcmd[string]: 'select DRIVERID,LOTNO from TABLEX where LOTNO in :lotnolist',
  //     (2) listmap[json object]:{':lotnolist':['a','b','c']}
  // Out: newqcmd[string]:"select DRIVERID,LOTNO from TABLEX where LOTNO in ('a','b','c')" (need to using double quotes for oracle db query)
  for(var key in listmap){
    var inliststr = "(";
    var listlen = listmap[key].length;
    for(var i=0;i<listlen;i++){
      inliststr+="'"+listmap[key][i]+"'";
      if(i<listlen-1)
        inliststr+=",";
      else
        inliststr+=")";
    }
    qcmd = qcmd.replace(key,inliststr);
  }
  return qcmd;
};