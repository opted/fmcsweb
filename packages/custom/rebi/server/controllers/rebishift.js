var mongoose = require('mongoose'),
    Q = require('q'),
    _ = require('lodash');
var oqtool = require('./odbQueryTool.js'),
    yieldreport = require('./yieldreport.js'),
    rebi = require('./rebi.js');
var ReportConfig = mongoose.model('YieldReportConfig');
var catchFuncion = function(err,res,msg){
  console.log(err.stack);
  return res.status(400).send(msg);
}
var mesconn = undefined;
var UTCTimeNumber = (new Date().getTimezoneOffset())/(-60);
exports.initMesConnection = function(utils){
  mesconn = utils.ctrls.mes;
};

var sendShiftChart = function(rootDateGroup,handelFunction,res,args){
  var resData = [];
  for(var date in rootDateGroup){
    var dateData = rootDateGroup[date];
    var aggData = {'date':date,'fsttestqty':0,'fstpassqty':0,'fintestqty':0,'finpassqty':0,'biqty':0,'rebiqty':0};
    handelFunction(aggData,dateData,args);
    resData.push(aggData);
  }
  resData = _.orderBy(resData,'date');
  res.json(resData);
};

var type1HandelFunction = function(aggData,dateData){
  aggData['rebiqty'] = _.sumBy(dateData,function(o){return o['ISREBI']?o.INPUTQTY:0;});
  aggData['biqty'] = _.sumBy(dateData,function(o){
    if(o.OPNO.startsWith('BURNIN') && o['ISREBI']==undefined)
      return o.INPUTQTY;
    else
      return 0;
  });
  aggData['rebirate'] = _.round((aggData['rebiqty']/aggData['biqty'])*100,2);
  aggData['fstpassqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('BURNIN1')?o.GOODQTY:0;});
  aggData['fsttestqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('BURNIN1')?o.INPUTQTY:0;});
  aggData['finpassqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('MERGE LOT CHECK')?o.GOODQTY:0;});
  aggData['fintestqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('MERGE LOT CHECK')?o.INPUTQTY:0;});
  aggData['fstyield'] = _.round((aggData['fstpassqty']/aggData['fsttestqty'])*100,2);
  aggData['finyield'] = _.round((aggData['finpassqty']/aggData['fintestqty'])*100,2);
};

var nvHandleFunction = function(aggData,dateData,args){
  // standardFunction
  // 不需計算重奔資料
  _.remove(fdoc,{ISREBI:true});
  var fdoc = _.filter(dateData,{'OPNO':'BURNIN1'});
  /**
    calculate rebi
  */
  aggData['rebiqty'] = _.sumBy(fdoc,function(o){
    var failbinmap = args.failBinMapFamily[o.FAMILY];
    var total=0;
    if(failbinmap){
      var failbinlist = failbinmap[0].list;
      for(var key in failbinlist)
      if(failbinlist[key])
        total+=o['BIN'+key];
    }
    else
      total=o.FAILQTY;
    return total;
  });// count fail qty by failbinmap
  aggData['biqty'] = _.sumBy(fdoc,'INPUTQTY');
  aggData['rebirate'] = _.round((aggData['rebiqty']/aggData['biqty'])*100,2);
  /**
    calculate yield
  */
  // 公式2 計算良率需要去除 rebi 的資料，計算時的良率和重奔率必須區分清楚
  // 推移圖算良率時一率不考慮用 customize 算法，也不需要去計算 bin
  aggData['biqty'] = _.sumBy(fdoc,'INPUTQTY');// re-calculate, 結果不是以結批的結果為單位則不能用 dieqty 來算
  aggData['fsttestqty'] = aggData['biqty'];
  aggData['fstpassqty'] = aggData['biqty']-(_.sumBy(fdoc,'FAILQTY')+_.sumBy(fdoc,'SCRAPQTY')+_.sumBy(fdoc,'LOSSQTY')+_.sumBy(fdoc,'OTHERQTY'));
  aggData['finpassqty'] = aggData['fstpassqty'];
  aggData['fintestqty'] = aggData['fsttestqty'];
  aggData['fstyield'] = _.round((aggData['fstpassqty']/aggData['fsttestqty'])*100,2);//first yield
  aggData['finyield'] = _.round((aggData['finpassqty']/aggData['fintestqty'])*100,2);//final yield
};

var type3HandelFunction = function(aggData,dateData){
  aggData['rebiqty'] = _.sumBy(dateData,function(o){return o.INPUTQTY-o.GOODQTY;});
  aggData['fsttestqty'] = aggData['biqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('BURNIN1')?o.INPUTQTY:0;});
  aggData['rebirate'] = _.round((aggData['rebiqty']/aggData['biqty'])*100,2);
  aggData['fstpassqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('BURNIN1')?o.GOODQTY:0;});
  aggData['finpassqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('Split-MERGE')?o.GOODQTY:0;});
  aggData['fintestqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('Split-MERGE')?o.INPUTQTY:0;});
  aggData['fstyield'] = _.round((aggData['fstpassqty']/aggData['fsttestqty'])*100,2);
  aggData['finyield'] = _.round((aggData['finpassqty']/aggData['fintestqty'])*100,2);
};

var type4HandelFunction = function(aggData,dateData){
  aggData['rebiqty'] = _.sumBy(dateData,function(o){return o['ISREBI']?o.INPUTQTY:0;});
  aggData['biqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('BURNIN')?o.INPUTQTY:0;});
  aggData['rebirate'] = _.round((aggData['rebiqty']/aggData['biqty'])*100,2);
  aggData['finpassqty'] = aggData['fstpassqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('BURNIN')?o.GOODQTY:0;});
  aggData['fintestqty'] = aggData['fsttestqty'] = _.sumBy(dateData,function(o){return o.OPNO.startsWith('BURNIN')?o.INPUTQTY:0;});
  aggData['finyield'] = aggData['fstyield'] = _.round((aggData['fstpassqty']/aggData['fsttestqty'])*100,2);
};

exports.getShiftMapChart = function(req,res,next){
  req.setTimeout(1800000); // 30 min
  var toDate = new Date(req.body.DATETIME.toDate);
  var fromDate = new Date(req.body.DATETIME.fromDate);
  var datetype = req.body.DATETIME.datetype;
  var dateField='', 
    whereFilter='', 
    opnoFilter="RegExp_like(A.OPNO,'^BURNIN')";

  var ftype = req.body.ftype,
    failBinMapFamily = req.body.failbinmapfamily?req.body.failbinmapfamily:{};
  
  // type 3 計算重奔需要完整母批
  if(ftype.rebi==1 || ftype.rebi==4) // 計算完後要刪除 out range data
    whereFilter=' and SUBSTR(A.LOTNO,1,7) in (select distinct SUBSTR(LOTNO,1,7) ROOTLOT from HISMFT.TBLWIPLOTLOG where CHECKOUTTIME between :fromDate and :toDate)';
  else
    whereFilter=' and CHECKOUTTIME between :fromDate and :toDate';
  
  if(datetype=='day')
    dateField = "to_char(CHECKOUTTIME,'YYYY-MM-DD')";
  else if(datetype=='week')
    dateField = "to_char(CHECKOUTTIME,'YYYY-WW')";
  else if(datetype=='month')
    dateField = "to_char(CHECKOUTTIME,'YYYY-MM')";
  // Set filter of first query
  // LOTNO, ROOTLOT, EQUIPMENTNO, CUSTOMERCHISNAME, DEVICENOLOG, PACKAGETYPE, OPNO, FAMILY
  for(var key in req.body.match){
    if(key=='ROOTLOT')
      whereFilter+=" and SUBSTR(A.LOTNO,1,7)='"+req.body.match[key]+"'";
    else if(key=='CUSTOMERCHISNAME')
      whereFilter+=" and "+yieldreport.setFieldInList('B.CUSTOMERCHISNAME',req.body.match[key]); // B.CUSTOMERCHISNAME in ('','','')
    else if(key=='PACKAGETYPE')
      whereFilter+=" and D.PACKAGETYPE='"+req.body.match[key]+"'";
    else if(key=='FAMILY')
      whereFilter+=" and "+yieldreport.setFieldInList('C.FAMILY',req.body.match[key]); // C.FAMILY in ('','','')
    else if(key=='DEVICENOLOG')
      whereFilter+=" and D.DEVICENO='"+req.body.match[key]+"'";
    else if(key=='CUSTOMERLOTNOLOG')
      whereFilter+=" and D.CUSTOMERLOTNO='"+req.body.match[key]+"'";
    else if(key=='FAMILY')
      whereFilter+=" and "+yieldreport.setFieldInList('C.FAMILY',req.body.match[key]); // C.FAMILY in ('','','')
    else{
      var value = req.body.match[key];
      whereFilter+=' and A.'+key+"='"+req.body.match[key]+"'";
    }
  }
  
  if(ftype.rebi==1 || ftype.rebi==2 || ftype.rebi==4)
    opnoFilter+=" or A.OPNO='MERGE LOT CHECK1'";
  else if(ftype.rebi==3)
    opnoFilter+=" or A.OPNO='Split-MERGE LOT CHECK1'";
  
  var qcmd="";
  var qcmdlist = [];
  var params = {toDate:toDate,fromDate:fromDate};
  if(ftype.rebi!=2){
    qcmd = "select A.LOTNO,A.OPNO,A.CHECKOUTTIME,A.INPUTQTY,A.GOODQTY,C.FAMILY from HISMFT.TBLWIPLOTLOG A "+
    "left join HISMFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO "+
    "left join Masterdm.DIM_Customer B on D.CUSTOMERNO=B.CUSTNO "+
    "left join HISMFT.TBLENGPRODUCTDATA C on D.PRODUCTNO=C.PRODUCTNO "+
    "where ("+opnoFilter+")"+whereFilter;
    qcmdlist.push(mesconn.exec(qcmd,params));
  }
  else{
    var binfield = "";
    for(var binno=1;binno<=8;binno++)
      binfield+="max(case when BINTABLE.BINNO="+binno+" then BINTABLE.BINQTY end) BIN"+binno+",";
    binfield = _.trimEnd(binfield,',');
    qcmd =
    "select A.LOTSERIAL,max(A.LOTNO) LOTNO,max(A.OPNO) OPNO,max(A.CHECKOUTTIME) CHECKOUTTIME,"+
    "max(A.INPUTQTY) INPUTQTY,max(A.GOODQTY) GOODQTY,max(A.FAILQTY) FAILQTY,"+
    "max(A.FAMILY) FAMILY,max(A.SCRAPQTY) SCRAPQTY,max(A.LOSSQTY) LOSSQTY,max(A.OTHERQTY) OTHERQTY,"+
    binfield+
    // select from subTable
    " from (SELECT A.LOTSERIAL,A.LOTNO,A.OPNO,A.CHECKOUTTIME,A.INPUTQTY,A.GOODQTY,A.FAILQTY,A.SCRAPQTY,A.LOSSQTY,A.OTHERQTY,C.FAMILY "+
    "from HISMFT.TBLWIPLOTLOG A "+
    "left join HISMFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO "+
    "left join Masterdm.DIM_Customer B on D.CUSTOMERNO=B.CUSTNO "+
    "left join HISMFT.TBLENGPRODUCTDATA C on D.PRODUCTNO=C.PRODUCTNO where ("+opnoFilter+")"+whereFilter+") A "+// inner join bintable
    "inner join HISMFT.TBLWIPBINCONTENT BINTABLE on A.LOTSERIAL=BINTABLE.LOTSERIAL group by A.LOTSERIAL";
    qcmdlist.push(mesconn.exec(qcmd,params));
    /*var splitmft_qcmd =
    "select A.LOTSERIAL,max(A.LOTNO) LOTNO,max(A.OPNO) OPNO,max(A.CHECKOUTTIME) CHECKOUTTIME,A.FAMILY,"+
    "max(A.INPUTQTY) INPUTQTY,max(A.GOODQTY) GOODQTY,max(A.FAILQTY) FAILQTY,"+binfield+
    // select from subTable
    " from (SELECT A.LOTSERIAL,A.LOTNO,A.OPNO,A.CHECKOUTTIME,A.INPUTQTY,A.GOODQTY,A.FAILQTY,C.FAMILY "+
    "from HISLFT.TBLWIPLOTLOG A "+
    "left join HISLFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO "+
    "left join Masterdm.DIM_Customer B on D.CUSTOMERNO=B.CUSTNO "+
    "left join HISLFT.TBLENGPRODUCTDATA C on D.PRODUCTNO=C.PRODUCTNO where ("+opnoFilter+")"+whereFilter+") A "+// inner join bintable
    "inner join HISLFT.TBLWIPBINCONTENT BINTABLE on A.LOTSERIAL=BINTABLE.LOTSERIAL group by A.LOTSERIAL";
    qcmdlist.push(mesconn.exec(splitmft_qcmd,params));*/
  }
  
  var sdate = new Date();
  // 1. Query LotLog
  Q.all(qcmdlist).then(function(result){
   result = _.flatten(result);
    // set GMT +8 for processing ISOString
    var oneHour = 60*60*1000;
    var rootlotlist = [], shiftFamily = undefined;
    result.forEach(function(o){
      o.CHECKOUTTIME = new Date(o.CHECKOUTTIME.getTime()+UTCTimeNumber*oneHour);
      rootlotlist.push(o.LOTNO.slice(0,7));
    });
    shiftFamily = _.uniq(_.map(result,'FAMILY'));
    if(shiftFamily.length!=1)
      shiftFamily = undefined;
      
    var multi_rootlotlist = rebi.cutArray(_.uniq(rootlotlist),500); // avoid lotnolist too large
    var qcmd_list = [];
    for(var i in multi_rootlotlist){
      qcmd = "select A.FROMLOTNO,B.DIEQTY from "+
        "(select MIN(FROMLOTNO) as FROMLOTNO from HISMFT.TBLWIPLOTSMCONTENT where SUBSTR(FROMLOTNO,1,7) in :rootlotlist group by SUBSTR(FROMLOTNO,1,7)) A "+
        "left join HISMFT.TBLWIPLOTDATA B on A.FROMLOTNO=B.LOTNO";
      qcmd = oqtool.insertListToQcmd(qcmd,{':rootlotlist':multi_rootlotlist[i]});
      qcmd_list.push(mesconn.exec(qcmd,{}));
    }
    // 2. Query Dieqty
    Q.all(qcmd_list).then(function(qdieqty_result){
      qdieqty_result = _.flatten(qdieqty_result);
      var rootlot_DieqtyMap = {};
      qdieqty_result.forEach(function(o){rootlot_DieqtyMap[o.FROMLOTNO.slice(0,7)]=o.DIEQTY});
      // Find rebi data
      if(ftype.rebi==1 || ftype.rebi==4 || ftype.yield==2){
        // 1. group by rootlot and set ISREBI
        var rootLotGroup = _.groupBy(result,function(o){ return o.LOTNO.slice(0,7)});
        for(var rootlot in rootLotGroup){
          var orderData = _.orderBy(rootLotGroup[rootlot],'CHECKOUTTIME');
          // select out merege data
          var dieqty = rootlot_DieqtyMap[rootlot];
          if(!dieqty)
            continue;
          // pick out reburn lotno
          var sumqty = 0;
          var biData = _.filter(orderData,function(o){return o.OPNO.startsWith('BURNIN');});
          biData.forEach(function(o){
            sumqty+=o.INPUTQTY;
            // exceed total input qty that were reburn qty
            if(sumqty>dieqty)
              o.ISREBI=true;
          });
          /*if(ftype.yield==2){
            // 標記沒有 Split-MFT 站點的 LotLog
            if(_.filter(orderData,{OPNO:'Split-MFT'}).length){ // no split-mft
              orderData.forEach(o => o.nosplitmft=true);
            }
          }*/
        }
      }
      // filter in range data
      fromDate = new Date(fromDate.getTime()+UTCTimeNumber*oneHour);
      toDate = new Date(toDate.getTime()+UTCTimeNumber*oneHour);
      result = _.filter(result,function(o){return (o.CHECKOUTTIME>=fromDate && o.CHECKOUTTIME<toDate) });
    
      // 2. group date and aggreage some field
      var resData = [];
      var dateFilter = undefined;
      if(datetype=='day')
        dateFilter = function(date){ return date.toISOString().slice(0,10)};
      else if(datetype=='week')
        dateFilter = function(date){ return rebi.getWeek(new Date(date.getTime()-UTCTimeNumber*oneHour))}; // 時區會導致一些計算錯誤 -8 taipei time 修正
      else if(datetype=='month')
        dateFilter = function(date){ return date.toISOString().slice(0,7)};
      var rootDateGroup = _.groupBy(result,function(o){ return dateFilter(o.CHECKOUTTIME)});
      // 3. Caculate rebi & yield by different formular
      // 3.1 Change week datetime format
      if(ftype.rebi==1){
        sendShiftChart(rootDateGroup,type1HandelFunction,res);
      }
      else if(ftype.rebi==2){
        sendShiftChart(rootDateGroup,nvHandleFunction,res,{failBinMapFamily:failBinMapFamily});
      }
      else if(ftype.rebi==3){
        sendShiftChart(rootDateGroup,type3HandelFunction,res);
      }
      else if(ftype.rebi==4){
        sendShiftChart(rootDateGroup,type4HandelFunction,res);
      }
    }).catch(err => catchFuncion(err,res,'query dieqty error!'));// Query Dieqty
  }).catch(err => catchFuncion(err,res,'shit map query error!'));
};