'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Q = require('q'),
    _ = require('lodash'),
    fs = require('fs'),
    xl = require('excel4node');
var oqtool = require('./odbQueryTool.js');
var rebi = require('./rebi.js');

var chtime = function(sdate,tag){console.log(tag+(new Date().valueOf()-sdate)/1000)};
var ReportConfig = mongoose.model('YieldReportConfig');
var oneHour = 60*60*1000;
var reportFolder = './tmp/';
var UTCTimeNumber = (new Date().getTimezoneOffset())/(-60);

var mesconn = undefined;
exports.initMesConnection = function(utils){
  mesconn = utils.ctrls.mes;
};

var pythonWriteExcelYieldReport = function(infile,outfile,callback){
  var PythonShell = require('python-shell');
  var options = {
    scriptPath: './python/rebi/',
    args: [infile,outfile]
  };
  console.log(infile,outfile);
  PythonShell.run('yieldReportWriter.py', options, callback);
}

var writeYieldReport = function(filename,sheetData,chartData,next,callback){
  // 1. filename[String]: Excele Name
  // 2. sheetData[JSON]: {Standard:{rawdata:[],settledata:[]},Customize_LS:{}, Customize_no_LS:{}}
  //var wb = new xl.Workbook();
  
  var rawHeaderOrder = ['CustomerName','CustomerLotNo','ProductFamily','OpNo','Temp','LotNo','LotRelationShip',
  'DeviceNo','PackageType','BIHour','ProgramName','ProgramVer','Tester','CheckInTime','CheckOutTime','DieQty','TestQty','SampleQty','PassQty',
  'FailQty','Bin1','Bin2','Bin3','Bin4','Bin5','Bin6','Bin7','Bin8',
  'ScrapQty','LossQty','OtherQty','FinalYield','Diff','FirstYield','FirstBin1','FirstBin2','FirstBin3',
  'FirstBin4','FirstBin5','FirstBin6','FirstBin7','FirstBin8','FirstMarkQty','FirstFootQty','FirstPassQty','Status',
  'LotSerial','ProductNo'];
  var columnMap = {'CustomerName':'CUSTOMERCHISNAME','CustomerLotNo':'CUSTOMERLOTNOLOG','ProductFamily':'FAMILY',
  'DeviceNo':'DEVICENOLOG','Tester':'EQUIPMENTNO','ProductNo':'PRODUCTNOLOG','Status':'RCVSTATUS'};
  rawHeaderOrder.forEach(function(item){
    if(columnMap[item]==undefined)
      columnMap[item]=item.toUpperCase();
  });
  var settleHeaderOrder = _.cloneDeep(rawHeaderOrder);
  // remove unuse header: LotNo, OpNo, LotRelationShip
  var removelist = ['LotNo', 'LotRelationShip','SampleQty','FirstMarkQty','FirstFootQty','LotSerial'];
  /*_.remove(settleHeaderOrder,function(item){
    return removelist.indexOf(item)>=0;
  });*/
  var tsvText = '';
  for(var key in sheetData){
    // It has three sheet key: Standard, Customize_LS, Customize_no_LS
    tsvText+='######'+key+'\n';
    var data = sheetData[key];

    if(data.settledata.length<=0)
      continue;
    /**
      write raw Data
    */
    // write header
    for(var i in rawHeaderOrder){
      tsvText+=rawHeaderOrder[i]+'\t';
    }
    tsvText+='\n';
    data.rawdata = _.sortBy(data.rawdata,['LOTNO']);
    for(var i in data.rawdata){
      var rawdata = data.rawdata[i];
      for(var j in rawHeaderOrder){
        var value = rawdata[columnMap[rawHeaderOrder[j]]];
        if(value!=undefined)
          tsvText+=value+'\t';
        else
          tsvText+=''+'\t';
      }
      tsvText+='\n';
    }
    //console.log('##### write raw data is completed!');
    tsvText+='\n';
    /** 
      write settle Data
    */
    // write header
    for(var i in settleHeaderOrder){
      tsvText+=settleHeaderOrder[i]+'\t';
    }
    tsvText+='\n';
    data.settledata = _.sortBy(data.settledata,['ROOTLOT']);
    for(var i in data.settledata){
      var settledata = data.settledata[i];
      for(var j in settleHeaderOrder){
        var value = settledata[columnMap[settleHeaderOrder[j]]];
        if(removelist.indexOf(settleHeaderOrder[j])>=0)
          value = undefined;
        if(_.isNaN(value))
          value=undefined;
        if(value!=undefined)
          tsvText+=value+'\t';
        else
          tsvText+=''+'\t';
      }
      tsvText+='\n';
    }
    //console.log('##### write settle data is completed!');
  }
  tsvText+='######RawDataChartAnalysis\n';
  var chartSheetHeader = ['Key','TestQty','FinalYield','FirstYield','Gap'];
  chartSheetHeader.forEach(function(item){
    tsvText+=item+'\t';
  });
  tsvText+='\n';
  for(var i in chartData){
    for(var j in chartSheetHeader){
      var field = chartSheetHeader[j];
      tsvText+=chartData[i][field]+'\t';
    }
    tsvText+='\n';
  }
  //console.log('##### write out');
  var fs = require('fs');
  var tsvfile = String(Math.floor(Math.random()*1000))+'-yield-reoprt.tsv';
  tsvfile=reportFolder+tsvfile;
  fs.writeFile(tsvfile,tsvText, function (err) {
    if (err){
      callback(err);
    }
    else
    {
      pythonWriteExcelYieldReport(tsvfile,filename,function(err,results){
        // remove tsv file
        setTimeout(function(){
          fs.unlink(tsvfile);
        },10000);
        callback();
      });
    }
  });
};

exports.test = function(req,res,next){
  var testingData = {'Standard':{
  "rawdata": [
    { "OVENID": "OG-25","LOTNO": "LHA06AUAJ1","ROOTLOT":"LHA06AUAB1","DATETIME": "2017-10-12T15:05:49.000Z","BILEVEL": 1,"DAY": "10/12","MONTH": "2017/10","WEEK": "10/8/2017 ~ 10/14/2017","PACKAGETYPE": "HS-FCBGA1208(42.5*42.5*2.78mm)","CUSTOMERCHISNAME": "nVIDIA","FAMILY": "T124AKV","LOTSERIAL": "LHA06AUAJ1-002","CUSTOMERLOTNOLOG": "T06C32.S6P","OPNO": "BURNIN1","EQUIPMENTNO": "OG-25","CHECKINTIME": "2017-10-12T15:59:34.000Z","CHECKOUTTIME": "2017-10-12T23:59:34.000Z","INPUTQTY": 189,"GOODQTY": 189,"FAILQTY": 0,"OTHERQTY": 0,"SCRAPQTY": 0,"LOSSQTY": 0,"DEVICENOLOG": "PF-T124-B1XB-A1","PRODUCTNOLOG": "PF-T124-B1XB-A1_BI_MP_4-(FN2M)","BIN1": 189,"DIEQTY": 1543,"PASSQTY": 189,"TESTQTY": 189,"FIRSTYIELD": 1,"FINALYIELD": 1,"DIFF": 0,"FIRSTBIN1": 189,"FIRSTPASSQTY": 189,"FIRSTBIN2": 0,"BIN2": 0,"FIRSTBIN3": 0,"BIN3": 0,"FIRSTBIN4": 0,"BIN4": 0,"FIRSTBIN5": 0,"BIN5": 0,"FIRSTBIN6": 0,"BIN6": 0,"FIRSTBIN7": 0,"BIN7": 0,"FIRSTBIN8": 0,"BIN8": 0},
    ],
  "settledata":[
    {"OVENID": "OG-25","LOTNO": "LHA06AUAJ1","ROOTLOT": "LHA06AUAB1","DATETIME": "2017-10-12T15:05:49.000Z","BILEVEL": 1,"DAY": "10/12","MONTH": "2017/10","WEEK": "10/8/2017 ~ 10/14/2017","PACKAGETYPE": "HS-FCBGA1208(42.5*42.5*2.78mm)","CUSTOMERCHISNAME": "nVIDIA","FAMILY": "T124AKV","LOTSERIAL": "LHA06AUAJ1-002","CUSTOMERLOTNOLOG": "T06C32.S6P","OPNO": "BURNIN1","EQUIPMENTNO": "OG-25,OG-23","CHECKINTIME": "2017-10-11T13:18:22.000Z","CHECKOUTTIME": "2017-10-13T13:07:22.000Z","INPUTQTY": 189,"GOODQTY": 189,"FAILQTY": 1,"OTHERQTY": 0,"SCRAPQTY": 0,"LOSSQTY": 0,"DEVICENOLOG": "PF-T124-B1XB-A1","PRODUCTNOLOG": "PF-T124-B1XB-A1_BI_MP_4-(FN2M)","BIN1": 1542,"BIN2": 0,"FIRSTBIN2": 0,"BIN3": 0,"FIRSTBIN3": 0,"BIN4": 0,"FIRSTBIN4": 0,"BIN5": 0,"FIRSTBIN5": 0,"BIN6": 0,"FIRSTBIN6": 0,"BIN7": 1,"FIRSTBIN7": 1,"BIN8": 0,"FIRSTBIN8": 0,"TESTQTY": 1543,"DIEQTY": 1543,"SAMPLEQTY": 0,"BODYQTY": 0,"MARKQTY": 0,"FOOTQTY": 0,"FACEQTY": 0,"FIRSTMARKQTY": 0,"FIRSTFOOTQTY": 0,"PASSQTY": 1542,"FIRSTBIN1": 1542,"FINALYIELD": 0.9993519118600129,"FIRSTYIELD": 0.9993519118600129,"DIFF": 0,"FIRSTPASSQTY": 1542}
  ]},'Customize_LS':{rawdata:[],settledata:[]},'Customize_no_LS':{rawdata:[],settledata:[]}};
  var filename='test-yieldreport.xlsx';
  writeYieldReport(reportFolder+filename,testingData,next,function(){
    res.json({sheetdata:testingData,filename:filename,length:0});
  });
};

exports.example = function(req, res, next) {
  //var qcmd = 'select * from HisMFT.TBLENGPRODOPRECIPECONTENT where ATTRIBNO=:attribno order by REVISEDATE desc where rownum=1';
  var qcmd = "select * from HISLFT.TblWipLotData where lotno like :lotno";
  var params = {'lotno':'LH401W5AA1'};
  var qcmd_list = [];
  qcmd_list.push(mesconn.exec(qcmd,params));
  Q.all(qcmd_list).then(function(results){
    results = _.flatten(results);
    res.json(results);
  });
};

var yieldReportfilledGroupColumn = function(docs){
  // fill the missing column
  docs.forEach(function(doc){
    doc['PASSQTY']=doc['GOODQTY'];
    doc['TESTQTY']=doc['INPUTQTY'];
    doc['FINALYIELD']=doc['FIRSTYIELD']=doc['PASSQTY']/doc['TESTQTY'];
    doc['DIFF']=0;
    doc['FIRSTPASSQTY']=doc['BIN1']=doc['FIRSTBIN1']=doc['PASSQTY'];
    for(var i=2;i<=8;i++){
      if(doc['BIN'+i]==undefined)
        doc['BIN'+i]=doc['FIRSTBIN'+i]=0;
      else
        doc['FIRSTBIN'+i]=doc['BIN'+i];
      if(doc['OPNO']=='Split-MFT')
        doc['FIRSTBIN'+i]=undefined;
    }
    var zeroColumn_list = ['SAMPLEQTY','BODYQTY','MARKQTY','FOOTQTY','FACEQTY','FIRSTMARKQTY','FIRSTFOOTQTY'];
    zeroColumn_list.forEach(function(zcol){
      doc[zcol]=0;
    });
    if(doc['OPNO']=='Split-MFT'){
      var mergedocs = _.filter(docs,function(o){return (o.OPNO.startsWith('MERGE') || o.OPNO.startsWith('L/S'))});
      doc['FIRSTBIN1']=undefined;
      for(var key in mergedocs[0]){
        if(key.startsWith('FIRST'))
          continue;
        else
          if(doc[key]==undefined)
            doc[key]=mergedocs[0][key];
      }
    }
  });
}

var calYield = function(opno,groupData){
  var acc_gqty = 0;
  var acc_tqty = 0;
  var regopno=undefined;
  if(_.endsWith(opno,'*')) // ex: BURNIN*
    regopno=opno.replace('*','');
  var fdoc = undefined;
  if(regopno){
    fdoc = _.filter(groupData,function(o){
      return _.startsWith(o.OPNO,regopno);
    });
  }
  else
    fdoc = _.filter(groupData,{'OPNO':opno});

  var failbinqty = _.sumBy(fdoc,'FAILBINQTY'); // for type 2
  var goodqty = _.sumBy(fdoc,'GOODQTY');
  var inputqty = _.sumBy(fdoc,'INPUTQTY');
  if(failbinqty)
    goodqty=inputqty-failbinqty;
  acc_gqty+=goodqty;
  acc_tqty+=inputqty;
  return {'passqty':acc_gqty,'testqty':acc_tqty};
};

var getExcelChartData = function(sheetData,ftype){
  var rawdata = []
  if(ftype.yield!=2){
    rawdata = sheetData['Sheet1'].rawdata;
  }
  else{
    for(var key in sheetData)
      rawdata.push.apply(rawdata,sheetData[key].settledata);
  }
  var groupMap = _.groupBy(rawdata,'FAMILY');
  var sdatalist = [];
  for(var key in groupMap){
    var groupData = groupMap[key];
    var sdata = {'Key':key};
    if(ftype.yield==1){
      // Type 1: 
      // first yield = (BURNIN1 GOODQTY)/(BURNIN1 TESTQTY)
      var caly_data = calYield('BURNIN1',groupData);
      sdata['FirstYield']=caly_data['passqty']/caly_data['testqty'];
      sdata['TestQty']=caly_data['testqty'];
      // final yield = (MERGE-LOT-CHECK1 GOODQTY)/(MERGE-LOT-CHECK1 TESTQTY)
      caly_data=calYield('MERGE LOT CHECK1',groupData);
      sdata['FinalYield']=caly_data['passqty']/caly_data['testqty'];
    }
    else if(ftype.yield==2){
      // Type 2:
      // first yield = FirstPassQty/TestQty
      // final yield = PassQty/TestQty
      var fpassqty = _.sumBy(groupData,'FIRSTPASSQTY');
      var passqty = _.sumBy(groupData,'PASSQTY');
      var testqty = _.sumBy(groupData,'TESTQTY');
      sdata['FirstYield']=fpassqty/testqty;
      sdata['FinalYield']=passqty/testqty;
      sdata['TestQty']=testqty;
    }
    else if(ftype.yield==3){
      // Type 3:
      // first yield = (BURNIN1 GOODQTY)/(BURNIN1 TESTQTY)
      var caly_data = calYield('BURNIN1',groupData);
      sdata['FirstYield']=caly_data['passqty']/caly_data['testqty'];
      sdata['TestQty']=caly_data['testqty'];
      // final yield = (Split-MERGE LOT CHECK1 GOODQTY)/(Split-MERGE LOT CHECK1 TESTQTY)
      caly_data=calYield('Split-MERGE LOT CHECK1',groupData);
      sdata['FinalYield']=caly_data['passqty']/caly_data['testqty'];
    }
    else if(ftype.yield==4){
      // Type 4:
      // final yield = first yield
      var caly_data = calYield('BURNIN*',groupData);
      sdata['FirstYield']=sdata['FinalYield']=caly_data['passqty']/caly_data['testqty'];
      caly_data = calYield('BURNIN1',groupData);
      sdata['TestQty']=caly_data['testqty'];
    }
    sdata['Gap']=sdata['FinalYield']-sdata['FirstYield'];
    sdatalist.push(sdata);
  }
  return sdatalist;
};
var nvBiFilterFunction = function(o){return (o.OPNO.startsWith('BURNIN') && o.BILEVEL==1)};
var nvSettleFunction = function(f_lotloglist,ftype,res,next){
  var settleList = [];
  var sheetData = {'Standard':{rawdata:[],settledata:[]}
    ,'Customize_LS':{rawdata:[],settledata:[]}
    ,'Customize_no_LS':{rawdata:[],settledata:[]}}; //saveDataToWriteExcel
  var familylist = _.uniq(_.map(f_lotloglist,'FAMILY'));
  ReportConfig.find({'FAMILY':{'$in':familylist},'SETTING':'customize'}, function(err, docs) {
    var configMap = {};
    if(docs){
      configMap = _.groupBy(docs,'FAMILY');
    }
    var groupByRoot = _.groupBy(f_lotloglist,'ROOTLOT');
    for(var key in groupByRoot){
      var lotlogDocs = groupByRoot[key];
      // check the family is standard or customize
      var settleResult = undefined;
      for(var lld_index in lotlogDocs){
        if(lotlogDocs[lld_index].OPNO.startsWith('BURNIN')){
          settleResult = _.cloneDeep(lotlogDocs[lld_index]);
          break;
        }
      }
      
      var configtype = 'standard'; // standard, customize
      var reportType = 'Standard';
      var lsfilter = [];
      //if this family has customize config
      if(configMap[settleResult['FAMILY']]){
        configtype='customize';
        lsfilter = _.filter(lotlogDocs,function(o){return o.OPNO.startsWith('L/S')});
        if(lotlogDocs[0]['nosplitmft']){ // customize 不結算無 Split-MFT 站點的資料，但是仍需要塞 rawdata
          if(lsfilter.length>0)
            reportType = 'Customize_LS';
          else
            reportType = 'Customize_no_LS';
          sheetData[reportType].rawdata.push.apply(sheetData[reportType].rawdata,lotlogDocs);
          continue;
        }
      }
      /**
        Common field, both standard and customize are same
      */
      var bifilter = _.filter(lotlogDocs,nvBiFilterFunction);
      if(bifilter[0]==undefined)
        continue;
      settleResult['OPNO'] = _.orderBy(_.map(bifilter,'OPNO'),[],['desc'])[0];
      // a. Sum
      for(var i=2;i<=8;i++){
        settleResult['BIN'+i] = _.sumBy(bifilter,'BIN'+i);
        if(settleResult['BIN'+i]==undefined)
          settleResult['BIN'+i]=0;
        settleResult['FIRSTBIN'+i] = settleResult['BIN'+i];
      }
      settleResult['TESTQTY']=_.sumBy(bifilter,'INPUTQTY');
      settleResult['DIEQTY']=bifilter[0]['DIEQTY'];
      // b. Combined field: EQUIPMENTNO
      settleResult['EQUIPMENTNO']="";
      var eqlist = _.uniq(_.map(bifilter,'EQUIPMENTNO'));
      _.forEach(eqlist,function(eq,i){
        if(i>0)
          settleResult['EQUIPMENTNO']+=',';
        settleResult['EQUIPMENTNO']+=eq;
      });
      // c. Max & Min field: CHECKOUTTIME, CHECKINTIME
      settleResult['CHECKINTIME']= _.min(_.map(bifilter,'CHECKINTIME'));
      settleResult['CHECKOUTTIME']= _.max(_.map(bifilter,'CHECKOUTTIME'));
      // d. Modified other field
      // e. zero value
      var zlist=['SAMPLEQTY','BODYQTY','MARKQTY','FOOTQTY','FACEQTY','FIRSTMARKQTY','FIRSTFOOTQTY'];
      zlist.forEach(function(field){
        settleResult[field]=0;
      });

      if(configtype=='standard'){
        // settle: EQUIPMENTNO, CHECKINTIME, CHECKOUTTIME, DIEQTY, TESTQTY, PASSQTY, FAILQTY, BIN1~BIN8,
        //  SCRAPQTY, LOSSQTY, OTHERQTY, FinalYield, Diff, FirstYield, FirstBin1~8, FirstMarkQty, FirstFootQty, FirstPassQty
        // Sum field: BIN, FIRSTBIN, SCRAPQTY, LOSSQTY, OTHERQTY, FAILQTY
        reportType='Standard';
        settleResult['FAILQTY']= _.sumBy(bifilter,'FAILQTY');
        settleResult['SCRAPQTY']= _.sumBy(bifilter,'SCRAPQTY');
        settleResult['LOSSQTY']= _.sumBy(bifilter,'LOSSQTY');
        settleResult['OTHERQTY']= _.sumBy(bifilter,'OTHERQTY');
        
        // Modified other field
        settleResult['PASSQTY']=settleResult['DIEQTY']-(settleResult['FAILQTY']+settleResult['SCRAPQTY']+settleResult['LOSSQTY']+settleResult['OTHERQTY']);
        settleResult['BIN1']=settleResult['PASSQTY'];
        settleResult['FIRSTBIN1']=settleResult['BIN1'];
        settleResult['FINALYIELD']=settleResult['PASSQTY']/settleResult['DIEQTY'];
        settleResult['FIRSTYIELD']=settleResult['PASSQTY']/settleResult['DIEQTY'];
        settleResult['DIFF']=settleResult['FINALYIELD']-settleResult['FIRSTYIELD'];
        settleResult['FIRSTPASSQTY']=settleResult['PASSQTY'];
      }else if(configtype=='customize'){
        reportType='Customize_LS';
        settleResult['FIRSTFAILQTY']=0; // initial this field
        if(lsfilter.length>0){ // has LS
          var spmftlist = _.filter(lotlogDocs,{OPNO:'Split-MFT'});
          var lslist = _.filter(lotlogDocs,function(o){return o.OPNO.startsWith('L/S')});
          // cal BIN2~8, SCRAPQTY, LOSSQTY, OTHERQTY
          // L/S* - Split-MFT accumulate
          settleResult['FAILQTY']=0;
          for(var i=2;i<=8;i++){
            var spmft_bin = _.sumBy(spmftlist,'BIN'+i);
            var ls_bin = _.sumBy(lslist,'BIN'+i);
            if(spmft_bin==undefined)
              spmft_bin=0;
            if(ls_bin==undefined)
              ls_bin=0;
            settleResult['BIN'+i]=spmft_bin-ls_bin;
            settleResult['FAILQTY']+=settleResult['BIN'+i]; // FAILQTY=BIN2+...+BIN8
            settleResult['FIRSTFAILQTY']+=settleResult['FIRSTBIN'+i]; // FIRSTFAILQTY=FIRSTBIN2+...+FIRSTBIN8
          }
          settleResult['SCRAPQTY']=_.sumBy(spmftlist,'SCRAPQTY')-_.sumBy(lslist,'SCRAPQTY');
          settleResult['LOSSQTY']=_.sumBy(spmftlist,'LOSSQTY')-_.sumBy(lslist,'LOSSQTY');
          settleResult['OTHERQTY']=_.sumBy(spmftlist,'OTHERQTY')-_.sumBy(lslist,'OTHERQTY');
        }
        else{ // no LS
          reportType='Customize_no_LS';
          var spmftlist = _.filter(lotlogDocs,{OPNO:'Split-MFT'});
          settleResult['FAILQTY']=0;
          for(var i=2;i<=8;i++){
            var spmft_bin = _.sumBy(spmftlist,'BIN'+i);
            if(spmft_bin==undefined)
              spmft_bin=0;
            settleResult['BIN'+i]=spmft_bin;
            settleResult['FAILQTY']+=settleResult['BIN'+i]; // FAILQTY=BIN2+...+BIN8
            settleResult['FIRSTFAILQTY']+=settleResult['FIRSTBIN'+i]; // FIRSTFAILQTY=FIRSTBIN2+...+FIRSTBIN8
          }
          settleResult['SCRAPQTY']=_.sumBy(spmftlist,'SCRAPQTY');
          settleResult['LOSSQTY']=_.sumBy(spmftlist,'LOSSQTY');
          settleResult['OTHERQTY']=_.sumBy(spmftlist,'OTHERQTY');
        }
        // Change formual of FIRSTPASSQTY & FIRSTBIN1 from 【ReBI&YieldReport功能驗證報告_20171221.xlsx】 request
        // FIRSTPASSQTY = DieQty-(該客批各子批"BURNIN*"的Bin2~Bin8的總和) -> DieQty-(該客批各子批"BURNIN*"的(Bin2~Bin8+Scrap+Loss+Other)的總和)
        settleResult['FIRSTPASSQTY']=settleResult['DIEQTY']-(settleResult['FIRSTFAILQTY']+settleResult['SCRAPQTY']+settleResult['LOSSQTY']+settleResult['OTHERQTY']);
        settleResult['FIRSTBIN1']=settleResult['FIRSTPASSQTY'];
        settleResult['PASSQTY']=settleResult['DIEQTY']-settleResult['FAILQTY']-settleResult['SCRAPQTY']
            -settleResult['LOSSQTY']-settleResult['OTHERQTY'];
        settleResult['BIN1']=settleResult['PASSQTY'];
        settleResult['FINALYIELD']=settleResult['PASSQTY']/settleResult['DIEQTY'];
        settleResult['FIRSTYIELD']=settleResult['FIRSTPASSQTY']/settleResult['DIEQTY'];
        settleResult['DIFF']=settleResult['FINALYIELD']-settleResult['FIRSTYIELD'];
      }
      // fill missing column
      yieldReportfilledGroupColumn(lotlogDocs);
      sheetData[reportType].settledata.push(settleResult);
      sheetData[reportType].rawdata.push.apply(sheetData[reportType].rawdata,lotlogDocs);
      settleList.push(settleResult);
    }
    
    var filename=_.random(1000)+'-'+new Date().getTime()+'.xlsx';
    //console.log('### save yield report at tmp folder',filename);
    // Gen Data of RawDataChartAnalysis Sheet
    var excleChartData = getExcelChartData(sheetData,ftype);
    writeYieldReport(reportFolder+filename,sheetData,excleChartData,next,function(err){
      if(err){
        console.log(err.stack);
        return res.status(400).send('write report error!');
      }
      res.json({sheetdata:sheetData,filename:filename,length:f_lotloglist.length});
    });
  }); // yield report config query
};

var otherSettleFunction = function(f_lotloglist,ftype,res,next){
  var settleList = [];
  var sheetData = {'Sheet1':{rawdata:[],settledata:[]}}; //saveDataToWriteExcel
  var groupByRoot = _.groupBy(f_lotloglist,'ROOTLOT');
  for(var key in groupByRoot){
    var lotlogDocs = groupByRoot[key];
    // check the family is standard or customize
    var settleResult = _.cloneDeep(lotlogDocs[0]);
    /**
      Common field, both standard and customize are same
    */
    var bifilter = _.filter(lotlogDocs,function(o){return o.OPNO.startsWith('BURNIN')});
    if(bifilter[0]==undefined)
      continue;
    settleResult['OPNO'] = _.orderBy(_.map(bifilter,'OPNO'),[],['desc'])[0];
    // a. Sum
    for(var i=2;i<=8;i++){
      settleResult['BIN'+i] = _.sumBy(bifilter,'BIN'+i);
      if(settleResult['BIN'+i]==undefined)
        settleResult['BIN'+i]=0;
      settleResult['FIRSTBIN'+i] = settleResult['BIN'+i];
    }
    settleResult['TESTQTY']=_.sumBy(bifilter,'INPUTQTY');
    settleResult['DIEQTY']=bifilter[0]['DIEQTY'];
    // b. Combined field: EQUIPMENTNO
    settleResult['EQUIPMENTNO']="";
    var eqlist = _.uniq(_.map(bifilter,'EQUIPMENTNO'));
    _.forEach(eqlist,function(eq,i){
      if(i>0)
        settleResult['EQUIPMENTNO']+=',';
      settleResult['EQUIPMENTNO']+=eq;
    });
    // c. Max & Min field: CHECKOUTTIME, CHECKINTIME
    settleResult['CHECKINTIME']= _.min(_.map(bifilter,'CHECKINTIME'));
    settleResult['CHECKOUTTIME']= _.max(_.map(bifilter,'CHECKOUTTIME'));
    // d. Modified other field
    // e. zero value
    var zlist=['SAMPLEQTY','BODYQTY','MARKQTY','FOOTQTY','FACEQTY','FIRSTMARKQTY','FIRSTFOOTQTY'];
    zlist.forEach(function(field){
      settleResult[field]=0;
    });
    // settle: EQUIPMENTNO, CHECKINTIME, CHECKOUTTIME, DIEQTY, TESTQTY, PASSQTY, FAILQTY, BIN1~BIN8,
    //  SCRAPQTY, LOSSQTY, OTHERQTY, FinalYield, Diff, FirstYield, FirstBin1~8, FirstMarkQty, FirstFootQty, FirstPassQty
    // Sum field: BIN, FIRSTBIN, SCRAPQTY, LOSSQTY, OTHERQTY, FAILQTY
    
    var bifilter = undefined;
    if(ftype.yield==1 || ftype.yield==3)
      bifilter = _.filter(lotlogDocs,function(o){return o.OPNO.startsWith('BURNIN1')});
    else if(ftype.yield==4)
      bifilter = _.filter(lotlogDocs,function(o){return o.OPNO.startsWith('BURNIN')});
    
    settleResult['FAILQTY']= _.sumBy(bifilter,'FAILQTY');
    settleResult['SCRAPQTY']= _.sumBy(bifilter,'SCRAPQTY');
    settleResult['LOSSQTY']= _.sumBy(bifilter,'LOSSQTY');
    settleResult['OTHERQTY']= _.sumBy(bifilter,'OTHERQTY');

    settleResult['FIRSTPASSQTY']= _.sumBy(bifilter,'GOODQTY');
    settleResult['BIN1']=settleResult['FIRSTPASSQTY'];
    settleResult['FIRSTBIN1']=settleResult['BIN1'];
    settleResult['FIRSTYIELD']=settleResult['FIRSTPASSQTY']/settleResult['DIEQTY'];
    if(ftype.yield==1){
      var mlc_filter = _.filter(lotlogDocs,function(o){return o.OPNO.startsWith('MERGE LOT CHECK1')});
      var mlc_passqty = _.sumBy(mlc_filter,'GOODQTY');
      var mlc_testqty = _.sumBy(mlc_filter,'INPUTQTY');
      settleResult['PASSQTY']=mlc_passqty;
      settleResult['FINALYIELD']=mlc_passqty/mlc_testqty;
    }
    else if(ftype.yield==3){
      var mlc_filter = _.filter(lotlogDocs,function(o){return o.OPNO.startsWith('Split-MERGE LOT CHECK1')});
      var mlc_passqty = _.sumBy(mlc_filter,'GOODQTY');
      var mlc_testqty = _.sumBy(mlc_filter,'INPUTQTY');
      settleResult['PASSQTY']=mlc_passqty;
      settleResult['FINALYIELD']=mlc_passqty/mlc_testqty;
    }
    else if(ftype.yield==4){
      settleResult['PASSQTY']=settleResult['FIRSTPASSQTY'];
      settleResult['FINALYIELD']=settleResult['FIRSTYIELD'];
    }
    settleResult['DIFF']=settleResult['FINALYIELD']-settleResult['FIRSTYIELD'];
    var reportType='Sheet1';
    // fill missing column
    yieldReportfilledGroupColumn(lotlogDocs);
    sheetData[reportType].settledata.push(settleResult);
    sheetData[reportType].rawdata.push.apply(sheetData[reportType].rawdata,lotlogDocs);
    settleList.push(settleResult);
  }
  var filename=_.random(1000)+'-'+new Date().getTime()+'.xlsx';
  //console.log(filename);
  // Gen Data of RawDataChartAnalysis Sheet
  var excleChartData = getExcelChartData(sheetData,ftype);
  writeYieldReport(reportFolder+filename,sheetData,excleChartData,next,function(err){
    if(err){
      console.log(err.stack);
      return res.status(400).send('write report error!');
    }
    res.json({sheetdata:sheetData,filename:filename,length:f_lotloglist.length});
  });
}; // otherSettleFunction

var filterFormat = function(colname,value){
   var valuesplit = value.split(',');
   var filtercmd = colname+" in :valuesplit";
   return oqtool.insertListToQcmd(filtercmd,{':valuesplit':valuesplit});
};

var setFieldInList = function(field,values){
  var vlist = values.split(',');
  if(vlist.length>1){
    var respText = field+' in (';
    for(var i in vlist){
      respText+="'"+vlist[i]+"'";
      if(i<vlist.length-1)
        respText+=",";
    }
    return respText+=")";
  }
  else
    return field+"='"+values+"'";
}

var setWeek = function(doc){
  doc.WEEK = rebi.getWeek(doc.CHECKOUTTIME);
};
var setDM = function(doc){
  // Set Detailed column for Date
  var date = new Date(doc.CHECKOUTTIME);
  doc.DAY = doc.CHECKOUTTIME.slice(0,10);
  doc.MONTH = doc.CHECKOUTTIME.slice(0,7);
};


var setDataFormat = function(data,timeRange,ftype,rebionly,res,next){
  var lotlog_map = data.lotlogMap;
  var rootlot_dieqty = data.rootLotDieQty;
  // 4.1 flatten the lotlog_map 
  var f_lotloglist = [];
  var lotloglist = _.values(lotlog_map);
  lotloglist.forEach(function(doc){
    for(var i in doc.lotlogs){
      // one group may be assign to many doc.lotlog new data will replcae old data (use same pointer)
      // so put doc.lotlog be assign object
      var flotlog = _.assignIn(doc.lotlogs[i],doc.group);
      // remove time zone (time of this db be saved utc+8 as utc+0)
      if(flotlog.CHECKOUTTIME >= timeRange.from && flotlog.CHECKOUTTIME <= timeRange.to)
        flotlog.inrange=true;
      else
        flotlog.inrange=false;
      if(rebionly && !flotlog.inrange && ftype.rebi!=3)
        continue; // ingnore this log
      setWeek(flotlog); // +8 hours 前處理
      flotlog.CHECKINTIME = new Date(flotlog.CHECKINTIME.getTime()+UTCTimeNumber*oneHour);
      flotlog.CHECKOUTTIME = new Date(flotlog.CHECKOUTTIME.getTime()+UTCTimeNumber*oneHour);
      flotlog.CHECKINTIME = flotlog.CHECKINTIME.toISOString().slice(0,19);
      flotlog.CHECKOUTTIME = flotlog.CHECKOUTTIME.toISOString().slice(0,19);
      setDM(flotlog);
      if(!rebionly)
        flotlog['DIEQTY']=rootlot_dieqty[flotlog['LOTNO'].slice(0,7)];
      f_lotloglist.push(flotlog);
    }
  });
  
  if(rebionly){
    if(ftype.rebi==2){
      f_lotloglist.forEach(function(doc){
        for(var i=2;i<=8;i++)
          if(doc['BIN'+i]==undefined)
            doc['BIN'+i]=0;
      });
    }
    res.json({rawdata:f_lotloglist,length:f_lotloglist.length});
  }
  else{
    // 4.2 label group which dose not have Split-MFT
    var groupByRoot = _.groupBy(f_lotloglist,'ROOTLOT');
    for(var rootlot in groupByRoot){
      var lotlogDocs = groupByRoot[rootlot];
      var spmftCount = _.countBy(lotlogDocs,{OPNO:'Split-MFT'});
      // Dose not has 'Split-MFT' OPNO remove all data of this group
      // if ftype not type 2 (for nvidia) not use this rule
      if(spmftCount['true']==undefined && ftype.yield==2){
        // _.remove(f_lotloglist,function(doc){return doc.ROOTLOT==rootlot});
        lotlogDocs.forEach(function(doc){
          doc['nosplitmft'] = true;
        });
      }
    }
    // 4.3 根據使用者設定結算資料
    if(ftype.yield==2)
      nvSettleFunction(f_lotloglist,ftype,res,next);
    else
      otherSettleFunction(f_lotloglist,ftype,res,next);
  }
};//append Bin Data
exports.setFieldInList = setFieldInList;

exports.setFieldInList = setFieldInList;
exports.searchYieldReport = function(req, res, next) {
  req.setTimeout(1800000); // 30 min
  var match = req.body.match;
  var ftype = req.body.ftype; // structure: {yield: number}, number has 1~4
  var rebionly = req.body.rebionly;
  //match = {LOTNO:'LHA075EAC1'};
  // Example lotno:
  // standard: LHA06AUAC1, customize ls: LHA075EAC1, customize nols: LH80YYCAB1
  /**
   Example of match structure:
   {LOTNO:'LH60U1IAC1',CUSTOMERLOTNO:'PFBY68.M00',CUSTOMERCHISNAME:'nVIDIA',DATETIME:{FROM:,TO:}}
  */
  var missingParams=true;
  for(var key in match)
    if(key!='DATETIME'){
      missingParams=false;
      break;
    }
  if(missingParams)
    return res.status(400).json('Missing parameters for query!');
  // set time ragnge, remove time zone
  
  var from = new Date(match.DATETIME.FROM);
  var to = new Date(match.DATETIME.TO);
  //from=new Date(from.setHours(from.getHours()+8));
  //to=new Date(to.setHours(to.getHours()+8));
  var timeRange = {to:to,from:from};
    
  // try to get lotno and customerlotnolog
  
  var fparams = [];
  var lotdata_filter="";
  //test
  if(match.LOTNO)
    lotdata_filter=setFieldInList('LOTNO',match.LOTNO);//"LOTNO='"+match.LOTNO+"'";
  else if(match.CUSTOMERLOTNO)
    lotdata_filter=setFieldInList('CUSTOMERLOTNOLOG',match.CUSTOMERLOTNO);//"CUSTOMERLOTNOLOG='"+match.CUSTOMERLOTNO+"'";
  else{
    lotdata_filter="CHECKOUTTIME between :fromDate and :toDate";
    fparams={fromDate:timeRange.from,toDate:timeRange.to};
  }
  
  var where_filter="";
  for(var key in match){
    if(key=='DEVICENO' || key=='PRODUCTNO'){
      if(where_filter.length>0)
        where_filter+=' and ';
      where_filter+="A."+key+"LOG='"+match[key]+"'";
    }
    else{
      var tableNo='';
      if(key=='CUSTOMERCHISNAME' || key=='CUSTOMERNAME')
        tableNo='B.';
      else if(key=='FAMILY')
        tableNo='C.';
      else if(key=='PACKAGETYPE')
        tableNo='D.';
      if(tableNo.length>0){
        if(where_filter.length>0)
          where_filter+=' and ';
        where_filter+=setFieldInList(tableNo+key,match[key]);//tableNo+key+"='"+match[key]+"'";
      }
    }
  }
  if(where_filter.length>0)
    where_filter=' where '+where_filter;
  // 1. defined rootlon query substr 1 to 7 of lotn
  var fstqcmd = "select DISTINCT SUBSTR(A.LOTNO,1,7) as PLOTNO"+
  " from (select LOTNO,CUSTOMERLOTNOLOG,DEVICENOLOG,PRODUCTNOLOG from HISMFT.TBLWIPLOTLOG where "+lotdata_filter+") A";
  if(where_filter.length>0){
    var leftjoin = " left join HISMFT.TBLWIPLOTDATA D on A.LOTNO=D.LOTNO "+
    "left join Masterdm.DIM_Customer B on D.CUSTOMERNO=B.CUSTNO "+
    "left join HISMFT.TBLENGPRODUCTDATA C on D.PRODUCTNO=C.PRODUCTNO "+
    where_filter;
    fstqcmd+=leftjoin;
  }  
  if(ftype.yield==2 || ftype.rebi==2){
    rebi.getLotLogWithPath(fstqcmd,fparams,ftype,'yreport',rebionly,res,function(data){
      //console.log('### yreport');
      setDataFormat(data,timeRange,ftype,rebionly,res,next);
    });
  }
  else{
    rebi.getLotLogWithPath(fstqcmd,fparams,ftype,'rebi',rebionly,res,function(data){
      //console.log('### rebi');
      setDataFormat(data,timeRange,ftype,rebionly,res,next);
    });
  }
};

exports.downloadYieldReport = function(req, res, next) {
  var filename = req.body.filename;
  var filepath = reportFolder+filename;
  if(fs.existsSync(filepath)){
    res.download(filepath);
  }
  else
    return res.status(400).json('Can not find file '+filename+'.');
};