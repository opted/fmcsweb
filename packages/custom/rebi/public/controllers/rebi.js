'use strict';
var openFile = undefined;
var daySeconds = 24*60*60*1000;
function RebiController($scope, Global, $stateParams, $window, $http, utilsSrchOpt, common, $uibModal, MeanUser, _, checkInitalParams) {
  var removeAllTabActive = function(){
    $('#rebi').removeClass();
    $('#rebiyield').removeClass();
    $('#yieldreport').removeClass();
    $('#rebi-tab').removeClass().addClass('tab-pane');
    $('#rebiyield-tab').removeClass().addClass('tab-pane');
    $('#yieldreport-tab').removeClass().addClass('tab-pane');
  };
  var switchTab = function(tabname){
    removeAllTabActive();
    if(tabname=='yieldreport'){
      $('#yieldreport').addClass('active');
      $('#yieldreport-tab').addClass('tab-pane in active');
      $scope.changeAnalysis('yield');
    }
    else if(tabname=='rebiyield' || tabname=='yield'){
      $('#rebiyield').addClass('active');
      $('#rebiyield-tab').addClass('tab-pane in active');
      $scope.changeAnalysis('yield');
    }
    else{
      $('#rebi').addClass('active');
      $('#rebi-tab').addClass('tab-pane in active');
      $scope.changeAnalysis('rebi');
    }
  };
  /*
    search panel
  */
  // multiple selection for customer or family
  $scope.multiSelection = {customername:[],family:[]};
  $scope.multiOption = {customername:[],family:[]};
  var multiSelectionMap = {'customername':'CUSTOMERCHISNAME','family':'FAMILY'};
  $scope.updateZone = function(selectObject,type){
    var searchStr = selectObject.search;
    if(searchStr.length>0){
      $http.post('/api/rebi/mes/getmesmultiselection',{fstr:searchStr,field:type}) // type is customername or family
      .then(function(response){
        $scope.multiOption[type] = response.data;
      },
      function(err){
        console.log(err);
      });
    }
  };
  
  $scope.selected = {};
  $scope.mesfields = [
    utilsSrchOpt.genField("客戶型號(DeviceNo)", "DEVICENO", "string", true, true),
    utilsSrchOpt.genField("產品型號(ProductNo)", "PRODUCTNO", "string", true, true),
    utilsSrchOpt.genField("封裝型號(PackageType)", "PACKAGETYPE", "string", true, true),
    utilsSrchOpt.genField("客批(CustomerLotno)", "CUSTOMERLOTNO", "string", true, true),
    utilsSrchOpt.genField("內批(LotNo)", "LOTNO", "string", true, true)
  ];
  $scope.mesfsURL = '/api/rebi/mes/getsearchfield';
  $scope.filterExtraMatch = {datetime:$scope.selected,multiselection:$scope.multiSelection};
  $scope.resetTime = function(){
    var curtime = new Date();
    curtime.setSeconds(0,0);
    $scope.selected.from = new Date(common.getToday().getTime()-daySeconds);
    $scope.selected.to = curtime;
  };
  $scope.resetTime();
  /*
    search panel: formula config part
  */
  $scope.formulaType = {
    rebi:{selected:1,itemlist:[1,2,3,4]},
    yield:{selected:1,itemlist:[1,2,3,4]}
  };
  $scope.changeFormula = function(changFormula){
    if(changFormula=='rebi')
      $scope.formulaType.yield.selected = $scope.formulaType.rebi.selected;
  }
  $scope.formulaTableModal = function(){
    $scope.modalInstance = $uibModal.open({
      templateUrl: 'rebi/views/formulaTableModal.html',
      size: 'lg',
      scope: $scope
    });
  };
  $scope.closeModal = function(){
    $scope.uploadStatus = undefined;
    $scope.modalInstance.dismiss('cancel');
  };
  $scope.trendChartFormulaComment = 
  '<h4>範例</h4>\
  套公式1計算2018/01/01當日FAMILY=\'SD6611\'的ReBI Rate<br>\
  <ol>\
  <li>查詢2018/01/01當日、Family為SD6611之所有批號資料，標記BurnIn站點的ReBurn子批，可查族譜圖或利用母批的DieQty去推算何者為ReBurn子批。</li>\
  <li>則 ReBI Rate = (BurnIn站點ReBurn子批(被標記)) / (BurnIn站點一般子批(未被標記))。</li>\
  </ol>';
  /*
    Check fail bin, for type 2 only
  */
  var checkItem = {family:'',list:{1:false,2:true,3:true,4:true,5:true,6:true,7:true,8:true}};
  $scope.failBinCheckBox = [];
  $scope.addFailBinCheckBox = function(){
    $scope.failBinCheckBox.push(_.cloneDeep(checkItem));
  }
  $scope.removeFailBinCheckBox = function(index){
    $scope.failBinCheckBox.splice(index,1);
  }
  $scope.addFailBinCheckBox();
  
  /*
   query summary information
  */
  var tabKeys = ['rebi','yield'];
  $scope.tabkey = tabKeys[0];
  $scope.searchTypeName = {yield:'ReBI+YieldReport查詢',rebi:'ReBI查詢'};
  var infoTemp = {requestStatus:undefined,queryTime:undefined,rowNum:undefined,ftypeText:undefined,waitingResponse:false};
  $scope.queryInfo = {rebi:_.cloneDeep(infoTemp),yield:_.cloneDeep(infoTemp)};
  var initQueryInfo = function(searchType){
    for(var key in $scope.queryInfo[searchType])
      $scope.queryInfo[searchType][key]=undefined;
  }
  var setQueryInfo = function(doc,searchType){
    var tabkey=tabKeys[0];
    if(searchType=='yield')
      tabkey = tabKeys[1];
    for(var key in doc)
      $scope.queryInfo[tabkey][key]=doc[key];
  }
  /*
    RawData
  */
  $scope.amchartRawData = [];
  /**
  Handel Yield Report
  */
  $scope.downloadFileName = undefined;
  $scope.YpChartData = {rawdata:[],settledata:[],ftype:{}};
  $scope.downloadYieldReport = function(){
    var reportFileName = 'yield-report.xlsx';
    $http.post('/api/yieldreport/download',{filename:$scope.downloadFileName},{responseType:'arraybuffer'})
    .then(function(response){
      var data = new Blob([response.data],{type: 'octet/stream'});
      saveAs(data, reportFileName);
    },
    function(err){
      console.log(err);
    });
  };
  /*
    Query
  */
  var successFunction = function(rawdata,params,settledata,searchType){
    var chartRawData = {data:rawdata,ftype:params.ftype,failbinlist:_.cloneDeep($scope.failBinCheckBox),match:params.match};
    
    if(settledata)
      chartRawData['settledata']=settledata;
    else
      chartRawData['settledata']=[];
    
    if(searchType=='yield')
      $scope.rebiYieldRawdata = chartRawData;
    else if(searchType=='rebi')
      $scope.rebiRawdata = chartRawData;
    var reqstatus;
    if(rawdata.length==0)
      reqstatus = '搜尋條件查無資料';
    else
      reqstatus = $scope.searchTypeName[searchType]+'完成';
    setQueryInfo({
      requestStatus:reqstatus,
      rowNum:rawdata.length,
      queryTime:new Date() - $scope.startTime,
      ftypeText:"ReBIRate: Type"+params.ftype.rebi+", Yield: Type"+params.ftype.yield,
      waitingResponse: false
    },searchType);
    prevStatus = {match:_.cloneDeep(params.match),rownum:$scope.rowNum,ftype:_.cloneDeep(params.ftype)};
  };
  var errorFunction = function(err,searchType){
    setQueryInfo({
      requestStatus:'查詢失敗, '+err.data,
      waitingResponse:false
    },searchType);
    console.log(err);
  };
  var prevStatus = {match:{},ftype:{}};
  var RebiFailBinHint = function(searchFamily){
    if(searchFamily){
      var familylist = _.uniq(searchFamily);
      var FailBinMap = _.groupBy($scope.failBinCheckBox,'family');
      var hintMessage='ReBI Fail Bin:\n';
      for(var i in familylist){
        var key = familylist[i];
        var failbindoc = FailBinMap[key];
        if(failbindoc){
          failbindoc = failbindoc[0];
          hintMessage+='* '+key+': failbin: ';
          var failbinnos = '';
          for(var binno in failbindoc.list)
            if(failbindoc.list[binno])
              failbinnos+=binno+',';
          failbinnos = _.trimEnd(failbinnos,',');
          hintMessage+=failbinnos+'\n';
        }
        else
          hintMessage+='* '+key+': failbin: 2~8\n';
      }
      hintMessage+='* Others: failbin: 2~8 (default)';//_.trimEnd(hintMessage,'\n');
      return confirm(hintMessage);
    }
    else
      return true;
  };
  $scope.rebiYieldQuery = function(searchType){
    initQueryInfo(searchType);
    //clear previous status
    var params = {match:utilsSrchOpt.getMatch($scope.mesfields)};
    params['ftype'] = {rebi:$scope.formulaType.rebi.selected,
      yield:$scope.formulaType.yield.selected};
    if(params.ftype.rebi==2){
      var familylist = _.map($scope.failBinCheckBox,'family');
      _.remove(familylist,function(o){return o.length<=0});
      if(!RebiFailBinHint(familylist))
        return;
    }
    /*if(params.ftype.rebi==2){ // assign family filter by fail bin check function
      var familylist = _.map($scope.failBinCheckBox,'family');
      _.remove(familylist,function(o){return o.length<=0});
      params.match.FAMILY='';
      familylist.push.apply(familylist,$scope.multiSelection['family'])
      $scope.multiSelection['family'] = _.uniq(familylist);
      for(var i in familylist){
        if(familylist[i].length<=0)
          continue;
        params.match.FAMILY+=familylist[i];
        if(i<familylist.length-1 && params.match.FAMILY.length>0)
          params.match.FAMILY+=','
      }
      if(params.match.FAMILY.length<=0)
        delete params.match.FAMILY;
      // show fail bin hint
      if(!RebiFailBinHint(params.match.FAMILY))
        return;
    }*/
    // Assign multiselection result
    for(var key in $scope.multiSelection){
      var selectionList = $scope.multiSelection[key];
      if(!_.isEmpty(selectionList)){
        if(key=='customername'){
          selectionList = _.map(selectionList,function(o){return o.split(',')[0];});
        }
        params.match[multiSelectionMap[key]]='';
        for(var i in selectionList){
          params.match[multiSelectionMap[key]]+=selectionList[i];
          if(i < selectionList.length-1)
            params.match[multiSelectionMap[key]]+=',';
        }
      }
      else
        params.match[multiSelectionMap[key]] = undefined;
    }
    var noFilter = true;
    for(var key in params.match)
      if(params.match[key]!=undefined){
        if(params.match[key].length>1)
          noFilter = false;
      }
    if(noFilter)
      return alert('至少篩選一個欄位');
    params.match.DATETIME = {FROM:$scope.selected.from,TO:$scope.selected.to};
    // check the $scope.rawdat has data or not
    $scope.startTime = new Date();
    var queryStatus = $scope.searchTypeName[searchType]+'中, 等待 Server 回應...';
    // Testing query
    /*alert('This is testing query!');
    params.ftype = {rebi:2,yield:2};
    params.match.DATETIME = {FROM:new Date('2018-01-04'),TO:new Date('2018-01-06')};
    var testparam = {CUSTOMERCHISNAME:'nVIDIA',CUSTOMERLOTNO:'PGKY64.C01',FAMILY:'GV1004S4'};
    for(var key in testparam)
      params.match[key] = testparam[key];*/
    
    if(searchType=='yield'){
      $scope.downloadFileName=undefined;
      $scope.rebiYieldRawdata = undefined;
      $scope.ReportChartSheetData = undefined;
      switchTab(searchType);
    }
    else{
      $scope.rebiRawdata = undefined;
      switchTab(searchType);
    }
    setQueryInfo({
      requestStatus:queryStatus,
      rowNum:undefined,
      queryTime:undefined,
      waitingResponse:true
    },searchType);
    if(searchType=='yield'){
      $http.post('/api/yieldreport/searchyreport',params).then(
        function(response){
          //sheetData[JSON]:{Standard:{rawdata:[],settledata:[]},Customize_LS:{}, Customize_no_LS:{}} 
          // or {Sheet1:{rawdata:[],settledata:[]}}
          var sheetData = response.data.sheetdata;
          
          $scope.ReportChartSheetData = {sheetdata:sheetData,ftype:params.ftype,match:_.cloneDeep(params.match)};
          
          $scope.downloadFileName = response.data.filename;
          // combine all rawdata
          var rawdata = [];
          var settledata = [];
          for(var key in sheetData){
            rawdata.push.apply(rawdata,sheetData[key].rawdata);
            settledata.push.apply(settledata,sheetData[key].settledata);
          }
          successFunction(rawdata,params,settledata,searchType);
        }
        ,function(err){errorFunction(err,searchType)});
    }
    else if(searchType=='rebi'){
      $http.post('/api/rebi/searchrebi',params).then(
        function(response){
          // respnose: {rawdata:[],length:Number}
          successFunction(response.data.rawdata,params,undefined,searchType);
        }
        ,function(err){errorFunction(err,searchType)});
    }
  };
  
  $scope.changeAnalysis = function(atype){
    if(atype=='rebi')
      $scope.tabkey = tabKeys[0];
    else
      $scope.tabkey = tabKeys[1];
  };
  /**
    Set initial status
  */
  if(checkInitalParams){
    switchTab(checkInitalParams.inittab);
  }
}

angular
    .module('mean.rebi')
    .controller('RebiController', RebiController);

RebiController.$inject = ['$scope', 'Global', '$stateParams', '$window', '$http', 'utilsSrchOpt', 'common', '$uibModal','MeanUser','_','checkInitalParams'];
