
(function(){
	'use strict';
	function yieldReportChart(Rebi,common,_){
    var chartName = "yreport-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=' // Data {rawdata:[],settledata:[]}
			},
			template: 
      // drilldown tab list,
      '<div id="rebi-loadingchart" ng-style="loadingChartStyle" class="utils-loading-chart" is-loading="chartLoading" ng-if="chartLoading"></div>'+
      '<div ng-show="display">\
      <div class="ypchart">\
      <uib-tabset active="drillDownTabActive">\
        <uib-tab ng-repeat="tab in drillTablist" ng-click="changeDrillDownTab($index)" index="tab.id">\
        <uib-tab-heading>\
          {{tab.title}}<button type="button" class="close" aria-label="Close" style="font-size:medium" ng-click="closeTab($index)" ng-show="$index>0">\
          <span aria-hidden="true">&times;</span>\
          </button>\
        </uib-tab-heading>\
        </uib-tab>\
      </uib-tabset>'+
      // tab title
      '<h3>{{ curChartParams.tabtitle }}</h3>'+
      // grouping button
      // render all pill tabset and hide unneeded tabset to fix a bug
      '<div ng-repeat="tab in drillTablist"><uib-tabset class="rebi-groupby" type="pills" ng-show="selectedTabItemIndex==$index">\
      <uib-tab index="$index" ng-repeat="item in tab.chartparams.groupby.itemlist" heading="{{GroupByMapping[item]}}" select="changeGroup(item)">\
      </uib-tab>\
      </uib-tabset></div>'+
      // sorting select
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">排序</label>\
        <select vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglist" ng-change="changeSorting(0)">\
        </select>\
        <div class="btn-group vcenter">\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="0" ng-click="changeSorting(1)">降序</label>\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="1" ng-click="changeSorting(2)">升序</label>\
        </div>\
      </div>'+
      // shift control
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">推移</label>\
        <input type="number" name="input" ng-model="shiftControl.number" class="form-control"></input>\
        <select class="form-control" ng-model="shiftControl.selected" ng-options="opt for opt in shiftControl.itemlist">\
        </select>\
      </div>'+
      // control hint
      '<label class="text-primary">* 單擊drill down, 雙擊查詢推移圖</label>'+
      // amchart
      '<div id="'+chartName+'" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      '<div class="row"><div class="col-md-6"><div style="height:200px;padding-left:20px;" class="ag-blue" ag-grid="gridOptions"></div></div>\
      <div class="col-md-6"><button class="btn btn-success" ng-click="exportAgGrid(\'chartdata\',\'xls\')">下載Excel</button></div></div>'+
      // shift chart
      '<shift-chart data="shiftChartData" type="yp" opts="shiftOpts"></shift-chart>'+
      '</div>'+ // div class=ypchart
       // ag-grid
      '<hr><!-- Navigation Tab -->\
      <ul class="nav nav-tabs" ng-show="resultFormulaType==2">\
        <li class="active"><a data-toggle="tab" ng-click="switchReport(\'std\')" href="#std">Standard</a></li>\
        <li><a data-toggle="tab" ng-click="switchReport(\'cus_ls\')" href="#cus_ls">Customize_LS</a></li>\
        <li><a data-toggle="tab" ng-click="switchReport(\'cus_no_ls\')" href="#cus_no_ls">Customize_no_LS</a></li>\
      </ul>\
      <h3>Raw Data</h3>\
        <div style="height: 300px;" class="ag-blue" ag-grid="rawDataGrid"></div>\
        <button class="btn btn-success" ng-click="exportAgGrid(\'rawdata\',\'xls\')">下載Excel</button>\
      <h3>結算資料</h3>\
      <label class="text text-primary">* By客批(CustomerLotNo)結算 </label>\
      <div style="height: 200px;" class="ag-blue" ag-grid="settleDataGrid"></div>\
      <button class="btn btn-success" ng-click="exportAgGrid(\'settledata\',\'xls\')">下載Excel</button>\
      </div>'
      , // chart
			link: function (scope, element, attrs) {
        scope.display = false;
        /**
          Chart parameters, use to drilldown, sorting, limit or change group by
        */
        /*
          Chart params structure
          {
          level:,//drill down 等級，初始等級為0
          groupby:{itemlist:["LOTNO","ROOTLOT",...],selected:"LOTNO",active:0}, // 分群用項目
          sorting:{selected:'X軸字典排序',type:0},//可用的排序規則記在外部
          limit:{selected:'X軸字典排序',type:0}, //圖表呈現上限
          drillpath:[],
          data:{rawdata:[],settledata:[]},
          showdata:[]
          }
        */
        scope.curChartParams = undefined;
        scope.GroupByMapping = Rebi.ypGroupByMapping();
        scope.changeGroup = function(item){
          Rebi.chartChangeGroup(scope,chart,item);
          // set shift chart data
          scope.shiftOpts = scope.curChartParams.shiftdata.opts;
          scope.shiftChartData = scope.curChartParams.shiftdata.data;
          scope.gridOptions.api.setRowData(scope.curChartParams.allshowdata);
        };
        // sorting selected
        scope.sortinglist = ['X軸字典排序','testqty 數值排序','first yield 數值排序','final yield 數值排序'];
        scope.changeSorting = function(changeItem){
          Rebi.chartChangeSorting(scope,chart,changeItem);
        };
        /**
          Drill down control
        */
        scope.drillDownTriggerChangeGroup=false;
        scope.drillTablist = [];
        scope.drillDown = function(graphItemCategory){
          Rebi.chartDrillDown(scope,chart,graphItemCategory);
        };
        scope.triggerCloseTab = false;
        scope.selectedTabItemIndex = 0;
        scope.changeDrillDownTab = function(index){
          Rebi.chartChangeDrillDownTab(scope,chart,index);
        };
        scope.closeTab = function(tabindex){
          Rebi.chartCloseTab(scope,tabindex);
        };
        /**
          Defined amchart options
        */
        var setBallonText = function(graphDataItem,postfixSymbol){
          var value = graphDataItem.values.value;
          var title = graphDataItem.graph.title;
          var category = graphDataItem.category;
          return "<span style='font-size:12px;'>"+title+" in "+category+":<br><span style='font-size:20px;'>"+value+postfixSymbol+"</span>[[additional]]</span>";
        }
        var basicBallonFunction = function(graphDataItem, graph){
          return setBallonText(graphDataItem,'');
        };
        var percentBallonFunction = function(graphDataItem, graph){
          return setBallonText(graphDataItem,'%');
        };
        var chartBallonFunction = common.amChartCustomizeBallonFunction();
        // bar1: #118AB2, bar2: #06D6A0, bar3: #073B4C, line1: #FFD166, line2: #EF476F
        var chartOptions = {
          "type": "serial",
          "addClassNames": true,
          "theme": "light",
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
                "useGraphSettings": true,
                "markerSize":12,
                "valueWidth":0,
                "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [ {
            "id": "v1",
            "title": "Yield",
            "position": "left",
            "maximum":100,
            //"minimum":0,
            "labelFunction": function(value) {
              return value+"%";
            }
          },
          {
            "id": "v2",
            "title": "TestQty",
            "position": "right",
            "integersOnly": true
          }],
          "startDuration": 1,
          "graphs": [ 
            {
              "id":"testqty",
              "balloonFunction": chartBallonFunction.basic,
              "fillAlphas": 1,
              "title": "test qty",
              "type": "column",
              "valueField": "testqty",
              "valueAxis": "v2",
              "dashLengthField": "dashLengthColumn",
              "lineColor": "#118AB2",
              "labelText": "[[value]]",
              "showAllValueLabels":true
            },
            {
            "id": "finyield",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "lineColor":"#FFD166",
            "title": "final yield",
            "valueField": "finyield",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
           },
           {
            "id": "fstyield",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "lineColor":"#EF476F",
            "title": "first yield",
            "valueField": "fstyield",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          }
          ],
          "categoryField": "key",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":10
          },
          "export": {
            "enabled": true
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
          "chartScrollbar": {
          }
        };

				var chart = AmCharts.makeChart(chartName, chartOptions);
         // 設定推移圖互動功能
        Rebi.rebiExtraSetChart(chart,scope);
        /**
          Whating the data link of outer controller
        */
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined){
            scope.display = false;
            return;
          }
          var sheetData = scope.data.sheetdata;
          var rawdata=[];
          var settledata=[];
          for(var key in sheetData){
            _.remove(sheetData[key]['rawdata'],{nosplitmft:true}); // remove no split-mft data
            rawdata.push.apply(rawdata,sheetData[key].rawdata);
            settledata.push.apply(settledata,sheetData[key].settledata);
          }
          var ftype = scope.data.ftype;
          scope.queryMatch = scope.data.match;
          if(_.isArray(rawdata)){
            if(rawdata.length>0){
              scope.display = true;
              // set am-chart
              scope.curChartParams = Rebi.initYpChartParams(rawdata,settledata,ftype);
              scope.drillTablist = [];
              scope.drillTablist.push(Rebi.getInitDrillTab(scope.curChartParams));
              chart.dataProvider = scope.curChartParams.showdata;
              chart.validateData();
              chart.validateNow();
              // set chart ag-grid
              scope.gridOptions.api.setRowData(scope.curChartParams.allshowdata);
              setTimeout(function(){
                scope.drillDownTabActive = scope.drillTablist[0].id;
                scope.$apply();
              },100);
              // set raw data ag-grid init variable
              scope.sheetData = sheetData;
              scope.resultFormulaType = ftype.yield;
              scope.setAgGridRowData();
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
				chart.validateData();
				chart.validateNow();
			},//link
    controller:['$scope',function($scope){
      /**
        Shift Chart control
      */
      $scope.shiftControl = Rebi.getShiftControl();
      /**
        Chart Data Ag-Grid
      */
      var showPercentFunction = function(params){
        return params.value+'%';
      };
      var gridColumnDefs = [
        {headerName: 'Key', field: 'key', filter: 'text', width: 150},
        {headerName: 'FinalYield', field: 'finyield', filter: 'text', width: 100,cellRenderer:showPercentFunction},
        {headerName: 'FirstYield', field: 'fstyield', filter: 'text', width: 100,cellRenderer:showPercentFunction},
        {headerName: 'Gap', field: 'gap', filter: 'text', width: 100,cellRenderer:showPercentFunction},
        {headerName: 'TestQty', field: 'testqty', filter: 'number', width: 100}
      ];
      $scope.gridOptions = {
        columnDefs:gridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.exportAgGrid = function(gridType,exportType){
        if(gridType=='chartdata')
          common.exportAgGrid('chart_data',exportType,$scope.gridOptions);
        else if(gridType=='rawdata')
          common.exportAgGrid('raw_data',exportType,$scope.rawDataGrid);
        else if(gridType=='settledata')
          common.exportAgGrid('settle_data',exportType,$scope.settleDataGrid);
      };
      /**
        Switch Report
      */
      $scope.currentReportType = 'Standard';
      var reportTypeMap = {'std':'Standard','cus_ls':'Customize_LS','cus_no_ls':'Customize_no_LS'};
      // set ag-grid & am-chart rawdata
      $scope.switchReport = function(reportType){
        $scope.currentReportType=reportTypeMap[reportType];
        $scope.setAgGridRowData();
      };
      /**
        Raw Data Ag-Grid
      */
      $scope.agSettleData = [];
      // Undefined: Temp, BIHour, ProgramName, ProgramVer, Status
      var agYieldRoundedFunction = function(params){
        return _.round(params.value,4);
      };
      var rawDataColumnDefs = [
        {headerName: 'CustomerName', field: 'CUSTOMERCHISNAME', filter: 'text', width: 120},
        {headerName: 'CustomerLotNo', field: 'CUSTOMERLOTNOLOG', filter:'text', width: 120},
        {headerName: 'ProductFamily', field: 'FAMILY', filter: 'text', width: 120},
        {headerName: 'OpNo', field: 'OPNO', filter: 'text', width: 120},
        {headerName: 'Temp', field: 'TEMP', filter: 'number', width: 100},
        {headerName: 'LotNo', field: 'LOTNO', filter: 'text', width: 120},
        {headerName: 'DeviceNo', field: 'DEVICENOLOG', filter: 'text', width: 120},
        {headerName: 'PackageType', field: 'PACKAGETYPE', filter: 'text', width: 120},
        {headerName: 'BIHour', field: 'BIHOUR', filter: 'number', width: 100},
        {headerName: 'ProgramName', field: 'PROGRAMNAME', filter: 'text', width: 180},
        {headerName: 'ProgramVer', field: 'PROGRAMVER', filter: 'text', width: 100},
        {headerName: 'Tester', field: 'EQUIPMENTNO', filter: 'text', width: 100},
        {headerName: 'CheckInTime', field: 'CHECKINTIME', filter: 'text', width: 150},
        {headerName: 'CheckOutTime', field: 'CHECKOUTTIME', filter: 'text', width: 150},
        {headerName: 'DieQty', field: 'DIEQTY', filter: 'text', width: 100},
        {headerName: 'TestQty', field: 'TESTQTY', filter: 'text', width: 100},
        {headerName: 'PassQty', field: 'PASSQTY', filter: 'text', width: 100},
        {headerName: 'FailQty', field: 'FAILQTY', filter: 'text', width: 100},
        {headerName: 'ScrapQty', field: 'SCRAPQTY', filter: 'text', width: 100},
        {headerName: 'LossQty', field: 'LOSSQTY', filter: 'text', width: 100},
        {headerName: 'OtherQty', field: 'OTHERQTY', filter: 'text', width: 100},
        {headerName: 'FinalYield', field: 'FINALYIELD', filter: 'text', width: 100,cellRenderer:agYieldRoundedFunction},
        {headerName: 'Diff', field: 'DIFF', filter: 'text', width: 100,cellRenderer:agYieldRoundedFunction},
        {headerName: 'FirstYield', field: 'FIRSTYIELD', filter: 'text', width: 100,cellRenderer:agYieldRoundedFunction},
        {headerName: 'FirstPassQty', field: 'FIRSTPASSQTY', filter: 'text', width: 100},
        {headerName: 'ProductNo', field: 'PRODUCTNOLOG', filter: 'text', width: 100}
      ];
      // defined bin data
      for(var i=1;i<=8;i++)
        rawDataColumnDefs.push({headerName: 'Bin'+i, field: 'BIN'+i, filter: 'text', width: 100});
      for(var i=1;i<=8;i++)
        rawDataColumnDefs.push({headerName: 'FirstBin'+i, field: 'FIRSTBIN'+i, filter: 'text', width: 100});
      // clone column defined, remove some column not show insettleDataGrid
      var settleDataColumnDefs = [];
      for(var i in rawDataColumnDefs){
        var item = rawDataColumnDefs[i];
        if(item.headerName!='LotNo') // && item.headerName!='OpNo'
          settleDataColumnDefs.push(_.cloneDeep(item));
      }
      $scope.rawDataGrid = {
        columnDefs:rawDataColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.settleDataGrid = {
        columnDefs:settleDataColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.setAgGridRowData = function(init){
        if($scope.resultFormulaType==2){
          if($scope.sheetData){
            $scope.rawDataGrid.api.setRowData($scope.sheetData[$scope.currentReportType]['rawdata']);
            $scope.settleDataGrid.api.setRowData($scope.sheetData[$scope.currentReportType]['settledata']);
          }
        }
        else{
          if($scope.sheetData){
            $scope.rawDataGrid.api.setRowData($scope.sheetData['Sheet1']['rawdata']);
            $scope.settleDataGrid.api.setRowData($scope.sheetData['Sheet1']['settledata']);
          }
        }
      };
    }]//controller
		};
	};

	angular.module('mean.rebi').directive('yieldReportChart', yieldReportChart);
	yieldReportChart.$inject = ['Rebi','common','_'];
})();
'use strict';
