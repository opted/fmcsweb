
(function(){
	'use strict';
	function rawDataGrid(Rebi,common,_){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data {data:rawdata,ftype:{rebi:selectedtype,yield:selectedtype}}
        rebionly: '@?'
			},
			template: 
      `<hr><h3>ReBI Rate - Raw Data Table</h3>
      <div style="height: 250px;" class="ag-blue" ag-grid="rebiRateGrid"></div>
      <button type="button" class="btn btn-success" ng-click="exportAgGrid('rebirate','xls')">下載Excel</button>`+
      `<div ng-show='!rebionly'><hr><h3>Yield - Raw Data Table</h3>
      <div style="height: 250px;" class="ag-blue" ag-grid="rebiYieldGrid"></div>
      <button type="button" class="btn btn-success" ng-click="exportAgGrid('rebiyield','xls')">下載Excel</button></div>`, 
			link: function (scope, element, attrs) {
        
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data){
            var ftype = scope.data.ftype;
            if(_.isArray(scope.data.data)){
              if(ftype.rebi==3)
                scope.rebiRateGrid.api.setRowData(scope.data.data);
              else{
                if(ftype.rebi==2){
                  var gridColumnDefs = _.cloneDeep(scope.gridColumnDefs);
                  // set new columnDefs
                  for(var i=1;i<=8;i++)
                    gridColumnDefs.push({headerName: 'Bin'+i, field: 'BIN'+i, filter: 'text', width: 80});
                  scope.rebiRateGrid.api.setColumnDefs(gridColumnDefs);
                }
                scope.rebiRateGrid.api.setRowData(_.filter(scope.data.data,{inrange:true}));
              }
              scope.rebiYieldGrid.api.setRowData(_.filter(scope.data.data,function(o){return o.nosplitmft==undefined;}));
            }
          }
				});
			},//link
    controller:['$scope',function($scope){
      /**
          Ag-Grid Options
      */
      var gridColumnDefs = [
        {headerName: '批號', field: 'LOTNO', filter:'text', width: 120},
        {headerName: '客批', field: 'CUSTOMERLOTNOLOG', filter:'text', width: 100},
        {headerName: '站別', field: 'OPNO', filter: 'text', width: 150},
        {headerName: '母批', field: 'ROOTLOT', filter: 'text', width: 100},
        {headerName: '爐號', field: 'OVENID', filter: 'text', width: 100},
        {headerName: 'ProductFamily', field: 'FAMILY', filter: 'text', width: 100},
        {headerName: 'CheckInTime', field: 'CHECKINTIME', filter: 'text', width: 140},
        {headerName: 'CheckOutTime', field: 'CHECKOUTTIME', filter: 'text', width: 140},
        {headerName: 'TestQty', field: 'INPUTQTY', filter: 'text', width: 100},
        {headerName: 'PassQty', field: 'GOODQTY', filter: 'text', width: 100},
        {headerName: 'FailQty', field: 'FAILQTY', filter: 'text', width: 100},
        {headerName: 'OtherQty', field: 'OTHERQTY', filter: 'text', width: 100},
        {headerName: '客戶別', field: 'CUSTOMERCHISNAME', filter: 'text', width: 100},
        {headerName: '客戶型號', field: 'DEVICENOLOG', filter: 'text', width: 100},
        {headerName: '封裝型號', field: 'PACKAGETYPE', filter: 'text', width: 100},
        {headerName: 'BI次序', field: 'BILEVEL', filter: 'text', width: 100}
      ];
      $scope.gridColumnDefs = gridColumnDefs;
      
      $scope.rebiRateGrid = {
        columnDefs:gridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.rebiYieldGrid = {
        columnDefs:gridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.exportAgGrid = function(gridType,fileType){
        if(gridType=='rebirate')
          common.exportAgGrid('rebirate_export',fileType,$scope.rebiRateGrid);
        else if(gridType=='rebiyield')
          common.exportAgGrid('rebiyield_export',fileType,$scope.rebiYieldGrid);
      };
    }]
		};
	};

	angular.module('mean.rebi').directive('rawDataGrid', rawDataGrid);
	rawDataGrid.$inject = ['Rebi','common','_'];
})();
'use strict';