
(function(){
	'use strict';
	function shiftChart(Rebi,common,_){
		return {
			restrict: 'E',
			scope: {
				data: '=', //
        opts: '=',
        type: '@?'
			},
			template: 
      // loading chart hint
      //'<div id="rebi-loadingshiftchart" class="utils-loading-chart" is-loading="loading" ng-if="loading"></div>'+
      // drilldown tab list
      '<div ng-show="display">\
      <button class="btn btn-danger" style="float: right;" ng-click="removeData()"><span class="glyphicon glyphicon-remove"></span></button>\
      <h3>推移圖</h3>\
      <label class="text-primary">Filter: {{dataFilter}}</label>'+
      // amchart
      '<div id="{{chartName}}" style="width:100%;height: 250px; margin:20px;"><div></div></div>'+
      '</div>', // chart
			link: function (scope, element, attrs) {
        scope.display = false;
        var fieldNameMapping = _.assign(Rebi.ypGroupByMapping(),_.assign(Rebi.eaGroupByMapping(),Rebi.paGroupByMapping()));
        var type = scope.type;
        var chartName = scope.chartName = "shift-chart-"+type;
				var chart = undefined;
        scope.removeData = function(){
          chart.dataProvider = [];
          chart.validateData();
          chart.validateNow();
          scope.display = false;
          if(scope.opts.removeData)
            scope.opts.removeData();
        }
        /**
          Whating the data link of outer controller
        */
				scope.$watchGroup(['data','opts'], function(){ // Change of scope.data, Only trigger by http query
        
          /*if(scope.opts){
            if(scope.opts.loading){
              scope.loading = true;
              scope.display = true;
              return;
            }
            else{
              scope.loading = false;
            }
          }
          else
            return;*/
          
          if(scope.data==undefined){
            scope.display = false;
            return;
          }
          var rawdata = scope.data;
          var matchfilter = scope.opts.match;
          if(_.isArray(rawdata)){
            if(rawdata.length>0){
              if(chart){
                chart.dataProvider = rawdata;
                chart.validateData();
                chart.validateNow();
              }
              else{
                if(type=='ea'){
                  chart = AmCharts.makeChart(chartName, scope.getChartOptions('ea'));
                  chart.dataProvider = rawdata;
                  chart.validateData();
                  chart.validateNow();
                }
                else if(type=='pa'){
                  chart = AmCharts.makeChart(chartName, scope.getChartOptions('pa'));
                  chart.dataProvider = rawdata;
                  chart.validateData();
                  chart.validateNow();
                }
                else if(type=='yp'){
                  chart = AmCharts.makeChart(chartName, scope.getChartOptions('yp'));
                  chart.dataProvider = rawdata;
                  chart.validateData();
                  chart.validateNow();
                }
              }
              if(matchfilter){
                var filterText = '';
                for(var key in matchfilter){
                  if(key=='EQUIPMENTNO')
                    filterText+='爐號: '+matchfilter[key]+', ';
                  else
                    filterText+=fieldNameMapping[key]+': '+matchfilter[key]+', ';
                }
                scope.dataFilter = _.trimEnd(filterText,', ');
              }
              scope.display = true;
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
			},//link
    controller:['$scope',function($scope){
      /**
        Defined amchart options
      */
      var setBallonText = function(graphDataItem,postfixSymbol){
        var value = graphDataItem.values.value;
        var title = graphDataItem.graph.title;
        var category = graphDataItem.category;
        return "<span style='font-size:12px;'>"+title+" in "+category+":<br><span style='font-size:20px;'>"+value+postfixSymbol+"</span>[[additional]]</span>";
      }
      var chartBallonFunction = common.amChartCustomizeBallonFunction();
      var chartOptions = {
        "type": "serial",
        "addClassNames": true,
        "balloon": {
          "adjustBorderColor": false,
          "horizontalPadding": 10,
          "verticalPadding": 8,
          "color": "#ffffff"
        },
        "legend": {
              "useGraphSettings": true,
              "markerSize":12,
              "valueWidth":0,
              "verticalGap":0
        },
        "dataProvider": [],
        "valueAxes": [],
        "startDuration": 1,
        "graphs": [],
        "categoryField": "date",
        "categoryAxis": {
          "gridPosition": "start",
          "axisAlpha": 0,
          "tickLength": 5,
          "autoRotateAngle":-8,
          "autoRotateCount":10
        },
        "export": {
          "enabled": true
        },
        "chartCursor": {
          "categoryBalloonDateFormat": "DD",
          "cursorAlpha": 0.1,
          "cursorColor":"#000000",
          "fullWidth":true,
          "valueBalloonsEnabled": false,
          "zoomable": false
        }
      };
      
      $scope.getChartOptions = function(type){
        // set ea config
        if(type=='ea'){
          chartOptions.valueAxes = [{
            "id": "v1",
            "title": "Yield",
            "position": "left",
            "maximum":100,
            //"minimum":0,
            "labelFunction": function(value) {
              return value+"%";
            }
          },
          {
            "id": "v2",
            "title": "Re-Burn Rate",
            "position": "right",
            "maximum":100,
            //"minimum":0,
            "labelFunction": function(value) {
              return value+"%";
            }
          }];
          chartOptions.graphs = [{
              "id":"g1",
              "balloonFunction": chartBallonFunction.percent,
              "fillAlphas": 1,
              "title": "first yield",
              "type": "column",
              "valueField": "fstyield",
              "valueAxis": "v1",
              "dashLengthField": "dashLengthColumn",
              "labelText": "[[value]]",
              "showAllValueLabels":true
            },
            {
              "id":"g2",
              "balloonFunction": chartBallonFunction.percent,
              "fillAlphas": 1,
              "title": "final yield",
              "type": "column",
              "valueField": "finyield",
              "valueAxis": "v1",
              "dashLengthField": "dashLengthColumn",
              "labelText": "[[value]]",
              "showAllValueLabels":true
            }
            , {
              "id": "g3",
              "balloonFunction": chartBallonFunction.percent,
              "bullet": "round",
              "lineThickness": 3,
              "bulletSize": 7,
              "bulletBorderAlpha": 1,
              "bulletColor": "#FFFFFF",
              "useLineColorForBulletBorder": true,
              "bulletBorderThickness": 3,
              "fillAlphas": 0,
              "lineAlpha": 1,
              "title": "re-burn rate",
              "valueField": "rebirate",
              "valueAxis": "v2",
              "dashLengthField": "dashLengthLine",
              "labelText": "[[value]]",
              "showAllValueLabels":true
          }];
        }
        else if(type=='pa'){
          chartOptions.valueAxes = [
            {
              "id": "v1",
              "title": "TestQty",
              "position": "left",
              "integersOnly": true
            },
            {
              "id": "v2",
              "title": "Re-Burn Rate",
              "position": "right",
              "maximum":100,
              //"minimum":0,
              "labelFunction": function(value) {
                return value+"%";
              }
            }];
          chartOptions.graphs = [
            {
              "id":"testqty",
              "balloonFunction": chartBallonFunction.basic,
              "fillAlphas": 1,
              "title": "test qty",
              "type": "column",
              "valueField": "biqty",
              "valueAxis": "v1",
              "dashLengthField": "dashLengthColumn",
              "lineColor": "#118AB2",
              "labelText": "[[value]]",
              "showAllValueLabels":true
            },
            {
              "id": "rebirate",
              "balloonFunction": chartBallonFunction.percent,
              "bullet": "round",
              "lineThickness": 3,
              "bulletSize": 7,
              "bulletBorderAlpha": 1,
              "bulletColor": "#FFFFFF",
              "useLineColorForBulletBorder": true,
              "bulletBorderThickness": 3,
              "fillAlphas": 0,
              "lineAlpha": 1,
              "title": "re-burn rate",
              "valueField": "rebirate",
              "valueAxis": "v2",
              "dashLengthField": "dashLengthLine",
              "labelText": "[[value]]",
              "showAllValueLabels":true
            }];
        }
        else if(type=='yp'){
          chartOptions.valueAxes =  [ {
              "id": "v1",
              "title": "Yield",
              "position": "left",
              "maximum":100,
              //"minimum":0,
              "labelFunction": function(value) {
                return value+"%";
              }
            },
            {
              "id": "v2",
              "title": "TestQty",
              "position": "right",
              "integersOnly": true
            }];
          chartOptions.graphs = [
            {
              "id":"testqty",
              "balloonFunction": chartBallonFunction.basic,
              "fillAlphas": 1,
              "title": "test qty",
              "type": "column",
              "valueField": "fsttestqty",
              "valueAxis": "v2",
              "dashLengthField": "dashLengthColumn",
              "lineColor": "#118AB2",
              "labelText": "[[value]]",
              "showAllValueLabels":true
            },
            {
            "id": "finyield",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "lineColor":"#FFD166",
            "title": "final yield",
            "valueField": "finyield",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
           },
           {
            "id": "fstyield",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "lineColor":"#EF476F",
            "title": "first yield",
            "valueField": "fstyield",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          }];
        }
        return chartOptions;
      }
    }]//controller
		};
	};

	angular.module('mean.rebi').directive('shiftChart', shiftChart);
	shiftChart.$inject = ['Rebi','common','_'];
})();
'use strict';