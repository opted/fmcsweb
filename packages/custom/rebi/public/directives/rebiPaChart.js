
(function(){
	'use strict';
	function rebiPaChart(Rebi,common,_){
    var chartName = "rebi-pa-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=' // Data {data:rawdata,ftype:{rebi:selectedtype,yield:selectedtype}}
			},
			template: 
      // drilldown tab list
      '<div id="rebi-loadingchart" ng-style="loadingChartStyle" class="utils-loading-chart" is-loading="chartLoading" ng-if="chartLoading"></div>'+
      '<div ng-show="display">\
      <div class="pachart">\
      <uib-tabset active="drillDownTabActive">\
        <uib-tab ng-repeat="tab in drillTablist" ng-click="changeDrillDownTab($index)" index="tab.id">\
        <uib-tab-heading>\
          {{tab.title}}<button type="button" class="close" aria-label="Close" style="font-size:medium" ng-click="closeTab($index)" ng-show="$index>0">\
          <span aria-hidden="true">&times;</span>\
          </button>\
        </uib-tab-heading>\
        </uib-tab>\
      </uib-tabset>'+
      // tab title
      '<h3>{{ curChartParams.tabtitle }}</h3>'+
      // grouping button
      // render all pill tabset and hide unneeded tabset to fix a bug
      '<div ng-repeat="tab in drillTablist"><uib-tabset class="rebi-groupby" type="pills" ng-show="selectedTabItemIndex==$index">\
      <uib-tab index="$index" ng-repeat="item in tab.chartparams.groupby.itemlist" heading="{{GroupByMapping[item]}}" select="changeGroup(item)">\
      </uib-tab>\
      </uib-tabset></div>'+
     // sorting select
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">排序</label>\
        <select vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglist" ng-change="changeSorting(0)">\
        </select>\
        <div class="btn-group vcenter">\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="0" ng-click="changeSorting(1)">降序</label>\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="1" ng-click="changeSorting(2)">升序</label>\
        </div>\
      </div>'+
      // shift control
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">推移</label>\
        <input type="number" name="input" ng-model="shiftControl.number" class="form-control"></input>\
        <select class="form-control" ng-model="shiftControl.selected" ng-options="opt for opt in shiftControl.itemlist">\
        </select>\
      </div>'+
      // control hint
      '<label class="text-primary">* 單擊drill down, 雙擊查詢推移圖</label>'+
      // amchart
      '<div id="'+chartName+'" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      // formula ag-grid
      `
      <uib-accordion>
      <div uib-accordion-group class="panel-default" is-open="fgridParams.open">
        <uib-accordion-heading>
          公式驗證參考表<i class="pull-right glyphicon" ng-class="{'glyphicon-chevron-down': fgridParams.open, 'glyphicon-chevron-right': !fgridParams.open}"></i>
        </uib-accordion-heading>
       <div class="form-group row"> 
        <div style="height:250px;margin:10px;" class="ag-blue" ag-grid="fvAgGrid"></div>
      </div>
      </div>
      </uib-accordion>
      `+
      // shift chart
      '<shift-chart data="shiftChartData" type="pa" opts="shiftOpts"></shift-chart>'+
      '</div>'+ // div class=pachart
      // rawdata ag-grid
      '<raw-data-grid data="gridData" rebionly="true"></raw-data-grid></div>', // chart
			link: function (scope, element, attrs) {
        scope.display = false;
        /**
          Chart parameters, use to drilldown, sorting, limit or change group by
        */
        /*
          Chart params structure
          {
          level:,//drill down 等級，初始等級為0
          groupby:{itemlist:["LOTNO","ROOTLOT",...],selected:"LOTNO",active:0}, // 分群用項目
          sorting:{selected:'X軸字典排序',type:0},//可用的排序規則記在外部
          limit:{selected:'X軸字典排序',type:0}, //圖表呈現上限
          drillpath:"",
          rawdata:[],
          showdata:[]
          }
        */
        scope.curChartParams = undefined;
        scope.GroupByMapping = Rebi.paGroupByMapping();
        scope.changeGroup = function(item){
          Rebi.chartChangeGroup(scope,chart,item);
          // set shift chart data
          scope.shiftOpts = scope.curChartParams.shiftdata.opts;
          scope.shiftChartData = scope.curChartParams.shiftdata.data;
          scope.fvAgGrid.api.setRowData(scope.curChartParams.allshowdata);
        };
        // sorting selected
        scope.sortinglist = ['X軸字典排序','re-burn rate 數值排序','testqty 數值排序'];
        scope.changeSorting = function(changeItem){
          Rebi.chartChangeSorting(scope,chart,changeItem);
        };
        /**
          Drill down control
        */
        scope.drillDownTriggerChangeGroup=false;
        scope.drillTablist = [];
        scope.drillDown = function(graphItemCategory){
          Rebi.chartDrillDown(scope,chart,graphItemCategory);
        };
        scope.triggerCloseTab = false;
        scope.selectedTabItemIndex = 0;
        scope.changeDrillDownTab = function(index){
          Rebi.chartChangeDrillDownTab(scope,chart,index);
        };
        scope.closeTab = function(tabindex){
          Rebi.chartCloseTab(scope,tabindex);
        };
        /**
          Defined amchart options
        */
        var chartBallonFunction = common.amChartCustomizeBallonFunction();
        var chartOptions = {
          "type": "serial",
          "addClassNames": true,
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
                "useGraphSettings": true,
                "markerSize":12,
                "valueWidth":0,
                "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [
          {
            "id": "v1",
            "title": "TestQty",
            "position": "left",
            "integersOnly": true
          },
          {
            "id": "v2",
            "title": "Re-Burn Rate",
            "position": "right",
            "maximum":100,
            //"minimum":0,
            "labelFunction": function(value) {
              return value+"%";
            }
          }],
          "startDuration": 1,
          "graphs": [ 
          {
            "id":"testqty",
            "balloonFunction": chartBallonFunction.basic,
            "fillAlphas": 1,
            "title": "test qty",
            "type": "column",
            "valueField": "testqty",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthColumn",
            "lineColor": "#118AB2",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },
          {
            "id": "rebirate",
            "balloonFunction": chartBallonFunction.percent,
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "re-burn rate",
            "valueField": "rebirate",
            "valueAxis": "v2",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          } ],
          "categoryField": "key",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":10
          },
          "export": {
            "enabled": true
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
          "chartScrollbar": {
          }
        };

				var chart = AmCharts.makeChart(chartName, chartOptions);
        // 設定推移圖互動功能
        Rebi.rebiExtraSetChart(chart,scope);
        /**
          Whating the data link of outer controller
        */
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined){
            scope.display = false;
            return;
          }
          var rawdata = scope.data.data;
          var settledata = scope.data.settledata;
          var ftype = scope.data.ftype;
          var failbinlist = scope.data.failbinlist;// for type2 only
          scope.queryMatch = scope.data.match;
          if(_.isArray(rawdata)){
            if(rawdata.length>0){
              scope.display = true;
              scope.curChartParams = Rebi.initPaChartParams(rawdata,settledata,ftype,failbinlist);
              scope.drillTablist = [];
              scope.drillTablist.push(Rebi.getInitDrillTab(scope.curChartParams));
              chart.dataProvider = scope.curChartParams.showdata;
              chart.validateData();
              chart.validateNow();
              // handel ag-grid data
              var rowData = rawdata;
              scope.gridData = {data:rowData,ftype:ftype};
              setTimeout(function(){
                scope.drillDownTabActive = scope.drillTablist[0].id;
                scope.$apply();
              },100);
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
				chart.validateData();
				chart.validateNow();
			},//link
    controller:['$scope',function($scope){
      /**
        Formula Verification
      */
      var fvGridColumnDefs = [
        {headerName: 'KEY', field: 'key', filter: 'text', width: 150},
        {headerName: 'ReBI Rate(%)', field: 'rebirate', filter: 'text', width: 100},
        {headerName: 'ReBI Rate 分子', field: 'rebirate_n', filter:'number', width: 120},
        {headerName: 'ReBI Rate 分母', field: 'rebirate_d', filter:'number', width: 120},
      ];
      $scope.fvAgGrid = {
        columnDefs:fvGridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.fgridParams = {'open':false};
      /**
        Shift Chart control
      */
      $scope.shiftControl = Rebi.getShiftControl();
    }]
		};
	};

	angular.module('mean.rebi').directive('rebiPaChart', rebiPaChart);
	rebiPaChart.$inject = ['Rebi','common','_'];
})();
'use strict';