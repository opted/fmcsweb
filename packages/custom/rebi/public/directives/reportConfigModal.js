var reportOpenFile = undefined;
(function(){
	'use strict';
	function reportConfigModal($http,$uibModal,MeanUser){
		return {
			restrict: 'E',
			scope: {
        btnStyle: '@'
			},
			template: '<button type="button" class="btn btn-default" style="{{btnStyle}}" ng-click="nvConfigModal()">調整nV設定</button>', 
			link: function (scope, element, attrs) {
        
			},//link
    controller:['$scope',function($scope){
      $scope.nvConfigModal = function(){
        loadUserInfo();
        $http.get('/api/rebi/loadconfig').then(
          function(response){
            $scope.configText = response.data;
          },function(err){
            console.log(err);
        });
        $scope.modalInstance = $uibModal.open({
          templateUrl: 'rebi/views/nvConfigModal.html',
          size: 'md',
          scope: $scope
        });
      };
      $scope.uploadStatus = undefined;
      $scope.nvUploadHint = '\
        <ol>\
        <li>上傳csv檔編碼必須為UTF-8</li>\
        <li>必定要有Header且Header名稱必須正確，參考以下上傳範例</li>\
        <li><p>上傳csv檔格式範例如下:</p>\
        CustomerName,Family,Setting<br>\
        nVIDIA,T124AKV,Standard<br>\
        nVIDIA,GP106AK,Customize</li></ol>\
        '
      reportOpenFile = function(event) {
        var input = event.target;
        // filename = input.value
        if(!_.endsWith(input.value,'.csv')){
          $scope.$apply(function(){
            $scope.uploadStatus = '上傳失敗! 非CSV檔!';
          });
          return;
        }
        else{
          var reader = new FileReader();
          reader.onload = function(){
            var csvtext = reader.result.replace('\ufeff',''); // remove bom code
            $http.post('/api/rebi/uploadconfig',{'csvtext':csvtext}).then(
              function(response){
                $scope.uploadStatus = '上傳成功!';
                $scope.configText = csvtext;
                console.log('success!');
              },function(err){
                $scope.uploadStatus = '上傳失敗! '+err.data;
            });
          };
          reader.readAsText(input.files[0]);
        }
      };
      $scope.close = function(){
        $scope.uploadStatus = undefined;
        $scope.modalInstance.dismiss('cancel');
      }
      var loadUserInfo = function(){
        if(MeanUser.userInfo())
          $scope.username = MeanUser.userInfo().local.name;
        else
          $scope.username = undefined;
      };
    }]
		};
	};

	angular.module('mean.rebi').directive('reportConfigModal', reportConfigModal);
	reportConfigModal.$inject = ['$http','$uibModal','MeanUser'];
})();
'use strict';
