(function() {
    'use strict';
    function Rebi($http,$q,_,common) {
    var eaGroupByNameMapping = {'FAMILY':'ProductFamily','CUSTOMERCHISNAME':'客戶別','CUSTOMERLOTNOLOG':'客批',
    'DEVICENOLOG':'客戶型號','PACKAGETYPE':'封裝型號','OPNO':'站別',
    'OVENID':'爐號','ROOTLOT':'母批','BILEVEL':'BI次序'};
    var paGroupByNameMapping = {'FAMILY':'ProductFamily','CUSTOMERCHISNAME':'客戶別','CUSTOMERLOTNOLOG':'客批',
    'DEVICENOLOG':'客戶型號','PACKAGETYPE':'封裝型號','OPNO':'站別',
    'OVENID':'爐號','ROOTLOT':'母批','BILEVEL':'BI次序','MONTH':'月','WEEK':'週','DAY':'日'};
    var ypGroupByNameMapping = {'FAMILY':'ProductFamily','CUSTOMERLOTNOLOG':'客批','DEVICENOLOG':'客戶型號','PACKAGETYPE':'封裝型號','OPNO':'站別','MONTH':'月','WEEK':'週','DAY':'日'};
    var eaBasicGroupByList = _.keys(eaGroupByNameMapping);
    var paBasicGroupByList = _.keys(paGroupByNameMapping);
    var ypBasicGroupByList = _.keys(ypGroupByNameMapping);
    // only ea(工程分析) need yield data
    var calYield = function(opno,groupData){
      var acc_gqty = 0;
      var acc_tqty = 0;
      var regopno=undefined;
      if(_.endsWith(opno,'*')) // ex: BURNIN*
        regopno=opno.replace('*','');
      var fdoc = undefined;
      if(regopno){
        fdoc = _.filter(groupData,function(o){
          return _.startsWith(o.OPNO,regopno);
        });
      }
      else
        fdoc = _.filter(groupData,{'OPNO':opno});

      var failbinqty = _.sumBy(fdoc,'FAILBINQTY'); // for type 2
      var goodqty = _.sumBy(fdoc,'GOODQTY');
      var inputqty = _.sumBy(fdoc,'INPUTQTY');
      if(failbinqty)
        goodqty=inputqty-failbinqty;
      acc_gqty+=goodqty;
      acc_tqty+=inputqty;
      return {'passqty':acc_gqty,'testqty':acc_tqty};
    };
    return {
      calYield:function(opno,groupData){
        return calYield(opno,groupData);
      },
      eaGroupByMapping: function(){
        return eaGroupByNameMapping;
      },
      paGroupByMapping: function(){
        return paGroupByNameMapping;
      },
      ypGroupByMapping: function(){
        return ypGroupByNameMapping;
      },
      rebiCalculateShowData: function(chartParams){
        var rawdata = chartParams.data.rawdata;
        var settledata = chartParams.data.settledata;
        var selected = chartParams.groupby.selected;
        var formularType = chartParams.ftype;
        var ctype = chartParams.ctype;
        var failBinFamilyMap = _.groupBy(chartParams.failbinlist,'family');

        var isSettle = false;
        if(selected=='FAMILY' || selected=='DEVICENOLOG' || selected=='PACKAGETYPE' || selected=='CUSTOMERCHISNAME'|| selected=='CUSTOMERLOTNOLOG' || selected=='ROOTLOT') {
          //To calculate type 2 yield for some group, we need to calculate by settle data
          isSettle=true;
        }

        // 1. group by selected
        var groupMap = _.groupBy(rawdata,selected);
        var settleGroupMap = _.groupBy(settledata,selected);
        // 2. calculate by selected
        var chartData = [];
        for(var key in groupMap){
          var groupData = groupMap[key];
          var sdata = {key:key};
          // 2.1 get rebi-rate
          if(ctype=='ea' || ctype=='pa'){
            if(formularType.rebi==1){
              // Type 1: (bilv 3 + bilv2 TESTQTY)/(bilv 1 TESTQTY)
              // p.s. while OPNO='BURNIN1'
              var fdoc = _.filter(groupData,{'BILEVEL':1,'inrange':true});
              var bilv1_testqty = _.sumBy(fdoc,'INPUTQTY');
              fdoc = _.filter(groupData,function(o){return (o.BILEVEL>1 && o.inrange)});
              var bilv23_testqty =_.sumBy(fdoc,'INPUTQTY');
              sdata['rebirate_n']=bilv23_testqty;
              sdata['rebirate_d']=bilv1_testqty;
              sdata['rebirate']=bilv23_testqty/bilv1_testqty;
            }
            else if(formularType.rebi==2){
              // Type 2: (BURNIN1 FAILQTY by config(if has config))/(BURNIN1 TESTQTY)
              // nV 的算法需要MES TBLWIPBINCONTENT BINQTY 資料
              // only type 2 using fail bin list
              // 只算 BILEVEL = 1 的站點資料
              var fdoc = _.filter(groupData,{'OPNO':'BURNIN1','BILEVEL':1,'inrange':true});
              var failqty = _.sumBy(fdoc,function(o){
                var failbinmap = failBinFamilyMap[o.FAMILY];
                var total=0;
                if(failbinmap){
                  var failbinlist = failbinmap[0].list;
                  for(var key in failbinlist)
                    if(failbinlist[key])
                      total+=o['BIN'+key]
                }
                else
                  total=o.FAILQTY;
                return total;
              });//_.sumBy(fdoc,'FAILQTY');
              var testqty = _.sumBy(fdoc,'INPUTQTY');
              sdata['rebirate_n']=failqty;
              sdata['rebirate_d']=testqty;
              sdata['rebirate']=failqty/testqty;
            }
            else if(formularType.rebi==3){
              // Type 3: (BURNIN1 FAILQTY)/(BURNIN1 TESTQTY)
              // not need to in range
              var bi1_fqty = 0;
              var bi1_tqty = 0;
              var fdoc = _.filter(groupData,{'OPNO':'BURNIN1'});
              var failqty = _.sumBy(fdoc,'FAILQTY');
              var inputqty = _.sumBy(fdoc,'INPUTQTY');
              bi1_fqty+=failqty;
              bi1_tqty+=inputqty;
              sdata['rebirate_n']=bi1_fqty;
              sdata['rebirate_d']=bi1_tqty;
              sdata['rebirate']=bi1_fqty/bi1_tqty;
            }
            else if(formularType.rebi==4){
              // Type 4: (TESTQTY of bilv>1)/(TESTQTY of all level)
              var fdoc = _.filter(groupData,{'BILEVEL':1,'inrange':true});
              var bilv1_testqty = _.sumBy(fdoc,'INPUTQTY');
              fdoc = _.filter(groupData,function(o){return (o.BILEVEL>1 && o.inrange)});
              var bilv23_testqty =_.sumBy(fdoc,'INPUTQTY');
              sdata['rebirate_n']=bilv23_testqty;
              sdata['rebirate_d']=(bilv1_testqty+bilv23_testqty);
              sdata['rebirate']=bilv23_testqty/(bilv1_testqty+bilv23_testqty);
            }
          }
          // 2.2 get first yield & final yield
          if(ctype=='ea' || ctype=='yp'){ // only engineering analysis need yield data
            if(formularType.yield==1){
              // Type 1: 
              // first yield = (BURNIN1 GOODQTY)/(BURNIN1 TESTQTY)
              var caly_data = calYield('BURNIN1',groupData);
              sdata['fstyield_n']=caly_data['passqty'];
              sdata['fstyield_d']=caly_data['testqty'];
              sdata['fstyield']=caly_data['passqty']/caly_data['testqty'];
              // final yield = (MERGE-LOT-CHECK1 GOODQTY)/(MERGE-LOT-CHECK1 TESTQTY)
              caly_data=calYield('MERGE LOT CHECK1',groupData);
              sdata['finyield_n']=caly_data['passqty'];
              sdata['finyield_d']=caly_data['testqty'];
              sdata['finyield']=caly_data['passqty']/caly_data['testqty'];
            }
            else if(formularType.yield==2){
              // Type 2:
              // first yield = FirstPassQty/DieQty
              // final yield = PassQty/DieQty
              var fdoc = [];
              if(isSettle)
                fdoc = settleGroupMap[key];
              else{
                // remove nosplitmft data for type 2
                _.remove(groupData,{nosplitmft:true});
                fdoc = _.filter(groupData,function(o){return _.startsWith(o.OPNO,'BURNIN')});
              }
              var fpassqty = _.sumBy(fdoc,'FIRSTPASSQTY');
              var passqty = _.sumBy(fdoc,'PASSQTY');
              var testqty = _.sumBy(fdoc,'TESTQTY');
              var dieqty = _.sumBy(fdoc,'DIEQTY');
              sdata['fstyield_n']=fpassqty;
              sdata['fstyield_d']=dieqty;
              sdata['fstyield']=fpassqty/dieqty;
              sdata['finyield_n']=passqty;
              sdata['finyield_d']=dieqty;
              sdata['finyield']=passqty/dieqty;
              sdata['testqty']=testqty;
            }
            else if(formularType.yield==3){
              // Type 3:
              // first yield = (BURNIN1 GOODQTY)/(BURNIN1 TESTQTY)
              var caly_data = calYield('BURNIN1',groupData);
              sdata['fstyield_n']=caly_data['passqty'];
              sdata['fstyield_d']=caly_data['testqty'];
              sdata['fstyield']=caly_data['passqty']/caly_data['testqty'];
              // final yield = (Split-MERGE LOT CHECK1 GOODQTY)/(Split-MERGE LOT CHECK1 TESTQTY)
              caly_data=calYield('Split-MERGE LOT CHECK1',groupData);
              sdata['finyield_n']=caly_data['passqty'];
              sdata['finyield_d']=caly_data['testqty'];
              sdata['finyield']=caly_data['passqty']/caly_data['testqty'];
            }
            else if(formularType.yield==4){
              // Type 4:
              // final yield = first yield
              var caly_data = calYield('BURNIN*',groupData);
              sdata['finyield_n']=sdata['fstyield_n']=caly_data['passqty'];
              sdata['finyield_d']=sdata['fstyield_d']=caly_data['testqty'];
              sdata['finyield']=sdata['fstyield']=caly_data['passqty']/caly_data['testqty'];
            }
          }
          if(ctype=='yp'){ // firstyield testqty
            if(formularType.yield==4)
             sdata['testqty']=_.sumBy(_.filter(groupData,function(o){return _.startsWith(o.OPNO,'BURNIN')}),'INPUTQTY');
            else if(formularType.yield==2){
              var fdoc = [];
              if(isSettle)
                fdoc = settleGroupMap[key];
              else
                fdoc = _.filter(groupData,function(o){return _.startsWith(o.OPNO,'BURNIN')});
              sdata['testqty']=_.sumBy(fdoc,'TESTQTY');
            }
            else{
             sdata['testqty']=_.sumBy(_.filter(groupData,{OPNO:'BURNIN1'}),'INPUTQTY');
            }
          }
          else if(ctype=='pa'){ // rebi testqty
            // need in range
            if(formularType.yield==4)
             sdata['testqty']=_.sumBy(_.filter(groupData,function(o){return (_.startsWith(o.OPNO,'BURNIN') && o.inrange)}),'INPUTQTY');
            else if(formularType.yield==2){
              var fdoc = _.filter(groupData,{OPNO:'BURNIN1',inrange:true});
              sdata['testqty']=_.sumBy(fdoc,'INPUTQTY');
            }
            else{
             if(formularType.yield==3)
              sdata['testqty']=_.sumBy(_.filter(groupData,{OPNO:'BURNIN1'}),'INPUTQTY');
             else if(formularType.yield==1)
              sdata['testqty']=_.sumBy(_.filter(groupData,{'BILEVEL':1,'inrange':true}),'INPUTQTY');
            }
          }
          for(var key in sdata)
            if(key=='rebirate' || key=='fstyield' || key=='finyield')
              sdata[key]=_.round((sdata[key]*100),2);
          // calculate gap
          if(ctype=='yp')
            sdata['gap']=_.round(sdata['finyield']-sdata['fstyield'],2);
          chartData.push(sdata);
        }
        chartParams.allshowdata = chartData;
        chartParams.allshowdata = this.sortingAllShowData(chartParams);
        var limitselected = chartParams.limit.selected;
        var newChartData = [];
        // handle limit item part
        if(limitselected=='all' || limitselected>=chartParams.allshowdata.length)
          newChartData = chartParams.allshowdata;
        else
          for(var i=0;i<limitselected;i++)
            newChartData.push(chartParams.allshowdata[i]);
        return newChartData;
      },
      initBasicChartParams:function(level,rawdata,groupbylist,chartType,extraParams){
        var chartParams = {
         level:level,//drill down 等級，初始等級為0
         groupby:{itemlist:groupbylist,selected:groupbylist[0],active:0}, // 分群用項目
         sorting:{selected:'X軸字典排序',type:1},//可用的排序規則記在外部
         limit:{selected:'all'}, //圖表X軸分類顯示上限
         drillpath:[],
         data:rawdata,
         showdata:[],
         allshowdata:[],
         tmpgroupresult:{}, // 為增加效能，暫存不同 group by 結果的 showdata, sorting and limit
         ctype:chartType
        };
        for(var key in extraParams)
          chartParams[key]=extraParams[key];
        return chartParams;
      },
      getChartParams: function(level,rawdata,settledata,ftype,groupbylist,failbinlist,chartType){
        // Iinitial status was defined as following
        var extraParams = {
         ftype: ftype, // Formular type
         data:{rawdata:rawdata,settledata:settledata},
         failbinlist:failbinlist, // selected fail bin (for type 2 formula only)
         shiftdata:{data:[],opts:{}} // for 推移圖
        };
        var chartParams = this.initBasicChartParams(level,rawdata,groupbylist,chartType,extraParams);
        chartParams.showdata = this.rebiCalculateShowData(chartParams);
        chartParams.tmpgroupresult[chartParams.groupby.selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,
          sorting:chartParams.sorting,limit:chartParams.limit,shiftdata:chartParams.shiftdata};
        return chartParams;
      },
      initEaChartParams: function(rawdata,settledata,ftype,failbinlist){
        return this.getChartParams(0,rawdata,settledata,ftype,eaBasicGroupByList,failbinlist,'ea');
      },
      initPaChartParams: function(rawdata,settledata,ftype,failbinlist){
        return this.getChartParams(0,rawdata,settledata,ftype,paBasicGroupByList,failbinlist,'pa');
      },
      initYpChartParams: function(rawdata,settledata,ftype){
        return this.getChartParams(0,rawdata,settledata,ftype,ypBasicGroupByList,undefined,'yp');
      },
      getDrillDownParams: function(chartParams,category){
        var key = _.keys(category)[0];
        // 1. get new rawdata
        var rawdata = _.filter(chartParams.data.rawdata,function(o){
          if(o[key]==category[key])
            return true;
          else
            false;
        });
        // 2. get new settledata
        // drill down by 較低層級資料
        var settledata=[];
        if(key=='OPNO' || key=='DAY' || key=='WEEK' || key=='MONTH' || key=='LOTNO' || key=='BILEVEL')
          settledata = rawdata;
        else
          settledata = _.filter(chartParams.data.settledata,function(o){
            if(o[key]==category[key])
              return true;
            else
              false;
          });
        // 3. get new groupbylist
        var groupbylist = _.filter(chartParams.groupby.itemlist,function(o){return o!=key})
        var ddChartParams = this.getChartParams(chartParams.level+1,rawdata,settledata,chartParams.ftype,groupbylist,chartParams.failbinlist,chartParams.ctype);
        // 4. set drillpath
        ddChartParams.drillpath = _.cloneDeep(chartParams.drillpath);
        ddChartParams.drillpath.push(category);
        return ddChartParams;
      },
      changeGroup: function(chartParams,selected){
        chartParams.groupby.selected = selected;
        if(chartParams.tmpgroupresult[selected]){ //defined
          var tmpresult = chartParams.tmpgroupresult[selected];
          chartParams.showdata = tmpresult.showdata;
          chartParams.allshowdata = tmpresult.allshowdata;
          chartParams.sorting = tmpresult.sorting;
          chartParams.limit = tmpresult.limit;
          chartParams.shiftdata = tmpresult.shiftdata;
        }
        else{ // undefined
          chartParams.showdata = this.rebiCalculateShowData(chartParams);
          chartParams.sorting = _.cloneDeep(chartParams.sorting);
          chartParams.limit = _.cloneDeep(chartParams.limit);
          chartParams.shiftdata = _.cloneDeep({data:[],opts:{}});
          // 暫存 groupby 資料, 這部分資料在切換 groupby, limit, sorting 時要確認是否確實置換
          chartParams.tmpgroupresult[selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit,shiftdata:chartParams.shiftdata};
        }
        return chartParams;
      },
      changeLimit: function(chartParams){
        var selected = chartParams.limit.selected;
        if(selected=='all' || selected>=chartParams.allshowdata.length)
          chartParams.showdata = chartParams.allshowdata;
        else{
          chartParams.showdata = [];
          for(var i=0;i<selected;i++)
            chartParams.showdata.push(chartParams.allshowdata[i]);
        }
        chartParams.tmpgroupresult[chartParams.groupby.selected].showdata = chartParams.showdata;
        return chartParams;
      },
      sortingAllShowData: function(chartParams){
        var selected = chartParams.sorting.selected;
        var type = chartParams.sorting.type;
        var order = 'desc';
        if(type==0)
          order='desc';
        else
          order='asc';
        var sortingField = '';
        // sorting allshowdata
        if(chartParams.sorting.field){
          sortingField = chartParams.sorting.field;
        }
        else{
          if(selected.indexOf('X軸')>=0)
            sortingField = 'key';
          else if(selected.indexOf('re-burn rate')>=0)
            sortingField = 'rebirate';
          else if(selected.indexOf('testqty')>=0)
            sortingField = 'testqty';
          else if(selected.indexOf('first yield')>=0)
            sortingField = 'fstyield';
          else if(selected.indexOf('final yield')>=0)
            sortingField = 'finyield';
        }
        // 針對 MBI 的爐號命名標準定義的排序，EX:升序排列時，OG29要排在OG1001前，普通的字典排序無法達成此結果。
        // 混合數字的字串都可以使用此方法來排序
        if(sortingField=='key' && chartParams.groupby.selected=='oven_ID'){
          console.log('start common sorting!');
          chartParams.allshowdata = common.sortingBy(chartParams.allshowdata,sortingField,order);
        }
        else{
          chartParams.allshowdata = _.orderBy(chartParams.allshowdata,function(o){
            if(_.isNaN(o[sortingField]) || _.isUndefined(o[sortingField]) || _.isNull(o[sortingField]))
              return 0;
            else
              return o[sortingField];
          },[order]);
        }
        return chartParams.allshowdata;
      },
      changeSorting: function(chartParams){
        chartParams.allshowdata = this.sortingAllShowData(chartParams);
        chartParams.tmpgroupresult[chartParams.groupby.selected].allshowdata = chartParams.allshowdata;
        return this.changeLimit(chartParams);
      },
      chartChangeGroup: function(scope,chart,item){
        if(scope.drillDownTriggerChangeGroup){
          // drill down cilck will make new groupby tabset and trigger this function, 
          // do not trigger at this time.
          scope.drillDownTriggerChangeGroup = false;
          return;
        }
        scope.curChartParams = this.changeGroup(scope.curChartParams,item);
        chart.dataProvider = scope.curChartParams.showdata;
        chart.validateData();
        chart.validateNow();
      },
      chartChangeLimit: function(scope,chart){
        scope.curChartParams = this.changeLimit(scope.curChartParams);
        chart.dataProvider = scope.curChartParams.showdata;
        chart.validateData();
        chart.validateNow();
      },
      chartChangeSorting: function(scope,chart,changeItem){
        scope.curChartParams = this.changeSorting(scope.curChartParams);
        chart.dataProvider = scope.curChartParams.showdata;
        chart.validateData();
        chart.validateNow();
      },
      setDrillDownTab:function(scope,newChartParams){
        var drillpathText = '';
        newChartParams.drillpath.forEach(function(o){
          for(var dpkey in o)
            drillpathText+='['+scope.GroupByMapping[dpkey]+'] '+o[dpkey]+' ';
        });
        
        var title = drillpathText;//'Drill from '+scope.GroupByMapping[scope.curChartParams.groupby.selected]+': '+graphItemCategory;
        newChartParams.tabtitle = title;
        scope.drillTablist.push({title:title,chartparams:newChartParams,id:new Date().getTime()});
      },
      chartDrillDown: function(scope,chart,graphItemCategory){
        if(scope.curChartParams.groupby.itemlist.length<=1){
          alert("Dose not has any grouping for drilling down.");
          return;
        }
        // get new chart params
        var category = {};
        category[scope.curChartParams.groupby.selected] = graphItemCategory;
        var newChartParams = this.getDrillDownParams(scope.curChartParams,category);
        this.setDrillDownTab(scope,newChartParams);
        this.chartDrillDownAutoChangeTab(scope);
      },
      chartChangeDrillDownTab: function(scope,chart,index){
        if(scope.triggerCloseTab){
          return;
        }
        // selectedTabItemIndex may different with drillDownTabActive but always same as index of selected itme in drillTablist
        scope.selectedTabItemIndex=index;
        // renew chart
        scope.curChartParams = scope.drillTablist[index].chartparams;
        chart.dataProvider = scope.curChartParams.showdata;
        scope.changeGroup(scope.curChartParams.groupby.selected);
      },
      chartCloseTab: function(scope,tabindex){
        if(tabindex<=0)
          return;
        scope.triggerCloseTab = true; // click close tab button will trigger closeTab function first, and then changeDrillDownTab
        setTimeout(function(){ // don't set time out will route to home page...tricky bug
          scope.drillTablist.splice(tabindex,1);
          if(scope.drillTablist[0].id)
            scope.drillDownTabActive = scope.drillTablist[0].id;
          else
            scope.drillDownTabActive=0; // tab active not work, it will auto change 
          scope.triggerCloseTab=false;
          var new_tab_index = 0;
          // has been fixed at 2017/11/20 by changed version ui-bootstrap
          /*if(tabindex>=scope.drillTablist.length) //last one
            new_tab_index=tabindex-1;
          else
            new_tab_index=tabindex+1;*/
          scope.changeDrillDownTab(new_tab_index);
          scope.$apply();
        },250);
      },
      genShiftMapChart: function(scope,clickItem,match,shift){
        var deferred = $q.defer();
        var promise = deferred.promise;
        var toDate = match.DATETIME.TO, fromDate = common.shiftDate(match.DATETIME.TO,shift.number,shift.datetype);
        delete match.DATETIME;
        var params = {match:{},
          DATETIME:{fromDate:fromDate,toDate:toDate,datetype:shift.datetype},
          failbinmapfamily:_.groupBy(scope.curChartParams.failbinlist,'family'),
          ftype:scope.curChartParams.ftype};
        // set date time
        params.match = _.assign(params.match,match);
        // remove undefined value
        for(var key in params.match){
          if(params.match[key]==undefined)
            delete params.match[key];
        }
        // assign category
        params.match[scope.curChartParams.groupby.selected]=clickItem;
        _.forEach(scope.curChartParams.drillpath,function(o){
          for(var key in o){
            params.match[key] = o[key];
          }
        });
        // for testing
        /*setTimeout(function(){
          var testdata = [{"date":"2017-12-11","fsttestqty":8841,"fstpassqty":8742,"fintestqty":7227,"finpassqty":7227,"biqty":8841,"rebiqty":0,"rebirate":0,"fstyield":98.88,"finyield":100},
            {"date":"2017-12-10","fsttestqty":11122,"fstpassqty":11036,"fintestqty":11367,"finpassqty":11367,"biqty":11122,"rebiqty":0,"rebirate":0,"fstyield":99.23,"finyield":100},
            {"date":"2017-12-02","fsttestqty":11368,"fstpassqty":11095,"fintestqty":16026,"finpassqty":16026,"biqty":11368,"rebiqty":0,"rebirate":0,"fstyield":97.6,"finyield":100}];
          var result = {
            data: testdata,
            opts: {match:params.match}
          };
          deferred.resolve(result);
        },3000);*/
        if(params.match.DAY || params.match.WEEK || params.match.MONTH){
          deferred.reject(' 無法在 drill down from 日、週、月 時間維度的條件下產生推移圖!');
          return promise;
        }
        if(params.match.OVENID){
          params.match['EQUIPMENTNO'] = params.match.OVENID;
          delete params.match.OVENID;
        }
        $http.post('/api/rebi/shiftmap',params).then(function(response){
          var result = {
            data: response.data,
            opts: {match:params.match}
          };
          deferred.resolve(result);
        },function(err){
          console.log(err);
          deferred.reject(err);
        });
        return promise;
      },
      setZoomChart: function(chart,number){
        number=number?number:20;
        var zoomChart = function(){
          try{
            chart.zoomToIndexes(0,number);
          }
          catch(e){} // data size less than number
        }
        chart.addListener("dataUpdated", zoomChart);
      },
      rebiExtraSetChart: function(chart,scope,chartType){
        this.setZoomChart(chart);
        var genShiftMapChart = this.genShiftMapChart;
        common.setAmChartClickHandler(chart,{
          singleClick:{confirmText:'Drill down from ',clickFunction:function(event){
            scope.drillDownTriggerChangeGroup = true;
            scope.drillDown(event.item.category);
          }},
          doubleClick:{confirmText:'產生推移圖 by ',clickFunction:function(event){
            var selectedGroup = scope.curChartParams.groupby.selected;
            if(selectedGroup=='BILEVEL' || selectedGroup=='DAY' || selectedGroup=='WEEK' || selectedGroup=='MONTH')
              return alert('此分類不支援推移圖');
            var shiftParams = {
              datetype:scope.shiftControl.typeMap[scope.shiftControl.selected],
              number:scope.shiftControl.number
              };
            // loading data
            scope.loadingChartStyle = {height:document.getElementsByClassName(scope.curChartParams.ctype+'chart')[0].offsetHeight+'px'}
            scope.chartLoading = true;
            // using apply then watchGroup function can find out that variable has change
            setTimeout(function(){scope.$apply();},100);
            var removeShiftChartData = function(){
              scope.curChartParams.shiftdata.data = [];
              var selected = scope.curChartParams.groupby.selected;
              if(scope.curChartParams.tmpgroupresult[selected])
                scope.curChartParams.tmpgroupresult[selected].shiftdata.data = [];
            };
            genShiftMapChart(scope,event.item.category,_.cloneDeep(scope.queryMatch),shiftParams).then(function(result){
              scope.shiftOpts = scope.curChartParams.shiftdata.opts = {match:result.opts.match,removeData:removeShiftChartData};
              scope.shiftChartData = scope.curChartParams.shiftdata.data = result.data;
              var selected = scope.curChartParams.groupby.selected;
              if(scope.curChartParams.tmpgroupresult[selected])
                scope.curChartParams.tmpgroupresult[selected].shiftdata.data = result.data;
              scope.chartLoading = false;
            },function(err){
              alert('query error!'+err); 
              scope.chartLoading = false;});
          }},
        });
      },
      getInitDrillTab: function(curChartParams){
        return {title:"Overall",chartparams:curChartParams,id:new Date().getTime()};
      },
      getShiftControl: function(){
        return {selected:'日',itemlist:['日','週','月'],number:12,typeMap:{'日':'day','週':'week','月':'month'}};
      },
      chartDrillDownAutoChangeTab(scope){
        scope.changeDrillDownTab(scope.drillTablist.length-1);
        scope.$apply();
        if(scope.drillTablist[scope.drillTablist.length-1].id)
          scope.drillDownTabActive = scope.drillTablist[scope.drillTablist.length-1].id;
      }
    };
  }
  angular
      .module('mean.rebi')
      .factory('Rebi', Rebi);
  Rebi.$inject = ['$http','$q','_','common'];
})();
