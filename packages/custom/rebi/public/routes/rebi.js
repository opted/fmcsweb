(function() {
    'use strict';

    function Rebi($stateProvider) {
        $stateProvider.state('rebi main page', {
            url: '/rebi',
            templateUrl: 'rebi/views/index.html',
            controller:RebiController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              },
              checkInitalParams:function(){
                return {inittab:'ea'};
              }
            }
        })
        .state('rebi yreport page', {
            url: '/rebi/yieldreport',
            templateUrl: 'rebi/views/index.html',
            controller:RebiController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              },
              checkInitalParams:function(){
                return {inittab:'yieldreport'};
              }
            }
        });
    }

    angular
        .module('mean.rebi')
        .config(Rebi);

    Rebi.$inject = ['$stateProvider'];

})();
