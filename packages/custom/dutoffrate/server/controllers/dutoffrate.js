
var _ = require('lodash');
var mongoose = require('mongoose');
var Q = require('q');
var DORDailyReport = mongoose.model('DORDailyReport');
var moment = require('moment');

function addHours(queryDateTime){
  var tempDate = new Date(queryDateTime)
  tempDate.setHours(tempDate.getHours()+8)
  return moment(tempDate).format("YYYY-MM-DD HH:mm");
}

class DutoffrateManager {
  constructor(utils) {
    this.coll = DORDailyReport;
    this.utils = utils;
    this.pathMapping = {
      // 'CUSTOMERLOTNO': 'CUSTOMERLOTNO',
      // 'SITENO': 'SITENO',
      'CUSTNAME': 'CUSTNAME',
      'ASSSERIALNO': 'ASSSERIALNO',
      'NEW_ASSSERIALNO': 'NEW_ASSSERIALNO',
      'BOARDNO':'BOARDNO'
    };
    this.arrayFields = [];
  }

  getSearchResult (params) {
    var df = Q.defer();
    try {
        var query = params;
        var match = {}
      
        var dateRange = {'DATETIME': {}};
        if (query.from !== null){
          dateRange.DATETIME['$gte'] = new Date(query.from);
        }
        if (query.to !== null){
          dateRange.DATETIME['$lt'] = new Date(query.to);
        }

        if (!_.isEmpty(dateRange.DATETIME)){
          _.assign(match, dateRange);
        }
        // console.log("@@@@@@@@@@  ", query)
        _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, [ 'CUSTNAME', 'NEW_ASSSERIALNO', 'ASSSERIALNO', 'BOARDNO']), this.pathMapping));

        var pipe = [{'$match': match}];
        
        // if(('FAMILYS' in query)){
        	// pipe.push({'$unwind': "$FAMILYS"});
        	// pipe.push({'$match': this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['FAMILYS']), this.pathMapping)});
        // }
        

        pipe.push({'$project': {
            '_id': 0,
            'ASSSERIALNO': 1,
            'NEW_ASSSERIALNO': 1, 
            'BOARDNO': 1, 
            'CUSTNAME': 1, 
            'TOTAL_SOCKET': 1, 
            'TOTAL_USED_SOCKET': 1, 
            'TOTAL_BIB': 1, 
            'DUTQTY': 1, 
            'PACKAGETYPE': 1, 
            'DUTONRATE': { "$multiply": [ "$DUTONRATE", 100 ] },
            // { 
              
            //       "$concat": [ { "$substr": [ { "$multiply": [ "$DUTONRATE", 100 ] }, 0,2 ] }, "", "%" ]
            // },
            // 'DUTOFFRATE': '$DUTOFFRATE',
            'DUTOFFRATE': { "$multiply": [ "$DUTOFFRATE", 100 ] },
            // { 
            //       "$concat": [ { "$substr": [ { "$multiply": [ "$DUTOFFRATE", 100 ] }, 0,2 ] }, "", "%" ]
            // },
			      'SMAP_P': { $add:  ['$SMAP_CAL.P','$SMAP_CAL.E'] }, 
            'SMAP_X': '$SMAP_CAL.X', 
            'SMAP_D': '$SMAP_CAL.D', 
            'SMAP_T': '$SMAP_CAL.T',
            'SMAP_O': '$SMAP_CAL.O',
            'SMAP_D_T': { $add:  ['$SMAP_CAL.D', '$SMAP_CAL.T'] },
            'SMAP_E': '$SMAP_CAL.E', 
            'BIB_STATUS_0': '$BIB_STATUS.0', 
            'BIB_STATUS_1': '$BIB_STATUS.1', 
            'BIB_STATUS_2': '$BIB_STATUS.2',  
            'BIB_STATUS_3': '$BIB_STATUS.3',  
            'BIB_STATUS_4': '$BIB_STATUS.4',  
            'BIB_STATUS_5': '$BIB_STATUS.5',  
            'BIB_STATUS_6': '$BIB_STATUS.6',  
            'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME"}}
            // 'FAMILYS': 1,
        }});
        pipe.push({'$sort': {"DATETIME": 1}});
      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(DORDailyReport, pipe)
      .then(function(result){
        try {

            var countt = 0;
            _.forEach(result,function(r){
              r.DATETIME = addHours(r.DATETIME);
            })
            df.resolve({'data': result});
        } catch (e) {
            df.reject(e.stack);
            console.log(e)
        }
      });
    } catch (err){
      df.reject(err.stack);
      console.log(err)
    }

    return df.promise;
  };

  getBibSearchResult (params) {
    var df = Q.defer();
    try {
        var query = params;
        var match = {}
      
        var dateRange = {'DATETIME': {}};
        if (query.from !== null){
          dateRange.DATETIME['$gte'] = new Date(query.from);
        }
        if (query.to !== null){
          dateRange.DATETIME['$lt'] = new Date(query.to);
        }

        if (!_.isEmpty(dateRange.DATETIME)){
          _.assign(match, dateRange);
        }
        // console.log("@@@@@@@@@@  ", query)
        _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, [ 'CUSTNAME', 'NEW_ASSSERIALNO', 'ASSSERIALNO', 'BOARDNO']), this.pathMapping));

        var pipe = [{'$match': match}];
        

        
        var groupObject = {'$group':
                {
                    '_id': 
                        { 
                          // '$week':'$DATETIME',
                            'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }},
                            'NEW_ASSSERIALNO':'$NEW_ASSSERIALNO'
                        },
                    'TOTAL_USED_SOCKET':
                        {
                            '$sum':"$TOTAL_USED_SOCKET"
                        },
                    'BOARDNO':
                        {
                          '$first':"$BOARDNO"
                        },
                    'CUSTNAME':
                        {
                          '$first':"$CUSTNAME"
                        },
                    'TOTAL_SOCKET':
                        {
                            '$sum':'$TOTAL_SOCKET'
                        },
                    'TOTAL_USED_SOCKET':
                        {
                            '$sum':'$TOTAL_USED_SOCKET'
                        },
                    'TOTAL_BIB':
                        {
                            '$sum':'$TOTAL_BIB'
                        }, 
                    'DUTQTY':
                        {
                            '$sum':'$DUTQTY'
                        },
                    'PACKAGETYPE':
                        {
                          '$first':"$PACKAGETYPE"
                        },
                    // 'DUTONRATE':
                    //     {
                    //         '$sum':{ $add:  ['$SMAP_CAL.P','$SMAP_CAL.E'] }
                    //     },
                    // 'DUTOFFRATE':
                    //     {
                    //         '$sum':{ $add:  ['$SMAP_CAL.P','$SMAP_CAL.E'] }
                    //     },   
                    'SMAP_P':
                        {
                            '$sum':{ $add:  ['$SMAP_CAL.P','$SMAP_CAL.E'] }
                        },
                    'SMAP_X':
                        {
                            '$sum':'$SMAP_CAL.X'
                        },
                    'SMAP_T':
                        {
                            '$sum':'$SMAP_CAL.T'
                        },
                    'SMAP_O':
                        {
                            '$sum':'$SMAP_CAL.O'
                        }, 
                    'SMAP_D_T':
                        {
                            '$sum':{ $add:  ['$SMAP_CAL.D','$SMAP_CAL.T'] }
                        },
                    'SMAP_E':
                        {
                            '$sum':'$SMAP_CAL.E'
                        },
                    'BIB_STATUS_0':
                        {
                            '$sum':'$BIB_STATUS.0'
                        },
                    'BIB_STATUS_1':
                        {
                            '$sum':'$BIB_STATUS.1'
                        }, 
                    'BIB_STATUS_2':
                        {
                            '$sum':'$BIB_STATUS.2'
                        },
                    'BIB_STATUS_3':
                        {
                            '$sum':'$BIB_STATUS.3'
                        },
                    'BIB_STATUS_4':
                        {
                            '$sum':'$BIB_STATUS.4'
                        },
                    'BIB_STATUS_5':
                        {
                            '$sum':'$BIB_STATUS.5'
                        }, 
                    'BIB_STATUS_6':
                        {
                            '$sum':'$BIB_STATUS.6'
                        }, 

                }
            };


        pipe.push(groupObject);

        
        pipe.push({'$project': {
            '_id': "$_id",
            'NEW_ASSSERIALNO': '$_id.NEW_ASSSERIALNO',
            'BOARDNO': '$BOARDNO', 
            'CUSTNAME': '$CUSTNAME', 
            'TOTAL_SOCKET': '$TOTAL_SOCKET', 
            'TOTAL_USED_SOCKET': '$TOTAL_USED_SOCKET', 
            'TOTAL_BIB': '$TOTAL_BIB', 
            'DUTQTY': '$DUTQTY', 
            'PACKAGETYPE': '$PACKAGETYPE', 
            
            // 'DUTOFFRATE': { "$subtract": [1, { "$multiply": [ { "$divide" : ["$SMAP_P", "$TOTAL_USED_SOCKET"] }, 100 ] }]},
            'SMAP_P': '$SMAP_P', 
            // 'DUTONRATE': { $multiply: [ { $divide : ["$SMAP_P", "$TOTAL_USED_SOCKET"] }, 100 ] },
            // 'DUTONRATE': { $divide : ["$SMAP_P", 3] },
            'SMAP_X': '$SMAP_X', 
            'SMAP_D': '$SMAP_D', 
            'SMAP_T': '$SMAP_T',
            'SMAP_O': '$SMAP_O',
            'SMAP_D_T': '$SMAP_D_T',
            'SMAP_E': '$SMAP_E', 
            'BIB_STATUS_0': '$BIB_STATUS_0', 
            'BIB_STATUS_1': '$BIB_STATUS_1', 
            'BIB_STATUS_2': '$BIB_STATUS_2',  
            'BIB_STATUS_3': '$BIB_STATUS_3',  
            'BIB_STATUS_4': '$BIB_STATUS_4',  
            'BIB_STATUS_5': '$BIB_STATUS_5',  
            'BIB_STATUS_6': '$BIB_STATUS_6',  
            'DATETIME':"$_id.DATETIME"
            // 'FAMILYS': 1,
        }});
        pipe.push({'$sort': {"DATETIME": 1}});
        
        var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(DORDailyReport, pipe)
        .then(function(result){
          try {

            _.forEach(result, function(r,idx){ 
              if(result[idx]['TOTAL_USED_SOCKET']!=0){
                result[idx]['DUTONRATE'] = result[idx]['SMAP_P']/result[idx]['TOTAL_USED_SOCKET']*100;
                result[idx]['DUTOFFRATE'] = (1 - result[idx]['SMAP_P']/result[idx]['TOTAL_USED_SOCKET'])*100;
              }
            })
            var countt = 0;
            df.resolve({'data': result});
            
          } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
          }
        });
      // var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(DORDailyReport, pipe)
      // .then(function(result){
      //   try {
      //     console.log("%%%%%%%%%%%%%%%%%%%%%%% 222  ")
      //       var countt = 0;
      //       _.forEach(result,function(r){
      //         r.DATETIME = addHours(r.DATETIME);
      //       })
      //       df.resolve({'data': result});
      //   } catch (e) {
      //     console.log(e)
      //       df.reject(e.stack);
            
      //   }
      // });
    } catch (err){
      console.log(err)
      df.reject(err.stack);
      
    }

    return df.promise;
  };
 
  getWeekSearchResult (params) {
    var df = Q.defer();
    try {
        var query = params;
        var nowGroupBy = query['nowGroupBy']
        var match = {}

        var dateRange = {'DATETIME': {}};
        if (query.from !== null){
          dateRange.DATETIME['$gte'] = new Date(query.newWeekFrom);
        }
        if (query.to !== null){
          dateRange.DATETIME['$lt'] = new Date(query.newWeekTo);
        }

        if (!_.isEmpty(dateRange.DATETIME)){
          _.assign(match, dateRange);
        }

        _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, [ 'CUSTNAME', 'NEW_ASSSERIALNO', 'ASSSERIALNO', 'BOARDNO']), this.pathMapping));

        var pipe = [{'$match': match}];
        
        // if(('FAMILYS' in query)){
        // 	pipe.push({'$unwind': "$FAMILYS"});
        // 	pipe.push({'$match': this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['FAMILYS']), this.pathMapping)});
        // }
        
        var groupObject = {'$group':
                {
                    '_id': 
                        { 
                          // '$week':'$DATETIME',
                            'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }}
                        },
                    'TOTAL_USED_SOCKET':
                        {
                            '$sum':"$TOTAL_USED_SOCKET"
                        },
                    'SMAP_P':
                        {
                            '$sum':{ $add:  ['$SMAP_CAL.P','$SMAP_CAL.E'] }
                        },
                        
                    'DATETIME2':
                      {
                            '$first':"$DATETIME"
                        }
                }
            };
        groupObject['$group']['_id'][nowGroupBy] = "$"+nowGroupBy;

        pipe.push(groupObject);
        var projectObject ={
              '$project': {
                  '_id': 1,
                  'DATETIME': "$_id.DATETIME",
                  'SMAP_P': "$SMAP_P",
                  'TOTAL_USED_SOCKET': "$TOTAL_USED_SOCKET", 
                  "WEEK": { "$week": "$DATETIME2" }
                }
              };
        projectObject['$project'][nowGroupBy] = "$_id."+nowGroupBy;
        pipe.push(projectObject);
        // console.log("pipe",  pipe)
        pipe.push({'$sort': {"DATETIME": 1}});
        var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(DORDailyReport, pipe)
        .then(function(result){
          try {
            var countt = 0;
            df.resolve({'data': result});
            
          } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
          }
        });
    } catch (err){
      console.log(err)
      df.reject(err.stack);
      
    }

    return df.promise;
  };






  getMonthSearchResult (params, nowGroupBy) {
    var df = Q.defer();
    try {
        var query = params;
        var match = {}
        var nowGroupBy = query['nowGroupBy']
		    var dateRange = {'DATETIME': {}};
        if (query.from !== null){
          dateRange.DATETIME['$gte'] = new Date(query.newMonthFrom);
        }
        if (query.to !== null){
          dateRange.DATETIME['$lt'] = new Date(query.newMonthTo);
        }

        if (!_.isEmpty(dateRange.DATETIME)){
          _.assign(match, dateRange);
        }
        _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, [ 'CUSTNAME', 'NEW_ASSSERIALNO', 'ASSSERIALNO', 'BOARDNO']), this.pathMapping));
        
        var pipe = [{'$match': match}];
        
        // if(('FAMILYS' in query)){
        // 	pipe.push({'$unwind': "$FAMILYS"});
        // 	pipe.push({'$match': this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['FAMILYS']), this.pathMapping)});
        // }
        var groupObject = {'$group':
                          {
                              '_id': 
                                  { 
                                    // '$week':'$DATETIME',
                                      'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }}
                                  },
                              'TOTAL_USED_SOCKET':
                                  {
                                      '$sum':"$TOTAL_USED_SOCKET"
                                  },
                              'SMAP_P':
                                  {
                                      '$sum':{ $add:  ['$SMAP_CAL.P','$SMAP_CAL.E'] }
                                  },
                              'DATETIME2':
                                {
                                      '$first':"$DATETIME"
                                  }
                          }
                      };
        groupObject['$group']['_id'][nowGroupBy] = "$"+nowGroupBy;
        pipe.push(groupObject);

        var projectObject = {
                        '$project': {
                            '_id': 1,
                            'DATETIME': "$_id.DATETIME",
                            'SMAP_P': "$SMAP_P",
                            'TOTAL_USED_SOCKET': "$TOTAL_USED_SOCKET", 
                            "MONTH": { "$month": "$DATETIME2" }
                          }
                        };
        projectObject['$project'][nowGroupBy] = "$_id."+nowGroupBy;
        pipe.push(projectObject);
        // console.log("groupObject['$group']['_id']",  groupObject['$group']['_id'])
        // console.log("pipe",  pipe)
        pipe.push({'$sort': {"DATETIME": 1}});

        var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(DORDailyReport, pipe)
        .then(function(result){
          try {
            var countt = 0;
            df.resolve({'data': result});
            // console.log("!!!!!!!!!!!!!!!result mon  ",  result)
          } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
          }
        });
    } catch (err){
      console.log(err)
      df.reject(err.stack);
      
    }

    return df.promise;
  };

}

module.exports = DutoffrateManager;