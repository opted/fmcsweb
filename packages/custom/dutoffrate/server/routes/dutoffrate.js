(function() {
    'use strict';

    /* jshint -W098 */
    // The Package is past automatically as first parameter
    module.exports = function(Dutoffrate, app, auth, database, utils) {
    	var dutoffrateManager = require('../controllers/dutoffrate');
        var dutoffrateMgr = new dutoffrateManager(utils);

        app.post('/api/dutoffrate/getSearchResult', function(req, res, next) {
            dutoffrateMgr.getSearchResult(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });

        app.post('/api/dutoffrate/getBibSearchResult', function(req, res, next) {

            dutoffrateMgr.getBibSearchResult(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });

        app.post('/api/dutoffrate/getWeekSearchResult', function(req, res, next) {
        	
            dutoffrateMgr.getWeekSearchResult(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });

        app.post('/api/dutoffrate/getMonthSearchResult', function(req, res, next) {
        	
            dutoffrateMgr.getMonthSearchResult(req.body, res, next)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));;
        });

        app.post('/api/dutoffrate/getsearchfield', function(req, res, next){
       
            utils.ctrls.mongoQuery.getSearchField(dutoffrateMgr.coll, req.body, dutoffrateMgr.pathMapping, dutoffrateMgr.arrayFields)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });
    };
})();
