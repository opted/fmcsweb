'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Dutoffrate = new Module('dutoffrate');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Dutoffrate.register(function(app, auth, database, utils) {

  //We enable routing. By default the Package Object is passed to the routes
  Dutoffrate.routes(app, auth, database, utils);
  Dutoffrate.angularDependencies(['agGrid', 'ui.bootstrap']);
  //We are adding a link to the main menu for all authenticated users
//   Dutoffrate.menus.add({
//     title: 'dutoffrate example page',
//     link: 'dutoffrate example page',
//     roles: ['authenticated'],
//     menu: 'main'
//   });
  
  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Dutoffrate.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Dutoffrate.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Dutoffrate.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return Dutoffrate;
});
