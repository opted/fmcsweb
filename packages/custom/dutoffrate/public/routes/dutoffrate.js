(function() {
    'use strict';

    function Dutoffrate($stateProvider) {
        $stateProvider.state('dutoffrate page', {
            url: '/dutoffrate',
            templateUrl: 'dutoffrate/views/index.html',
            controller:DutoffrateController
        });
    }

    angular
        .module('mean.dutoffrate')
        .config(Dutoffrate);

    Dutoffrate.$inject = ['$stateProvider'];

})();
