
'use strict';   

    /* jshint -W098 */
function getToday(){
  var cur = new Date();  // Get current time
  cur.setHours(0, 0, 0, 0);  // Set to the start of day
  return cur;
}

function DutoffrateController(_,$scope, Global, Dutoffrate, $stateParams, $window, $http, srchDutoffrateOpt) {
    $scope.global = Global;

    $scope.package = {
        name: 'selftest'
    };
    $scope.selected = {
        "from": getToday(),  // Set default start day
        "to": getToday(),
        "cust": null,
        "custlotno": null,
        "dutlog": null
    };

    $scope.imageType = "histogramImage";
    $scope.bibBoardInfoGrid = {
        columnDefs: [
            {headerName: "DATETIME", cellClass:"textFormat", field: "DATETIME", width: 120},
            {headerName: "BIB ID", cellClass:"textFormat", field: "NEW_ASSSERIALNO", width: 70},
            {headerName: "客戶產品型號", cellClass:"textFormat", field: "BOARDNO", width: 120},
            {headerName: "客戶", cellClass:"textFormat", field: "CUSTNAME", width: 100},
            {headerName: "總Socket數", cellClass:"textFormat", field: "TOTAL_SOCKET", width: 80},
            {headerName: "總BIB數", cellClass:"textFormat", field: "TOTAL_BIB", width: 70},
            {headerName: "DUT數", cellClass:"textFormat", field: "DUTQTY", width: 70},
            {headerName: "封裝型式", cellClass:"textFormat", field: "PACKAGETYPE", width: 120},
            {headerName: "Dut on rate (%)", cellClass:"textFormat" , field: "DUTONRATE", width: 70},
            {headerName: "Dut off rate (%)", cellClass:"textFormat" , field: "DUTOFFRATE", width: 70},
            {headerName: "好座", cellClass:"textFormat", field: "SMAP_P", width: 50},
            {headerName: "壞座", cellClass:"textFormat", field: "SMAP_X", width: 50},
            {headerName: "暫停座", cellClass:"textFormat", field: "SMAP_D_T", width: 50},
            {headerName: "AutoDutOff", cellClass:"textFormat", field: "SMAP_O", width: 50},
            {headerName: "生產中", cellClass:"textFormat", field: "BIB_STATUS_0", width: 50},
            {headerName: "Idle (in BU5)", cellClass:"textFormat", field: "BIB_STATUS_1", width: 50},
            {headerName: "Idle (not in BU5)", cellClass:"textFormat", field: "BIB_STATUS_2", width: 50},
            {headerName: "工程領出", cellClass:"textFormat", field: "BIB_STATUS_3", width: 50},
            {headerName: "待修中", cellClass:"textFormat", field: "BIB_STATUS_4", width: 50},
            {headerName: "無法維修", cellClass:"textFormat", field: "BIB_STATUS_5", width: 50},
            {headerName: "待清潔", cellClass:"textFormat", field: "BIB_STATUS_6", width: 50}
        ],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        suppressExcelExport :false,
        excelStyles:[{id:'textFormat', dataType:'string'}]
    };
    $scope.boardInfoGrid = {
        columnDefs: [
            {headerName: "DATETIME", cellClass:"textFormat", field: "DATETIME", width: 120},
            {headerName: "BIB ID", cellClass:"textFormat", field: "NEW_ASSSERIALNO", width: 70},
            {headerName: "舊配件編號", cellClass:"textFormat", field: "ASSSERIALNO", width: 70},
            {headerName: "客戶產品型號", cellClass:"textFormat", field: "BOARDNO", width: 120},
            {headerName: "客戶", cellClass:"textFormat", field: "CUSTNAME", width: 100},
            {headerName: "總Socket數", cellClass:"textFormat", field: "TOTAL_SOCKET", width: 80},
            {headerName: "總BIB數", cellClass:"textFormat", field: "TOTAL_BIB", width: 70},
            {headerName: "DUT數", cellClass:"textFormat", field: "DUTQTY", width: 70},
            {headerName: "封裝型式", cellClass:"textFormat", field: "PACKAGETYPE", width: 120},
            {headerName: "Dut on rate (%)", cellClass:"textFormat" , field: "DUTONRATE", width: 70},
            {headerName: "Dut off rate (%)", cellClass:"textFormat" , field: "DUTOFFRATE", width: 70},
            {headerName: "好座", cellClass:"textFormat", field: "SMAP_P", width: 50},
            {headerName: "壞座", cellClass:"textFormat", field: "SMAP_X", width: 50},
            {headerName: "暫停座", cellClass:"textFormat", field: "SMAP_D_T", width: 50},
            {headerName: "AutoDutOff", cellClass:"textFormat", field: "SMAP_O", width: 50},
            {headerName: "生產中", cellClass:"textFormat", field: "BIB_STATUS_0", width: 50},
            {headerName: "Idle (in BU5)", cellClass:"textFormat", field: "BIB_STATUS_1", width: 50},
            {headerName: "Idle (not in BU5)", cellClass:"textFormat", field: "BIB_STATUS_2", width: 50},
            {headerName: "工程領出", cellClass:"textFormat", field: "BIB_STATUS_3", width: 50},
            {headerName: "待修中", cellClass:"textFormat", field: "BIB_STATUS_4", width: 50},
            {headerName: "無法維修", cellClass:"textFormat", field: "BIB_STATUS_5", width: 50},
            {headerName: "待清潔", cellClass:"textFormat", field: "BIB_STATUS_6", width: 50}
        ],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        suppressExcelExport :false,
        excelStyles:[{id:'textFormat', dataType:'string'}]
    };
    // $scope..groupBy = "CUSTNAME";
    $scope.groupByObject = {
        groupBy : "CUSTNAME",
        mainType : "0"  //0 for dut off rate 1 for dut on rate
    }
    $scope.filterSelected;
    $scope.groupListSelected;
    $scope.firstFieldListSelected="CUSTNAME";
    $scope.imageTypeSelected="histogramImage";
    $scope.secondListSelected;
    $scope.failedmodeSelected;
    $scope.level2Show = false;
    $scope.drillDownShow = false;
    $scope.histogramClickData = {};
    $scope.drillDownTitle = "";
    $scope.level2Info = {
        nowClickCus: "",
        drillDownType: 0, //0 for CUSTNAME, 1 for ASSSERIALNO, 2 for BOARDNO
        detectHaveClick: 0, 
        clickType:"0" //0 for single click, 1 for double click
    }
    $scope.$watch('level2Info.detectHaveClick',function(currentValue, previousValue){

        if($scope.level2Info.nowClickCus!=""){
            if($scope.level2Info.clickType == "0"){
                $scope.level2Show = true;
                $scope.level2QueryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
                
                if($scope.groupByObject.groupBy == "ASSSERIALNO"){
                    $scope.level2QueryParam.ASSSERIALNO = $scope.level2Info.nowClickCus;
                }else if($scope.groupByObject.groupBy == "NEW_ASSSERIALNO"){
                    $scope.level2QueryParam.NEW_ASSSERIALNO = $scope.level2Info.nowClickCus;
                }else if($scope.groupByObject.groupBy == "BOARDNO"){
                    $scope.level2QueryParam.BOARDNO = $scope.level2Info.nowClickCus;
                }else{
                    $scope.level2QueryParam.CUSTNAME = $scope.level2Info.nowClickCus;
                };
                if (_.isNull($scope.level2QueryParam.from) || _.isNull($scope.level2QueryParam.to)){
                    $window.alert("請設定日期");
                    return 0;
                }
        
                $scope.level2QueryParam.from = $scope.level2QueryParam.from.toISOString();
        
                if($scope.failedmodeSelected!==undefined)
                    $scope.level2QueryParam.failedmode = $scope.failedmodeSelected;
                    $scope.level2QueryParam.to.setDate($scope.level2QueryParam.to.getDate() + 1);
                    $scope.level2QueryParam.to = $scope.level2QueryParam.to.toISOString();
                    _.assign($scope.level2QueryParam, srchDutoffrateOpt.getMatch($scope.fields));
        
                    var config = {
                        eventHandlers: {
                            progress: function(event){
                                // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                                $scope.requestStatus = "正在下載資料..."; 
                            },
                        }
                    };
                    $scope.requestStatus = "等待 Server 回應...";
                    $http.post('/api/dutoffrate/getBibSearchResult', $scope.queryParam, config)
                    .then(function(result){
                        $scope.bibBoardInfoGrid.api.setRowData(result.data.data); 
                        $scope.bibRowNum = result.data.data.length;
                        $scope.bibRequestStatus = "Done";
                        // $scope.mbiQuery();
                    }, function(err){
                        $window.alert("Server failed...");
                        console.log(err)
                        $scope.requestStatus = "查詢失敗";
                    });
                    $http.post('/api/dutoffrate/getSearchResult', $scope.level2QueryParam, config)
                    .then(function(result){
                        // $scope.requestStatus = "處理回傳資料...";
                        $scope.histogramClickData = result.data.data;
                        $scope.requestStatus = "Done";
                        // $scope.mbiQuery();
                    }, function(err){
                        $window.alert("Server failed...");
                        console.log(err)
                        $scope.requestStatus = "查詢失敗";
                    });
            }else{
                if($scope.level2Info.drillDownType == 3){
                    $scope.drillDownTitle = "BIB ID"+" "+$scope.level2Info.nowClickCus;    
                }else if($scope.level2Info.drillDownType == 1){
                    $scope.drillDownTitle = "舊配件編號"+" "+$scope.level2Info.nowClickCus;    
                }else if($scope.level2Info.drillDownType == 2){
                    $scope.drillDownTitle = "客戶產品型號"+" "+$scope.level2Info.nowClickCus;
                }else{
                    $scope.drillDownTitle = "客戶"+" "+$scope.level2Info.nowClickCus;
                }
                
                $scope.drillDownShow = true;
                $scope.level2Show = false;
                
                $scope.queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
                if($scope.groupByObject.groupBy == "NEW_ASSSERIALNO"){
                    $scope.queryParam.NEW_ASSSERIALNO = $scope.level2Info.nowClickCus;
                }else if($scope.groupByObject.groupBy == "ASSSERIALNO"){
                    $scope.queryParam.ASSSERIALNO = $scope.level2Info.nowClickCus;
                }else if($scope.groupByObject.groupBy == "BOARDNO"){
                    $scope.queryParam.BOARDNO = $scope.level2Info.nowClickCus;
                }else{
                    $scope.queryParam.CUSTNAME = $scope.level2Info.nowClickCus;
                };
                if($scope.groupByObject.groupBy == "NEW_ASSSERIALNO"){
                    $scope.groupByObject.groupBy = "ASSSERIALNO";
                }
                if($scope.groupByObject.groupBy == "BOARDNO"){
                    $scope.groupByObject.groupBy = "NEW_ASSSERIALNO";
                }
                if($scope.groupByObject.groupBy == "CUSTNAME"){
                    $scope.groupByObject.groupBy = "BOARDNO";
                }

                if (_.isNull($scope.queryParam.from) || _.isNull($scope.queryParam.to)){
                    $window.alert("請設定日期");
                    return 0;
                }
        
                $scope.queryParam.from = $scope.queryParam.from.toISOString();
        
                if($scope.failedmodeSelected!==undefined)
                    $scope.queryParam.failedmode = $scope.failedmodeSelected;
                    $scope.queryParam.to.setDate($scope.queryParam.to.getDate() + 1);
                    $scope.queryParam.to = $scope.queryParam.to.toISOString();
                    _.assign($scope.queryParam, srchDutoffrateOpt.getMatch($scope.fields));
        
                    var config = {
                        eventHandlers: {
                            progress: function(event){
                                // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                                $scope.requestStatus = "正在下載資料..."; 
                            },
                        }
                    };
                    $scope.requestStatus = "等待 Server 回應...";
                    $http.post('/api/dutoffrate/getBibSearchResult', $scope.queryParam, config)
                    .then(function(result){
                        $scope.bibBoardInfoGrid.api.setRowData(result.data.data); 
                        $scope.bibRowNum = result.data.data.length;
                        $scope.bibRequestStatus = "Done";
                        // $scope.mbiQuery();
                    }, function(err){
                        $window.alert("Server failed...");
                        console.log(err)
                        $scope.requestStatus = "查詢失敗";
                    });
                    $http.post('/api/dutoffrate/getSearchResult', $scope.queryParam, config)
                    .then(function(result){
        
                        // $scope.requestStatus = "處理回傳資料...";
                        $scope.rows = result.data.data;
                        $scope.boardInfoGrid.api.setRowData($scope.rows); 
        
                        $scope.rowNum = $scope.rows.length;
                        $scope.requestStatus = "Done";
                        // $scope.mbiQuery();
                    }, function(err){
                        $window.alert("Server failed...");
                        console.log(err)
                        $scope.requestStatus = "查詢失敗";
                    });
            }
            
        }else{
            $scope.level2Show = false;
        }

    },true)

    $scope.varsSeleted = {
        firstFieldList: [
            {"name":"All",id:"all"},
            {"name":"客戶",id:"CUSTNAME"},
            // {"name":"客戶產品型號",id:"FAMILY"}
        ],
        mainTypeList: [
            {"name":"Dutoffrate",id:"0"},
            {"name":"Dutonrate",id:"1"},
            // {"name":"客戶產品型號",id:"FAMILY"}
        ],
        imageTypeList:[
            {"name": "正常圖", id:"normalImage"},
            {"name": "推移圖", id:"moveImage"},
            {"name": "直方圖", id:"histogramImage"},
        ]
    }
    $scope.fieldSearchURL = "/api/dutoffrate/getsearchfield";
    $scope.fields = srchDutoffrateOpt.fields;
    $scope.disableSubField = true;

    $scope.downloadExel = function(){
        // _.forEach($scope.rows, function(r,idx){ 
        //      $scope.rows[idx]= _.omit(r,'ISODATE')
        // })

        $scope.boardInfoGrid.api.setRowData($scope.rows);
        $scope.exportXLS()

    }

    $scope.changeFailedmode = function(mode){
        $scope.failedmodeSelected = mode.name;
    }
    $scope.exportBibXLS = function(){
    // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
        if ($scope.bibBoardInfoGrid.api.getModel().getRowCount() < 1){
            $window.alert("資料處理中，請稍後...");
            return;
        }

        var params = {
            fileName: "DutOffRate_Export.xls"
        };
        $scope.bibBoardInfoGrid.api.exportDataAsExcel(params);
        // $scope.boardInfoGrid.api.getDataAsExcel(params);
    };
    $scope.exportXLS = function(){
    // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
        if ($scope.boardInfoGrid.api.getModel().getRowCount() < 1){
          $window.alert("資料處理中，請稍後...");
          return;
        }

        var params = {
          fileName: "DutOffRate_Export.xls"
        };
        $scope.boardInfoGrid.api.exportDataAsExcel(params);
        // $scope.boardInfoGrid.api.getDataAsExcel(params);
    };

    $scope.changeGroup = function(groupBy){
        $scope.groupByObject.groupBy = groupBy;
    }
    $scope.changeMainType = function(groupBy){
        $scope.groupByObject.imgeType = groupBy;
    }
    $scope.changeImage = function(imageType){
        $scope.imageType = imageType;
    }




    $scope.uploadXmlData = null;
    $scope.fields = srchDutoffrateOpt.fields;


    $scope.ok = function() {
        $scope.drillDownShow = false;
        if(!$scope.drillDownShow){
            $scope.groupByObject.groupBy = "CUSTNAME";
        }

        $scope.queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
        if (_.isNull($scope.queryParam.from) || _.isNull($scope.queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }

        $scope.queryParam.from = $scope.queryParam.from.toISOString();

        if($scope.failedmodeSelected!==undefined)
            $scope.queryParam.failedmode = $scope.failedmodeSelected;
            $scope.queryParam.to.setDate($scope.queryParam.to.getDate() + 1);
            $scope.queryParam.to = $scope.queryParam.to.toISOString();
            _.assign($scope.queryParam, srchDutoffrateOpt.getMatch($scope.fields));

            var config = {
                eventHandlers: {
                    progress: function(event){
                        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                        $scope.requestStatus = "正在下載資料..."; 
                    },
                }
            };
            $scope.requestStatus = "等待 Server 回應...";

            $http.post('/api/dutoffrate/getBibSearchResult', $scope.queryParam, config)
                    .then(function(result){
                        $scope.bibBoardInfoGrid.api.setRowData(result.data.data); 
                        $scope.bibRowNum = result.data.data.length;
                        $scope.bibRequestStatus = "Done";
                        // $scope.mbiQuery();
                    }, function(err){
                        $window.alert("Server failed...");
                        console.log(err)
                        $scope.bibRequestStatus = "查詢失敗";
                    });
            $http.post('/api/dutoffrate/getSearchResult', $scope.queryParam, config)
            .then(function(result){

                // $scope.requestStatus = "處理回傳資料...";
                $scope.rows = result.data.data;
                $scope.boardInfoGrid.api.setRowData($scope.rows); 

                $scope.rowNum = $scope.rows.length;
                $scope.requestStatus = "Done";
                // $scope.mbiQuery();
            }, function(err){
                $window.alert("Server failed...");
                console.log(err)
                $scope.requestStatus = "查詢失敗";
            });
    };


}

angular
    .module('mean.dutoffrate')
    .controller('DutoffrateController', DutoffrateController);

DutoffrateController.$inject = ['_','$scope', 'Global', 'Dutoffrate', '$stateParams', '$window', '$http', 'srchDutoffrateOpt'];

