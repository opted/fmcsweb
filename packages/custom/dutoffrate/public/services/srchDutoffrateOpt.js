(function(){
    'use strict';
    function srchDutoffrateOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("客戶", "CUSTNAME", "string"),
                uOpt.genField("客戶產品型號", "BOARDNO", "string"),
                uOpt.genField("BIB ID (判別碼)", "NEW_ASSSERIALNO", "string"),
                uOpt.genField("舊配件編號", "ASSSERIALNO", "string"),
                
                // uOpt.genField("BOARD", "BOARD", "string", false, false),
            ],
            getMatch: uOpt.getMatch 
        }; 
    }; 

    angular.module('mean.dutoffrate').factory('srchDutoffrateOpt', srchDutoffrateOpt);
    srchDutoffrateOpt.$inject = ['utilsSrchOpt'];
})();