(function() {
    'use strict';

    function Dutoffrate($http, $q) {
        return {
            name: 'dutoffrate',
            checkCircle: function(circle) {
                var deferred = $q.defer();

                $http.get('/api/dutoffrate/example/' + circle).success(function(response) {
                    deferred.resolve(response);
                }).error(function(response) {
                    deferred.reject(response);
                });
                return deferred.promise;

            }
        };
    }

    angular
        .module('mean.dutoffrate')
        .factory('Dutoffrate', Dutoffrate);

    Dutoffrate.$inject = ['$http', '$q'];

})();
