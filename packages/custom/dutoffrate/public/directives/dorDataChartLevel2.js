(function(){
    'use strict';
    // var Q = require('q');
    function dorDataChartLevel2(_, $http, $q){
  
      return {
        restrict: 'E',
        scope: {
          groupBy:"=",
          mainType:"=",
          tb: "=",
          imageType: "=",
          queryParam: "=",
          firstFieldList: "=",
          histogramClickData: "=",
          nowClickCus: "=",
        },
        // templateUrl: 'selftest/views/mtbfDataPanel.html',
        template: '<div style="width: 100%;height: 500px;"><div></div></div>',
        link: function(scope, element, attrs){
          var chartOptions;
          var genGraph = function(id, title, valueField){
              // var graphList = [];
              var graph = new Object({
                  // "type": "serial",
                  "bullet": "square",
                  "bulletBorderAlpha": 1,
                  "bulletBorderThickness": 1,
                  // "bulletColor": "#FF6600",
                  // "legendPeriodValueText": "total: [[value.sum]]",
                  "legendValueText": "[[value]]",
                  "labelText": "[[value]]",
                  "id": id,
                  "title": title,
                  "valueField": valueField,
                  "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
              });
              // graphList.push(graph)
              return graph
          }
          var genHistogramGraph = function(id, title, valueField){
              // var graphList = [];
              var graph = new Object({
                  // "type": "column",
                  "alphaField": "alpha",
                  "balloonText": "[[value]] 次",
                  "dashLengthField": "dashLength",
                  "fillAlphas": 0.8,
                  "fillColors": "#99CCFF",
                  "lineColor": "#0066CC",
                  "legendPeriodValueText": "total: [[value.sum]]",
                  "legendValueText": "[[value]]",
                  "labelText": "[[value]]",
                  "labelPosition": "top",
                  "showAllValueLabels": true,
                  "color": "#3366CC",
                  "fontSize": 15,
                  "type": "column",
                  "id": id,
                  "title": title,
                  "valueField": valueField,
                  "valueAxis": "numOfRepairAxis"
                });
              // graphList.push(graph)
              return graph
          }
  
  
          chartOptions = {
              "type": "serial",
              "theme": "light",
              "autoMargins": true,
              // "marginRight": 40,
              "marginLeft": 60,
              // "marginBottom": 60,
              // "autoMarginOffset": 20,
              "mouseWheelZoomEnabled":true,
              "dataDateFormat": "YYYY-MM-DD JJ:NN:SS",
              "valueAxes": [{
                  "id": "v1",
                  "axisAlpha": 0,
                  "position": "left",
                  "ignoreAxisWidth":true,
                  "title":"DUTOFFRATE %"
              }],
  
              "balloon": {
                  "borderThickness": 1,
                  "shadowAlpha": 0
              },
              // "legend": {
              //     "valueText":"",
              //     "horizontalGap": 6,
              //     "position": "right",
              //     "useGraphSettings": true,
              //     "markerSize": 6,
              //     "maxColumns": 6,
              // },
              "titles": [
                  {"text": "推移圖", "size": 30},{}
              ],
              "legend": {
                "equalWidths": false,
                "useGraphSettings": true,
                "valueAlign": "left",   
                "valueWidth": 120
              },
              "graphs": [],
              "chartCursor": {
  
              },
              "valueField": "DUTOFFRATE",
              "categoryField": "DATETIME",
              "categoryAxis": {
                  "parseDates": true,
                  "dashLength": 1,
                  "minorGridEnabled": true,
                  "minPeriod":"ss"
              },
              "export": {
                  "enabled": true
              },
              "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
                    "cursorAlpha": 0.1,
                    "cursorColor":"#000000",
                    "fullWidth":true,
                    "valueBalloonsEnabled": false,
                    "zoomable": true
                },
                "chartScrollbar": {
                    "autoGridCount": true,
                    "scrollbarHeight": 25
                },
              "categoryAxis": {
                  "autoRotateCount": 8,
                  "autoRotateAngle": -45
              },
  
              "dataProvider": []
  
          }
  
          var target = element[0].children[0];
  
  
          var chart = AmCharts.makeChart(target, chartOptions);
          
          scope.$watchGroup(['histogramClickData'], function () {
            if(scope.nowClickCus!=""){
                // if(scope.groupBy == "ASSSERIALNO"){
                //     scope.groupBy = "all"
                // }else if(scope.groupBy == "BOARDNO"){
                //     scope.groupBy = "all"
                // };
                chart.mouseWheelZoomEnabled = true;
                updateByMoveImage();
            }
          });
  
          var getWeekSearchResult = function(queryParam){
              var deferred = $q.defer();
              $http.post('/api/dutoffrate/getWeekSearchResult', queryParam)
                  .then(function(result){

                      deferred.resolve(result);
                  }, function(err){
                      deferred.reject(err);
                      console.log(err)
                      
                  });
            return deferred.promise;
          };
  
          var getMonthSearchResult = function(queryParam){
              var deferred = $q.defer();
              $http.post('/api/dutoffrate/getMonthSearchResult', queryParam)
                  .then(function(result){
                      deferred.resolve(result);
                  }, function(err){
                      deferred.reject(err);
                      console.log(err)
                      
                  });
            return deferred.promise;
          };
          var getUniqueValuesOfKey = function(array, key){
            return array.reduce(function(carry, item){
              if(item[key] && !~carry.indexOf(item[key])) carry.push(item[key]);
              return carry;
            }, []);
          }
          var getCosNewRows = function(now_rows, store_keys, group_by_key){
              var new_rows2 = [];
              var now_date = '';
              var new_row = {};
              _.forEach(now_rows, function(now_object) {
                  if(now_date == ''){
                      now_date = now_object[group_by_key]
                      new_row[group_by_key] = now_object[group_by_key]
                  }
                  if(now_date == now_object[group_by_key]){
                      if(isNaN(now_object.DUTOFFRATE)){
                        if(scope.mainType == "0"){
                          new_row['DUTOFFRATE'+store_keys.indexOf(now_object[scope.groupBy])] = 0
                        }else{
                          new_row['DUTONRATE'+store_keys.indexOf(now_object[scope.groupBy])] = 0
                        }
                      }else{
                        if(scope.mainType == "0"){
                          new_row['DUTOFFRATE'+store_keys.indexOf(now_object[scope.groupBy])] = now_object.DUTOFFRATE
                        }else{
                          new_row['DUTONRATE'+store_keys.indexOf(now_object[scope.groupBy])] = now_object.DUTONRATE
                        }
                      }
                      
                  }else{
                      new_rows2.push(new_row)
                      new_row = {}
                      now_date = now_object[group_by_key]
                      new_row[group_by_key] = now_object[group_by_key]
                      if(isNaN(now_object.DUTOFFRATE)){
                        if(scope.mainType == "0"){
                          new_row['DUTOFFRATE'+store_keys.indexOf(now_object[scope.groupBy])] = 0
                        }else{
                            new_row['DUTONRATE'+store_keys.indexOf(now_object[scope.groupBy])] = 0
                        }
                      }else{
                        if(scope.mainType == "0"){
                          new_row['DUTOFFRATE'+store_keys.indexOf(now_object[scope.groupBy])] = now_object.DUTOFFRATE
                        }else{
                            new_row['DUTONRATE'+store_keys.indexOf(now_object[scope.groupBy])] = now_object.DUTONRATE
                        }
                      }
                  }
              });
              new_rows2.push(new_row)
              return new_rows2
          }
          var updateByMoveImage = function(){
              var df = $q.defer();
              var weekData = []
              var monthData = []
              var queryList = []
              if(scope.groupBy == "NEW_ASSSERIALNO"){
                scope.queryParam['nowGroupBy'] = "NEW_ASSSERIALNO";
              }else if(scope.groupBy == "ASSSERIALNO"){
                scope.queryParam['nowGroupBy'] = "ASSSERIALNO";
              }else if(scope.groupBy == "BOARDNO"){
                scope.queryParam['nowGroupBy'] = "BOARDNO";
              }else{
                scope.queryParam['nowGroupBy'] = "CUSTNAME";
              }

              if(moment(scope.queryParam.from).date() - moment(scope.queryParam.from).isoWeekday() > 0){
                  
                  scope.queryParam.newWeekTo = moment(scope.queryParam.from).add(-moment(scope.queryParam.from).isoWeekday(), 'days').format("YYYY-MM-DD HH:mm:ss")
                  scope.queryParam.newWeekFrom = moment(scope.queryParam.newWeekTo).add(-Math.ceil((moment(scope.queryParam.from).date() - moment(scope.queryParam.from).isoWeekday())/7)*7, 'days').format("YYYY-MM-DD HH:mm:ss")
  
                  queryList.push(getWeekSearchResult(scope.queryParam))
              }
              scope.queryParam.newMonthTo = moment(scope.queryParam.from).subtract(1, 'months').endOf('month').format('YYYY-MM-DD');
              scope.queryParam.newMonthFrom = moment(scope.queryParam.from).subtract(2, 'months').startOf('month').format('YYYY-MM-DD');
  
              queryList.push(getMonthSearchResult(scope.queryParam))
              var new_rows = [];
              if(scope.groupBy == "CUSTNAME"){
                  chart.titles[1] = {"text": "Group By 客戶", "size": 15, "bold": false};
                  $q.all(queryList)
                  .then(function(results){ 
                      var groups = _.groupBy(scope.histogramClickData, function(value){
                          return value.CUSTNAME + '#' + value.DATETIME;
                      });
                      
                      var day_rows = _.map(groups, function(group){
                          return {
                              "CUSTNAME": group[0].CUSTNAME,
                              // "DATETIME": group[0].DATETIME,
                              'SELFDEFINEDATE': moment(group[0].DATETIME).format('YYYY-MM-DD'),
                              'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                              'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                          }
                      });
                      var store_costnames = [];
                      var store_rows = {};
                      _.forEach(day_rows,function(new_row){
                          var now_custname = new_row.CUSTNAME;
                          if(now_custname != ""){
                              if(store_costnames.indexOf(now_custname)>-1){
                                  store_rows[now_custname].push(new_row)
                              }else{
                                  store_costnames.push(now_custname)
                                  store_rows[now_custname] = []
                                  store_rows[now_custname].push(new_row)
                              }
                          }
                      });
                      var store_keys = Object.keys(store_rows);
                      
                      if(results.length == 2){
                          var week_groups = _.groupBy(results[0].data.data, function(value){
                              return value.CUSTNAME + '#' + value.WEEK;
                          });
                          var week_rows = _.map(week_groups, function(group){
                              return {
                                  "CUSTNAME": group[0].CUSTNAME,
                                  // "DATETIME": group[0].DATETIME,
                                  'SELFDEFINEDATE': "WEEK" + group[0].WEEK,
                                  'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                                  'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                              }
                          }).reverse();
                          
                      }
                      var month_groups = _.groupBy(results[((results.length == 2) ? 1 : 0)].data.data, function(value){
                          return value.CUSTNAME + '#' + value.MONTH;
                      });
                      var month_rows = _.map(month_groups, function(group){
                          return {
                              "CUSTNAME": group[0].CUSTNAME,
                              // "DATETIME": group[0].DATETIME,
                              'SELFDEFINEDATE': "MONTH" + group[0].MONTH,
                              'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                              'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                          }
                      });
                      day_rows = day_rows.reverse();
                      day_rows.push.apply(day_rows, week_rows)
                      day_rows.push.apply(day_rows, month_rows)
                      day_rows = getCosNewRows(day_rows, store_keys, "SELFDEFINEDATE");
                      day_rows = day_rows.reverse();
                      chart.categoryField  = "SELFDEFINEDATE";
                      chart.categoryAxis.parseDates = false;
                      chart.dataProvider = day_rows
                      var graphList = []
                      for(var i = 0 ; i < store_keys.length ; i++){
                        if(scope.mainType == "0"){
                          graphList.push(genGraph('dutoffrate'+i, store_keys[i], "DUTOFFRATE"+i))
                        }else{
                          graphList.push(genGraph('dutonrate'+i, store_keys[i], "DUTONRATE"+i))
                        }
                      }
                      chart.graphs = graphList
                      chart.validateData();
                      chart.validateNow(); 
  
                  })
                  .catch((err) => {
                      console.log(err);
                      df.reject(err.stack);
                  });
                  
              }else if(scope.groupBy == "NEW_ASSSERIALNO"){
                chart.titles[1] = {"text": "Group By BIB ID", "size": 15, "bold": false};
                $q.all(queryList)
                .then(function(results){ 
                    var groups = _.groupBy(scope.histogramClickData, function(value){
                        return value.NEW_ASSSERIALNO + '#' + value.DATETIME;
                    });

                    var day_rows = _.map(groups, function(group){
                        return {
                            "NEW_ASSSERIALNO": group[0].NEW_ASSSERIALNO,
                            // "DATETIME": group[0].DATETIME,
                            'SELFDEFINEDATE': moment(group[0].DATETIME).format('YYYY-MM-DD'),
                            'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                            'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                        }
                    });
                    var store_costnames = [];
                    var store_rows = {};
                    _.forEach(day_rows,function(new_row){
                        var now_asserialno = new_row.NEW_ASSSERIALNO;
                        if(now_asserialno != ""){
                            if(store_costnames.indexOf(now_asserialno)>-1){
                                store_rows[now_asserialno].push(new_row)
                            }else{
                                store_costnames.push(now_asserialno)
                                store_rows[now_asserialno] = []
                                store_rows[now_asserialno].push(new_row)
                            }
                        }
                    });
                    var store_keys = Object.keys(store_rows);
                    
                    if(results.length == 2){
                        var week_groups = _.groupBy(results[0].data.data, function(value){
                            return value.NEW_ASSSERIALNO + '#' + value.WEEK;
                        });
                        var week_rows = _.map(week_groups, function(group){
                            return {
                                "NEW_ASSSERIALNO": group[0].NEW_ASSSERIALNO,
                                // "DATETIME": group[0].DATETIME,
                                'SELFDEFINEDATE': "WEEK" + group[0].WEEK,
                                'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                                'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                            }
                        }).reverse();
                        
                    }
                    var month_groups = _.groupBy(results[((results.length == 2) ? 1 : 0)].data.data, function(value){
                        return value.NEW_ASSSERIALNO + '#' + value.MONTH;
                    });
                    var month_rows = _.map(month_groups, function(group){
                        return {
                            "NEW_ASSSERIALNO": group[0].NEW_ASSSERIALNO,
                            // "DATETIME": group[0].DATETIME,
                            'SELFDEFINEDATE': "MONTH" + group[0].MONTH,
                            'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                            'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                        }
                    });
                    day_rows = day_rows.reverse();
                    day_rows.push.apply(day_rows, week_rows)
                    day_rows.push.apply(day_rows, month_rows)
                    day_rows = getCosNewRows(day_rows, store_keys, "SELFDEFINEDATE");
                    day_rows = day_rows.reverse();
                    chart.categoryField  = "SELFDEFINEDATE";
                    chart.categoryAxis.parseDates = false;
                    chart.dataProvider = day_rows
                    var graphList = []
                    for(var i = 0 ; i < store_keys.length ; i++){
                        if(scope.mainType == "0"){
                            graphList.push(genGraph('dutoffrate'+i, store_keys[i], "DUTOFFRATE"+i))
                        }else{
                            graphList.push(genGraph('dutonrate'+i, store_keys[i], "DUTONRATE"+i))
                        }
                    }
                    chart.graphs = graphList
                    chart.validateData();
                    chart.validateNow(); 

                })
                .catch((err) => {
                    console.log(err);
                    df.reject(err.stack);
                });
                
            }else if(scope.groupBy == "ASSSERIALNO"){
                chart.titles[1] = {"text": "Group By 舊配件編號", "size": 15, "bold": false};
                $q.all(queryList)
                .then(function(results){ 
                    var groups = _.groupBy(scope.histogramClickData, function(value){
                        return value.ASSSERIALNO + '#' + value.DATETIME;
                    });

                    var day_rows = _.map(groups, function(group){
                        return {
                            "ASSSERIALNO": group[0].ASSSERIALNO,
                            // "DATETIME": group[0].DATETIME,
                            'SELFDEFINEDATE': moment(group[0].DATETIME).format('YYYY-MM-DD'),
                            'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                            'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                        }
                    });
                    var store_costnames = [];
                    var store_rows = {};
                    _.forEach(day_rows,function(new_row){
                        var now_asserialno = new_row.ASSSERIALNO;
                        if(now_asserialno != ""){
                            if(store_costnames.indexOf(now_asserialno)>-1){
                                store_rows[now_asserialno].push(new_row)
                            }else{
                                store_costnames.push(now_asserialno)
                                store_rows[now_asserialno] = []
                                store_rows[now_asserialno].push(new_row)
                            }
                        }
                    });
                    var store_keys = Object.keys(store_rows);
                    
                    if(results.length == 2){
                        var week_groups = _.groupBy(results[0].data.data, function(value){
                            return value.ASSSERIALNO + '#' + value.WEEK;
                        });
                        var week_rows = _.map(week_groups, function(group){
                            return {
                                "ASSSERIALNO": group[0].ASSSERIALNO,
                                // "DATETIME": group[0].DATETIME,
                                'SELFDEFINEDATE': "WEEK" + group[0].WEEK,
                                'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                                'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                            }
                        }).reverse();
                        
                    }
                    var month_groups = _.groupBy(results[((results.length == 2) ? 1 : 0)].data.data, function(value){
                        return value.ASSSERIALNO + '#' + value.MONTH;
                    });
                    var month_rows = _.map(month_groups, function(group){
                        return {
                            "ASSSERIALNO": group[0].ASSSERIALNO,
                            // "DATETIME": group[0].DATETIME,
                            'SELFDEFINEDATE': "MONTH" + group[0].MONTH,
                            'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                            'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                        }
                    });
                    day_rows = day_rows.reverse();
                    day_rows.push.apply(day_rows, week_rows)
                    day_rows.push.apply(day_rows, month_rows)
                    day_rows = getCosNewRows(day_rows, store_keys, "SELFDEFINEDATE");
                    day_rows = day_rows.reverse();
                    chart.categoryField  = "SELFDEFINEDATE";
                    chart.categoryAxis.parseDates = false;
                    chart.dataProvider = day_rows
                    var graphList = []
                    for(var i = 0 ; i < store_keys.length ; i++){
                        if(scope.mainType == "0"){
                            graphList.push(genGraph('dutoffrate'+i, store_keys[i], "DUTOFFRATE"+i))
                        }else{
                            graphList.push(genGraph('dutonrate'+i, store_keys[i], "DUTONRATE"+i))
                        }
                    }
                    chart.graphs = graphList
                    chart.validateData();
                    chart.validateNow(); 

                })
                .catch((err) => {
                    console.log(err);
                    df.reject(err.stack);
                });
                
            }else if(scope.groupBy == "BOARDNO"){
                chart.titles[1] = {"text": "Group By 客戶產品型號", "size": 15, "bold": false};
                $q.all(queryList)
                .then(function(results){ 
                    var groups = _.groupBy(scope.histogramClickData, function(value){
                        return value.BOARDNO + '#' + value.DATETIME;
                    });

                    var day_rows = _.map(groups, function(group){
                        return {
                            "BOARDNO": group[0].BOARDNO,
                            // "DATETIME": group[0].DATETIME,
                            'SELFDEFINEDATE': moment(group[0].DATETIME).format('YYYY-MM-DD'),
                            'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                            'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                        }
                    });

                    var store_costnames = [];
                    var store_rows = {};
                    _.forEach(day_rows,function(new_row){
                        var now_boardno = new_row.BOARDNO;
                        if(now_boardno != ""){
                            if(store_costnames.indexOf(now_boardno)>-1){
                                store_rows[now_boardno].push(new_row)
                            }else{
                                store_costnames.push(now_boardno)
                                store_rows[now_boardno] = []
                                store_rows[now_boardno].push(new_row)
                            }
                        }
                    });

                    var store_keys = Object.keys(store_rows);
                    
                    if(results.length == 2){
                        var week_groups = _.groupBy(results[0].data.data, function(value){
                            return value.BOARDNO + '#' + value.WEEK;
                        });
                        var week_rows = _.map(week_groups, function(group){
                            return {
                                "BOARDNO": group[0].BOARDNO,
                                // "DATETIME": group[0].DATETIME,
                                'SELFDEFINEDATE': "WEEK" + group[0].WEEK,
                                'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                                'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                            }
                        }).reverse();
                        
                    }
                    var month_groups = _.groupBy(results[((results.length == 2) ? 1 : 0)].data.data, function(value){
                        return value.BOARDNO + '#' + value.MONTH;
                    });
                    var month_rows = _.map(month_groups, function(group){
                        return {
                            "BOARDNO": group[0].BOARDNO,
                            // "DATETIME": group[0].DATETIME,
                            'SELFDEFINEDATE': "MONTH" + group[0].MONTH,
                            'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                            'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                        }
                    });
                    day_rows = day_rows.reverse();
                    day_rows.push.apply(day_rows, week_rows)
                    day_rows.push.apply(day_rows, month_rows)
                    day_rows = getCosNewRows(day_rows, store_keys, "SELFDEFINEDATE");
                    day_rows = day_rows.reverse();
                    chart.categoryField  = "SELFDEFINEDATE";
                    chart.categoryAxis.parseDates = false;
                    chart.dataProvider = day_rows
                    var graphList = []
                    for(var i = 0 ; i < store_keys.length ; i++){
                        if(scope.mainType == "0"){
                            graphList.push(genGraph('dutoffrate'+i, store_keys[i], "DUTOFFRATE"+i))
                        }else{
                            graphList.push(genGraph('dutonrate'+i, store_keys[i], "DUTONRATE"+i))
                        }
                    }
                    chart.graphs = graphList
                    chart.validateData();
                    chart.validateNow(); 

                })
                .catch((err) => {
                    console.log(err);
                    df.reject(err.stack);
                });
                
            }else{
                  chart.titles[1] = {"text": "Group By 時間", "size": 15, "bold": false};
                  $q.all(queryList)
                  .then(function(results){  
                      
                      var day_rows = _.chain(scope.data)
                          .groupBy('DATETIME')
                          .map((r,k) => new Object({
                              'SELFDEFINEDATE': moment(k).format('YYYY-MM-DD'),
                              'DUTOFFRATE':parseFloat(Number(100-(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100)).toFixed(1)),
                              'DUTONRATE':parseFloat(Number(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100).toFixed(1))
                              }))
                          .value()
                      
                      if(results.length == 2){
                          var week_rows = _.chain(results[0].data.data)
                          
                          .groupBy('WEEK')
                          .map((r,k) => new Object({
                              'SELFDEFINEDATE':"WEEK" + k,
                              'DUTOFFRATE':parseFloat(Number(100-(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100)).toFixed(1)),
                              'DUTONRATE':parseFloat(Number(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100).toFixed(1))
                              })).reverse()
                          .value()
                          
                      }
                      var month_rows = _.chain(results[((results.length == 2) ? 1 : 0)].data.data)
                      
                      .groupBy('MONTH')
                      .map((r,k) => new Object({
                          'SELFDEFINEDATE':"MONTH" + k,
                          'DUTOFFRATE':parseFloat(Number(100-(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100)).toFixed(1)),
                          'DUTONRATE':parseFloat(Number(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100).toFixed(1))
                          }))
                      .value()
                      
                      day_rows = day_rows.reverse();
                      day_rows.push.apply(day_rows, week_rows)
                      day_rows.push.apply(day_rows, month_rows)
                      day_rows = day_rows.reverse();
                      chart.categoryField  = "SELFDEFINEDATE";
                      chart.categoryAxis.parseDates = false;
                      chart.dataProvider = day_rows
                      chart.graphs = [genGraph('dutoffrate', "Dutoffrate", "DUTOFFRATE")]
                      if(scope.mainType == "0"){
                        chart.graphs = [genGraph('dutoffrate', "Dutoffrate", "DUTOFFRATE")]
                      }else{
                        chart.graphs = [genGraph('dutonrate', "Dutonrate", "DUTONRATE")]
                      }
                      chart.validateData();
                      chart.validateNow();
  
                  })
                  .catch((err) => {
                      console.log(err);
                      df.reject(err.stack);
                  });
                  
              }
  
  
          }
  
          var updateByCustname = function(){
              var groups = _.groupBy(scope.data, function(value){
                  return value.CUSTNAME + '#' + value.DATETIME;
              });
              
              var new_rows = _.map(groups, function(group){
                  return {
                      "CUSTNAME": group[0].CUSTNAME,
                      "DATETIME": group[0].DATETIME,
                      'DUTOFFRATE':100-Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2),
                      'DUTONRATE':Math.round(_.sumBy(_.map(group, 'SMAP_P'))/_.sumBy(_.map(group, 'TOTAL_USED_SOCKET'))*100,2)
                  }
              });
  
              var store_costnames = [];
              var store_rows = {};
              _.forEach(new_rows,function(new_row){
                  var now_custname = new_row.CUSTNAME;
                  if(now_custname != ""){
                      if(store_costnames.indexOf(now_custname)>-1){
                          store_rows[now_custname].push(new_row)
                      }else{
                          store_costnames.push(now_custname)
                          store_rows[now_custname] = []
                          store_rows[now_custname].push(new_row)
                      }
                  }
              });
  
              var store_keys = Object.keys(store_rows);
              var new_rows2 = [];
              new_rows2 = getCosNewRows(new_rows, store_keys, "DATETIME");
  
              chart.dataProvider = new_rows2
              var graphList = []
              for(var i = 0 ; i < store_keys.length ; i++){
                  graphList.push(genGraph('dutoffrate'+i, store_keys[i], "DUTOFFRATE"+i))
              }
              chart.graphs = graphList
              chart.validateData();
              chart.validateNow(); 
          }
          var updateChart = function(){
            if(_.isArray(scope.data)){
                // console.log(scope.data)
                chart.dataProvider = scope.data;
                chart.validateData();
                chart.validateNow();
            } 
          }
  
          scope.getChart = function(){
            return chart;
          }
          chart.validateData();
          chart.validateNow();
  
        }
      };
    };
    
    angular.module('mean.dutoffrate').directive('dorDataChartLevel2', dorDataChartLevel2);
    dorDataChartLevel2.$inject = ["_", '$http', "$q"];
    // stDataChart.$inject = ['mtbfQueryService', 'mtbfTabService'];
  })();