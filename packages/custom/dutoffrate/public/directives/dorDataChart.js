(function(){
  'use strict';
  // var Q = require('q');
  function dorDataChart(_, $http, $q){
    return {
      restrict: 'E',
      scope: {
        data:"=",
        groupBy:"=",
        mainType:"=",
        tb: "=",

        queryParam: "=",
        firstFieldList: "=",
        histogramClickData: "=",
        nowClickCus: "=",
        clickType: "=",
        detectHaveClick: "=",
        drillDownType: "=",
        dorClickHandler: "&"
      },
    //   template: '',
      // templateUrl: 'selftest/views/mtbfDataPanel.html',
      template: // using hint
      '<div class="row">\
      <label class="text-primary control-label col-sm-12 vcenter">* 點擊一次 產生推移圖</label>\
      <label class="text-primary control-label col-sm-12 vcenter">* 點擊兩次 Drill down 層級</label></div>'+
      '<div style="width: 100%;height: 500px;"><div></div></div>',
      link: function(scope, element, attrs){
        var chartOptions;

        
        var genHistogramGraph = function(id, title, valueField){
            // var graphList = [];
            var graph = new Object({
                // "type": "column",
                "alphaField": "alpha",
                "balloonText": "[[value]] %",
                "dashLengthField": "dashLength",
                "fillAlphas": 0.8,
                "fillColors": "#99CCFF",
                "lineColor": "#0066CC",
                "legendPeriodValueText": "total: [[value.sum]]",
                "legendValueText": "[[value]]",
                "labelText": "[[value]]",
                "labelPosition": "top",
                "showAllValueLabels": true,
                "color": "#3366CC",
                "fontSize": 15,
                "type": "column",
                "id": id,
                "title": title,
                "valueField": valueField,
                "listeners": [{
                    "event": "rendered", 
                    // "method": (e) => scope.fmLoading = false
                }],
                "valueAxis": "numOfRepairAxis"
              });
            // graphList.push(graph)
            return graph
        }


        chartOptions = {
            "type": "serial",
            "theme": "light",
            "autoMargins": true,
            // "marginRight": 40,
            "marginLeft": 60,
            // "marginBottom": 60,
            // "autoMarginOffset": 20,
            "mouseWheelZoomEnabled":true,
            "dataDateFormat": "YYYY-MM-DD JJ:NN:SS",
            "valueAxes": [{
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth":true,
                "title":"DUTOFFRATE %"
            }],
            "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
                "cursorAlpha": 0.1,
                "cursorColor":"#000000",
                "fullWidth":true,
                "valueBalloonsEnabled": false,
                "zoomable": true
            },
            "chartScrollbar": {
                "autoGridCount": true,
                "scrollbarHeight": 25
            },
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            // "legend": {
            //     "valueText":"",
            //     "horizontalGap": 6,
            //     "position": "right",
            //     "useGraphSettings": true,
            //     "markerSize": 6,
            //     "maxColumns": 6,
            // },
            "titles": [
                {"text": "DUT OFF RATE", "size": 30},{}
            ],
            "legend": {
              "equalWidths": false,
              "useGraphSettings": true,
              "valueAlign": "left",   
              "valueWidth": 120
            },
            "graphs": [],
            "chartCursor": {

            },
            "valueField": "DUTOFFRATE",
            "categoryField": "DATETIME",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true,
                "minPeriod":"ss"
            },
            "export": {
                "enabled": true
            },
            // "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
            //     "categoryBalloonDateFormat": "DD",
            //     "cursorAlpha": 0.1,
            //     "cursorColor":"#000000",
            //     "fullWidth":true,
            //     "valueBalloonsEnabled": false,
            //     "zoomable": false
            // },
            "categoryAxis": {
                "autoRotateCount": 8,
                "autoRotateAngle": -45
            },
            
            "dataProvider": []

        }

        var target = element[0].children[1];

        var chart = AmCharts.makeChart(target, chartOptions);
        chart.clickTimeout = 0;
        chart.lastClick = 0;
        chart.doubleClickDuration = 300;


        scope.$watchGroup(['data','groupBy', 'mainType'], function () {
            // var new_rows =_.chain(scope.data)
            // [
            //     {"name":"All",id:"all"},
            //     {"name":"客戶",id:"CUSTNAME"},
            //     // {"name":"客戶產品型號",id:"FAMILY"}
            // ]

            chart.events.clickGraphItem = [];
            // scope.nowClickCus = scope.imageType;

                scope.firstFieldList = [
                    {"name":"客戶",id:"CUSTNAME"},
                    {"name":"客戶產品型號",id:"BOARDNO"},
                    {"name":"BIB ID",id:"NEW_ASSSERIALNO"},
                    {"name":"舊配件編號",id:"ASSSERIALNO"}
                ]
                chart.mouseWheelZoomEnabled = false;
                if(scope.groupBy == "all"){
                    scope.groupBy = "CUSTNAME"
                }
                var new_rows = []
                function dorClickHandler(event) {
                    var ts = (new Date()).getTime();
                    scope.detectHaveClick++;
                    if(scope.groupBy == "NEW_ASSSERIALNO"){
                        scope.drillDownType = 3;
                    }else if(scope.groupBy == "BOARDNO"){
                        scope.drillDownType = 2;
                    }else if(scope.groupBy == "ASSSERIALNO"){
                        scope.drillDownType = 1;
                    }else{
                        scope.drillDownType = 0;
                    }
                    
                    if ((ts - chart.lastClick) < chart.doubleClickDuration) {  // [[ Double click ]]
                      
                      if (chart.clickTimeout) {
                        clearTimeout(chart.clickTimeout);
                      }
                      chart.lastClick = 0; // reset last click
                      doubleClickEvent(event);
                    } else {  // [[ Single click ]]
                        chart.clickTimeout = setTimeout(() => singleClickEvent(event), chart.doubleClickDuration);
                    }
                    chart.lastClick = ts;
                    
                    return;
                }
                chart.addListener("clickGraphItem", dorClickHandler);

                if((scope.groupBy == "CUSTNAME")){
                    
                    chart.categoryField  = "CUSTNAME";
                    chart.categoryAxis.parseDates = false;
                    chart.titles[1] = {"text": "Group By 客戶", "size": 15, "bold": false};
                    new_rows = _.chain(scope.data)
                    // .filter(row => row['TOTAL']>0)
                    .groupBy("CUSTNAME")
                    .map((r,k) => new Object({
                        "CUSTNAME": k,
                        'DUTOFFRATE':parseFloat(Number(100-(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100)).toFixed(1)),
                        'DUTONRATE':parseFloat(Number(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100).toFixed(1))
                        }))
                    .sortBy("DUTOFFRATE")
                    .reverse()
                    .value()
                    new_rows = _.filter(new_rows, function(a){return !_.isNaN(a.DUTOFFRATE) && a.DUTOFFRATE >= 0})
                    chart.dataProvider = new_rows
                    
                    if(scope.mainType == "0"){
                        chart.titles[0] = {"text": "DUT OFF RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutoffrate', "Dutoffrate", "DUTOFFRATE")]
                    }else{
                        chart.titles[0] = {"text": "DUT ON RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutonrate', "Dutonrate", "DUTONRATE")]
                    }
                    
                    chart.validateData();
                    chart.validateNow();
                }else if((scope.groupBy == "BOARDNO")){
                    chart.categoryField  = "BOARDNO";
                    chart.categoryAxis.parseDates = false;
                    chart.titles[1] = {"text": "Group By 客戶產品型號", "size": 15, "bold": false};
                    new_rows = _.chain(scope.data)
                    // .filter(row => row['TOTAL']>0)
                    .groupBy("BOARDNO")
                    .map((r,k) => new Object({
                        "BOARDNO": k,
                        'DUTOFFRATE':parseFloat(Number(100-(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100)).toFixed(1)),
                        'DUTONRATE':parseFloat(Number(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100).toFixed(1))
                        }))
                    .sortBy("DUTOFFRATE")
                    .reverse()
                    .value()
                    new_rows = _.filter(new_rows, function(a){return !_.isNaN(a.DUTOFFRATE) && a.DUTOFFRATE >= 0})
                    chart.dataProvider = new_rows
                    if(scope.mainType == "0"){
                        chart.titles[0] = {"text": "DUT OFF RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutoffrate', "Dutoffrate", "DUTOFFRATE")]
                    }else{
                        chart.titles[0] = {"text": "DUT ON RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutonrate', "Dutonrate", "DUTONRATE")]
                    }
                    chart.validateData();
                    chart.validateNow();
                }else if((scope.groupBy == "ASSSERIALNO")){
                    chart.categoryField  = "ASSSERIALNO";
                    chart.categoryAxis.parseDates = false;
                    chart.titles[1] = {"text": "Group By 舊配件編號", "size": 15, "bold": false};
                    new_rows = _.chain(scope.data)
                    // .filter(row => row['TOTAL']>0)
                    .groupBy("ASSSERIALNO")
                    .map((r,k) => new Object({
                        "ASSSERIALNO": k,
                        'DUTOFFRATE':parseFloat(Number(100-(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100)).toFixed(1)),
                        'DUTONRATE':parseFloat(Number(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100).toFixed(1))
                        }))
                    // .reverse()
                    .sortBy("DUTOFFRATE")
                    .reverse()
                    .value()
                    new_rows = _.filter(new_rows, function(a){return !_.isNaN(a.DUTOFFRATE) && a.DUTOFFRATE >= 0})
                    chart.dataProvider = new_rows
                    if(scope.mainType == "0"){
                        chart.titles[0] = {"text": "DUT OFF RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutoffrate', "Dutoffrate", "DUTOFFRATE")]
                    }else{
                        chart.titles[0] = {"text": "DUT ON RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutonrate', "Dutonrate", "DUTONRATE")]
                    }
                    chart.validateData();
                    chart.validateNow();
                }else if((scope.groupBy == "NEW_ASSSERIALNO")){
                    chart.categoryField  = "NEW_ASSSERIALNO";
                    chart.categoryAxis.parseDates = false;
                    chart.titles[1] = {"text": "Group By BIB ID", "size": 15, "bold": false};
                    new_rows = _.chain(scope.data)
                    // .filter(row => row['TOTAL']>0)
                    .groupBy("NEW_ASSSERIALNO")
                    .map((r,k) => new Object({
                        "NEW_ASSSERIALNO": k,
                        'DUTOFFRATE':parseFloat(Number(100-(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100)).toFixed(1)),
                        'DUTONRATE':parseFloat(Number(_.sumBy(r, 'SMAP_P')/_.sumBy(r, 'TOTAL_USED_SOCKET')*100).toFixed(1))
                        }))
                    // .reverse()
                    .sortBy("DUTOFFRATE")
                    .reverse()
                    .value()
                    new_rows = _.filter(new_rows, function(a){return !_.isNaN(a.DUTOFFRATE) && a.DUTOFFRATE >= 0})
                    chart.dataProvider = new_rows
                    if(scope.mainType == "0"){
                        chart.titles[0] = {"text": "DUT OFF RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutoffrate', "Dutoffrate", "DUTOFFRATE")]
                    }else{
                        chart.titles[0] = {"text": "DUT ON RATE", "size": 30};
                        chart.graphs = [genHistogramGraph('dutonrate', "Dutonrate", "DUTONRATE")]
                    }
                    chart.validateData();
                    chart.validateNow();
                }

            
        });
        
        var singleClickEvent = function(event){
            scope.nowClickCus = event.item.category;
            scope.clickType = "0"
            scope.$apply()
        };
        var doubleClickEvent = function(event){
            scope.nowClickCus = event.item.category;
            scope.clickType = "1"
            scope.$apply()
        };

       
        var updateChart = function(){
          if(_.isArray(scope.data)){
              // console.log(scope.data)
              chart.dataProvider = scope.data;
              chart.validateData();
              chart.validateNow();
          } 
        }

        // scope.getChart = function(){
        //   return chart;
        // }
        // chart.validateData();
        // chart.validateNow();

      }
    };
  };
  
  angular.module('mean.dutoffrate').directive('dorDataChart', dorDataChart);
  dorDataChart.$inject = ["_", '$http', "$q"];
  // stDataChart.$inject = ['mtbfQueryService', 'mtbfTabService'];
})();