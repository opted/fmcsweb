#!/bin/bash
echo "produce new package" $1
cp -r sample $1
mv $1/public/assets/css/sample.css "$1/public/assets/css/$1.css"
mv $1/public/routes/sample.js "$1/public/routes/$1.js"
mv $1/public/controllers/sample.js "$1/public/controllers/$1.js"
mv $1/server/controllers/sampleQuery.js "$1/server/controllers/$1Query.js"
mv $1/server/routes/sample.js "$1/server/routes/$1.js"
fc=`echo $1| tr '[a-z]' '[A-Z]'`
len=`echo $1| wc -c`-1
cap=${fc:0:1}${1:1:$len}
grep -rl "sample" $1 | xargs -i sed -i "s/sample/$1/g" {}
grep -rl "Sample" $1 | xargs -i sed -i "s/Sample/$cap/g" {}
