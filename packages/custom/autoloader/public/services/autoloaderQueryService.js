(function(){
  'use strict';
  function autoloaderQueryService(_, $http, $window){
    var isSearching = false;
    var status = {
      str: "",
      err: ""
    };

    var  encode_utf8=function(s){
      return decodeURIComponent( encodeURIComponent(s));
    }

    var config = {
      eventHandlers: {
      progress: function(event){
        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
        status.str = "正在下載資料...";
      },
      // readystatechange: function(event){
      //   console.log("STATUS:", event);
      // }
      }
    };
    return {
      isSearching: () => isSearching,
      status: status,
      uploadErrorConfig: function(uploadData,errorType, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        var processedArr=[]
        if(errorType =='PPLU600'){
          _.forEach(uploadData.data,function(r){
                var newKeyObj=_.mapKeys(r,function(v,k){
                  var newKey ="";
                  if(k=='訊息分類')
                    newKey = 'MSG_CLASS';
                  if(k=='錯誤碼')
                    newKey = 'ERR_NO';
                  if(k=="錯誤訊息/訊息")
                    newKey = 'MSG_TYPE';
                  if(k=='錯誤編碼 錯誤訊息及排除')
                    newKey = 'ERR_DESC';
                 
                  return  newKey
                  
               })
                newKeyObj['TYPE']=errorType
                processedArr.push(newKeyObj)
            })
        }
        else{//(errorType =='PPLU800')
          _.forEach(uploadData.data,function(r){
                var newKeyObj=_.mapKeys(r,function(v,k){
                  var newKey ="";
                  if(k=='訊息分類')
                    newKey = 'MSG_CLASS';
                  if(k=='NO')
                    newKey = 'ERR_NO';
                  if(k=="錯誤訊息/訊息")
                    newKey = 'MSG_TYPE';
                  if(k=='中文訊息')
                    newKey = 'ERR_DESC';
                 
                  return  newKey
                  
               })
                newKeyObj['TYPE']=errorType
                processedArr.push(newKeyObj)
            })
        }
        var queryParam=new Object({'data':processedArr,'errType':errorType})
        // console.log(queryParam)
        return $http.post('/api/autoloader/uploaderrorconfig', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          // var rows=[];
          // _.forEach(result.data,function(ovenInfo){
          //   rows = _.concat(rows,ovenInfo)
          // })
          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },

      getErrorConfig: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/autoloader/geterrorconfig', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);
          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },

      query: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        // console.log(queryParam)
        return $http.post('/api/autoloader/geterrordata', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";

          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
    };
  };

  angular.module('mean.autoloader').factory('autoloaderQueryService', autoloaderQueryService);
  autoloaderQueryService.$inject = ["_", "$http", "$window"];
})();