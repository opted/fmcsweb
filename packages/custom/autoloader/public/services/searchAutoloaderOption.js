(function(){
    'use strict';
    function srchALOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        var typeArray=[null,'PPLU600','PPLU800','TBPP14000']
        var typeField = uOpt.genField("機型", "TYPE", "string", false, false);
        typeField.filteredData = typeArray;
        
        var msgTypeArray = [null,'錯誤訊息','訊息'];
        var msgTypeField = uOpt.genField("訊息分類", "MSG_TYPE", "string", false, false);
        msgTypeField.filteredData = msgTypeArray;
        return {
            fields: [
                typeField,
                uOpt.genField("內批(Lotno)", "LOTNO"),
                uOpt.genField("機台編號", "OVENID"),
                msgTypeField,
                uOpt.genField("中分類", "MSG_CLASS"),
                
            ],
            getMatch: uOpt.getMatch
                    };
                        };

    angular.module('mean.autoloader').factory('srchALOpt', srchALOpt);
        srchALOpt.$inject = ['utilsSrchOpt'];
})();
