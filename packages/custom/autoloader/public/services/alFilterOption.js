(function() {
    'use strict';

    function alFilterOptionService(_) {
        var filterOption = {
   //          "group1": [
			// 	[],//left Column
			// 	[],			
			// ]
        };

        return {
            filterOption: filterOption,
            genFilterOption: function(errorList, callback){
                // console.log(errorList)
                var newFilter={
                    "PPLU600": [ [],[] ],
                    "PPLU800": [ [],[] ],
                    "TBPP14000": [ [],[] ]
                }
                _.forEach(errorList,function(errObj,idx){
                    var checkBoxObj = { "name": errObj['ERR_DESC'],
                                        "TYPE": errObj['TYPE'] ,  
                                        "ERR_NO":errObj['ERR_NO']
                                        ,"checked": false }
                    newFilter[errObj['TYPE']][idx%2].push(checkBoxObj)
                })

   
                return newFilter
            },
        }
    }

    angular
        .module('mean.autoloader')
        .factory('alFilterOptionService', alFilterOptionService);

    alFilterOptionService.$inject = ['_'];

})();
