(function() {
  'use strict';

  function Autoloader($stateProvider) {
    $stateProvider.state('autoloader main page', {
      url: '/autoloader',
      templateUrl: 'autoloader/views/index.html',
      controller:AutoloaderController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.autoloader')
    .config(Autoloader);

  Autoloader.$inject = ['$stateProvider'];

})();
