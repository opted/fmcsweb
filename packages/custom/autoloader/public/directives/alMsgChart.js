(function(){
	'use strict';

	function alMsgChart(_,aqs){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				showList:'=',
				titleName:'=',
				groupKey:'=',
				secondGroupKey:'=',
				groupValue:'=',
				mode:'='
			},
			template: '<div style="width: 100%;height: 300px;"><div></div></div>',
			         
			link: function (scope, element, attrs) {

				scope.graphs =[
					{   
				    "fillAlphas": 1,
				    "lineColor":'#2B5F75',
				    "type": "column",
				    "valueField": "msgA",
				    "title":"訊息",
				    "valueAxis": "v1"
				  },
				  {   
				    "fillAlphas": 1,
				    "lineColor":'#9F353A',
				    "type": "column",
				    "title":"錯誤訊息",
				    "valueField": "msgB",
				    "valueAxis": "v1"
				  }
				];
 				scope.xdownGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
				    "fillAlphas": 0,
				    "lineAlpha": 1,
					"maximum": 100,
					"minimum": 0,
				    "valueField": "XDOWNRATE",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

 				scope.failGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
				    "fillAlphas": 0,
				    "lineAlpha": 1,
					"maximum": 100,
					"minimum": 0,
				    "valueField": "percentFails",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

				scope.drillLevel = 0;
				var chartOptions;
				chartOptions = {
					 "type": "serial",
					  "theme": "light",
					  "autoMargins": false,
					  "marginLeft": 80,
					  "marginRight": 80,
					  "marginTop": 10,
					  "marginBottom": 80,
					  "dataProvider": [ 

					  ],
					  "valueAxes": [{
						"id": "v1",
						"axisAlpha": 0,
						"position": "left",
						"ignoreAxisWidth":true,
						"integersOnly":true,
						"title":"異常次數"
						}],
					  "legend": {
					    "useGraphSettings": true,
					    "postion":'top'
					  },
					  "startDuration": 1,
					  "graphs": scope.graphs,
					  "titles":[{text: "",size:15}],
						"chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
						// "categoryBalloonDateFormat": "DD",
						"cursorAlpha": 0.1,
						"cursorColor":"#000000",
						"fullWidth":true,
						"valueBalloonsEnabled": false,
					"zoomable": true
					          },
					  "categoryField": 'DATETIME',
					  "categoryAxis": {
					    "gridPosition": "start",
					    "axisAlpha": 1,
									}
					}

				var target = element[0].children[0];
				var chart = AmCharts.makeChart(target, chartOptions);

				scope.$watch('data', function (nv,ov) {
					if(nv != ov && _.isArray(nv)) {
						scope.drillLevel=0;
						chart.titles[0].text="";
						updateChart();
					}
				});

				var processData = function(dataArr,secondGroupKey){
					var outputArr=[]
					var processRecord={}

					_(dataArr).sortBy('DATETIME').forEach(function(errInfo){
						var groupName;

						//統計每日的訊息、錯誤訊息數量
						groupName = dateGroupBy(errInfo)
						_.update(processRecord,groupName,function(e){
							if(e!=undefined){
								e['count']+=1;
								if(errInfo['MSG_TYPE']=="錯誤訊息"){
									e['msgB'] +=1;
								}
								else if(errInfo['MSG_TYPE']=="訊息")	{
									e['msgA'] +=1;
								} 
								return e
							}
							else{
								var recordObj ={};
								recordObj['msgB'] =0;
								recordObj['msgA'] =0;
								recordObj['count']=1;
								if(errInfo['MSG_TYPE']=="錯誤訊息"){
									recordObj['msgB'] =1;
								}
								else if(errInfo['MSG_TYPE']=="訊息")	{
									recordObj['msgA'] =1;
								}

								return recordObj;
							}
						})

					})

					_.forOwn(processRecord,function(v,k){
						v['DATETIME'] = k;
						outputArr.push(v)

					})
					return outputArr
				}

	

				var updateChart = function(){

					if(_.isArray(scope.data)){

						chart.dataProvider = processData(_.cloneDeep(scope.data));
						chart.categoryField='DATETIME';
						chart.validateData();
						chart.validateNow();
				
					}
				}


				scope.getChart = function(){
					return chart;
				}

        	var customGroupBy = function(row,key){
        		if(key=='DATETIME'){ return row[key].slice(5,10)}
        		return row[key]
        	}

        	var dateGroupBy = function(row){ 
       	    	return row['DATETIME'].slice(5,10)
        	}


				updateChart()
				chart.validateData();
				chart.validateNow();
			}
		};
	};

	angular.module('mean.autoloader').directive('alMsgChart', alMsgChart);
	alMsgChart.$inject = ['_','autoloaderQueryService'];
})();