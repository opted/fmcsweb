(function(){
	'use strict';

	function alErrorChart(_,aqs){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				showList:'=',
				titleName:'=',
				groupKey:'=',
				secondGroupKey:'=',
				groupValue:'=',
				mode:'='
			},
			template: '<div id="err" style="width: 100%;height: 300px;"><div></div></div>',
			         
			link: function (scope, element, attrs) {

				scope.graphs =[{   
							    // "balloonText": "<span style='font-size:12px;'>[[title]] in [[category]]:<br><span style='font-size:20px;'>[[value]]</span> [[additional]]</span>",
							    "fillAlphas": 1,
							    "type": "column",
							    "valueField": "count",
							    "valueAxis": "v1"
							  }];
 				scope.xdownGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
				    "fillAlphas": 0,
				    "lineAlpha": 1,
					"maximum": 100,
					"minimum": 0,
				    "valueField": "XDOWNRATE",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

 				scope.failGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
				    "fillAlphas": 0,
				    "lineAlpha": 1,
					"maximum": 100,
					"minimum": 0,
				    "valueField": "percentFails",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

				scope.drillLevel = 0;
				var chartOptions;
				chartOptions = {
					 "type": "serial",
					  "theme": "light",
					  "autoMargins": false,
					  "marginLeft": 80,
					  "marginRight": 80,
					  "marginTop": 10,
					  "marginBottom": 80,
					  "dataProvider": [ 

					  ],
					  "valueAxes": [{
						"id": "v1",
						"axisAlpha": 0,
						"position": "left",
						"ignoreAxisWidth":true,
						"integersOnly":true,
						"title":"異常次數"
						}],
					  "startDuration": 1,
					  "graphs": [],
					  "titles":[{text: "",size:15}],
						"chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
						// "categoryBalloonDateFormat": "DD",
						"cursorAlpha": 0.1,
						"cursorColor":"#000000",
						"fullWidth":true,
						"valueBalloonsEnabled": false,
					"zoomable": true
					          },
					  "categoryField": scope.groupKey,
					  "categoryAxis": {
					    "gridPosition": "start",
					    "axisAlpha": 1,
					    "autoRotateCount":5,
						"autoRotateAngle":30
									}
					}

				var target = element[0].children[0];
				var chart = AmCharts.makeChart(target, chartOptions);

                chart.clickTimeout = 0;
                chart.lastClick = 0;
                chart.doubleClickDuration = 300;

				function clickHandler(event) {
	              var ts = (new Date()).getTime();
	              // if ((ts - chart.lastClick) < chart.doubleClickDuration) {  // [[ Double click ]]
	              //   if (chart.clickTimeout) {
	              //     clearTimeout(chart.clickTimeout);
	              //   }
	              //   chart.lastClick = 0; // reset last click
	              //   	drillDown(event);
	              // } else {  // [[ Single click ]]
	                chart.clickTimeout = setTimeout(() => genChart(event), chart.doubleClickDuration);
	              // }
	              chart.lastClick = ts;
	            }

	            chart.addListener("clickGraphItem", clickHandler);

				var genGraphs = function(data){
					var graphs =[]; 
					_(data)
					.map('OVENID')
					.uniq()
					.forEach(function(oid){
					  var g = new Object({
									    "fillAlphas": 1,
									    "type": "column",
									    "valueField": oid,
									    "valueAxis": "v1",
									    "labelText": oid,
									    "labelPosition":"top"
									  })
						graphs.push( g)  
					  })
					return graphs;
				}

				scope.$watch('data', function (nv,ov) {
					if(nv != ov && _.isArray(nv)) {
						// console.log(scope.data)
						scope.drillLevel=0;
						chart.titles[0].text="";
						chart.graphs = genGraphs(scope.data)
						updateChart();
					}
				});


				var processData = function(dataArr,secondGroupKey){
					// console.log(dataArr.length)
					var outputArr=[]
					var processRecord={}

					_.forEach(dataArr,function(errInfo){
						var groupName ;

						groupName = errInfo['ERR_DESC']
						_.update(processRecord,groupName,function(e){
							if(e!=undefined){
								e['count']+=1;
								e[errInfo['OVENID']] = (e[errInfo['OVENID']]||0)+1; 
								return e
							}
							else{
								var recordObj ={};
								recordObj['count']=1;
								recordObj[errInfo['OVENID']]=1;
								recordObj['ERR_NO'] = errInfo['ERR_NO']
								return recordObj;
							}
						})

					})

					_.forOwn(processRecord,function(v,k){
						v['ERR_DESC'] = k;
						outputArr.push(v)

					})
					outputArr.sort(function(a, b){return b['count']-a['count']})
					return outputArr
				}

	

				var updateChart = function(){

					if(_.isArray(scope.data)){

						chart.dataProvider = processData(_.cloneDeep(scope.data));
						chart.categoryField='ERR_DESC';
						chart.graphs=genGraphs(scope.data)
						chart.titles[0].text="";
						chart.validateData();
						chart.validateNow();
				
					}
				}


				scope.getChart = function(){
					return chart;
				}
        function genChart(event){
        	
          // 設定所選的 group
          var filterOven = event.item.graph.valueField;
          if(scope.drillLevel==0){
          	scope.drillLevel=1;
		    chart.dataProvider= _(scope.data)
		    .filter(e => e.OVENID == filterOven)
		    .groupBy(dateGroupBy)
			.map(function(r,d){
			   var o = new Object();
			   o['CREATEDATE'] = d;
			   o['count'] = r.length||0;
					   return o
			}).value()

			chart.categoryField = 'CREATEDATE';
			chart.titles[0].text = filterOven +' '+ event.item.category +' 異常次數'
			chart.graphs=scope.graphs;

			chart.validateData();
			chart.validateNow();
			}
			else {
				scope.drillLevel=0;
				chart.graphs = genGraphs(scope.data)
				updateChart()
			}
        }

        	var customGroupBy = function(row,key){
        		if(key=='DATETIME'){ return row[key].slice(5,10)}
        		return row[key]
        	}

        	var dateGroupBy = function(row){
       	    	return  row['DATETIME'].slice(5,10)
        	}

				updateChart()
				chart.validateData();
				chart.validateNow();
			}
		};
	};

	angular.module('mean.autoloader').directive('alErrorChart', alErrorChart);
	alErrorChart.$inject = ['_','autoloaderQueryService'];
})();
