(function(){
	'use strict';

	function alChart(_,aqs){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				showList:'=',
				titleName:'=',
				groupKey:'=',
				secondGroupKey:'=',
				groupValue:'=',
				mode:'='
			},
			template: '<div style="width: 100%;height: 500px;"><div></div></div>',
			link: function (scope, element, attrs) {

 				scope.failGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
                	"legendValueText": "[[value]]%",
            		"labelText": "[[value]]%",
				    "fillAlphas": 0,
				    "lineAlpha": 1,
					"maximum": 100,
					"minimum": 0,
				    "valueField": "percentFails",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

				var chartOptions;
				chartOptions = {
				 "type": "serial",
				  "theme": "light",
				  "autoMargins": false,
				  "marginLeft": 80,
				  "marginRight": 80,
				  "marginTop": 10,
				  "marginBottom": 80,
				  "dataProvider": [ 

				  ],
				  "valueAxes": [{
					"id": "v1",
					"axisAlpha": 0,
					"position": "left",
					"ignoreAxisWidth":true,
					"integersOnly":true,
					"title":"異常次數"
					},
					{
					"id": "v2",
					"axisAlpha": 0,
					"position": "right",
					"gridAlpha":0,
					"ignoreAxisWidth":true,
					"title":"Fail Rate",
					"unit":"%"
									}],
				  "startDuration": 1,
				  "graphs": scope.failGraphs,
				  "titles":[{text: "",size:15}],
					"chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
					// "categoryBalloonDateFormat": "DD",
					"cursorAlpha": 0.1,
					"cursorColor":"#000000",
					"fullWidth":true,
					"valueBalloonsEnabled": false,
				"zoomable": true
				          },
				  "categoryField": 'ERR_DESC',
				  "categoryAxis": {
				    "gridPosition": "start",
				    "axisAlpha": 1,
				    "autoRotateCount":10,
				    "autoRotateAngle":30
					}
				}

				var target = element[0].children[0];

				var chart = AmCharts.makeChart(target, chartOptions);

				scope.$watch('data', function (nv,ov) {
					if(nv != ov) {
						updateChart();
					}
				});

				var processData = function(dataArr){
					var outputArr=[]
					var processRecord={}
					var totalFailCount=dataArr.length;
					_.forEach(dataArr,function(oInfo){
						var groupName = _.replace(oInfo['ERR_DESC'],/[\]\[.]/g,"")
							_.update(processRecord,groupName,function(o){
							if(o!=undefined){
								o['count']+=1;
								// o['DATEDELTA'] += oInfo['DATEDELTA']
								return o
							}
							else{
								var recordObj ={};
								recordObj['count']=1;
								return recordObj;
							}
						})

					})

					_.forOwn(processRecord,function(v,k){
						v['ERR_DESC'] = k;
						outputArr.push(v)
					})
					// if(scope.groupKey !='DATETIME')
					outputArr.sort(function(a, b){return b['count']-a['count']})

					var accumulationPFail = 0;
					_.forEach(outputArr,function(e){
						accumulationPFail += ((e['count'] / totalFailCount) * 100);
						e['percentFails'] = accumulationPFail.toFixed(2);
					})
					return outputArr
				}



				var updateChart = function(filterData){

					if(_.isArray(scope.data)){

						chart.dataProvider = processData(_.cloneDeep(scope.data));
					}
					chart.categoryField='ERR_DESC';
					chart.validateData();
					chart.validateNow();
				}

				var groupDataByKey = function(groupKey){
					if(groupKey=='DATETIME'){
						chart.dataProvider = _(scope.filterData)
							.groupBy(function(r){
								return customGroupBy(r,groupKey)
							})
							.map(function(r,k){
								var o =new Object({
							   'count':_.countBy(r,function(row){
       								return customGroupBy(row,groupKey)
									})[k]
								})
								o[groupKey]=k;
								return o
							})
							.value()
					}
					else{
					chart.dataProvider = _(scope.filterData)
							.groupBy('ERR_DESC')
							.map(function(r,k){
								var o =new Object({
							   'count':_.countBy(r,groupKey)[k]
								})	
								o[groupKey]=k;
								return o
							})
							.value()
					}		
					chart.categoryField =groupKey
					chart.graphs=scope.graphs;
					chart.validateData();
					chart.validateNow();
				}


				scope.getChart = function(){
					return chart;
				}

        	var customGroupBy = function(row,key){
        		if(key=='DATETIME'){ return row[key].slice(5,10)}
        		return row[key]
        	}

        	var dateGroupBy = function(row){
        	    var sd = (new Date(row['DATETIME'])).setHours(6,50,0)
        	    var d = new Date(row['DATETIME'])

       	    	return (d>sd)? d.toLocaleDateString().slice(5,10) 
       	    				: (new Date(sd-86400000)).toLocaleDateString().slice(5,10)  
        	}


				updateChart()
				chart.validateData();
				chart.validateNow();
			}
		};
	};

	angular.module('mean.autoloader').directive('alChart', alChart);
	alChart.$inject = ['_','autoloaderQueryService'];
})();
'use strict';

