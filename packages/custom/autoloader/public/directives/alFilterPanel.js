(function() {
	'use strict';

	function alFilterPanel() {
		return {
			restrict: 'E',
			scope: {
				filterOption: '=',
				isVisible: '=',
				errorsList:'='
			},
			templateUrl: 'autoloader/views/alFilterPanel.html',
			link: function(scope) {
				scope.toggleShow = function() {
					scope.isVisible = !(scope.isVisible);
					console.log(scope.filterOption)
					scope.errorsList =_.filter(scope.errorsList,function(e){
							return e['checked']==true})
				}
				scope.addToList = function(item){
					if(item.name=="全選")
					{
						_.forOwn(scope.filterOption, group => 
							_.forOwn(group, checkboxList => 
								_.forEach(checkboxList, function(box){
									box.checked=item.checked;
								} )))
					}
					else{
						var match = _.find(scope.errorsList,function(e){
							return (e['ERR_NO'] == item['ERR_NO'])
									&&(e['TYPE'] == item['TYPE'])
									&&(e['name'] == item['name']) 
						})
						if(match==undefined)
							scope.errorsList.push(item)
					} 
				}
				scope.getErrorsList = function(){
					return scope.errorsList
				}
			}
		};
	}

	angular
		.module('mean.autoloader')
		.directive('alFilterPanel', alFilterPanel);

	alFilterPanel.$inject = [];

})();