var reportOpenFile = undefined;
(function(){
  'use strict';

  function errorConfigModal(_,$http,$uibModal,MeanUser,aqs){
    return {
        restrict: 'E',
        scope: {
          btnStyle: '@',
          readyToUpload:'=',
          sheetData:'=',
          errorType:'='
        },
        template: '<button type="button" class="btn btn-default" style="{{btnStyle}}" ng-click="errorConfigModal()">上傳 Error Configure</button>', 
        link: function (scope, element, attrs) {
          
        },//link

        link: function(scope, element, attrs){

        scope.errorConfigGrid = {
            columnDefs: [
                {headerName: "機型", field: "TYPE", width: 80},
                {headerName: "錯誤碼", field: "ERR_NO", width: 100},
                {headerName: "訊息分類", field: "MSG_TYPE", width: 100},
                {headerName: "中分類", field: "MSG_CLASS", width: 120},
                {headerName: "錯誤訊息及排除", field: "ERR_DESC", width: 400}
            ],
            rowData:[],
            groupUseEntireRow: false,
            enableSorting: true,
            enableColResize: true,
            suppressExcelExport :false
        };
        // 

          // console.log(scope.sheetData)
          scope.errorConfigModal = function(){
            scope.readyToUpload = _.isEmpty(scope.sheetData)? false:true;
            scope.uploadStatus = scope.readyToUpload ? '準備上傳 '+scope.errorType.name:'請選擇欲上傳檔案';
            aqs.getErrorConfig({}).then(function(result){
              // console.log(result)
              scope.errorConfigGrid.api.setRowData(result);
              // scope.errorConfigGrid.rowData = result.data
            })

            scope.modalInstance = $uibModal.open({
            templateUrl: 'autoloader/views/errorConfigModal.html',
            size: 'xl',
            scope: scope
            });
        };

        scope.getSheetData = function(){

            aqs.uploadErrorConfig(scope.sheetData,scope.errorType.name).then(function(result){
              // console.log(result)
              aqs.getErrorConfig({}).then(function(errConfig){
              // console.log(errConfig)
              scope.errorConfigGrid.api.setRowData(errConfig);
              scope.uploadStatus='上傳成功'
              // scope.errorConfigGrid.rowData = result.data
            })

            })
        }

        scope.close = function(){
          scope.uploadStatus = undefined;
          scope.readyToUpload=false;
          scope.modalInstance.dismiss('cancel');
        }
        if(MeanUser.userInfo())
          scope.username = MeanUser.userInfo().local.name;
        else
          scope.username = undefined;

      }
    };
  };

  angular.module('mean.autoloader').directive('errorConfigModal', errorConfigModal);
  errorConfigModal.$inject = ['_','$http','$uibModal','MeanUser','autoloaderQueryService'];
})();