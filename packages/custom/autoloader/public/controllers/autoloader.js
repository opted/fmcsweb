'use strict';
var ted;
function AutoloaderController(_, $scope,$http,srchALOpt,aqs,afs) {
    $scope.typeSelected;
    $scope.selected = {
        "from": getToday(),  // Set default start day
        "to": getToday()  // Set default end day
    };
    $scope.changeType = function(type){
        $scope.typeSelected = type.name;
    }
    
    angular.element(document).ready(function () {
       aqs.getErrorConfig().then(function(errorConfig){
           $scope.errorConfig=errorConfig
        })
    });

    $scope.errorsList=[]

    $scope.errorType = {name:"PPLU600"};
    $scope.setErrotType=function(errorType){
        if($scope.errorType.name != errorType.name)
            $scope.errorType.name = errorType.name;      
    }
    ted = $scope;

    $scope.fields = srchALOpt.fields;
    $scope.fieldSearchURL = "/api/autoloader/getsearchfield";
    $scope.autoloaderGrid = {
      columnDefs:  [     
        {headerName: "Date Time", field: "DATETIME", width: 120},
            {headerName: "LotNo", field: "LOTNO", width: 120},
            {headerName: "機型", field: "TYPE", width: 80},
            {headerName: "機台編號", field: "OVENID", width: 80},
            {headerName: "錯誤碼", field: "ERR_NO", width: 80},
            {headerName: "訊息分類", field: "MSG_TYPE", width: 120},
            {headerName: "中分類", field: "MSG_CLASS", width: 120},
            {headerName: "訊息描述", field: "ERR_DESC", width: 400},
            ],
      rowData:[],
      groupUseEntireRow: false,
      enableSorting: true,
      enableColResize: true
   };
    $scope.sheetData;



    $scope.ok = function() {

        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }
        if($scope.typeSelected)
          queryParam.TYPE=$scope.typeSelected
        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        _.assign(queryParam, srchALOpt.getMatch($scope.fields));

        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };

      $scope.requestStatus = "等待 Server 回應...";
      var startTime = new Date();
      $scope.queryTime = "";
      $scope.rowNum = undefined;
      $scope.isSearching = true;
      aqs.query(queryParam)
          .then(function(result){

            // $scope.rawData = result.sort(function(a,b){return a['DATETIME']-b['DATETIME']})
            $scope.rawData = _.sortBy(result,'DATETIME')
            _.forEach($scope.rawData,function(r){
              var match = _.find($scope.errorConfig,function(conf){
                return (conf.ERR_NO == r.ERR_NO && conf.TYPE == r.TYPE)
              })
              if(match != undefined){
                _.assign(r,match)
              }
            })
            // Filter MSG_CLASS and MSG_TYPE here. Filter TYPE,LOTNO,OVEN at backend 
            $scope.rows = (queryParam.MSG_CLASS==undefined && queryParam.MSG_TYPE==undefined )? $scope.rawData: 
            						_.filter($scope.rawData,_.omit(queryParam,['from','to']));
            $scope.rowNum=$scope.rows.length
            $scope.autoloaderGrid.api.setRowData($scope.rows)
            $scope.chartData =  $scope.rows;
            $scope.requestStatus = "查詢完成";
            $scope.queryTime = new Date() - startTime;

            // gen Error List by data in each query
            var filterErrorList = _($scope.rows)
                                      .orderBy(['TYPE','ERR_NO'])
                                      .groupBy('ERR_DESC')
                                      .map(function(v,k){
                                        var o = new Object()
                                        var tempError = v[0];
                                        o['ERR_DESC'] = k;
                                        o['TYPE'] = tempError['TYPE']
                                        o['ERR_NO'] = tempError['ERR_NO']
                                        return o 
                                    }).value()

            $scope.filterOption=afs.genFilterOption(filterErrorList)
          },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              console.log(err)
          }).finally(function(){
              $scope.isSearching = false;
          });

    };

    // $scope.filterOption = afs.filterOption;
    $scope.isFilterPanelVisible = false;
    $scope.toggleFilterPanelShow = function() {
        $scope.isFilterPanelVisible = !($scope.isFilterPanelVisible);
        var refreshErrorsList=[]
        _.forOwn($scope.filterOption,function(errType){
          _.forEach(errType,function(errDesc){
             _.forEach(errDesc,function(c){
                if(c['checked'])
                  refreshErrorsList.push(c)
           })
          })
        })
        $scope.errorsList = refreshErrorsList;

    };

    $scope.$watch('isFilterPanelVisible',function(newVal,oldVal){
      //更新特定錯誤訊息圖表的資料
      if(newVal == false && oldVal == true){
        $scope.errorData=_($scope.rows)
        .groupBy('TYPE')
        .map(function(rows,t){
           var errnoList=_($scope.errorsList)
            .filter({'TYPE':t})
            .map('ERR_NO').value()
          return  _.filter(rows,r=> _.includes(errnoList,r['ERR_NO']))
        }).flatten().value()
      }
    })

    $scope.export = function(){
    // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
    if ($scope.autoloaderGrid.api.getModel().getRowCount() < 1){
      $window.alert("無資料可供下載");
      return;
    }
    var params = {
      fileName: "Autoloader_Export.xls"
    };
    $scope.autoloaderGrid.api.exportDataAsExcel(params);
  };

};

angular
  .module('mean.autoloader')
  .controller('AutoloaderController', AutoloaderController);

AutoloaderController.$inject = ['_', '$scope', '$http','srchALOpt','autoloaderQueryService','alFilterOptionService'];
