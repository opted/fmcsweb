(function() {
  'use strict';
  module.exports = function(Autoloader, app, auth, database, utils) {
    var AutoloaderManager = require('../controllers/autoloaderQuery');
    var autoloaderMgr = new AutoloaderManager(utils);

    app.post('/api/autoloader/getsearchfield', function(req, res, next){
      if(req.body.field =='OVENID'|| req.body.field =='LOTNO'){ // autoloader error log
        utils.ctrls.mongoQuery.getSearchField(autoloaderMgr.coll, req.body, autoloaderMgr.pathMapping, autoloaderMgr.arrayFields)
        .then((data) => res.json(data))
        .catch((err) => res.status(500).send({'error': err}));
      }
      else{    // erro config  
        utils.ctrls.mongoQuery.getSearchField(autoloaderMgr.configColl, req.body, autoloaderMgr.configPathMapping,[])
        .then((data) => res.json(data))
        .catch((err) => res.status(500).send({'error': err}));
      }
    });

    app.post('/api/autoloader/geterrordata', function(req, res, next) {
      autoloaderMgr.getErrorData(req.body, res, next)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));;
    });

    app.post('/api/autoloader/geterrorconfig', function(req, res, next) {
      autoloaderMgr.getErrorConfig(req.body, res, next)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));;
    });
 
    app.post('/api/autoloader/uploaderrorconfig', function(req, res, next) {
      autoloaderMgr.uploadErrorConfig(req.body, res, next)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));;
  });
 };
})();
