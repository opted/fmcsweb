'use strict'
var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');
// exports.getBackEndResponse = function(requestBody) {
//   var deferred = Q.defer();
//   console.log('test query from:', requestBody.packageName, 'package');
//   deferred.resolve();
//   return deferred.promise;
// };

var AL = mongoose.model('AL');
var ALErrorConfig = mongoose.model('ALErrorConfig');

function addHours(queryDateTime){
  var tempDate = new Date(queryDateTime)
  return tempDate.setHours(tempDate.getHours()+8);
}
function substractHours(queryDateTime){
  var tempDate = new Date(queryDateTime)
  return tempDate.setHours(tempDate.getHours()-8);
}
function encode_utf8(s){
      return decodeURIComponent( encodeURIComponent(s));
}


class AutoloaderManager {
  constructor(utils) {
    this.coll = AL;
    this.configColl = ALErrorConfig;
    this.utils = utils;
    this.pathMapping = {
      'OVENID':'OVENID',
      'LOTNO':'LOTNO'
    };
    this.configPathMapping = {
      'MSG_CLASS':'MSG_CLASS',
    };
    this.arrayFields = ['ERRORS'];
  }

  getErrorConfig(params){
    var df = Q.defer();
    try {

      ALErrorConfig.find({},{'_id':0}, function(err, ALErrorConfigues){
          if(err){
            console.log(err.stack);
            df.reject(err.stack);
          } else{
            df.resolve(ALErrorConfigues);
          }
      })
      
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }

  getErrorData(params){
    var df = Q.defer();
    try {
      var query = params;
      var match = {},matchFilter={};
      var pipe = [{'$unwind': "$ERRORS"}];

      var dateRange = {'ERR_DATETIME':{}};
      if (query.from !== null){
        dateRange.ERR_DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.ERR_DATETIME['$lt'] = new Date(addHours(query.to));
      }

      if (!_.isEmpty(dateRange.ERR_DATETIME)){
        // _.assign(match['ERRORS.ERR_DATETIME'], dateRange);
        match= {'ERRORS.ERR_DATETIME':dateRange.ERR_DATETIME}
      }
      _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['OVENID']), this.pathMapping));      
      pipe.push({'$match': match});
      if(query.TYPE !== undefined){
        _.assign(matchFilter,{'TYPE':query.TYPE});
      }
      if(_.keys(matchFilter).length>0){
        pipe.push({'$match': matchFilter});
      }
      pipe.push({
        '$project': {
          '_id': 0,
          'DATETIME': {
            '$dateToString': { 'format': '%Y-%m-%d %H:%M:%S', 'date': '$ERRORS.ERR_DATETIME' }
          },
          'ISODATE':'$ERRORS.ERR_DATETIME',
          'ERR_DESC': '$ERRORS.ERR_DESC',
          'ERR_NO':'$ERRORS.ERR_NO',
          'LOTNO':1,
          'OVENID': 1,
          'TYPE':1,
          'LOTLIST':1
        }}
      );
      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(AL, pipe)
      .then(function(result){
        try {
            // match by time
             _.forEach(result,function(r){
            if(r['TYPE']=='TBPP14000' && !(_.isEmpty(r['LOTLIST']))){
              var match_lot = _.find(r['LOTLIST'],function(lotTime){
                   return (lotTime.END_TIME >=r['ISODATE']  
                            && lotTime.START_TIME<=r['ISODATE']);
                })
              if(match_lot!=undefined) { r['LOTNO']=match_lot['LOTNO'];}
            }
            
          })
            // TBPP14000 LOTNO bind after mongo query, so filter here
             if(query.LOTNO!=undefined){
             	df.resolve(  _.filter(result,{"LOTNO":query.LOTNO}) );
             }
             else {  
             	df.resolve(result);  
             }
        } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
        }
      })
      
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }

  deleteAllErrorConfig(type){
    var df = Q.defer();
    
    try {
         ALErrorConfig.collection.remove({'TYPE': type})
         .then(function(result){
            df.resolve(result);
         })

    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }


  uploadErrorConfig(params){
    var df = Q.defer();
    var queryList=[];
    this.deleteAllErrorConfig(params.errType).then(function(removeReult){

      try {
	      for( var i = 0  ; i < params.data.length ; i++ ){
	        var type = params.data[i].TYPE;
	        var errno = params.data[i].ERR_NO;
	        queryList.push(ALErrorConfig.collection.findOneAndReplace({'TYPE': type,'ERR_NO':errno}, params.data[i], {upsert: true}));          
	  	  }

	  	  Q.all(queryList).then(r=> df.resolve(r));
      
      } catch (err){
	      console.log(err);
	      df.reject(err.stack);
      }
    })

    return df.promise;
  }

}

module.exports = AutoloaderManager;