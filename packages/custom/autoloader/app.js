'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Autoloader = new Module('autoloader');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Autoloader.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Autoloader.routes(app, auth, database, utils);
  Autoloader.aggregateAsset('css', 'autoloader.css');
  Autoloader.angularDependencies(['agGrid']);
  return Autoloader;
});
