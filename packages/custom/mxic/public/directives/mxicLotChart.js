(function(){
	'use strict';

	function mxicLotChart(_,mqs){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				param: '='
			},
			template: '<div style="width: 100%;height: 500px;"><div></div></div>',
			link: function (scope, element, attrs) {
				var defaultColors =["#FF6600", "#FCD202", "#B0DE09", "#0D8ECF", "#2A0CD0",
				 "#CD0D74", "#CC0000", "#00CC00", "#0000CC", "#DDDDDD", 
				 "#999999", "#333333", "#990000"];

 				scope.passGraphs=[{
				    "fillAlphas": 1,
				    "type": "column",
				    "valueField": "count",
				    "colorField":'dayColor',
				    "title":"投入數",
				    "valueAxis": "v1"
				  }, 
				  {
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
                	"legendValueText": "[[value]]%",
            		"labelText": "[[value]]%",
				    "fillAlphas": 0,
				    "lineAlpha": 1,
				    "title":"N0",
					"maximum": 100,
					"minimum": 0,
					"lineColor":"#ECB88A",
				    "valueField": "N0_passRate",
				    "valueAxis": "v2",
				    "type":"line"
				  }, 
				  {
				    "id": "graph3",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
                	"legendValueText": "[[value]]%",
            		"labelText": "[[value]]%",
				    "fillAlphas": 0,
				    "lineAlpha": 1,
				    "title":"N1",
					"maximum": 100,
					"minimum": 0,
					"lineColor":"#90B44B",
				    "valueField": "N1_passRate",
				    "valueAxis": "v2",
				    "type":"line"
				  }];

				var chartOptions;
				chartOptions = {
				 "type": "serial",
				  "theme": "light",
				  "autoMargins": false,
				  "marginLeft": 80,
				  "marginRight": 80,
				  "marginTop": 10,
				  "marginBottom": 80,
				  "dataProvider": [ 

				  ],
				  "valueAxes": [{
					"id": "v1",
					"axisAlpha": 0,
					"position": "left",
					"ignoreAxisWidth":true,
					"integersOnly":true,
					"title":"投入數"
					},
					{
					"id": "v2",
					"axisAlpha": 0,
					"position": "right",
					"gridAlpha":0,
					"ignoreAxisWidth":true,
					"title":"Pass Rate",
					"unit":"%"
									}],
				  "startDuration": 0,
				  "graphs": scope.passGraphs,
				  "titles":[{text: "",size:15}],
					"chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
					// "categoryBalloonDateFormat": "DD",
					"cursorAlpha": 0.1,
					"cursorColor":"#000000",
					"fullWidth":true,
					"valueBalloonsEnabled": false,
				"zoomable": true
				          },
				  "categoryField": 'LOT_NUMBER',
				  "categoryAxis": {
				    "gridPosition": "start",
				    "axisAlpha": 1,
				    "labelFunction": function( value, dataItem ) {
				    	if(scope.drillLevel==0)
			          		return value +'<br>'+dataItem.dataContext.DATETIME;
			          	else return value;
			        	},
			        "autoRotateAngle":45,
			        "autoRotateCount":10
					},
				   "legend": {
    					"useGraphSettings": true
  					},
				}

				var target = element[0].children[0];

				var chart = AmCharts.makeChart(target, chartOptions);

                chart.clickTimeout = 0;
                chart.lastClick = 0;
                chart.doubleClickDuration = 300;
                scope.drillLevel=0;
				function clickHandler(event) {
	              var ts = (new Date()).getTime();
  	              if ((ts - chart.lastClick) < chart.doubleClickDuration) {  // [[ Double click ]]
	                if (chart.clickTimeout) {
	                  clearTimeout(chart.clickTimeout);
	                }
	                chart.lastClick = 0; // reset last click
	                	resetChart(event);
	              	} else {  // [[ Single click ]]
	                chart.clickTimeout = setTimeout(() => drillDown(event), chart.doubleClickDuration);
	              }
	              chart.lastClick = ts;
	            }

	            chart.addListener("clickGraphItem", clickHandler);

	    function resetChart(event){
			scope.drillLevel=0;
			chart.titles[0].text ="";
			updateChart();
	    } 

        function drillDown(event){
        	
          // 設定所選的 level,group
          var filterValue = event.item.category;
          if(scope.drillLevel==0){
          	scope.drillLevel=1;
          	
          	chart.titles[0].text = filterValue;
          	updateChart(_.filter(scope.data,{'LOT_NUMBER':filterValue}))
			
	      }
	      else if(scope.drillLevel==1){
	      	scope.drillLevel=2;
	      	var params = _.cloneDeep(scope.param);
	      	_.assign(params,{'LOT_NUMBER':chart.titles[0].text,'OVENID':filterValue})
	      	chart.titles[0].text += '_'+ filterValue ;
	      	mqs.query(params).then(function(result){
	      		updateChart(result.data)
	      	})
	      }

        }
				scope.$watch('data', function (nv,ov) {

					if(nv != ov) {
						scope.dateList = _(scope.data).map('DATETIME').uniq().value()
						scope.drillLevel=0;
						chart.titles[0].text ="";
						updateChart();
					}
				});

				var genGraphs = function(data){
					var graphs =[{
				    "id": "graph2",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
                	"legendValueText": "[[value]]%",
            		"labelText": "[[value]]%",
				    "fillAlphas": 0,
				    "lineAlpha": 1,
				    "title":"N0",
					"maximum": 100,
					"minimum": 0,
				    "valueField": "N0_passRate",
				    "valueAxis": "v2",
				    "type":"line"
				  }, 
				  {
				    "id": "graph3",
				    "balloonText": "<span style='font-size:12px;'><span style='font-size:20px;'>[[value]] %</span> </span>",
				    "bullet": "round",
				    "lineThickness": 3,
				    "bulletSize": 7,
                	"legendValueText": "[[value]]%",
            		"labelText": "[[value]]%",
				    "fillAlphas": 0,
				    "lineAlpha": 1,
				    "title":"N1",
					"maximum": 100,
					"minimum": 0,
				    "valueField": "N1_passRate",
				    "valueAxis": "v2",
				    "type":"line"
				  }]; 
					_(data)
					.map('LOT_NUMBER')
					.uniq()
					.forEach(function(lot){
					  var g = new Object({
									    "fillAlphas": 1,
									    "type": "column",
									    "valueField": 'count',
									    "valueAxis": "v1",
									    "labelText": lot,
									    "labelPosition":"top"
									  })
						graphs.push(g)  
					  })
					return graphs;
				}
				var genLegend = function(list){
					var legends=[]
					_.forEach(list,function(d){
						var colorIdx=_.indexOf(scope.dateList,d)%13
					  var l = new Object({title: d,color:defaultColors[colorIdx]})
						legends.push(l)  
					  })
					return legends;
				}

				var processData = function(dataArr){
					var outputArr=[]
					var processRecord={}
					_.forEach(dataArr,function(lotInfo){
						// _.update(lotInfo,'count',function(o){
							// var recordObj ={};
							var colorIdx=_.indexOf(scope.dateList,lotInfo['DATETIME'])%13
							lotInfo['dayColor']=defaultColors[colorIdx];
							var n0=lotInfo['N0_count']||0;
							var n1=lotInfo['N1_count']||0;
							var n0_pass=lotInfo['N0_pass']||0;
							var n1_pass=lotInfo['N1_pass']||0;
							lotInfo['count']=n1>n0?n1:n0;
							if(n0) lotInfo['N0_passRate']=(100*n0_pass/n0).toFixed(2);
							if(n1) lotInfo['N1_passRate']=(100*n1_pass/n1).toFixed(2);
							// return recordObj;
						// })

					})
					return dataArr
				}




				var updateChart = function(filterData){

					if(_.isArray(scope.data)){
						if(scope.drillLevel==0){
							var firstLevelData = _(scope.data).groupBy(dateGroupBy)
	            						.map(function(r,d){
	            							var dayData=countRateByType(r,'LOT_NUMBER')
	            							_.forEach(dayData,row=> row['DATETIME'] = d)
	            							return dayData;
	            						}).flatten().value()
							chart.dataProvider = processData(firstLevelData);
							chart.categoryField='LOT_NUMBER';
						}
						if(scope.drillLevel==1){
							// console.log(filterData)
							var secondLevelData = _(filterData).groupBy(dateGroupBy)
	            						.map(function(r,d){
	            							// console.log(r)
	            							var dayData=countRateByType(r,'TESTER_ID')
	            							_.forEach(dayData,row=> row['DATETIME'] = d)
	            							return dayData;
	            						}).flatten().value()
							chart.dataProvider = processData(secondLevelData)
							chart.categoryField='TESTER_ID';
						}
						if(scope.drillLevel==2){
							chart.dataProvider=processData(countRateByType(filterData,'BOARD'))
							chart.categoryField='BOARD';
						}
					}
					
					// chart.legend.data.push(genLegend(scope.dateList))
					chart.validateData();
					chart.validateNow();
				}

				var dateGroupBy = function(row){
				    	return  row['DATETIME'].slice(0,10)
				}
			    var countRateByType= function(rows,groupKey){
			    	return _(rows).groupBy(groupKey)
			                .map(function(r,groupValue){

			                    var o =new Object();
			                    o[groupKey]=groupValue;

			                    _(r).groupBy('TEST_TYPE').forOwn(function(tr,t){
			                     
			                    var passIC =_.sumBy(tr,scope.drillLevel>1?'PASS_IC':'TOTAL_PASS_IC');
			                    var failIC=_.sumBy(tr,scope.drillLevel>1?'FAIL_IC':'TOTAL_FAIL_IC');
			                    o[t+'_pass'] = passIC;
			                    o[t+'_fail'] = failIC;
			                    o[t+'_count'] = passIC + failIC;   
			                   })
			                    if(o['N1_count']==undefined) o['N1_count']=0;

			                   return o;
			                }).value()
			    }
				scope.getChart = function(){
					return chart;
				}

        	var customGroupBy = function(row,key){
        		if(key=='DATETIME'){ return row[key].slice(5,10)}
        		return row[key]
        	}

				updateChart()
				chart.validateData();
				chart.validateNow();
			}
		};
	};

	angular.module('mean.mxic').directive('mxicLotChart', mxicLotChart);
	mxicLotChart.$inject = ['_','mxicQueryService'];
})();
'use strict';

