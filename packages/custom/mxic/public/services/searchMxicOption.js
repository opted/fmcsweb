(function(){
    'use strict';
    function srchMxicOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("客批", "LOT_NUMBER"),
                uOpt.genField("客型", "PRODUCT_CODE"),
                uOpt.genField("機台編號", "OVENID"),
                uOpt.genField("BIB編號", "BOARD_ID"),
            ],
            getMatch: uOpt.getMatch
                    };
                        };

    angular.module('mean.mxic').factory('srchMxicOpt', srchMxicOpt);
        srchMxicOpt.$inject = ['utilsSrchOpt'];
})();
