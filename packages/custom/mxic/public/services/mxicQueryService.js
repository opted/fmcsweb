(function(){
  'use strict';
  function mxicQueryService(_, $http, $window){
    var isSearching = false;
    var status = {
      str: "",
      err: ""
    };

    var config = {
      eventHandlers: {
      progress: function(event){
        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
        status.str = "正在下載資料...";
      },
      // readystatechange: function(event){
      //   console.log("STATUS:", event);
      // }
      }
    };
    return {
      isSearching: () => isSearching,
      status: status,
      uploadErrorConfig: function(uploadData,errorType, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        var processedArr=[]
        if(errorType =='PPLU600'){
          _.forEach(uploadData.data,function(r){
                var newKeyObj=_.mapKeys(r,function(v,k){
                  var newKey ="";
                  if(k=='訊息分類')
                    newKey = 'MSG_CLASS';
                  if(k=='錯誤碼')
                    newKey = 'ERR_NO';
                  if(k=="錯誤訊息/訊息")
                    newKey = 'MSG_TYPE';
                  if(k=='錯誤編碼 錯誤訊息及排除')
                    newKey = 'ERR_DESC';
                 
                  return  newKey
                  
               })
                newKeyObj['TYPE']=errorType
                processedArr.push(newKeyObj)
            })
        }
        else{//(errorType =='PPLU800')
          _.forEach(uploadData.data,function(r){
                var newKeyObj=_.mapKeys(r,function(v,k){
                  var newKey ="";
                  if(k=='訊息分類')
                    newKey = 'MSG_CLASS';
                  if(k=='NO')
                    newKey = 'ERR_NO';
                  if(k=="錯誤訊息/訊息")
                    newKey = 'MSG_TYPE';
                  if(k=='中文訊息')
                    newKey = 'ERR_DESC';
                 
                  return  newKey
                  
               })
                newKeyObj['TYPE']=errorType
                processedArr.push(newKeyObj)
            })
        }
        var queryParam=new Object({'data':processedArr,'errType':errorType})
        return $http.post('/api/mxic/uploaderrorconfig', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";

          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },

      getLotData: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/mxic/getlotdata', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);

          return result.data
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
      getOvenData: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        return $http.post('/api/mxic/getovendata', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          // callback(result.data);

          return _(result.data)
                .groupBy('OVENID')
                .map(function(r,oid){
                   return _(r).groupBy('BOARD')
                   .map(function(rows,s){
                       var o={}
                       o['SLOT']=s;
                       var p = _.sumBy(rows,'PASS_IC');
                       var f = _.sumBy(rows,'FAIL_IC');
                       o['PASSRATE']=(100*p/(p+f)).toFixed(2);
                       return o; 
                   }).forEach(slot => slot['OVENID']=oid);
                })
                .flatten().sortBy('OVENID').value()
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },

      query: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        // console.log(queryParam)
        return $http.post('/api/mxic/getdata', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
      getBIBData: function(queryParam, callback){
        isSearching = true;
        status.str = "等待 Server 回應...";
        // console.log(queryParam)
        return $http.post('/api/mxic/getbibdata', queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          return result.data;
          status.str = "Done";
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      },
    };
  };

  angular.module('mean.mxic').factory('mxicQueryService', mxicQueryService);
  mxicQueryService.$inject = ["_", "$http", "$window"];
})();