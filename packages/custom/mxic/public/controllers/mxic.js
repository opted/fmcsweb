'use strict';
var ted;
function MxicController(_, $scope,$http,srchMxicOpt,mqs) {
  $scope.packageName = 'Mxic';
  $scope.selected = {
    "from": getToday(),  // Set default start day
    "to": getToday()  // Set default end day
  };
  	ted = $scope;
  	//to slot level
  	$scope.detailGridDefs =[     
        {headerName: "Test Program", field: "TEST_PROGRAM", width: 120},
            {headerName: "Product Code", field: "PRODUCT_CODE", width: 80},
            {headerName: "Lot Number(客批)", field: "LOT_NUMBER", width: 120},
            {headerName: "Sub Lot Id(內批)", field: "LOTNO", width: 100},
            {headerName: "Lot Size", field: "LOT_SIZE", width: 80},
            {headerName: "Test Mode", field: "TEST_MODE", width: 80},
            {headerName: "Test Type", field: "TEST_TYPE", width: 80},
            {headerName: "Package Type", field: "PACKAGE_TYPE", width: 100},
            {headerName: "Operator", field: "OPERATOR", width: 80},
            {headerName: "Tester ID", field: "TESTER_ID", width: 80},
            {headerName: "Test Time", field: "TEST_TIME", width: 80},
            {headerName: "Index Time", field: "INDEX_TIME", width: 100},
            {headerName: "Code No.", field: "CODE_NO", width: 100},
            {headerName: "Zone", field: "ZONE", width: 50},
            {headerName: "Board", field: "BOARD", width: 50},
            {headerName: "Board ID", field: "BOARD_ID", width: 100},
            {headerName: "Pass IC", field: "PASS_IC", width: 80},
            {headerName: "Fail IC", field: "FAIL_IC", width: 80},
            {headerName: "Empty IC", field: "EMPTY_IC", width: 80},
            {headerName: "良率(%)", field: "PASS_RATE", width: 100},
            {headerName: "Start Test Time", field: "START_TEST_TIME", width: 150},
            {headerName: "End Test Time", field: "DATETIME", width: 150},
            ]

  	$scope.ovenGridDefs =[     
        {headerName: "OVEN ID", field: "OVENID", width: 120},
        {headerName: "Slot No.", field: "SLOT", width: 80},
        {headerName: "Yield", field: "PASSRATE", width: 120},
        ]

  $scope.boardSeleted = {
            onSelected: {},
            boardsList: [
            // {"name":"爐號",id:"EQUIPMENTNO"},
            ],
        };
  $scope.mapGroup='BOARD_ID';
  $scope.changeItem = function(itemName){  
      $scope.boardSeleted.onSelected = itemName
      var filterObj = {};
      filterObj[$scope.mapGroup]=itemName;
      $scope.chartData = _(_.filter($scope.bibRows,filterObj))
                          .groupBy('BOARD')
                          .map(function(r,slot){
                            var o = new Object()
                            o['slot']=slot;
                            o['slotData']=r;
                            return o;
                            }).value()
  }


  $scope.$watch('mapSwitch', function (nv,ov) {
          if(nv != ov) {
            $scope.mapGroup= nv?'TESTER_ID':'BOARD_ID';
          }
  });

  $scope.diffCountLots=[];
  $scope.exportXLS = function(isDaily){
    // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
    if ($scope.mxicGrid.api.getModel().getRowCount() < 1){
      $window.alert("無資料可供下載");
      return;
    }

    var params = {
      fileName: "MXIC.xls"
    };

    $scope.mxicGrid.api.exportDataAsExcel(params);
  };
  $scope.fieldSearchURL = "/api/mxic/getsearchfield";
	$scope.fields = srchMxicOpt.fields;
    $scope.mxicGrid = {
      columnDefs:  [],
      rowData:[],
      groupUseEntireRow: false,
      enableSorting: true,
      enableColResize: true
   };
   $scope.mxicOvenGrid = {
      columnDefs:  [     
        {headerName: "OVEN ID", field: "OVENID", width: 120},
        {headerName: "Slot No.", field: "SLOT", width: 80},
        {headerName: "Yield", field: "PASSRATE", width: 120},
        ],
      rowData:[],
      groupUseEntireRow: false,
      enableSorting: true,
      enableColResize: true
   };

    $scope.getLotData = function() {

        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }

        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();

        _.assign(queryParam, srchMxicOpt.getMatch($scope.fields));
        $scope.param =queryParam;
        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };
      $scope.diffCountLots=[];        
      $scope.requestStatus = "等待 Server 回應...";
      var startTime = new Date();
      $scope.queryTime = "";
      $scope.rowNum = undefined;
      $scope.isSearching = true;
      mqs.getLotData(queryParam)
          .then(function(result){
            $scope.lotRows=result;
            //check if type n0,n1 have different count in same lot_number data 
            _(result).groupBy('LOT_NUMBER')
                          .forOwn(function(rows,lot){
                var n0= _.partition(rows,r=>r["TEST_TYPE"]=='N0')[0];

                var n1= _.partition(rows,r=>r["TEST_TYPE"]=='N0')[1];
                
                _.forEach(n1,function(r){
                  console.log(n0,r)
                   var match = _.find(n0,_.pick(r,['LOT_SIZE']))
                   if(match==undefined ) {
                      var compareN0= _(n0).filter(_.pick(r,['LOT_NUMBER']))
                                    .maxBy(m=>parseInt(m['LOT_SIZE']));
                      $scope.diffCountLots.push({'n0Count':compareN0['LOT_SIZE'],
                                               'LOT_NUMBER':r['LOT_NUMBER'],
                                                'n1Count':r['LOT_SIZE']
                                              });
                  }
               }) 
            })
   
            $scope.queryTime = new Date() - startTime;
            $scope.ok()
          },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              console.log(err)
          }).finally(function(){
              $scope.isSearching = false;
          });

    };
    $scope.getOvenData = function() {

        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }

        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();

        _.assign(queryParam, srchMxicOpt.getMatch($scope.fields));
        $scope.param =queryParam;
        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };
        // $scope.mxicGrid.api.setColumnDefs($scope.lotGridDefs);
      $scope.requestStatus = "等待 Server 回應...";
      var startTime = new Date();
      $scope.queryTime = "";
      $scope.isSearching = true;
      mqs.getOvenData(queryParam)
          .then(function(result){
            $scope.ovenRows=result;
            $scope.mxicOvenGrid.api.setRowData($scope.ovenRows)
            $scope.requestStatus = "查詢完成";
            $scope.queryTime = new Date() - startTime;

          },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              console.log(err)
          }).finally(function(){
              $scope.isSearching = false;
          });

    };
    $scope.ok = function() {

        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }

        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        _.assign(queryParam, srchMxicOpt.getMatch($scope.fields));
        
        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };

      $scope.requestStatus = "等待 Server 回應...";
      var startTime = new Date();
      $scope.queryTime = "";
      $scope.rowNum = undefined;
      $scope.isSearching = true;
      mqs.query(queryParam)
          .then(function(result){
            $scope.rawData = result.data.sort(function(a,b){return a['DATETIME']-b['DATETIME']})
            $scope.dutNum=result.dutNum;
            var dutColDefs=[],dutIndex=0;
            while (dutIndex<$scope.dutNum){
              dutColDefs.push({headerName: "dut"+dutIndex, field: "dut"+dutIndex, width: 75});
              dutIndex++;
            }

            $scope.rows=$scope.rawData;
            $scope.rowNum=$scope.rows.length
            $scope.mxicGrid.api.setColumnDefs($scope.detailGridDefs.concat(dutColDefs));
            $scope.mxicGrid.api.setRowData($scope.rows)
            $scope.requestStatus = "查詢完成";
            $scope.queryTime = new Date() - startTime;

          },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              console.log(err)
          }).finally(function(){
              $scope.isSearching = false;
          });

    };

    $scope.getBIBData = function() {

        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }
        if (_.isNull(queryParam.BOARD_ID)){
            $window.alert("請設定BIB編號");
            return 0;
        }

        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        _.assign(queryParam, srchMxicOpt.getMatch($scope.fields));

        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };

      $scope.requestStatus = "等待 Server 回應...";
      var startTime = new Date();
      $scope.queryTime = "";
      // $scope.rowNum = undefined;
      $scope.isSearching = true;
      _.assign(queryParam, srchMxicOpt.getMatch($scope.fields));
      mqs.getBIBData(queryParam)
          .then(function(result){

            $scope.bibRows=result;
            $scope.boardSeleted.boardsList = _($scope.bibRows)
                                            .groupBy($scope.mapGroup).map(function(r,b){
                                              return {id:b,name:b,fc:_.sumBy(r,'FAIL_IC')}
                                            }).sortBy('fc').reverse().value()

            
            $scope.requestStatus = "查詢完成";
            $scope.queryTime = new Date() - startTime;

          },function(err){
              $scope.requestStatus = '查詢失敗, '+err.data;
              console.log(err)
          }).finally(function(){
              $scope.isSearching = false;
          });

    };
};

angular
  .module('mean.mxic')
  .controller('MxicController', MxicController);

MxicController.$inject = ['_', '$scope', '$http','srchMxicOpt','mxicQueryService'];
