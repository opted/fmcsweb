(function() {
  'use strict';

  function Mxic($stateProvider) {
    $stateProvider.state('mxic main page', {
      url: '/mxic',
      templateUrl: 'mxic/views/index.html',
      controller:MxicController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.mxic')
    .config(Mxic);

  Mxic.$inject = ['$stateProvider'];

})();
