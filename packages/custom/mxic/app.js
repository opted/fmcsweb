'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Mxic = new Module('mxic');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Mxic.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Mxic.routes(app, auth, database, utils);
  Mxic.aggregateAsset('css', 'mxic.css');
  Mxic.angularDependencies(['agGrid']);
  return Mxic;
});
