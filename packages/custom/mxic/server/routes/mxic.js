(function() {
  'use strict';
  module.exports = function(Mxic, app, auth, database, utils) {
    var MxicManager = require('../controllers/mxicQuery');
    var mxicMgr = new MxicManager(utils);

    app.post('/api/mxic/getsearchfield', function(req, res, next){
      console.log(req.body)
      utils.ctrls.mongoQuery.getSearchField(mxicMgr.coll, req.body, mxicMgr.pathMapping, mxicMgr.arrayFields)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));
    });


    app.post('/api/mxic/getdata', function(req, res, next) {
      mxicMgr.getData(req.body, res, next)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));;
    });
    app.post('/api/mxic/getlotdata', function(req, res, next) {
      mxicMgr.getLotData(req.body, res, next)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));;
    });
    app.post('/api/mxic/getovendata', function(req, res, next) {
      mxicMgr.getOvenData(req.body, res, next)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));;
    });

    app.post('/api/mxic/getbibdata', function(req, res, next) {
      mxicMgr.getBIBData(req.body, res, next)
      .then((data) => res.json(data))
      .catch((err) => res.status(500).send({'error': err}));;
    });
  };
})();
