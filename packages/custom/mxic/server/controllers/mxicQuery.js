'use strict'
var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');

var MXIC = mongoose.model('MXIC');

function addHours(queryDateTime){
  var tempDate = new Date(queryDateTime)
  return tempDate.setHours(tempDate.getHours()+8);
}
function substractHours(queryDateTime){
  var tempDate = new Date(queryDateTime)
  return tempDate.setHours(tempDate.getHours()-8);
}


class MxicManager {
  constructor(utils) {
    this.coll = MXIC;
    this.utils = utils;
    this.pathMapping = {
      'OVENID':'OVENID',
      'LOT_NUMBER':'LOT_NUMBER',
      'PRODUCT_CODE':'PRODUCT_CODE',
      'BOARD_ID':'SLOTS.BOARD_ID'
    };
    this.searchPathMapping = {
      'OVENID': 'OVENID',
      'PRODUCT_CODE':'PRODUCT_CODE',
      'LOT_NUMBER':'LOT_NUMBER'
    };
    this.arrayFields = ['SLOTS'];
  }


  getData(params){
    var df = Q.defer();
    try {
      var query = params;
      var match = {},matchFilter={};
      var dateRange = {'DATETIME': {}};
      if (query.from !== null){
        dateRange.DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATETIME['$lt'] = new Date(addHours(query.to));
      }

      if (!_.isEmpty(dateRange.DATETIME)&& _.isEmpty(query.LOT_NUMBER)){
        _.assign(match, dateRange);
      }
     
      var pipe = [{'$unwind': "$SLOTS"},{'$match': match}];
      // pipe.push({'$unwind': "$SLOTS"});
 _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['OVENID','LOT_NUMBER','PRODUCT_CODE','BOARD_ID']), this.pathMapping));
      pipe.push({
        '$project': {
          '_id': 0,
          'ISODATE':'$DATETIME',
          'TEST_PROGRAM':1,
          'PRODUCT_CODE':1,
          'LOTNO':1,
          'LOT_NUMBER':1,
          'LOT_SIZE':1,
          'TEST_MODE':1,
          'TEST_TYPE':1,
          'PACKAGE_TYPE':1,
          'OPERATOR':1,
          'TESTER_ID':1,
          'TEST_TIME':1,
          'INDEX_TIME':1,
          'CODE_NO':1,
            'ZONE': '$SLOTS.ZONE',
          	'BOARD': '$SLOTS.BOARD',
            'BOARD_ID':'$SLOTS.BOARD_ID' ,
            'PASS_IC': '$SLOTS.PASS_IC',
            'FAIL_IC': '$SLOTS.FAIL_IC',
			'EMPTY_IC': '$SLOTS.EMPTY_IC',
			'PASS_RATE': '$SLOTS.PASS_RATE',
			'DUTMAP': '$SLOTS.DUTMAP',
			'Y': '$SLOTS.Y',
			'X': '$SLOTS.X',
          'START_TEST_TIME':{
            '$dateToString': { 'format': '%Y-%m-%d %H:%M:%S', 'date': '$START_TEST_TIME' }
          },			
          'DATETIME': {
            '$dateToString': { 'format': '%Y-%m-%d %H:%M:%S', 'date': '$DATETIME' }
          },			
			
        }}
      );

      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(MXIC, pipe)
      .then(function(result){
        try {
             _.forEach(result,function(r){
              _.forEach(r['DUTMAP'],(dut_value,idx)=> r['dut'+idx]=dut_value);
              r['PASS_RATE'] = parseFloat(r['PASS_RATE']);
             })
            df.resolve(  {'dutNum':_(result).map(r=> r.DUTMAP.length).max(),'data':result} );
        } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
        }
      })
      
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }

  getOvenData(params){
    var df = Q.defer();
    try {
      var query = params;
      var match = {},matchFilter={};
      var dateRange = {'DATETIME': {}};
      if (query.from !== null){
        dateRange.DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATETIME['$lt'] = new Date(addHours(query.to));
      }

      if (!_.isEmpty(dateRange.DATETIME)){
        _.assign(match, dateRange);
      }
      var pipe = [{'$match': match}];
      pipe.push({'$unwind': "$SLOTS"});

      pipe.push({
        '$project': {
          '_id': 0,
          'ISODATE':'$DATETIME',
          'OVENID':1,
          'TESTER_ID':1,
            'ZONE': '$SLOTS.ZONE',
            'BOARD': '$SLOTS.BOARD',
            'PASS_IC': '$SLOTS.PASS_IC',
            'FAIL_IC': '$SLOTS.FAIL_IC',
      'EMPTY_IC': '$SLOTS.EMPTY_IC',
      'PASS_RATE': '$SLOTS.PASS_RATE'
        }}
      );

      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(MXIC, pipe)
      .then(function(result){
        try {
            df.resolve( result);
        } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
        }
      })
      
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }


  getBIBData(params){
    var df = Q.defer();
    try {
      var query = params;
      var match = {},matchFilter={};
      var dateRange = {'DATETIME': {}};
      if (query.from !== null){
        dateRange.DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATETIME['$lt'] = new Date(addHours(query.to));
      }

      if (!_.isEmpty(dateRange.DATETIME)){
        _.assign(match, dateRange);
      }

      _.assign(matchFilter, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['OVENID','LOT_NUMBER','PRODUCT_CODE','BOARD_ID']), this.pathMapping));
      var pipe = [{'$match': match}];
      pipe.push({'$unwind': "$SLOTS"});
      pipe.push({'$match':matchFilter});
      pipe.push({
        '$project': {
          '_id': 0,
          'ISODATE':'$DATETIME',
          'TESTER_ID':1,
        	'BOARD': '$SLOTS.BOARD',
          'BOARD_ID':'$SLOTS.BOARD_ID' ,
          'PASS_IC': '$SLOTS.PASS_IC',
          'FAIL_IC': '$SLOTS.FAIL_IC',
    			'EMPTY_IC': '$SLOTS.EMPTY_IC',
    			'PASS_RATE': '$SLOTS.PASS_RATE',
    			'DUTMAP': '$SLOTS.DUTMAP',
    			'Y': '$SLOTS.Y',
    			'X': '$SLOTS.X',		
          'DATETIME': {
            '$dateToString': { 'format': '%Y-%m-%d %H:%M:%S', 'date': '$DATETIME' }
          },			
			
        }}
      );

      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(MXIC, pipe)
      .then(function(result){
        try {
            df.resolve(result);
        } catch (e) {
            console.log(e)
            df.reject(e.stack);
        }
      })
      
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }

  getLotData(params){
    var df = Q.defer();
    try {
      var query = params;
      var match = {},matchFilter={};
      var dateRange = {'DATETIME': {}};
      if (query.from !== null){
        dateRange.DATETIME['$gte'] = new Date(addHours(query.from));
      }
      if (query.to !== null){
        dateRange.DATETIME['$lt'] = new Date(addHours(query.to));
      }

      if (!_.isEmpty(dateRange.DATETIME)&& _.isEmpty(query.LOT_NUMBER)){
        _.assign(match, dateRange);
      }
      _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['OVENID','LOT_NUMBER','PRODUCT_CODE']), this.pathMapping));
      var pipe = [{'$match': match}];
      // pipe.push({'$unwind': "$SLOTS"});
      pipe.push({
        '$project': {
          '_id': 0,
          'ISODATE':'$DATETIME',
          'TEST_PROGRAM':1,
          'PRODUCT_CODE':1,
          'LOTNO':1,
          'LOT_NUMBER':1,
          'LOT_SIZE':1,
          'TEST_MODE':1,
          'TEST_TYPE':1,
          'PACKAGE_TYPE':1,
          'OPERATOR':1,
          'TESTER_ID':1,
          'TEST_TIME':1,
          'INDEX_TIME':1,
          'CODE_NO':1,
          'TOTAL_PASS_IC':1,
          'TOTAL_FAIL_IC':1,
          'TOTAL_EMPTY_IC':1,
          'START_TEST_TIME':{
            '$dateToString': { 'format': '%Y-%m-%d %H:%M:%S', 'date': '$START_TEST_TIME' }
          },
          'DATETIME': {
            '$dateToString': { 'format': '%Y-%m-%d %H:%M:%S', 'date': '$DATETIME' }
          }
          }
        }
      );


      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(MXIC, pipe)
      .then(function(result){
        try {
            df.resolve(result);
        } catch (e) {
            console.log(e)
            df.reject(e.stack);
            
        }
      })
      
      
    } catch (err){
      console.log(err);
      df.reject(err.stack);
    }
    return df.promise;
  }

}

module.exports = MxicManager;