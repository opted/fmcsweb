'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Ecid = new Module('ecid');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Ecid.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Ecid.routes(app, auth, database, utils);

  Ecid.aggregateAsset('css', '../css/ecid.css');

  //We are adding a link to the main menu for all authenticated users
  /*
  Ecid.menus.add({
    title: 'ecid example page',
    link: 'ecid example page',
    roles: ['authenticated'],
    menu: 'main'
  });
  */
  
  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Ecid.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Ecid.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Ecid.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  Ecid.angularDependencies(['agGrid']);
  return Ecid;
});
