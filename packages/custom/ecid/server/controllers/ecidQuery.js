'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var ECID = mongoose.model('ECID');
var Q = require('q');

class EcidManager {
  constructor(utils) {
    this.coll = ECID;
    this.utils = utils;
    this.pathMapping = {
      'SITENO': 'SITENO',
      'CUSTOMERLOTNO': 'CUSTOMERLOTNO',
      'LOTNO': 'LOTNO',
      'ECID': 'ZONES.BOARDS.DUTS.ECID',
      'ZONE': 'ZONES.ZONE',
      'BOARD': 'ZONES.BOARDS.BOARD',
      'BID': 'ZONES.BOARDS.BID',
      'DUT': 'ZONES.BOARDS.DUTS.DUT'
    };
    this.arrayFields = ['ZONES', 'BOARDS', 'DUTS'];
  }

  getSearchResult (params) {
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      
      var dateRange = {'DATE': {}};
      if (query.from !== null){
        dateRange.DATE['$gte'] = new Date(query.from);
      }
      if (query.to !== null){
        dateRange.DATE['$lt'] = new Date(query.to);
      }
      
      if (!_.isEmpty(dateRange.DATE)){
        _.assign(match, dateRange);
      }

      if (!_.isEmpty(query.ECID) && _.isArray(query.ECID)){
        match[this.pathMapping['ECID']] = {'$in': query.ECID};        
      }

      _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['SITENO', 'CUSTOMERLOTNO', 'LOTNO']), this.pathMapping));
      
      var pipe = [{'$match': match}];
      pipe.push({'$sort': {"DATE": -1}});

      // 僅取每個 lot 的最後一次檔案
      if (query.isLastLot === true){
        pipe.push({'$group': {
          '_id': '$LOTNO',
          'last': {'$first': '$$ROOT'}
        }});

        var firstLvlCols = [
          'DATE', 'FILEPATH', 'DB', 
          'SITENO', 'CUSTOMERNO', 'CUSTOMERLOTNO', 'LOTNO', 'DEVICENO', 'PROGRAMNAME',
          'BILOAD', 'BURNIN', 'BIUNLOAD', 
          'OVENID', 'OVEN', 'ZONES'
        ];
        pipe.push({'$project': _.zipObject(firstLvlCols, _.map(firstLvlCols, c => '$last.'+ c))});
      }

      pipe.push({'$unwind': "$ZONES"});
      pipe.push({'$unwind': "$ZONES.BOARDS"});
      pipe.push({'$match': this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['ZONE', 'BOARD', 'BID']), this.pathMapping)});
      pipe.push({'$project': {
        '_id': 0,
        'FILEPATH': 1, 'OVEN': 1, 'DATE': 1, 'LOTNO': 1, 'CUSTOMERLOTNO': 1, 'DEVICENO': 1, 'PROGRAMNAME': 1,
        'BILOAD_CHECKINEMPLOYEENO': '$BILOAD.CHECKINEMPLOYEENO',
        'BILOAD_CHECKINTIME': '$BILOAD.CHECKINTIME',
        'BILOAD_CHECKOUTEMPLOYEENO': '$BILOAD.CHECKOUTEMPLOYEENO',
        'BILOAD_CHECKOUTTIME': '$BILOAD.CHECKOUTTIME',
        'BILOAD_EQUIPMENTNO': '$BILOAD.EQUIPMENTNO',
        'BURNIN_CHECKINEMPLOYEENO': '$BURNIN.CHECKINEMPLOYEENO',
        'BURNIN_CHECKINTIME': '$BURNIN.CHECKINTIME',
        'BURNIN_CHECKOUTEMPLOYEENO': '$BURNIN.CHECKOUTEMPLOYEENO',
        'BURNIN_CHECKOUTTIME': '$BURNIN.CHECKOUTTIME',
        'BURNIN_EQUIPMENTNO': '$BURNIN.EQUIPMENTNO',
        'BIUNLOAD_CHECKINEMPLOYEENO': '$BIUNLOAD.CHECKINEMPLOYEENO',
        'BIUNLOAD_CHECKINTIME': '$BIUNLOAD.CHECKINTIME',
        'BIUNLOAD_CHECKOUTEMPLOYEENO': '$BIUNLOAD.CHECKOUTEMPLOYEENO',
        'BIUNLOAD_CHECKOUTTIME': '$BIUNLOAD.CHECKOUTTIME',
        'BIUNLOAD_EQUIPMENTNO': '$BIUNLOAD.EQUIPMENTNO',
        'ZONE': '$ZONES.ZONE',
        'BOARD': '$ZONES.BOARDS.BOARD',
        'BTS': '$ZONES.BOARDS.BTS',
        'DRIVER_ID': '$ZONES.BOARDS.DRIVER_ID',
        'X': '$ZONES.BOARDS.X',
        'Y': '$ZONES.BOARDS.Y',
        'BID': '$ZONES.BOARDS.BID',
        'DUTS': '$ZONES.BOARDS.DUTS',
      }});

      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(ECID, pipe)
      .then(function(result){
        try {
          var dutList = [];
          _.forEach(result, function(board){
            if (board.DUTS != null) {
              _.forEach(board.DUTS, function(dut) {
                if (!_.includes(dutList, dut.DUT)){
                  dutList.push(dut.DUT);
                }
                board[dut.DUT] = dut.ECID;
              });

              delete board.DUTS;
            }
          });

          dutList.sort(function(a, b){
            return a.slice(1, 3) - b.slice(1, 3);
          });

          df.resolve({"boards": result, "dutlist": dutList});
        } catch(e){
          df.reject(e.stack);
        }
      });
    } catch (err){
      df.reject(err.stack);
    }

    return df.promise;
  };
}

module.exports = EcidManager;