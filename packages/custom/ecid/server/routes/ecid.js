(function() {
    'use strict';
    // The Package is past automatically as first parameter
    module.exports = function(Ecid, app, auth, database, utils) {
        var EcidManager = require('../controllers/ecidQuery');
        var ecidMgr = new EcidManager(utils);
        app.post('/api/ecid/getsearchfield', function(req, res, next){ // search field too often, do not save requestlog
            utils.ctrls.mongoQuery.getSearchField(ecidMgr.coll, req.body, ecidMgr.pathMapping, ecidMgr.arrayFields)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });

        app.post('/api/ecid/getsearchresult',function(req, res, next){
            ecidMgr.getSearchResult(req.body)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });
    };
})();
