'use strict';

/* jshint -W098 */
function toDateString(dateObj) {
  return new Date(dateObj).toISOString().replace(/T/, ' ').replace(/\..+/, '');
};

function getToday(){
  var cur = new Date();  // Get current time
  cur.setHours(0, 0, 0, 0);  // Set to the start of day
  return cur;
}

function EcidController(_, $scope, $window, $http, srchOpt) {
  $scope.selected = {
    "from": getToday(),  // Set default start day
    "to": getToday(),  // Set default end day
    "isLastLot": false
  };

  $scope.isSearching = false;  // 卡介面操作
  $scope.rowNum = 0;  // 資料筆數

  var DATE_COLUMNS = [
    "DATE",
    "BILOAD_CHECKINTIME",
    "BILOAD_CHECKOUTTIME",
    "BURNIN_CHECKINTIME",
    "BURNIN_CHECKOUTTIME",
    "BIUNLOAD_CHECKINTIME", 
    "BIUNLOAD_CHECKOUTTIME"
  ];

  var colDefs = [
    {headerName: "Date Time", field: "DATE", width: 200},
    {headerName: "Handler", field: "BILOAD_EQUIPMENTNO", width: 85},
    {headerName: "File Path", field: "FILEPATH", width: 300},
    {headerName: "Handler Kit", field: "HANDLER_KIT", width: 80},
    {headerName: "BTS", field: "BTS", width: 120},
    {headerName: "Driver ID", field: "DRIVER_ID", width: 120},
    {headerName: "Oven ID", field: "OVEN", width: 80},
    {headerName: "Zone", field: "ZONE", width: 80},
    {headerName: "Board", field: "BOARD", width: 80},
    {headerName: "Board ID", field: "BID", width: 80},
    {headerName: "LOTNO", field: "LOTNO", width: 80},
    {headerName: "CUSTOMERLOTNO", field: "CUSTOMERLOTNO", width: 140},
    {headerName: "客戶型號(DEVICENO)", field: "DEVICENO", width: 140},
    {headerName: "PROGRAMNAME", field: "PROGRAMNAME", width: 160},
    {headerName: "X", field: "X", width: 40},
    {headerName: "Y", field: "Y", width: 40},
    {headerName: "[Load] EQUIPMENTNO", field: "BILOAD_EQUIPMENTNO", width: 150},
    // {headerName: "[Load] Checkin Personal", field: "BILOAD_CHECKINEMPLOYEENO", width: 150},
    // {headerName: "[Load] Checkin Time", field: "BILOAD_CHECKINTIME", width: 150},
    {headerName: "[Load] CheckOut Personal", field: "BILOAD_CHECKOUTEMPLOYEENO", width: 150},
    {headerName: "[Load] CheckOut Time", field: "BILOAD_CHECKOUTTIME", width: 150},
    {headerName: "[BI] EQUIPMENTNO", field: "BURNIN_EQUIPMENTNO", width: 150},
    // {headerName: "[BI] Checkin Personal", field: "BURNIN_CHECKINEMPLOYEENO", width: 150},
    // {headerName: "[BI] Checkin Time", field: "BURNIN_CHECKINTIME", width: 150},
    {headerName: "[BI] CheckOut Personal", field: "BURNIN_CHECKOUTEMPLOYEENO", width: 150},
    {headerName: "[BI] CheckOut Time", field: "BURNIN_CHECKOUTTIME", width: 150},
    {headerName: "[Unload] EQUIPMENTNO", field: "BIUNLOAD_EQUIPMENTNO", width: 150},
    // {headerName: "[Unload] Checkin Personal", field: "BIUNLOAD_CHECKINEMPLOYEENO", width: 150},
    // {headerName: "[Unload] Checkin Time", field: "BIUNLOAD_CHECKINTIME", width: 150},
    {headerName: "[Unload] CheckOut Personal", field: "BIUNLOAD_CHECKOUTEMPLOYEENO", width: 150},
    {headerName: "[Unload] CheckOut Time", field: "BIUNLOAD_CHECKOUTTIME", width: 150}
  ];

  $scope.boardInfoGrid = {
    columnDefs: [],
    rowData:[],
    groupUseEntireRow: false,
    enableSorting: true,
    enableColResize: true,
    //groupSuppressAutoColumn: true,
    //suppressAggFuncInHeader: true,
    excelStyles: [
      {
        id: "greenBackground",
        interior: {
          color: "#aaffaa", pattern: 'Solid'
        }
      },
    ]
  };

  $scope.fields = srchOpt.fields;  // 網頁的查詢欄位
  $scope.fieldSearchURL = "/api/ecid/getsearchfield";
  $scope.requestStatus = "";  // http Request 當下狀態
  $scope.sheetData;
  
  $scope.ok = function() {
    $scope.rowNum = 0;
    $scope.isSearching = true;
    $scope.queryTime = "";
    var startTime = new Date();

    // 設定時間
    var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));  // [NOTE] cloneDeep 是因為 _.pick 回傳的是淺層複製
    if (queryParam.to !== null){
      queryParam.to.setDate(queryParam.to.getDate() + 1);  // 讓結束天 = 另一天的起頭  
    }

    // 設定其他參數
    _.assign(queryParam, srchOpt.getMatch($scope.fields));

    if (_.isEmpty(_.pickBy(queryParam, _.identity)) && _.isEmpty($scope.sheetData)){
      $window.alert("請設定至少一種搜尋條件(時間、客戶、客批、ECID、內批)");
      $scope.isSearching = false;
      return;
    }

    if (!_.isEmpty(_.pickBy(_.pick(queryParam, ['ZONE', 'BOARD', 'BID']), _.identity))){
      if (_.isEmpty(_.pickBy(_.pick(queryParam, ['SITENO', 'CUSTOMERLOTNO', 'ECID', 'LOTNO']), _.identity))){
        $window.alert("請設定至少一種搜尋條件(時間、客戶、客批、ECID、內批)");
        $scope.isSearching = false;
        return;
      }
    }

    // 檢查單一 ECID 查詢
    if (!_.isEmpty(_.pick(queryParam, 'ECID'))){
      queryParam.ECID = [queryParam.ECID];
    }

    // 檢查大量 ECID 查詢 (忽略 單一ECID 查詢欄位)
    if (!_.isEmpty($scope.sheetData) && _.isArray($scope.sheetData)){
      queryParam.ECID = $scope.sheetData;       
    }

    queryParam.isLastLot = $scope.selected.isLastLot;

    var config = {
      eventHandlers: {
        progress: function(event){
          // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
          $scope.requestStatus = "正在下載資料...";
        },
        // readystatechange: function(event){
        //   console.log("STATUS:", event);
        // }
      }
    };
    $scope.requestStatus = "等待 Server 回應...";
    $http.post('/api/ecid/getsearchresult', queryParam, config)
    .then(function(result){
        $scope.requestStatus = "處理回傳資料...";
        var dutList = result.data.dutlist;
        var rows = result.data.boards;
        $scope.rowNum = rows.length;

        // 轉換時間格式 (因 ag-grid export 時不會使用 cellRenderer)
        _.forEach(rows, function(row){
          _.forEach(DATE_COLUMNS, function(dateCol){
            if (row[dateCol]){

              _.update(row, dateCol, toDateString);

            }
          });
        });

        $scope.boardInfoGrid.api.setColumnDefs(genColumnDefs(dutList, colDefs, queryParam.ECID));
        $scope.boardInfoGrid.api.setRowData(rows);
        $scope.requestStatus = "查詢完成";
        $scope.queryTime = new Date() - startTime;
      }, function(err){
        $window.alert("Server failed...");
        $scope.requestStatus = "查詢失敗, " + err.data;
      }).finally(function(){
        $scope.isSearching = false;
      });
  };

  $scope.exportXLS = function(){
    // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
    if ($scope.boardInfoGrid.api.getModel().getRowCount() < 1){
      $window.alert("無資料可供下載");
      return;
    }

    var params = {
      fileName: "ecid_export.xls"
    };

    $scope.boardInfoGrid.api.exportDataAsExcel(params);
  };

  /*
    若有上傳大量 ECID sheet 查詢，則清除預設的時間區間
    反之，設回預設時間區間
  */
  $scope.$watch('sheetData', function(newVal, oldVal){
    if (!_.isEmpty($scope.sheetData)){
      $scope.selected.from = null;
      $scope.selected.to = null;
    } else {
      $scope.selected.from = getToday();
      $scope.selected.to = getToday();
    }
  });
}

var genColumnDefs = function(list, defs, specialList){
  var newColDefs = _.cloneDeep(defs);
  _.forEach(list, element => newColDefs.push({ headerName: element, field: element, width: 150, cellClassRules: {
    greenBackground: function(params){
      /* 此設定僅影響 excel 輸出的欄位顏色判斷，還必須至 css 加入 style 如下:
          .greenBackground {
            background-color: lightgreen;
          }
      */
      return _.includes(specialList, params.value);
    }
  }}));
  return newColDefs;
}

angular
  .module('mean.ecid')
  .controller('EcidController', EcidController);

EcidController.$inject = ['_', '$scope', '$window', '$http', 'srchOpt'];
