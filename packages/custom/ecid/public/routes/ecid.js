(function() {
    'use strict';

    function Ecid($stateProvider) {
        $stateProvider.state('ecid main page', {
            url: '/ecid',
            templateUrl: 'ecid/views/index.html',
            controller:EcidController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              }
            }
        })
            .state('ecid circles example', {
            url: '/ecid/example/:circle',
            templateUrl: 'ecid/views/example.html'
        });
    }

    angular
        .module('mean.ecid')
        .config(Ecid);

    Ecid.$inject = ['$stateProvider'];

})();
