(function(){
    'use strict';
    function srchOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("客戶(Siteno)", "SITENO"),
                uOpt.genField("客批(CustomerLotno)", "CUSTOMERLOTNO"),
                uOpt.genField("ECID", "ECID", "string", false, false),
                uOpt.genField("內批(Lotno)", "LOTNO", "string", false),
                uOpt.genField("ZONE", "ZONE", "string", false, false),
                uOpt.genField("BOARD", "BOARD", "string", false, false),
                uOpt.genField("Board ID", "BID", "string", false, false)
            ],
            getMatch: uOpt.getMatch
        };
    };

    angular.module('mean.ecid').factory('srchOpt', srchOpt);
    srchOpt.$inject = ['utilsSrchOpt'];
})();