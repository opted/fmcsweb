'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var MBISQL = new Module('mbisql');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
MBISQL.register(function(app, auth, database, utils) {
  
  MBISQL.aggregateAsset('css', '../css/mbisql.css');
  //We enable routing. By default the Package Object is passed to the routes
  MBISQL.routes(app, auth, database, utils);
  MBISQL.angularDependencies(['agGrid']);
  return MBISQL;
});
