(function() {
  'use strict';
  /* jshint -W098 */
  var mbisqlCtrl = require('../controllers/mbisql');
  module.exports = function(MBISQL, app, auth, database,utils) {
    mbisqlCtrl .initDBConnection(utils); // I can't figure out why class constructor not work, use this function to replace it
    app.post('/api/mbisql/getsearchfield', mbisqlCtrl.getSearchField);
    app.post('/api/mbisql/querydata', mbisqlCtrl.MBIQueryData);
  };
})();
