'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Q = require('q'),
    _ = require('lodash');
var rebiCtrl = require('../../../../custom/rebi/server/controllers/rebi.js'),
    oqtool = require('../../../../custom/rebi/server/controllers/odbQueryTool.js');
var mbiconn = undefined, mesconn=undefined;//new mesclass();
var maxbinnum = 8;
var gvar = require('../../../utils/server/controllers/globalVariables');

var getMbiTableList = function(from,to){
  var tempdate = new Date(from);  
  var mbiTableList = [];
  while(tempdate<to){
    mbiTableList.push('MBI'+tempdate.getFullYear().toString().slice(2)+_.padStart(tempdate.getMonth()+1,2,'0'));
    tempdate = new Date(tempdate.setMonth(tempdate.getMonth()+1));
  }
  mbiTableList.push('MBI'+to.getFullYear().toString().slice(2)+_.padStart(to.getMonth()+1,2,'0'));
  return _.uniq(mbiTableList);
}

exports.initDBConnection = function(utils){
  mbiconn = utils.ctrls.mbi;
  mesconn = utils.ctrls.mes;
};

exports.getMBITableList = function(from,to){
  return getMbiTableList(from,to);
};

exports.example = function(req, res, next) {
  mbiconn.exec("select top 1 id,flowname,blockname,Driver_ID from MBI1712",[]).then(function(data){
    res.json(data);
  });  
};

var mesFieldMap = {'CUSTOMERNAME':'CUSTOMERCHISNAME','CUSTOMERLOTNO':'CUSTOMERLOTNO'};

exports.MBIQueryData = function(req, res, next) {
  req.setTimeout(180000); // 30 mins
  var match = req.body.match;
  var time = req.body.time;
  time.to = new Date(time.to);
  time.to = new Date(time.to.setSeconds(59,0));
  time.from = new Date(time.from);
  var mesqcmd='', mesWhereStr='', mesparams={fromDate:new Date(time.from),toDate:new Date(time.to)};
  mesqcmd = 'select distinct A.LOTNO,C.CUSTOMERCHISNAME,B.CUSTOMERLOTNO,B.DEVICENO,B.PACKAGETYPE from HISMFT.TBLWIPLOTLOG A '+
  'left join HISMFT.TBLWIPLOTDATA B on A.LOTNO=B.LOTNO '+
  'left join Masterdm.DIM_Customer C on B.CUSTOMERNO=C.CUSTNO where ';
  for(var key in match){
    if(mesFieldMap[key]){
      if(mesWhereStr.length>0)
        mesWhereStr+=' and ';
      if(key=='CUSTOMERNAME')
        mesWhereStr+="C.CUSTOMERCHISNAME='"+match[key]+"'";
      else if(key=='CUSTOMERLOTNO')
        mesWhereStr+="A.CUSTOMERLOTNOLOG='"+match[key]+"'";
      delete match[key];
    }
  }
  mesWhereStr+=' and A.CHECKOUTTIME between :fromDate and :toDate';
  mesqcmd+=mesWhereStr;
  mesconn.exec(mesqcmd,mesparams).then(function(mesresult){
    if(mesresult.length==0)
      return res.json([]); // mes result dose not has any data
    var meslotnos = _.map(mesresult,'LOTNO');
    var matchStr = '';
    var params = {};
    // find lotno by field CustomerName and CustomerLotNo from MES Table
    for(var key in match){
      if(matchStr.length>0)
        matchStr+=" and";
      if(key.startsWith('ZONE') || key.startsWith('SLOT'))
        matchStr+=" "+key+" in ("+match[key]+")";
      else if(key.startsWith('BID'))
        matchStr+=" SUBSTRING(board_id,2,3)='"+match[key]+"'";
      else
        matchStr+=" "+key+"='"+match[key]+"'";
    }
    if(!_.isEmpty(meslotnos)){ // append lotnos
      if(matchStr.length>0)
        matchStr+=" and";
      matchStr+=oqtool.insertListToQcmd(" lotnum in :meslotnos",{':meslotnos':meslotnos});
    }
    // Change Time Zone, mbi table store UTC+8 time to UTC+0
    time.to = new Date(time.to.setHours(time.to.getHours()+8));
    time.from = new Date(time.from.setHours(time.from.getHours()+8));
    params = {to:new Date(time.to),from:new Date(time.from)};
    
    var mbiTableList = getMbiTableList(time.from,time.to);
    var qcmdlist = [];
    mbiTableList.forEach(function(table){
      var qcmd = "select * from "+table+" where (log_time between @from and @to) and"+matchStr;
      qcmdlist.push(mbiconn.exec(qcmd,params));
    });
    
    Q.all(qcmdlist).then(function(result){
      result = _.flatten(result);
      if(result.length==0)
        return res.json([]); // mbi result dose not has any data
      var lotnoMesMap = {};
      for(var i in mesresult)
        lotnoMesMap[mesresult[i].LOTNO] = mesresult[i];
      // process data
      result.forEach(function(o){
        // set missing column
        o['BID'] = o['board_id'].slice(1,4);
        o['lotnum'] = _.trimEnd(o['lotnum']);
        var mapData = lotnoMesMap[o['lotnum']];
        if(!mapData)
          return;
        o['CustomerName'] = mapData['CUSTOMERCHISNAME'];
        o['CustomerLotNo'] = mapData['CUSTOMERLOTNO'];
        o['DeviceNo'] = mapData['DEVICENO'];
        o['PackageType'] = mapData['PACKAGETYPE'];
        o['TestQty']=0;
        o['FailQty']=0;
        o['Z/S']='Z'+o['Zone_ID']+'/S'+o['Slot_ID'];
        var binmap = o['binmap1']+o['binmap2'];
        var hbinmap = {};
        
        // Calculate Qty
        for(var i=0;i<binmap.length;i++){
          if(binmap[i]==1)
            o['TestQty']+=1;
          else if(binmap[i]>1){
            o['TestQty']+=1;
            o['FailQty']+=1;
          }
          if(hbinmap[binmap[i]])
            hbinmap[binmap[i]]+=1;
          else
            hbinmap[binmap[i]]=1;
        }
        // Fill Dut, dut has a special counting method
        var xsock=Number(o['xsock']), ysock=Number(o['ysock']);
        var count=0;
        for(var i=0;i<ysock;i++){
          for(var j=xsock-1;j>=0;j--){
            o['DUT'+count]=Number(binmap[i+ysock*j]);
            count+=1;
          }
        }
        
        // gen HBinList
        o['HBinList']='';
        for(var key in hbinmap){
          o['HBinList']+=key+':'+hbinmap[key]+';';
        }
        _.trimEnd(o['HBinList'],';');
        // process hbin data
        var hbinlist = o['HBinList'].split(';');
        for(var i=0;i<hbinlist.length;i++){
          var kv = hbinlist[i].split(':');
          var key = kv[0];
          var value = Number(kv[1]);
          if(key=='0')
            o['Empty']=value;
          else{
            o['HBin'+key]=value;
          }
        }
        if(o['Empty']==undefined)
          o['Empty']=0;
        o['Yield'] = _.round(((o['TestQty']-o['FailQty'])/o['TestQty'])*100,2);
        // Fill Empty HBin
        for(var i=1;i<=maxbinnum;i++){
          var binnum = String(i);
          if(o['HBin'+binnum]==undefined)
            o['HBin'+binnum]=0;
        }
      });
      // check continue fail, same ovenid, lotnum, boardid, dut fail in continue log_time
      // modify: do not check ovenid & lotnum anymore
      var fresult = _.filter(result,{'flowname':'summary','BlockName':'auto'});
      var groupByBoardID = _.groupBy(fresult,'board_id');
      for(var key in groupByBoardID){
        // sort by time
        var boardIdGroup = _.orderBy(groupByBoardID[key],'log_time','desc');
        var binmap = boardIdGroup[0]['binmap1']+boardIdGroup[0]['binmap2'];
        // init dut fail counter
        var prevDut = [];
        for(var i=0;i<binmap.length;i++)
          prevDut[i]=0;
        for(var i=0;i<boardIdGroup.length;i++)
        {
          var doc = boardIdGroup[i], predoc = undefined;
          if(i>0)
            predoc = boardIdGroup[i-1];
          for(var dutnum=0;dutnum<binmap.length;dutnum++){
            if(doc['DUT'+dutnum]>1 && prevDut[dutnum]>1){ // continue fail was occured
              doc['contFail'] = true;
              if(predoc)
                predoc['contFail'] = true;
            }
            prevDut[dutnum] = doc['DUT'+dutnum];
          }
        };
      }
        
      // set final Data
      // 同 board_id, oven_ID, lotnum, flowname, BlockName 最後時間點的資料
      var finalDataGroup = _.groupBy(result,function(o){
        return  o['board_id']+o['oven_ID']+o['lotnum']+o['flowname']+o['BlockName'];
      });
      for(var key in finalDataGroup){
        var groupItems = finalDataGroup[key];
        groupItems = _.orderBy(groupItems,'log_time','desc');
        groupItems[0]['finalData']=true;
      }
      return res.json(result);
    })
    .catch(function(err){
      console.log(err.stack);
      return res.status(400).json('query error! '+err);
    });
  })//mesconn
  .catch(function(err){
    console.log(err.stack);
    return res.status(400).json('mes table query error! '+err);
  });
};

exports.getSearchField = function(req, res, next) {
  var timerange = req.body.exmatch.datetime;
  var customernameList = req.body.exmatch.customer?req.body.exmatch.customer.name:undefined;
  var to = new Date(timerange.to),
    from = new Date(timerange.from),
    field = req.body.field,
    match = req.body.match; //{LOTNO:,CUSTOMERCHISNAME:}
  
  if(mesFieldMap[field]){ // field from mes table
    req.body = {exmatch:{datetime:{from:from,to:to}},match:{},field:mesFieldMap[field]};
    for(var key in match){
      if(mesFieldMap[key])
        req.body.match[mesFieldMap[key]] = match[key];
      else
        continue;
    }
    if(field=='CUSTOMERNAME'){
      req.body.fstr = match[field];
      req.body.field = 'customername';
      rebiCtrl.getMesMultiSelection(req,res, next);
    }
    else
      rebiCtrl.mesGetSearchField(req,res, next);
  }
  else{ // field from mbi table
    for(var key in mesFieldMap) // delete mes field
      delete match[key];
    var subfield = field=='BID'?'SUBSTRING(board_id,2,3) BID':field;
    var qcmd='';
    var params = {to:to,from:from};
    var matchStr = "";
    for(var key in match){
      if(matchStr.length>0)
        matchStr+=' and ';
      if(key=='BID')
        matchStr+="SUBSTRING(board_id,2,3) like '"+match[key]+"%'"
      else
        matchStr+=key+" like '"+match[key]+"%'" // expression of mssql satrts with , ex: Hello%, starts with Hello
    }
    // Check required table between datetime to and from
    var mbiTableList = getMbiTableList(from,to);
    var qcmdlist = [];
    mbiTableList.forEach(function(table){
      var qcmd= "select top "+gvar.searchFilterMaximumRowNum+" "+field+" from "+
      // sub table
      "(select distinct "+subfield+" from (select * from "+table+" where (log_time between @from and @to)) subTable1 where "+matchStr+") subTable2";
      //console.log(qcmd,params);
      qcmdlist.push(mbiconn.exec(qcmd,params));
    });
    Q.all(qcmdlist).then(function(result){
      result = _.flatten(result);
      result = _.map(result,field);
      result = _.map(result,function(o){ return o.replace(/ /g,'')});
      result = _.orderBy(result);
      return res.json(_.uniq(result));
    })
    .catch(function(err){
      return res.status(400).json('query error!');
    });
  }
};
