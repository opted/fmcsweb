(function() {
'use strict';
  function MBISQL($http,$q,_,Rebi) {
    var groupByNameMapping = {'DATE':'時間','board_id':'BoardID','BID':'BID','driver_id':'DriverID','Zone_ID':'ZoneID','Z/S':'ZoneID/SlotID'};
    var level2GroupBy = ['board_id','BID','driver_id','Zone_ID','Z/S'];
    var MbisqlService = Object.create(Rebi); // MbiSql Service inheritance from Rebi Service
    // Defined new function
    MbisqlService.pyGroupByMapping = function(){
      return groupByNameMapping;
    };
    MbisqlService.fyGroupByMapping = function(){
      return groupByNameMapping;
    };
    MbisqlService.initPassYieldChartParams = function(rawdata){
      return this.getChartParams(0,rawdata,['DATE'],'py');
    };
    MbisqlService.initFailYieldChartParams = function(rawdata){
      return this.getChartParams(0,rawdata,['DATE'],'fy');
    };
    // Override function
    MbisqlService.getChartData = function(chartParams){
      var rawdata = chartParams.data;
      var selected = chartParams.groupby.selected;
      var ctype = chartParams.ctype;

      // 1. group by selected
      var groupMap = _.groupBy(rawdata,selected);
      // 2. calculate by selected
      var chartData = [];
      for(var key in groupMap){
        var groupData = groupMap[key];
        var sdata = {key:key};
        // 2.1 cal testqty, passrate or failrate
        
        var testqty =_.sumBy(groupData,'TestQty');
        var failqty =_.sumBy(groupData,'FailQty');
        sdata['testqty']=testqty;
        sdata['failqty']=failqty;
        if(ctype=='py'){
          sdata['passrate']=(testqty-failqty)/testqty;
        }
        else if(ctype=='fy')
        {
          sdata['failrate']=failqty/testqty;
          if(selected=='board_id')
            for(var i=1;i<=8;i++)
              sdata['bin'+i]=_.sumBy(groupData,'HBin'+i);
        }
        
        for(var key in sdata)
          if(key=='passrate' || key=='failrate')
            sdata[key] = _.round((sdata[key]*100),2);
            //sdata[key]=String(_.round((sdata[key]*100),2))+'%';
        chartData.push(sdata);
      }
      chartParams.allshowdata = chartData;
      chartParams.allshowdata = this.sortingAllShowData(chartParams);
      var limitselected = chartParams.limit.selected;
      var newChartData = [];
      // handle limit item part
      if(limitselected=='all' || limitselected>=chartParams.allshowdata.length)
        newChartData = chartParams.allshowdata;
      else
        for(var i=0;i<limitselected;i++)
          newChartData.push(chartParams.allshowdata[i]);
      return newChartData;
    };
    MbisqlService.getChartParams = function(level,rawdata,groupbylist,chartType){
      // Iinitial status was defined as following
      var chartParams = Rebi.initBasicChartParams(level,rawdata,groupbylist,chartType);
      chartParams.showdata = this.getChartData(chartParams);
      chartParams.tmpgroupresult[chartParams.groupby.selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
      return chartParams;
    };
    MbisqlService.getDrillDownParams = function(chartParams,category){
      if(chartParams.level>=1){
        alert('Can not drill down, the maximum levle is 1.');
        return undefined;
      }
      var key = _.keys(category)[0];
      // 1. get new rawdata
      var rawdata = _.filter(chartParams.data,function(o){
        if(o[key]==category[key])
          return true;
        else
          return false;
      });
      // 2. get new groupbylist
      // drill down level 
      var groupbylist = level2GroupBy;
      var ddChartParams = this.getChartParams(chartParams.level+1,rawdata,groupbylist,chartParams.ctype);
      // 4. set drillpath
      ddChartParams.drillpath = _.cloneDeep(chartParams.drillpath);
      ddChartParams.drillpath.push(category);
      return ddChartParams;
    };
    MbisqlService.changeGroup = function(chartParams,selected){
      chartParams.groupby.selected = selected;
      if(chartParams.tmpgroupresult[selected]){ //defined
        var tmpresult = chartParams.tmpgroupresult[selected];
        chartParams.showdata = tmpresult.showdata;
        chartParams.allshowdata = tmpresult.allshowdata;
        chartParams.sorting = tmpresult.sorting;
        chartParams.limit = tmpresult.limit;
      }
      else{ // undefined
        chartParams.showdata = this.getChartData(chartParams);
        chartParams.sorting = _.cloneDeep(chartParams.sorting);
        chartParams.limit = _.cloneDeep(chartParams.limit);
        // 暫存 groupby 資料, 這部分資料在切換 groupby, limit, sorting 時要確認是否確實置換
        chartParams.tmpgroupresult[selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
      }
      return chartParams;
    };
    MbisqlService.chartChangeGroup = function(scope,chart,item){
      if(scope.drillDownTriggerChangeGroup){
        // drill down cilck will make new groupby tabset and trigger this function, 
        // do not trigger at this time.
        scope.drillDownTriggerChangeGroup = false;
        return;
      }
      scope.curChartParams = this.changeGroup(scope.curChartParams,item);
      chart.dataProvider = scope.curChartParams.showdata;
      chart.validateData();
      chart.validateNow();
    };
    MbisqlService.chartDrillDown = function(scope,chart,graphItemCategory){
      // get new chart params
      var category = {};
      category[scope.curChartParams.groupby.selected] = graphItemCategory;
      var newChartParams = this.getDrillDownParams(scope.curChartParams,category);
      if(newChartParams){
        Rebi.setDrillDownTab(scope,newChartParams);
        Rebi.chartDrillDownAutoChangeTab(scope);
        scope.$apply();
      }
      else
        return;
    };
    MbisqlService.setSortinField = function(chartParams){
      if(chartParams.sorting.selected.indexOf('passrate')>=0)
        chartParams.sorting.field = 'passrate';
      else if(chartParams.sorting.selected.indexOf('failrate')>=0)
        chartParams.sorting.field = 'failrate';
      else
        chartParams.sorting.field = undefined;
    };
    MbisqlService.sortingAllShowData = function(chartParams){
      this.setSortinField(chartParams);
      return Rebi.sortingAllShowData(chartParams);
    };
    MbisqlService.chartChangeSorting = function(scope,chart,changeItem){
      this.setSortinField(scope.curChartParams);
      return Rebi.chartChangeSorting(scope,chart,changeItem);
    };
    return MbisqlService;
  }
  angular
      .module('mean.mbisql')
      .factory('MBISQL', MBISQL);
  MBISQL.$inject = ['$http','$q','_','Rebi'];
})();