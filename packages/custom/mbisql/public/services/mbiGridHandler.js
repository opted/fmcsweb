(function() {
'use strict';
  function mbiGridHandler($http,common,_) {
    var hbinFieldList = ['ID','board_id','oven_ID','lotnum','Zone_ID','Slot_ID','xsock','ysock','binmap',
      'log_time','DATE','bipgm','flowname','BlockName','PCValue','BoardStatus',
      'driver_id','TestQty','FailQty','Empty','Yield','DeviceNo',
      'PackageType','CustomerName',
      'BID','CustomerLotNo','socketmap','HBinList'];
    var dutbinFieldList = ['ID','board_id','oven_ID','lotnum','Zone_ID','Slot_ID','xsock','ysock','binmap',
      'log_time','DATE','bipgm','flowname','BlockName','PCValue','BoardStatus',
      'driver_id','TestQty','FailQty','Yield','DeviceNo',
      'PackageType','CustomerName',
      'BID','CustomerLotNo','socketmap','HBinList'];
	var numberFields = ['ID','Zone_ID','Slot_ID','xsock','ysock','PCValue','BoardStatus','TestQty','FailQty','Empty'];
    var maxbinnum = 8;
	
    var defineColumn = function(key,columnDefs,maxdutcount,type){
      if(key=='FailQty' && type=='hbin'){
        columnDefs.push({headerName: key, field: key, filter: 'text', width: 100});
        for(var i=1;i<=maxbinnum;i++)
          columnDefs.push({headerName: 'HBin'+i, field: 'HBin'+i, filter: 'text', width: 80});
      }
      else if(key=='FailQty' && type=='dut'){
        columnDefs.push({headerName: key, field: key, filter: 'text', width: 100});
        for(var i=0;i<maxdutcount;i++)
          columnDefs.push({headerName: 'DUT'+i, field: 'DUT'+i, filter: 'text', width: 80});
      }
      else if(key=='driver_id'){
        columnDefs.push({headerName: key, field: key, width: 100, cellClass:"textFormat"});
	  }
      else if(key=='binmap'){
        columnDefs.push({headerName: 'binmap', field: 'binmap1', filter: 'text', width: 120, cellClass:"textFormat", valueGetter: function(params) {
          return params.data.binmap1+params.data.binmap2;
        }});
	  }
      else if(key=='socketmap'){
        columnDefs.push({headerName: 'socketmap', field: 'socketmap1', filter: 'text', width: 120,valueGetter: function(params) {
          return params.data.socketmap1+params.data.socketmap2;
        }});
	  }
	  else if(key=='Yield'){
        columnDefs.push({headerName: 'Yield', field: 'Yield', filter: 'text', width: 100,cellRenderer:common.agGridcellRendererAddPercent});
	  }
      else{
		//var filtertype = _.indexOf(numberFields,key)>=0?'number':'text';
        columnDefs.push({headerName: key, field: key, filter: 'text', width: 100});
	  }
    }
    return {
      getHBinColumnDefs: function(){
        var HBinColumnDefs = [];
        hbinFieldList.forEach(function(key){
          defineColumn(key,HBinColumnDefs,0,'hbin');
        });
        HBinColumnDefs.push({headerName: 'contFail', field: 'contFail', width: 80, hide:true});
        HBinColumnDefs.push({headerName: 'finalData', field: 'finalData', width: 80, hide:true});
        return HBinColumnDefs;
      },
      getDutBinColumnDefs: function(maxdutcount){
        var DutBinColumnDefs = [];
        dutbinFieldList.forEach(function(key){
          defineColumn(key,DutBinColumnDefs,maxdutcount,'dut');
        });
        DutBinColumnDefs.push({headerName: 'contFail', field: 'contFail', width: 80, hide:true});
        DutBinColumnDefs.push({headerName: 'finalData', field: 'finalData', width: 80, hide:true});
        return DutBinColumnDefs;
      }
    }
  }
  angular
      .module('mean.mbisql')
      .factory('mbiGridHandler', mbiGridHandler);
  mbiGridHandler.$inject = ['$http','common','_'];
})();