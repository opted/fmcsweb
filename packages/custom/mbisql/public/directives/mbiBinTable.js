(function(){
	'use strict';
	function mbiBinTable(common,mbiGridHandler,_){
		return {
			restrict: 'E',
			scope: {
				data: '='
			},
			template: 
      '<div ng-show="display">'+
        `<div style="margin-top:15px;"><label class="checkbox-inline">
          <input type="checkbox" ng-model="contFail.label" ng-change="changeContinueFailLable()">標示Continuous Failure</input>
        </label>
        <label class="checkbox-inline">
          <input type="checkbox" ng-model="contFail.showonly" ng-change="setGridFilter('contFail')">只顯示Continuous Failure資料</input>
        </label><label class="checkbox-inline">
          <input type="checkbox" ng-model="contFail.finaldata" ng-change="setGridFilter('finalData')">只顯示Final資料</input>
        </label></div>`+
        '<h3>HBin分佈</h3>'+
        '<div class="row" style="padding-left:20px;"><div style="height:350px;" class="ag-blue" ag-grid="HBinGrid"></div>\
        <button class="btn btn-success" ng-click="exportAgGrid(\'hbin\',\'xls\')">下載Excel</button></div>'+
        '<hr><h3>DutBin分佈</h3>'+
        '<div class="row" style="padding-left:20px;"><div style="height:350px;" class="ag-blue" ag-grid="DutBinGrid"></div>\
        <button class="btn btn-success" ng-click="exportAgGrid(\'dutbin\',\'xls\')">下載Excel</button></div>'+
      '</div>'
      , // chart
			link: function (scope, element, attrs) {
        scope.display = false;
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined)
            return;
          var rawdata = scope.data;
          if(_.isEmpty(rawdata)!=true){
            scope.display = true;
            var maxbincount=0;
            rawdata.forEach(function(o){
              var binmap = (o['binmap1']+o['binmap2']);//.replace(/ /g,'');
              maxbincount=Math.max(maxbincount,binmap.length);
            });
            // defined hbin column
            var HBinColumnDefs = mbiGridHandler.getHBinColumnDefs(maxbincount);
            // defined dutbin column
            var DutBinColumnDefs = mbiGridHandler.getDutBinColumnDefs(maxbincount);
            // set ag-grid rowdata
            scope.rawdata = rawdata;
            scope.HBinGrid.api.setColumnDefs(HBinColumnDefs);
            scope.DutBinGrid.api.setColumnDefs(DutBinColumnDefs);
            scope.HBinGrid.api.setRowData(rawdata);
            scope.DutBinGrid.api.setRowData(rawdata);
          }
          else
            scope.display = false;
				});
			},//link
    controller:['$scope',function($scope){
      /**
        MBI Bin Ag-Grid
      */
      // Column number not a fixed value, dynamically proccess it
      $scope.rawdata = [];
      $scope.contFail = {label:false,showonly:false,finaldata:false};
      var changeFilter = function(){
        if($scope.contFail.showonly)
         $scope.setGridFilter('contFail');
        if($scope.contFail.finaldata)
         $scope.setGridFilter('finalData');
      }
      $scope.changeContinueFailLable = function(){
        $scope.DutBinGrid.api.setRowData($scope.rawdata);
        $scope.HBinGrid.api.setRowData($scope.rawdata);
        changeFilter();
      };
      $scope.setGridFilter = function(field){
        var gridOptions = {'hbin':$scope.HBinGrid,'dutbin':$scope.DutBinGrid};
        var checkStatment = false;
        if(field=='contFail')
          checkStatment = $scope.contFail.showonly;
        else if(field=='finalData')
          checkStatment = $scope.contFail.finaldata;
        
        for(var key in gridOptions){
          var filterComponent = gridOptions[key].api.getFilterInstance(field);
          if(checkStatment){
            filterComponent.selectNothing();
            filterComponent.selectValue(true);
            gridOptions[key].api.onFilterChanged();
          }
          else{
            filterComponent.selectEverything();
            gridOptions[key].api.onFilterChanged();
          }
        }
      };
      $scope.HBinGrid = {
        columnDefs:[],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true,
        getRowStyle:function(params){
          if (params.data['contFail']==true && $scope.contFail.label) {
            return {'background-color': 'lightsalmon'};
          }
        },
        excelStyles:[{id:'textFormat', dataType:'string'}]
      };
      $scope.DutBinGrid = _.cloneDeep($scope.HBinGrid);
      $scope.exportAgGrid = function(gridType,fileType){
        if(gridType=='hbin')
          common.exportAgGrid('hbin_rawdata',fileType,$scope.HBinGrid);
        else if(gridType=='dutbin')
          common.exportAgGrid('dutbin_rawdata',fileType,$scope.DutBinGrid);
      };
    }]//controller
		};
	};

	angular.module('mean.mbisql').directive('mbiBinTable', mbiBinTable);
	mbiBinTable.$inject = ['common','mbiGridHandler','_'];
})();
'use strict';