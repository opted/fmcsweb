
(function(){
	'use strict';
	function mbiPassYieldChart(MBISQL,common,mbiGridHandler,_){
    var chartName = "mbisql-py-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=' // Data {data:rawdata,ftype:{rebi:selectedtype,yield:selectedtype}}
			},
			template: 
      // drilldown tab list
      '<div ng-show="display"><uib-tabset active="drillDownTabActive">\
        <uib-tab ng-repeat="tab in drillTablist" ng-click="changeDrillDownTab($index)" index="tab.id">\
        <uib-tab-heading>\
          {{tab.title}}<button type="button" class="close" aria-label="Close" style="font-size:medium" ng-click="closeTab($index)" ng-show="$index>0">\
          <span aria-hidden="true">&times;</span>\
          </button>\
        </uib-tab-heading>\
        </uib-tab>\
      </uib-tabset>'+
      // tab title
      '<h3>{{ curChartParams.tabtitle }}</h3>'+
      // grouping button
      // render all pill tabset and hide unneeded tabset to fix a bug
      '<div ng-repeat="tab in drillTablist"><uib-tabset class="rebi-groupby" type="pills" ng-show="selectedTabItemIndex==$index">\
      <uib-tab index="$index" ng-repeat="item in tab.chartparams.groupby.itemlist" heading="{{GroupByMapping[item]}}" select="changeGroup(item)">\
      </uib-tab>\
      </uib-tabset></div>'+
      // using hint
      '<div class="row">\
      <label ng-show="curChartParams.level==0" class="text-primary control-label col-sm-12 vcenter">* 點擊一次 Drill down</label>\
      <label ng-show="curChartParams.level>=1" class="text-primary control-label col-sm-12 vcenter">* 點擊一次顯示Dut疊圖 </label></div>'+
      // sorting select
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">排序</label>\
        <select vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglist" ng-change="changeSorting(0)">\
        </select>\
        <div class="btn-group vcenter">\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="0" ng-click="changeSorting(1)">降序</label>\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="1" ng-click="changeSorting(2)">升序</label>\
        </div>\
      </div>'+
      // amchart
      '<div id="'+chartName+'" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      '<div class="stackmap" ng-show="curChartParams.level>=1"><hr><h3>Dut疊圖分析</h3>\
      <label class="text-default">Stack By {{curChartParams.dutMap.by}}</label>\
      <dut-map rawdata="curChartParams.dutMap.data"></dut-map>\
      <hr><h3>疊圖 Raw Data</h3>\
      <div class="row" style="padding-left:20px;"><div style="height:300px;" class="ag-blue" ag-grid="DutMapGrid"></div>\
        <button class="btn btn-success" ng-click="exportAgGrid(\'dutmap\',\'xls\')">下載Excel</button></div>',
			link: function (scope, element, attrs) {
        scope.display = false;
        /**
          Chart parameters, use to drilldown, sorting, limit or change group by
        */
        /*
          Chart params structure
          {
          level:,//drill down 等級，初始等級為0
          groupby:{itemlist:["LOTNO","ROOTLOT",...],selected:"LOTNO",active:0}, // 分群用項目
          sorting:{selected:'X軸字典排序',type:0},//可用的排序規則記在外部
          limit:{selected:'X軸字典排序',type:0}, //圖表呈現上限
          drillpath:"",
          rawdata:[],
          showdata:[]
          }
        */
        scope.curChartParams = undefined;
        scope.GroupByMapping = MBISQL.pyGroupByMapping();
        scope.changeGroup = function(item){
          MBISQL.chartChangeGroup(scope,chart,item);
          //scope.chartGridOptions.api.setRowData(scope.curChartParams.allshowdata);
        };
        // limit selected
        scope.limitlist = [10,20,50,"all"];
        scope.changeLimit = function(){
          MBISQL.chartChangeLimit(scope,chart);
        };
        // sorting selected
        scope.sortinglist = ['X軸字典排序','testqty 數值排序','passrate 數值排序'];
        scope.changeSorting = function(changeItem){
          MBISQL.chartChangeSorting(scope,chart,changeItem);
        };
        /**
          Drill down control
        */
        scope.drillDownTriggerChangeGroup=false;
        scope.drillTablist = [];
        scope.drillDown = function(graphItemCategory){
          MBISQL.chartDrillDown(scope,chart,graphItemCategory);
        };
        scope.triggerCloseTab = false;
        scope.selectedTabItemIndex = 0;
        scope.changeDrillDownTab = function(index){
          MBISQL.chartChangeDrillDownTab(scope,chart,index);
          if(scope.curChartParams.dutMap){
            scope.DutMapGrid.api.setRowData(scope.curChartParams.dutMap.data);
          }
        };
        scope.closeTab = function(tabindex){
          MBISQL.chartCloseTab(scope,tabindex);
        };
        scope.showDutMap = function(){
          if(scope.curChartParams)
            if(scope.curChartParams.groupby.selected=='board_id' && scope.curChartParams.dutMap)
              return true;
            else
              return false;
          else
            return false;
        }
        /**
          Defined amchart options
        */
        var ballonFunction = common.amChartCustomizeBallonFunction();
        var chartOptions = {
          "type": "serial",
          "addClassNames": true,
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
            "useGraphSettings": true,
            "markerSize":12,
            "valueWidth":0,
            "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [ {
            "id": "v1",
            "title": "TestQty",
            "position": "left"
          },
          {
            "id": "v2",
            "title": "PassRate",
            "position": "right",
            "maximum":100,
            "labelFunction": function(value) {
              return value+"%";
            }
          }],
          "startDuration": 1,
          "graphs": [ {
            "id":"testqty",
            "balloonFunction": ballonFunction.basic,
            "lineColor":"#72b1e4",
            "fillAlphas": 1,
            "title": "test qty",
            "type": "column",
            "valueField": "testqty",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthColumn",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },
          {
            "id": "passrate",
            "balloonFunction": ballonFunction.percent,
            "lineColor":"#119828",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "pass rate",
            "valueField": "passrate",
            "valueAxis": "v2",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          } ],
          "categoryField": "key",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":10
          },
          "export": {
            "enabled": true
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
          "chartScrollbar": {
          },
          "listeners": [{
            "event": "clickGraphItem",
            "method": function(event) {
              if(scope.curChartParams.level==0){
                var clickItem = scope.GroupByMapping[scope.curChartParams.groupby.selected]+': '+event.item.category;
                var ok = confirm('Drill down from '+clickItem);
                if(ok){
                  scope.drillDownTriggerChangeGroup = true;
                  scope.drillDown(event.item.category);
                }
              }
              else{
                var selected = scope.curChartParams.groupby.selected;
                //if(selected=='board_id'){
                  // Get dut stack map for click board id
                  var drillpath = scope.curChartParams.drillpath;
                  var filterCondition = {};
                  drillpath.forEach(function(pathItem){
                    for(var key in pathItem){
                      filterCondition[key]=pathItem[key];
                    }
                  });
                  if($.isNumeric(event.item.category))
                    filterCondition[selected] = Number(event.item.category);
                  else
                    filterCondition[selected] = event.item.category;
                  var stackByMsg = scope.GroupByMapping[selected]+': '+event.item.category;
                  var dutmapdata = _.filter(scope.rawData,filterCondition);
                  scope.curChartParams.dutMap = {'data':_.filter(scope.rawData,filterCondition),'by':stackByMsg};
                  // set dut map grid rawdata
                  // defined column
                  var maxbincount = dutmapdata[0]['xsock']*dutmapdata[0]['ysock'];
                  var DutMapColumnDefs = mbiGridHandler.getDutBinColumnDefs(maxbincount);
                  // set ag-grid rowdata
                  scope.DutMapGrid.api.setColumnDefs(DutMapColumnDefs);
                  scope.DutMapGrid.api.setRowData(dutmapdata);
                  setTimeout(function(){
                    scope.$apply();
                    window.scrollTo(0,document.body.scrollHeight);
                  },100);
                //}
              }
            }
          }]
        };

				var chart = AmCharts.makeChart(chartName, chartOptions);
        var zoomChart = function(){
          try{
            chart.zoomToIndexes(0,20);
          }
          catch(e){} // data size less than 20
        }
        chart.addListener("dataUpdated", zoomChart);
        /**
          Whating the data link of outer controller
        */
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined)
            return;
          var rawdata = scope.data;
          scope.rawData = scope.data;
          if(_.isArray(rawdata)){
            if(rawdata.length>0){
              scope.display = true;
              scope.curChartParams = MBISQL.initPassYieldChartParams(rawdata);
              scope.drillTablist = [];
              scope.drillTablist.push(MBISQL.getInitDrillTab(scope.curChartParams));
              chart.dataProvider = scope.curChartParams.showdata;
              chart.validateData();
              chart.validateNow();
              //scope.chartGridOptions.api.setRowData(scope.curChartParams.allshowdata);
              setTimeout(function(){
                scope.drillDownTabActive = scope.drillTablist[0].id;
                scope.$apply();
              },100);
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
				chart.validateData();
				chart.validateNow();
			},//link
    controller:['$scope',function($scope){
      /**
        Chart Data Grid
      */
      var chartDataGridColumnDefs = [
        {headerName: 'KEY', field: 'key', filter: 'text', width: 150},
        {headerName: 'FailQty', field: 'failqty', filter: 'text', width: 100},
        {headerName: 'TestQty', field: 'testqty', filter: 'text', width: 100},
        {headerName: 'PassRate', field: 'passrate', filter:'text', width: 100}
      ];
      $scope.chartGridOptions = {
        columnDefs:chartDataGridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.DutMapGrid = {
        columnDefs:[],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };
      $scope.exportAgGrid = function(gridType,fileType){
        if(gridType=='dutmap')
          common.exportAgGrid('dutmap_rawdata',fileType,$scope.DutMapGrid);
      };
    }]
		};
	};

	angular.module('mean.mbisql').directive('mbiPassYieldChart', mbiPassYieldChart);
	mbiPassYieldChart.$inject = ['MBISQL','common','mbiGridHandler','_'];
})();
'use strict';