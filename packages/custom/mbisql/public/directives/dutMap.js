(function(){
  'use strict';
  function dutMap(_){
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        rawdata: '='
      },
      templateUrl: 'mbisql/views/dutMap.html',
      link: function(scope, element, attrs){
        scope.dutMaps = [];
        scope.defaultColorSet = [
          { min: -1, max: -1, color: '#FFFFFF'},
          { min: 0, max: 0, color: '#3CB15C'},
          { min: 1, max: 1, color: '#FF7763'},
          { min: 2, max: 10, color: '#FF2C20'},
          { min: 11, max: 'max', color: '#993539'},
        ];
        scope.colorSet = _.cloneDeep(scope.defaultColorSet);
        scope.$watch('rawdata', function(newVal, oldVal){
          if (_.isEmpty(scope.rawdata)){
            return;
          };
          scope.dutMaps = [];
          var xsock = scope.rawdata[0].xsock;
          var ysock = scope.rawdata[0].ysock;
          var maps = [];
          _.forEach(scope.rawdata, function(data){
            var binmap = data['binmap1']+data['binmap2'];
            var map = [];
            _.forEach(_.range(0,ysock), function(index){
              map.push([])
            })
            var i = 0;
            var j = ysock;
            while(j <= binmap.length) {
              var sliceData = binmap.slice(i,j);
              _.forEach(sliceData, function(val, index){
                map[index].unshift(val);
              });
              i += ysock;
              j += ysock;
            };
            maps.push(map)
          });
          var sumMap = [];
          _.forEach(_.range(0,ysock), function(yIndex){
            var tmpRow = [];
            _.forEach(_.range(0,xsock), function(xIndex){
              var count = _.countBy(_.map(_.map(maps, yIndex), xIndex));
              var fail = 0;
              for(var key in count){
                if(Number(key)>=2)
                  fail+=count[key];
              }
              tmpRow.push({'FAIL': fail, 'PASS': count['1']||0 })
            });
            sumMap.push(tmpRow);
          });
          scope.dutMaps = sumMap;
        }, true);

        scope.btnStyle = function(dut){
          var style = {};
          var emptyColor = _.find(scope.colorSet, {min:-1, max:-1})
          if (dut.FAIL === 0 && dut.PASS === 0) {
            style["background-color"] = emptyColor.color;
            return style;
          }
          _.forEach(scope.colorSet, function(config){
            if ( config.min === 'N/A' || config.max === 'N/A') {
              return;
            };
            if (dut.FAIL >= config.min && config.max === 'max') {
              style["background-color"] = config.color;
              return false;
            } else if (dut.FAIL >= config.min && dut.FAIL <= config.max ) {
              style["background-color"] = config.color;
              return false;
            }
          });
          return style;
        }

        scope.exportSocketMap = function(id){
          html2canvas($("#dutmap-" + id), {
            background: "#ffffff",
            onrendered: function(canvas) {
              var url = canvas.toDataURL();
              $("<a>", {
                href: url,
                download: "dutmap.png"
              })
              .on("click", () => $(this).remove())
              .appendTo("body")[0].click()
            }
          });
        }
        scope.addColorRange = function(){
          scope.colorSet.push({min: 'N/A', max: 'N/A', color: '#000000'})
        };
        scope.recoveryColor = function(){
          console.log('reset') ;
          scope.colorSet = _.cloneDeep(scope.defaultColorSet);
        };
      },
    };
  };
  angular.module('mean.mbisql').directive('dutMap', dutMap);
  dutMap.$inject = ['_'];
})();