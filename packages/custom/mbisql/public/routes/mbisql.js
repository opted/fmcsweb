(function() {
    'use strict';

    function MBISQL($stateProvider) {
        $stateProvider.state('mbisql main page', {
            url: '/mbisql',
            templateUrl: 'mbisql/views/index.html',
            controller:MBISQLController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              }
            }
        });
    }

    angular
        .module('mean.mbisql')
        .config(MBISQL);

    MBISQL.$inject = ['$stateProvider'];

})();
