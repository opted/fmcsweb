'use strict';
var openFile = undefined;
var daySeconds = 24*60*60*1000;
function MBISQLController($scope, Global, $stateParams, $window, $http, utilsSrchOpt, common, _) {
  /*
    search panel
  */
  $scope.selected = {datetype:'day'};
  $scope.mbifields = [
    utilsSrchOpt.genField("客戶別(CustomerName)", "CUSTOMERNAME", "string", true, true),
    utilsSrchOpt.genField("FlowName", "FLOWNAME", "string", true, true),
    utilsSrchOpt.genField("BlockName", "BLOCKNAME", "string", true, true),
    utilsSrchOpt.genField("客批(CustomerLotNo)", "CUSTOMERLOTNO", "string", true, true),
    utilsSrchOpt.genField("內批(Lotnum)", "LOTNUM", "string", true, true),
    utilsSrchOpt.genField("爐號(OvenID)", "OVEN_ID", "string", true, true),
    utilsSrchOpt.genField("BID", "BID", "string", true, true),
    utilsSrchOpt.genField("BoardID", "BOARD_ID", "string", true, true),
    utilsSrchOpt.genField("BIProgram", "BIPGM", "string", true, true),
    utilsSrchOpt.genField("DriverID", "DRIVER_ID", "string", true, true)
  ];
  $scope.mbifsURL = '/api/mbisql/getsearchfield';
  $scope.filterExtraMatch = {datetime:$scope.selected};
  $scope.resetTime = function(){
    var curtime = new Date();
    curtime.setHours(23,59,59,999);
    $scope.selected.from = new Date(common.getToday().getTime()-daySeconds);
    $scope.selected.to = curtime;
  };
  $scope.resetTime();
  // select zone & slot
  $scope.zoneSlotOpts = {zone:{selected:[],list:[0,1,2,3]},slot:{selected:[],list:[]}};
  for(var i=0;i<14;i++)
    $scope.zoneSlotOpts.slot.list.push(i);
  /*
   query summary information
  */
  $scope.requestStatus = undefined;
  $scope.rowNum = undefined;
  $scope.waitingResponse = false;
  var startQuery = function(){
    $scope.startTime = new Date();
    $scope.waitingResponse = true;
    $scope.requestStatus = undefined;
    $scope.rowNum = undefined;
  };
  var endQuery = function(err,length){
    $scope.waitingResponse = false;
    if(err){
      $scope.requestStatus = 'Fail! '+err;
    }
    else{
      $scope.rowNum = length;
      $scope.requestStatus = 'Success';
    }
    $scope.queryTime = new Date() - $scope.startTime;
  };
  /*
   query function
  */
  $scope.queryData = function(){
    var params = {};
    params.time = {from:$scope.selected.from,to:$scope.selected.to};
    // test parameters
    params.match = utilsSrchOpt.getMatch($scope.mbifields);
    //append zoneid and slotid
    if(!_.isEmpty($scope.zoneSlotOpts.zone.selected))
      params.match['ZONE_ID'] = $scope.zoneSlotOpts.zone.selected.toString();
    if(!_.isEmpty($scope.zoneSlotOpts.slot.selected))
      params.match['SLOT_ID'] = $scope.zoneSlotOpts.slot.selected.toString();
    if(params.match.CUSTOMERNAME==undefined || params.match.FLOWNAME==undefined || params.match.BLOCKNAME==undefined){
      return $window.alert('CustomerName, FlowName, BlockName 這三個欄位為必填欄位!另外需選填一個欄位，合計共四個過查詢條件。');
    }
    else{
      var otherFilter = false;
      for(var key in params.match){
        if(key!='CUSTOMERNAME' && key!='FLOWNAME' && key!='BLOCKNAME'){
          if(params.match[key]){
            otherFilter = true;
            break;
          }
        }
        else if(key=='CUSTOMERNAME'){
          params.match[key] = params.match[key].split(',')[0];
        }
      }
      if(!otherFilter)
        return $window.alert('除了 CustomerName, FlowName, BlockName 三個欄位外，還需要另外選填一個欄位，合計共四個過查詢條件!');
    }
    //params.match = {CUSTOMERNAME:'nVIDIA',FLOWNAME:'summary',BLOCKNAME:'auto',ZONE_ID:'0'};
    $scope.queryTime = undefined;
    startQuery();
    $http.post('/api/mbisql/querydata',params)
    .then(function(response){
      if($scope.selected.datetype=='day'){
        response.data.forEach(function(o){
          o['DATE'] = o['log_time'].slice(0,10);
        });
      }
      if($scope.selected.datetype=='week'){
        response.data.forEach(function(o){
          var logtime = new Date(o['log_time'].slice(0,10));
          o['DATE'] = o['log_time'].slice(0,4)+'W'+_.padStart(common.getWeek(logtime),2,'0');
        });
      }
      else if($scope.selected.datetype=='month'){
        response.data.forEach(function(o){
          o['DATE'] = o['log_time'].slice(0,7);
        });
      }
      $scope.rawData = response.data;
      endQuery(undefined,response.data.length);
    },
    function(err){
      console.log(err);
      endQuery(err);
    });
  };
}

angular
    .module('mean.mbisql')
    .controller('MBISQLController', MBISQLController);

MBISQLController.$inject = ['$scope', 'Global', '$stateParams', '$window', '$http', 'utilsSrchOpt', 'common','_'];