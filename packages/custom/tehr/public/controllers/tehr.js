'use strict';

function TehrController($scope, $rootScope, $timeout, _, tso, tqs, tts, tcs, tfm) {
  $scope.selected = {
    'from': moment().startOf('day')._d,  // Set default start day
    'to': moment().startOf('day')._d,  // Set default end day
    'fmode': null,  // 選擇的 Failure mode
    'schema': "HISMFT",  // 預設 Query schema 為 HISMFT
    'excludedStatus': [{status: "異常單作廢", val: 10}] // 預設排除 "異常單作廢"
  };

  // Status 篩選項目
  $scope.status_list = [
    {status: "第一處理單位", val: 0},
    {status: "相關處理單位", val: 1},
    {status: "客戶處理", val: 2},
    {status: "異常處置或異常解除", val: 4},
    {status: "執行結果", val: 5},
    {status: "執行確認", val: 6},
    {status: "異常單作廢", val: 10},
    {status: "異常單已結單", val: 11} 
  ];
  

  $scope.isSearching = tqs.isSearching;  // 卡介面操作
  $scope.fields = tso.fields;  // 網頁的查詢欄位
  $scope.fieldSearchURL = "/api/tehr/getsearchfield";
  $scope.tabs = tts;

  // Tab 數量增減時，跳最後一個
  $scope.$watch('tabs.getSize()', nv => nv > 0 ? $timeout(() => $rootScope.active_tehr_tabs = $scope.tabs.getLastTab()) : {});

  // 設定 Query Service 的 Schema 及取得 Failure Mode list
  $scope.$watch('selected.schema', nv => {
    if (nv) {
      tqs.setSchema(nv);
      if (nv == "HISLFT") alert("[ 警告 ] LFT 之 lot 資料量遠大於 MFT，可能導致分析時間拉長至數分鐘");
      tfm.init();
    }
  });

  $scope.fmodeObj = tfm.fmodeObj;

  $scope.ok = function() {
    var params = tso.params; // 搜尋面板條件

    // 設定時間
    _.assign(params, _.cloneDeep(_.pick($scope.selected, ['from', 'to'])));  // [NOTE] cloneDeep 是因為 _.pick 回傳的是淺層複製
    if (params.to !== null){
      params.to.setDate(params.to.getDate() + 1);  // 讓結束天 = 另一天的起頭  
    }

    // 設定其他參數
    params.match = tso.getMatch($scope.fields);
    params.fmode = $scope.selected.fmode;
    params.excludedStatus = _.map($scope.selected.excludedStatus, 'val');

    // 新增 1st Tab
    $scope.tabs.addTab(_.cloneDeep(params), {}, [], []);
  };
};

angular
  .module('mean.tehr')
  .controller('TehrController', TehrController);

TehrController.$inject = ['$scope', '$rootScope', '$timeout', '_', 'tehrSrchOpt', 'tehrQueryService', 'tehrTabService', 'tehrChartService', 'tehrFailureMode'];
