(function() {
  'use strict';

  function Tehr($stateProvider) {
    $stateProvider.state('tehr main page', {
      url: '/tehr',
      templateUrl: 'tehr/views/index.html',
      controller:TehrController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.tehr')
    .config(Tehr);

  Tehr.$inject = ['$stateProvider'];

})();
