(function(){
  'use strict';

  function tehrDataPanel($sce, $q, $location, $anchorScroll, _, tqs, tts, tcs, tso){
    return {
      restrict: 'E',
      scope: {
        tb: "="
      },
      templateUrl: 'tehr/views/tehrDataPanel.html',
      link: function(scope, element, attrs){
        var grpChart;
        var pieChart;
        var trdChart;
        var fmChart;
        var chtSubtitle = "";

        // Group 圖 Option
        var grpOpt = tcs.getSerialOption();
        grpOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.grpLoading = false
        }];

        var pieOpt = tcs.getPieOption();
        pieOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.pieLoading = false
        }];

        // Trend 圖 Option
        var trdOpt = tcs.getSerialOption();
        trdOpt.titles[0].text = "HoldRate 推移分析";
        trdOpt.titles.push({"text": "", "size": 15, "bold": false});
        _.chain(trdOpt.graphs).find({"title": "異常單數"})
          .assign({
            "fillColors": "#00CC66",
            "lineColor": "#006600",
            "color": "#006600",
          }).value();
        _.chain(trdOpt.graphs).find({"title": "Hold Rate"})
          .assign({
            "fillColors": "#993399",
            "lineColor": "#FF3399",
            "color": "#993399",
          }).value();
        trdOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.trdLoading = false
        }];

        scope.fields = _.cloneDeep(scope.tb.fields);  // 網頁的查詢欄位
        scope.fields.push({'label': "Failure Mode", 'name': "HOLDNAME"});  // 增加 Failure Mode groupby 切換

        // 僅有第一層 有 Overall group by 選項
        if (_.isEmpty(scope.tb.subMatch)){
          scope.fields = [{'label': "Overall", 'name': "OVERALL"}].concat(scope.fields);  
        }
        
        scope.groupBy = _.head(scope.fields); // 預設值
        scope.grpLoading = true;  // group loading 畫面
        scope.pieLoading = true;  // pie loading 畫面
        scope.fmLoading = true;  // failure mode loading 畫面
        scope.trdLoading = true;  // trend loading 畫面
        scope.showTrend = false;  // 預設不顯示 trend chart
        scope.holdRowNum = null;  // Hold 筆數
        scope.prdRowNum = null;  // 生產 筆數
        
        scope.fmodes = [];  // Failure Mode 清單
        var holdData = scope.tb.holdData;  // 異常單 raw data (參考 QDN Report)
        var productData = scope.tb.productData;  // 生產批數 raw data (參考 Test Yield Report)
        var groupData = [];  // 異常 + 生產 Group by 後的 data
        var firstTime = true;  // 按下查詢後，首次產生頁面

        // Group 圖操作提示
        scope.grpTips =$sce.trustAsHtml(`
          <div class="form-group">
            <p><b>[提示]</b> 對Bar或方塊</p>
            <ol>
              <li><b>Single Click</b>: 產生推移圖</li>
              <li><b>Double Click</b>: Drill down 層級</li>
            </ol>
            <p><b>[NOTE]</b> 若切換左側推移圖單位/範圍，需重新點選 Bar</p>
          </div>
        `);

        // 推移圖單位
        scope.periods = [
          {label: "日", name: "DAY"},
          {label: "週", name: "WEEK"},
          {label: "月", name: "MONTH"},
        ];
        scope.period = scope.periods[0];

        // 推移圖範圍
        scope.pRanges = _.map(_.reverse(_.range(1, 13)), r => new Object({idx: r}));
        scope.pRange = scope.pRanges[0];

        function drillDown(event){
          if (_.keys(scope.tb.subMatch).length == tso.getSize() - 1) {
            alert("已 Drill down 至最後一層");
          } else {
            if (scope.groupBy.name == "OVERALL") {
              scope.$apply(() => scope.groupBy = _.find(scope.fields, {name: 'CUSTOMERCHISNAME'}));
            } else {
              // 根據點選的 bar 更新 sub query
              var subQ = _.cloneDeep(scope.tb.subMatch);
              var cond = {[scope.groupBy.name]: event.item.category};
              _.assign(subQ, cond);
              tts.addTab(scope.tb.mainCondition, subQ, _.filter(holdData, cond), _.filter(productData, cond));  // 新增下一個 tab
            }
          }
        }

        function genTrendChart(event){
          // 捲動頁面
          $location.hash('trd-chart');
          $anchorScroll();

          scope.showTrend = true;
          scope.trdLoading = true;
          var qry = _.cloneDeep(scope.tb.mainCondition);
          _.assign(qry.match, scope.tb.subMatch);

          if (scope.groupBy.name != "OVERALL")
            qry.match[scope.groupBy.name] = event.item.category;  // 設定所選的 group
          
          qry.groupBy = scope.period.name;  // 選擇 日/週/月
          qry.pRange = scope.pRange.idx;  // 選擇範圍

          tqs.query(qry, 'gettrend', function(data){
            // 時間 label (除 年 以外皆顯示日)
            var format = 'YYYY/MM' + (qry.groupBy != "MONTH" ? ('/DD' + (qry.groupBy == "WEEK" ? ' ([w]W)' : '')) : '');
            _.forEach(data, d => d.GRP = moment(d.GRP).format(format));

            // 副標題
            var subtitle = (scope.groupBy.label == "Overall" ? "" : (scope.groupBy.label + " " + event.item.category)) + " " + qry.pRange + " " + qry.groupBy;
            if (qry.groupBy == "WEEK") {
              subtitle += "(時間標記為週一日期)";
            }

            if (!trdChart) {
              trdChart = AmCharts.makeChart('tehr-trdcht-' + scope.$id, trdOpt);
            }
            trdChart.titles[1].text = subtitle;
            trdChart.titles[2].text = chtSubtitle;
            trdChart.dataProvider = data;
            trdChart.validateData();
            trdChart.validateNow();
          });
        }

        scope.$watch("groupBy", function(nv, ov){
          // 生成 Query
          var qry = scope.tb.mainCondition;
          _.assign(qry.match, scope.tb.subMatch);
          qry.groupBy = nv.name;

          // 產生圖的條件副標
          var tle = _.cloneDeep(qry.match);  // 搜尋條件
          tle.fmode = qry.fmode;  // 加入 fmode
          var from = _.cloneDeep(qry.from);
          var to = _.cloneDeep(qry.to);
          to.setDate(to.getDate() - 1);  // 顯示的結束日期減一，因為在查詢時增加了一天作為終止條件
          var dateStr = from.toLocaleDateString() + " ~ " + to.toLocaleDateString();
          chtSubtitle = dateStr + "  " + tso.genTabTitle(tle);

          // 取得異常單 Raw Data
          var hd = $q.defer();
          if (_.isEmpty(holdData)){
            scope.holdGridOpt.api.showLoadingOverlay();
            tqs.query(qry, 'getholddata', data => {
              holdData = data;
              hd.resolve();
            });  
          } else {
            hd.resolve();
          }

          // 取得生產單 Raw Data
          var pd = $q.defer();
          if (_.isEmpty(productData)){
            scope.testYieldGridOpt.api.showLoadingOverlay();
            tqs.query(qry, 'getproductdata', data => {
              productData = data;
              pd.resolve();
            });
          } else {
            pd.resolve();
          }

          // 異常/生產單 皆取得後，開始計算圖表
          $q.all([hd.promise, pd.promise]).then(rslt => {
            if (_.isEmpty(holdData) || _.isEmpty(productData)){
              alert("尚無資料 無法計算, hold:" + holdData.length + "筆, 生產:" + productData.length + "筆")
            } else {
              if (firstTime){
                // 設定 異常單 table
                if (_.isEmpty(holdData)){
                  scope.holdGridOpt.api.showNoRowsOverlay(); 
                } else {
                  scope.holdRowNum = holdData.length;
                  scope.holdGridOpt.api.setRowData(holdData);
                }

                // 設定 生產數 table (Test Yield Report)
                if (_.isEmpty(productData)){
                  scope.testYieldGridOpt.api.showNoRowsOverlay(); 
                } else {
                  scope.prdRowNum = productData.length;
                  scope.testYieldGridOpt.api.setRowData(productData);
                }

                // [Summary] 
                scope.hold = holdData.length;  // Total hold lot
                scope.total = productData.length;  // Total product lot 次數
                scope.holdRate = _.round(scope.hold*100/scope.total, 2);  // Total Hold Rate
                scope.fmodes = _.uniq(_.map(holdData, "HOLDNAME"));
                firstTime = false;
              }

              var gb = scope.groupBy.name;

              // 計算 Hold Rate By Group
              groupData = _(holdData).groupBy(gb).map((rd, g) => {
                //  [特別注意] 若 groupBy 為 HoldName 則生產數為條件下所有生產單數
                var hold = rd.length;
                var prod = (gb == "HOLDNAME" || gb == "OVERALL") ? scope.total : _.get(_.countBy(productData, {[gb]: g}), 'true', 0);
                var bar = {
                  'GRP': gb == "OVERALL" ? gb : g,
                  'HOLDNUM': hold,
                  'PRODUCTNUM': prod,
                  'TEHR': prod == 0 ? 0 : _.round((hold / prod) * 100, 2),
                  'LABEL': 0
                };
                _.forEach(_.groupBy(rd, "HOLDNAME"), (dd, cond)=> bar[cond] = dd.length);
                return bar;
              }).value();
              groupData = sort(groupData, 'HOLDNUM');

              // [Group 圖]
              if (!grpChart) {
                grpOpt.titles[1].text = chtSubtitle;
                grpChart = AmCharts.makeChart('tehr-grpcht-' + scope.$id, grpOpt);
                tcs.setClickHandlerToChart(grpChart, genTrendChart, drillDown);
              }
              grpChart.dataProvider = groupData;              
              grpChart.validateData(); // 重畫 data
              grpChart.validateNow();  // 觸發 listener

              // [Pie 圖]
              if (!pieChart) {
                pieOpt.titles[1].text = chtSubtitle;
                pieChart = AmCharts.makeChart('tehr-piecht-' + scope.$id, pieOpt);
              }
              pieChart.dataProvider = groupData;
              pieChart.validateData(); // 重畫 data
              pieChart.validateNow();  // 觸發 listener

              // 產生 Failure Mode Chart
              if (fmChart) fmChart.clear();  // 重畫 failure mode，因為要觸發 AmCharts 的 addinitHandler，使 group 內的bar可 sorting
              var fmOpt = tcs.genClusterOption(scope.fmodes, "name");
              fmOpt.titles[1].text = chtSubtitle;
              fmOpt.listeners = [{
                "event": "rendered", 
                "method": (e) => scope.fmLoading = false
              }];
              fmOpt.dataProvider = groupData;
              fmChart = AmCharts.makeChart('tehr-fmcht-' + scope.$id, fmOpt);
            }
          });
        });

        var sort = (data, sortBy) => _.reverse(_.sortBy(data, sortBy));

        // Group 圖 sort 切換
        scope.$watch('sortBy', (nv, ov) => {
          if (nv){
            grpChart.dataProvider = sort(groupData, nv);
            grpChart.validateData(); // 重畫 data
            grpChart.validateNow();  // 觸發 listener
          }
        }); 
      },
      controller: ['$scope', function($scope){
        $scope.holdGridOpt = {
          columnDefs: [
            {headerName: "Hold Doc#", field: "ERFNO", width: 100},
            {headerName: "Cust.", field: "CUSTOMERCHISNAME", width: 100},
            {headerName: "Device", field: "DEVICENO", width: 100},
            {headerName: "Lot No", field: "CUSTOMERLOTNO", width: 100},
            {headerName: "ProductFamily", field: "FAMILY", width: 100},
            {headerName: "內批", field: "LOTNO", width: 120},
            {headerName: "Hold Time", field: "CREATEDATE", width: 180},
            {headerName: "站別", field: "OPNO", width: 100},
            {headerName: "異常項目", field: "HOLDNAME", width: 200},
            {headerName: "異常說明", field: "HOLDDESCRIPTION", width: 220},
            {headerName: "異常機台", field: "EQUIPMENTNO", width: 200},
            {headerName: "Package Type", field: "PACKAGETYPE", width: 100},
            {headerName: "處置方式", field: "HOLDDISPOSITION", width: 100},
            {headerName: "通知客戶處置時間", field: "NOTECUSTHANLDEDATE", width: 180},
            {headerName: "通知客戶解除時間", field: "NOTECUSTRELEASEDATE", width: 180},
            {headerName: "QTY", field: "DIEQTY", width: 100},
            {headerName: "status", field: "STATUS", width: 100},
            {headerName: "處置別", field: "REWORKTYPE", width: 100},
            {headerName: "收費別", field: "CHARGETYPE", width: 100},
            {headerName: "處置類別", field: "HOLDDISPTYPE", width: 100},
            {headerName: "PROGRAMNAME", field: "PROGRAMNAME", width: 220},
            {headerName: "京型", field: "PRODUCTNO", width: 250},
            {headerName: "Release Time", field: "REVISEDATE", width: 180},  // status 要為 11 才是 release!!!!
            {headerName: "貨批流程", field: "LOTFLOW", width: 750}
          ],
          rowData:[],
          groupUseEntireRow: false,
          enableSorting: true,
          enableColResize: true,
          excelStyles: [{ id: "textFormat", dataType: "string"}]
        };

        $scope.testYieldGridOpt = {
          columnDefs: [
            {headerName: "PURPOSE", field: "PURPOSE", cellClass: "textFormat", width: 100},
            {headerName: "京型", field: "PRODUCTNO", cellClass: "textFormat", width: 250},
            {headerName: "CUSTOMERNO", field: "CUSTOMERNO", cellClass: "textFormat", width: 120},
            {headerName: "客戶", field: "CUSTOMERCHISNAME", cellClass: "textFormat", width: 100},
            {headerName: "CustomerLotNo", field: "CUSTOMERLOTNO", cellClass: "textFormat", width: 100},
            {headerName: "OpNo", field: "OPNO", cellClass: "textFormat", width: 100},
            {headerName: "LotNo", field: "LOTNO", cellClass: "textFormat", width: 100},
            {headerName: "IncomeCustomerLotNo", field: "INCOMECUSTOMERLOTNO", width: 100},
            {headerName: "DateCode", field: "DATECODE", width: 100},
            {headerName: "DeviceNo", field: "DEVICENO", cellClass: "textFormat", width: 200},
            {headerName: "PackageType", field: "PACKAGETYPE", cellClass: "textFormat", width: 100},
            {headerName: "LotType", field: "LOTTYPE", cellClass: "textFormat", width: 80},
            {headerName: "ProgramName", field: "PROGRAMNAME", cellClass: "textFormat", width: 120},
            {headerName: "ProgramVer", field: "PROGRAMVERSION", width: 90},
            {headerName: "INVInDate", field: "INVINDATE", width: 100},
            {headerName: "CheckInTime", field: "CHECKINTIME", width: 160},
            {headerName: "CheckOutTime", field: "CHECKOUTTIME", width: 160},
            {headerName: "InputQty", field: "INPUTQTY", width: 80},
            {headerName: "GoodQty", field: "GOODQTY", width: 80},
            {headerName: "FailQty", field: "FAILQTY", width: 80},
            {headerName: "ScrapQty", field: "SCRAPQTY", width: 80},
            {headerName: "LossQty", field: "LOSSQTY", width: 80},
            {headerName: "OtherQty", field: "OTHERQTY", width: 80},
            {headerName: "BinNo", field: "BINNO", width: 80},
            {headerName: "BinQty", field: "BINQTY", width: 80},
            {headerName: "LotSerial", field: "LOTSERIAL", width: 100},
            {headerName: "BinValue", field: "BINVALUE", width: 80},
            {headerName: "EquipmentNo", field: "EQUIPMENTNO", cellClass: "textFormat", cellClass: "textFormat", width: 100},
            {headerName: "IncomeCustomer", field: "INCOMECUSTOMER", cellClass: "textFormat", width: 100},
            {headerName: "IncomingNo", field: "INCOMINGNO", cellClass: "textFormat", width: 100},
            {headerName: "IncomingItem", field: "INCOMINGITEM", width: 100},
            {headerName: "BIHour", field: "BIHOUR", width: 100},
            {headerName: "CarrierType", field: "CARRIERTYPE", cellClass: "textFormat", width: 100},
            {headerName: "TRPurpose", field: "TRPURPOSE", cellClass: "textFormat", width: 100},
            {headerName: "RuncardNo", field: "RUNCARDNO", cellClass: "textFormat", width: 100},
            {headerName: "IsNewProduct", field: "ISNEWPRODUCT", cellClass: "textFormat", width: 100},
            {headerName: "Family", field: "FAMILY", cellClass: "textFormat", width: 150},
          ],
          rowData:[],
          groupUseEntireRow: false,
          enableSorting: true,
          enableColResize: true,
          excelStyles: [
            { id: "textFormat", dataType: "string"}
          ]
        };
      }]
    };
  };
  
  angular.module('mean.tehr').directive('tehrDataPanel', tehrDataPanel);
  tehrDataPanel.$inject = ['$sce', '$q', '$location', '$anchorScroll', '_', 'tehrQueryService', 'tehrTabService', 'tehrChartService', 'tehrSrchOpt'];
})();