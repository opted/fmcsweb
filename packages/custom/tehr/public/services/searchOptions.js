(function(){
  'use strict';
  function srchOpt(_, utilsSrchOpt){
    var uOpt = utilsSrchOpt;
    var params = {};
    var fields = [
      uOpt.genField("客戶", "CUSTOMERCHISNAME"),
      uOpt.genField("Family", "FAMILY"),
      uOpt.genField("京型", "PRODUCTNO"),
      uOpt.genField("客型", "DEVICENO"),
      uOpt.genField("站別", "OPNO")
    ];
    return {
      params: params,
      fields: fields,
      getSize: () => fields.length,
      getMatch: uOpt.getMatch,
      genTabTitle: (subMatch) => {
        subMatch = _.omitBy(subMatch, _.isNil);
        if (_.isEmpty(subMatch)){
          return "Overall";
        } else {
          // 特別給予 default Failure Mode，因為該篩選欄位非直接從 SearchOption 產生
          return _.join(_.map(subMatch, (v, k) => {
            var tag = "[" + _.defaultTo(_.find(fields, {'name': k}), {'label': "Failure Mode"}).label + "] ";
            return tag + v ;
          }), " ");
        }
      },
      getRemainField: (subMatch) => _.reject(fields, f => _.includes(_.keys(subMatch), f.name))
    };
  };

  angular.module('mean.tehr').factory('tehrSrchOpt', srchOpt);
  srchOpt.$inject = ['_', 'utilsSrchOpt'];
})();