(function(){
  'use strict';
  function tehrQueryService(_, $http, $window){
    var isSearching = false;
    var schema = "HISMFT";
    var status = {
      str: "",
      err: ""
    };

    var config = {
      eventHandlers: {
      progress: function(event){
        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
        status.str = "正在下載資料...";
      }}
    };
    return {
      setSchema: (s) => schema = s,
      isSearching: () => isSearching,
      query: function(queryParam, dataType, callback){
        isSearching = true;
        queryParam.schema = schema;
        status.str = "等待 Server 回應...";
        return $http.post('/api/tehr/' + _.toLower(dataType), queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          callback(result.data);
          status.str = "Done";
        }, function(err){
          console.error(err);
          $window.alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      }
    };
  };

  angular.module('mean.tehr').factory('tehrQueryService', tehrQueryService);
  tehrQueryService.$inject = ["_", "$http", "$window"];
})();