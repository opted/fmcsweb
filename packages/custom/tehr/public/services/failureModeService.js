(function(){
  'use strict';
  function FailureMode(_, tqs){
    var obj = {
      list: null
    };
    return {
      fmodeObj: obj,
      init: () => tqs.query({}, 'getfailuremodelist', (data) => obj.list = data)
    };
  };

  angular.module('mean.tehr').factory('tehrFailureMode', FailureMode);
  FailureMode.$inject = ['_', 'tehrQueryService'];
})();