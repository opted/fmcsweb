(function(){
  'use strict';
  function tehrTabService(_, $timeout, tso) {
    var tabs = [];

    return {
      list: tabs,
      getSize: () => tabs.length,
      getLastTab: () => _.get(_.last(tabs), 'id'),
      removeTab: (i) => tabs.splice(i, 1),
      addTab: function(mainCondition, subMatch, holdData, productData) {
        var newTab = {
          title: tso.genTabTitle(subMatch),
          mainCondition: mainCondition,
          subMatch: subMatch,
          fields: tso.getRemainField(subMatch),
          id: new Date().getTime(),
          holdData: holdData,
          productData: productData
        };
        $timeout(tabs.push(newTab));
      }
    };
  };

  angular.module('mean.tehr').factory('tehrTabService', tehrTabService);
  tehrTabService.$inject = ['_', '$timeout', 'tehrSrchOpt'];
})();