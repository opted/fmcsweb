(function(){
  'use strict';

  function tehrChartService(_) {
    var serialOption = {
      "type": "serial",
      "theme": "none",
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "Hold Rate 分析", "size": 30},
        {"text": "", 'size': 15}
      ],
      "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 120
      },
      "dataProvider": [],
      "valueAxes": [{
        "id": "numOfHoldAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "minimum": 0,
        "title": "異常單數",
        "titleFontSize": 20
      },{
        "id": "tehrAxis",
        "axisAlpha": 1,
        "gridAlpha": 0,
        "position": "right",
        "maximum": 100,
        "minimum": 0,
        "title": "Hold Rate(%)",
        "titleFontSize": 20
      }],
      "graphs": [{
        "alphaField": "alpha",
        "balloonFunction": (item, graph) => {
          var data = item.dataContext;
          return `
            <b>[ ` + data.GRP + ` ]</b>
            <div class="block">
              <label>異常單: </label>
              <a>` + data.HOLDNUM + `筆</a>
            </div>
            <div class="block">
              <label>生產單: </label>
              <a>` + data.PRODUCTNUM + `筆</a>
            </div>
            <div class="block">
              <label>Hold Rate: </label>
              <a>` + data.TEHR + `%</a>
            </div>
          `;
        },
        "dashLengthField": "dashLength",
        "fillAlphas": 0.8,
        "fillColors": "#99CCFF",
        "lineColor": "#0066CC",
        "legendPeriodValueText": "total: [[value.sum]]",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "top",
        "showAllValueLabels": true,
        "color": "#3366CC",
        "fontSize": 15,
        "title": "異常單數",
        "type": "column",
        "valueField": "HOLDNUM",
        "valueAxis": "numOfHoldAxis"
      },{
        "bullet": "square",
        "bulletBorderAlpha": 1,
        "bulletBorderThickness": 1,
        "bulletColor": "#FF6600",
        "balloonFunction": (item, graph) => {
          var data = item.dataContext;
          return `
            <b>[ ` + data.GRP + ` ]</b>
            <div class="block">
              <label>異常單: </label>
              <a>` + data.HOLDNUM + `筆</a>
            </div>
            <div class="block">
              <label>生產單: </label>
              <a>` + data.PRODUCTNUM + `筆</a>
            </div>
            <div class="block">
              <label>Hold Rate: </label>
              <a>` + data.TEHR + `%</a>
            </div>
          `;
        },
        "lineColor": "#FF9933",
        "dashLengthField": "dashLength",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "left",
        "color": "#FF0000",
        "fontSize": 15,
        "title": "TE Hold Rate",
        "fillAlphas": 0,
        "valueField": "TEHR",
        "valueAxis": "tehrAxis"
      }],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "GRP",
      "export": {
        "enabled": true
      },
      "categoryAxis": {
        // "autoRotateCount": 5,
        // "autoRotateAngle": -45,
        "labelRotation": -20,
        "centerRotatedLabels": true
      }
    };

    var pieOption = {
      "type": "pie",
      "theme": "light",
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "Hold 分布圖", "size": 30},
        {"text": "", 'size': 15}
      ],
      "dataProvider": [],
      "valueField": "HOLDNUM",
      "titleField": "GRP",
       "balloon":{
       "fixedPosition":true
      },
      "export": {
        "enabled": true
      }
    } 

    /*
      groupchart with sorting
      參考: https://www.amcharts.com/kbase/ordering-columns-within-category-value/
    */
    AmCharts.addInitHandler(function(chart) {
      // Check if enabled
      if (chart.sortColumns != true)
        return;

      /**
       * Iterate through data
       */
      _.forEach(chart.dataProvider, d => {
        // Collect all values for all graphs in this data point
        var values = _.map(chart.graphs, g => new Object({'value': d[g.valueField], 'graph': g}));

        // Drop undefined bar
        values = _.reject(values, v => _.isUndefined(v.value));

        // Sort by value
        values.sort((a, b) => b.value - a.value);

        // Apply `columnIndexField`
        _.forEach(values, (v, indx) => {
          v.graph.columnIndexField = v.graph.valueField + "_index";
          d[v.graph.columnIndexField] = indx;
        });
      });
    }, [ "serial" ] );

    var clusterOption = {
      "type": "serial",
      "theme": "light",
      "sortColumns": true,
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "Failure Mode 分析", "size": 30},
        {"text": "", 'size': 15}
      ],
      "legend": {
        "horizontalGap": 5,
        "maxColumns": 8,
        "position": "bottom",
        "useGraphSettings": true,
        "markerSize": 10
      },
      "valueAxes": [{
        "id": "numOfHoldAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "title": "異常單數",
        "titleFontSize": 20,
        "minimum": 0
      }],
      "dataProvider": [],
      "graphs": [],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "GRP",
      "categoryAxis": {
        // "autoRotateCount": 6,
        // "autoRotateAngle": -10,
        "labelRotation": -20,
        // "gridPosition": "start",
        "gridAlpha": 0,
        // "position": "left"
      },
      "export": {
        "enabled": true
      },
    };

    function hashCode(str) {
      var hash = 0;
      _.forEach(_.range(str.length), i => hash = str.charCodeAt(i) + ((hash << 5) - hash))
      return hash;
    }

    function intToRGB(i){
      var c = (i & 0x00FFFFFF).toString(16).toUpperCase();
      return "#" + ("00000".substring(0, 6 - c.length) + c);
    }

    return {
      getSerialOption: () => _.cloneDeep(serialOption),
      getPieOption: () => _.cloneDeep(pieOption),
      genClusterOption: (fields) => {
        var opt = _.cloneDeep(clusterOption);
        _.forEach(fields, (f, i) => {
           // 採用 hash 讓每次同一名稱的 bar 顏色一致
          var color = intToRGB(hashCode(f));

          opt.graphs.push({
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "fillColors": color,
            "labelText": "[[value]]",
            "showAllValueLabels": true,
            "lineAlpha": 0.3,
            "title": f,
            "type": "column",
            "color": "#000000",
            "valueField": f
          })
        });
        return opt;
      },
      setClickHandlerToChart: (chart, singleClickFunc, doubleClickFunc) => {
        chart.clickTimeout = 0;
        chart.lastClick = 0;
        chart.doubleClickDuration = 300;
        chart.addListener("clickGraphItem", event => {
          var ts = new Date().getTime();
          if ((ts - chart.lastClick) < chart.doubleClickDuration) {  // [[ Double click ]]
            if (chart.clickTimeout) clearTimeout(chart.clickTimeout);
            chart.lastClick = 0; // reset last click
            doubleClickFunc(event);
          } else {  // [[ Single click ]]
            chart.clickTimeout = setTimeout(() => singleClickFunc(event), chart.doubleClickDuration);
          }
          chart.lastClick = ts;
        });
      }
    };
  };

  angular.module('mean.tehr').factory('tehrChartService', tehrChartService);
  tehrChartService.$inject = ['_'];
})();