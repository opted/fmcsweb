(function() {
  'use strict';
  module.exports = function(Tehr, app, auth, database, utils) {
    var tehrManager = require('../controllers/tehrQuery');
    var _ = require('lodash');
    var tehrMgr = new tehrManager(utils);
    var timeout = 30*60*1000; // 30 min
    var checkSchema = utils.ctrls.mes.checkSchema;

    app.post('/api/tehr/getsearchfield', function(req, res, next){
      req.setTimeout(timeout);
      tehrMgr.getSearchField(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    app.post('/api/tehr/gettrend', checkSchema, function(req, res, next){
      req.setTimeout(timeout);
      tehrMgr.getTrend(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    app.post('/api/tehr/getfailuremodelist', checkSchema, function(req, res, next){
      tehrMgr.getFailureModeList(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    app.post('/api/tehr/getholddata', checkSchema, function(req, res, next){
      req.setTimeout(timeout);
      tehrMgr.getHoldData(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    app.post('/api/tehr/getproductdata', checkSchema, function(req, res, next){
      req.setTimeout(timeout);
      tehrMgr.getProductData(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    function handleError(err, res) {
      console.error(err);
      res.status(500).send({'error': err});
    }
  };
})();
