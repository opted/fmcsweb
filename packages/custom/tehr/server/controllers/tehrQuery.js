'use strict'

var _ = require('lodash');
var Q = require('q');
var moment = require('moment');

function getFieldStr(field){
  switch (field){
    case 'CUSTOMERCHISNAME':
      return 'c.' + field;
      break;
    case 'FAMILY':
      return 'p.' + field;
      break;
    case 'DEVICENO':
    case 'PRODUCTNO':
      return 'l.' + field;
      break;
    case 'PHASE':
    case 'OPNO':
      return 'e.' + field;
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}

/*
* ex: ` r.ASSNO = '40010018' 
*       AND r.BOARDNO = '10204194' `
*/
function genMatch(cond, callback){
  return _(cond).pick(['CUSTOMERCHISNAME', 'FAMILY', 'DEVICENO', 'PRODUCTNO', 'OPNO', 'HOLDNAME'])
    .map((v, k) => callback(k) + ` = '` + v + `' `).value().join('\n AND ');
}

// 取得異常單數 by group
function getGroupHoldSQL(schema, match, fmode, excludedStatus, groupBy, prdFunc){
  var excludedStatusSyntax = ``;
  if (!_.isEmpty(excludedStatus) && _.isArray(excludedStatus)) {
    excludedStatusSyntax = ` AND a.PHASE NOT IN (` + excludedStatus.join(', ') + `)`
  }

  var sql = `
  SELECT
    g.PERIOD as GRP,
    g.HOLDNAME,
    COUNT(g.ERFNO) as HOLDNUM
  FROM (
    SELECT
      r.PERIOD,
      r.ERFNO,
      r.HOLDNAME
    FROM (
      SELECT
        a.LOTNO,
        a.ERFNO,
        a.OPNO,
        b.CUSTOMERNO,
        b.DEVICENO,
        b.PRODUCTNO,
        c.FAMILY,
        d.CUSTOMERCHISNAME,
        e.HOLDNO,
        f.HOLDNAME,
        ` + prdFunc('a.CREATEDATE') + ` as PERIOD
      FROM
        ` + schema + `.TBLWIPERFDATA a,
         `+ schema + `.TBLWIPLOTDATA b,
         `+ schema + `.TBLENGPRODUCTDATA c,
        Masterdm.Dim_Customer d,
         `+ schema + `.TBLWIPERFREASON e,
         `+ schema + `.TBLENGHOLDDATA f
      WHERE
        a.CREATEDATE BETWEEN :fromdate AND :todate
        ` + excludedStatusSyntax + `
        AND a.MCLASSNo = 0
        AND (a.OPNO LIKE 'BILOAD%' OR a.OPNO LIKE 'BIUNLOAD%' OR a.OPNO LIKE 'BURNIN%')
        ` + (fmode ? (`AND f.HOLDNAME = '` + fmode + `'`) : '') + `
        AND a.LOTNO = b.LOTNO
        AND b.PRODUCTNO = c.PRODUCTNO
        AND a.CUSTOMERNO = d.CUSTNO
        AND a.ERFNO = e.ERFNO
        AND e.HOLDNO = f.HOLDNO
      ) r ` + (_.isEmpty(match) ? '' : `WHERE ` + genMatch(match, (k) => 'r.' + k)) + ` 
  ) g
  GROUP BY g.PERIOD, g.HOLDNAME
  `;
  return sql;
}

// 取得生產單 "總過站次數" by group
function getGroupProductSQL(schema, match, groupBy, prdFunc){
  match = _.omit(match, groupBy);
  match = _.omit(match, "HOLDNAME");
  var mtchFld = genMatch(match, getFieldStr);
  var sql = `
  SELECT
    g.PERIOD as GRP,
    COUNT(*) as PRODUCTNUM
  FROM (
    SELECT
      e.OPNO, 
      l.DEVICENO, 
      l.PRODUCTNO, 
      p.FAMILY, 
      c.CUSTOMERCHISNAME, 
      ` + prdFunc('e.CHECKOUTTIME') + ` as PERIOD
    FROM
      `+ schema + `.TBLWIPLOTDATA l,
      `+ schema + `.TBLENGPRODUCTDATA p,
      Masterdm.Dim_Customer c,
      `+ schema + `.tblWipLotLog e
    WHERE
      ` + (_.isEmpty(mtchFld) ? '' : mtchFld + ' AND ' ) + `
      ` + prdFunc('e.CHECKOUTTIME') + ` = :grp
      AND c.CUSTNO = l.CUSTOMERNO
      AND l.LOTNO = e.LOTNO
      AND l.PRODUCTNO = p.PRODUCTNO
      AND (e.OPNO LIKE 'BILOAD%' OR e.OPNO LIKE 'BIUNLOAD%' OR e.OPNO LIKE 'BURNIN%')
      AND e.CHECKOUTTIME BETWEEN :fromdate AND :todate
  ) g
  GROUP BY g.PERIOD
  `;
  return sql;
}

// 取得 Hold Data
function getHoldDataSQL(schema, match, fmode, excludedStatus){
  var excludedStatusSyntax = ``;
  if (!_.isEmpty(excludedStatus) && _.isArray(excludedStatus)) {
    excludedStatusSyntax = ` AND a.PHASE NOT IN (` + excludedStatus.join(', ') + `)`
  }

  var sql = `
    SELECT
      *
    FROM (
      SELECT
        a.LOTNO,
        a.ERFNO,
        a.OPNO,
        a.CREATEDATE,
        a.HOLDDESCRIPTION,
        (CASE a.PHASE WHEN 11 THEN a.REVISEDATE ELSE NULL END) as REVISEDATE,
        a.PHASE,
        a.REWORKCHARGE,
        a.HOLDDISPTYPE,
        a.equipmentno,
        b.CUSTOMERNO,
        b.CUSTOMERLOTNO,
        b.DEVICENO,
        b.PRODUCTNO,
        b.PACKAGETYPE,
        b.DIEQTY,
        c.FAMILY,
        d.CUSTOMERCHISNAME,
        e.HOLDNO,
        f.HOLDNAME,
        h.HOLDDISPOSITION,
        g.PROGRAMNAME
      FROM
        `+ schema + `.TBLWIPERFDATA a,
        `+ schema + `.TBLWIPLOTDATA b,
        `+ schema + `.TBLENGPRODUCTDATA c,
        Masterdm.Dim_Customer d,
        `+ schema + `.TBLWIPERFREASON e,
        `+ schema + `.TBLENGHOLDDATA f,
        (
          SELECT
            *
          FROM (
            SELECT
              x.LOTSERIAL, 
              y.PROGRAMNAME,
              ROW_NUMBER() OVER (PARTITION BY x.LOTSERIAL ORDER BY y.PROGRAMNAME) AS rn
            FROM
              `+ schema + `.TBLWIPERFDATA x
            LEFT JOIN
              `+ schema + `.TBLWIPBINCONTENT y
              ON y.LOTSERIAL = x.LOTSERIAL
            WHERE
              (x.OPNO LIKE 'BILOAD%' OR x.OPNO LIKE 'BIUNLOAD%' OR x.OPNO LIKE 'BURNIN%')
              AND x.CREATEDATE BETWEEN :fromdate AND :todate
          )
          WHERE
            rn = 1
        ) g,
        `+ schema + `.TBLWIPERFREASONDISP h
      WHERE
        a.CREATEDATE BETWEEN :fromdate AND :todate
        ` + excludedStatusSyntax + `
        AND a.MCLASSNo = 0
        AND (a.OPNO LIKE 'BILOAD%' OR a.OPNO LIKE 'BIUNLOAD%' OR a.OPNO LIKE 'BURNIN%')
        ` + (fmode ? (`AND f.HOLDNAME = '` + fmode + `'`) : '') + `
        AND a.LOTNO = b.LOTNO
        AND b.PRODUCTNO = c.PRODUCTNO
        AND a.CUSTOMERNO = d.CUSTNO
        AND a.ERFNO = e.ERFNO
        AND e.HOLDNO = f.HOLDNO
        AND a.LOTSERIAL = g.LOTSERIAL
        AND e.ERFNO = h.ERFNO
        AND e.HOLDNO = h.HOLDNO
      ) r ` + (_.isEmpty(match) ? '' : `WHERE ` + genMatch(match, (k) => 'r.' + k)) + `
  `;
  return sql;
}

function getLotFlowSQL(schema, lot) {
  var sql = `
    SELECT 
      b.NODEID, 
      b.NODENO, 
      c.NEXTNODEID 
    FROM 
      ` + schema + `.TBLWIPLOTSTATE a,
      ` + schema + `.TBLENGPROCESSNODEMASTER b,
      ` + schema + `.TBLENGPROCESSNODERELATION c
    WHERE 
      a.LOTNO = '` + lot + `'
      AND a.PROCESSNO = b.PROCESSNO
      AND b.NODEID = c.NODEID(+)
  `;
  return sql;
}

function getCustNoteDateSQL(schema, erfno) {
  var sql = `
    SELECT
      CREATEDATE,
      NEXTPHASE,
      PHASE
    FROM
      ` + schema + `.TBLWIPERFHandle a
    WHERE
      a.ERFNO = '` + erfno + `'
  `;
  return sql;
}

// 取得 Hold Data
function getProductDataSQL(schema, match, fmode){
  match = _.omit(match, "HOLDNAME");
  var mtchFld = genMatch(match, getFieldStr);
  var sql = `
    SELECT  
      l.Purpose,
      l.ProductNo,
      l.CustomerNo,
      c.CustomerChisName,
      l.CustomerLotNo,
      e.OpNo,
      l.LotNo,
      l.INCOMECUSTOMERLOTNO,
      l.DateCode,
      l.DeviceNo,
      l.PackageType,
      l.LotType,
      e.ProgramName,
      e.ProgramVersion,
      l.INVInDate,
      e.CheckInTime,
      e.CheckOutTime,
      e.InPutQty,
      e.GoodQty,
      e.FailQty,
      e.ScrapQty,
      e.LossQty,
      e.OtherQty,
      e.LotSerial,
      e.EquipmentNo,
      l.IncomeCustomer,
      l.IncomingNo,
      l.IncomingItem,
      l.BIHour,
      l.CarrierType,
      l.TRPurpose,
      l.RuncardNo,
      l.IsNewProduct,
      p.family
    FROM   
      `+ schema + `.tblWipLotData l,
      Masterdm.Dim_Customer c,
      `+ schema + `.tblEngProductData p,
      (
        SELECT
          *
        FROM (
          SELECT
            x.*, 
            y.PROGRAMNAME,
            y.ProgramVersion,
            ROW_NUMBER() OVER (PARTITION BY x.LOTSERIAL ORDER BY y.PROGRAMNAME) AS rn
          FROM
            `+ schema + `.tblWipLotLog x
          LEFT JOIN
            `+ schema + `.TBLWIPBINCONTENT y
            ON y.LOTSERIAL = x.LOTSERIAL
          WHERE
            (x.OPNO LIKE 'BILOAD%' OR x.OPNO LIKE 'BIUNLOAD%' OR x.OPNO LIKE 'BURNIN%')
            AND x.CheckOutTime BETWEEN :fromdate AND :todate
        )
        WHERE
          rn = 1
      ) e
    WHERE
      c.CustNo = l.CustomerNo
      AND l.LotNo = e.LotNo
      AND l.productno = p.productno
      ` + (_.isEmpty(mtchFld) ? '' : (' AND ' + mtchFld)) + `
  `;
  return sql;
}

// 整理同一 Group 資料成單一 object，並以 array 存 data
function group(data, keyFunc){
  return  _.map(_.groupBy(data, r => r.GRP), (g, k) => new Object({
    'GRP': keyFunc(k), 
    'DATA': _.map(g, gg => _.omit(gg, "GRP"))
  }));
}

// 處理時間單位
function formatDateStr(d, field){
  d[field] = d[field] ? moment(d[field]).format("YYYY/MM/DD HH:mm:ss") : null;
}


class TehrManager {
  constructor(utils) {
    this.mes = utils.ctrls.mes;
    this.dt = utils.ctrls.dateTool;
  }

  getFailureModeList(query) {
    try {
      var schema = query.schema;
      var sql = `
        SELECT
          DISTINCT h.HOLDNAME
        FROM
          ` + schema + `.TBLWIPERFDATA e,
          ` + schema + `.TBLWIPERFREASON r,
          ` + schema + `.TBLENGHOLDDATA h
        WHERE
          (e.OPNO LIKE 'BILOAD%' OR e.OPNO LIKE 'BIUNLOAD%' OR e.OPNO LIKE 'BURNIN%')
          AND e.ERFNO = r.ERFNO
          AND r.HOLDNO = h.HOLDNO
          AND e.MCLASSNo = 0
      `;
      return this.mes.exec(sql)
        .then(rslt => _.map(rslt, "HOLDNAME"));
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getSearchField (query) {
    try {
      // 取得其他欄位的條件 (omit 用來排除要搜尋的欄位)
      var mtchFld = genMatch(_.omit(query.match, query.field), getFieldStr);
      var field = getFieldStr(query.field);
      var schema = query.exmatch.schema;
      var sql = `
        SELECT 
          r.` + query.field + `
        FROM (
          SELECT
            DISTINCT ` + field + `
          FROM
            ` + schema + `.TBLWIPERFDATA e,
            ` + schema + `.TBLWIPLOTDATA l,
            ` + schema + `.TBLENGPRODUCTDATA p,
            Masterdm.Dim_Customer c
          WHERE
            e.CREATEDATE BETWEEN :fromdate AND :todate
            AND e.MCLASSNo = 0
            AND (e.OPNO LIKE 'BILOAD%' OR e.OPNO LIKE 'BIUNLOAD%' OR e.OPNO LIKE 'BURNIN%')
            AND UPPER(` + field + `) LIKE :regex
            AND e.LOTNO = l.LOTNO
            AND e.CUSTOMERNO = c.CUSTNO 
            AND l.PRODUCTNO = p.PRODUCTNO
            ` + (_.isEmpty(mtchFld) ? '' : 'AND ' + mtchFld ) + `
        ) r
        WHERE
          rownum <= :rowlimit
      `;

      if (query.exmatch.from == null || query.exmatch.to == null){
        return Q.reject("Must set date time (from/to)...");
      }

      var date = {
        from: new Date(query.exmatch.from),
        to: new Date(query.exmatch.to)
      };
      date.to.setDate(date.to.getDate() + 1);  // 讓結束天 = 另一天的起頭  
      
      var vars = {
        regex: _.toUpper(query.regex) + '%',
        rowlimit: 100,
        fromdate: date.from,
        todate: date.to
      };
      return this.mes.exec(sql, vars)
        .then(rslt => _.map(rslt, query.field));
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getTrend (query) {
    try {
      var [from, to, range, prd] = this.dt.getPeriodSetting(
        query.from, query.to, query.pRange, query.groupBy, 
        date_col => `TRUNC(` + date_col + `, 'DD')`,
        date_col => `TRUNC(` + date_col + `, 'IW')`,
        date_col => `TO_CHAR(` + date_col + `, 'YYYY-MM')`
        )

      // 取得 [異常單數量]
      var sql = getGroupHoldSQL(query.schema, query.match, query.fmode, query.excludedStatus, query.groupBy, prd);
      var vars = {'fromdate': from, 'todate': to};
      var mes = this.mes;
      return mes.exec(sql, vars)
      .then(function(rslt){
        try {
          rslt = group(rslt, k => query.groupBy == "MONTH" ? k : new Date(k));

          // 對每個 Group 取得各自的 [生產單過站次數]
          var qs = _.map(rslt, r => {
            var pdsql = getGroupProductSQL(query.schema, query.match, query.groupBy, prd);
            var pdvars = _.cloneDeep(vars);
            pdvars.grp = query.groupBy == "MONTH" ? r.GRP : new Date(r.GRP);
            return mes.exec(pdsql, pdvars)
            .then(rr => _.assign(r, {"PRODUCTNUM": _.get(_.head(rr), "PRODUCTNUM")}));
          });
          return Q.all(qs)
          .then(rslt => {
            // 計算 TEHR (Hold Rate)
            _.forEach(rslt, (r, i) => {
              var holds = _.sumBy(r.DATA, d => d.HOLDNUM);
              rslt[i].HOLDNUM = holds;
              rslt[i].TEHR = _.round((holds / r.PRODUCTNUM)*100, 2);
            });

            // 補齊空月/週
            _.forEach(range, r => {
              if (!_.some(rslt, {"GRP": r})){
                rslt.push({'GRP': r, 'TEHR': null, '': 0});
              }
            });

            rslt.sort((a, b) => new Date(a.GRP) - new Date(b.GRP));  // Sort by date
            return Q.resolve(rslt);  
          });
        } catch (err) {
          return Q.reject(err.stack);
        }
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getStatus(n){
    var status = {
      '0': '第一處理單位',
      '1': '相關處理單位',
      '2': '客戶處理',
      '4': '異常處置或異常解除',
      '5': '執行結果',
      '6': '執行確認',
      '10': '異常單作廢',
      '11': '異常單已結單'
    }
    return _.get(status, String(n), '');
  }

  getReworkType(n){
    if (n === 0){
      return "不重工";
    } else if ( n <= 6 && n >= 1){
      return "重工";
    } else {
      return "";
    }
  }

  getChargeType(n){
    var typ = {
      '0': '不收費',
      '1': '未確定',
      '2': '不收費',
      '3': '收費',
      '4': '未確定',
      '5': '收費',
      '6': '不收費'
    }
    return _.get(typ, String(n), '');
  }

  getHoldDispType(n){
    var typ = {
      '0': '續Go',
      '1': '重工',
      '2': '拆批',
      '3': '轉流程',
      '4': '回貨',
      '5': '其他'
    }
    return _.get(typ, String(n), '無');
  }

  getHoldData (query) {
    try {
      var qry = _.omit(query.match, "OVERALL");
      var sql = getHoldDataSQL(query.schema, qry, query.fmode, query.excludedStatus);
      var vars = {
        fromdate: new Date(query.from),
        todate: new Date(query.to)
      };
      
      var df = Q.defer();
      this.mes.exec(sql, vars).then(rslt => {
        try {

          // 處理 狀態 及 部分時間格式
          _.forEach(rslt, d => {
            formatDateStr(d, 'CREATEDATE');
            formatDateStr(d, 'REVISEDATE');
            d['STATUS'] = this.getStatus(d.PHASE);
            d['REWORKTYPE'] = this.getReworkType(d.REWORKCHARGE);
            d['CHARGETYPE'] = this.getChargeType(d.REWORKCHARGE);
            d['HOLDDISPTYPE'] = this.getHoldDispType(d.HOLDDISPTYPE);
          });

          // 取得 [貨批流程]
          return Q.all(_.map(rslt, r => {
            return this.mes.exec(getLotFlowSQL(query.schema, r.LOTNO), {})
            .then(nodes => {
              // 找到首個貨批點
              var firstNode = _.find(nodes, {NODEID: _.find(_.map(nodes, "NODEID"), n => !_.some(nodes, {"NEXTNODEID": n}))});

              // 遞迴串接所有貨批節點
              var flow = '';
              var findNext = (n) => {
                flow += n.NODENO;
                var nextNode = _.find(nodes, {"NODEID": n.NEXTNODEID});
                if(nextNode) {
                  flow += '->';
                  findNext(nextNode);
                }
              }
              findNext(firstNode);
              r.LOTFLOW = flow;
              return r;
            });
          }))
          .then(rr => {
            // 取得  [通知客戶處置時間] 及 [通知客戶解除時間]
            Q.all(_.map(rr, r => {
              return this.mes.exec(getCustNoteDateSQL(query.schema, r.ERFNO), {})
              .then(custRslt => {
                var nextPhase2 = _.find(custRslt, {NEXTPHASE: 2});
                var phase2 = _.find(custRslt, {PHASE: 2});

                r.NOTECUSTHANLDEDATE = nextPhase2 ? nextPhase2.CREATEDATE : null;
                r.NOTECUSTRELEASEDATE = phase2 ? phase2.CREATEDATE : null;
                formatDateStr(r, 'NOTECUSTHANLDEDATE');
                formatDateStr(r, 'NOTECUSTRELEASEDATE');
              });

            }))
            .then(finalRslt => df.resolve(_.flatten(rr)))
            .catch(err => df.reject(err));
          })
          .catch(err => df.reject(err));
        } catch (e) {
          df.reject(e);
        }
      }).catch(err => df.reject(err));

      return df.promise;
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getProductData (query) {
    try {
      var qry = _.omit(query.match, "OVERALL");
      var sql = getProductDataSQL(query.schema, qry);
      var vars = {
        fromdate: new Date(query.from),
        todate: new Date(query.to)
      };
      return this.mes.exec(sql, vars).then(rslt => {
        _.forEach(rslt, d => {
          formatDateStr(d, 'CHECKINTIME');
          formatDateStr(d, 'CHECKOUTTIME');
        });
        return rslt;
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  }
}

module.exports = TehrManager;