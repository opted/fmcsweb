'use strict';

var Module = require('meanio').Module;
var Tehr = new Module('tehr');

Tehr.register(function(app, auth, database, utils) {
  Tehr.routes(app, auth, database, utils);
  Tehr.aggregateAsset('css', 'tehr.css');
  Tehr.angularDependencies(['agGrid']);
  return Tehr;
});
