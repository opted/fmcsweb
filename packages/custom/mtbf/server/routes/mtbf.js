(function() {
    'use strict';
    // The Package is past automatically as first parameter
    module.exports = function(Mtbf, app, auth, database, utils) {
        var MtbfManager = require('../controllers/mtbfQuery');
        var mtbfMgr = new MtbfManager(utils);
        var checkSchema = utils.ctrls.mes.checkSchema;
        var timeout = 30*60*1000; // 30 min

        app.post('/api/mtbf/getsearchfield', function(req, res, next){ // search field too often, do not save requestlog
            mtbfMgr.getSearchField(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/mtbf/getgroup', checkSchema, function(req, res, next){
            mtbfMgr.getGroup(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/mtbf/gettrend', checkSchema, function(req, res, next){
            mtbfMgr.getTrend(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/mtbf/getbibinfo', checkSchema, function(req, res, next){
            mtbfMgr.getBoardInfo(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/mtbf/getrepairdetail', checkSchema, function(req, res, next){
            mtbfMgr.getRepair(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/mtbf/getrepairdutmap', checkSchema, function(req, res, next){
            mtbfMgr.getDutMap(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/mtbf/updateerrcodedef', function(req, res, next){
            mtbfMgr.updateERRCODEDef(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.get('/api/mtbf/geterrcodedef', function(req, res, next){
            mtbfMgr.getERRCODEDef(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });

        app.post('/api/mtbf/getfailuremode', checkSchema, function(req, res, next){
            req.setTimeout(timeout);
            mtbfMgr.getFailureMode(req.body)
            .then(data => res.json(data))
            .catch(err => handleError(err, res));
        });
    };

    function handleError(err, res) {
        console.error(err);
        res.status(500).send({'error': err});
    }
})();
