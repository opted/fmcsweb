'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var DUTLOG = mongoose.model('DUTLOG');
var MTBF_ERRCODEDEF = mongoose.model('MTBF_ERRCODEDEF');
var Q = require('q');
var moment = require('moment');

// 產生區間內的 年月字串 array ex: ["1711", "1712", "1801", "1802"]
function getThroughMonth(from, to) {
  var from = moment(from);
  var to = moment(to);
  var range = _.range(to.diff(from, 'months') + 1);
  var arr = _.map(range, d => from.add(_.min([d, 1]), 'month').format('YYMM'));
  return arr;
}

function numTo2Char(num) {
  return ("0" + num).slice(-2);  // ex: "1" => "01", "23" => "23"
}

// 計算 dut MTBF
function dutMTBF(duts, endDate){
  return _.map(duts, d => {
    d.MTBF = d.REPAIR === 1 ? moment(endDate).diff(d.MMX, 'hours', true) : d.HR_RANGE/(d.REPAIR-1);
    return d;
  });
}

// 計算 group by 的 MTBF
function groupMTBF(data, groupBy, groupKeyFunc, precision=2) {
  return _(data).groupBy(groupBy)
    .map((r, k) => new Object({
      'GRP': groupKeyFunc(r, k),
      'MTBF': _.round(_.min(_.map(r, 'MTBF')), precision), // 取該群集之 MTBF 最小值
      'REPAIR': _.sumBy(r, 'REPAIR'),
      'KEY': k,
      'BIBTYPE': _.get(_.head(r), 'BIBTYPE')
    })).value();
}

function getFieldStr(field, isCondition){
  switch (field){
    case 'CUSTOMERCHISNAME':
      return 'c.' + field;
      break;
    case 'BOARDNO':
    case 'ASSNO':
    case 'ASSSERIALNO':
      return 'b.' + field;
      break;
    case 'BIBTYPE':
      return isCondition ? "SUBSTR(b.ASSSERIALNO, 2, 3)" : "SUBSTR(b.ASSSERIALNO, 2, 3) as BIBTYPE";
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}

function getRepairSQL(schema, match, isRaw, prd){
  var sql = `
    SELECT
      r.*
    FROM (
      SELECT
        d.*,
        b.BOARDNO,
        b.ASSSERIALNO,
        SUBSTR(b.ASSSERIALNO, 2, 3) as BIBTYPE,
        b.ASSX*d.ASSY - d.ASSX as DUT,
        c.CUSTOMERCHISNAME
      FROM
        (
          SELECT
            ASSNO,
            ASSX,
            ASSY,
            ` + (isRaw ? `
              REPAIRNO,
              CREATEDATE
              ` : `
              COUNT(*) as REPAIR,
              MAX(CREATEDATE) as MMX,
              MIN(CREATEDATE) as MMI,
              24 * (MAX(CREATEDATE) - MIN(CREATEDATE)) as HR_RANGE
              ` + (prd ? (`, ` + prd + ` as PERIOD `) : ``)) + `
          FROM 
            ` + schema + `.TBLWIPASSREPAIRSOCKETDETAIL
          WHERE
            CREATEDATE BETWEEN :fromdate AND :todate
          ` + (isRaw ? `` : `
          GROUP BY
            ASSNO, ASSX, ASSY ` + (prd ? (`,` + prd) : `` ))+  `
        ) d
      LEFT JOIN
        ` + schema + `.TBLENGASSDATA b ON b.ASSNO = d.ASSNO
        LEFT JOIN
          Masterdm.Dim_Customer c ON c.CUSTOMERNAME = b.CUSTOMERNAME
      ) r
  `;

  if (!_.isEmpty(match)){
    sql += `WHERE ` + genMatch(match, (k) => 'r.' + k);
  }
  return sql;
}

function getBibInLotSQL(yearMonth){
  return `
    SELECT
      Zone_ID,
      Slot_ID,
      lotnum,
      log_time AS date,
      flowname AS flow,
      binmap1,
      binmap2
    FROM
      MBI` + yearMonth + ` WITH(NOLOCK)
    WHERE
      board_id = @bid
      AND log_time BETWEEN @min AND @max
      AND oven_id NOT LIKE 'BTS%'
      AND flowname = 'summary'
      AND BlockName = 'auto'
  `;
}

/*
* ex: ` AND r.ASSNO = '40010018' 
*       AND r.BOARDNO = '10204194' `
*/
function genMatch(cond, callback){
  return _(cond).pick(['CUSTOMERCHISNAME', 'BOARDNO', 'ASSNO', 'ASSSERIALNO', 'BIBTYPE'])
    .map((v, k) => callback(k, true) + ` = '` + v + `' `)
    .value().join('\n AND ');
}

class MtbfManager {
  constructor(utils) {
    this.mes = utils.ctrls.mes;
    this.mbi = utils.ctrls.mbi;
    this.mongoQuery = utils.ctrls.mongoQuery;
    this.dt = utils.ctrls.dateTool;
  }

  getSearchField (query) {
    try {
      var mtchFld = genMatch(_.omit(query.match, query.field), getFieldStr);
      var schema = query.exmatch.schema;
      var sql = `
        SELECT 
          *
        FROM (
            SELECT
              DISTINCT ` + getFieldStr(query.field) + `
            FROM
              Masterdm.Dim_Customer c
            FULL JOIN 
              ` + schema + `.TBLENGASSDATA b ON c.CUSTOMERNAME = b.CUSTOMERNAME
          WHERE
            UPPER(` + getFieldStr(query.field, true) + `) LIKE :regex
            ` + (_.isEmpty(mtchFld) ? '' : 'AND ' + mtchFld ) + `
        )
        WHERE
          rownum <= :rowlimit
      `;

      var vars = {
        regex: _.toUpper(query.regex) + '%',
        rowlimit: 100
      };

      return this.mes.exec(sql, vars)
        .then(rslt => _.map(rslt, query.field));
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  // 取得 Group By 資料
  getGroup (query) {
    try {
      var sql = getRepairSQL(query.schema, query.match);
      var vars = {
        fromdate: new Date(query.from),
        todate: new Date(query.to)
      };
      return this.mes.exec(sql, vars)
      .then(function(rslt){
        try {
          // Count MTBF
          var dutRslt = dutMTBF(rslt, vars.todate);  // Dut MTBF
          rslt = groupMTBF(dutRslt, "ASSNO", (r, k) => _.head(r)[query.groupBy]);  // Borad MTBF
          var assno = rslt.length === 1 ? _.get(_.head(rslt), 'KEY') : undefined;  // 確認 是否為單一 board
          var bibtype = _.uniq(_.map(rslt, 'BIBTYPE')).length === 1 ? _.get(rslt[0], 'BIBTYPE') : undefined; // 確認是否為單一 BIB type
          rslt = groupMTBF(rslt, "GRP", (r, k) => k);  // Group MTBF

          // Filter repair times
          if (query.limit.llm || query.limit.hlm) {
            rslt = _.filter(rslt, (r) => !((query.limit.llm && r.REPAIR < query.limit.llm) || (query.limit.hlm && r.REPAIR > query.limit.hlm)));
          }

          rslt.sort((a, b) => b.REPAIR - a.REPAIR);  // Sort by repair times

          _.forEach(dutRslt, d => d.MTBF = _.round(d.MTBF, 2));
          dutRslt = _.sortBy(dutRslt, "DUT");
          return Q.resolve({
            data: rslt, 
            assno: assno, 
            bibtype: bibtype, 
            duts: assno ? dutRslt : null // dutRslt 是透過判斷是否為單板，來決定要不要附帶
          });
        } catch (err) {
          return Q.reject(err.stack);
        }
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  };

  // 取得 Trend Chart 資料
  getTrend (query) {
    try {
      var [from, to, range, prd] = this.dt.getPeriodSetting(
        query.from, query.to, query.pRange, query.groupBy, 
        null,
        `TRUNC(CREATEDATE, 'IW')`,
        `TO_CHAR(CREATEDATE, 'YYYY-MM')`
        )

      var sql = getRepairSQL(query.schema, query.match, false, prd);
      var vars = {'fromdate': from, 'todate': to};

      return this.mes.exec(sql, vars)
      .then(function(rslt){
        try {
          // Count MTBF
          rslt = dutMTBF(rslt, vars.todate);  // Dut MTBF
          rslt = groupMTBF(rslt, r => r.ASSNO + "#" + r.PERIOD, (r, k) => _.head(r).PERIOD);  // Borad MTBF
          rslt = groupMTBF(rslt, "GRP", (r, k) => _.head(r).GRP);  // Group MTBF

          // 補齊空月/週
          _.forEach(range, r => {
            if (!_.some(rslt, {"GRP": r})){
              rslt.push({'GRP': r, 'MTBF': null,'REPAIR': 0});
            }
          });

          rslt.sort((a, b) => new Date(a.GRP) - new Date(b.GRP));  // Sort by date
          return Q.resolve(rslt);
        } catch (err) {
          return Q.reject(err.stack);
        }
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  };

  getRepair (query) {
    try {
      var sql = getRepairSQL(query.schema, query.match, true);
      var vars = {
        fromdate: new Date(query.from),
        todate: new Date(query.to)
      };      
      return this.mes.exec(sql, vars)
      .then(rslt => this.fetchFirstErrCode(rslt, query.schema));
    } catch (err){
      return Q.reject(err.stack);
    }
  };

  fetchFirstErrCode (repairs, schema) {
    try {
      var qs = _(repairs).groupBy(r => [r.ASSSERIALNO, r.ASSX, r.ASSY].join('-'))
        .map((dutr, k) => {  // 每個 dut
          dutr.sort((a, b) => a.CREATEDATE - b.CREATEDATE);
          return _.map(dutr, (d, indx) => {
            // 檢查第一筆的前一個月是否有最接近的一筆維修，
            // - 有: 則以該筆的時間回查 errcode
            // - 無: 則以一個月內的時間回查 errcode
            var sql = `
              SELECT 
                *
              FROM (
                SELECT
                  * 
                FROM 
                  ` + schema + `.TBLWIPASSREPAIRSOCKETDETAIL 
                WHERE
                  ASSNO = :assno
                  AND ASSX = :x AND ASSY = :y
                  AND CREATEDATE >= :preOneMonth AND CREATEDATE < :dd
                ORDER BY CREATEDATE DESC
              ) r
              WHERE 
                rownum <= 1
            `;
            var preMth = moment(new Date(d.CREATEDATE)).subtract(1, 'months').startOf('d')._d;  // 前一個月
            return this.mes.exec(sql, {
              assno: d.ASSNO,
              x: d.ASSX,
              y: d.ASSY,
              dd: new Date(d.CREATEDATE),
              preOneMonth: preMth
            }).then(preOne => {
              /*
                - indx == 0 為第一筆，須尋找前一個月是否有時間最接近之一次維修
                  - 有: 以該維修時間為起始
                  - 無: 則以前一個月時間為起始
                - index > 0，皆以前一筆的維修時間為起始
              */
              var vars = {
                bid: d.ASSSERIALNO,
                max: d.CREATEDATE,
                min: indx == 0 ? (_.isEmpty(preOne) ? preMth : new Date(_.get(_.head(preOne), "CREATEDATE"))) : new Date(dutr[indx-1].CREATEDATE)
              };
              var mths = getThroughMonth(vars.min, vars.max);
              var sql = `
                SELECT 
                  *
                FROM ( `
                  + _.map(mths, m => getBibInLotSQL(m)).join("\n UNION ALL \n") + 
                ` ) r
                ORDER BY
                  date
              `;
              return this.mbi.exec(sql, vars).then(lots => {
                if (_.isEmpty(lots)){
                  return d;
                }
                function findErrInDutlog(mongo, i){  // 遞迴找該 dut 的 First Fail in Dutlog
                  var binfo = lots[i];
                  var pipe = [
                    {'$match': {'LOTNO': binfo.lotnum.trim()}},
                    {'$unwind': "$SLOTINFO"},
                    {'$match': {"SLOTINFO.ZONE": numTo2Char(binfo.Zone_ID), "SLOTINFO.BOARD": numTo2Char(binfo.Slot_ID)}},
                    {'$unwind': "$SLOTINFO.DUT_LIST"},
                    {'$match': {
                      "SLOTINFO.DUT_LIST.DUT": numTo2Char(d.DUT),
                      "SLOTINFO.DUT_LIST.ERRCODE": {'$nin': ["00", "FF"]},
                    }},
                    {'$limit': 1}
                  ];
                  return mongo.massiveQuery(DUTLOG, pipe)
                  .then(rslt => {
                    if (!_.isEmpty(rslt) || i == lots.length - 1){
                      var errcode = _.get(_.head(rslt), 'SLOTINFO.DUT_LIST.ERRCODE');
                      return MTBF_ERRCODEDEF.find({'RAW': errcode})
                      .then(def => {
                        var prs = _.get(_.head(def), "PRESENT");
                        return _.assign(d, {
                          'ERRCODE': prs ? prs : errcode,
                          'FLOW': binfo.flow,
                          'BINMAP': binfo.binmap1 + binfo.binmap2,
                          'LOTNO': binfo.lotnum
                        });
                      });
                    } else {
                      return findErrInDutlog(mongo, i+1);
                    }
                  }).catch(err => console.error("[Get ErrCode]" + err));
                }
                return findErrInDutlog(this.mongoQuery, 0);
              });
            });
          });
        }).value();

      return Q.all(_.flattenDeep(qs)).then(r => r);
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getDutMap (query) {
    try {
      var schema = query.schema;
      var sql = `
        SELECT
          ASSX as X,
          ASSY as Y,
          COUNT(*) as NUM
        FROM 
          ` + schema + `.TBLWIPASSREPAIRSOCKETDETAIL
        WHERE
          CREATEDATE BETWEEN :fromdate AND :todate
          AND ASSNO = :assno
        GROUP BY
          ASSNO, ASSX, ASSY
      `;

      var vars = {
        fromdate: new Date(query.from),
        todate: new Date(query.to),
        assno: query.assno
      };
      return this.mes.exec(sql, vars);
    } catch (err){
      return Q.reject(err.stack);
    }
  };

  getBoardInfo (query) {
    var sql = `SELECT * FROM ` + query.schema + `.TBLENGASSDATA WHERE ASSNO = :assno`;
    var vars = {assno: query.assno};
    return this.mes.exec(sql, vars).then(rslt => _.head(rslt));
  }

  updateERRCODEDef(data){
    _.forEach(data, d => {
      MTBF_ERRCODEDEF.findOneAndUpdate({'RAW': d.RAW}, d, {'upsert': true}).exec();
    });
    return Q.resolve("更新完成");
  }

  getERRCODEDef(){
    return MTBF_ERRCODEDEF.find({}, {_id: 0, RAW: 1, DEF: 1, PRESENT: 1}).exec();
  }

  getFailureMode(query){
    try {
      var sql = getRepairSQL(query.schema, query.match, true);
      var vars = {
        fromdate: new Date(query.from),
        todate: new Date(query.to)
      }; 
      return this.mes.exec(sql, vars)
      .then(rslt => this.fetchFirstErrCode(rslt, query.schema));
    } catch (err){
      return Q.reject(err.stack);
    }
  }
}

module.exports = MtbfManager;