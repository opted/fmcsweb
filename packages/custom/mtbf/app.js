'use strict';

var Module = require('meanio').Module;
var Mtbf = new Module('mtbf');

Mtbf.register(function(app, auth, database, utils, users) {
  Mtbf.routes(app, auth, database, utils, users);
  Mtbf.aggregateAsset('css', 'mtbf.css');
  Mtbf.angularDependencies(['agGrid', 'ui.bootstrap']);
  return Mtbf;
});
