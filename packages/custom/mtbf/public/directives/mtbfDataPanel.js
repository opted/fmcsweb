(function(){
  'use strict';

  function mtbfDataPanel($sce, $uibModal, _, mqs, mts, mcs, mso){
    return {
      restrict: 'E',
      scope: {
        tb: "="
      },
      templateUrl: 'mtbf/views/mtbfDataPanel.html',
      link: function(scope, element, attrs){
        scope.fields = scope.tb.fields;  // 網頁的查詢欄位
        scope.groupBy = _.head(scope.fields); // 預設值
        scope.grpLoading = true;  // group loading 畫面
        scope.trdLoading = true;  // trend loading 畫面
        scope.dutLoading = true;  // dut mtbf loading 畫面
        scope.fmLoading = true;  // Failure Mode loading 畫面
        scope.showTrend = false;  // 預設不顯示 trend chart
        scope.status = mqs.status;  // Query 狀態
        scope.rowNum = null;  // 維修筆數
        scope.showFailureModeBtn = false;  // 顯示 Failure Mode 按鈕 (單一 BIB type 時)
        scope.showFailureMode = false;  // 顯示 Failure Mode 分析圖 (單一 BIB type 時)
        scope.showRepairDetail = false;  // 顯示 Dutmap 及其他維修細節 (單一板時)
        scope.groupData = [];
        scope.assno = null;  // 若為單板，則紀錄該板新配件編號以利查詢維修

        scope.grpTips =$sce.trustAsHtml(`
          <div class="form-group">
            <p><b>[提示]</b> 對Bar或方塊</p>
            <ol>
              <li><b>Single Click</b>: 產生推移圖</li>
              <li><b>Double Click</b>: Drill down 層級</li>
            </ol>
            <p><b>[NOTE]</b> 若切換左側推移圖單位/範圍，需重新點選 Bar</p>
          </div>
        `);

        // 推移圖單位
        scope.periods = [
          {label: "週", name: "WEEK"},
          {label: "月", name: "MONTH"},
        ];
        scope.period = scope.periods[0];

        // 推移圖範圍
        scope.pRanges = _.map(_.reverse(_.range(1, 13)), r => new Object({idx: r}));
        scope.pRange = scope.pRanges[0];

        var getCurLevelQry = () => {
          var qry = _.cloneDeep(mso.params);  // 主查詢面板 query
          _.assign(qry.match, scope.tb.subMatch);  // 加入子查詢 query (drilldown 維度)
          return qry;
        };

        var grpChart;
        var trdChart;
        var dutChart;
        var fmChart;

        // Group Chart Option
        var grpOpt = mcs.getSerialOption();
        grpOpt.titles.push({"text": "Group By " + scope.groupBy.label, "size": 15, "bold": false});
        grpOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.grpLoading = false
        }];

        // Trend Chart Option
        var trdOpt = mcs.getSerialOption();
        trdOpt.titles[0].text = "MTBF 推移分析";
        trdOpt.titles.push({"text": "", "size": 15, "bold": false});
        _.chain(trdOpt.graphs).find({"title": "維修次數"})
          .assign({
            "fillColors": "#00CC66",
            "lineColor": "#006600",
            "color": "#006600",
          }).value();
        _.chain(trdOpt.graphs).find({"title": "MTBF"})
          .assign({
            "fillColors": "#993399",
            "lineColor": "#FF3399",
            "color": "#993399",
          }).value();
        trdOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.trdLoading = false
        }];

        // Dut Chart Option
        var dutOpt = mcs.getSerialOption();
        dutOpt.titles[0].text = "DUT MTBF 分析";
        dutOpt.categoryField = "DUT";
        dutOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.dutLoading = false
        }];

        // Failure Mode Chart Option
        var fmOpt = mcs.getFailureModeOption();
        fmOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.fmLoading = false
        }];

        function drillDown(event){
          if (_.keys(scope.tb.subMatch).length == mso.getSize() - 1) {
            alert("已 Drill down 至最後一層");
          } else {
            // 根據點選的 bar 更新 sub query
            var subQ = _.cloneDeep(scope.tb.subMatch);
            subQ[scope.groupBy.name] = event.item.category;
            mts.addTab(subQ);  // 新增下一個 tab
          }
        }

        function genTrendChart(event){
          scope.showTrend = true;
          scope.trdLoading = true;

          var selectedGrp = event.item.category;
          var qry = getCurLevelQry();
          qry.match[scope.groupBy.name] = selectedGrp;  // 設定所選群集
          _.assign(qry, {groupBy: scope.period.name, pRange: scope.pRange.idx});  // 設定週期區間/範圍

          mqs.query(qry, 'gettrend', function(data){
            // 時間軸 label (顯示 月/週)
            _.forEach(data, d => d.GRP = moment(d.GRP).format(qry.groupBy == "WEEK" ? 'YYYY/MM/DD ([w]W)' : 'YYYY/MM'));

            // 產圖
            if (!trdChart) trdChart = AmCharts.makeChart('mtbf-trdcht-' + scope.$id, trdOpt);
            trdChart.titles[1].text = [scope.groupBy.label, selectedGrp, qry.pRange, qry.groupBy].join(" ");
            trdChart.dataProvider = data;
            trdChart.validateNow();
            trdChart.validateData();
          });
        }

        // 切換 groupBy 維度
        scope.$watch("groupBy", function(nv, ov){
          if (nv != ov || _.isEmpty(scope.groupData)) {
            scope.grpLoading = true;

            var qry = getCurLevelQry();
            qry.groupBy = nv.name;  // 設定 groupby 維度

            mqs.query(qry, 'getgroup', result => {
              if (_.isEmpty(result.data)){
                alert("此條件無任何資料，請重新設定，謝謝~");
              }
              scope.groupData = result.data;
              scope.bibtype = result.bibtype;  // 設定是否為唯一 bibtype

              // 僅在此設定是否有 assno (意即結果為單板)
              // 因為若此 tab 為 單板，drilldwon/group 也一定為單板
              scope.assno = result.assno;
              scope.duts = result.duts;

            });
          }
        });

        // Group Data 變化時的處理
        scope.$watch('groupData', function(nv, ov){
          if (nv != ov) {
            if (!_.isEmpty(nv)) {
              var subtitle = "Group By " + scope.groupBy.label;
              if (!grpChart) {
                grpChart = AmCharts.makeChart('mtbf-grpcht-' + scope.$id, grpOpt);

                grpChart.clickTimeout = 0;
                grpChart.lastClick = 0;
                grpChart.doubleClickDuration = 300;

                function clickHandler(event) {
                  var ts = (new Date()).getTime();
                  if ((ts - grpChart.lastClick) < grpChart.doubleClickDuration) {  // [[ Double click ]]
                    if (grpChart.clickTimeout) {
                      clearTimeout(grpChart.clickTimeout);
                    }
                    grpChart.lastClick = 0; // reset last click
                    drillDown(event);
                  } else {  // [[ Single click ]]
                    grpChart.clickTimeout = setTimeout(() => genTrendChart(event), grpChart.doubleClickDuration);
                  }
                  grpChart.lastClick = ts;
                }

                grpChart.addListener("clickGraphItem", clickHandler);
              }
              grpChart.titles[1].text = subtitle;
              grpChart.dataProvider = nv;
              grpChart.validateNow();  // 觸發 listener
              grpChart.validateData(); // 重畫 data
            } else {
              scope.grpLoading = false;
            }
          }
        }, true);

        // 若為單板，則顯示 dut mtbf, repair table 及 dutmap
        scope.$watch('assno', function(nv, ov){
          if (nv) {
            scope.showRepairDetail = true;

            // [Dut MTBF]
            dutOpt.dataProvider = scope.duts;
            dutChart = AmCharts.makeChart('mtbf-dutcht-' + scope.$id, dutOpt);

            var qry = getCurLevelQry();
            qry.assno = nv;
            mqs.query(qry, 'getbibinfo', (bib) => {
              scope.binfo = bib;

              // [Repair 疊圖]  (觸發 mtbfDutMap Directive)
              mqs.query(qry, 'getrepairdutmap', (repairs) => scope.dutRepairs = repairs);

              // [Repair 資訊 table]
              scope.repairGrid.api.showLoadingOverlay();
              scope.binMapLoading = "(載入中...)";
              mqs.query(qry, 'getrepairdetail', (data) => {
                if (_.isEmpty(data)){
                  scope.repairGrid.api.showNoRowsOverlay();
                  scope.binMapLoading = "(MBI無法找到對應 binmap)";
                } else {
                  scope.rowNum = data.length;
                  _.forEach(data, d => d.BINMAP = _.join(_.flattenDeep(_.unzip(_.reverse(_.chunk(_.map(d.BINMAP, s=> s), scope.binfo.ASSY)))), ""));
                  scope.repairGrid.api.setRowData(data);

                  // [Fail Binmap 疊圖]  (觸發 mtbfDutMap Directive)
                  var binmaps = _.map(_.groupBy(data, d => d.LOTNO + "-" + d.CREATEDATE), (v, k) => _.get(_.head(v), "BINMAP"));
                  binmaps = _.reject(binmaps, b => b === undefined);
                  if (_.isEmpty(binmaps)){
                    scope.binMapLoading = "(MBI無法找到對應 binmap)";
                  } else {
                    scope.failBinMap = _.map(_.range(scope.binfo.ASSX * scope.binfo.ASSY), (i) => {
                      return _.sumBy(_.map(binmaps, bm => bm.charAt(i)), t => t != "0" && t != "1" ? 1 : 0);
                    });
                    scope.binMapLoading = null;
                  }
                }
              });
            });
          } else {
            scope.showRepairDetail = false;
          }
        });

        // 若為單一 BIB type，則顯示 Failure Mode 分析 按鈕
        scope.$watch('bibtype', nv => scope.showFailureModeBtn = !_.isNil(nv) && !_.isEmpty(nv));

        // Failure Mode Chart 產生
        scope.genFModeChart = function(){
          scope.showFailureModeBtn = false;
          scope.showFailureMode = true;

          var qry = getCurLevelQry();
          qry.bibtype = scope.bibtype;

          mqs.query(qry, 'getfailuremode', (fm) => {
            // 合併各 dut 的 errcode 次數
            var fm = _(fm).groupBy("ERRCODE")
            .map((fail, errcode) => new Object({'ERRCODE': errcode, 'FAIL': fail.length}))
            .sortBy('FAIL').reverse().value();
            fmOpt.dataProvider = fm;
            fmChart = AmCharts.makeChart('mtbf-fmcht-' + scope.$id, fmOpt);
            if (_.isEmpty(fm)){
              fmChart.addLabel("50%", "50%", "無法找到對應 Dutlog (可能SQLMBI中未有對應 lotnum)", "middle", 35);
            }
          });
        };

        // 彈跳視窗 => 確認是否要顯示 failure mode chart
        scope.showFMode = function() {
          scope.rowNum = _.sumBy(scope.groupData, "REPAIR");
          if (scope.rowNum > 100) {
            var modalInstance = $uibModal.open({
              template: `
                <div class="modal-header">
                  <h3 class="modal-title" id="modal-title">警告</h3>
                </div>
                <div class="modal-body" id="modal-body">
                  查詢的 dut 筆數為 {{ rownum}} ，預估計算時間 {{ rownum/50 }} 分鐘
                  (1000 筆以上，可能導致查詢時間過程太長而 timeout)
                  確定顯示?
                </div>
                <div class="modal-footer">
                  <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
                  <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
                </div>`,
              controller: function($scope, $uibModalInstance, rownum){
                $scope.rownum = rownum;
                $scope.ok = () => $uibModalInstance.close(true);
                $scope.cancel = () => $uibModalInstance.close(false);
              },
              resolve: {
                rownum: () => scope.rowNum
              }
            });
            modalInstance.result.then(k => {
              if (k) {
                scope.genFModeChart();
              }  
            });
          } else {
            scope.genFModeChart();
          }
        };

      },
      controller: ['$scope', function($scope){
        $scope.repairGrid = {
          columnDefs: [
            {headerName: "結單時間", field: "CREATEDATE", width: 180},
            {headerName: "客戶", field: "CUSTOMERCHISNAME", width: 100},
            {headerName: "BIB Type", field: "BIBTYPE", width: 80},
            {headerName: "板全號", field: "BOARDNO", width: 100},
            {headerName: "維修單號", field: "REPAIRNO", width: 100},
            {headerName: "配件編號", field: "ASSNO", width: 180},
            {headerName: "舊配件編號", field: "ASSSERIALNO", width: 90},
            {headerName: "X", field: "ASSX", width: 50},
            {headerName: "Y", field: "ASSY", width: 50},
            {headerName: "Dut", field: "DUT", width: 60},
            {headerName: "ERRCODE", field: "ERRCODE", width: 180},
            {headerName: "FLOW", field: "FLOW", width: 90},
            {headerName: "BINMAP", field: "BINMAP", width: 120},
            {headerName: "1st Failed at LOT", field: "LOTNO", width: 120},
          ],
          rowData:[],
          groupUseEntireRow: false,
          enableSorting: true,
          enableColResize: true,
          excelStyles: [{
            id: "greenBackground",
            interior: {color: "#aaffaa", pattern: 'Solid'}
          }]
        };
      }]
    };
  };
  
  angular.module('mean.mtbf').directive('mtbfDataPanel', mtbfDataPanel);
  mtbfDataPanel.$inject = ['$sce', '$uibModal', '_', 'mtbfQueryService', 'mtbfTabService', 'mtbfChartService', 'mtbfSrchOpt'];
})();