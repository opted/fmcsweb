(function(){
  'use strict';

  function upload(_, mqs){
    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'mtbf/views/upload.html',
      link: function(scope, element, attrs){
        scope.cols = [
          {field: "RAW", label:'原始資料'}, 
          {field: "DEF", label:'意義'},
          {field: "PRESENT", label:'呈現'}
        ];
        scope.showCur = true;
        scope.newDefs = [];

        // 顯示上傳之設定
        scope.$watch('sheetData', function(nv, ov){
          if (!_.isEmpty(_.get(nv, 'data')) && nv !== ov){
            if (!_.isEqual(_.map(scope.sheetData.columnDefs, "field").sort(), _.map(scope.cols, 'label').sort())){
              alert("格式錯誤，請依照[原始資料], [意義], [呈現] 三個欄位產生檔案...");
              scope.sheetData = {};
              scope.status = "";
              return;
            }

            scope.newDefs = _.map(scope.sheetData.data, row => {
              return Object({
                "RAW": row['原始資料'],
                "DEF": row['意義'],
                "PRESENT": row['呈現']
              });
            });
            scope.newDefs = _.filter(scope.newDefs, v => _.head(_.values(v)) != undefined);  // 清除空 row
            scope.status = " (尚未更新)";
          }
        }, true);

        // 更新設定
        scope.update = function(){
          mqs.update(scope.newDefs, 'updateerrcodedef', function(result){
            scope.status = " (更新完成)";
          })
        };

        // 切換 [現況]/[上傳更新] 功能
        scope.curStyle = {border: "4px solid #0099cc"};
        scope.switch = function(isCur){
          scope.showCur = isCur;
          scope.curStyle = isCur ? {border: "4px solid #0099cc"} : {};
          scope.updateStyle = isCur ? {} : {border: "4px solid #0099cc"};
        };

        // 刷新頁面，顯示 DB 最新更新
        scope.refresh = function() {
          mqs.get('geterrcodedef', rslt => {
            scope.curDefs = _.map(rslt, row => {
              return Object({
                "RAW": row.RAW,
                "DEF": row.DEF,
                "PRESENT": row.PRESENT
              });
            });
          });
        };

        scope.refresh();  // 預設顯示目前 DB 的紀錄
      }
    };
  }

  angular.module('mean.mtbf').directive('errcodeUpload', upload);
  upload.$inject = ['_', 'mtbfQueryService'];
})();