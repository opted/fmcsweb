(function(){
  'use strict';

  function mtbfDutMap(_){
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        binfo: '=',  // 該板資訊，至少要有該板長寬, ex: {ASSX: 10, ASSY: 20, ...}
        duts: '=',  // 各 dut 座標/數值 
        useDutNum: '='  // 是否採用 DutNum
      },
      /*
        [NOTE] useDutNum:
          - false: 僅需有值的 dut (這邊 NUM 為值)，會自動補上空缺部分
                   ex: [{X: 2, Y: 1, NUM: 1}, {X: 4, Y: 1, NUM: 1}, ...]
          - true:  需給定所有 dut value 之 array，但不需座標，會自動認定 array index 為 dut number
                   ex: [0, 0, 0, 1, 0, ...]
      */
      templateUrl: 'mtbf/views/dutMap.html',
      link: function(scope, element, attrs){
        scope.dutMaps = [];
        scope.xlabel = null;
        scope.duts = [];

        // 若為單板，則顯示 dutmap
        scope.$watch('duts', function(newVal, oldVal){
          if (!_.isEmpty(newVal)) {
            // 製作 x 軸 label (y 軸 label 可在頁面上自動產生)
            scope.xlabel = _.reverse(_.map(_.range(scope.binfo.ASSX), r => r + 1));
            if (scope.useDutNum) {  // [使用 dut number]
              scope.dutMaps = _.map(_.range(scope.binfo.ASSY), y => _.map(_.range(scope.binfo.ASSX), x => 0));
              _.forEach(scope.duts, (d, indx) => scope.dutMaps[Math.floor(indx/scope.binfo.ASSX)][indx % scope.binfo.ASSX] = d);
            } else {  // [使用 ASSX/ASSY 座標]
              // 建立空 map 及 填上次數
              scope.dutMaps = _.map(_.range(scope.binfo.ASSY), y => _.map(_.range(scope.binfo.ASSX), x => 0));
              _.forEach(scope.duts, d => scope.dutMaps[d.Y - 1][scope.binfo.ASSX - d.X] = d.NUM);
            }
          }
        }, true);

        scope.btnStyle = function(x, y, num){
          var style = {};
          var red = (n) => (255 - n >= 0) ? (255 - n * 5) : 0;
          style["background-color"] = num > 0 ? "rgb(" + red(num) + ",51,51)" : "#85E085";
          return style;
        }

        scope.exportDutMap = function(id){
          html2canvas($("#dutmap-" + id), {
            background: "#ffffff",
            onrendered: function(canvas) {
              // Convert and download as image 
              var url = canvas.toDataURL();
              $("<a>", {
                href: url,
                download: "dutmap.png"
              })
              .on("click", () => $(this).remove())
              .appendTo("body")[0].click()
            }
        });
        }
      },
      controller: ['$scope', function($scope){
        
      }]
    };
  };
  
  angular.module('mean.mtbf').directive('mtbfDutMap', mtbfDutMap);
  mtbfDutMap.$inject = ['_'];
})();