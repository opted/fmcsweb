(function() {
    'use strict';

    function Mtbf($stateProvider) {
        $stateProvider.state('mtbf main page', {
            url: '/mtbf',
            templateUrl: 'mtbf/views/index.html',
            controller:MtbfController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              }
            }
        })
            .state('mtbf circles example', {
            url: '/mtbf/example/:circle',
            templateUrl: 'mtbf/views/example.html'
        });
    }

    angular
        .module('mean.mtbf')
        .config(Mtbf);

    Mtbf.$inject = ['$stateProvider'];

})();
