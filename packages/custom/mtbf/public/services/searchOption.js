(function(){
    'use strict';
    function srchOpt(_, utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        var params = {};
        var fields = [
            uOpt.genField("客戶", "CUSTOMERCHISNAME"),
            uOpt.genField("BIB Type 三碼", "BIBTYPE"),
            uOpt.genField("Family 板全號", "BOARDNO"),
            uOpt.genField("配件編號", "ASSNO"),
            uOpt.genField("舊配件編號", "ASSSERIALNO")
        ];
        return {
            params: params,
            fields: fields,
            getSize: () => fields.length,
            getMatch: uOpt.getMatch,
            genTabTitle: (subMatch) => {
              if (_.isEmpty(subMatch)){
                return "Overall";
              } else {
                return _.join(_.map(subMatch, (v, k) => "[" + _.find(fields, {'name': k}).label + "] " + v), " ");
              }
            },
            getRemainField: (subMatch) => _.reject(fields, f => _.includes(_.keys(subMatch), f.name))
        };
    };

    angular.module('mean.mtbf').factory('mtbfSrchOpt', srchOpt);
    srchOpt.$inject = ['_', 'utilsSrchOpt'];
})();