(function(){
  'use strict';
  function mtbfChartService(_) {
    var serialOption = {
      "type": "serial",
      "theme": "none",
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "MTBF 分析", "size": 30}
      ],
      "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 120
      },
      "dataProvider": [],
      "valueAxes": [{
        "id": "numOfRepairAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "title": "掛修次數",
        "titleFontSize": 20
      },{
        "id": "mtbfAxis",
        "axisAlpha": 1,
        "gridAlpha": 0,
        "position": "right",
        "title": "MTBF(%)",
        "titleFontSize": 20
      }],
      "graphs": [{
        "alphaField": "alpha",
        "balloonText": "[[value]] 次",
        "dashLengthField": "dashLength",
        "fillAlphas": 0.8,
        "fillColors": "#99CCFF",
        "lineColor": "#0066CC",
        "legendPeriodValueText": "total: [[value.sum]]",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "top",
        "showAllValueLabels": true,
        "color": "#3366CC",
        "fontSize": 15,
        "title": "維修次數",
        "type": "column",
        "valueField": "REPAIR",
        "valueAxis": "numOfRepairAxis"
      },{
        "bullet": "square",
        "bulletBorderAlpha": 1,
        "bulletBorderThickness": 1,
        "bulletColor": "#FF6600",
        "lineColor": "#FF9933",
        "dashLengthField": "dashLength",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "left",
        "showAllValueLabels": true,
        "color": "#FF0000",
        "fontSize": 15,
        "title": "MTBF(%)",
        "fillAlphas": 0,
        "valueField": "MTBF",
        "valueAxis": "mtbfAxis"
      }],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "GRP",
      "export": {
        "enabled": true
      },
      "categoryAxis": {
        "autoRotateCount": 8,
        "autoRotateAngle": -45
      }
    };

    var failureModeOption = {
      "type": "serial",
      "theme": "none",
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "Failure Mode 分析", "size": 30}
      ],
      "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 120
      },
      "dataProvider": [],
      "valueAxes": [{
        "id": "numOfFailAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "title": "Fail 次數",
        "titleFontSize": 20
      }],
      "graphs": [{
        "alphaField": "alpha",
        "balloonText": "[[value]] 次",
        "dashLengthField": "dashLength",
        "fillAlphas": 0.8,
        "fillColors": "#FFA07A",
        "lineColor": "#FF8C00",
        "legendPeriodValueText": "total: [[value.sum]]",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "top",
        "showAllValueLabels": true,
        "color": "#800000",
        "fontSize": 15,
        "title": "Fail 次數",
        "type": "column",
        "valueField": "FAIL",
        "valueAxis": "numOfFailAxis"
      }],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "ERRCODE",
      "export": {
        "enabled": true
      },
      "categoryAxis": {
        "autoRotateCount": 8,
        "autoRotateAngle": -45
      }
    };

    return {
      getSerialOption: () => _.cloneDeep(serialOption),
      getFailureModeOption: () => _.cloneDeep(failureModeOption)
    };
  };

  angular.module('mean.mtbf').factory('mtbfChartService', mtbfChartService);
  mtbfChartService.$inject = ['_'];
})();