(function(){
  'use strict';
  function mtbfTabService(_, $timeout, mso) {
    var tabs = [];
    return {
      list: tabs,
      getSize: () => tabs.length,
      removeTab: (i) => tabs.splice(i, 1),
      getLastTab: () => _.get(_.last(tabs), 'id'),
      addTab: function(subMatch) {
        var newTab = {
          title: mso.genTabTitle(subMatch),
          subMatch: subMatch,
          fields: mso.getRemainField(subMatch),
          id: new Date().getTime()
        };
        $timeout(tabs.push(newTab));
      }
    };
  };

  angular.module('mean.mtbf').factory('mtbfTabService', mtbfTabService);
  mtbfTabService.$inject = ['_', '$timeout', 'mtbfSrchOpt'];
})();