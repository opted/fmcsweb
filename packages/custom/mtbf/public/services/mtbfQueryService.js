(function(){
  'use strict';
  function mtbfQueryService(_, $http, $window){
    var isSearching = false;
    var schema = "HISMFT";
    var status = {
      str: "",
      err: ""
    };

    var config = {
      eventHandlers: {
      progress: function(event){
        // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
        status.str = "正在下載資料...";
      },
      // readystatechange: function(event){
      //   console.log("STATUS:", event);
      // }
      }
    };
    return {
      setSchema: (s) => schema = s,
      isSearching: () => isSearching,
      status: status,
      query: function(queryParam, dataType, callback){
        isSearching = true;
        queryParam.schema = schema;
        status.str = "等待 Server 回應...";
        status.queryTime = "";
        status.rowNum = 0;
        var startTime = new Date();        
        return $http.post('/api/mtbf/' + _.toLower(dataType), queryParam, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          callback(result.data);
          status.str = "查詢完成";
          status.queryTime = new Date() - startTime;
          status.rowNum = result.data.length;
        }, function(err){
          console.log(err);
          $window.alert("Server failed...");
          status.str = '查詢失敗, '+err.data
          status.err = err;
          status.rowNum = 0;
        }).finally(() => isSearching = false);
      },
      update: function(queryParam, dataType, callback){
        return $http.post('/api/mtbf/' + _.toLower(dataType), queryParam)
        .then(
          result => callback(result.data), 
          err => $window.alert("Server failed..." + err)
        );
      },
      get: function(dataType, callback){
        return $http.get('/api/mtbf/' + _.toLower(dataType))
        .then(
          result => callback(result.data), 
          err => $window.alert("Server failed..." + err)
        );
      }
    };
  };

  angular.module('mean.mtbf').factory('mtbfQueryService', mtbfQueryService);
  mtbfQueryService.$inject = ["_", "$http", "$window"];
})();
