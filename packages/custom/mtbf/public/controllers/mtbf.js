'use strict';

/* jshint -W098 */
function MtbfController(_, $scope, $window, $timeout, $rootScope, mso, mqs, mts) {
  $scope.selected = {
    "from": moment().startOf('day')._d,  // Set default start day
    "to": moment().startOf('day')._d,  // Set default end day
  };

  $scope.isSearching = mqs.isSearching;  // 卡介面操作
  $scope.rowNum = 0;  // 資料筆數
  $scope.fields = mso.fields;  // 網頁的查詢欄位
  $scope.fieldSearchURL = "/api/mtbf/getsearchfield";
  $scope.requestStatus = mqs.status; // http Request 當下狀態
  $scope.tabs = mts;
  $scope.llm = null;  // 維修次數下限
  $scope.hlm = null;

  // Tab 數量增減時，跳最後一個
  $scope.$watch('tabs.getSize()', nv => nv > 0 ? $timeout(() => $rootScope.active = $scope.tabs.getLastTab()) : {});

  // 設定預設 schema
  $scope.selected.schema = "HISMFT";
  $scope.$watch('schema', nv => {
    if (nv){
      mqs.setSchema(nv);
      $scope.selected.schema = nv;
    }
  });

  $scope.ok = function() {
    $scope.rowNum = 0;
    var params = mso.params; // 搜尋面板條件

    // 設定時間
    _.assign(params, _.cloneDeep(_.pick($scope.selected, ['from', 'to'])));  // [NOTE] cloneDeep 是因為 _.pick 回傳的是淺層複製
    if (params.to !== null){
      params.to.setDate(params.to.getDate() + 1);  // 讓結束天 = 另一天的起頭  
    }

    // 設定其他參數
    params.match = mso.getMatch($scope.fields);
    params.limit = {
      llm: $scope.llm,
      hlm: $scope.hlm
    };

    if (_.isEmpty(_.pickBy(params, _.identity)) && _.isEmpty($scope.sheetData)){
      $window.alert("請設定至少一種搜尋條件(時間、客戶、BIB Type、板全號、配件/舊配件編號、Failure Mode)...");
      return;
    }

    // 新增 1st Tab
    $scope.tabs.addTab({});
  };
}

angular
  .module('mean.mtbf')
  .controller('MtbfController', MtbfController);

MtbfController.$inject = ['_', '$scope', '$window', '$timeout', '$rootScope', 'mtbfSrchOpt', 'mtbfQueryService', 'mtbfTabService'];
