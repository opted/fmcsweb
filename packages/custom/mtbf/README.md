README: mtbf

[定義]
----
期望找到"兩次維修間的平均時間區間"，因此 MTBF 值越小表示維修間距越短，越不理想。
1. Dut MTBF
    時間區間內最後一張維修單時間減去第一張維修單之時間(hr) /（維修單數-1）
    例外：當區間內該Dut之維修單數= 1時，分母設為1，分子為（查詢時間之End Time-維修單時間）；當維修單數= 0時，不列入計算。
2. Board MTBF
    該 Board 所有 Dut MTBF 之最小值
3. 上層 Group 的 MTBF
    該 Group 所有 Borad MTBF 之最小值
