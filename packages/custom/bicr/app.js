'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Bicr = new Module('bicr');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Bicr.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Bicr.routes(app, auth, database, utils);
  Bicr.aggregateAsset('css', 'bicr.css');
  Bicr.angularDependencies(['agGrid']);
  return Bicr;
});
