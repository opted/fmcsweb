(function() {
  'use strict';
  module.exports = function(Bicr, app, auth, database, utils) {
    var bicrManager = require('../controllers/bicrQuery');
    var _ = require('lodash');
    var bicrMgr = new bicrManager(utils);
    var timeout = 30*60*1000; // 30 min
    var checkSchema = utils.ctrls.mes.checkSchema;

    app.post('/api/bicr/getsearchfield', function(req, res, next){
      req.setTimeout(timeout);
      bicrMgr.getSearchField(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    app.post('/api/bicr/getgroup', checkSchema, function(req, res, next){
      req.setTimeout(timeout);
      bicrMgr.getGroup(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    app.post('/api/bicr/gettrend', checkSchema, function(req, res, next){
      req.setTimeout(timeout);
      bicrMgr.getTrend(req.body)
      .then(data => res.json(data))
      .catch(err => handleError(err, res));
    });

    function handleError(err, res) {
      console.log(err);
      res.status(500).send({'error': err});
    }
  };
})();
