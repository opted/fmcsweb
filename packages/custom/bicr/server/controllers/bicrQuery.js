'use strict'
var _ = require('lodash');
var Q = require('q');
var moment = require('moment');
var mongoose = require('mongoose');

function getFieldStr(field){
  switch (field){
    case 'CUSTOMERCHISNAME':
      return 'c.' + field;
      break;
    case 'CUSTOMERLOTNO':
    case 'DEVICENO':
    case 'PRODUCTNO':
    case 'LOTNO':
      return 'l.' + field;
      break;
    default:
      throw Error("Can't identify the query field: " + field);
  }
}

/*
* ex: ` r.ASSNO = '40010018' 
*       AND r.BOARDNO = '10204194' `
*/
function genMatch(cond, callback){
  return _(cond).pick(['CUSTOMERCHISNAME', 'CUSTOMERLOTNO', 'DEVICENO', 'PRODUCTNO', 'LOTNO'])
    .map((v, k) => callback(k) + ` = '` + v + `' `).value().join('\n AND ');
}

// 取得lot by group
function getMesLotSQL(schema, match){
  var mtchFld = genMatch(match, getFieldStr);
  var sql = `
    SELECT
      c.CUSTOMERCHISNAME,
      l.CUSTOMERLOTNO,
      l.DEVICENO,
      l.PRODUCTNO,
      l.LOTNO,
      l.CREATEDATE,
      p.VALUE as HOURS
    FROM
       `+ schema + `.TBLWIPLOTDATA l,
       `+ schema + `.TBLWIPLOTOPRECIPE r,
       `+ schema + `.TBLENGPRODOPRECIPECONTENT p,
      Masterdm.Dim_Customer c
    WHERE
      l.CREATEDATE BETWEEN :fromdate AND :todate
      AND l.CUSTOMERNO = c.CUSTNO
      AND r.LOTNO = l.LOTNO
      AND r.OPNO LIKE 'BURNIN%'
      AND r.OPRECIPEID = p.RECIPEID
      AND p.ATTRIBNO = 'BIhourCycle'
      ` + (_.isEmpty(mtchFld) ? '' : 'AND ' + mtchFld ) + `
  `;
  return sql;
}

// 取得實際 BI 時數 (from SysLog)
function getRealBIHour(lotno){
  var SysLog = mongoose.model('SysLog');
  return SysLog.aggregate([
    {$match: {TYPE: "AUTO", LOTNO: lotno, OVENID: {$not: /BTS/}}},
    {$project: {_id: 0, interval: {$subtract: ["$END", "$START"]}, OVENID: 1, START: 1}},  // 取得該 lot 所有區間
    {$group: {_id: 1, interval: {$sum: "$interval"}, OVENID: {$first: "$OVENID" }, START: {$first: "$START"}}},  // 計算 total 時數
    {$project: {_id: 0, BIHOURS: {$divide: ["$interval", 1000*60*60]}, OVENID: 1, START: 1}}  // 轉換成 hour 單位
  ]).then(rslt => [_.get(_.head(rslt), 'BIHOURS', 0), _.get(_.head(rslt), 'OVENID', null), _.get(_.head(rslt), 'START', null)]);
}


// Count Slot Total/Alarm (from MBI table)
function getSlot(lotno, createdate, mbi){
  var startDate = moment(createdate);
  var fisrtMBITable = 'MBI' + startDate.format('YYMM');
  var getSQL = (table, lot) => `
    SELECT
      SUM(CASE WHEN BoardStatus = 1 THEN 1 ELSE 0 END) as ALARM,
      COUNT(*) as TOTAL
    FROM
      ` + table + `
    WHERE
      lotnum = '` + lot + `'
      AND oven_ID NOT LIKE 'BTS%'
  `;

  var df = Q.defer();
  mbi.exec(getSQL(fisrtMBITable, lotno), {})
  .then(rslt => {
    // 若該月份 MBI 未發現該 lot，則往下一個月的查詢(僅會往下一個月)
    if (_.get(_.head(rslt), 'TOTAL', 0) == 0) {      
      var secondMBITable = 'MBI' + startDate.add(1, 'months').format('YYMM');
      mbi.exec(getSQL(secondMBITable, lotno), {})
      .then(rslt => df.resolve(rslt))
      .catch(err => df.resolve([]));
    } else {
      df.resolve(rslt);
    }
  }).catch(err => df.reject(err));
  return df.promise.then(rslt => {
    var r = _.head(rslt);
    return [_.get(r, 'ALARM', 0), _.get(r, 'TOTAL', 0)];
  });
}


function getDut(lotno, mongo) {
  var DUTLOG = mongoose.model('DUTLOG');
  var pipe = [
    {'$match': {'LOTNO': lotno, 'SLOTINFO': {'$gt': []}}},  // 取得 "有紀錄" 的 dutlog
    {'$sort': {'ENDTIME': -1}}, 
    {'$limit': 1},  // 取得該 lot 最新一筆 Dutlog
    {'$unwind': "$SLOTINFO"},
    {'$unwind': "$SLOTINFO.DUT_LIST"},
    {'$sort': {'SLOTINFO.DATETIME': -1}},  // 排序同一份 dutlog 中 dut 重複測值
    {'$group': {
      '_id': {'Z': '$SLOTINFO.ZONE', 'B': '$SLOTINFO.BOARD', 'D': '$SLOTINFO.DUT_LIST.DUT'},
      'ERRCODE': {'$first': '$SLOTINFO.DUT_LIST.ERRCODE'},
    }},
    {'$group': {
      '_id': 1,
      'TOTAL': {'$sum': {'$cond': {'if': {'$eq': ['$ERRCODE', '00']}, 'then': 0, 'else': 1}}}, // 排除空座(00)
      'ALARM': {'$sum': {'$cond': {'if': {'$or': [  // 計算 alram dut, 不包含 空座(00) 及 好座(FF)
        {'$eq': ['$ERRCODE', '00']}, 
        {'$eq': ['$ERRCODE', 'FF']}
      ]}, 'then': 0, 'else': 1}}}
    }}
  ];
  return mongo.massiveQuery(DUTLOG, pipe)
  .then(rslt => {
    var r = _.head(rslt);
    return [_.get(r, 'ALARM', 0), _.get(r, 'TOTAL', 0)];
  });
}


// 處理時間單位,  ex: "05/12/2017, 11:27:16" => ["05/12/2017", " 11:27:16"] => "2017/12/05 11:27:16"
function formatDateStr(d, field, isUTC){
  d[field] = d[field] ? (isUTC ? moment.utc(d[field]) : moment(d[field])).format("YYYY/MM/DD HH:mm:ss") : null;
}

class BicrManager {
  constructor(utils) {
    this.mes = utils.ctrls.mes;
    this.mbi = utils.ctrls.mbi;
    this.dt = utils.ctrls.dateTool;
    this.mongoQuery = utils.ctrls.mongoQuery;
  }

  getSearchField (query) {
    try {
      // 取得其他欄位的條件 (omit 用來排除要搜尋的欄位)
      var mtchFld = genMatch(_.omit(query.match, query.field), getFieldStr);
      var field = getFieldStr(query.field);
      var schema = query.exmatch.schema;
      var sql = `
        SELECT 
          DISTINCT r.` + query.field + `
        FROM (
          SELECT
            ` + field + `
          FROM
            ` + schema + `.TBLWIPLOTDATA l,
            Masterdm.Dim_Customer c
          WHERE
            l.CUSTOMERNO = c.CUSTNO
            AND UPPER(` + field + `) LIKE :regex
            ` + (_.isEmpty(mtchFld) ? '' : 'AND ' + mtchFld ) + `
        ) r
        WHERE
          rownum <= :rowlimit
      `;

      var vars = {
        regex: _.toUpper(query.regex) + '%',
        rowlimit: 100,
      };
      return this.mes.exec(sql, vars)
        .then(rslt => _.map(rslt, query.field));
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getGroup (query) {
    try {
      var sql = getMesLotSQL(query.schema, query.match);
      var vars = {
        fromdate: new Date(query.from),
        todate: new Date(query.to)
      };

      // 取得 lot 列表
      var mbi = this.mbi;
      var mongo = this.mongoQuery;
      return this.mes.exec(sql, vars)
      .then(lots => {
        // 取得每個 Lot 的資料
        var qs = _.map(lots, lot => {
          var df = Q.defer();
          try {
            var sysInfo = getRealBIHour(lot.LOTNO);  // 取得 BI 實際時數 (MongoDB.SysLog)
            var aSlot = getSlot(lot.LOTNO, lot.CREATEDATE, mbi);  // 取得 Alarm & Total Slot (MBI 年月)
            var aDut = getDut(lot.LOTNO, mongo)// 取得 Alarm & Total Dut (MongoDB.DUT_logs)

            Q.all([sysInfo, aSlot, aDut]).then(rslt => {
              var [realHr, oven, start_t] = rslt[0];
              var [alarmSlot, totalSlot] = rslt[1];
              var [alarmDut, totalDut] = rslt[2];

              // 補上資訊
              lot.HOURS = Number(lot.HOURS);  // 預估 BI 時數
              lot.REALHRS = _.round(realHr, 4);  // 實際 BI 時數
              lot.OVENID = oven;  // Oven id
              lot.START = start_t;  // BI start time
              lot.ALARMSLOT = alarmSlot;  // alarm Slot
              lot.TOTALSLOT = totalSlot;  // total Slot
              lot.ALARMDUT = alarmDut;  // alarm Slot
              lot.TOTALDUT = totalDut;  // total Slot

              // 計算 BICR
              lot.BIRATE = lot.HOURS > 0 ? _.min([_.divide(lot.REALHRS, lot.HOURS), 1]) : 0;  // BI時數比
              lot.ALARMSLOTRATE = _.round(_.divide(lot.ALARMSLOT, lot.TOTALSLOT), 4);  // Alarm Slot Rate
              lot.ALARMDUTRATE = _.round(_.divide(lot.ALARMDUT, lot.TOTALDUT), 4);  // Alarm Dut Rate
              lot.ISHIGHPOWER = lot.TOTALDUT > 0;  // 判斷 High Power 還是 Normal
              lot.BICR = lot.BIRATE * (1 - (lot.ISHIGHPOWER ? lot.ALARMDUTRATE : lot.ALARMSLOTRATE));  // BICR
              lot.BICR = _.round((lot.BICR ? lot.BICR : 0)*100, 4);  // 以百分比顯示

              formatDateStr(lot, "CREATEDATE");
              formatDateStr(lot, "START", true);
              df.resolve(lot);
            }).catch(ee => df.reject(ee));
          } catch(e) {
            df.reject(e);
          }
          return df.promise;
        });

        return Q.all(qs).then(rslt => {
          rslt = _.filter(rslt, r => r.REALHRS > 0);

          // 合併相同 lot，京元要求，不要問我為什麼
          rslt = _(rslt).groupBy("LOTNO").map(lotSet => {
            var lot = _.head(lotSet);

            // 補上資訊
            lot.HOURS = _.sumBy(lotSet, "HOURS");  // 預估 BI 時數
            lot.ALARMSLOT = _.sumBy(lotSet, "ALARMSLOT");  // alarm Slot
            lot.TOTALSLOT = _.sumBy(lotSet, "TOTALSLOT");  // total Slot
            lot.ALARMDUT = _.sumBy(lotSet, "ALARMDUT");  // alarm Slot
            lot.TOTALDUT = _.sumBy(lotSet, "TOTALDUT");  // total Slot

            // 計算 BICR
            lot.BIRATE = lot.HOURS > 0 ? _.min([_.divide(lot.REALHRS, lot.HOURS), 1]) : 0;  // BI時數比
            lot.ALARMSLOTRATE = _.round(_.divide(lot.ALARMSLOT, lot.TOTALSLOT), 4);  // Alarm Slot Rate
            lot.ALARMDUTRATE = _.round(_.divide(lot.ALARMDUT, lot.TOTALDUT), 4);  // Alarm Dut Rate
            lot.ISHIGHPOWER = lot.TOTALDUT > 0;  // 判斷 High Power 還是 Normal
            lot.BICR = lot.BIRATE * (1 - (lot.ISHIGHPOWER ? lot.ALARMDUTRATE : lot.ALARMSLOTRATE));  // BICR
            lot.BICR = _.round((lot.BICR ? lot.BICR : 0)*100, 4);  // 以百分比顯示

            return lot;
          }).value();

          return rslt;
        });
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  }

  getTrend (query) {
    try {
      var [from, to, range, prd] = this.dt.getPeriodSetting(
        query.from, query.to, query.pRange, query.groupBy, null, null, null)
      query.from = from;
      query.to = to;
      return this.getGroup(query)
      .then(rslt => {
        rslt = _(rslt).groupBy(r => moment(new Date(r.CREATEDATE).toISOString()).startOf('day')._d).map((lots, grp) => {
          return {
            GRP: grp,
            BICR: _.round(_.meanBy(lots, "BICR"), 2),
            numOfLot: lots.length
          };
        }).value().sort((a, b) => new Date(a.GRP) - new Date(b.GRP));
        return rslt;
      });
    } catch (err){
      return Q.reject(err.stack);
    }
  }
}

module.exports = BicrManager;