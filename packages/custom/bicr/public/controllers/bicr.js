'use strict';

function BicrController(_, $scope, $timeout, $rootScope, bso, bqs, bts) {
  // $scope.selected = {
  //   "from": moment().startOf('day')._d,
  //   "to": moment().startOf('day')._d
  // };

  $scope.selected = {
    "from": moment().subtract(1, 'd').startOf('day')._d,
    "to": moment().subtract(1, 'd').startOf('day')._d
  };

  $scope.isSearching = bqs.isSearching;  // 卡介面操作
  $scope.fields = bso.fields;  // 網頁的查詢欄位
  $scope.fieldSearchURL = "/api/bicr/getsearchfield";
  $scope.requestStatus = bqs.status; // http Request 當下狀態
  $scope.tabs = bts;

  // Tab 數量增減時，跳最後一個
  $scope.$watch('tabs.getSize()', nv => nv > 0 ? $timeout(() => $rootScope.bicr_active = $scope.tabs.getLastTab()) : {});

  $scope.selected.schema = "HISMFT";
  $scope.$watch('schema', nv => {
    if (nv){
      bqs.setSchema(nv);
      $scope.selected.schema = nv;
    }
  });

  $scope.ok = function() {
    $scope.rowNum = 0;
    var params = bso.params; // 搜尋面板條件

    // 設定時間
    _.assign(params, _.cloneDeep(_.pick($scope.selected, ['from', 'to'])));  // [NOTE] cloneDeep 是因為 _.pick 回傳的是淺層複製
    if (params.to !== null){
      params.to.setDate(params.to.getDate() + 1);  // 讓結束天 = 另一天的起頭  
    }

    if (params.to == null || params.from == null) {
      alert("必須指定時間區間");
      return;
    }

    if (moment(params.to).diff(params.from, 'day') > 7) {
      alert("時間區間不得大於7天");
      return;
    }

    // 設定其他參數
    params.match = bso.getMatch($scope.fields);

    // 新增 1st Tab
    $scope.tabs.addTab({});
  };

};

angular
  .module('mean.bicr')
  .controller('BicrController', BicrController);

BicrController.$inject = ['_', '$scope', '$timeout', '$rootScope', 'bicrSrchOpt', 'bicrQueryService', 'bicrTabService'];
