(function(){
  'use strict';

  function bicrDataPanel($sce, $uibModal, _, bqs, bts, bcs, bso){
    return {
      restrict: 'E',
      scope: {
        tb: "="
      },
      templateUrl: 'bicr/views/bicrDataPanel.html',
      link: function(scope, element, attrs){
        scope.fields = scope.tb.fields;  // 網頁的查詢欄位
        scope.groupBy = _.head(scope.fields); // 預設值
        scope.grpLoading = true;  // group loading 畫面
        scope.trdLoading = true;  // group loading 畫面
        scope.showTrend = false;  // 預設不顯示 trend chart
        scope.status = bqs.status;  // Query 狀態
        scope.rowNum = null;  // 維修筆數
        scope.groupData = [];

        scope.grpTips =$sce.trustAsHtml(`
          <div class="form-group">
            <p><b>[提示]</b> 對Bar或方塊</p>
            <ol>
              <li><b>Single Click</b>: 產生推移圖</li>
              <li><b>Double Click</b>: Drill down 層級</li>
            </ol>
            <p><b>[NOTE]</b> 若切換左側推移圖單位/範圍，需重新點選 Bar</p>
          </div>
        `);

        // 推移圖單位
        scope.periods = [
          {label: "日", name: "DAY"},
        ];
        scope.period = scope.periods[0];

        // 推移圖範圍
        scope.pRanges = _.map(_.reverse(_.range(1, 8)), r => new Object({idx: r}));
        scope.pRange = scope.pRanges[6];

        var grpChart;
        var trdChart;

        var grpOpt = bcs.getSerialOption();
        grpOpt.titles[1].text = "Group By " + scope.groupBy.label;
        grpOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.grpLoading = false
        }];

        var trdOpt = bcs.getSerialOption();
        trdOpt.titles[0].text = "BICR 推移分析";
        trdOpt.listeners = [{
          "event": "rendered", 
          "method": (e) => scope.trdLoading = false
        }];

        function drillDown(event){
          if (_.keys(scope.tb.subMatch).length == bso.getSize() - 1) {
            alert("已 Drill down 至最後一層");
          } else {
            // 根據點選的 bar 更新 sub query
            var subQ = _.cloneDeep(scope.tb.subMatch);
            subQ[scope.groupBy.name] = event.item.category;
            bts.addTab(subQ);  // 新增下一個 tab
          }
        }

        function genTrendChart(event){
          scope.showTrend = true;
          scope.trdLoading = true;
          var qry = _.cloneDeep(bso.params);
          _.assign(qry.match, scope.tb.subMatch);
          qry.match[scope.groupBy.name] = event.item.category;  // 設定所選的 group
          qry.groupBy = scope.period.name;  // 選擇 週/月
          qry.pRange = scope.pRange.idx;  // 選擇範圍

          scope.trendGroup = event.item.category;  
          bqs.query(qry, 'gettrend', function(data){
            _.forEach(data, d => d.GRP = moment(new Date(d.GRP)).format("YYYY/MM/DD"));

            var subtitle = scope.groupBy.label + " " + event.item.category + " " + qry.pRange + " " + qry.groupBy;
            if (trdChart) {
              trdChart.titles[1].text = subtitle;
              trdChart.dataProvider = data;
              trdChart.validateNow();
              trdChart.validateData();
            } else {
              trdOpt.titles[1].text = subtitle;
              trdOpt.dataProvider = data;
              trdChart = AmCharts.makeChart('bicr-trdcht-' + scope.$id, trdOpt);
            }
          });
        }

        scope.$watch("groupBy", function(nv, ov){
          if (nv != ov || _.isEmpty(scope.groupData)) {
            scope.grpLoading = true;
            var qry = _.cloneDeep(bso.params);
            _.assign(qry.match, scope.tb.subMatch);
            qry.groupBy = nv.name;
            bqs.query(qry, 'getgroup', result => {
              if (_.isEmpty(result)){
                alert("此條件無任何資料，請重新設定，謝謝~");
              }

              scope.repairGrid.api.setRowData(result);
              scope.lotnum = result.length;
              result = _(result).groupBy(qry.groupBy).map((lots, grp) => {
                return {
                  GRP: grp,
                  BICR: _.round(_.meanBy(lots, "BICR"), 2),
                  numOfLot: lots.length
                };
              }).sortBy("BICR").value();
              scope.groupData = result;
            });
          }
        });

        scope.$watch('groupData', function(nv, ov){
          if (nv != ov) {
            if (!_.isEmpty(nv)) {
              var subtitle = "Group By " + scope.groupBy.label;
              if (!grpChart) {
                grpChart = AmCharts.makeChart('bicr-grpcht-' + scope.$id, grpOpt);

                grpChart.clickTimeout = 0;
                grpChart.lastClick = 0;
                grpChart.doubleClickDuration = 300;

                function clickHandler(event) {
                  var ts = (new Date()).getTime();
                  if ((ts - grpChart.lastClick) < grpChart.doubleClickDuration) {  // [[ Double click ]]
                    if (grpChart.clickTimeout) {
                      clearTimeout(grpChart.clickTimeout);
                    }
                    grpChart.lastClick = 0; // reset last click
                    drillDown(event);
                  } else {  // [[ Single click ]]
                    grpChart.clickTimeout = setTimeout(() => genTrendChart(event), grpChart.doubleClickDuration);
                  }
                  grpChart.lastClick = ts;
                }

                grpChart.addListener("clickGraphItem", clickHandler);
              }
              grpChart.titles[1].text = subtitle;
              grpChart.dataProvider = nv;
              grpChart.validateNow();  // 觸發 listener
              grpChart.validateData(); // 重畫 data
            } else {
              scope.grpLoading = false;
            }
          }
        }, true);
      },
      controller: ['$scope', function($scope){
        $scope.repairGrid = {
          columnDefs: [
            {headerName: "時間", field: "CREATEDATE", width: 130},
            {headerName: "客戶", field: "CUSTOMERCHISNAME", width: 100},
            {headerName: "客批", field: "CUSTOMERLOTNO", width: 120},
            {headerName: "客型", field: "DEVICENO", width: 120},
            {headerName: "京型", field: "PRODUCTNO", width: 120},
            {headerName: "內批", field: "LOTNO", width: 100},
            {headerName: "BI 開始時間", field: "START", width: 130},
            {headerName: "Oven", field: "OVENID", width: 100},
            {headerName: "BI Complete Rate", field: "BICR", width: 140},
            {headerName: "是否為 HighPower", field: "ISHIGHPOWER", width: 140},
            {headerName: "預估總BI時數", field: "HOURS", width: 110},
            {headerName: "實際總BI時數", field: "REALHRS", width: 110},
            {headerName: "BI時數比", field: "BIRATE", width: 90},
            {headerName: "Alarm Slot", field: "ALARMSLOT", width: 90},
            {headerName: "Total Slot", field: "TOTALSLOT", width: 90},
            {headerName: "Alarm Slot Rate", field: "ALARMSLOTRATE", width: 120},
            {headerName: "Alarm Dut", field: "ALARMDUT", width: 90},
            {headerName: "Total Dut", field: "TOTALDUT", width: 90},
            {headerName: "Alarm Dut Rate", field: "ALARMDUTRATE", width: 120},
          ],
          rowData:[],
          groupUseEntireRow: false,
          enableSorting: true,
          enableColResize: true,
          excelStyles: [{
            id: "greenBackground",
            interior: {color: "#aaffaa", pattern: 'Solid'}
          }]
        };
      }]
    };
  };
  
  angular.module('mean.bicr').directive('bicrDataPanel', bicrDataPanel);
  bicrDataPanel.$inject = ['$sce', '$uibModal', '_', 'bicrQueryService', 'bicrTabService', 'bicrChartService', 'bicrSrchOpt'];
})();