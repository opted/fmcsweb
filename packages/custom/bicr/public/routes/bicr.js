(function() {
  'use strict';

  function Bicr($stateProvider) {
    $stateProvider.state('bicr main page', {
      url: '/bicr',
      templateUrl: 'bicr/views/index.html',
      controller:BicrController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.bicr')
    .config(Bicr);

  Bicr.$inject = ['$stateProvider'];

})();
