(function(){
  'use strict';
  function srchOpt(_, uOpt){
    var params = {};
    var fields = [
      uOpt.genField("客戶", "CUSTOMERCHISNAME"),
      uOpt.genField("客批", "CUSTOMERLOTNO"),
      uOpt.genField("客型", "DEVICENO"),
      uOpt.genField("京型", "PRODUCTNO"),
      uOpt.genField("內批", "LOTNO"),
    ];
    return {
      params: params,
      fields: fields,
      getSize: () => fields.length,
      getMatch: uOpt.getMatch,
      genTabTitle: mtch => _.isEmpty(mtch) ? "Overall" : _.join(_.map(mtch, (v, k) => "[" + _.find(fields, {'name': k}).label + "] " + v), " "),
      getRemainField: (subMatch) => _.reject(fields, f => _.includes(_.keys(subMatch), f.name))
    };
  };

  angular.module('mean.bicr').factory('bicrSrchOpt', srchOpt);
  srchOpt.$inject = ['_', 'utilsSrchOpt'];
})();