(function(){
  'use strict';
  function tabService(_, $timeout, bso) {
    var tabs = [];
    return {
      list: tabs,
      getSize: () => tabs.length,
      removeTab: (i) => tabs.splice(i, 1),
      getLastTab: () => _.get(_.last(tabs), 'id'),
      addTab: function(subMatch) {
        var newTab = {
          'id': new Date().getTime(),
          'title': bso.genTabTitle(subMatch),
          'subMatch': subMatch,
          'fields': bso.getRemainField(subMatch)
        };
        $timeout(tabs.push(newTab));
      }
    };
  };

  angular.module('mean.bicr').factory('bicrTabService', tabService);
  tabService.$inject = ['_', '$timeout', 'bicrSrchOpt'];
})();