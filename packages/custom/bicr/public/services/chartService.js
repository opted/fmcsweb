(function(){
  'use strict';
  function ChartService(_) {
    var serialOption = {
      "type": "serial",
      "theme": "none",
      "backgroundColor": "#FFFFFF",
      "backgroundAlpha": 100,
      "titles": [
        {"text": "BICR 分析", "size": 30},
        {"text": "", "size": 15}
      ],
      "legend": {
        "equalWidths": false,
        "useGraphSettings": true,
        "valueAlign": "left",
        "valueWidth": 120
      },
      "dataProvider": [],
      "valueAxes": [{
        "id": "bicrAxis",
        "axisAlpha": 1,
        "gridAlpha": 0.1,
        "position": "left",
        "title": "BICR (%)",
        "minimum": 0,
        "maximum": 100,
        "titleFontSize": 20
      }],
      "graphs": [{
        "alphaField": "alpha",
        "balloonText": `
          <p><b>[[category]]</b></p>
          <p>BICR: [[BICR]]</p>
          <p>lot num: [[numOfLot]]</p>
        `,
        "dashLengthField": "dashLength",
        "fillAlphas": 0.8,
        "fillColors": "#99CCFF",
        "lineColor": "#0066CC",
        "legendPeriodValueText": "BI Complete Rate",
        "legendValueText": "[[value]]",
        "labelText": "[[value]]",
        "labelPosition": "top",
        "showAllValueLabels": true,
        "color": "#3366CC",
        "fontSize": 15,
        "title": "BICR",
        "type": "column",
        "valueField": "BICR",
        "valueAxis": "numOfRepairAxis"
      }],
      "chartCursor": { // 圖上，滑鼠移動時附帶灰色半透明框
        "categoryBalloonDateFormat": "DD",
        "cursorAlpha": 0.1,
        "cursorColor":"#000000",
        "fullWidth":true,
        "valueBalloonsEnabled": false,
        "zoomable": false
      },
      "categoryField": "GRP",
      "export": {
        "enabled": true
      },
      "categoryAxis": {
        "autoRotateCount": 8,
        "autoRotateAngle": -45
      }
    };
    return {
      getSerialOption: () => _.cloneDeep(serialOption),
    };
  };

  angular.module('mean.bicr').factory('bicrChartService', ChartService);
  ChartService.$inject = ['_'];
})();