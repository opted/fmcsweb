(function(){
  'use strict';
  function QueryService(_, $http){
    var isSearching = false;
    var schema = "HISMFT";
    var status = {
      str: "",
      err: ""
    };

    var config = {
      eventHandlers: {
        progress: function(event){
          // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
          status.str = "正在下載資料...";
        }
      }
    };
    return {
      setSchema: (s) => schema = s,
      isSearching: () => isSearching,
      status: status,
      query: function(qry, url, callback){
        isSearching = true;
        qry.schema = schema;
        status.str = "等待 Server 回應...";
        return $http.post('/api/bicr/' + _.toLower(url), qry, config)
        .then(function(result){
          status.str = "處理回傳資料...";
          callback(result.data);
          status.str = "Done";
        }, function(err){
          console.log(err);
          alert("Server failed...");
          status.str = "查詢失敗";
          status.err = err;
        }).finally(() => isSearching = false);
      }
    };
  };

  angular.module('mean.bicr').factory('bicrQueryService', QueryService);
  QueryService.$inject = ["_", "$http"];
})();