'use strict';

function SampleController(_, $scope,$http) {
  $scope.packageName = 'Sample';
  $http
    .post('/api/sample/getBackEndResponse', {'packageName': $scope.packageName})
    .then(function(res){
      console.log(res.data);
    })
};

angular
  .module('mean.sample')
  .controller('SampleController', SampleController);

SampleController.$inject = ['_', '$scope', '$http'];
