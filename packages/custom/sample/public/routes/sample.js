(function() {
  'use strict';

  function Sample($stateProvider) {
    $stateProvider.state('sample main page', {
      url: '/sample',
      templateUrl: 'sample/views/index.html',
      controller:SampleController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.sample')
    .config(Sample);

  Sample.$inject = ['$stateProvider'];

})();
