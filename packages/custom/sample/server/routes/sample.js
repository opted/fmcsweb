(function() {
  'use strict';
  module.exports = function(Sample, app, auth, database, utils) {
    var sampleQuery = require('../controllers/sampleQuery');
    app.post('/api/sample/getBackEndResponse', function(req, res, next){
      sampleQuery
        .getBackEndResponse(req.body)
        .then(function(){
          res.sendStatus(201);
        }, function(err){
          res.status(500).send(err);
        });
    });
  };
})();
