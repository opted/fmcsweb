'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Sample = new Module('sample');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Sample.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Sample.routes(app, auth, database, utils);
  Sample.aggregateAsset('css', 'sample.css');
  Sample.angularDependencies(['agGrid']);
  return Sample;
});
