'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Bakdetail = new Module('bakdetail');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Bakdetail.register(function(app, auth, database, utils) {

  //We enable routing. By default the Package Object is passed to the routes
  Bakdetail.routes(app, auth, database, utils);
  
  Bakdetail.angularDependencies(['agGrid']);
  return Bakdetail;
});
