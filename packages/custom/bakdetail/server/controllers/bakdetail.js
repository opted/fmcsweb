'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Q = require('q'),
    _ = require('lodash');
var dbConfig = {
  user: process.env.NODE_ORACLEDB_USER || "EDAAP",
  password: process.env.NODE_ORACLEDB_PASSWORD || "wpfbaxd3d",
  connectString: process.env.NODE_ORACLEDB_CONNECTIONSTRING || "10.5.139.117:1801/CHDMPD3",
  externalAuth: process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
};
var bakdetail = mongoose.model('Baking'); //BakDetail_logs collection
var ovenDefaultTemp = 327.67;
var gvar = require('../../../utils/server/controllers/globalVariables');

var mesconn = undefined;
exports.initMesConnection = function(utils){
  mesconn = utils.ctrls.mes;
};

var biRealTimeCal = function(res,lotno,temp){
  bakdetail.find({LOTNO:lotno}).sort({DATETIME:1}).exec(function(err,result){
    if(err)
      return res.status(400).json('bakdetail query error! '+err.message);
    // struture example:
    // {lotno:"ABCDE01",ovenid:"OC-56",
    //  data:[
    //  {time:datetime,s0:123.1,s1:123.s2,...,s5:122.5}, // per second
    //  ...],
    //  bitemp:126,
    //  standard:123}
    try{
      var newtemp=standardTempToNumber(temp);
      var stemp=newtemp;
      if(stemp) // standard temperature = bi emperature -3
        stemp=stemp-3;
      var resResult = {lotno:lotno,ovenid:result[0]['OVENID'],data:[],bitemp:newtemp,standard:stemp,status:''};
      result.forEach(function(doc){
        var datetime = doc['DATETIME'].getTime();
        doc['MINUTES'].forEach(function(mindata){
          var mins = mindata['TIME'];
          mindata['SECONDS'].forEach(function(secdata){
            if(secdata['SENSOR0']<ovenDefaultTemp){ // Ignore temp bigger than 327.67 temperature
              var secs = secdata['TIME'];
              var time = new Date(datetime+(60*mins+secs)*1000);
              var dataElement = {time:time.toISOString().substring(0,19)};
              for(var i=0;i<6;i++){
                dataElement['s'+String(i)]=secdata['SENSOR'+String(i)];
              }
              resResult['data'].push(dataElement);
            }
          });
        });
      });
      // Append customer for this lotno
      var qcmd = "select lotdata.lotno,lotdata.customerno,dimcustomer.customerchisname "+
          "from HISMFT.TBLWIPLOTDATA lotdata left outer join Masterdm.DIM_Customer dimcustomer "+
          "on lotdata.customerno=dimcustomer.custno where lotdata.lotno like :lotno";
      mesconn.exec(qcmd,{'lotno':lotno}).then(function(result){
        if(result[0])
          resResult['customername']=result[0]['CUSTOMERCHISNAME'];
        
        resResult['data'] = _.sortBy(resResult['data'],function(o){return o.time});
        if(result.length>0 && resResult['data'].length==0)
          resResult['status']='所有溫度皆大於 327.67 度';
        else if(result.length==0 && resResult['data'].length==0)
          resResult['status']='MongoDB 查無此資料';
        // Ignore temp bigger than 327.67 temperature at the start and end
        return res.status(200).json(resResult); // If customer accept one time query only one lotno then do not doing simplify, send all data to frontend
      });
    }
    catch(err){
      console.log('bakdetail query error!',err.stack);
      return res.status(400).json('bakdetail query error! '+err);
    }
  });
};

var standardTempToNumber = function(temp){
  if(typeof(temp)=='string'){
    if(temp.includes('+/-')){
      return Number(temp.split('+/-')[0]);
    }
    else
      return Number(temp);
  }
  else
    return Number(temp);
};

exports.burnInRealTimeInfo = function(req, res, next) {
  var qcmd = req.body;
  var usedFields=['OVENID','LOTNO'];
  //remove not used field
  for(var key in qcmd)
    if(!(usedFields.includes(key)))
      delete qcmd[key];
  //var example_LOTNO = 'NH702HVBP1'; // lotno which have standard temparture: NH4027WAF1(small), NH702HVBP1(small), LH80WMHAA1(too big...6mb),
  var selected_LOTNO = qcmd.LOTNO;
  if(!selected_LOTNO)
    return res.status(400).json('LOTNO is undefined.');
    
  // 1. Query OVENID from BakDetail_logs by LOTNO
  bakdetail.find(qcmd).limit(1)
  .then(function(result1){
    if(result1.length<=0)
      return res.status(400).json('data not found.');
    var ovenid = result1[0].OVENID;
    // 2. Query Temperature
    var qcmd2="select A.LOTNO,B.VALUE from HisMFT.TBLWIPLOTLOG A \
    left join HisMFT.TBLENGPRODOPRECIPECONTENT B on A.OPRECIPEID=B.RECIPEID \
    where A.EQUIPMENTNO=:ovenid and A.LOTNO=:lotno and B.ATTRIBNO='BITemp'";
    var params2 = {'ovenid':ovenid,'lotno':selected_LOTNO};
    mesconn.exec(qcmd2,params2).then(function(result2){
      var lotTempMap = {};
      lotTempMap[selected_LOTNO]=undefined;
      
      var rebical = function(temp){
        biRealTimeCal(res,selected_LOTNO,temp);
      }
      if(result2.length<=0){ // 未出爐資料, using following query
        var qcmd3 = "select VALUE from HISMFT.TBLENGPRODOPRECIPECONTENT where RECIPEID in \
        (select OPRECIPEID from HISMFT.TBLWIPLOTSTATE where LOTSTATE=2 and LOTNO='"+selected_LOTNO+"') and ATTRIBNO='BITemp'"
        mesconn.exec(qcmd3,[]).then(function(result3){
          if(result3.length<=0)
            rebical(undefined);
          else
            rebical(result3[0]['VALUE']);
        })
        .catch(function(err){
          return res.status(400).json('TBLENGPRODOPRECIPECONTENT query error! '+err.message);
        });
      }
      else{
        rebical(result2[0]['VALUE']);
      }
    })// TBLWIPLOTLOG query
    .catch(function(err){
      return res.status(400).json('TBLWIPLOTLOG query error! '+err.message);
    });
  })
  .then(function(err){
   return res.status(400).json('bakdetail_logs query error! '+err.toString());
  });
};

exports.mesGetSearchField = function(req, res, next){ // search field too often, do not save requestlog
  var timerange = req.body.exmatch.datetime;
  var to = new Date(timerange.to),
    from = new Date(timerange.from),
    field = req.body.field,
    match = req.body.match; //{LOTNO:,CUSTOMERCHISNAME:}
  // do not modified time zone, oracle db will automatically do it
  // append 'startwtih' regexp '^'
  var matchField = "";
  var keys = _.keys(match);
  var qcmd='';
  var params={};
  
  if(keys.length==1 && keys.indexOf('CUSTOMERCHISNAME')>=0){ // only customer name
    qcmd="select DISTINCT CUSTOMERCHISNAME from Masterdm.DIM_Customer where RegExp_like(CUSTOMERCHISNAME,'^"+match['CUSTOMERCHISNAME']+"','i')";
  }
  else{
    var handelMatchField = function(matchField){
      return matchField+" and ";
    };
    for(var key in match){
      match[key]='^'+match[key];
      var table="";
      if(key=='CUSTOMERCHISNAME' || key=='CUSTOMERNAME')
        table='B'
      else if(key=='PACKAGETYPE')
        table='C'
      else // LOTNO, CUSTOMERLOTNO, EQUIPMENTNO
        table='A'
      matchField = handelMatchField(matchField);
      matchField+="RegExp_like("+table+"."+key+",'"+match[key]+"','i')";
    }
    // Set filed with table
    var fieldWithTable = '';
    if(field=='LOTNO')
      fieldWithTable = 'A.'+field;
    else
      fieldWithTable = field;
    
    qcmd= "select "+field+" from (select max("+fieldWithTable+") "+field+
        " from HISMFT.TBLWIPLOTLOG A "+
        "left join HISMFT.TBLWIPLOTDATA C on A.LOTNO=C.LOTNO "+
        "inner join Masterdm.DIM_Customer B on C.CUSTOMERNO=B.CUSTNO "+
        "where RegExp_like(OPNO,'BURNIN*','i')"+
        //" where A.REVISEDATE >= :fromDate and A.REVISEDATE < :toDate and rownum<20"+
        matchField+" group by "+fieldWithTable+") where rownum<="+gvar.searchFilterMaximumRowNum;
    //params['fromDate']=from;
    //params['toDate']=to;
  }
  mesconn.exec(qcmd,params).then(function(result){
    result = _.map(result,field);
    result = _.orderBy(result);
    return res.json(result);
  })
  .catch(function(err){
    return res.status(400).json('query error!');
  });
};

exports.fieldSearch = {
  coll: bakdetail,
  pathMapping: {
    'OVENID': 'OVENID',
    'LOTNO': 'LOTNO'
  },
  arrayFields: []};