'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Baking = mongoose.model('Baking'),
  _ = require('lodash'),
  Q = require('q');
var oqtool = require('../../../rebi/server/controllers/odbQueryTool.js');
var mesconn = undefined;
exports.initMesConnection = function(utils){
  mesconn = utils.ctrls.mes;
};

var groupType = {
  MINUTE: 0,
  HOUR: 1
};


function trans2UTC(date) {
  return new Date(Date.UTC(
    date.getFullYear(),
    date.getMonth(),
    date.getDate(),
    date.getHours(),
    date.getMinutes()
  ));
}

function modifyDatetimeQuery(obj) {
  if ('$gte' in obj) obj.$gte = trans2UTC(new Date(obj.$gte));
  if ('$lt' in obj) obj.$lt = trans2UTC(new Date(obj.$lt));
  return _.omit(obj, function(val) {
    return _.isNaN(val.valueOf());
  });
}

function modifyQuery(query) {
  return _.mapValues(query, function(value, key) {
      return (key === 'DATETIME') ? 
        modifyDatetimeQuery(value) : ( _.isArray(value) ? value : [value] );
    });
}

function maxFun(keys) {
  function maxReduce(key0, keysOthers) {
    if (keysOthers.length) {
      var res = {
        $cond: {
          if: { $gt: [key0, keysOthers[0]] },
          then: maxReduce(key0, keysOthers.slice(1)),
          else: maxReduce(keysOthers[0], keysOthers.slice(1))
        }
      };
      return res;
    } else {
      return key0;
    }
  }
  return maxReduce(keys[0], keys.slice(1));
}

function minFun(keys) {
  function minReduce(key0, keysOthers) {
    if (keysOthers.length) {
      var res = {
        $cond: {
          if: { $gt: [key0, keysOthers[0]] },
          then: minReduce(keysOthers[0], keysOthers.slice(1)),
          else: minReduce(key0, keysOthers.slice(1))
        }
      };
      return res;
    } else {
      return key0;
    }
  }
  return minReduce(keys[0], keys.slice(1));
}

exports.getTemp = function(req, res, next) {
  var findQuery = JSON.parse(req.query.find);
  var lowerDate = new Date(findQuery.DATETIME.$gte);
  var upperDate = new Date(findQuery.DATETIME.$lte);
  findQuery.DATETIME.$gte = new Date(Math.floor(lowerDate.valueOf()/1000/60/60)*3600*1000);
  findQuery.DATETIME.$lte = upperDate;
  Baking.aggregate([
      { $match: findQuery }, 
      { $unwind: '$MINUTES' }, 
      { $unwind: '$MINUTES.SECONDS' }, 
      { $project: {
        _id: 0,
        DATETIME: { $add: [ '$DATETIME', { $multiply: [ '$MINUTES.TIME', 60000 ] }, { $multiply: [ '$MINUTES.SECONDS.TIME', 1000] } ]},
        Sensor0: '$MINUTES.SECONDS.SENSOR0',
        Sensor1: '$MINUTES.SECONDS.SENSOR1',
        Sensor2: '$MINUTES.SECONDS.SENSOR2',
        Sensor3: '$MINUTES.SECONDS.SENSOR3',
        Sensor4: '$MINUTES.SECONDS.SENSOR4',
        Sensor5: '$MINUTES.SECONDS.SENSOR5',
        RANGE: '$MINUTES.SECONDS.RANGE'
      }}, 
      { $match: { DATETIME: {$gte: lowerDate, $lte: upperDate}} },
      { $sort: { DATETIME: 1 } }
  ])
  .exec(function(err, data) {
    res.json(data);
  });
};

var groupQuery =  { 
    _id: '$DATETIME',
    Sensor0: {
      $avg: '$MINUTES.SECONDS.SENSOR0'
    },
    Sensor1: {
      $avg: '$MINUTES.SECONDS.SENSOR1'
    },
    Sensor2: {
      $avg: '$MINUTES.SECONDS.SENSOR2'
    },
    Sensor3: {
      $avg: '$MINUTES.SECONDS.SENSOR3'
    },
    Sensor4: {
      $avg: '$MINUTES.SECONDS.SENSOR4'
    },
    Sensor5: {
      $avg: '$MINUTES.SECONDS.SENSOR5'
    },
    MAXRANGE: {
      $max: '$MINUTES.SECONDS.RANGE'
    },
    COUNT: {
      $sum: 1
    }
  };

exports.getTempAggregate = function(req, res, next) {
  var findQuery = JSON.parse(req.query.find);
  var lowerDate = new Date(findQuery.DATETIME.$gte);
  var upperDate = new Date(findQuery.DATETIME.$lte);
  findQuery.DATETIME.$gte = new Date(Math.floor(lowerDate.valueOf()/1000/60/60)*3600*1000);
  findQuery.DATETIME.$lte = upperDate;
  var elementAggregate = Baking.aggregate([
      { $match: findQuery }, 
      { $unwind: '$MINUTES' }, 
      { $unwind: '$MINUTES.SECONDS' }, 
      { $project: {
        _id: 0,
        DATETIME: { $add: [ '$DATETIME', { $multiply: [ '$MINUTES.TIME', 60000 ] }, { $multiply: [ '$MINUTES.SECONDS.TIME', 1000] } ]},
        Sensor0: '$MINUTES.SECONDS.SENSOR0',
        Sensor1: '$MINUTES.SECONDS.SENSOR1',
        Sensor2: '$MINUTES.SECONDS.SENSOR2',
        Sensor3: '$MINUTES.SECONDS.SENSOR3',
        Sensor4: '$MINUTES.SECONDS.SENSOR4',
        Sensor5: '$MINUTES.SECONDS.SENSOR5',
        RANGE: '$MINUTES.SECONDS.RANGE'
      }}, 
      { $match: { DATETIME: {$gte: lowerDate, $lte: upperDate}} },
      { $sort: { DATETIME: 1 } }
  ]);
  var groupAggregate = Baking.aggregate([
      { $match: findQuery }, 
      { $unwind: '$MINUTES' }, 
      { $unwind: '$MINUTES.SECONDS' }, 
      { $project: {
        DATETIME: { $add: [ '$DATETIME', { $multiply: [ '$MINUTES.TIME', 60000 ] } ] },
        'MINUTES.SECONDS.SENSOR0': 1,
        'MINUTES.SECONDS.SENSOR1': 1,
        'MINUTES.SECONDS.SENSOR2': 1,
        'MINUTES.SECONDS.SENSOR3': 1,
        'MINUTES.SECONDS.SENSOR4': 1,
        'MINUTES.SECONDS.SENSOR5': 1,
        'MINUTES.SECONDS.RANGE': 1
      }}, 
      { $match: { DATETIME: {$gte: lowerDate, $lte: upperDate}} },
      { $group: groupQuery },
      { $sort: { _id: 1 } }
  ]);
  Q.all([elementAggregate.exec(), groupAggregate.exec()])
  .then(function(results) {
    if (results[0].length >= 3000 && results[1].length) {
      res.json(results[1])
    } else {
      res.json(results[0])
    }
  });
};


exports.getMenu = function(req, res, next) {
  Baking.aggregate([{
        $group: {
          _id: { 
            STARTTIME: '$STARTTIME',
          LOTNO: '$LOTNO',
          OVENID: '$OVENID'
        }}}, {
        $project: {
          _id: 0,
          STARTTIME: '$_id.STARTTIME',
          LOTNO: '$_id.LOTNO',
          OVENID: '$_id.OVENID'
  }}]).exec(function(err, data) {
    res.json(data);
  });
};

var mesQuery = function(field,match,callback){
  var matchField = '';
  for(var key in match){
    if(key=='DATETIME')
      continue;
    var table="";
    if(key=='CUSTOMERCHISNAME')
      table='B'
    else if(key=='PACKAGETYPE')
      table='C'
    else // LOTNO, CUSTOMERLOTNO, EQUIPMENTNO
      table='A'
    matchField+=" and "+table+"."+key+"='"+match[key]+"'";
  }
  var qcmd= "select DISTINCT A."+field+" from HISMFT.TBLWIPLOTLOG A "+
    "left join HISMFT.TBLWIPLOTDATA C on A.LOTNO=C.LOTNO "+
    "inner join Masterdm.DIM_Customer B on C.CUSTOMERNO=B.CUSTNO "+
    "where RegExp_like(A.OPNO,'BURNIN*')"+matchField+
    " and A.CHECKOUTTIME >= :fromDate and A.CHECKOUTTIME < :toDate";
  var params = {'fromDate':new Date(match.DATETIME['$gte']),'toDate':new Date(match.DATETIME['$lt'])};
  mesconn.exec(qcmd,params).then(function(result){
    callback(undefined,_.map(result,field));
  }).catch(function(err){
    callback(err);
  });
}

exports.getOvenIds = function(match,callback) {
  mesQuery('EQUIPMENTNO',match,callback);
};

exports.getLotNos = function(match,callback) {
  mesQuery('LOTNO',match,callback);
};

function getTimeFromDoc(doc) {
  return doc.DATETIME.valueOf() + doc.MINUTES[0].TIME*60000 + doc.MINUTES[0].SECONDS[0].TIME*1000;
}

function getTempFromDoc(doc) {
  var temps = doc.MINUTES[0].SECONDS[0];
  return {
    SENSOR0: temps.SENSOR0,
    SENSOR1: temps.SENSOR1,
    SENSOR2: temps.SENSOR2,
    SENSOR3: temps.SENSOR3,
    SENSOR4: temps.SENSOR4,
    SENSOR5: temps.SENSOR5,
    DATETIME: new Date(getTimeFromDoc(doc))
  };
}

function getLastSensorTemp(OVENID) {
  return Baking.find({OVENID: OVENID}, {
      _id: 0,
      MINUTES: { $slice:-1 }, 
      'MINUTES.SECONDS': {$slice:-1},
      FILEPATH: 0,
      LOTNO: 0
  })
  .sort({DATETIME:-1})
  .limit(2)
  .exec()
  .then(function(results){
    if (_.isArray(results)) {
      if (results.length === 2) {
        return results[0].STARTTIME > results[1].STARTTIME ?
          getTempFromDoc(results[0]) : getTempFromDoc(results[1]);
      } else if (results.length === 1) {
           return getTempFromDoc(results[0]);
      }
    }
    return {};
  })
  .then(null, function(err) {
    console.log('[ERR-175]', err);
  });
}

function searchMesByLotNo(LOTNOS) {
  var lotnolist = _.keys(LOTNOS);
  var qcmd="select A.DEVICENO, A.CUSTOMERNO, A.PACKAGETYPE, A.LOTNO, A.CUSTOMERLOTNO, B.CUSTOMERCHISNAME "+
  "from HISMFT.TBLWIPLOTDATA A "+
  "left join Masterdm.DIM_Customer B on A.CUSTOMERNO=B.CUSTNO "+
  "where A.LOTNO in :lotnolist";
  qcmd = oqtool.insertListToQcmd(qcmd,{':lotnolist':lotnolist});
  return mesconn.exec(qcmd,{}).then(function(mesResults){
    return _.reduce(mesResults, function(LOTNOS, res) {
      LOTNOS[res.LOTNO] = res;
      return LOTNOS;
    }, LOTNOS);
  });
}

function searchBakByOvenAndDate(DATETIME, OVENID) {
  var boundDate = {};
  if (DATETIME.$lt) boundDate.$lt = DATETIME.$lt;
  if (DATETIME.$gte) {
    boundDate.$gte = new Date(DATETIME.$gte.getFullYear(), 
      DATETIME.$gte.getMonth(), DATETIME.$gte.getDate(), 
      DATETIME.$gte.getHours());
  }
  return Baking.aggregate([{
    $match: {
      OVENID: OVENID,
      DATETIME: boundDate 
  }}, {
    $unwind: '$MINUTES'
  }, {
    $unwind: '$MINUTES.SECONDS'
  },{
    $match: {
      'MINUTES.SECONDS.SENSOR0': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR1': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR2': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR3': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR4': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR5': { $lt: 327.67 }
  }}, {
    $project: {
      _id: 0,
      OVENID: 1,
      DATETIME: { $add: [ 
          '$DATETIME', 
          { $multiply: [ '$MINUTES.TIME', 60000 ] }, 
          { $multiply: [ '$MINUTES.SECONDS.TIME', 1000] } 
      ]},
      LOTNO: 1,
      RANGE: '$MINUTES.SECONDS.RANGE'
  }}, {
    $match: {
      DATETIME: DATETIME
  }}, {
    $group: {
      _id: { $dateToString: {
            format: '%Y-%m-%d %H:%M',
            date: '$DATETIME'
        }
      },
      OVENID: { $first: '$OVENID' },
      LOTNO: { $addToSet: '$LOTNO' },
      TOTAL: { $sum: 1 },
      MAXRANGE: { $max: '$RANGE' },
      MINRANGE: { $min: '$RANGE' },
      FAIL: { $sum: { 
          $cond: [ { $gte: [ '$RANGE', 6 ] }, 1, 0 ]
      }}
  }}, {
    $sort: {
      _id: 1
  }}
  ]).exec();
}

function searchBakByLotNo(match) {
  return Baking.aggregate([{
    $match: match
  }, {
    $unwind: '$MINUTES'
  }, {
    $unwind: '$MINUTES.SECONDS'
  }, {
    $match: {
      'MINUTES.SECONDS.SENSOR0': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR1': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR2': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR3': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR4': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR5': { $lt: 327.67 }
  }}, {
    $project: {
      _id: 0,
      OVENID: 1,
      DATETIME: { $add: [ 
          '$DATETIME', 
          { $multiply: [ '$MINUTES.TIME', 60000 ] }, 
          { $multiply: [ '$MINUTES.SECONDS.TIME', 1000] } 
      ]},
      LOTNO: 1,
      RANGE: '$MINUTES.SECONDS.RANGE'
  }}, {
    $group: {
      _id: { $dateToString: {
            format: '%Y-%m-%d %H:%M',
            date: '$DATETIME'
        }
      },
      OVENID: { $first: '$OVENID' },
      LOTNO: { $addToSet: '$LOTNO' },
      TOTAL: { $sum: 1 },
      MAXRANGE: { $max: '$RANGE' },
      MINRANGE: { $min: '$RANGE' },
      FAIL: { $sum: { 
          $cond: [ { $gte: [ '$RANGE', 6 ] }, 1, 0 ]
      }}
  }}, {
    $project: {
      _id: 0,
      DATETIME: '$_id',
      OVENID: 1,
      LOTNO: 1,
      TOTAL: 1,
      MAXRANGE: 1,
      MINRANGE: 1,
      FAIL: 1
  }}, {
    $sort: {
      DATETIME: 1
  }} 
  ]).exec();
}

function groupedLotNo(LOTNOS) {
  return Baking.aggregate([{
    $match: {
      LOTNO: { $in: LOTNOS }
  }}, {
    $group: {
      _id: '$LOTNO',
      OVENID: {
        $addToSet: '$OVENID'
      }
    }
  }, {
    $unwind: '$OVENID'
  }, {
    $project: {
      _id: 0,
      OVENID: 1,
      LOTNO: '$_id'
  }}]).exec();
}
// getLastSensorTemp('OI-05').then(function(results) {console.log(results);});
exports.getLastSensorTemp = getLastSensorTemp;

exports.test = function(req,res,next){
  // need ovenid
  /*req.body.OVENID = 'OD-69';
  req.body.DATETIME = {'$gte':new Date('2017-12-10T00:00:00.000Z'),'$lt':new Date('2017-12-17T11:59:59.000Z')}
  exports.postByTime(req,res,next);*/
  req.body.LOTNO = ['LHA05WDAD1','LHA05WDAD2'];
  exports.postByLotno(req,res,next);
};

var setOvenidAndQuery = function(query,doQuery,res){
  if(query.OVENID)
    doQuery();
  else{
    exports.getOvenIds(query,
    function(err,ovenids){
      if(err){
        console.log('[ERR]', err);
        return res.json([]);
      }
      else if(_.isEmpty(ovenids)){
        return res.json([]);
      }
      query.OVENID = ovenids;
      doQuery();
    });
  }
};

var setLotnoAndQuery = function(query,doQuery){
  if(query.LOTNO)
    doQuery();
  else{
    exports.getLotNos(query,
    function(err,lotnos){
      if(err){
        console.log('[ERR]', err);
        res.json([]);
      }
      query.LOTNO = lotnos;
      doQuery();
    });
  }
};

exports.postByCurrent = function(req, res, next) {
  // need ovenid only
  var query = modifyQuery(req.body);
  var doQuery = function(){
    Q.all(_.map(query.OVENID, function(OVENID) {
      return Baking.find({
          OVENID: OVENID 
        }, { _id: 0, MINUTES: 0, FILEPATH: 0 }
      ).sort({ DATETIME: -1, STARTTIME: -1}).limit(1).exec()
      .then(function(results) {
        return results.length ? Q.all([
            searchBakByOvenAndDate({$gte: results[0].STARTTIME}, OVENID),
            searchMesByLotNo(_.zipObject(results[0].LOTNO))
        ]) : [];
      });
    }))
    .then(function(results) {
      res.json(_.reduce(results, function(sum, result) {
        if (result.length && result[0].length) {
          _.forEach(result[0], function(bak) {
            bak.DATETIME = bak._id;
            delete bak._id;
            bak.LOTNO = _.uniq(_.flatten(bak.LOTNO));
          });
          sum.BAK = sum.BAK.concat(result[0]);
          _.merge(sum.MES, result[1]);
        }
        return sum;
      }, {
        BAK: [],
        MES: {}
      }));
    })
    .fail(function(error) {
      console.log('[ERR]', error);
      res.json([]);
    });
  }
  setOvenidAndQuery(query,doQuery,res);
};

exports.postByTime = function(req, res, next) {
  // need datetime & ovenid
  var query = modifyQuery(req.body);
  var doQuery = function(){
  //_.curry(searchBakByOvenAndDate)(query.DATETIME)
  Q.all(_.map(query.OVENID,function(ovenid){
    return searchBakByOvenAndDate(query.DATETIME,ovenid);
    })
  ).then(function(results) {
      var LOTNOS = {};
      results = _.map(_.flatten(results), function(result) {
        result.DATETIME = result._id;
        delete result._id;
        result.LOTNO = _.uniq(_.flatten(result.LOTNO));
        result.LOTNO.forEach(function(lotno) {
          LOTNOS[lotno] = undefined;
        });
        return result;
      });
      if(_.isEmpty(LOTNOS))
        return res.json([]);
      else
        return searchMesByLotNo(LOTNOS)
        .then(function(LOTNOS) {
          res.json({ MES: LOTNOS, BAK: results });
        });
    })
    .fail(function(error) {
      console.log('[ERR]', error);
      res.json([]);
    });
  }
  setOvenidAndQuery(query,doQuery,res);
};

exports.postByLotno = function(req, res, next) {
  // need lotnos only
  var query = modifyQuery(req.body);
  var doQuery = function(){
    groupedLotNo(query.LOTNO)
    .then(function(querys) {
      var LOTNOS = {};
      var bakPromises = _.map(querys, function(query) {
        query.LOTNO.forEach(function(lotno) {
          LOTNOS[lotno] = undefined;
        });
        return searchBakByLotNo(query)
      })
      if(_.isEmpty(LOTNOS))
        return [];
      else
        return Q.all(bakPromises.concat(searchMesByLotNo(LOTNOS)));
    })
    .then(function(results) {
      res.json({
          MES: _.last(results),
          BAK: _.initial(_.flatten(results))
      });
    })
    .then(null, function(error) {
      console.log('[ERR]', error);
      res.json([]);
    });
  }
  setLotnoAndQuery(query,doQuery);
};

function getIntervalCount(OVENID, DATETIME) {
  return Baking.find({
      OVENID: OVENID,
      DATETIME: {
        $gte: new Date(
          DATETIME.$gte.getFullYear(), 
          DATETIME.$gte.getMonth(), 
          DATETIME.$gte.getDate(), 
          DATETIME.$gte.getHours()),
        $lt: DATETIME.$lt
      }
  }, {MINUTES: 0}).count().exec();
}

function getSecondsDetail(OVENID, DATETIME) {
  if (+DATETIME.$lt === +DATETIME.$gte) {
    DATETIME.$lt = new Date(+DATETIME.$lt + 60000);
  }
  return Baking.aggregate([{
    $match: {
      OVENID: OVENID,
      DATETIME: new Date(DATETIME.$gte.getFullYear(), 
        DATETIME.$gte.getMonth(), DATETIME.$gte.getDate(), 
        DATETIME.$gte.getHours())
    }
  }, {
    $unwind: '$MINUTES'
  }, {
    $unwind: '$MINUTES.SECONDS'
  }, {
    $match: {
      'MINUTES.SECONDS.SENSOR0': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR1': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR2': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR3': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR4': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR5': { $lt: 327.67 }
  }}, {
    $project: {
      _id: 0,
      OVENID: 1,
      DATETIME: { $add: [ 
          '$DATETIME', 
          { $multiply: [ '$MINUTES.TIME', 60000 ] }, 
          { $multiply: [ '$MINUTES.SECONDS.TIME', 1000] } 
      ]},
      SENSOR0: '$MINUTES.SECONDS.SENSOR0',
      SENSOR1: '$MINUTES.SECONDS.SENSOR1',
      SENSOR2: '$MINUTES.SECONDS.SENSOR2',
      SENSOR3: '$MINUTES.SECONDS.SENSOR3',
      SENSOR4: '$MINUTES.SECONDS.SENSOR4',
      SENSOR5: '$MINUTES.SECONDS.SENSOR5',
      RANGE: '$MINUTES.SECONDS.RANGE'
  }}, {
    $match: {
      DATETIME: DATETIME
  }}, {
    $sort: {
      DATETIME: 1
  }} 
  ]).exec()
  .then(function(results) {
    return _.map(results, function(result) {
      result.DATETIME = result.DATETIME
          .toISOString().replace(/T/, ' ')
          .replace(/\..+/, '');
      result.SENSOR0 = _.round(result.SENSOR0, 2);
      result.SENSOR1 = _.round(result.SENSOR1, 2);
      result.SENSOR2 = _.round(result.SENSOR2, 2);
      result.SENSOR3 = _.round(result.SENSOR3, 2);
      result.SENSOR4 = _.round(result.SENSOR4, 2);
      result.SENSOR5 = _.round(result.SENSOR5, 2);
      result.RANGE = _.round(result.RANGE, 2);
      return result;
    });
  })
  .then(null, function(error) {
    console.log('[ERR]', error);
  });
}

function getDetail(OVENID, DATETIME, options) {
  var boundDate = {};
  if (DATETIME.$lt) boundDate.$lt = DATETIME.$lt;
  if (DATETIME.$gte) {
    boundDate.$gte = new Date(DATETIME.$gte.getFullYear(), 
      DATETIME.$gte.getMonth(), DATETIME.$gte.getDate(), 
      DATETIME.$gte.getHours());
  }
  return Baking.aggregate([{
    $match: {
      OVENID: OVENID,
      DATETIME: boundDate 
  }}, {
    $unwind: '$MINUTES'
  }, {
    $unwind: '$MINUTES.SECONDS'
  }, {
    $match: {
      'MINUTES.SECONDS.SENSOR0': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR1': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR2': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR3': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR4': { $lt: 327.67 },
      'MINUTES.SECONDS.SENSOR5': { $lt: 327.67 }
  }}, {
    $project: {
      _id: 0,
      DATETIME: { $add: [ 
          '$DATETIME', 
          { $multiply: [ '$MINUTES.TIME', 60000 ] }, 
          { $multiply: [ '$MINUTES.SECONDS.TIME', 1000] } 
      ]},
      SENSOR0: '$MINUTES.SECONDS.SENSOR0',
      SENSOR1: '$MINUTES.SECONDS.SENSOR1',
      SENSOR2: '$MINUTES.SECONDS.SENSOR2',
      SENSOR3: '$MINUTES.SECONDS.SENSOR3',
      SENSOR4: '$MINUTES.SECONDS.SENSOR4',
      SENSOR5: '$MINUTES.SECONDS.SENSOR5',
      RANGE: '$MINUTES.SECONDS.RANGE'
  }}, {
    $match: {
      DATETIME: DATETIME
  }}, {
    $group: {
      _id: { $dateToString: {
            format: options.groupType === groupType.MINUTE ? 
                    '%Y-%m-%d %H:%M' : '%Y-%m-%d %H',
            date: '$DATETIME'
        }
      },
      SENSOR0: { $avg: '$SENSOR0' },
      SENSOR1: { $avg: '$SENSOR1' },
      SENSOR2: { $avg: '$SENSOR2' },
      SENSOR3: { $avg: '$SENSOR3' },
      SENSOR4: { $avg: '$SENSOR4' },
      SENSOR5: { $avg: '$SENSOR5' },
      RANGE: { $max: '$RANGE' }
  }}, {
    $sort: { _id: 1 }} 
  ]).exec()
  .then(function(results) {
    return _.map(results, function(result) {
      result.DATETIME = result._id;
      result.OVENID = OVENID;
      result.SENSOR0 = _.round(result.SENSOR0, 2);
      result.SENSOR1 = _.round(result.SENSOR1, 2);
      result.SENSOR2 = _.round(result.SENSOR2, 2);
      result.SENSOR3 = _.round(result.SENSOR3, 2);
      result.SENSOR4 = _.round(result.SENSOR4, 2);
      result.SENSOR5 = _.round(result.SENSOR5, 2);
      result.RANGE = _.round(result.RANGE, 2);
      delete result._id;
      return result;
    });
  })
  .then(null, function(error) {
    console.log('[ERR]', error);
  });
}

exports.postByDetail = function(req, res, next) {
  var query = modifyQuery(req.body);
  getIntervalCount(_.first(query.OVENID), query.DATETIME)
  .then(function(count) {
    if (count > 2) {
      return getDetail(_.first(query.OVENID), 
        query.DATETIME, {
        groupType: count > 24 ? groupType.HOUR : groupType.MINUTE
      });
    } else {
      return getSecondsDetail(_.first(query.OVENID), query.DATETIME);
    }
  })
  .then(function(results) {
    res.json(results);
  })
  .then(null, function(error) {
    console.log('[ERR]', error);
    res.json([]);
  });
};

exports.outputDetail = function(req, res, next) {
};
