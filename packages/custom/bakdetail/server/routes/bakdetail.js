(function() {
  'use strict';
  var ctrl = require('../controllers/bakdetail.js');
  var bak = require('../controllers/bakdetail_p1.js');  
  var oracledb = require('oracledb');
  var _ = require('lodash');
  var dbConfig = {
    user: process.env.NODE_ORACLEDB_USER || "EDAAP",
    password: process.env.NODE_ORACLEDB_PASSWORD || "wpfbaxd3d",
    connectString: process.env.NODE_ORACLEDB_CONNECTIONSTRING || "10.5.139.117:1801/CHDMPD3",
    externalAuth: process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
  };

  module.exports = function(Bakdetail, app, auth, database, utils) {
    // I can't figure out why class constructor not work, use this function to replace it
    ctrl.initMesConnection(utils);
    app.post('/api/bakdetail/realtimeinfo', ctrl.burnInRealTimeInfo);
    app.post('/api/bakdetail/temp/getsearchfield', function(req, res, next){ // search field too often, do not save requestlog
      var to = new Date(req.body.exmatch.to),
          from = new Date(req.body.exmatch.from);
      req.body.exmatch = {$and:[
        // add GMT + 8
        {DATETIME:{$gte:new Date(from.setHours(from.getHours()+8))}}, 
        {DATETIME:{$lt:new Date(to.setHours(to.getHours()+8))}}
      ]};
      utils.ctrls.mongoQuery.getSearchField(ctrl.fieldSearch.coll, req.body, ctrl.fieldSearch.pathMapping, ctrl.fieldSearch.arrayFields)
      .then(function(data){
        if(req.body.field=='LOTNO'){
          var newData = [];
          data.forEach(function(item){
            item.forEach(function(lotno){
              newData.push(lotno);
            });
          });
          if(newData.length>0)
            data = newData;
        }
        data = _.orderBy(data);
        return res.json(data);
      })
      .catch((err) => res.status(500).send({'error': err}));
    });
    app.post('/api/bakdetail/mes/getsearchfield', ctrl.mesGetSearchField);
    app.post('/api/bakdetail/test', function(req,res,next){
      res.json('test');
    });
    // phase 1 bakdetail api
    bak.initMesConnection(utils);
    app.post('/api/baking/test', bak.test);
    app.post('/api/baking/current', bak.postByCurrent);
    app.post('/api/baking/time', bak.postByTime);
    app.post('/api/baking/lotno', bak.postByLotno);
    app.get('/api/baking/temp', bak.getTemp);
    app.get('/api/baking/agTemp', bak.getTempAggregate);
    app.get('/api/baking/menu', bak.getMenu);
    app.post('/api/baking/detail', bak.postByDetail);
    app.post('/api/baking/output', bak.outputDetail);
  };
})();
