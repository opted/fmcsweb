(function() {
    'use strict';
    function Bakdetail($http,Rebi,_) {
    var GroupByNameMapping = {'CUSTOMERCHISNAME':'客戶別','DEVICENO':'客戶型號','PACKAGETYPE':'封裝型號','CUSTOMERLOTNO':'客戶批號','LOTNO':'內批','OVENID':'爐號','DAY':'時間(日)','DATETIME':'時間(分)'};
    var basicGroupByList = _.keys(GroupByNameMapping);
    var bakService = Object.create(Rebi); // Bakdetail Service inheritance from Rebi Service
    
    // Override Following Function
    bakService.groupByMapping = function(){
      return GroupByNameMapping;
    };
    bakService.groupShowData = function(chartParams){
      var rawdata = chartParams.data.rawdata;
      var selected = chartParams.groupby.selected;
      // 1. group by selected
      var groupMap = _.groupBy(rawdata,selected);
      // 2. aggregate by selected
      var chartData = [];
      for(var key in groupMap){
        var groupData = groupMap[key];
        var sdata = {key:key};
        // 2.1 aggregate failrate, total, fail, maxrange, minrange
        sdata.total = _.sumBy(groupData,'TOTAL');
        sdata.fail = _.sumBy(groupData,'FAIL');
        sdata.maxrange = _.round(_.max(groupData,'MAXRANGE'),2);
        sdata.minrange = _.round(_.min(groupData,'MINRANGE'),2);
        sdata.failrate = _.round((sdata.fail/sdata.total)*100,2);
        chartData.push(sdata);
      }
      chartParams.allshowdata = chartData;
      chartParams.allshowdata = this.sortingAllShowData(chartParams);
      var limitselected = chartParams.limit.selected;
      var newChartData = [];
      // handle limit item part
      if(limitselected=='all' || limitselected>=chartParams.allshowdata.length)
        newChartData = chartParams.allshowdata;
      else
        for(var i=0;i<limitselected;i++)
          newChartData.push(chartParams.allshowdata[i]);
      return newChartData;
    };
    bakService.getChartParams = function(level,rawdata,groupbylist){
      // Iinitial status was defined as following
      var chartParams = {
       level:level,//drill down 等級，初始等級為0
       groupby:{itemlist:groupbylist,selected:groupbylist[0],active:0}, // 分群用項目
       sorting:{selected:'X軸字典排序',type:0},//可用的排序規則記在外部
       limit:{selected:'all'}, //圖表X軸分類顯示上限
       drillpath:[],
       data:{rawdata:rawdata},
       showdata:[],
       allshowdata:[],
       tmpgroupresult:{}, // 為增加效能，暫存不同 group by 結果的 showdata, sorting and limit
       ctype:undefined
      };
      chartParams.showdata = this.groupShowData(chartParams);
      chartParams.tmpgroupresult[chartParams.groupby.selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
      return chartParams;
    };
    
    bakService.initChartParams = function(rawdata){
      return this.getChartParams(0,rawdata,basicGroupByList);
    };
    
    bakService.getDrillDownParams = function(chartParams,category){
      var key = _.keys(category)[0];
      // 1. get new rawdata
      var rawdata = _.filter(chartParams.data.rawdata,function(o){
        if(o[key]==category[key])
          return true;
        else
          false;
      });
      // 2. get new groupbylist
      var groupbylist = _.filter(chartParams.groupby.itemlist,function(o){return o!=key})
      var ddChartParams = this.getChartParams(chartParams.level+1,rawdata,groupbylist);
      // 3. set drillpath
      ddChartParams.drillpath = _.cloneDeep(chartParams.drillpath);
      ddChartParams.drillpath.push(category);
      return ddChartParams;
    };
    bakService.changeGroup = function(chartParams,selected){
      chartParams.groupby.selected = selected;
      if(chartParams.tmpgroupresult[selected]){ //defined
        var tmpresult = chartParams.tmpgroupresult[selected];
        chartParams.showdata = tmpresult.showdata;
        chartParams.allshowdata = tmpresult.allshowdata;
        chartParams.sorting = tmpresult.sorting;
        chartParams.limit = tmpresult.limit;
      }
      else{ // undefined
        chartParams.showdata = this.groupShowData(chartParams);
        chartParams.sorting = _.cloneDeep(chartParams.sorting);
        chartParams.limit = _.cloneDeep(chartParams.limit);
        // 暫存 groupby 資料, 這部分資料在切換 groupby, limit, sorting 時要確認是否確實置換
        chartParams.tmpgroupresult[selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
      }
      return chartParams;
    };
    bakService.changeLimit = function(chartParams){
      var selected = chartParams.limit.selected;
      if(selected=='all' || selected>=chartParams.allshowdata.length)
        chartParams.showdata = chartParams.allshowdata;
      else{
        chartParams.showdata = [];
        for(var i=0;i<selected;i++)
          chartParams.showdata.push(chartParams.allshowdata[i]);
      }
      chartParams.tmpgroupresult[chartParams.groupby.selected].showdata = chartParams.showdata;
      return chartParams;
    };
    bakService.setSortinField = function(chartParams){
      if(chartParams.sorting.selected.indexOf('total')>=0)
        chartParams.sorting.field = 'total';
      else if(chartParams.sorting.selected.indexOf('failrate')>=0)
        chartParams.sorting.field = 'failrate';
      else
        chartParams.sorting.field = undefined;
    };
    bakService.sortingAllShowData = function(chartParams){
      this.setSortinField(chartParams);
      return Rebi.sortingAllShowData(chartParams);
    };
    bakService.changeSorting = function(chartParams){
      chartParams.allshowdata = this.sortingAllShowData(chartParams);
      chartParams.tmpgroupresult[chartParams.groupby.selected].allshowdata = chartParams.allshowdata;
      return this.changeLimit(chartParams);
    };
    bakService.chartChangeGroup = function(scope,chart,item){
      if(scope.drillDownTriggerChangeGroup){
        // drill down cilck will make new groupby tabset and trigger this function, 
        // do not trigger at this time.
        scope.drillDownTriggerChangeGroup = false;
        return;
      }
      scope.curChartParams = this.changeGroup(scope.curChartParams,item);
      chart.dataProvider = scope.curChartParams.showdata;
      chart.validateData();
      chart.validateNow();
    };
   bakService.chartChangeSorting = function(scope,chart,changeItem){
      this.setSortinField(scope.curChartParams);
      return Rebi.chartChangeSorting(scope,chart,changeItem);
    };
    bakService.chartDrillDown = function(scope,chart,graphItemCategory){
      if(scope.curChartParams.groupby.itemlist.length<=1){
        alert("Dose not has any grouping for drilling down.");
        return;
      }
      // get new chart params
      var category = {};
      category[scope.curChartParams.groupby.selected] = graphItemCategory;
      var newChartParams = this.getDrillDownParams(scope.curChartParams,category);
      Rebi.setDrillDownTab(scope,newChartParams);
      scope.$apply();
    };
    return bakService;
  }
  angular
      .module('mean.bakdetail')
      .factory('Bakdetail', Bakdetail);
  Bakdetail.$inject = ['$http','Rebi','_'];
})();
