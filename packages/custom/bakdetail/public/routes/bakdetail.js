(function() {
    'use strict';

    function Bakdetail($stateProvider) {
        $stateProvider.state('bakdetail page', {
            url: '/bakdetail?lotno&type',
            templateUrl: 'bakdetail/views/index.html',
            controller:BakdetailController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              },
              urlSearchParams: function($stateParams) {
                var searchOptions = {};
                searchOptions['type']=$stateParams['type'];
                if(searchOptions['type']=='time')
                  searchOptions['lotno']=$stateParams['lotno'];
                return searchOptions;
              }
            }
        });
    }

    angular
        .module('mean.bakdetail')
        .config(Bakdetail);

    Bakdetail.$inject = ['$stateProvider'];

})();
