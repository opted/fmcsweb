'use strict';
function BakdetailController($scope, $window, $http, common, utilsSrchOpt, urlSearchParams) {
  /*
    search panel
  */
  $scope.selected = {
    "from": new Date(common.getToday().getTime()-24*60*60*1000),  // Set default start day
    "to": common.getToday()
  };
  $scope.tempfields = [
    //utilsSrchOpt.genField("客戶(Siteno)", "SITENO", "string"),
    utilsSrchOpt.genField("爐號(Ovenid)", "OVENID", "string", true, true), // label name, query field name, type, ismain, support search
    utilsSrchOpt.genField("內批(Lotno)", "LOTNO", "string", true, true)
  ];
  $scope.tempfsURL = '/api/bakdetail/temp/getsearchfield';
  $scope.filterExtraMatch = $scope.selected;
  $scope.mesfields = [
    utilsSrchOpt.genField("客戶名稱(CustomerName)", "CUSTOMERCHISNAME", "string", true, true),
    utilsSrchOpt.genField("客戶型號(DeviceNo)", "DEVICENOLOG", "string", true, true),
    utilsSrchOpt.genField("封裝型號(PackageType)", "PACKAGETYPE", "string", true, true),
    utilsSrchOpt.genField("客批(CustomerLotno)", "CUSTOMERLOTNOLOG", "string", true, true),
    utilsSrchOpt.genField("爐號(OvenID)", "EQUIPMENTNO", "string", true, true),
    utilsSrchOpt.genField("內批(LotNo)", "LOTNO", "string", true, true)
  ];
  $scope.mesfsURL = '/api/bakdetail/mes/getsearchfield';
  $scope.filterMesExtraMatch = {datetime:$scope.selected};
  $scope.showRec = {tab1:true,tab2:false};
  $scope.changeTab = function(id){
    if(id=='rbitab'){
      $scope.showRec.tab1 = true;
      $scope.showRec.tab2 = false;
    }
    else if(id='baksystab'){
      $scope.showRec.tab1 = false;
      $scope.showRec.tab2 = true;
    }
  };
  /*
    Optional parameters
  */
  $scope.optinalParams = {status:{open:false},bitemp:0};
  $scope.biTempApply = function(){
    if($scope.optinalParams.bitemp==undefined){
      alert('未設定標準溫度');
      return;
    }
    else if(Number($scope.optinalParams.bitemp)==NaN){
      alert('請輸入數字');
      return;
    }
    else if($scope.optinalParams.bitemp<0){
      alert('溫度過低，請輸入大於0的數字');
      return;
    }
    else if($scope.optinalParams.bitemp>=327.67){
      alert('溫度過高，請輸入小於327.67的數字');
      return;
    }
    else if(_.isEmpty($scope.timeData)){
      alert('未載入任何爐溫資料');
      return;
    }
    applybakTimeData($scope.bakTimeData,$scope.optinalParams.bitemp);
  };
  /*
  ag-grid
  */
  var defaultColumnDefs = [
    {headerName: 'Time', field: 'time', filter:'text', width: 150},
    {headerName: 'Sensor0', field: 's0', filter: 'number', width: 85},
    {headerName: 'Sensor1', field: 's1', filter: 'number', width: 85},
    {headerName: 'Sensor2', field: 's2', filter: 'number', width: 85},
    {headerName: 'Sensor3', field: 's3', filter: 'number', width: 85},
    {headerName: 'Sensor4', field: 's4', filter: 'number', width: 85},
    {headerName: 'Sensor5', field: 's5', filter: 'number', width: 85},
    {headerName: 'Status', field: 'tempstat', width: 70, cellRenderer:function(params){
      if(params.value==undefined)
        return '';
      else
        return params.value;
    }}
  ];
  $scope.bakTimeGrid = {
    columnDefs:defaultColumnDefs,
    rowData:[],
    groupUseEntireRow: false,
    enableSorting: true,
    enableColResize: true
  };
  $scope.exportAgGrid = function(type){
    if ($scope.bakTimeGrid.api.getModel().getRowCount() < 1){
      $window.alert("無資料可供下載");
      return;
    }
    common.exportAgGrid('bakdetail_export',type,$scope.bakTimeGrid);
  };
  /*
   summary information
  */
  $scope.biTimeSummary = null;
  $scope.summaryInfo = {
    'rebitime':{waitingResponse:false,requestStatus:undefined,rowNum:undefined,dataStatus:undefined,queryTime:undefined},
    'baksys':{waitingResponse:false,requestStatus:undefined,rowNum:undefined,dataStatus:undefined,analysisType:undefined,queryTime:undefined}
  };
  
  var clearStatus = function(){
    $scope.biTimeSummary = undefined;
    $scope.summaryInfo.rebitime.rowNum = undefined;
    $scope.summaryInfo.rebitime.dataStatus = undefined;
    $scope.bakTimeGrid.api.setRowData([]);
    $scope.timeData = [];
  };
  /*
    amchart data
  */
  $scope.timeData = [];
  $scope.timeGuide = undefined;
  /*
    Request for Real BI Time (by increasing and reducing of temperature)
  */
  $scope.summaryInfo.rebitime.waitingResponse = false;
  $scope.getBiRealTime = function(urlquery){
    var startTime = new Date();
    var match = utilsSrchOpt.getMatch($scope.tempfields);
    if(urlquery){
      match.LOTNO=urlSearchParams['lotno'];
      $scope.tempfields[1].selected=match.LOTNO;
    }
    else if(urlquery==false) //not undefined
      return;
    else
      clearStatus();
    
    if(match.LOTNO==undefined){
      alert('請輸入內批(Lotno)');
      $scope.summaryInfo.rebitime.waitingResponse = false;
      return;
    }
    $scope.summaryInfo.rebitime.waitingResponse = true;
    $scope.summaryInfo.rebitime.requestStatus = '等待 Server 回應...';
    $scope.summaryInfo.rebitime.queryTime = "";
    // clear previous status
    
    $http.post('/api/bakdetail/realtimeinfo',match).then(
      function(response){
        // struture example:
        // {lotno:"ABCDE01",ovenid:"OC-56",
        //  data:[
        //  {time:datetime,s0:123.1,s1:123.s2,...,s5:122.5}, // per second
        //  ...],
        //  bitemp:126,
        //  standard:123}
        // 確保溫度有照順序排序後，尋找定溫點、降溫點、計算實際BI時間
        $scope.bakTimeData = response.data; // response.data = {'data':array,'standard':number,'customername':string}
        $scope.optinalParams.bitemp = $scope.bakTimeData['bitemp']
        // 1. sort
        applybakTimeData($scope.bakTimeData);
        $scope.summaryInfo.rebitime.waitingResponse = false;
        $scope.summaryInfo.rebitime.requestStatus = '查詢完成';
        if(response.data.status.length>0)
          $scope.summaryInfo.rebitime.dataStatus = ' , '+response.data.status;
        $scope.summaryInfo.rebitime.queryTime = new Date() - startTime;
      },function(err){
        $scope.summaryInfo.rebitime.requestStatus = '查詢失敗, '+err.data;
        $scope.summaryInfo.rebitime.waitingResponse = false;
        console.log(err);
      }
    );
  };
  var defaultTemp = 327;
  var applybakTimeData = function(bakTimeData,manualBiTemp){
    var fixedPoint = null;
    var collingPoint = null;
    var realBITime = 0;
    var fixedTemp_idx = 0;
    var collingTemp_idx = 0;
    
    if(manualBiTemp){
      bakTimeData['bitemp']=manualBiTemp;
      bakTimeData['standard']=manualBiTemp-3;
    }
    if(bakTimeData['standard']){
      // 2. find constant time
      for(var i=collingTemp_idx;i<bakTimeData['data'].length;i++){
        var bdata = bakTimeData['data'][i];
        var allgt = true;
        for(var key in bdata){
          if(key.startsWith('s')){
            var sensorTemp = bdata[key];
            if(sensorTemp>defaultTemp){ // temperature is far gerater than standard value, skip this record
              allgt = false;
              break;
            }
            else if(sensorTemp<bakTimeData['standard']){
              allgt=false;
              break;
            }
          }
        }
        if(allgt){// 6 sensors all greater than standard temperature, find the value
          bdata['tempstat'] = '定溫';
          fixedPoint = bdata;
          fixedTemp_idx=i;
          break;
        }
      }
      // 3.  find cooling time
      var hightemp=0, lowtemp=defaultTemp; // highest and lowest temp at constant time
      for(var i=fixedTemp_idx+1;i<bakTimeData['data'].length;i++){
        var bdata = bakTimeData['data'][i];
        var allLt = true;
        for(var key in bdata){
          if(key.startsWith('s')){
            var sensorTemp = bdata[key];
            if(sensorTemp>defaultTemp){ // temperature is far gerater than standard value, skip this record
              allLt = false;
              break;
            }
            else if(sensorTemp>bakTimeData['standard']){
              allLt=false;
              break;
            }
          }
        }
        for(var key in bdata){
          if(allLt)
            break;
          if(key.startsWith('s')){
            var sensorTemp = bdata[key];
            hightemp = Math.max(hightemp,sensorTemp);
            lowtemp = Math.min(lowtemp,sensorTemp); // 實際上是要找定溫時最低溫度，而非定溫後
          }
        }
        if(allLt){// 6 sensors all less than standard temperature,find the value
          bdata['tempstat'] = '降溫'; // 1: fixed, 2: colling
          collingPoint = bdata;
          collingTemp_idx = i;
          break;
        }
      }
    }// if standard temperature is defined
    // 4. cal real bi time
    if(fixedPoint!=null && collingPoint!=null && bakTimeData.standard!=undefined)
      realBITime = (new Date(collingPoint.time).getTime()-new Date(fixedPoint.time).getTime())/1000;
    else
      realBITime = undefined;
    // 5. set bi time summary info
    if(lowtemp==defaultTemp){
      lowtemp=undefined;
      hightemp=undefined;
    }
    $scope.biTimeSummary = {ovenid:bakTimeData.ovenid,lotno:bakTimeData.lotno,
      ftemp:fixedPoint?fixedPoint.time:undefined,ctemp:collingPoint?collingPoint.time:undefined,
      bitime:realBITime?common.secondsReadable(realBITime):undefined,
      bitemp:bakTimeData.bitemp?bakTimeData.bitemp:'未定義',
      standard:bakTimeData.standard?bakTimeData.standard:'未定義',
      customername:bakTimeData['customername'],f_htemp:hightemp,f_ltemp:lowtemp};
    // 6. set data to amChart
    $scope.timeData = bakTimeData['data'];
    $scope.timeGuide = {
      date: $scope.biTimeSummary.ftemp,
      toDate: $scope.biTimeSummary.ctemp,
      biRealTime: $scope.biTimeSummary.bitime};
    $scope.summaryInfo.rebitime.rowNum = bakTimeData['data'].length;
    
    // 7. set data to ag-grid
    $scope.bakTimeGrid.api.setRowData(bakTimeData['data']);
  };
  // query by loton behind url
  if(urlSearchParams.lotno)
    $scope.getBiRealTime(true);
  else
    $scope.getBiRealTime(false);
  /**
    Baking System Analysis
  */  
  /*
   Direcitve data
  */
  $scope.bakSysData = undefined;
  var aysisTypeMap = {'current':'現況','time':'歷史','lotno':'批號'};
  var bakAnalysisError = function(err){
    $scope.summaryInfo.baksys.requestStatus = 'Failed, '+err.data;
    $scope.summaryInfo.baksys.waitingResponse = false;
    console.log(err);
  };
  /*
    Query function
  */
  $scope.getBakAnalysis = function(){
    var startTime = new Date();
    var match = utilsSrchOpt.getMatch($scope.mesfields);
    match.DATETIME = {$gte:$scope.selected.from,$lt:$scope.selected.to};
    $scope.summaryInfo.baksys.waitingResponse = true;
    $scope.summaryInfo.baksys.requestStatus = 'Loading...';
    $scope.summaryInfo.baksys.rowNum = undefined;
    $scope.bakSysData = undefined;
    var queryurl = '/api/baking/'+$scope.bakSysTabType;
    $scope.summaryInfo.baksys.analysisType = aysisTypeMap[$scope.bakSysTabType];
    $scope.summaryInfo.baksys.queryTime = "";
    // proccess match data
    $http.post(queryurl,match).then(function(response){
        var rawdata = response.data;
        if(rawdata.BAK==undefined){
          $scope.summaryInfo.baksys.rowNum = 0;
          $scope.summaryInfo.baksys.waitingResponse = false;
          $scope.summaryInfo.baksys.requestStatus = 'Data not found';
          return;
        }
        for(var key in rawdata.MES)
          delete rawdata.MES[key].LOTNO;
        // flatten lotno and combine bak and mes data
          rawdata.BAK.forEach(function(o){
            // set HOUR and DAY
            o.HOUR = o.DATETIME.slice(0,13);
            o.DAY = o.DATETIME.slice(0,10);
            o.LOTNO = o.LOTNO[0];
            if(typeof(o.LOTNO)==typeof([]))
              o.LOTNO = o.LOTNO[0];
            try{
              var lotnos = o.LOTNO.split(',');
              lotnos.forEach(function(lotno){
                if(rawdata.MES[o.LOTNO])
                  _.assign(o,rawdata.MES[o.LOTNO]);
              });
            }catch(err){
              console.log(err,o.LOTNO);
            }
          });
        $scope.bakSysData = rawdata.BAK;
        //console.log($scope.bakSysData);
        $scope.summaryInfo.baksys.rowNum = $scope.bakSysData.length
        $scope.summaryInfo.baksys.waitingResponse = false;
        $scope.summaryInfo.baksys.requestStatus = 'Success';
        $scope.summaryInfo.baksys.queryTime = new Date() - startTime;
    }).catch(bakAnalysisError);
  };
  /*
   Other functions
  */
  $scope.bakSysTabType = 'current';
  $scope.changeBakSysTab = function(tabType){
    $scope.bakSysTabType = tabType;
  };
}

angular
  .module('mean.bakdetail')
  .controller('BakdetailController', BakdetailController);

BakdetailController.$inject = ['$scope', '$window', '$http','common','utilsSrchOpt','urlSearchParams'];
