'use strict';

angular.module('mean.bakdetail')
.directive('bakdetail', function ($compile, $http, $location, $anchorScroll) {
  function aggFun(rows) {
    var keys = ['OVENID', 'LOTNO', 'DEVICENO', 
      'PACKAGETYPE', 'CUSTOMERLOTNO', 'SITENO'];
    var agg = _.zipObject(_.unzip([keys, _.fill(new Array(keys.length), '')]));
    agg.FAIL = 0;
    agg.TOTAL = 0;
    agg.MAXRANGE = 0;
    agg.MINRANGE = Infinity;
    agg.DATETIME = [Infinity, -Infinity];
    agg = _.reduce(rows, function(agg, row) {
      _.forEach(keys, function(key) {
        if (agg[key].length <= 40 && !agg[key].match(row.data[key])) {
          agg[key] += row.data[key] + ',';
        }
      });
      agg.DATETIME = cmpDate(agg.DATETIME, row.data.DATETIME, row.group);
      if (!row.group) {
        this.ovenLists[row.data.OVENID] = cmpDate(
          this.ovenLists[row.data.OVENID] || [Infinity, -Infinity], 
          row.data.DATETIME,
          row.group);
      }
      agg.FAIL += row.data.FAIL;
      agg.TOTAL += row.data.TOTAL;
      agg.MAXRANGE = agg.MAXRANGE > row.data.MAXRANGE ? agg.MAXRANGE : row.data.MAXRANGE;
      agg.MINRANGE = agg.MINRANGE < row.data.MINRANGE ? agg.MINRANGE : row.data.MINRANGE;
      return agg;
    }, agg, this);
    keys.forEach(function(key) {
      agg[key] = _.trimRight(agg[key], ',');
    });
    agg.DATETIME = agg.DATETIME.join(' - ');
    return agg;
  }
  function cmpDate(srcDate, destDate, group) {
    if (group) {
      destDate = destDate.split(' - ');
      return [ 
        srcDate[0] < destDate[0] ? srcDate[0] : destDate[0], 
        srcDate[1] > destDate[1] ? srcDate[1] : destDate[1]
      ];
    } else {
      return [
        srcDate[0] < destDate ? srcDate[0] : destDate,
        srcDate[1] > destDate ? srcDate[1] : destDate 
      ];
    }
  }
  return {
    require: '^bakdetailCtrl',
    restrict: 'E',
    templateUrl: '/bakdetail/views/bakdetail.html',
    controller: function($scope, $element, $window) {
      $scope.gridOptions = {
        columnDefs: [
          {field: 'OVENID', headerName: '爐號(Oven Id)', width: 120},
          {field: 'DATETIME', headerName: '時間(分)', width: 120},
          {field: 'FAILRATE', headerName: '錯誤率(%)', width: 120,
            valueGetter: function(params) {
              return _.round(params.data.FAIL/params.data.TOTAL*100, 2);
          }},
          {field: 'FAIL', headerName: '錯誤數', width: 120},
          {field: 'TOTAL', headerName: '總數量', width: 120},
          {field: 'MAXRANGE', headerName: '最大Range', width: 120, 
            valueGetter: function(params) {
              return _.round(params.data.MAXRANGE, 2);
          }},
          {field: 'MINRANGE', headerName: '最小Range', width: 120,
            valueGetter: function(params) {
              return _.round(params.data.MINRANGE, 2);
          }},
          {field: 'LOTNO', headerName: '內批', width: 120},
          {field: 'DEVICENO', headerName: '客戶型號', width: 120},
          {field: 'PACKAGETYPE', headerName: '封裝型別', width: 120},
          {field: 'CUSTOMERLOTNO', headerName: '客戶批號', width: 120},
          {field: 'SITENO', headerName: '客戶別', width: 120 },
          {field: 'HOUR', headerName: '時間(時)', width: 120, hide: true },
          {field: 'DAY', headerName: '時間(日)', width: 120, hide: true }
        ],
        rowData: null,
        enableColResize: true,
        enableSorting: true,
        enableFilter: true,
        headerHeight: 30,
        toolPanelSuppressValues: true,
        toolPanelSuppressPivot: true,
        showToolPanel: true,
        rowSelection: 'multiple',
        selectionChanged: function() {
          if($scope.childApi) {
            if ($scope.gridOptions.selectedRows.length) {
              var group = _.last($scope.currentGroups);
              var keySet = {};
              _.forEach($scope.gridOptions.selectedRows, function(row) {
                if (_.keys(keySet).length <= 10) {
                  keySet[row[group.key]] = true;
                }
              });
              $scope.api.setTranslate(_.trunc(_.keys(keySet).join(','), 30));
              group.heading = _.union(_.map($scope.gridOptions.selectedRows, group.key));
              $scope.childApi.draw($scope.gridOptions.selectedRows);
              $scope.childApi.addChild();
            } else {
              $scope.api.setTranslate('');
              $scope.api.addSlide();
              $scope.childApi.hideChild();
            }
          }
        },
        groupSelectsChildren: true,
        groupAggFunction: aggFun.bind($scope),
        suppressRowClickSelection: true,
        groupColumnDef: {
          headerName: 'Group',
          field: 'GROUP',
          width: 200,
          cellRenderer: {
              renderer: 'group',
              checkbox: true
          }
        }
      };

      $scope.chartData = [{
        key: '錯誤率', bar: true, values: []
      }, {
        key: '測試 Slot 數量', values: []
      }];

      function lineBarChartClick(result) {
        var groupKey = result.data ? result.data.group : result.point.group;
        _.takeWhile($scope.gridOptions.api.getModel().getTopLevelNodes(),
          function(node, index) {
            if (node.key === groupKey) {
              $scope.gridOptions.api.selectNode(node);
              $scope.gridOptions.api.setFocusedCell(index, 0);
              return false;
            } 
            return true;
        });
        $scope.goHash($scope.prevGroups.length+1);
      }

      function fixedClipPath() {
        $element.find('[clip-path]').attr('clip-path', function(idx, attr) {
          return attr.replace('(#', '('+$location.path()+'#');
        });
      }
      $scope.nvOptions = {
        chart: {
          type: 'linePlusBarChart',
          height: 350,
          width: $window.innerWidth-135,
          margin : {
            top: 20, right: 75,
            bottom: 60, left: 75
          },
          useInteractiveGuideline: true,
          clipEdge: true,
          bars: {
            forceY: [0],
            clipEdge: true,
            dispatch: {
              elementClick: lineBarChartClick,
//               elementMouseover: fixedClipPath
            }
          },
          lines: {
            forceY: [0],
            clipEdge: true,
            dispatch: {
              elementClick: lineBarChartClick,
//               elementMouseover: fixedClipPath
            }
          },
          color: ['#aec7e8', 'darkred'],
          x: function(d, i){ return i; },
          y: function(d){ return d.value; },
          xAxis: {
            axisLabel: 'Group By',
            staggerLabels: true,
            tickFormat: function(d) {
              return _.get($scope.chartData, [1, 'values', d, 'group']);
            }
          },
          y1Axis: {
            axisLabel: '錯誤率(%)',
            axisLabelDistance: -20,
            tickFormat: function(d) { return d3.format('%')(d); }
          },
          y2Axis: {
            axisLabel: '總數量',
            axisLabelDistance: 20,
            tickFormat: function(d) { return d3.format('f')(d); },
          },
          transitionDuration: 500,
          focusEnable: false,
          tooltip: {
            contentGenerator: function(d) {
              var data = (_.get(d, 'data')) ? d.data : d.point;
              return '<h3>'+data.group+'</h3><table>'+
                '<tr><td>爐號</td><td>'+_.trunc(data.OVENID, 30)+'</td></tr>'+
                '<tr><td>時間(分)</td><td>'+_.trunc(data.DATETIME, 30)+'</td></tr>'+
                '<tr><td>錯誤率</td><td>'+_.round(data.FAIL/data.TOTAL, 2)+'</td></tr>'+
                '<tr><td>錯誤數</td><td>'+data.FAIL+'</td></tr>'+
                '<tr><td>總數量</td><td>'+data.TOTAL+'</td></tr>'+
                '<tr><td>最大Range</td><td>'+_.round(data.MAXRANGE, 2)+'</td></tr>'+
                '<tr><td>最小Range</td><td>'+_.round(data.MINRANGE, 2)+'</td></tr>'+
                '<tr><td>內批</td><td>'+_.trunc(data.LOTNO, 30)+'</td></tr>'+
                '<tr><td>客戶型號</td><td>'+_.trunc(data.DEVICENO, 30)+'</td></tr>'+
                '<tr><td>封裝型別</td><td>'+_.trunc(data.PACKAGETYPE, 30)+'</td></tr>'+
                '<tr><td>客戶批號</td><td>'+_.trunc(data.CUSTOMERLOTNO, 30)+'</td></tr>'+
                '<tr><td>客戶別</td><td>'+_.trunc(data.SITENO, 30)+'</td></tr>'+
                '</table>';
            }
          },
        }
      };

      $scope.nvDetailOptions = {
        chart: {
          type: 'multiChart',
          height: 350,
          width: $window.innerWidth-135,
          margin : {
            top: 20, right: 75,
            bottom: 60, left: 75
          },
          color: ['#1f77b4', '#aec7e8', '#ff7f0e', '#ffbb78', '#2ca02c',  '#ff9896', '#9467bd'],
          xAxis: {
            axisLabel: 'Time',
            staggerLabels: true,
            tickFormat: function(x) {
              return _.get($scope.detailDateList, x, '');
            }
          },
          y1Axis: {
            axisLabel: '溫度',
            axisLabelDistance: -20,
            tickFormat: function(y) {
              return d3.format('.02f')(y);
            }
          },
          y2Axis: {
            axisLabel: 'Max Range',
            axisLabelDistance: 20,
            tickFormat: function(d) {
              return d3.format('.02f')(d);
            },
          },
          clipVoronoi: true,
          tooltip: {
            contentGenerator: function(d) {
              var index = (_.get(d, 'data')) ? d.data.x : d.point.x;
              var data = _.map($scope.detailData, ['values', index, 'y']);
              return '<h3>'+$scope.detailDateList[index]+'</h3><table>'+
                '<tr><td>Sensor0</td><td>'+data[0]+'</td></tr>'+
                '<tr><td>Sensor1</td><td>'+data[1]+'</td></tr>'+
                '<tr><td>Sensor2</td><td>'+data[2]+'</td></tr>'+
                '<tr><td>Sensor3</td><td>'+data[3]+'</td></tr>'+
                '<tr><td>Sensor4</td><td>'+data[4]+'</td></tr>'+
                '<tr><td>Sensor5</td><td>'+data[5]+'</td></tr>'+
                '<tr><td>Max Range</td><td style="color: '+(data[6]>=6 ? 'red' : 'black')+'">'+data[6]+'</td></tr>'+
                '</table>';
            }
          }
        }
      };

      $scope.group = _.map([
          { key: 'SITENO', heading: '客戶別' }, 
          { key: 'DEVICENO', heading: '客戶型號' }, 
          { key: 'PACKAGETYPE', heading: '封裝型別' }, 
          { key: 'CUSTOMERLOTNO', heading: '客戶批號' }, 
          { key: 'LOTNO', heading: '內批' }, 
          { key: 'OVENID', heading: '爐號' }, 
//           { key: 'SENSOR', heading: 'Sensor' },
          { key: 'DATETIME', heading: '時間(分)' },
          { key: 'HOUR', heading: '時間(時)' },
          { key: 'DAY', heading: '時間(日)' },
        ], 
        function(obj) {
          obj.active = false;
          obj.disable = false;
          return obj;
      });

      $scope.show = true;
      $scope.groupChange = function(key) {
        $scope.currentGroups = $scope.prevGroups.concat({key: key});
        if (angular.isArray($scope.data) && $scope.data.length) {
          if ($scope.childApi) {
            $scope.childApi.setShow($scope.currentGroups);
            $scope.childApi.hideChild();
          }
          $scope.api.draw($scope.data);
        }
      };
      // order selection
      $scope.orders = [
          { value: 'TOTAL', name: '總數量' },
          { value: 'FAILRATE', name: '錯誤率' },
          { value: 'XAXIS', name: 'X軸' }
        ];
      $scope.order = {
        selected: $scope.orders[0]
      };
      $scope.orderChange = function(selected) {
        $scope.order.selected = selected
        $scope.groupChange(_.last($scope.currentGroups).key);
      };
      // draw limit selection
      $scope.drawLimits = [
        { name: '5', value: 5 },
        { name: '25', value: 25 },
        { name: '50', value: 50 },
        { name: 'ALL', value: null}
      ];
      $scope.drawLimit = {
        selected: $scope.drawLimits[1]
      };
      $scope.drawLimitChange = function(selected) {
        $scope.drawLimit.selected = selected;
        $scope.groupChange(_.last($scope.currentGroups).key);
      };

      // detail
      $scope.ovenDetailList = ['Grouped'];
      $scope.ovenDetail = {
        selected: $scope.ovenDetailList[0]
      };

      // api
      $scope.api = {
        draw: function(data) {
          this.setTranslate('');
          $scope.data = data || $scope.data;
          $scope.show = true;
          this.refreshGrid();
          this.refreshLineBarChart();
          $scope.api.addSlide();
          if ($scope.childApi) $scope.childApi.setShow($scope.currentGroups);
        },
        refreshGrid: function() {
          var pivots = _.reduce($scope.currentGroups, function(prev, obj, index) {
            prev[obj.key] = $scope.currentGroups.length - index;
            return prev;
          }, {});
          $scope.gridOptions.columnApi.setColumnState(_.map(
              $scope.gridOptions.columnApi.getColumnState(), function(state) {
                state.pivotIndex = _.get(pivots, state.colId, null);
                return state;
          }));
          $scope.ovenLists = {};
          $scope.gridOptions.api.setRowData($scope.data);
          $scope.ovenDetailList = ['Grouped'].concat(Object.keys($scope.ovenLists));
          $scope.ovenDetail.selected = $scope.ovenDetailList[0];
        },
        refreshLineBarChart: function() {
          var vdata = _.map($scope.gridOptions.api.getModel().getTopLevelNodes(),
            function(node) {
              return _.merge({
                  group: node.key,
                  value: node.data.FAIL/node.data.TOTAL
              }, node.data);
          });
          var drawLimit = _.get($scope, ['drawLimit', 'selected', 'value']);
          var orderKey = _.get($scope, ['order', 'selected', 'value'], 'TOTAL');
          if (orderKey === 'FAILRATE') {
            vdata = _.sortBy(vdata, 'value').reverse();
            if (angular.isNumber(drawLimit)) vdata = vdata.slice(0, drawLimit);
            // set nvOptions
            $scope.nvOptions.chart.y1Axis.axisLabel = '總數量';
            $scope.nvOptions.chart.y1Axis.tickFormat = function(d) { return d3.format('f')(d); };
            $scope.nvOptions.chart.y2Axis.axisLabel = '錯誤率(%)';
            $scope.nvOptions.chart.y2Axis.tickFormat = function(d) { return d3.format('%')(d); };
            $scope.nvOptions.chart.color = ['darkred', '#aec7e8'];
            $scope.chartData = [{
              key: '錯誤率', bar: false, values: vdata
            }, {
              key: '總數量', bar: true,
              values: _.map(vdata, function(obj) {
                return _.set(_.clone(obj), 'value', obj.TOTAL);
              })
            }];
          } else {
            if (orderKey === 'TOTAL') vdata = _.sortBy(vdata, 'TOTAL').reverse();
            else vdata = _.sortBy(vdata, _.last($scope.currentGroups).key);
            if (angular.isNumber(drawLimit)) vdata = vdata.slice(0, drawLimit);
            $scope.nvOptions.chart.y1Axis.axisLabel = '錯誤率(%)';
            $scope.nvOptions.chart.y1Axis.tickFormat = function(d) { return d3.format('%')(d); };
            $scope.nvOptions.chart.y2Axis.axisLabel = '總數量';
            $scope.nvOptions.chart.y2Axis.tickFormat = function(d) { return d3.format('f')(d); };
            $scope.nvOptions.chart.color = ['#aec7e8', 'darkred'];
            $scope.chartData = [{
              key: '錯誤率', bar: true, values: vdata
            }, {
              key: '總數量', bar: false,
              values: _.map(vdata, function(obj) {
                return _.set(_.clone(obj), 'value', obj.TOTAL);
              })
            }];
          }
          $scope.nvOptions.chart.xAxis.ticks = vdata.length;
        },
        getRawData: function() {
          return $scope.ovenDetail.selected !== 'Grouped' ? 
                $scope.detailRawData : $scope.data;
        },
        getHeaderNames: function() {
          return $scope.ovenDetail.selected !== 'Grouped' ?
            {} : _.reduce($scope.gridOptions.columnDefs, 
              function(prev, colDef) {
                if (!colDef.valueGetter) prev[colDef.field] = (colDef.headerGroup ? colDef.headerGroup + '/' : '') + colDef.headerName;
                return prev;
            }, {});
        },
        initGroup: function(prevGroups) {
          $scope.prevGroups = prevGroups;
          $scope.group = _.sortBy($scope.group, function (obj) {
            var index = _.indexOf(_.map(prevGroups, 'key'), obj.key);
            if (index === -1) {
              obj.disable = false;
              obj.active = false;
              return prevGroups.length;
            } else {
              obj.disable = true;
              obj.active = false;
              return index;
            }
          });
          $scope.group[prevGroups.length].active = true;
          $scope.currentGroups = prevGroups.concat({key: _.find($scope.group, {active: true}).key});
        },
        getCurrentGroups: function() {
          return $scope.currentGroups;
        },
        setHide: function() {
          $scope.show = false;
          $scope.data = [];
          if ($scope.childApi) $scope.childApi.setHide();
        },
        setShow: function(prevGroups) {
          $scope.show = true;
          this.initGroup(prevGroups);
        },
        hideChild: function() {
          if ($scope.childApi) $scope.childApi.setHide();
        }
      };
      function handle() {
        $scope.nvOptions.chart.width = $window.innerWidth-135;
        $scope.nvDetailOptions.chart.width = $window.innerWidth-135;
        $scope.$digest();
        $scope.api.refreshLineBarChart();
      }
      angular.element($window).bind('resize', handle);
      $scope.$on('$destroy', function() {
        angular.element($window).unbind('resize', handle);
      });
    },
    scope: {
      prevGroups: '=?',
      api: '=',
      onePlot: '='
    },
    link: function(scope, elem, attrs, bakdetailCtrl) {
      bakdetailCtrl.newElement(scope, elem);
      if (angular.isUndefined(scope.prevGroups)) scope.prevGroups = [];
      scope.api.initGroup(scope.prevGroups);
      scope.api.addChild = function() {
        if (angular.isUndefined(scope.child) && scope.prevGroups.length < scope.group.length-1) {
          scope.child = angular.element('<bakdetail data="selectedData" prev-groups="currentGroups" api="childApi" one-plot="onePlot"></bakdetail>');
          angular.element(document.getElementById('bakmain')).append(scope.child);
          $compile(scope.child)(scope);
        }
      };
      scope.api.setTranslate = function(value) {
        bakdetailCtrl.setTranslate(scope.prevGroups.length, value);
      };
      scope.api.addSlide = function() {
        bakdetailCtrl.addSlide(scope.prevGroups.length);
      };
      scope.api.removeSlide = function() {
        bakdetailCtrl.removeSlide(scope.prevGroups.length);
      };
      scope.goHash = function(index) {
        if (scope.onePlot) return bakdetailCtrl.selectSlide(index);
        var newHash = 'bak'+index;
        if ($location.hash() !== newHash) {
          $location.hash(newHash);
        } else {
          $anchorScroll();
        }
      };
      scope.$watch('ovenDetail.selected', function(newVal, oldVal) {
        if (newVal !== 'Grouped') {
          if (scope.detailData && scope.detailData.length) scope.detailData = [];
          $http.post('/api/baking/detail', {
              OVENID: newVal,
              DATETIME: {
                $gte: scope.ovenLists[newVal][0],
                $lt: scope.ovenLists[newVal][1]
              }
          })
          .then(function(results) {
            scope.detailDateList = [];
            var detailData = [
              { key: 'Sensor0', values: [], yAxis: 1, type: 'line' }, 
              { key: 'Sensor1', values: [], yAxis: 1, type: 'line' }, 
              { key: 'Sensor2', values: [], yAxis: 1, type: 'line' }, 
              { key: 'Sensor3', values: [], yAxis: 1, type: 'line' }, 
              { key: 'Sensor4', values: [], yAxis: 1, type: 'line' }, 
              { key: 'Sensor5', values: [], yAxis: 1, type: 'line' },
              { key: 'Range', values: [], yAxis: 2, type: 'bar' }];
            _.forEach(results.data, function(result, index) {
              scope.detailDateList.push(result.DATETIME);
              detailData[0].values[index] = { x: index, y: result.SENSOR0};
              detailData[1].values[index] = { x: index, y: result.SENSOR1};
              detailData[2].values[index] = { x: index, y: result.SENSOR2};
              detailData[3].values[index] = { x: index, y: result.SENSOR3};
              detailData[4].values[index] = { x: index, y: result.SENSOR4};
              detailData[5].values[index] = { x: index, y: result.SENSOR5};
              detailData[6].values[index] = { x: index, y: result.RANGE};
            });
            scope.detailData = detailData;
            scope.detailRawData = results.data;
          });
        }
      });
    }
  };
});
