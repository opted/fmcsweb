
(function(){
	'use strict';

	function bakSystemChart(common,Bakdetail,_){
    var chartName = "baksys-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=' // Data
			},
			template:
      '<div ng-show="display"><uib-tabset active="drillDownTabActive">\
        <uib-tab ng-repeat="tab in drillTablist" ng-click="changeDrillDownTab($index)">\
        <uib-tab-heading>\
          {{tab.title}}<button type="button" class="close" aria-label="Close" style="font-size:medium" ng-click="closeTab($index)" ng-show="$index>0">\
          <span aria-hidden="true">&times;</span>\
          </button>\
        </uib-tab-heading>\
        </uib-tab>\
      </uib-tabset>'+
      // grouping button
      // render all pill tabset and hide unneeded tabset to fix a bug
      '<div ng-repeat="tab in drillTablist"><uib-tabset class="rebi-groupby" type="pills" ng-show="selectedTabItemIndex==$index">\
      <uib-tab index="$index" ng-repeat="item in tab.chartparams.groupby.itemlist" heading="{{GroupByMapping[item]}}" select="changeGroup(item,tab.chartparams.level)">\
      </uib-tab>\
      </uib-tabset></div>'+
      // sorting select
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">排序</label>\
        <select vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglist" ng-change="changeSorting(0)">\
        </select>\
        <div class="btn-group vcenter">\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="0" ng-click="changeSorting(1)">降序</label>\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="1" ng-click="changeSorting(2)">升序</label>\
        </div>\
      </div>'+
      // amchart
      '<div id="'+chartName+'" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      // ag-grid
      '<div style="height: 300px;" class="ag-blue" ag-grid="baksysGrid"></div>\
      <button class="btn btn-success" ng-click="exportAgGrid(\'xls\')">下載Excel</button>',
			link: function (scope, element, attrs) {
        scope.display = true;
        // chart params contains all variables about the group of chart
        scope.curChartParams = undefined;
        scope.GroupByMapping = Bakdetail.groupByMapping();
        scope.changeGroup = function(item,level){
          Bakdetail.chartChangeGroup(scope,chart,item);
          // change ag-grid group
          if(scope.curChartParams.level==0 && level==0){
            var selected = scope.curChartParams.groupby.selected
            scope.gridColumnDefs[0].headerName = 'Group('+scope.GroupByMapping[item]+')';
            scope.gridColumnDefs.forEach(function(data){
              if(data.field==item){
                data.rowGroupIndex=0;
              }
              else
                delete data.rowGroupIndex;
            });
            scope.baksysGrid.api.setColumnDefs(scope.gridColumnDefs);
          }
        };
        // sorting selected
        scope.sortinglist = ['X軸字典排序','total 數值排序','failrate 數值排序'];
        scope.changeSorting = function(changeItem){
          Bakdetail.chartChangeSorting(scope,chart,changeItem);
        };
        /**
          Drill down control
        */
        scope.drillDownTriggerChangeGroup=false;
        scope.drillDownTabActive = 0;
        scope.drillTablist = [];
        scope.drillDown = function(graphItemCategory){
          Bakdetail.chartDrillDown(scope,chart,graphItemCategory);
        };
        scope.triggerCloseTab = false;
        scope.selectedTabItemIndex = scope.drillDownTabActive;
        scope.changeDrillDownTab = function(index){
          Bakdetail.chartChangeDrillDownTab(scope,chart,index);
        };
        scope.closeTab = function(tabindex){
          Bakdetail.chartCloseTab(scope,tabindex);
        };
        /**
          Defined amchart options
        */
        var ballonFunction = common.amChartCustomizeBallonFunction();
        var chartOptions = {
          "type": "serial",
          "addClassNames": true,
          "theme": "light",
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
                "useGraphSettings": true,
                "markerSize":12,
                "valueWidth":0,
                "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [ {
            "id": "v1",
            "title": "錯誤率(%)",
            "position": "left",
            "maximum":100,
            //"minimum":0,
            "labelFunction": function(value) {
              return value+"%";
            }
          },
          {
            "id": "v2",
            "title": "總數量",
            "position": "right",
            "integersOnly": true
          }],
          "startDuration": 1,
          "graphs": [
            {
              "id":"total",
              "balloonFunction": ballonFunction.basic ,
              "fillAlphas": 1,
              "title": "總數量",
              "type": "column",
              "valueField": "total",
              "valueAxis": "v2",
              "dashLengthField": "dashLengthColumn",
              "lineColor": "#59a9ef",
              "labelText": "[[value]]",
              "showAllValueLabels":true
            },
            {
              "id": "failrate",
              "balloonFunction": ballonFunction.percent,
              "bullet": "round",
              "lineThickness": 3,
              "bulletSize": 7,
              "bulletBorderAlpha": 1,
              "bulletColor": "#FFFFFF",
              "useLineColorForBulletBorder": true,
              "bulletBorderThickness": 3,
              "fillAlphas": 0,
              "lineAlpha": 1,
              "lineColor":"#a70909",
              "title": "錯誤率(%)",
              "valueField": "failrate",
              "valueAxis": "v1",
              "dashLengthField": "dashLengthLine",
              "labelText": "[[value]]",
              "showAllValueLabels":true
           }],
          "categoryField": "key",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":10
          },
          "export": {
            "enabled": true
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
          "listeners": [{
            "event": "clickGraphItem",
            "method": function(event) {
              scope.drillDownTriggerChangeGroup = true;
              scope.drillDown(event.item.category);
            }
          }],
          "chartScrollbar": {
          },
        };
				var chart = AmCharts.makeChart(chartName, chartOptions);
        var zoomChart = function(){
          try{
            chart.zoomToIndexes(0,20);
          }
          catch(e){} // data size less than 20
        }
        chart.addListener("dataUpdated", zoomChart);
				scope.$watchGroup(['data'], function(){
          if(_.isArray(scope.data)){
            if(scope.data.length>0){
              scope.display = true;
              scope.curChartParams = Bakdetail.initChartParams(scope.data);
              scope.drillTablist = [];
              scope.drillTablist.push(Bakdetail.getInitDrillTab(scope.curChartParams));
              chart.dataProvider = scope.curChartParams.showdata;
              chart.validateData();
              chart.validateNow();
              scope.baksysGrid.api.setRowData(scope.data);
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
				chart.validateData();
				chart.validateNow();
			},//link
      controller:['$scope',function($scope){
        /**
            Ag-Grid Options
        */
        var aggFun = function(nodes) {
          var result = {
            FAILRATE:0,
            FAIL:0,
            TOTAL:0,
            MAXRANGE:-Infinity,
            MINRANGE:Infinity
          };
          nodes.forEach( function(node) {
            var data = node.data;
            result.FAIL+=data.FAIL;
            result.TOTAL+=data.TOTAL;
            result.MAXRANGE=Math.max(data.MAXRANGE,result.MAXRANGE);
            result.MINRANGE=Math.min(data.MINRANGE,result.MINRANGE);
          });
          result.MAXRANGE=_.round(result.MAXRANGE,2);
          result.MINRANGE=_.round(result.MINRANGE,2);
          result.FAILRATE = _.round(result.FAIL/result.TOTAL*100,2);
          return result;
        };
        var sumFunction = function(values) {
          var result = 0;
          values.forEach( function(value) {
            if (typeof value === 'number') {
              result += value;
            }
          });
          return result;
        }
        // Group 放置欄位需宣告參數: comparator: agGrid.defaultGroupComparator, cellRenderer: 'group'
        // Group 目標 rowGroupIndex 必須為 0
        $scope.gridColumnDefs = [
          {field: 'GROUP', headerName: 'Group', width: 150,comparator: agGrid.defaultGroupComparator, cellRenderer: 'group'},
          {field: 'FAILRATE', headerName: '錯誤率(%)', width: 120,
            valueGetter: function(params) {
              return _.round(params.data.FAIL/params.data.TOTAL*100, 2);
          }},
          {field: 'FAIL', headerName: '錯誤數', width: 120,enableValue: true},
          {field: 'TOTAL', headerName: '總數量', width: 120},
          {field: 'MAXRANGE', headerName: '最大Range', width: 120, 
            valueGetter: function(params) {
              return _.round(params.data.MAXRANGE, 2);
          }},
          {field: 'MINRANGE', headerName: '最小Range', width: 120,
            valueGetter: function(params) {
              return _.round(params.data.MINRANGE, 2);
          }},
          {field: 'OVENID', headerName: '爐號(Oven Id)', width: 120},
          {field: 'LOTNO', headerName: '內批', width: 120},
          {field: 'DATETIME', headerName: '時間(分)', width: 120},
          {field: 'DEVICENO', headerName: '客戶型號', width: 120},
          {field: 'PACKAGETYPE', headerName: '封裝型別', width: 120},
          {field: 'CUSTOMERLOTNO', headerName: '客戶批號', width: 120},
          {field: 'CUSTOMERCHISNAME', headerName: '客戶別', width: 120 },
          {field: 'HOUR', headerName: '時間(時)', width: 120, hide: true},
          {field: 'DAY', headerName: '時間(日)', width: 120, hide: true}
        ];
        $scope.baksysGrid = {
          columnDefs:$scope.gridColumnDefs,
          rowData:[],
          groupUseEntireRow: false,
          enableSorting: true,
          enableColResize: true,
          groupSuppressAutoColumn: true,
          groupRowAggNodes: aggFun
        };
        $scope.exportAgGrid = function(exportType){
          common.exportAgGrid('baksys-data',exportType,$scope.baksysGrid);
        };
      }]
		};
	};
	angular.module('mean.bakdetail').directive('bakSystemChart', bakSystemChart);
	bakSystemChart.$inject = ['common','Bakdetail','_'];
})();
'use strict';