
(function(){
	'use strict';

	function biTempTimeChart(common,_){
    var chartName = "bi-temptime-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
        guide: '='
			},
			template: '<div id="'+chartName+'" style="width: 85%;height: 300px; margin:20px;" ng-show="display"><div></div></div>\
      <div class="col col-sm-2" style="font-size: xx-small;">* 標準溫度=Burn-In溫度 - 3</div>\
      <hr>\
      <div ng-show="false" style="height: 300px;" class="ag-blue" ag-grid="agGrid"></div>',
			link: function (scope, element, attrs) {
        var chromeVersion = common.getChromeVersion();
        scope.display = false;
        var chartOptions = {
          "type": "serial",
          "theme": "light",
          "legend": {
              "useGraphSettings": true
          },
          "dataProvider": [],
          "synchronizeGrid":true,
          "mouseWheelZoomEnabled":true,
          "valueAxes": [{
              "id":"temp",
              "axisColor": "#000000",
              "axisThickness": 2,
              "axisAlpha": 1,
              "position": "left"
          }],
          "graphs": [{
              "valueAxis": "temp",
              "lineColor": "#0A615C",
              "bullet": "triangleUp",
              "bulletBorderThickness": 1,
              "hideBulletsCount": 30,
              "title": "sensor0",
              "valueField": "s0",
          "fillAlphas": 0
          }, {
              "valueAxis": "temp",
              "lineColor": "#D00000",
              "bullet": "square",
              "bulletBorderThickness": 1,
              "hideBulletsCount": 30,
              "title": "sensor1",
              "valueField": "s1",
          "fillAlphas": 0
          }, {
              "valueAxis": "temp",
              "lineColor": "#FFBA08",
              "bullet": "round",
              "bulletBorderThickness": 1,
              "hideBulletsCount": 30,
              "title": "sensor2",
              "valueField": "s2",
          "fillAlphas": 0
          }, {
              "valueAxis": "temp",
              "lineColor": "#3F88C5",
              "bullet": "triangleDown",
              "bulletBorderThickness": 1,
              "hideBulletsCount": 30,
              "title": "sensor3",
              "valueField": "s3",
          "fillAlphas": 0
          }, {
              "valueAxis": "temp",
              "lineColor": "#032B43",
              "bullet": "bubble",
              "bulletBorderThickness": 1,
              "hideBulletsCount": 30,
              "title": "sensor4",
              "valueField": "s4",
          "fillAlphas": 0
          }, {
              "valueAxis": "temp",
              "lineColor": "#B0DE09",
              "bullet": "square",
              "bulletBorderThickness": 1,
              "hideBulletsCount": 30,
              "title": "sensor5",
              "valueField": "s5",
          "fillAlphas": 0
          }],
          "chartScrollbar": {},
          "chartCursor": {
            "cursorPosition": "mouse",
            "categoryBalloonDateFormat": "YYYY-MM-DDTHH:NN:SS",
          },
          "categoryField": "time",
          "categoryAxis": {
              "parseDates": true,
              "minPeriod":"ss",
              "axisColor": "#DADADA",
              "minorGridEnabled": true
          },
          "export": {
            "enabled": true,
              "position": "bottom-right"
           }
        };
        // 舊版 Chrome Date function 不會自動設時區，在 new Date 運作上可能會造成一些 bug
        // 固在此針對修正 Am-Chart parseDates 運作在舊版 chrome 上的一個 bug
        if(chromeVersion<50){
          chartOptions['categoryAxis']['categoryFunction'] = function(value){
            var newvalue = new Date(value);
            return new Date(newvalue.setHours(newvalue.getHours()-8));
          }
        }
				var chart = AmCharts.makeChart(chartName, chartOptions);
				
				scope.$watchGroup(['data','guide'], function(){
          if(_.isArray(scope.data)){
            if(scope.data.length>0){
              scope.display = true;
              chart.dataProvider = scope.data;
              chart.categoryAxis.guides = [];
              if(scope.guide){
                if(scope.guide.date || scope.guide.toDate)
                  // for old version chrome
                  if(chromeVersion<50){
                    var guide = _.cloneDeep(scope.guide);
                    var fromDate = new Date(guide.date);
                    var toDate = new Date(guide.toDate);
                    chart.categoryAxis.guides =[
                    {
                      date: new Date(fromDate.setHours(fromDate.getHours()-8)),
                      toDate: new Date(toDate.setHours(toDate.getHours()-8)),
                      lineColor: "#0000AA",
                      lineAlpha: 1,
                      fillAlpha: 0.1,
                      fillColor: "#0000AA",
                      dashLength: 2,
                      inside: true
                    }];
                  }
                  else{
                    chart.categoryAxis.guides =[
                    {
                      date: scope.guide.date,
                      toDate: scope.guide.toDate,
                      lineColor: "#0000AA",
                      lineAlpha: 1,
                      fillAlpha: 0.1,
                      fillColor: "#0000AA",
                      dashLength: 2,
                      inside: true
                    }];
                  }
                if(scope.guide.biRealTime){ // standard temperature is defined
                  chart.categoryAxis.guides[0].labelRotation = 0;
                  chart.categoryAxis.guides[0].label = '實際BI時間: '+scope.guide.biRealTime;
                }
              }
              chart.validateData();
              chart.validateNow();
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
				scope.getChart = function(){
					return chart;
				}
				chart.validateData();
				chart.validateNow();
			},//link
      controller:['$scope',function($scope){
        /**
            Ag-Grid Options
        */
        // bakdetail/controllers/bakdetail.js 宣告之 ag-grid 遭遇物件 undefined 的 bug
        // 在此宣告呈現 ag-grid 可修正以上錯誤 (以上ag-grid物件於html呈現位置若放在bi-temp-time-chart物件前也可避免bug...)
        var gridColumnDefs = [
        ];
        $scope.agGrid = {
          columnDefs:gridColumnDefs,
          rowData:[],
          groupUseEntireRow: false,
          enableSorting: true,
          enableColResize: true
        };
      }]
		};
	};

	angular.module('mean.bakdetail').directive('biTempTimeChart', biTempTimeChart);
	biTempTimeChart.$inject = ['common','_'];
})();
'use strict';