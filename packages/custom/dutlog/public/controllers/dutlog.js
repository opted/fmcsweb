'use strict';
function getToday(){
  var cur = new Date();  // Get current time
  cur.setHours(0, 0, 0, 0);  // Set to the start of day
  return cur;
}
/* jshint -W098 */
function DutlogController(_,$scope, Global, $stateParams, $window, $http, srchDutOpt, dutFilterOptionService) {
    $scope.selected = {
        "from": getToday(),  // Set default start day
        "to": getToday(),
        "cust": null,
        "custlotno": null,
        "dutlog": null
    };

    $scope.varsSeleted = {
        defaultBydutChoose : [],
        defaultByboardChoose : [],
        varsList: [{"name":"TJ"},{"name":"CAL_TJ"},{"name":"TC"},{"name":"DUTV"},{"name":"DUTA"},{"name":"ERRCODE"}],
        // slotChooseList: [{"name":"board","checked":true},{"name":"dut","checked":false}],
        slotChooseList: [{"name":"board","checked":true},{"name":"dut","checked":false}],
        slotOptionList: [],
        otherSlotOptionList:[],
        varsListSelected: "TJ",
        slotChooseListSelected: "board",
        slotOptionListSelected: null
    };
    // console.log("!!!!!!!!!!!!!!!!!  "+ $scope.varsSeleted.slotChooseListSelected)
    $scope.boardInfoGrid = {
        columnDefs: [
            {headerName: "Date Time", field: "DATETIME", width: 200},
            {headerName: "File Path", field: "FILEPATH", width: 100},
            {headerName: "Oven ID", field: "OVENID", width: 100},
            {headerName: "Zone", field: "ZONE", width: 80},
            {headerName: "Board", field: "BOARD", width: 80}
        ],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        //groupSuppressAutoColumn: true,
        //suppressAggFuncInHeader: true,
    };
    $scope.requestStatus = "";  // http Request 當下狀態
    $scope.fieldSearchURL = "/api/dutlog/getsearchfield";
    $scope.fields = srchDutOpt.fields;
    $scope.disableSubField = true;
    $scope.checkMainFiled = function() {
        var queryFiled = _.pick($scope.selected, ['from', 'to', 'cust']);
        if (_.isNull(queryFiled.from) || _.isNull(queryFiled.to)) {
            $scope.disableSubField = false;
        } else {
            $scope.disableSubField = true;
        }
    };
    
    $scope.ok = function() {
        var startTime = new Date();
        $scope.queryTime = "";
        $scope.rowNum = 0;
        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }

        queryParam.from = queryParam.from.toISOString();
        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        // queryParam.VARS = $scope.varsSeleted.onSelected
        queryParam.VARS = $scope.varsSeleted.varsList
        _.assign(queryParam, srchDutOpt.getMatch($scope.fields));

        if(_.isUndefined(queryParam.CUSTOMERLOTNO) && _.isUndefined(queryParam.LOTNO) ) {
            $window.alert("請設定客批或內批");
            return 0;
        }

        // console.log(queryParam)
        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };
        $scope.requestStatus = "等待 Server 回應...";
        $scope.isSearching = true;
        $http.post('/api/dutlog/getsearchresult', queryParam, config)
        .then(function(result){
                $scope.boardInfoGrid.columnDefs = [
                    {headerName: "Date Time", field: "DATETIME", width: 200},
                    {headerName: "CUSTOMER LOTNO", field: "CUSTOMERLOTNO", width: 100},
                    {headerName: "LOTNO", field: "LOTNO", width: 100},
                    {headerName: "File Path", field: "FILEPATH", width: 100},
                    {headerName: "Oven ID", field: "OVENID", width: 100},
                    {headerName: "Zone", field: "ZONE", width: 80},
                    {headerName: "Board", field: "BOARD", width: 80}
                ]
                var dutInfoList = result.data.dutInfoList;
                $scope.requestStatus = "處理回傳資料...";
                $scope.varsSeleted.defaultBydutChoose = result.data.defaultBydutHeader;
                $scope.varsSeleted.defaultByboardChoose = result.data.defaultByboardHeader;
                // console.log("@@@@@@@@@@@@@@@@@$scope.varsSeleted.defaultBydutChoose  "+$scope.varsSeleted.defaultBydutChoose)
                // console.log("@@@@@@@@@@@@@@@@@$scope.varsSeleted.defaultByboardChoose  "+$scope.varsSeleted.defaultByboardChoose)
                // console.log("@@@@@@@@@@@@@@@@@",result.data.data)
                if($scope.varsSeleted.slotChooseListSelected==="dut"){
                    $scope.varsSeleted.slotOptionList = $scope.varsSeleted.defaultBydutChoose;
                    $scope.varsSeleted.otherSlotOptionList = $scope.varsSeleted.defaultByboardChoose;
                    if($scope.varsSeleted.defaultBydutChoose.length != 0){
                        $scope.varsSeleted.slotOptionListSelected = $scope.varsSeleted.defaultBydutChoose[0];
                    }
                }else{
                    $scope.varsSeleted.slotOptionList = $scope.varsSeleted.defaultByboardChoose;
                    $scope.varsSeleted.otherSlotOptionList = $scope.varsSeleted.defaultBydutChoose;
                    if($scope.varsSeleted.defaultByboardChoose.length != 0){
                        $scope.varsSeleted.slotOptionListSelected = $scope.varsSeleted.defaultByboardChoose[0];
                    }
                }
                $scope.rows = result.data.data;
                $scope.boardInfoGrid.api.setColumnDefs(genColumnDefs(dutInfoList, $scope.boardInfoGrid.columnDefs));
                $scope.boardInfoGrid.api.setRowData($scope.rows);
                $scope.rowNum = $scope.rows.length;
                $scope.requestStatus = "查詢完成";
                $scope.queryTime = new Date() - startTime;
            }, function(err){
                $window.alert("Server failed...");
                console.log(err)
                $scope.requestStatus = '查詢失敗, '+err.data;
            }).finally(function(){
                $scope.isSearching = false;
            });
    };


    $scope.exportXLS = function(){
        // Reference: https://www.ag-grid.com/javascript-grid-excel/#gsc.tab=0
        if ($scope.boardInfoGrid.api.getModel().getRowCount() < 1){
          $window.alert("無資料可供下載");
          return;
        }

        var params = {
          fileName: "dutlog_export.xls"
        };

        $scope.boardInfoGrid.api.exportDataAsExcel(params);
    };

    $scope.exportAllXLSX = function(){
        if (_.isNull($scope.varsSeleted.slotOptionListSelected) || "" == $scope.varsSeleted.slotOptionListSelected){
            $window.alert("請確認是否選取需要觀看的DUT位置或BOARD位置");
            return 0;
        }
        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
        queryParam.from = queryParam.from.toISOString();

        queryParam.to.setDate(queryParam.to.getDate() + 1);
        queryParam.to = queryParam.to.toISOString();
        // queryParam.ZONE = $scope.selectedZone.zone
        // queryParam.BOARD = $scope.selectedBoard.board

        queryParam.VARS = $scope.varsSeleted.varsList
        _.assign(queryParam, srchDutOpt.getMatch($scope.fields));
        queryParam.slotChooseListSelected = $scope.varsSeleted.slotChooseListSelected
        queryParam.slotOptionListSelected = $scope.varsSeleted.slotOptionListSelected
        // console.log("queryParam   ", queryParam)
        // _.assign(queryParam, srchOptDc.getMatch());
        $http.post('/api/dutlog/downloadallxls'
            ,queryParam
             ,{responseType: 'arraybuffer'}
        )
            .then(function(result){
                // console.log(result)
                var headers = result.headers();
                var filename = ""
                if("LOTNO" in queryParam){
                    filename = queryParam.LOTNO + '.xlsx';
                }else{
                    filename = queryParam.CUSTOMERLOTNO + '.xlsx';
                }
                var contentType = headers['content-type'];
                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([result.data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);

                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", filename);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    // setTimeout(function () {
                    linkElement.dispatchEvent(clickEvent);
                    // },10000)
                } catch (ex) {
                    console.log(ex);
                }
        }, function(err){
            $window.alert("Server failed...");
        });
    };

    // filter panel
    $scope.dutFilterOption = dutFilterOptionService.dutFilterOption;
    $scope.isPanelVisible = false;
    $scope.togglePanelShow = function() {
        $scope.isPanelVisible = !($scope.isPanelVisible);
        var varsList =getSeletedVars($scope.dutFilterOption)
        if( varsList.length > 0){
            var tempList =[]
            _.forEach(varsList,function(v){
                var tempObj = {'id': v, 'name':v}
                tempList.push(tempObj);
            })
            $scope.varsSeleted.varsList=tempList
        }
    };
    $scope.setVarlist = function() {
        var varsList =getSeletedVars($scope.dutFilterOption)
        if( varsList.length > 0){
            var tempList =[]
            _.forEach(varsList,function(v){
                var tempObj = {'id': v, 'name':v}
                tempList.push(tempObj);
            })
            $scope.varsSeleted.varsList=tempList
        }
    };
    $scope.changeSlotChoose = function(){
        // console.log("!!!!"+$scope.varsSeleted.slotChooseListSelected)
        // console.log("!!!!scope.varsSeleted.slotOptionList   "+$scope.varsSeleted.slotOptionList)
        if($scope.varsSeleted.slotChooseListSelected==="dut"){
            $scope.varsSeleted.slotOptionList = $scope.varsSeleted.defaultBydutChoose;
            $scope.varsSeleted.otherSlotOptionList = $scope.varsSeleted.defaultByboardChoose;
            if($scope.varsSeleted.defaultBydutChoose.length != 0){
                $scope.varsSeleted.slotOptionListSelected = $scope.varsSeleted.defaultBydutChoose[0];
            }
        }else{
            $scope.varsSeleted.slotOptionList = $scope.varsSeleted.defaultByboardChoose;
            $scope.varsSeleted.otherSlotOptionList = $scope.varsSeleted.defaultBydutChoose;
            if($scope.varsSeleted.defaultByboardChoose.length != 0){
                $scope.varsSeleted.slotOptionListSelected = $scope.varsSeleted.defaultByboardChoose[0];
            }
        }

        // $scope.rows
    }
    var genColumnDefs = function(list, defs){
        _.forEach(list, function(element){
            defs.push({ headerName: element, field: element, width: 150});
        });
        return defs;
    }

    var getSeletedVars = function(filterOpt){
        var selectedList=[]
        _.forOwn(filterOpt,function(group){
            selectedList = _.concat(selectedList, _.flattenDeep(group))
        })
        // console.log(_.filter(selectedList,function(v){return v.checked; }))
        return _.map(_.filter(selectedList,function(v){return v.checked }),'name')
    }
}


angular
    .module('mean.dutlog')
    .controller('DutlogController', DutlogController);

DutlogController.$inject = ['_','$scope', 'Global', '$stateParams', '$window', '$http', 'srchDutOpt', 'dutFilterOptionService'];
