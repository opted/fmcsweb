(function(){
    'use strict';
    function srchDutOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("客批(CustomerLotno)", "CUSTOMERLOTNO"),
                uOpt.genField("客戶(Siteno)", "SITENO", "string"),
                uOpt.genField("內批(Lotno)", "LOTNO", "string", false),
                uOpt.genField("ZONE", "ZONE", "string", false, false),
                uOpt.genField("BOARD", "BOARD", "string", false, false),
                uOpt.genField("DUT", "DUT", "string", false, false),
            ],
            getMatch: uOpt.getMatch
        };
    };

    angular.module('mean.dutlog').factory('srchDutOpt', srchDutOpt);
    srchDutOpt.$inject = ['utilsSrchOpt'];
})();