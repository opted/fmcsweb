(function() {
    'use strict';

    function dutFilterOptionService() {
		var test = '123';
        var dutFilterOption = {
            "group": [
            	//[ { "name": "TJ", "checked": false }, { "name": "CAL_TJ", "checked": false }, { "name": "TC", "checked": false }, { "name": "DUTV", "checked": false }, { "name": "ERRCODE", "checked": false } ],
				[ { "name": "TJ", "checked": true } ],
				[ { "name": "CAL_TJ", "checked": true } ],
				[ { "name": "TC", "checked": true } ],
				[ { "name": "DUTV", "checked": true } ],
				[ { "name": "DUTA", "checked": true } ],
				[ { "name": "ERRCODE", "checked": true } ]
			],
        };

        return {
            dutFilterOption: dutFilterOption
        }
    }

    angular
        .module('mean.dutlog')
        .factory('dutFilterOptionService', dutFilterOptionService);

    dutFilterOptionService.$inject = [];

})();