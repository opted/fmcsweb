(function() {
    'use strict';

    function Dutlog($stateProvider) {
        $stateProvider.state('dutlog example page', {
            url: '/dutlog/example',
            templateUrl: 'dutlog/views/index.html',
            controller:DutlogController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              }
            }
        });
    }

    angular
        .module('mean.dutlog')
        .config(Dutlog);

    Dutlog.$inject = ['$stateProvider'];

})();
