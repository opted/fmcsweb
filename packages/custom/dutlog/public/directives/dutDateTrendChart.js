
(function(){
	'use strict';

	function dutDateTrendChart(){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				showList:'=',
				varsListSelected:'=',
				slotChooseListSelected:'=',
				otherSlotOptionList:'=',
				slotOptionListSelected:'='
				//isDynamic:'=',
			},
			template: '<div style="width: 100%;height: 500px;"><div></div></div>',
			link: function (scope, element, attrs) {
				var chartOptions;
				var genGraphs = function(varsListSelected,slotChooseListSelected,showList){
					var graphList = []
					if(showList==undefined)
						return graphList;
					_.forEach(showList,function(varId){
						// console.log("zoneBoard "+ zoneBoard)
						var id = "";
						var title = "";
						var valueField = "";
						if('dut' === scope.slotChooseListSelected){
							id = "ZONE"+varId.split(";")[0]+"BOARD"+varId.split(";")[1];
							title = "ZONE"+varId.split(";")[0]+"BOARD"+varId.split(";")[1];

							valueField = varsListSelected+varId.split(";")[0]+varId.split(";")[1];
						}else if('board' === scope.slotChooseListSelected){
							id = "DUT"+varId;
							title = "DUT"+varId;
							valueField = varsListSelected+varId;
						}
						
						var graph = new Object({
							"id": id,
							// "balloon":{
							// 	"drop":true,
							// 	"adjustBorderColor":false,
							// 	"color":"#ffffff"
							// },
							// "bullet": "round",
							// "bulletBorderAlpha": 1,
							// "bulletColor": "#FFFFFF",
							// "bulletSize": 5,
							// "hideBulletsCount": 50,
							// "lineThickness": 2,
							// "useLineColorForBulletBorder": true,
							"title": title,
							"valueField": valueField,
							"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
						});
						graphList.push(graph)
					})
					return graphList
				}
				chartOptions = {
					"type": "serial",
					"theme": "light",
					"autoMargins": false,
                                        "marginRight": 40,
					"marginLeft": 60,
					"autoMarginOffset": 20,
					"mouseWheelZoomEnabled":true,
					"dataDateFormat": "YYYY-MM-DD JJ:NN:SS",
					"valueAxes": [{
						"id": "v1",
						"axisAlpha": 0,
						"position": "left",
						"ignoreAxisWidth":true
					}],
					"balloon": {
						"borderThickness": 1,
						"shadowAlpha": 0
					},
					"legend": {
						"valueText":"",
						"horizontalGap": 6,
						"position": "right",
						"useGraphSettings": true,
						"markerSize": 6,
						"maxColumns": 6,

					},
					"graphs": [
					// 	{
					// 	"id": "g1",
					// 	"balloon":{
					// 		"drop":true,
					// 		"adjustBorderColor":false,
					// 		"color":"#ffffff"
					// 	},
					// 	"bullet": "round",
					// 	"bulletBorderAlpha": 1,
					// 	"bulletColor": "#FFFFFF",
					// 	"bulletSize": 5,
					// 	"hideBulletsCount": 50,
					// 	"lineThickness": 2,
					// 	"title": "red line",
					// 	"useLineColorForBulletBorder": true,
					// 	"valueField": "value",
					// 	"balloonText": "<span style='font-size:18px;'>[[value]]</span>"
					// }
					],
					// "chartScrollbar": {
					// 	// "graph": "g1",
					// 	"oppositeAxis":false,
					// 	"offset":30,
					// 	"scrollbarHeight": 80,
					// 	"backgroundAlpha": 0,
					// 	"selectedBackgroundAlpha": 0.1,
					// 	"selectedBackgroundColor": "#888888",
					// 	"graphFillAlpha": 0,
					// 	"graphLineAlpha": 0.5,
					// 	"selectedGraphFillAlpha": 0,
					// 	"selectedGraphLineAlpha": 1,
					// 	"autoGridCount":true,
					// 	"color":"#AAAAAA"
					// },
					"chartCursor": {
						// "pan": true,
						// "valueLineEnabled": true,
						// "valueLineBalloonEnabled": true,
						// "cursorAlpha":1,
						// "cursorColor":"#258cbb",
						// "valueLineAlpha":0.2,
						// "valueZoomable":true
					},
					// "valueScrollbar":{
					// 	"oppositeAxis":false,
					// 	"offset":50,
					// 	"scrollbarHeight":10
					// },
					"chartScrollbar": {
					        // "autoGridCount": true,
					        "scrollbarHeight": 40
					    },
					"categoryField": "DATETIME",
					"categoryAxis": {
						"parseDates": true,
						"dashLength": 1,
						"minorGridEnabled": true,
						"minPeriod":"ss"
					},
					"export": {
						"enabled": true
					},
					"dataProvider": []
					// 	[
					// 	{
					// 		"date": "2012-07-27",
					// 		"value": 13
					// 	}, {
					// 		"date": "2012-07-28",
					// 		"value": 11
					// 	}
					// ]
				}

				var target = element[0].children[0];

				var chart = AmCharts.makeChart(target, chartOptions);

				scope.$watchGroup(['data', 'varsListSelected','slotChooseListSelected','slotOptionListSelected'], function () {
					if(_.isArray(scope.data)){
						var newShowNumList = []
						var newData = [];
						if(scope.varsListSelected!=null&& scope.slotChooseListSelected !=null&&scope.slotOptionListSelected!=null){

							
							if('dut' === scope.slotChooseListSelected){
								var dutID = scope.slotOptionListSelected.substring(scope.slotOptionListSelected.indexOf("DUT")+3,scope.slotOptionListSelected.indexOf("DUT")+5);
								_.forEach(scope.otherSlotOptionList,function(slotOptionListItem){
									var zoneID = slotOptionListItem.substring(slotOptionListItem.indexOf("ZONE")+4,slotOptionListItem.indexOf("ZONE")+6);
									var boardID = slotOptionListItem.substring(slotOptionListItem.indexOf("BOARD")+5,slotOptionListItem.indexOf("BOARD")+7);
									newShowNumList.push(zoneID+";"+boardID);
								})
								var testDateStr = "";
								var newDataObj = {};

								_.forEach(scope.data,function(dataInfo){
									
									if(""==testDateStr){
										testDateStr = dataInfo["DATETIME"];
										newDataObj[scope.varsListSelected+dataInfo["ZONE"]+dataInfo["BOARD"]] = dataInfo[scope.varsListSelected + dutID];
										newDataObj["DATETIME"] = dataInfo["DATETIME"];
									}else{
										
										if(testDateStr!=dataInfo["DATETIME"]){
											testDateStr = dataInfo["DATETIME"];
											newData.push(newDataObj);
											newDataObj = {};
											newDataObj[scope.varsListSelected+dataInfo["ZONE"]+dataInfo["BOARD"]] = dataInfo[scope.varsListSelected + dutID];
											newDataObj["DATETIME"] = dataInfo["DATETIME"];
										}else{
											newDataObj[scope.varsListSelected+dataInfo["ZONE"]+dataInfo["BOARD"]] = dataInfo[scope.varsListSelected + dutID];
										}
									}
								})
								if(newDataObj != {}){
									newData.push(newDataObj);
									newDataObj = {};
								}
							}else if('board' === scope.slotChooseListSelected){
								var zoneID = scope.slotOptionListSelected.substring(scope.slotOptionListSelected.indexOf("ZONE")+4,scope.slotOptionListSelected.indexOf("ZONE")+6);
								var boardID = scope.slotOptionListSelected.substring(scope.slotOptionListSelected.indexOf("BOARD")+5,scope.slotOptionListSelected.indexOf("BOARD")+7);

								_.forEach(scope.otherSlotOptionList,function(slotOptionListItem){
									newShowNumList.push(slotOptionListItem.substring(slotOptionListItem.indexOf("DUT")+3,slotOptionListItem.indexOf("DUT")+5))
								})

								_.forEach(scope.data,function(dataInfo){

									if(zoneID == dataInfo["ZONE"]&&boardID==dataInfo["BOARD"]){
										newData.push(dataInfo);
									}
								})
							}
						}

						scope.graphList = genGraphs(scope.varsListSelected,scope.slotChooseListSelected,newShowNumList)

						chart.graphs =scope.graphList
						chart.dataProvider = newData;

						chart.validateData();
						chart.validateNow();
					}
				});
				
				scope.getChart = function(){
					return chart;
				}

				chart.validateData();
				chart.validateNow();
			}
		};
	};

	angular.module('mean.dutlog').directive('dutDateTrendChart', dutDateTrendChart);
	dutDateTrendChart.$inject = [];
})();
'use strict';

