(function(){
    'use strict';

    function searchDutFilter($http, $window, srchDutOpt){
        return {
            restrict: 'E',
            scope: {
                field: "="
            },
            templateUrl: 'dutlog/views/searchDutFilter.html',
            link: function(scope, element, attrs){
                scope.getField = function($select, field){
                    var searchStr = $select.search;
                    field.selected = searchStr;
                    field.searchField = $select; // keep field's info object
                    if (!_.isEmpty(searchStr)) {
                        if (searchStr.slice(-1) != "*" && searchStr.search(",") == -1) { // Query if without * and comma
                            var match = srchDutOpt.getMatch();
                            // console.log(srchDutOpt)
                            $http.post('/api/dutlog/getsearchfield', {
                                'field': field.name,
                                'regex': searchStr,
                                'match': match
                            }).then(function(res) {
                                // set field selections
                                field.filteredData = res.data;
                            }, function(err){
                                $window.alert("Server failed...");
                            });
                        }
                    } else {
                        field.filteredData = null; // clear the drop down list
                    }
                };
                
                scope.syncSearchField = function ($select, showFieldObj) {
                    $select.search = showFieldObj.selected;
                };
            }
        };
    };
    
    angular.module('mean.dutlog').directive('searchDutFilter', searchDutFilter);
    searchDutFilter.$inject = ['$http', '$window', 'srchDutOpt'];
})();