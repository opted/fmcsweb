(function() {
	'use strict';

	function dutFilterPanel(dutFilterOptionService) {
		return {
			restrict: 'E',
			scope: {
				dutFilterOption: '=',
				isVisible: '='
			},
			templateUrl: 'dutlog/views/dutFilterPanel.html',
			link: function(scope) {
				scope.toggleShow = function() {
					scope.isVisible = !(scope.isVisible);
				}
			}
		};
	}

	angular
		.module('mean.dutlog')
		.directive('dutFilterPanel', dutFilterPanel);

	dutFilterPanel.$inject = [];

})();