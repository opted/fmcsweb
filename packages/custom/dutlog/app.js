'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Dutlog = new Module('dutlog');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Dutlog.register(function(app, auth, database, utils) {

  //We enable routing. By default the Package Object is passed to the routes
  Dutlog.routes(app, auth, database, utils);

  //We are adding a link to the main menu for all authenticated users
  // Dutlog.menus.add({
  //   title: 'dutlog example page',
  //   link: 'dutlog example page',
  //   roles: ['authenticated'],
  //   menu: 'main'
  // });
  
  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Dutlog.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Dutlog.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Dutlog.settings(function(err, settings) {
        //you now have the settings object
    });
    */
  Dutlog.angularDependencies(['agGrid']);
  return Dutlog;
});
