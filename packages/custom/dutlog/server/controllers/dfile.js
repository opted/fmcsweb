'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    fs = require('fs'),
    xl = require('excel4node');
var sizeOf = require('image-size');


exports.downloadExcelpython = function(req, res, next) {
    req.setTimeout(1800000); // 30 min

    var DclogManager = require('./dutlogQuery');
    var dclogMgr = new DclogManager(next) // utils

    var PythonShell = require('python-shell');

    var dbaddr = process.env.DB_PORT_27017_TCP_ADDR.split(':');
    var ipaddr = dbaddr[0];
    var dbport = '27017';
    if (dbaddr.length > 1)
        dbport = dbaddr[1];

    var dbname = process.env.DB_PORT_27017_DATABASE;
    // console.log('##### Download req: ', req);
    // console.log('##### Download Excel: ', ipaddr, dbport, dbname);
    var fileName = './python/report/dut.xlsx';

    var options = {
        scriptPath: './python/dutlog/',
        args: [JSON.stringify(req.body)]
    };
    PythonShell.run('gen_dut_excel.py', options, function(err) {
        if (err) {
            // console.log('=====================================')
            res.download(fileName);
            // res.status(500);
            // res.json(err);
        } else {
            res.status(500);
            res.json('Try again')
            // res.download(fileName);
            // setTimeout(function() {
            //     fs.unlink(fileName, function(err) {
            //         if (err) console.log(err)
            //     });
            // }, 20000);
        }
    });
}


exports.downloadExcel = function(req, res, next) {
    var multi = req.body.multi;
    if (multi == undefined)
        multi = false;

    var timg_group = [];
    if (!multi)
        timg_group = [{ table: req.body.table, headerorder: req.body.headerorder, pngb64: req.body.pngb64 }];
    else
        timg_group = req.body.items;

    // Generate export excel file
    var excelFileName = "/tmp/excel-" + Math.ceil(Math.random() * 10000) + ".xlsx";
    var wb = new xl.Workbook();
    var ws = wb.addWorksheet('Sheet 1');
    var curRowIdx = 1;
    var imageRowSize = 10; // 10 height of image, approximate 180 px 

    var recursiveExport = function(ti_idx) {
        var table = timg_group[ti_idx].table; //JSON.parse(req.query.table);	
        var headerorder = timg_group[ti_idx].headerorder; //req.query.headerorder;
        var pngbase64 = timg_group[ti_idx].pngb64.replace(/^data:image\/png;base64,/, ""); //req.query.pngb64.replace(/^data:image\/png;base64,/, "");
        headerorder==undefined?headerorder=[]:headerorder=headerorder;
        if(headerorder.length==0) {
            headerorder = [];
            for (var key in table[0])
                headerorder.push(key);
        }
        var rnd_num = Math.ceil(Math.random() * 10000);
        var pngfilename = "/tmp/out-" + rnd_num + ".png";

        //turn the base64 to png
        writeFile(pngfilename, pngbase64, 'base64', function(err) {
            // Get size of image
            sizeOf(pngfilename, function(err_imgsize, dimensions) {
                if (!err_imgsize) {
                    imageRowSize = Math.ceil(dimensions.height / 18);
                } else
                    imageRowSize = 2;
                //if(err!=null)
                //	return res.status(500).send("convert base64 file error!");

                // write header
                for (var i in headerorder) {
                    i = Number(i);
                    ws.cell(curRowIdx, i + 1).string(headerorder[i]);
                }
                curRowIdx += 1;

                // write table
                for (var i in table) {
                    for (var j in headerorder) {
                        var colidx = Number(j) + 1;
                        var value = table[i][headerorder[j]];
                        if ( value ==null) {
                            ws.cell(curRowIdx, colidx).string(String(' '))
                        }
                        else if (_.isNumber(Number(value)) && !isNaN(Number(value))) {
                            ws.cell(curRowIdx, colidx).number(Number(value));
                        } else
                            ws.cell(curRowIdx, colidx).string(String(value));
                    }
                    curRowIdx += 1;
                }
                // write png
                if (pngbase64.length > 100) {
                    ws.addImage({
                        path: pngfilename,
                        type: 'picture',
                        position: {
                            type: 'oneCellAnchor',
                            from: {
                                col: 1,
                                colOff: '0.5in',
                                row: curRowIdx,
                                rowOff: 0
                            }
                        }
                    });
                    curRowIdx += imageRowSize;
                } else
                    curRowIdx += 2;
                ti_idx += 1;
                if (ti_idx < timg_group.length) {
					setTimeout(function() {
						fs.unlink(pngfilename, function(err) {
							// if (err) console.log(err)
						});
					}, 10000);
                    recursiveExport(ti_idx);
                } else {
                    wb.write(excelFileName);
						
						var downloadExcel = function(count){
                            // console.log("HERE");
							if(count>5)
								return;
							setTimeout(function() {
								if (fs.existsSync(excelFileName))
									res.download(excelFileName);
								else
									downloadExcel(count+1);
							}, 1000);
						};
						downloadExcel(0);
                        setTimeout(function() {
                            fs.unlink(pngfilename, function(err) {
                                // if (err) console.log(err)
                            });
                            fs.unlink(excelFileName, function(err) {
                                // if (err) console.log(err)
                            });
                        }, 10000);
                }
                //res.json("create success!");	
            }); // get image size
        });
    }; //recursiveExport
    recursiveExport(0);
};
//write file
var mkdirp = require('mkdirp');
var getDirName = require('path').dirname;

function writeFile(path, contents, options, callback) {
    //without options
    if (typeof(options) == "function") {
        callback = options;
        options = {
            encoding: 'utf8',
            mode: '0o666',
            flag: 'w'
        }
    }
    mkdirp(getDirName(path), function(err) {
        if (err) return callback(err);

        fs.writeFile(path, contents, options, callback);
    });
};