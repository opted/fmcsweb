'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var DUTLOG = mongoose.model('DUTLOG');
var Q = require('q');

class DutlogManager {
  constructor(utils) {
    this.coll = DUTLOG;
    this.utils = utils;
    this.pathMapping = {
      'CUSTOMERLOTNO': 'CUSTOMERLOTNO',
      'SITENO': 'SITENO',
      'LOTNO': 'LOTNO',
      'ZONE': 'SLOTINFO.ZONE',
      'BOARD': 'SLOTINFO.BOARD',
      'DUT': 'SLOTINFO.DUT_LIST.DUT'
    };
    this.arrayFields = ['SLOTINFO','DUT_LIST'];
  }
  getSearchResult (params) {
    var df = Q.defer();
    try {
        var query = params;
        var match = {}
      
        var dateRange = {'CREATETIME': {}};
        if (query.from !== null){
          dateRange.CREATETIME['$gte'] = new Date(query.from);
        }
        if (query.to !== null){
          dateRange.CREATETIME['$lt'] = new Date(query.to);
        }
      
        if (!_.isEmpty(dateRange.CREATETIME)){
          _.assign(match, dateRange);
        }
        _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, [ 'CUSTOMERLOTNO', 'SITENO', 'LOTNO']), this.pathMapping));
        
        var pipe = [{'$match': match}];

        pipe.push({'$unwind': "$SLOTINFO"});
        pipe.push({'$unwind': "$SLOTINFO.DUT_LIST"});
        pipe.push({'$match': this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['ZONE', 'BOARD','DUT']), this.pathMapping)});
        var pushObj = {}
        pushObj["DUT"] = "$SLOTINFO.DUT_LIST.DUT"
        _.forEach(query.VARS,function(varObj){
            var searchName = varObj["name"];
            pushObj[searchName] = "$SLOTINFO.DUT_LIST." + searchName
        });

        pipe.push({'$group':
                        {
                            '_id': 
                                { 
                                    'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$SLOTINFO.DATETIME" }},
                                    'OVEN': "$OVENID" ,
                                    'ZONE': "$SLOTINFO.ZONE" , 
                                    'BOARD' : "$SLOTINFO.BOARD" 
                                },
                            'DUTLIST':
                                { 
                                    '$push': pushObj
                                        // {  
                                        //     'DUT': "$SLOTINFO.DUT_LIST.DUT",
                                        //     'TJ': "$SLOTINFO.DUT_LIST.TJ",
                                        //     'CAL_TJ': "$SLOTINFO.DUT_LIST.CAL_TJ",
                                        //     'TC': "$SLOTINFO.DUT_LIST.TC",
                                        //     'DUTV': "$SLOTINFO.DUT_LIST.DUTV",
                                        //     'DUTA': "$SLOTINFO.DUT_LIST.DUTA",
                                        //     'ERRCODE': "$SLOTINFO.DUT_LIST.ERRCODE" 
                                        // }
                                },
                            'FILEPATH':
                                {
                                    '$first':"$FILEPATH"
                                },
                            'OVENID':
                                {
                                    '$first':"$OVENID"
                                },
                            'LOTNO':
                                {
                                    '$first':"$LOTNO"
                                },
                            'CUSTOMERLOTNO':
                                {
                                    '$first':"$CUSTOMERLOTNO"
                                }
                        }
                    });
        pipe.push({'$project': {
            '_id': 0,
            'FILEPATH': "$FILEPATH",
            'OVENID': "$OVENID", 
            'LOTNO': "$LOTNO", 
            'CUSTOMERLOTNO': "$CUSTOMERLOTNO", 
            'DATETIME':  "$_id.DATETIME" ,
            'ZONE': '$_id.ZONE',
            'BOARD': '$_id.BOARD',
            'DUT': '$_id.DUT_LIST.DUTS',
            'DUTLIST': 1
        }});
        pipe.push({'$sort': {"DATETIME": 1}});

      var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(DUTLOG, pipe)
      .then(function(result){
        try {
            // if (res.statusCode === 500) {
            //     res.end();
            //     return;
            // }
            // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!tt");
            // console.log(result);
            var dutInfoList = new Set();
            var defaultBydutHeader = [];
            var defaultByboardHeader = [];
            // var boardArray = [];
            // var zoneArray = [];
            var boardArray = new Set();
            var zoneArray = new Set();
            var bydutIndex = 0;
            _.map(result,"BOARD")
            var countt = 0;
            // _.forEach(result,function(ovenInfo){
            //     boardArray.add(ovenInfo['BOARD']);
            //     zoneArray.add(ovenInfo['ZONE']);
            //     _.forEach(ovenInfo.DUTLIST,function(dutInfo){
            //         if(bydutIndex  == 0 ){
            //             defaultBydutHeader.push("DUT"+dutInfo.DUT);                            
            //         }
            //         _.forEach(dutInfo, function(v, k){
            //             if(k === 'DUT'){
            //                 return;
            //             };
            //             countt++;
            //             // var dutNo = ;
            //             dutInfoList.add(k+dutInfo.DUT);
            //             ovenInfo[k+dutInfo.DUT] = dutInfo[k];
            //             k=null;
            //             v=null;
            //         });
            //         dutInfo = null;
            //     });
            //     delete ovenInfo["DUTLIST"];
            //     // console.log("!!!!ovenInfo  ", ovenInfo)
            //     bydutIndex++;
            // });

            for(var i = 0 ; i <= (result.length - 1) ; i++){
                boardArray.add(result[i]['BOARD']);
                zoneArray.add(result[i]['ZONE']);
                for(var j = 0 ; j <= (result[i].DUTLIST.length - 1) ; j++){
                    if(bydutIndex  == 0 ){
                        defaultBydutHeader.push("DUT"+result[i].DUTLIST[j].DUT);                            
                    }

                    _.forEach(result[i].DUTLIST[j], function(v, k){
                        if(k === 'DUT'){
                            return;
                        };
                        countt++;
                        dutInfoList.add(k+result[i].DUTLIST[j].DUT);
                        result[i][k+result[i].DUTLIST[j].DUT] = result[i].DUTLIST[j][k];
                        k=null;
                        v=null;
                    });
                }
                delete result[i]["DUTLIST"];
                bydutIndex++;
            }
            boardArray = Array.from(boardArray);
            zoneArray = Array.from(zoneArray);
            _.forEach(boardArray,function(boardID){
                _.forEach(zoneArray,function(zoneID){
                    for(var i = 0 ; i < 4 ; i++){
                        defaultByboardHeader.push("ZONE"+zoneID+"BOARD"+boardID);
                    }
                })
            })
            defaultByboardHeader = _.uniq(defaultByboardHeader);
            df.resolve( {'data': result , 'dutInfoList':Array.from(dutInfoList),'defaultBydutHeader':defaultBydutHeader,'defaultByboardHeader':defaultByboardHeader});
        } catch (e) {
            df.reject(e.stack);
            // console.log(e)
        }
      });
    } catch (err){
      df.reject(err.stack);
      // console.log(err)
    }

    return df.promise;
  };
}

module.exports = DutlogManager;