(function() {
    'use strict';

    /* jshint -W098 */
    // The Package is past automatically as first parameter
    module.exports = function(Dutlog, app, auth, database, utils) {
        var dfile = require('../controllers/dfile.js');
        var DutlogManager = require('../controllers/dutlogQuery');
        var dutlogMgr = new DutlogManager(utils);
        
        app.post('/api/dutlog/downloadxls', dfile.downloadExcel);
        app.post('/api/dutlog/downloadallxls', function(req, res, next){
            //dfile.downloadAllExcel(req,res,utils)
            dfile.downloadExcelpython(req,res,utils)
            
            // dclogMgr.getSearchResult(req.body)
            //     .then((data) => res.json(data))
            // .catch((err) => res.status(500).send({'error': err}));
        })        
        app.post('/api/dutlog/getsearchfield', function(req, res, next){
            utils.ctrls.mongoQuery.getSearchField(dutlogMgr.coll, req.body, dutlogMgr.pathMapping, dutlogMgr.arrayFields)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });

        app.post('/api/dutlog/getsearchresult',  function(req, res, next){
            dutlogMgr.getSearchResult(req.body)
            .then((data) => res.json(data))
            .catch((err) => res.status(500).send({'error': err}));
        });
    };

})();
