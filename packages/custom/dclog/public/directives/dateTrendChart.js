(function(){
	'use strict';

	function dateTrendChart(_){
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data
				showList:'=',
				titleName:'=',
				showTemp:'='
			},
			template: '<div style="width: 100%;height: 500px;"><div></div></div>',
			link: function (scope, element, attrs) {
				var chartOptions;
				var genGraphs = function(showList,showTemp){
					var ovenList = ['Left_Oven','Right_Oven'];
					var graphList = [], totalShowList=[];
					if(showList==undefined)
						return graphList;
	
					totalShowList = showTemp?_.concat(ovenList,showList):showList;

					_.forEach(totalShowList,function(zoneBoard){
						var graph = new Object({
							"id": zoneBoard,
							"title": zoneBoard,
							"valueField": zoneBoard,
							"balloonText": "<span style='font-size:18px;'>[[value]]</span>",
							"valueAxis": (zoneBoard =='Left_Oven' || zoneBoard =='Right_Oven')? "v2":"v1"
						});
						graphList.push(graph)
					})
					return graphList
				}
				chartOptions = {
					"type": "serial",
					"theme": "light",
					"autoMargins": false,
					"marginRight": 40,
					"marginLeft": 40,
					// "autoMarginOffset": 20,
					"mouseWheelZoomEnabled":true,
					"dataDateFormat": "YYYY/MM/DD JJ:NN:SS",
					"valueAxes": [{
						"id": "v1",
						"axisAlpha": 0,
						"position": "left",
						"ignoreAxisWidth":true
					},{
						"id": "v2",
						"axisAlpha": 0,
						"position": "right",
						"gridAlpha":0,
						"ignoreAxisWidth":true
					}],
					"balloon": {
						"borderThickness": 1,
						"shadowAlpha": 0
					},
					"legend": {
						"valueText":"",
						"horizontalGap": 6,
						"position": "right",
						"useGraphSettings": true,
						"markerSize": 6,
						"maxColumns": 6,

					},
					"titles":[{
						"text": ""
					}],
					"graphs": [

					],
	
					"chartCursor": {
						"categoryBalloonDateFormat":"YYYY/MM/DD JJ:NN:SS"
					},

					"chartScrollbar": {
					        "autoGridCount": true,
					        // "graph": "g1",
					        "scrollbarHeight": 40
					    },

					"categoryField": "DATE",
					"categoryAxis": {
						"parseDates": true,
						"dashLength": 1,
						"minorGridEnabled": true,
						"minPeriod":"ss"
					},
					"export": {
						"enabled": true
					},
					"dataProvider": []

				}

				var target = element[0].children[0];

				var chart = AmCharts.makeChart(target, chartOptions);
				// scope.lineChartData = genHistogramData(scope.siteData, scope.min, scope.range);

				scope.$watch('data', function () {
					updateChart();
				});
				scope.$watch('showTemp', function () {
					updateChart();
				});
				
				var updateChart = function(){
					if(_.isArray(scope.data)){
					scope.graphList = genGraphs(scope.showList,scope.showTemp)
					chart.graphs =scope.graphList;
					chart.dataProvider = scope.data;
					chart.titles[0].text = scope.titleName;
					chartOptions.titles.text = scope.titleName;
					chart.validateData();
					chart.validateNow();
					}	
				}
				scope.getChart = function(){
					return chart;
				}

				chart.validateData();
				chart.validateNow();
			}
		};
	};

	angular.module('mean.dclog').directive('dateTrendChart', dateTrendChart);
	dateTrendChart.$inject = ['_'];
})();
'use strict';

