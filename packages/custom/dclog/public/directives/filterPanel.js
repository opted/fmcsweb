(function() {
	'use strict';

	function filterPanel(filterOptionService) {
		return {
			restrict: 'E',
			scope: {
				filterOption: '=',
				isVisible: '=',
				varsList:'='
			},
			templateUrl: 'dclog/views/filterPanel.html',
			link: function(scope) {
				scope.toggleShow = function() {
					scope.isVisible = !(scope.isVisible);
				}
				scope.addToList = function(item){
					if(item.name=="全選")
					{
						_.forOwn(scope.filterOption, group => 
							_.forOwn(group, checkboxList => 
								_.forEach(checkboxList, function(box){
									box.checked=item.checked;
								} )))
					}
					// var selectedList=[]
					// _.forOwn(scope.filterOption,function(group){
					// 	selectedList = _.concat(selectedList, _.flattenDeep(group))
					//
					// })
					// scope.varsList = _.map(_.filter(selectedList,function(v){return v.checked }),'name')
				}
			}
		};
	}

	angular
		.module('mean.dclog')
		.directive('filterPanel', filterPanel);

	filterPanel.$inject = [];

})();