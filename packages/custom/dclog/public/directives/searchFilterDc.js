(function(){
    'use strict';

    function searchFilterDc($http, $window, srchDCOpt){
        return {
            restrict: 'E',
            scope: {
                field: "="
            },
            templateUrl: 'dclog/views/searchFilterDc.html',
            link: function(scope, element, attrs){
                scope.getField = function($select, field){
                    var searchStr = $select.search;
                    field.selected = searchStr;
                    field.searchField = $select; // keep field's info object
                    if(field.disable == true) ;
                    else if (!_.isEmpty(searchStr) && searchStr.length>1) {
                        if (searchStr.slice(-1) != "*" && searchStr.search(",") == -1) { // Query if without * and comma
                            // var match = srchOptDc.getMatch();
                            // $http.post('/api/dclog/getsearchfield', {
                            //     'field': field.name,
                            //     'regex': searchStr,
                            //     'match': match
                            // }).then(function(res) {
                            //     // set field selections
                            //     field.filteredData = res.data;
                            // }, function(err){
                            //     $window.alert("Server failed...");
                            // });
                        }
                    } else {
                        field.filteredData = null; // clear the drop down list
                    }
                };
                //update
                scope.syncSearchField = function ($select, showFieldObj) {
                    $select.search = showFieldObj.selected;
                };
            }
        };
    };
    
    angular.module('mean.dclog').directive('searchFilterDc', searchFilterDc);
    searchFilterDc.$inject = ['$http', '$window', 'srchDCOpt'];
})();