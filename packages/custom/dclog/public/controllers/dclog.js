'use strict';
var debugDC;

function addZero(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function strToLocalDate(dateIsoStr) {
    var dateObj = new Date(dateIsoStr);
    dateObj.setHours(dateObj.getHours()-8);
    var dateOptions = { year:'numeric',month: '2-digit', day: '2-digit'}
    var timeOptions = {minute:'2-digit',second:'2-digit'};
  return dateObj.toLocaleDateString('taiwan',dateOptions) + ' '+  addZero(dateObj.getHours())+":"+dateObj.toLocaleTimeString('taiwan',timeOptions);
};

function tempToFloat(tempStr) {
  return parseFloat(tempStr)
};

function getNow() {
    var now = new Date()
  return new Date(now.setSeconds(0,0))
};

function DclogController(_,$scope, Global, $stateParams, $window, $http, srchDCOpt, filterOptionService) {
    $scope.selected = {
        "from": getToday(),
        "to": getNow(),
        "oven": null
    };

    $scope.fields = srchDCOpt.fields;
    $scope.fieldSearchURL = "/api/dclog/getsearchfield";
    $scope.varsSeleted = {
        onSelected: null,
        varsList: [],
        tabList:[]
    };
    $scope.zoneBoardSeleted = {
        onSelected: null,
        zoneNo: null,
        boradNo:null
    };

    debugDC=$scope;
    
    $scope.showTemp = true;
    $scope.testType = {name:"vars"};
    $scope.setShowTemp = function(isShowTemp){
        $scope.dclogInfoGrid.columnApi.setColumnsVisible(['Left_Oven','Right_Oven'],isShowTemp)
    } 
    $scope.setTestType=function(testType){
        if($scope.testType.name != testType.name)
            $scope.testType.name = testType.name;

        var tempVarsList =[];
        var varsList = getVarsList($scope.filterOption)
        if( varsList.length > 0){
            _.forEach(varsList,v => tempVarsList.push({'id': v, 'name':v}))
            $scope.varsSeleted.varsList=tempVarsList;
        }
        
        if($scope.testType.name=='vars'){
            $scope.varsSeleted.tabList=tempVarsList;
        }
        else{
             var zbList = ($scope.getSeletedZB()).ZBLIST
            if( zbList.length > 0){
                var tempList =[]
                _.forEach(zbList,zb => tempList.push({'id': zb, 'name':zb}))
                $scope.varsSeleted.tabList=tempList
            }           
        }
    }
    $scope.selectedZone = {zone:[]};
    $scope.zoneOption = [0,1,2,3];

    $scope.selectedBoard0 = {board:[]};
    $scope.selectedBoard1 = {board:[]};
    $scope.selectedBoard2 = {board:[]};
    $scope.selectedBoard3 = {board:[]};    
    $scope.boardOption0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13];
    $scope.boardOption1 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13];
    $scope.boardOption2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13];
    $scope.boardOption3 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13];
    
    $scope.defaultColumnDefs = [
        {headerName: "Log Time", field: "DATE", width: 150},
        {headerName: "Oven ID", field: "OVENID", width: 100},
        {headerName: "Left Oven", field: "Left_Oven", width: 70},
        {headerName: "Right Oven", field: "Right_Oven", width: 70}
    ],
    $scope.dclogInfoGrid = {
        columnDefs:  [],
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        //groupSuppressAutoColumn: true,
        //suppressAggFuncInHeader: true,
    };

    $scope.requestStatus = "";  // http Request 當下狀態
    
    $scope.fields = srchDCOpt.fields;
    $scope.disableSubField = true;
    $scope.checkMainFiled = function() {
        var queryFiled = _.pick($scope.selected, ['from', 'to', 'oven']);
        if (_.isNull(queryFiled.from) || _.isNull(queryFiled.to) || _.isUndefined(queryFiled.OVENID)) {
            $scope.disableSubField = false;
        } else {
            $scope.disableSubField = true;
        }
    };

    $scope.ok = function() {
        $scope.queryTime = "";
        $scope.rowNum = 0;
        var startTime = new Date();
        $scope.setTestType($scope.testType);
        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        if (_.isNull(queryParam.from) || _.isNull(queryParam.to)){
            $window.alert("請設定日期");
            return 0;
        }
        if( $scope.isFilterPanelVisible)
            $scope.toggleFilterPanelShow();

        // if(!isOldDatetime(queryParam.from)){
             queryParam.from.setHours(queryParam.from.getHours() + 8);
        // }

        // if(!isOldDatetime(queryParam.to)){
             queryParam.to.setHours(queryParam.to.getHours() + 8);
        // }

         queryParam.from = queryParam.from.toISOString();
         queryParam.to = queryParam.to.toISOString();

        _.assign(queryParam,$scope.getSeletedZB());
        if($scope.varsSeleted.varsList.length==0){
            $window.alert("請選擇參數");
            return 0;
        }
        else if(_.isNull($scope.varsSeleted.onSelected)){ // init search field for first time
            $scope.setTempVartab($scope.varsSeleted.tabList[0],false)
            // $scope.varsSeleted.onSelected = $scope.varsSeleted.varsList[0].name
        }
        _.assign(queryParam,$scope.getSeletedVars());
        _.assign(queryParam, srchDCOpt.getMatch($scope.fields));

        if (_.isUndefined(queryParam.OVENID)){
            $window.alert("請設定爐號");
            return 0;
        }

        var config = {
            eventHandlers: {
                progress: function(event){
                    // 當 Request 脫離 pending 狀態並且開始回傳資料時會觸發此 func
                    $scope.requestStatus = "正在下載資料...";
                },
            }
        };
        $scope.requestStatus = "等待 Server 回應...";
        $scope.isSearching = true;
        console.log(queryParam)
        $http.post('/api/dclog/getsearchresult', queryParam,config)
            .then(function(result){
                $scope.requestStatus = "處理回傳資料...";
                $scope.currentTabName = queryParam.VARS;
                $scope.rows = result.data.data;
                if($scope.rows!=undefined){
                    $scope.rowNum = $scope.rows.length;
                    // 轉換時間格式 (因 ag-grid export 時不會使用 cellRenderer)
                    _.forEach($scope.rows, function(row){
                        // if (_.isDate(row['DATE'])){
                          _.update(row, 'DATE', strToLocalDate);
                        // }
                        _.update(row,'Left_Oven',tempToFloat);
                        _.update(row,'Right_Oven',tempToFloat);

                    });


                    $scope.boardList = result.data.boardList;
                    $scope.dclogInfoGrid.api.setColumnDefs(genColumnDefs($scope.boardList, _.cloneDeep($scope.defaultColumnDefs)));
                    $scope.setShowTemp($scope.showTemp);
                    $scope.dclogInfoGrid.api.setRowData($scope.rows);
                }
                else{
                    $scope.rows =[];
                    $scope.rowNum = $scope.rows.length;
                    $scope.dclogInfoGrid.api.setRowData([]);
                }
                $scope.requestStatus = "查詢完成";
                $scope.queryTime = new Date() - startTime;
            }, function(err){
                $window.alert("Server failed...");
                $scope.requestStatus = '查詢失敗, '+err.data;
            }).finally(function(){
                $scope.isSearching = false;
            });
    };

    $scope.exportAllXLSX = function(){

        $scope.setTestType($scope.testType);
        var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));

        queryParam.from.setHours(queryParam.from.getHours() + 8);
        queryParam.to.setHours(queryParam.to.getHours() + 8);

        _.assign(queryParam, srchDCOpt.getMatch($scope.fields));
        _.assign(queryParam,$scope.getSeletedZB());
        _.assign(queryParam,$scope.getSeletedVars());      
        _.assign(queryParam, srchDCOpt.getMatch());

        $http.post('/api/dclog/downloadallxls'
            ,queryParam
             ,{responseType: 'arraybuffer'}
        )
            .then(function(result){
                // console.log(result)
                var headers = result.headers();
                var filename = 'DC_log.xlsx';
                var contentType = headers['content-type'];
                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([result.data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);

                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", filename);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    // setTimeout(function () {
                    linkElement.dispatchEvent(clickEvent);
                    // },10000)
                } catch (ex) {
                    console.log(ex);
                }
        }, function(err){
            $window.alert("Server failed...");
        });
    };

    var getVarsList = function(filterOpt){
    var selectedList=[]
    _.forOwn(filterOpt,function(group){
        selectedList = _.concat(selectedList, _.flattenDeep(group))

    })
    // console.log(_.filter(selectedList,function(v){return v.checked; }))
    return _.map(_.filter(selectedList,function(v){return v.checked }),'name')
}



var isOldDatetime = function(dateObj){
    var newParserStartupTime = new Date(2017,8,28,15,28,15);
    return (dateObj<newParserStartupTime)
}

var genColumnDefs = function(list, defs){
    _.forEach(list, function(element){
        defs.push({ headerName: element, field: element, width: 100});
    });
    return defs;
}

    $scope.getSeletedZB = function(){
        var record = {}

        var isAllZone =($scope.selectedZone.zone.length==0)?true:false; 
        record.isAllZone = isAllZone?true:false; 
        record.ZONE = isAllZone?$scope.zoneOption:$scope.selectedZone.zone;

        record.BOARD0 = isAllZone? $scope.boardOption0:
                        (_.indexOf($scope.selectedZone.zone,0)>=0)?
                            ($scope.selectedBoard0.board.length>0?$scope.selectedBoard0.board:$scope.boardOption0):[]
        record.BOARD1 = isAllZone? $scope.boardOption1:
                        (_.indexOf($scope.selectedZone.zone,1)>=0)?
                            ($scope.selectedBoard1.board.length>0?$scope.selectedBoard1.board:$scope.boardOption1):[]
        record.BOARD2 = isAllZone? $scope.boardOption2:
                        (_.indexOf($scope.selectedZone.zone,2)>=0)?
                            ($scope.selectedBoard2.board.length>0?$scope.selectedBoard2.board:$scope.boardOption2):[]
        record.BOARD3 = isAllZone? $scope.boardOption3:
                        (_.indexOf($scope.selectedZone.zone,3)>=0)?
                            ($scope.selectedBoard3.board.length>0?$scope.selectedBoard3.board:$scope.boardOption3):[]

        record.BOARD = isAllZone? $scope.boardOption0: _.uniq(_.concat(record.BOARD0,record.BOARD1,record.BOARD2,record.BOARD3))

        record.ZBLIST=[]
        _.forEach(record.ZONE.sort(function(a,b){return a-b}), z => 
            _.forEach(record['BOARD'+z].sort(function(a,b){return a-b}), 
                        b=> record.ZBLIST.push("Z"+z+"B"+b)))  

        if( $scope.testType.name=='vars' || $scope.varsSeleted.onSelected ==null){
            record.CURZONE=record.ZBLIST[0][1];
            record.CURBOARD=record.ZBLIST[0].slice(3);
        }
        else{
            record.CURZONE=$scope.varsSeleted.onSelected[1];
            record.CURBOARD=$scope.varsSeleted.onSelected.slice(3);
        }
        return record;
        
    }
    $scope.getSeletedVars = function(){
        var record = {};
        
        record.VARS = $scope.varsSeleted.onSelected;
        record.VARSLIST = _.map($scope.varsSeleted.varsList,'name');
        record.testType = $scope.testType.name;
        record.showTemp = $scope.showTemp;
        
        return record;
    }

    $scope.setTempVartab = function(tab,isSearch){
        var preTabName = '#umtab #'+$scope.varsSeleted.onSelected;
        var curTabName = '#umtab #'+tab.name;
        //$(preTabName).attr('class','inactive');
        $scope.currentTab = tab;
        $scope.currentTabName = tab.name;
        $scope.varsSeleted.onSelected =tab.name;
        //$(curTabName).attr('class','active');
        // changeTabClass(preTabName,curTabName);
        if(isSearch)
            $scope.ok();
    }
    var changeTabClass= function(preTabIndex,curTabIndex){
        // console.log(preTabIndex,curTabIndex)
        angular.element(preTabIndex).attr('class','inactive');
        angular.element(preTabIndex).removeClass('ng-scope');

        angular.element(curTabIndex).attr('class','active');
        angular.element(curTabIndex).removeClass('ng-scope');
    }
    // filter panel
    $scope.filterOption = filterOptionService.filterOption;
    $scope.isFilterPanelVisible = false;
    $scope.toggleFilterPanelShow = function() {
        $scope.isFilterPanelVisible = !($scope.isFilterPanelVisible);
 
        $scope.setTestType($scope.testType);
    };

}




angular
    .module('mean.dclog')
    .controller('DclogController', DclogController);

DclogController.$inject = ['_','$scope', 'Global', '$stateParams', '$window', '$http', 'srchDCOpt', 'filterOptionService'];
