(function(){
    'use strict';
    function srchDCOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("機台編號", "OVENID"),        
            ],
            getMatch: uOpt.getMatch
        };
    };

    angular.module('mean.dclog').factory('srchDCOpt', srchDCOpt);
    srchDCOpt.$inject = ['utilsSrchOpt'];
})();