(function() {
    'use strict';

    function Dclog($http, $q) {
        return {
            name: 'dclog',
            checkCircle: function(circle) {
                var deferred = $q.defer();

                $http.get('/api/dclog/example/' + circle).success(function(response) {
                    deferred.resolve(response);
                }).error(function(response) {
                    deferred.reject(response);
                });
                return deferred.promise;

            }
        };
    }

    angular
        .module('mean.dclog')
        .factory('Dclog', Dclog);

    Dclog.$inject = ['$http', '$q'];

})();
