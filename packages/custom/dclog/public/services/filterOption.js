(function() {
    'use strict';

    function filterOptionService() {
		var test = '123';
        var filterOption = {
            "group1": [
				[ { "name": "VOH1", "checked": false }, { "name": "VOH2", "checked": false }, { "name": "VCLH", "checked": false }, { "name": "VCLL", "checked": false } ],
				[ { "name": "DPS1CUR", "checked": false }, { "name": "DPS2CUR", "checked": false }, { "name": "DPS3CUR", "checked": false } , { "name": "DPS4CUR", "checked": false } ],
				[ { "name": "DPS1SEN", "checked": false }, { "name": "DPS2SEN", "checked": false }, { "name": "DPS3SEN", "checked": false }, { "name": "DPS4SEN", "checked": false } ],
				[ { "name": "全選", "checked": false }]				
			],
			"group2": [
				[ { "name": "DPS1", "checked": false }, { "name": "DPS2", "checked": false }, { "name": "DPS3", "checked": false }, { "name": "DPS4", "checked": false } ],
				[ { "name": "DPS1EXP", "checked": false }, { "name": "DPS2EXP", "checked": false }, { "name": "DPS3EXP", "checked": false }, { "name": "DPS4EXP", "checked": false } ],
				[ { "name": "DPS1LIM", "checked": false }, { "name": "DPS2LIM", "checked": false }, { "name": "DPS3LIM", "checked": false }, { "name": "DPS4LIM", "checked": false } ],
				[ { "name": "VDPS1", "checked": false }, { "name": "VDPS2", "checked": false }, { "name": "VDPS3", "checked": false }, { "name": "VDPS4", "checked": false } ],
				[ { "name": "VVOH1", "checked": false }, { "name": "VVOH2", "checked": false } ],
				[ { "name": "VVCLH", "checked": false }, { "name": "VVCLL", "checked": false } ],
				[ { "name": "VCMP1", "checked": false }, { "name": "VCMP2", "checked": false } ],
				[ { "name": "VVCMP1", "checked": false }, { "name": "VVCMP2", "checked": false } ],
				[ { "name": "VOHL", "checked": false } ],
				[ { "name": "VVOHL", "checked": false } ],
				[ { "name": "OVP", "checked": false }, { "name": "OCP", "checked": false } ],
				[ { "name": "reverse", "checked": false }]
			]
        };

        return {
            filterOption: filterOption
        }
    }

    angular
        .module('mean.dclog')
        .factory('filterOptionService', filterOptionService);

    filterOptionService.$inject = [];

})();
