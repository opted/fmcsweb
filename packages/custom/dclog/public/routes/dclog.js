(function() {
    'use strict';

    function Dclog($stateProvider) {
        $stateProvider.state('dclog example page', {
            url: '/dclog/search',
            templateUrl: 'dclog/views/index.html',
            controller:DclogController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              }
            }
        });
    }

    angular
        .module('mean.dclog')
        .config(Dclog);

    Dclog.$inject = ['$stateProvider'];

})();
