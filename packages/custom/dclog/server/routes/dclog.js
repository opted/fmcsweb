(function() {
    'use strict';

    /* jshint -W098 */
    module.exports = function(Dclog, app, auth, database,utils) {
        // var dclogQueryCtr = require('../controllers/dclogQuery.js');
        var DclogManager = require('../controllers/dclogQuery');
        var dclogMgr = new DclogManager(utils)
        var dfile = require('../controllers/dfile.js');
        var rfile = require('../controllers/rfile.js');
        app.post('/api/dclog/getsearchfield',  function(req, res, next){
        utils.ctrls.mongoQuery.getSearchField(dclogMgr.coll, req.body, dclogMgr.pathMapping,[])
        .then((data) => res.json(data))
        .catch((err) => res.status(500).send({'error': err}));
      
    }); 
        app.post('/api/dclog/getsearchresult', function(req, res, next){
            dclogMgr.getSearchResult(req.body)
                .then((data) => res.json(data))
                .catch((err) => res.status(500).send({'error': err}));
        })
        
        app.post('/api/dclog/downloadxls', dfile.downloadExcel);
        app.post('/api/dclog/downloadallxls', function(req, res, next){
            dfile.downloadExcelpython(req,res,utils)
        })
        // app.post('/api/dclog/readxls',rfile.readExcel);
    };
})();
