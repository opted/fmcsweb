'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash'),
    fs = require('fs'),
    xl = require('excel4node'),
    XLSX = require('xlsx-style'),
    Q = require('q');

exports.readExcel = function(req, res, next) {


    // Generate export excel file
    var path = "C:/Users/operationcow/Documents/biis/biweb_phase2/tmp/"
    var excelFileName = path+"dc.xlsx" , modifyExcelFileName= path+"dc1.xlsx";
    var wb = XLSX.readFile(excelFileName);

    var sh = wb.SheetNames[0]
    wb.Sheets[sh]['!ref'] ="A1:L200"
    // fs.readFile(excelFileName,  function (err, data) {
    //     if (true){
    //         res.send(wb);
    //     }
    // });
    //wb.write(excelFileName, res);
    XLSX.writeFile(wb, modifyExcelFileName);

    if (fs.existsSync(modifyExcelFileName)){
          res.download(modifyExcelFileName);
        // res.send(wb)

    }



};



function createExcelBinary (isBase64Format, callBack){
    var xlsxPath = path.join('/tmp/dc.xlsx');
    fs.readFile(xlsxPath,  function (err, data) {
        if (isBase64Format){
            callBack("data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,"+data.toString('base64'));
        } else {
            callBack(data);
        }
    });
}