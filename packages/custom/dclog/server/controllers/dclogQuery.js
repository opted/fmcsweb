'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var DCLOG = mongoose.model('DCLOG');
var Q = require('q');

function transformRegexStr(str, isStrict){
    str = str.replace(/\+/g, '.');
    if(/.+\*$/g.test(str)){ // 'ABC*'
        return new RegExp('^'+str.slice(0, -1), 'i');
    }else if(/^\*.+/g.test(str)){ //'*abc'
        return new RegExp(str.slice(1)+"$", 'i');
    }else if(/.+\*.+/g.test(str)){ //'abc*dd'
        return new RegExp('^'+str.replace('*', '.*')+'$', 'i');
    }else{
        return new RegExp('^' + str + (isStrict ? '$' : '') , 'i');
    }
}

// exports.getSearchField = function(req, res, next) {
//     try {
//         var params = req.body;
//         var field = params.field;
//         var match = _.assign({}, genSubQuery(params.match, false));
//         console.log(params)
//         var regField = getMatchDocPath(field);
//         match[regField] = transformRegexStr(params.regex, false);
//         // console.log(regField)
//         var pipe = [];
//
//         pipe.push({'$match': match});
//         pipe.push({'$group': {'_id': '$' + getMatchDocPath(field)}});
//         pipe.push({'$limit': 20});
//         pipe.push({'$project': {'_id': 1}});
//
//         var results = [];
//         var cr = DCLOG.aggregate(pipe).cursor({batchSize: 1000}).allowDiskUse(true).exec();
//         cr.on('data', function(r){results.push(r._id);})
//           .on('end', function(){res.json(results);})
//           .on('error', function(e){res.status(500).send({'error': e});});
//
//     } catch (err) {
//         console.log(err.stack);
//         res.status(500).send({'error': err.stack});
//     }
// };
//
// exports.getSearchResult = function(req, res, next){
//     var deferred = Q.defer();
//     var isBackend= !_.has(req, 'body');
//     var query = isBackend ? req : req.body;
//     try {
//         console.log('isBackend: ',isBackend)
//         console.log('query: ', query)
//         var vars = query.VARS|| 'VOH1'//query.VARS,
//         var match = {
//             'DATETIME': {
//                 '$gte': new Date(query.from),
//                 '$lt': new Date(query.to)
//             },
//             "OVENID": _.toUpper(query.OVENID) || 'OC-03'//_.toUpper(query.OVENID),
//         };
//
//         var pipe = [{'$match': match}];
//         pipe.push({'$unwind': "$ZONES"});
//         pipe.push({'$unwind': "$ZONES.BOARDS"});
//         pipe.push({'$match': genSubZoneBoardQuery(_.pick(query, ['ZONE', 'BOARD']))});
//         pipe.push({'$group':
//                         { '_id': { 'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }}
//                             ,'OVEN': "$OVENID"},
//                             'OVENDATA':{ '$push': {'VARS': "$ZONES.BOARDS." + vars,
//                                                                 'ZONE':"$ZONES.ZONE",
//                                                                 'BOARD':"$ZONES.BOARDS.BOARD"}},
//                             'Right_Oven':{'$first':"$Right_Oven"},
//                             'Left_Oven':{'$first':"$Left_Oven"}
//                         }
//     })
//         pipe.push({'$project': {
//             // '_id': 0,
//              'DATETIME':  "$_id.DATETIME" ,
//             'OVENID':  "$_id.OVEN",
//             'Right_Oven': '$Right_Oven',
//             'Left_Oven': '$Left_Oven',
//             'OVENDATA' : 1
//
//         }});
//          pipe.push({'$sort': {"DATETIME": 1}});
//
//
//         var results = [];
//         var cr = DCLOG.aggregate(pipe).cursor({batchSize: 1000}).allowDiskUse(true).exec();
//         cr.on('data', function(r){
//             try {
//                 results.push(r);
//             } catch (err) {
//                 deferred.reject(err.stack);
//             }
//         });
//
//         cr.on('end', function(){
//                 try {
//                     var ovenBoardList = [];
//                     var resultObj={}
//                     _.forEach(results,function(ovenInfo,idx){
//                         _.forEach(ovenInfo.OVENDATA,function(zoneBoard){
//                                     var boardNo = 'Zone'+zoneBoard.ZONE + 'Board'+zoneBoard.BOARD
//                                     ovenBoardList.push(boardNo)
//                                     ovenInfo[boardNo] = zoneBoard.VARS
//                                 })
//                         if(idx == (results.length -1)){
//                             resultObj = {'data': results , 'boardList':_.uniq(ovenBoardList)}
//                         }
//                     })
//                     deferred.resolve(resultObj)
//                 } catch (endErr) {
//                     deferred.reject(endErr.stack);
//                 }
//             })
//             .on('error', function(streamErr){
//                 deferred.reject(streamErr);
//             });
//
//     } catch (err) {
//         deferred.reject(err.stack);
//     }
//
//    return  deferred.promise.then(
//         function(result){
//             if(isBackend){
//                 return result;
//             }
//             else
//                 res.json(result);
//         },
//         function(err){
//             console.log(err);
//             res.status(500).send({'error': err});
//         }
//     );
// };

function getMatchDocPath(field){
    var match = {
        'OVENID': 'OVENID',
        'ZONE': 'ZONES.ZONE',
        'BOARD': 'ZONES.BOARDS.BOARD',
        'CURZONE': 'ZONES.ZONE',
        'CURBOARD': 'ZONES.BOARDS.BOARD',
    };
    return match[field];
}

function genSubQuery(query,isStrict){
    var subQ = {};
    _.forEach(query, function(v, k){
        if (_.isString(v) && !/^\*$/g.test(v)){
            var doc_path = getMatchDocPath(k);
            if (_.isString(doc_path)){
                subQ[doc_path] = transformRegexStr(v, isStrict);
            }
        }
    });
    return subQ;
}




class DclogManager {
    constructor(utils) {
        this.coll = DCLOG;
        this.utils = utils;
        this.pathMapping = {
            'OVENID': 'OVENID'
        };
        this.arrayFields = ['ZONES', 'BOARDS'];
    }

    getSearchResult (params,vars) {
        var df = Q.defer();
        try {
            var query = params;
            var varName = (vars)? vars :query.VARS;
            var varsList=query.VARSLIST||[];
            var match = {  "OVENID": _.toUpper(query.OVENID) };
            var dateRange = {'DATETIME': {}};
            if (query.from !== null){
                dateRange.DATETIME['$gte'] = new Date(query.from);
            }
            if (query.to !== null){
                dateRange.DATETIME['$lt'] = new Date(query.to);
            }

            if (!_.isEmpty(dateRange.DATETIME)){
                // console.log(dateRange.DATETIME)
                _.assign(match, dateRange);
            }
            // console.log(query)
            //_.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, ['SITENO', 'CUSTOMERLOTNO', 'ECID', 'LOTNO']), this.pathMapping));

            // var pipe = [{'$match': match}];

            var pipe = [{'$match': match}];
            pipe.push({'$unwind': "$ZONES"});
            pipe.push({'$unwind': "$ZONES.BOARDS"});
            // console.log(genSubZoneBoardQuery(_.pick(query, ['CURZONE', 'CURBOARD'])))
            if(query.isAllZone==false){
                if(query.testType=='vars')
                    pipe.push({'$match': genSubZoneBoardQuery(_.pick(query, ['ZONE', 'BOARD']))});
                else
                    pipe.push({'$match': genSubZoneBoardQuery(_.pick(query, ['CURZONE', 'CURBOARD']))});                    
            }

            if(query.testType=='vars') {
                pipe.push({'$group':
                { '_id': { 'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }}
                    ,'OVEN': "$OVENID"},
                    'OVENDATA':{ '$push': {'VARS': "$ZONES.BOARDS." + varName,
                        'ZONE':"$ZONES.ZONE",
                        'BOARD':"$ZONES.BOARDS.BOARD"}},
                    'Right_Oven':{'$first':"$Right_Oven"},
                    'Left_Oven':{'$first':"$Left_Oven"},
                    'DATE':{'$first':"$DATETIME"}
                }
                })
                pipe.push({'$project': {
                    'DATETIME':  "$_id.DATETIME" ,
                    'OVENID':  "$_id.OVEN",
                    'Right_Oven': '$Right_Oven',
                    'Left_Oven': '$Left_Oven',
                    'DATE': '$DATE',
                    'OVENDATA' : 1
                }});
                pipe.push({'$sort': {"DATETIME": 1}});
            }
            else {
                var groupObj = { 
                        '_id': { 'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }}
                        ,'OVEN': "$OVENID"},
                        'OVENDATA':{ '$push': {
                            'ZONE':"$ZONES.ZONE",
                            'BOARD':"$ZONES.BOARDS.BOARD"}},
                        'Right_Oven':{'$first':"$Right_Oven"},
                        'Left_Oven':{'$first':"$Left_Oven"},
                        'DATE':{'$first':"$DATETIME"}
                    }
                //filter vars 
                _.forEach(varsList,function(key){
                    groupObj['OVENDATA']['$push'][key]= "$ZONES.BOARDS."+key
                })
                pipe.push({'$group':groupObj})
                
                var projectObj = {
                    'DATETIME':  "$_id.DATETIME" ,
                    'OVENID':  "$_id.OVEN",
                    'Right_Oven': "$Right_Oven",
                    'Left_Oven': "$Left_Oven",
                    'DATE':1,
                    'OVENDATA':1
                }

                pipe.push({'$project': projectObj});
                pipe.push({'$sort': {"DATETIME": 1}});   

            }

            var doQuery = this.utils.ctrls.mongoQuery.massiveQuery(DCLOG, pipe)
                .then(function(result){

                    try {
                        var ovenBoardList = [];
                        var resultObj={}
                        _.forEach(result,function(ovenInfo,idx){
                            _.forEach(ovenInfo.OVENDATA,function(zoneBoard){
                                if(query.testType=='vars'){
                                    var boardNo = 'Zone'+zoneBoard.ZONE + 'Board'+zoneBoard.BOARD
                                    if(_.indexOf(query['BOARD'+zoneBoard.ZONE],zoneBoard.BOARD)>=0
                                        && _.indexOf(query['ZONE'],zoneBoard.ZONE)>=0){
                                        if(idx==0) ovenBoardList.push(boardNo);
                                        ovenInfo[boardNo] = zoneBoard.VARS
                                    }
                                }
                                else {
                                    _.forEach(query.VARSLIST,function(vName){
                                       if(idx==0) ovenBoardList.push(vName);
                                       ovenInfo[vName] =  zoneBoard[vName]
                                    }) 
                                }
                            })
                            if(idx == (result.length -1)){
                                resultObj = {'data':  _.map(result,function(ovenInfo){return _.omit(ovenInfo,['_id','OVENDATA'])}) ,
                                                  'boardList':_.uniq(ovenBoardList),
                                                  'vars':varName}
                            }
                        })
                        df.resolve(resultObj)
                    } catch (endErr) {
                        df.reject(endErr.stack);
                    }
                });
        } catch (err){
            df.reject(err.stack);
        }

        return df.promise;
    };
}
function genSubZoneBoardQuery(query){
    var subQ = {};
    _.forEach(query, function(v, k){
        if (_.isString(v) && !/^\*$/g.test(v)){
            var doc_path = getMatchDocPath(k);
            if (_.isString(doc_path)){
                subQ[doc_path] = parseInt(v);
            }
        }
        else if(_.isArray(v) && v.length>0){
            var doc_path = getMatchDocPath(k);
            if (_.isString(doc_path)){
                subQ[doc_path] = {'$in': v}
            }
        }

    });
    return subQ;
}

module.exports = DclogManager;