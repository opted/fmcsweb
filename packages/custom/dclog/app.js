'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Dclog = new Module('dclog');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Dclog.register(function(app, auth, database, utils) {

  //We enable routing. By default the Package Object is passed to the routes
  Dclog.routes(app, auth, database, utils);

  //We are adding a link to the main menu for all authenticated users
  // Dclog.menus.add({
  //   title: 'dclog example page',
  //   link: 'dclog example page',
  //   roles: ['authenticated'],
  //   menu: 'main'
  // });
  //Dclog.aggregateAsset('js', '../js/amcharts/amcharts.js', {weight: 1});
  //Dclog.aggregateAsset('js', '../js/amcharts/serial.js', {weight: 2});
  
  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Dclog.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Dclog.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Dclog.settings(function(err, settings) {
        //you now have the settings object
    });
    */
  Dclog.angularDependencies(['agGrid']);
  return Dclog;
});
