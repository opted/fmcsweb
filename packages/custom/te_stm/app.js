'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Te_stm = new Module('te_stm');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Te_stm.register(function(app, auth, database, utils) {
  //We enable routing. By default the Package Object is passed to the routes
  Te_stm.routes(app, auth, database, utils);
  Te_stm.aggregateAsset('css', 'te_stm.css');
  Te_stm.angularDependencies(['agGrid']);
  return Te_stm;
});
