'use strict';

function Te_stmController(_, $scope, $http, srchTESTMOpt, download) {
  $scope.packageName = 'TE_STM BinMap+ColumnFail';
  $scope.selected = {
    "from": getToday(),
    "to":  getToday()
  };
  $scope.selectedToQuery = {}
  $scope.$watch('selected.from', date => {
    $scope.selectedToQuery['BI_START_TIME'] = {'$gte': date};
  });
  $scope.$watch('selected.to', date => {
    $scope.selectedToQuery['BI_END_TIME'] = {'$lte': date};
  });

  $scope.fieldSearchURL = "/api/te_stm/getSearchField";
  $scope.fields = srchTESTMOpt.fields;
  $scope.gridOption = download.gridOption;

  $scope.queryData = function() {
    $scope.rowNum = false;
    var queryParam = _.cloneDeep(_.pick($scope.selected, ['from', 'to']));
    _.assign(queryParam, srchTESTMOpt.getMatch($scope.fields));
    //DEBUG
    /*if (queryParam.OVENID === undefined) {
      queryParam.from = new Date( 2017, 10, 1);
      queryParam.to = new Date( 2017, 10, 30);
      //queryParam.OVENID = 'OH-09';
      queryParam.CUSTOMER_BOARD_ID = '0B900083';
      //queryParam.LOTNO = 'LHA0GPSAD1';
    };*/
    console.log(queryParam);

    var isSelectTime = queryParam.from && queryParam.to;
    if (queryParam.LOTNO === undefined && !isSelectTime) {
      $scope.isSearching = false;
      $scope.requestStatus = '查詢失敗, 查詢條件錯誤';
      alert("LOTNO/時間區間 至少需要輸入一個欄位");
      return;
    };

    var startTime = new Date();
    $scope.queryTime = "";
    $scope.isSearching = true;
    $scope.requestStatus = "等待 Server 回應...";
    queryParam.from = new Date(queryParam.from.getTime() + 8*60*60*1000 );
    queryParam.to = new Date(queryParam.to.getTime() + 8*60*60*1000 );
    $http.post('/api/te_stm/getSearchResult', queryParam)
      .then(function(res){
        $scope.isSearching = false;
        var sortedData = _.sortBy(res.data, 'KYEC_BOARD_ID')
        console.log(sortedData);
        $scope.requestStatus = '查詢完成';
        download.setOriginData(sortedData)
        $scope.rowNum = sortedData.length;
        if ($scope.rowNum === 0|| _.uniq(_.map(sortedData, 'XSOCK')).length > 1 || _.uniq(_.map(sortedData, 'YSOCK')).length > 1){
          $scope.socketMapData = false;
          $scope.columnFailData = false; 
        } else {
          $scope.socketMapData = sortedData;
          var columnFailData = _.filter(sortedData, function(x){
            if (x.FLOW_NAME === 'SUMMARY' && x.COLUMN_FAIL.length > 0) {
              return true;
            };
            return false;
          });
          $scope.columnFailData = _.groupBy(columnFailData, 'KYEC_BOARD_ID');
          $scope.columnFailDataNum = _.keys($scope.columnFailData).length;
        };
        $scope.avgData = [];
        _.forEach(_.groupBy(sortedData, 'KYEC_BOARD_ID'), function(groupData, groupKey){
          var tmp = {};
          tmp.PASS = _.sumBy(_.map(groupData, 'PASS'));
          tmp.FAIL = _.sumBy(_.map(groupData, 'FAIL'));
          tmp.YIELD = _.meanBy(groupData, 'YIELD')*100;
          tmp.KYEC_BOARD_ID = groupKey;
          $scope.avgData.push(tmp);
        });
        $scope.queryTime = new Date - startTime;
      },function(err){
        $scope.isSearching = false;
        $scope.requestStatus = '查詢失敗, '+err.data
      });
  };

  //DEBUG
  //$scope.queryData()

  $scope.exportXLS = function(){
    var params = {
      fileName: 'STM_Raw.xls',
      sheetName: 'KYEC_BIIS'
    };
    $scope.gridOption.api.exportDataAsExcel(params);
  }
};

angular
  .module('mean.te_stm')
  .controller('Te_stmController', Te_stmController);

Te_stmController.$inject = ['_', '$scope', '$http', 'srchTESTMOpt', 'download'];
