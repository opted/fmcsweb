(function(){
  'use strict';
  function socketMap(_){
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        binfo: '=',
        rawdata: '='
      },
      templateUrl: 'te_stm/views/socketMap.html',
      link: function(scope, element, attrs){
        scope.socketMaps = [];
        scope.defaultColorSet = [
          { min: -1, max: -1, color: '#FFFFFF'},
          { min: 0, max: 0, color: '#3CB15C'},
          { min: 1, max: 1, color: '#FF7763'},
          { min: 2, max: 10, color: '#FF2C20'},
          { min: 11, max: 'max', color: '#993539'},
        ];
        scope.colorSet = _.cloneDeep(scope.defaultColorSet);
        scope.$watch('rawdata', function(newVal, oldVal){
          if (_.isEmpty(scope.rawdata)){
            return;
          };
          scope.socketMaps = [];
          var xsock = scope.rawdata[0].XSOCK;
          var ysock = scope.rawdata[0].YSOCK;
          var maps = [];
          _.forEach(_.filter(scope.rawdata, {'FLOW_NAME': 'SUMMARY'}), function(data){
            var map = [];
            _.forEach(_.range(0,ysock), function(index){
              map.push([])
            })
            var i = 0;
            var j = ysock;
            while(j <= data.BINMAP.length) {
              var sliceData = data.BINMAP.slice(i,j);
              _.forEach(sliceData, function(val, index){
                map[index].unshift(val);
              });
              i += ysock;
              j += ysock;
            };
            maps.push(map)
          });
          var sumMap = [];
          _.forEach(_.range(0,ysock), function(yIndex){
            var tmpRow = [];
            _.forEach(_.range(0,xsock), function(xIndex){
              var count = _.countBy(_.map(_.map(maps, yIndex), xIndex));              
              tmpRow.push({'FAIL': count['2']||0, 'PASS': count['1']||0 })
            });
            sumMap.push(tmpRow);
          });
          scope.socketMaps = sumMap;
        }, true);

        scope.btnStyle = function(socket){
          var style = {};
          var emptyColor = _.find(scope.colorSet, {min:-1, max:-1})
          if (socket.FAIL === 0 && socket.PASS === 0) {
            style["background-color"] = emptyColor.color;
            return style;
          }
          _.forEach(scope.colorSet, function(config){
            if ( config.min === 'N/A' || config.max === 'N/A') {
              return;
            };
            if (socket.FAIL >= config.min && config.max === 'max') {
              style["background-color"] = config.color;
              return false;
            } else if (socket.FAIL >= config.min && socket.FAIL <= config.max ) {
              style["background-color"] = config.color;
              return false;
            }
          });
          return style;
        }

        scope.exportSocketMap = function(id){
          html2canvas($("#socketmap-" + id), {
            background: "#ffffff",
            onrendered: function(canvas) {
              var url = canvas.toDataURL();
              $("<a>", {
                href: url,
                download: "socketmap.png"
              })
              .on("click", () => $(this).remove())
              .appendTo("body")[0].click()
            }
          });
        }
        scope.addColorRange = function(){
          scope.colorSet.push({min: 'N/A', max: 'N/A', color: '#000000'})
        };
        scope.recoveryColor = function(){
          console.log('reset') ;
          scope.colorSet = _.cloneDeep(scope.defaultColorSet);
        };
      },
    };
  };
  angular.module('mean.te_stm').directive('socketMap', socketMap);
  socketMap.$inject = ['_'];
})();
         
