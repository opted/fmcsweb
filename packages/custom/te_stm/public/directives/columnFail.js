(function(){
  'use strict';
  function columnFail(_, $http, download){
    return {
      restrict: 'E',
      transclude: true,
      scope: {
        rawdata: '='
      },
      templateUrl: 'te_stm/views/columnFail.html',
      link: function(scope, element, attrs){
        scope.$watch('rawdata', function(newVal, oldVal){
          scope.columnFails = [];
          if (_.isEmpty(scope.rawdata)) {
            return false;
          };
          if (!_.isArray(scope.rawdata)) {
            scope.rawdata = [scope.rawdata];
          };
          var xsock = scope.rawdata[0].XSOCK;
          scope.kyecBID = scope.rawdata[0].KYEC_BOARD_ID;
          scope.customerBID = scope.rawdata[0].CUSTOMER_BOARD_ID.replace(/([0-9A-Z]{4})([0-9A-Z]{4})/g, '$1-$2');
          scope.dynamicWidth = xsock*50 + 'px';
          var columnFailData = _.groupBy(_.flatten(_.map(scope.rawdata, 'COLUMN_FAIL')), 'INDEX');
          var total = scope.rawdata.length;
          _.forEach(_.range(xsock , 0), function(index){
            var failCount = 0;
            if (columnFailData[index] !== undefined) {
              failCount = columnFailData[index].length;
            };
            scope.columnFails.push({INDEX: index, FAILS: failCount, RATE: _.round(failCount/total*100, 2)});
          }); 
          scope.dynamicWidth = scope.columnFails.length*50 + 'px';

          scope.subColumnFailData = []; 
          _.forEach(_.groupBy(scope.rawdata, 'OVENID'), function(ovenGroup, ovenId){
            var lastOvenID = "";
            _.forEach(_.groupBy(ovenGroup, 'SLOT'), function(rawdata, slot){
              var columnFails = [];
              //var columnFailData = _.groupBy(rawdata.COLUMN_FAIL, 'INDEX');
              var columnFailData = _.groupBy(_.flatten(_.map(rawdata, 'COLUMN_FAIL')), 'INDEX');
              var total = rawdata.length;
              var lotnums = _.uniq( _.map(rawdata, 'LOTNO'));
              _.forEach(_.range(xsock, 0), function(index){
                var failCount = 0;
                if (columnFailData[index] !== undefined) {
                  failCount = columnFailData[index].length;
                };
                columnFails.push({INDEX: index, FAILS: failCount, LOTNO: lotnums, RATE: _.round((failCount/total)*100, 2)});
              });
              if (ovenId != lastOvenID) {
                scope.subColumnFailData.push({columnFails: columnFails, SLOT: slot, OVENID: ovenId, showOven: true});
              } else {
                scope.subColumnFailData.push({columnFails: columnFails, SLOT: slot, OVENID: ovenId});
              };         
              lastOvenID = ovenId;
            });
          });
        }, true);
        scope.btnStyle = function(fails){
          var style = {};
          if (fails > 0) {
            style["background-color"] = '#F3DBDA';
          } else {
            style["background-color"] = 'white';
          };
          return style;
        };
        scope.showLotnos = function(sub, lotnums){
          sub.showDownload = !sub.showDownload
          sub.selectedLotno = lotnums;
        };
        scope.downloadLotData = function(lot){ 
          console.log('download',lot);
          download.downloadLotData(lot);
        };
        scope.genBorder = function(index, ovenId) {
          var style = {
              'border-right': "2px solid",
              'border-left': "2px solid",
          };
          if (index === 0) {
            scope.isFirstBorder = false;
            style['border-top'] = '2px solid';
          };
          if (index === _.findLastIndex(scope.subColumnFailData, {OVENID: ovenId})) {
            style['border-bottom'] = '2px solid';
          };
          return style;
        };
      },
    };
  };
  angular.module('mean.te_stm').directive('columnFail', columnFail);
  columnFail.$inject = ['_', '$http', 'download'];
})();
         
