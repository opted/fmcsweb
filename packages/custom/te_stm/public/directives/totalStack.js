(function(){
  'use strict';
  function lineBarChart(_){
    return {
      restrict: 'E',
      scope: {
        source: '=',
      },
      template: '<div id="lineBarChart" style="min-width: 310px; height: 400px; margin: 0 auto"><div></div></div>',
      link: function (scope, element, attrs) {
        var chart = false;
        var sumFunction = function (item, text) {
          if (text === "total") {
            var sum = 0;
            _.forEach(item.dataContext, function(data, key){
              if (['PASS', 'FAIL'].indexOf(key) > -1) {
                sum += data;
              };
            });
            return sum;
          };
        };
        var genChart = function() {
          if (chart) {
            chart.destroy();
          };
          var chartOption = {
            'type': 'serial',
            'theme': 'none',
            'marginLeft': 20,
            'dataProvider': scope.source,
            'legend': {
              'horizontalGap': 10,
              'maxColumns': 10,
              'position': 'top',
              'useGraphSettings': true,
              'markerSize': 10
            },
            'valueAxes': [{
              'id': 'left',
              'stackType': 'regular',
              'axisAlpha': 0.3,
              'gridAlpha': 0.15,
              'position': 'left',
              'title': '測試數',
              'totalText': 'total',
              'integersOnly': true
            }, {
              'id': 'right',
              'axisAlpha': 0,
              'position': 'right',
              'unit': '%',
              'title': 'YIELD(%)'
            }],
            'graphs': [{
              'valueAxis':'left',
              'balloonText': "[[category]]<br><b><span style='font-size:14px;'>PASS: [[value]]</span></b>",
              'alphaField': 'alpha',
              'fillAlphas': 1,
              'type': 'column',
              'valueField': 'PASS',
              'title': 'PASS',
              'lineColor': '#3CB15C',
              'labelFunction': sumFunction
            }, {
              'valueAxis':'left',
              'balloonText': "[[category]]<br><b><span style='font-size:14px;'>FAIL: [[value]]</span></b>",
              'alphaField': 'alpha',
              'fillAlphas': 1,
              'type': 'column',
              'valueField': 'FAIL',
              'title': 'FAIL',
              'lineColor': '#FF7763',
              'labelFunction': sumFunction
            }, {
              'valueAxis':'right',
              'balloonFunction': function(item, graph) {
                return  item.category + "<br><b><span style='font-size:14px;'>Yield:" + 
                        item.values.value.toFixed(2) + "%</span></b>"
              },
              'bullet': 'round',
              'bulletSize': 7,
              'lineThickness': 3,
              'bulletBorderAlpha': 1,
              'bulletColor': "#FFFFFF",
              'useLineColorForBulletBorder': true,
              'bulletBorderThickness': 3,
              'fillAlphas': 0,
              'lineAlpha': 1,
              'title': 'YILED %',
              'valueField': 'YIELD'
            }],
            'chartScrollbar': {},
            'categoryField': 'KYEC_BOARD_ID',
            'categoryAxis': {
              'gridPosition': 'start',
              'axisAlpha': 0,
              'gridAlpha': 0.15,
              'position': 'left',
              'labelRotation': 60
            }
          };
          var config = scope.config || {};
          var target = element[0].children[0];
          chart = AmCharts.makeChart(target, chartOption); 
        }// end of genChart function
        genChart();
        scope.$watch('source', function () {
          genChart();
        });
      }//end watch
    } 
  };

  angular.module('mean.te_stm').directive('totalStack', lineBarChart);
  lineBarChart.$inject = ['_'];
})();
