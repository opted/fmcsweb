(function(){
    'use strict';
    function srchOpt(utilsSrchOpt){
        var uOpt = utilsSrchOpt;
        return {
            fields: [
                uOpt.genField("Lotnum", "LOTNO", "string", false, true),
                uOpt.genField("Family", "FAMILY", "string", false, true),
                uOpt.genField("Customer Board ID", "CUSTOMER_BOARD_ID", "string", false, true),
                uOpt.genField("Kyec Board ID", "KYEC_BOARD_ID", "string", false, true),
                uOpt.genField("Oven ID", "OVENID", "string", false, true),
                uOpt.genField("Flowname", "FLOW_NAME", "string", false, true),
            ],
            getMatch: uOpt.getMatch
        };
    };

    angular.module('mean.te_stm').factory('srchTESTMOpt', srchOpt);
    srchOpt.$inject = ['utilsSrchOpt'];
})();
