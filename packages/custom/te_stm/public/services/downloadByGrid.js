(function(){
  'use strict';
  function download(_, $http){
    var originData = [];
    var colDefs = [
      {headerName: "Family", field: "FAMILY", width: 70},
      {headerName: "customer_board_id", field: "CUSTOMER_BOARD_ID", width: 100, cellClass: "textFormat"},
      {headerName: "Slot", field: "SLOT", width: 60},
      {headerName: "kyec_board_id", field: "KYEC_BOARD_ID", width: 100, cellClass: "textFormat"},
      {headerName: "oven_ID", field: "OVENID", width: 100},
      {headerName: "lotnum", field: "LOTNO", width: 100},
      {headerName: "Bi_Start_time", field: "BI_START_TIME", width: 150,
        valueGetter: function(params){
          return params.data.BI_START_TIME.replace('.000Z','').replace('T', ' ').replace(/-/g, '/')
        }
      },
      {headerName: "Bi_End_time", field: "BI_END_TIME", width: 150,
        valueGetter: function(params){
          return params.data.BI_END_TIME.replace('.000Z','').replace('T', ' ').replace(/-/g, '/')
        }
      },
      {headerName: "flowname", field: "FLOW_NAME", width: 100},
      {headerName: "xsock", field: "XSOCK", width: 70},
      {headerName: "ysock", field: "YSOCK", width: 70},
      {headerName: "PASS", field: "PASS", width: 70},
      {headerName: "FAIL", field: "FAIL", width: 70},
      {headerName: "EMPTY", field: "EMPTY", width: 70},
      {headerName: "Total", field: "TOTAL", width: 70},
      {headerName: "Yield(%)", field: "YIELD", width: 90,
        valueGetter: function(params){
          if (_.isNumber(params.data.YIELD)){
            var tmp = params.data.YIELD*100;
            return _.round(tmp, 2);
          } else {
            return 0;
          };
        }
      },
      {headerName: "FileName", field: "FILEPATH", width: 300},
      {headerName: "BinMap", field: "BINMAP", width: 700, cellClass: "textFormat"}
    ];
  
    var gridOption = {
      columnDefs: colDefs,
      rowData:[],
      groupUseEntireRow: false,
      enableSorting: true,
      enableColResize: true,
      excelStyles: [
        {
          id: "greenBackground",
          interior: {
            color: "#aaffaa", pattern: 'Solid'
          }
        }, {
          id: "textFormat",
          dataType: "string"
        },
      ]
    };
    return {
      gridOption: gridOption,
      setOriginData: function(data){
        originData = data;
        gridOption.api.setRowData(data);
      },
      downloadLotData : function(lot) {
        $http.post('/api/te_stm/getSearchResult', {LOTNO: lot})
          .then(function(res){
            gridOption.api.setRowData(res.data);
            var params = {
              fileName: lot + ".xls",
              sheetName: 'KYEC_BIIS'
            };
            gridOption.api.exportDataAsExcel(params);
            gridOption.api.setRowData(originData);
          },function(err){
            console.log(err);
          });
      }
    };
  };
  angular.module('mean.te_stm').factory('download', download);
  download.$inject = ['_', '$http'];
})();
