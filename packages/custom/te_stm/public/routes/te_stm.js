(function() {
  'use strict';

  function Te_stm($stateProvider) {
    $stateProvider.state('te_stm main page', {
      url: '/te_stm',
      templateUrl: 'te_stm/views/index.html',
      controller:Te_stmController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.te_stm')
    .config(Te_stm);

  Te_stm.$inject = ['$stateProvider'];

})();
