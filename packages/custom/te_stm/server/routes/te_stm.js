(function() {
  'use strict';
  module.exports = function(Te_stm, app, auth, database, utils) {
    var te_stmQuery = require('../controllers/te_stmQuery');
    var mongoose = require('mongoose');
    var _ = require('lodash');

    app.post('/api/te_stm/getSearchField', function(req, res, next){
      var pathMapping = {
        'OVENID': 'OVENID',
        'LOTNO': 'LOTNO',
        'FAMILY': 'FAMILY',
        'CUSTOMER_BOARD_ID': 'CUSTOMER_BOARD_ID',
        'KYEC_BOARD_ID': 'KYEC_BOARD_ID',
        'FLOW_NAME': 'FLOW_NAME'
      };
      utils.ctrls.mongoQuery.getSearchField(mongoose.model('STM'), req.body, pathMapping, [])
        .then((data) => res.json(data))
        .catch((err) => res.status(500).send({'error': err}));
    });

    app.post('/api/te_stm/getSearchResult', function(req, res, next){
      te_stmQuery
        .getSearchResult(req.body)
        .then(function(data){
          res.send(data);
        })
        .then(null, function(err){
          console.log(err);
          res.status(500).send(err);
        });
    });
  };
})();
