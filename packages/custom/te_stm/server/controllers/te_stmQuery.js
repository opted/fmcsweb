'use strict'
var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');

exports.getSearchResult = function(req) {
  var deferred = Q.defer();
  var STM = mongoose.model('STM');
  if (!_.isEmpty(req.from) && !_.isEmpty(req.to)) {
    req.BI_START_TIME = {'$gte': req.from};
    req.BI_END_TIME = {'$lte': req.to};
  };
  delete req.from;
  delete req.to;
  STM.find(req)
    .then(function(result){
      deferred.resolve(result);
    })
    .then(null, function(err){
      console.log(err);
      deferred.reject(err);
    })
  return deferred.promise;
};

