'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;
var Oms = new Module('oms');
var socketio = require('socket.io');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Oms.register(function(app, auth, database, utils, http) {
  var io = socketio.listen(http);
  //We enable routing. By default the Package Object is passed to the routes
  Oms.routes(app, auth, database, utils, io);
  Oms.angularDependencies(['agGrid', 'ui.bootstrap']);
  Oms.aggregateAsset('css', 'oms.css');
  return Oms;
});
