'use strict'
var assert = require('chai').assert;
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var getMongooseModule = require('../../../../testtool/loadAllMongoMedel.js');
var oveninfoEdit = require('../../controllers/oveninfoEdit.js');

describe('OMS LAYOUT QUERY TEST', function() {
  before(function() {
    mongoose.connect('mongodb://192.168.0.170:27019/test', { useMongoClient: true });
  });

  after(function() {
    mongoose.connection.close()
  });
 
  describe('test oveninfoEdit.addOvenInfo', function(){
    it('should save source into DB', function(){
      var source = {
        name: 'OG62',
        x: 12800,
        y: 4300,
      };
      return oveninfoEdit.addOvenInfo(source)
        .then(function(results){
          assert.equal(source.name, results[0].name, 'must same length');
        });
    });
  });
  describe('test oveninfoEdit.updateOvenInfo', function(){
    it('should overwrite DB collection by source', function(){
      var source = [
        { name: 'OG62',
          x: 12800,
          y: 4300,
        }, {
          name: 'OG63',
          x: 13550,
          y: 4300,
        }, {
          name: 'OG6101',
          x: 14300,
          y: 4300,
        }
      ];
      return oveninfoEdit.updateOvenInfo(source)
        .then(function(results){
          assert.equal(3, results.length, 'must same length');
        });
    });
  });
  describe('test oveninfoEdit.getOvenInfo', function(){
    it('should get 3 docs', function(){
      return oveninfoEdit.getOvenInfo()
        .then(function(results){
          assert.equal(3, results.length, 'must same length');
        })
    });
  });
  describe('test oveninfoEdit.deleteOvenInfo', function(){
    it('should delete OG62 oven in DB', function(){
      return oveninfoEdit.deleteOvenInfo('OG62')
        .then(function(results){
          assert.equal(true, results.ok, 'must be true');
        });
    });
  });
});

