'use strict'
var assert = require('chai').assert;
var mesQuery = require('../../controllers/mesQuery.js');

//you shouldn't use done when testing asynchronous 
describe('OMS MES QUERY TEST', function() { 
  describe('test mesQuery.getEquipmentInfo', function(){
    it('should get OG62 info', function(){
      return mesQuery.getEquipmentInfo('OG62')
        .then(function(results){
          assert.equal('OG-62', results[0].EQUIPMENTNO, 'must be OG-62 info');          
        });
    });
  });
  //LHB0YNMBE1, LH70C1YAT1批號資料有可能隨時間消失, 需要定期更新
  describe('test mesQuery.getBIperiodMFT', function(){
    it('should get LHB0YNMBE1 BICycle hours', function(){
      return mesQuery.getBIperiodMFT(['LHB0YNMBE1'])
        .then(function(results){
          assert.isNotEmpty(results);
          assert.equal('LHB0YNMBE1', results[0].LOTNO, 'must same LOTNO');
        });
    });
  });
  describe('test mesQuery.getBIperiodLFT', function(){
    it('should get LH70C1YAT1 BICycle hours', function(){
      return mesQuery.getBIperiodLFT(['LH70C1YAT1'])
        .then(function(results){
          assert.isNotEmpty(results);
          assert.equal('LH70C1YAT1', results[0].LOTNO, 'must same LOTNO');
        })
    });
  });
});

