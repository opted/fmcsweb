'use strict'
var assert = require('chai').assert;
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var getMongooseModule = require('../../../../testtool/loadAllMongoMedel.js');
var sql = require('../../controllers/sqlQuery.js');
var Mbi = require('../../../../utils/server/controllers/mbiConnection');
//不能在測試項目中new Mbi()
var utils = {
  ctrls: {
    mbi: new Mbi()
  }
};
var lotnoForTest;
var sttimeForTest;

describe('OMS SQL QUERY TEST', function() {
  before(function(done) {
    mongoose.connect('mongodb://192.168.0.170:27019/test', { useMongoClient: true });
    sql.getMachineStatusById('OG62', utils)
      .then(function(results){
        lotnoForTest = results[0].lotnum;
        sttimeForTest = results[0].start_time;
        done();
      });
  });

  after(function() {
    mongoose.connection.close();
    utils.ctrls.mbi.cp.close();
  });
 
  describe('test sql.getMachineStatusById', function(){
    it('should get OG62 machine status', function(){
      return sql.getMachineStatusById('OG62', utils)
        .then(function(results){
          assert.isNotEmpty(results);
        });
    });
  });
  describe('test sql.getAllMachineStatus', function(){
    it('should get 2 machine status', function(){
      return sql.getAllMachineStatus(utils)
        .then(function(results){
          assert.isNotEmpty(results);
        });
    });
  });
  describe('test sql.getBinData', function(){
    it('should get OG62 bin data', function(){
      return sql.getBinData('OG62', utils)
        .then(function(results){
          assert.isNotEmpty(results);
        });
    });
  });
  describe('test sql.getErrorData errorType 0', function(){
    it('should get OG62 and LOTNO:NHC02NVAC1 error data errorType 0', function(){
      return sql.getErrorData('OG62', 'NHC02NVAC1', '2017-12-28T13:46:39.000Z',  utils, 0)
        .then(function(results){
          assert.equal(1, 1, 'just test exec function');
        });
    });
  });
});

