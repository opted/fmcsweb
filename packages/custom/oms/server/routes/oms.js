(function() {
  'use strict';
  // The Package is past automatically as first parameter
  module.exports = function(Oms, app, auth, database, utils, io) {    
    var ovenStatus = require('../controllers/ovenStatus');
    var omsSocket = io.of('/omsSocket');
    var interval;
    var mesQuery = require('../controllers/mesQuery');
    var layoutEditor = require('../controllers/layoutEditor');
    var msSql = require('../controllers/sqlQuery');
    layoutEditor.logout('someOneDisconnect'); // initial oms editor login state
    var stInfo = require('../controllers/stInfo');
    var tools = require('../controllers/tools');
    var ovenToLot = {};
    var ovenToStartTime = {};
    var ovenToStatus = {};

    var getStatus = function(){
      var _ = require('lodash');
      var Q = require('q');
      var deferred = Q.defer();
      var dcQueryList = [];
      var bakQueryList = [];
      var aRunList = [];
      var errQueryList = [];

      function printErr(err) {
        console.log(err);
        deferred.reject(err);
      };

      var queryStartT = new Date();
      var stStatusQueryList = [];
      msSql
        .getAllMachineStatus(utils)
        .then(function(recordSet){
          recordSet.forEach(function (element, index) {
            dcQueryList.push(ovenStatus.checkLogStatus(tools.transOvenId(element.id)));
          });
          _.forEach(recordSet, function(element, idx){
            var stStatusParams = {
              'query': {"OVENID": element.id},
              'machine_status': element.status,
              'lotnum' : element.lotnum ,
              'start_time': element.start_time
            };
            stStatusQueryList.push(stInfo.getOneOvenStStatus(stStatusParams))
          });
          Q.all(stStatusQueryList).then(function(stStatusResults){
            stStatusResults.forEach(function (item, idx) {
              recordSet[idx].st_status = item['statusStr'];
            });
           
            Q.all(dcQueryList)
              .then(function (results) {
                results.forEach(function (item, idx) {
                  ovenToLot[recordSet[idx].id] = _.trimEnd(recordSet[idx].lotnum, ',').split(',');
                  ovenToStartTime[recordSet[idx].id] = recordSet[idx].start_time;
                  ovenToStatus[recordSet[idx].id] = recordSet[idx].status;
                  if (ovenStatus.lalarmCheck(recordSet[idx].id, recordSet[idx].start_time, recordSet[idx].status, item.pcLog)) {
                    recordSet[idx].status = 'L-ALARM';
                  };
                  if (!(/^BTS/i.test(recordSet[idx].id)) && recordSet[idx].status !== 'IDLE' ) {
                    bakQueryList.push(ovenStatus.getLastSensorTemp(tools.transOvenId(recordSet[idx].id)));
                    aRunList.push(idx);
                    if (recordSet[idx].status === 'ALARM'){
                      errQueryList.push(
                        ovenStatus.getOvenErrorState(recordSet[idx].id, ovenToLot[recordSet[idx].id],
                                                     ovenToStartTime[recordSet[idx].id], utils)
                      );
                    } else {
                      errQueryList.push({})
                    };
                  };
                });
                bakQueryList.push.apply(bakQueryList, errQueryList);
                Q.all(bakQueryList).then(function (results) {
                  var dataLen = aRunList.length;
                  var bakResults = results.slice(0, aRunList.length);
                  var errResults = results.slice(aRunList.length);
                  bakResults.forEach(function (item, idx) {
                    var ovenId = aRunList[idx];
                    if (!_.isEmpty(errResults[idx])) {
                      var errData = errResults[idx].STATE;
                      if (errData.all_escape) {
                        recordSet[ovenId].errStatus = {'type': 'all_escape', 'name': '整爐跳脫'};
                      } else if ( _.isNumber(recordSet[ovenId].BoardCount) && recordSet[ovenId].BoardCount ===
                                  recordSet[ovenId].BIBAlertCount) { 
                        recordSet[ovenId].errStatus = {'type': 'all_escape', 'name': '整爐跳脫'};
                      } else if (errData.all_zone_escape) { 
                        recordSet[ovenId].errStatus = {'type': 'all_zone_escape', 'name': '整Zone跳脫'};
                      } else if (recordSet[ovenId].BIBAlertCount > 0) {
                        recordSet[ovenId].errStatus = {'type': 'all_board_escape', 'name': '整板跳脫'};
                      } else if (errData.abnormal_temperature) {
                        recordSet[ovenId].errStatus = {'type': 'abnormal_temperature', 'name': '爐溫異常'};
                      };
                    };                   
                    if (_.isDate(item.DATETIME) === false) {             
                      recordSet[ovenId].status += '*';                   
                      if (recordSet[ovenId].errStatus) {
                        recordSet[ovenId].errStatus.type += '*';
                      };
                    };
                  });
                  layoutEditor.getOvenUpdateTimeAndUpdateStatus(recordSet)
                  deferred.resolve(recordSet);
                })
                .then(null, printErr);
              })
              .then(null, printErr);
            })
            .then(null, printErr);;
        })
        .then(null,printErr);
      
      return deferred.promise;      
    };
  
    exports.getOvenStatus = getStatus;
    app.get('/api/oms/oven/info', function(req, res, next) {
      var ovenInfo = require('../controllers/oveninfoEdit');
      ovenInfo.getOvenInfo(utils)
        .then(function(data){
          res.send(data);
        }, function(err){
          res.status(500).send(err);
        });
    });
    //TODO: Layout Editor Need check login state(Server Restart Issue)
    app.post('/api/oms/oven/info', function(req, res, next) {
      var ovenInfo = require('../controllers/oveninfoEdit');
      ovenInfo.addOvenInfo(req.body)
        .then(function(){
          res.send(201);
        }, function(err){
          res.status(500).send(err);
        });
    });
    app.put('/api/oms/oven/info', function(req, res, next) {
      var ovenInfo = require('../controllers/oveninfoEdit');
      ovenInfo.updateOvenInfo(req.body)
        .then(function(){
          res.sendStatus(200);
        }, function(err){
          res.status(500).send(err);
        });
    });
    app.delete('/api/oms/oven/info', function(req, res, next) {
      var ovenInfo = require('../controllers/oveninfoEdit');
      ovenInfo.deleteOvenInfo(req.query.id)
        .then(function(){
          res.sendStatus(200);
        }, function(err){
          res.status(500).send(err);
        });
    });
    app.get('/api/oms/oven/status', function(req, res, next) {
      getStatus().then(function(result){
        res.send(result);
      }).then(null, function(err){
        console.log(err);
        res.status(500).send(err);
      });
    });
    app.get('/api/oms/oven/status/:id', function(req, res, next) {
      if (ovenToStatus[req.params.id] === 'IDLE') {
        res.send({status: 'IDLE'});
      } else {
        ovenStatus.getDetail(req.params.id, ovenToLot[req.params.id] || '', ovenToStartTime[req.params.id], utils)
          .then(function(results){
            res.send(results);
          }).then(null, function(err){
            console.log(err);
            res.status(500).send(err);
          });
      };
    });
    app.post('/api/oms/oven/editorLogin', function(req, res, next){
      layoutEditor.login(req.body.id, req._remoteAddress)
        .then(function(results){
          res.send(results);
        }).then(null, function(err){
          res.status(500).send(err);
        });
    });
    app.post('/api/oms/oven/editorLogout', function(req, res, next){
      layoutEditor.logout(req.body.id, req._remoteAddress)
        .then(function(results){
          res.sendStatus(200);
        }).then(null, function(err){
          res.status(500).send(err);
        });
    });
    app.post('/api/oms/oven/stStatus', function(req, res, next){
      stInfo.getOneOvenStStatus(req.body)
        .then(function(results){
          res.send(results);
        }).then(null, function(err){ 
          res.status(500).send(err);
        });
    });
    app.post('/api/oms/oven/oneStInfo', function(req, res, next){
      stInfo.getOneOvenStInfo(req.body, utils)
        .then(function(results){
          res.send(results);
        }).then(null, function(err){
          res.status(500).send(err);
        });
    });
    //Socket.io
    omsSocket.on('connection', function(socket) {
      // disconnect(關掉頁面)
      socket.on('disconnect', function(){
        layoutEditor.logout('someOneDisconnect')
          .then(function(results){
            omsSocket.emit('checkEditor'); //請其他連線中的client再嘗試登入
          }).then(null, function(err){
            console.log(err);
          });
      });
      socket.on('error', function(err) {
        console.log('[ERR] socket.io', err);
      });
    });

    interval = setInterval(function () {
      if (process.env.OMSUpdate === 'true') {
        getStatus().then(function(result){
          omsSocket.emit('updateStatus', result);
          console.log('[OMS Auto Update Done.]')
        }).then(null, function(err){
          console.log('[OMS Auto Update Error!]')
          console.log(err);
        });
      };
    }, 60000);
  };
})();
