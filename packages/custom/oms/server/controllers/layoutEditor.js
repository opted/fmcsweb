'use strict';
var _ = require('lodash');
var Q = require('q');
var mongoose = require('mongoose');
var serviceState = mongoose.model('ServiceState');

exports.login = function(userid, address){
  var deferred = Q.defer();
  serviceState
    .findOne({service: 'OMS'})
    .then(function(info){
      var check = {}
      if (info.isUsing === true) {
        check.isLogin = false;
        check.user = info.userName;
        check.address = info.address;
        deferred.resolve(check);
      } else {
        var newUser = {
          isUsing: true,
          userName: userid,
          address: address
        };
        serviceState
          .findOneAndUpdate({service: 'OMS'}, newUser)
          .then(function(res){
            newUser.isLogin = true;
            deferred.resolve(newUser)
          })
          .then(null, function(err){
            console.log(err);
            deferred.reject(err);
          });
      };
    }).then(null, function(err){
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

exports.logout = function(userid, address){
  var deferred = Q.defer();
  serviceState
    .findOne({service: 'OMS'})
    .then(function(info){
      if ( !_.isEmpty(info) && info.isUsing === true && (info.userName === userid || userid === 'someOneDisconnect')) {
        var newUser = {
          isUsing: false,
          userName: userid,
          address: address
        };
        if ( userid === 'someOneDisconnect') {
          newUser.address = 'system';
        };
        serviceState
          .findOneAndUpdate({service: 'OMS'}, newUser)
          .then(function(res){
            deferred.resolve(newUser)
          })
          .then(null, function(err){
            console.log(err);
            deferred.reject(err);
          });
      };
    }).then(null, function(err){
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

exports.getOvenUpdateTimeAndUpdateStatus = function(recordSet){
  var deferred = Q.defer();
  var OmsStatus = mongoose.model('OmsStatus');
  var queryList = [];
  OmsStatus.find({})
    .then(function(results){
      _.forEach(recordSet, function(oven){
        var newState;
        if (oven.errStatus) {
          newState = oven.errStatus.type;
        } else {
          newState = oven.status;
        };
        var findResult = _.find(results, {'OVENID': oven.id});
        if (findResult === undefined || findResult.STATE !== newState) { 
          OmsStatus.findOneAndUpdate({OVENID: oven.id}, {'$set': {STATE: newState, UPDATETIME: new Date() }}, {upsert: true}).exec()
        };
      });
    });
  return deferred.promise;
};

