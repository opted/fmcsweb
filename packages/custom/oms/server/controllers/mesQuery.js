var oracledb = require('oracledb');
var _ = require('lodash');
var Q = require('q');
var tools = require('./tools');

exports.getEquipmentInfo = function(ovenId, utils){
  var deferred = Q.defer();
  var sqlStr = 
        "select HISMFT.TBLMSTEQUIPMENT.EQUIPMENTNO,HISMFT.TBLMSTEQUIPMENT.EQUIPMENTTYPE,HISMFT.TBLMSTEQUIPMENTTYPE.EQUIPMENTCATEGORY " +
        "from HISMFT.TBLMSTEQUIPMENT " +
        "INNER JOIN HISMFT.TBLMSTEQUIPMENTTYPE " +
        "ON HISMFT.TBLMSTEQUIPMENT.EQUIPMENTTYPE=HISMFT.TBLMSTEQUIPMENTTYPE.EQUIPMENTTYPE " +
        "where EQUIPMENTNO IN " + transOvenIdToQueryString(ovenId)
  utils.ctrls.mes.exec(sqlStr)
    .then(function(result){
      deferred.resolve(result)
    })
    .catch(function(err){
      console.error('getEquipmentInfo query fail')
      console.error(err.message);
      deferred.reject(err.stack)
    })
  return deferred.promise;
};

exports.getBIperiodMFT = function(lotNum, utils){
  var deferred = Q.defer();
  var sqlStr = 
        "SELECT HISMFT.TBLENGPRODOPRECIPECONTENT.UNIT,VALUE,lotno FROM HISMFT.TBLENGPRODOPRECIPECONTENT " +
        "INNER JOIN HISMFT.TBLWIPLOTOPRECIPE " +
        "ON HISMFT.TBLENGPRODOPRECIPECONTENT.RECIPEID=HISMFT.TBLWIPLOTOPRECIPE.OPRECIPEID " +
        "WHERE LOTNO IN " + transArrayToQueryString(lotNum) + " AND ATTRIBNO = 'BIhourCycle' " + 
        "AND HISMFT.TBLWIPLOTOPRECIPE.OPNO like 'BURNIN%' "
  utils.ctrls.mes.exec(sqlStr)
    .then(function(result){
      deferred.resolve(result)
    })
    .catch(function(err){
      console.error('getBIperiodMFT query fail')
      console.error(err.message);
      deferred.reject(err.stack)
    })
  return deferred.promise;
};

exports.getBIperiodLFT = function(lotNum, utils){
  var deferred = Q.defer();
  var sqlStr = 
        "SELECT HISLFT.TBLENGPRODOPRECIPECONTENT.UNIT,VALUE,lotno FROM HISLFT.TBLENGPRODOPRECIPECONTENT " +
        "INNER JOIN HISLFT.TBLWIPLOTOPRECIPE " +
        "ON HISLFT.TBLENGPRODOPRECIPECONTENT.RECIPEID=HISLFT.TBLWIPLOTOPRECIPE.OPRECIPEID " +
        "WHERE LOTNO IN " + transArrayToQueryString(lotNum) + " AND ATTRIBNO = 'BIhourCycle' " + 
        "AND HISLFT.TBLWIPLOTOPRECIPE.OPNO like 'BURNIN%' "
  utils.ctrls.mes.exec(sqlStr)
    .then(function(result){
      deferred.resolve(result)
    })
    .catch(function(err){
      console.error('getBIperiodLFT query fail')
      console.error(err.message);
      deferred.reject(err.stack)
    })
  return deferred.promise;
};

function transOvenIdToQueryString(ovenId){
  var queryString = "(";
  if (_.isArray(ovenId) === false){
    queryString = "('" + tools.transOvenId(ovenId) + "')";
    return queryString;
  };
  _.forEach(ovenId, function(oven) {
    queryString += "'" + tools.transOvenId(oven) + "',"
  });
  return _.trimEnd(queryString, ',') + ')';
};

function transArrayToQueryString(data){
  var queryString = "(";
  if (_.isArray(data) === false){
    queryString = "('" + data + "')";
    return queryString;
  };
  _.forEach(data, function(item) {
    queryString += "'" + item + "',"
  });
  return _.trimEnd(queryString, ',') + ')';
};

function doRelease(connection){
  connection
    .close( function(err) {
      if (err) {
        console.error(err.message);
      };
    });
};
