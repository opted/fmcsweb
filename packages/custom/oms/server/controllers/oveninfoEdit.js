'use strict';
var _ = require('lodash');
var fs = require('fs');
var Q = require('q');
var mes = require('./mesQuery');
var mongoose = require('mongoose');

var validate = function(data){
  if(!_.isString(data.name) || _.isEmpty(data.name)) return {result:false, msg: 'id format error'};
  if(!_.isNumber(data.x) || _.isNaN(data.x) || data.x < 0) return {result:false, msg: 'pos x format error'};
  if(!_.isNumber(data.y) || _.isNaN(data.y) || data.y < 0) return {result:false, msg: 'pos y format error'};
  return {result:true,msg:'no error'};
};

exports.getOvenInfo = function(utils){
  // TODO: get Equipment type and category from MES
  var collect = [];
  var deferred = Q.defer();
  var OmsLayout = mongoose.model('OmsLayout');
  OmsLayout.aggregate([{'$match': {}}])
    .then(function(collect){

    mes
      .getEquipmentInfo(_.map(collect, 'name'), utils)
      .then(function(equipmentInfo){
        _.forEach(equipmentInfo, function(oven){
          var resultIndex = _.findIndex(collect, {'name': oven.EQUIPMENTNO.replace(/-/g, '')});
          if (resultIndex > 0) {
            collect[resultIndex].equipmentType = oven.EQUIPMENTTYPE;
            collect[resultIndex].equipmentCategory = oven.EQUIPMENTCATEGORY;
          };
        });
        deferred.resolve(collect);
      })
      .then(null, function(err){
        deferred.reject(err);
      });
  });
  return deferred.promise;
};

exports.addOvenInfo = function(data){
  var validStatus = validate(data);
  var deferred = Q.defer();
  if(!validStatus.result) {
    deferred.reject(validStatus.msg);
    return deferred.promise;
  }
  var OmsLayout = mongoose.model('OmsLayout');
  OmsLayout.insertMany(data)
    .then(function(result){
      deferred.resolve(result);
    })
    .then(null, function(err){
      console.log(err);
      deferred.reject(err);
    })  
  return deferred.promise;
};

exports.updateOvenInfo = function(data){
  var deferred = Q.defer();  
  var OmsLayout = mongoose.model('OmsLayout');
  OmsLayout.deleteMany({})
    .then(function(result){
      OmsLayout.insertMany(data)
        .then(function(result){
          deferred.resolve(result);
        })
    })
  return deferred.promise;
};

exports.deleteOvenInfo = function(ovenId){
  var deferred = Q.defer();
  var OmsLayout = mongoose.model('OmsLayout');
  OmsLayout.deleteOne({name: ovenId})
    .then(function(result){
      deferred.resolve(result.result);
    })
    .then(null, function(err){
      console.log(err);
      deferred.reject(err);
    })
  return deferred.promise;
};

