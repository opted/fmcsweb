var mongoose = require('mongoose');
var Q = require('q');
var msSql = require('./sqlQuery');
var mesQuery = require('./mesQuery');
var _ = require('lodash');
var DC = mongoose.model('DCLOG');
var tools = require('./tools');

var dcDetectRange = 4200000; //dc log detect time (now - 4200000) ~ now
exports.dcDetectRange = dcDetectRange;

var stCheck = function(st) {
    return _.isDate(st)? _.gte( new Date() - new Date(st.toISOString().replace(/T/, ' ').replace(/\..+/, '')), dcDetectRange) : true;
};
exports.stCheck = stCheck;

var lalarmCheck = function(id, start_time, status, pc_log){
    return !(/^BTS/i.test(id)) &&  stCheck(start_time) && (status === 'AutoFlowRun' || status === 'ALARM') && pc_log === 'error';
};
exports.lalarmCheck = lalarmCheck;

var nullConvert = function(data){
  return data === 0? data : data ? data:'N/A';
};

var nullBidData = {
  'FILEPATH': 'N/A',
  'SLOTID': 'N/A',
  'ZONEID': 'N/A',
  'DATETIME' :'N/A'
};

var checkLogStatus = function(ovenId){
  var DC_logs = mongoose.model('DCLOG');
  var current = new Date();
  var last;
  return DC_logs.find({'OVENID' : ovenId},{'DATETIME' : 1})
    .sort({'DATETIME' : -1})
    .limit(1)
    .exec()
    .then(function(info){
      if (_.isEmpty(info)) {
        return { pcLog : 'error', pcLog_last_record : 'N/A' };
      };
      last = info[0].DATETIME.toISOString().replace(/T/, ' ').replace(/\..+/, '');
      return { pcLog : _.lte(current - new Date(last), dcDetectRange)? 'pass' : 'error',
               pcLog_last_record : last };
    }).then(null, function(err){
      console.log(err);
    });
};
exports.checkLogStatus = checkLogStatus;

var getLastSensorTemp = function(ovenId){
  var BakCurrent = mongoose.model('BakCurrent');
  return BakCurrent
    .find({'_id.OVENID': ovenId})
    .then(function(result){
      if (result.length > 0) {
        return result[0]
      } else {
        return {}
      }
    });
};
exports.getLastSensorTemp = getLastSensorTemp;

var findZoneData = function(dc_data) {
  var unfindResult = {
    vamp:'N/A',
    v_12:['N/A','N/A'],
    v_34:['N/A','N/A'],
    a_12:['N/A','N/A']
  };
  var zoneData = [_.clone(unfindResult),_.clone(unfindResult),_.clone(unfindResult),_.clone(unfindResult)];
  var currentZone;
  var propName;

  _.forEach(dc_data['ZONES'], function(zone) {
    currentZone = zone['ZONE'];
    var maxVamp = -1;
    _.forEach(zone['BOARDS'], function(board, idx) {
      if (_.inRange(currentZone, 4)) {
        var tmpVamp = nullConvert(board['VOH1']);
        if (tmpVamp > maxVamp) {
          maxVamp = tmpVamp;
          zoneData[currentZone]['vamp'] = tmpVamp;
          zoneData[currentZone]['v_12'] = [nullConvert(board['DPS1']),nullConvert(board['DPS2'])];
          zoneData[currentZone]['v_34'] = [nullConvert(board['DPS3']),nullConvert(board['DPS4'])];
        };
      };
    });
    zoneData[currentZone]['a_12'] = [nullConvert(zone['TOTAL_CURRENT_DPS1']), nullConvert(zone['TOTAL_CURRENT_DPS2'])];
  });
  return zoneData;
};

var getVoltCurrent = function(ovenId){
  var deferred = Q.defer();
  var queryList = [];
  var zoneNum = _.range(4);
  var query = {'OVENID' : ovenId };
  var filter = {'FILEPATH': 1, 'ZONES': 1, 'DATETIME': 1 };
  DC
    .find(query, filter)
    .sort({'DATETIME': -1})
    .limit(1)
    .exec()
    .then(function(dc_data){
      var unfindResult = {
        vamp:'N/A',
        v_12:['N/A','N/A'],
        v_34:['N/A','N/A'],
        a_12:['N/A','N/A']
      };
      if (!dc_data[0]) {
        deferred.resolve(_.fill(new Array(4), unfindResult));
      } else {
        deferred.resolve(findZoneData(dc_data[0]));
      };
    })
    .then(null, function(err){
      console.log('getVoltCurrent' +err); deferred.reject(err);
    });
  return deferred.promise;
};

var getUpdateTime = function(ovenId){
  var deferred = Q.defer();
  var OmsStatus = mongoose.model('OmsStatus');
  var queryList = [];
  OmsStatus.find({OVENID: ovenId})
    .then(function(results){
      if (results.length > 0) {
        deferred.resolve(results[0].UPDATETIME);
      } else {
        deferred.resolve(false);
      };
    })
    .then(null, function(err){
      console.log('getUpdateTime' +err); deferred.reject(err);
    });
  return deferred.promise;
};

exports.getDetail = function(ovenId, lotNum, sttime, utils){
  var kebabCaseID = tools.transOvenId(ovenId);
  var deferred = Q.defer();
  var queryList = [];
  var resData = {};
  queryList.push(getLastSensorTemp(kebabCaseID));
  //TODO: combine checkLogStatus and getVoltCurrent
  queryList.push(checkLogStatus(kebabCaseID));
  queryList.push(getVoltCurrent(kebabCaseID));
  queryList.push(msSql.getMachineStatusById(ovenId, utils));
  queryList.push(msSql.getBinData(ovenId, utils));
  queryList.push(mesQuery.getBIperiodMFT(lotNum, utils));
  queryList.push(mesQuery.getBIperiodLFT(lotNum, utils));
  queryList.push(msSql.getErrorData(kebabCaseID, lotNum, sttime, utils, 0));
  queryList.push(msSql.getErrorData(kebabCaseID, lotNum, sttime, utils, 1));
  queryList.push(msSql.getErrorData(kebabCaseID, lotNum, sttime, utils, 2));
  queryList.push(getUpdateTime(ovenId));
  Q.all(queryList)
    .then(function(results){
      resData.name = ovenId;
      var sensorData = results[0];
      var dclogStatus = results[1];
      var volt = results[2];
      var machineStatus = results[3];
      var binData = results[4];
      var expectTimeMFTData = results[5];
      var expectTimeLFTData = results[6];      
      var errorData = _.concat(results[7], results[8], results[9]);
      var mongoUpdateTime = results[10];

      resData.oven_temperature_last_record = _.isDate(sensorData.DATETIME)
        ? sensorData.DATETIME.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        :'N/A';

      resData.oven_temperature_last_lotno = sensorData.LOTNO
      resData.oven_temperature = [
        nullConvert(sensorData.SENSOR0),
        nullConvert(sensorData.SENSOR1),
        nullConvert(sensorData.SENSOR2),
        nullConvert(sensorData.SENSOR3),
        nullConvert(sensorData.SENSOR4),
        nullConvert(sensorData.SENSOR5)
      ];

      resData.pc_log = dclogStatus.pcLog;
      resData.pc_log_last_record = _.isDate(dclogStatus.pcLog_last_record)
        ? dclogStatus.pcLog_last_record.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        : dclogStatus.pcLog_last_record;
      resData.abnormal_slot = machineStatus[0] && !_.isUndefined(machineStatus[0].BIBAlertCount) && 
                              _.isNumber(+machineStatus[0].BIBAlertCount)? +machineStatus[0].BIBAlertCount : 'N/A';
      resData.total_slot =  machineStatus[0] && !_.isUndefined(machineStatus[0].BoardCount) &&
                            _.isNumber(+machineStatus[0].BoardCount)? machineStatus[0].BoardCount :'N/A';
      resData.status = machineStatus[0] && !_.isEmpty(machineStatus[0].status)? machineStatus[0].status  : 'N/A';

      resData.lotnum = machineStatus[0] && !_.isEmpty(machineStatus[0].lotnum)
        ? _.uniq(_.trimEnd(machineStatus[0].lotnum.trim(),',').split(',')) 
        : ['N/A'];
      resData.program = _.get((_.get(results, [3, 0, 'program'], '')).trim().match(/\\([^\\]{1,}).mdb[ ]*$/), 1, 'N/A');
      resData.start_time = machineStatus[0] && _.isDate(machineStatus[0].start_time)
        ? machineStatus[0].start_time.toISOString().replace(/T/, ' ').replace(/\..+/, '') 
        : 'N/A';
      resData.end_time = machineStatus[0] && _.isDate(machineStatus[0].end_time)
        ? machineStatus[0].end_time.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        : 'N/A';

      resData.update_time = machineStatus[0] && _.isDate(machineStatus[0].update_time)
        ? machineStatus[0].update_time.toISOString().replace(/T/, ' ').replace(/\..+/, '')
        : 'N/A';

      resData.stateChangeTime = mongoUpdateTime;

      resData.version = machineStatus[0] && !_.isEmpty(machineStatus[0].version)? machineStatus[0].version :'N/A';
      if (machineStatus[0] && lalarmCheck(ovenId, machineStatus[0].start_time, resData.status, resData.pc_log)) {
        resData.status = 'L-ALARM';
      };

      // Error Data
      var errData = processErrorData(errorData);
      _.assign(resData, errData);
      if (resData.status === 'ALARM') {
        if (errData.all_escape) {
          resData.errStatus = {'type': 'all_escape', 'name': '整爐跳脫'};
        } else if (_.isNumber(resData.total_slot) && resData.total_slot === resData.abnormal_slot) {
          resData.errStatus = {'type': 'all_escape', 'name': '整爐跳脫'};
          resData.all_escape = '是, 跳脫板數等於全部板數';
        } else if (errData.all_zone_escape) {
          resData.errStatus = {'type': 'all_zone_escape', 'name': '整Zone跳脫'};
        } else if (resData.abnormal_slot > 0) {
          resData.errStatus = {'type': 'all_board_escape', 'name': '整板跳脫'};
        } else if (errData.abnormal_temperature) {
          resData.errStatus = {'type': 'abnormal_temperature', 'name': '爐溫異常'};
        };
      };

      resData.zone = volt; 
      resData.binData = processBinData(binData);
      resData.expect_time = expectTimeMFTData.length > 0 && !_.isUndefined(machineStatus[0]) && _.isDate(machineStatus[0].start_time)
        ? new Date(machineStatus[0].start_time.getTime() + 
          (expectTimeMFTData[0].VALUE*60*60*1000)).toISOString().replace(/T/, ' ').replace(/\..+/, '')
        : 'NOMFTDATA';

      if (resData.expect_time === 'NOMFTDATA') {
        resData.expect_time = expectTimeLFTData.length > 0 && !_.isUndefined(machineStatus[0]) && _.isDate(machineStatus[0].start_time)
          ? new Date(machineStatus[0].start_time.getTime() + 
            (expectTimeLFTData[0].VALUE*60*60*1000)).toISOString().replace(/T/, ' ').replace(/\..+/, '')
          : 'BIhourCycle Data not found';
      };

      deferred.resolve(resData);
    })
    .then(null, function(err){ 
      console.log(err); deferred.reject(err);
    });
  return deferred.promise;
};

exports.getOvenErrorState = function(ovenId, lotNum, sttime, utils){
  var deferred = Q.defer();
  var kebabCaseID = tools.transOvenId(ovenId);
  var queryList = []
  queryList.push(msSql.getErrorData(kebabCaseID, lotNum, sttime, utils, 0));
  queryList.push(msSql.getErrorData(kebabCaseID, lotNum, sttime, utils, 1));
  queryList.push(msSql.getErrorData(kebabCaseID, lotNum, sttime, utils, 2));
  Q.all(queryList)    
    .then(function(results){
      var result = _.concat(results[0], results[1], results[2]);
      if (result.length > 0) {
        deferred.resolve({OVENID: ovenId, STATE: processErrorData(result)});
      } else {
        deferred.resolve({OVENID: ovenId, STATE: false});
      };
    })
    .then(null, function(err){ 
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

function processErrorData(sourceData) {
  var errorData = {
    all_escape: false,
    all_zone_escape: false,
    all_board_escape: false,
    abnormal_temperature: false
  };
  if (sourceData.length === 0) {
    return errorData;
  };
  _.forEach(sourceData, function(data){
    if (data.ErrorCode_1 === '001') {
      errorData.all_escape = '是, ' + data.MsgText;
    };
    if (data.MsgText.indexOf('整個Zone關閉') > -1) {
      errorData.all_zone_escape = '是, ' + data.MsgText;
    };
    if (data.ErrorCode_1 === '004') {
      errorData.abnormal_temperature = '是, ' + data.MsgText;
    };
  });
  return errorData;
};

function processBinData(data) {
  var binData = {
    nonZeroBin: 0,
    zeroBin: 0,
    logTime: 'N/A',
    BIN1: 0,
    BIN2: 0,
    BIN3: 0,
    BIN4: 0,
    BIN5: 0,
    BIN6: 0,
    BIN7: 0,
    BIN8: 0,
    BIN9: 0,
    BINA: 0,
    BINB: 0,
    BINC: 0,
    BIND: 0,
    BINE: 0,
    BINF: 0,
  };

  if (data.length === 0) {    
    return {};
  };
  
  var newestTime = data[0].log_time;
  var newestData = _.filter(data, {log_time: newestTime});
  binData.logTime = newestTime;

  var newestDataByLot = _.groupBy(newestData, 'lotnum')
  var binDataByLot = [];

  _.forEach(newestDataByLot, function(data, lot){
    var maps = _.map(data, 'binmap1');
    maps = _.concat(maps, _.map(data, 'binmap2'));
    var tmpData = _.clone(binData);
    _.forEach(maps, function(binmap){
      _.forEach(binmap, function(bin){
        if (bin !== '0') {
          tmpData.nonZeroBin += 1;
          if (tmpData['BIN' + bin] == undefined) {
            tmpData['BIN' + bin] = 0;
          };
          tmpData['BIN' + bin] += 1;
        } else {
          tmpData.zeroBin += 1;
        };
      });
    });
    binDataByLot.push({'lotNum': lot, 'data': tmpData})
  });
  return binDataByLot;
};
