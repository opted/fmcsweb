const sql = require('mssql');
var _ = require('lodash');
var Q = require('q');
var ovenInfo = require('../controllers/oveninfoEdit');

var queryfunc = function(utils, qSTR, bindVars){
  var deferred = Q.defer();
  utils.ctrls.mbi.exec(qSTR, bindVars)
    .then(function(result){
      deferred.resolve(result);
    })
    .catch(function(err){
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
}; 

exports.getMachineStatusById = function (ovenId, utils) {
  var query = 'select RTRIM(ovenstatus) status, lotnum, dbname as program, ' +
              'sttime as start_time, updatetime as update_time, ' +
              'exefilename as version, BIBAlertCount, BoardCount ' +
              'from machinestatus with(nolock) where machineid = @machineid';
  var bindVars = {
    'machineid': ovenId
  };
  return queryfunc(utils, query, bindVars)
};


exports.getAllMachineStatus = function (utils) {
  var deferred = Q.defer();
  ovenInfo.getOvenInfo(utils)
    .then(function(ovenInfoData){
      var query = "select RTRIM(machineid) id, RTRIM(ovenstatus) status, RTRIM(lotnum) lotnum, " +
                  "sttime start_time,BIBAlertCount,BoardCount from machinestatus with(nolock) " +
                  "where machineid IN " + transArrayToQueryString(_.map(ovenInfoData, 'name'));
      deferred.resolve(queryfunc(utils, query));
    })
    .then(null, function(err){
      console.log(err);
      deferred.reject(err);
    });
  return deferred.promise;
};

exports.getBinData = function (ovenId, utils){
  var nowDate = new Date()
  var nowYear = nowDate.getUTCFullYear() % 100
  var nowMonth = nowDate.getMonth() + 1;
  var tableName = nowMonth >= 10 ? nowYear.toString() + nowMonth.toString(): nowYear.toString() + '0' +  nowMonth.toString()
  var query = "SELECT TOP 50 ID,lotnum,oven_ID,log_time,binmap1,binmap2 " +
              "FROM MBI" + tableName + " with(nolock) " +
              "WHERE oven_ID like '" + ovenId + "' " +
              "ORDER BY ID DESC ";

  return queryfunc(utils, query);
};

exports.getErrorData = function (ovenId, lotnum, sttime, utils, errorType){
  var query = "SELECT TOP 1 Log_Time,Oven_ID,MsgText,ErrorCode_1,lotnum " +
              "FROM OvenErrorMsgALL with(nolock) " +
              "WHERE Oven_ID like '" + ovenId + "' " +  
              "AND lotnum IN " + transArrayToQueryString(lotnum) + " ";
  if (errorType === 0) {
    query += "AND ErrorCode_1 = '001' ";
  } else if (errorType === 1) {
    query += "AND MsgText like '%整個Zone關閉%' ";
  } else if (errorType === 2) {
    query += "AND ErrorCode_1 = '004' ";
    query += "AND MsgText not like '%關閉該DUT%' ";
  };

  if (_.isDate(sttime)) {
    query += "AND Log_Time >= '" + sttime.toISOString().slice(0, 19).replace('T', ' ') + "'";
  };
  return queryfunc(utils, query);
};

function transArrayToQueryString(data){
  var queryString = "(";
  if (_.isArray(data) === false){
    queryString = "('" + data + "')";
    return queryString;
  };
  _.forEach(data, function(item) {
    queryString += "'" + item + "',"
  });
  return _.trimEnd(queryString, ',') + ')';
};
