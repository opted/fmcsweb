//james add-2017/11/1
'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var DC = mongoose.model('DCLOG');
var ST = mongoose.model('ST');
var STOvenConfigue = mongoose.model('STOvenConfigue');
var Q = require('q');


function separate_oven_id(s) {
  var s_array = s.split(/(\d+)/);
  // console.log("@@@@@@@@@@@@@@@", s , s_array)

  if(s_array.length==1){
    return s_array[0]
  }else{
    return s_array[0] + "-" + s_array[1]
  }
}


var getOneOvenStStatus = function(params){
  var deferred = Q.defer();
  // var DC_logs = mongoose.model('DCLOG');
  // var current = new Date();
  // var last;
  if (!(params["machine_status"].indexOf("IDLE") > -1  || params["machine_status"].indexOf("AutoFlowRun") > -1 )){
    deferred.resolve("")
  }
  if(params["machine_status"].indexOf("IDLE") > -1){
    ST.find({'OVENID' : separate_oven_id(params['query']['OVENID']), 'DATETIME': {"$gt" : params["start_time"] }})
    .sort({'DATETIME' : -1})
    .limit(1)
    .exec()
    .then(function(info){
      var o={}
      if(info.length>0){
        var d=new Date(info[0]['DATETIME'])
        var c=new Date();
        o['isExpired'] = ((c-d) / 86400000)>7 ? true:false;
      }

      if(info.length == 0){
        o['statusStr']="待執行Self-test";
              deferred.resolve(o);
        // deferred.resolve("待執行Self-test");
      }else{
         o['statusStr']="Self-test執行中";
              deferred.resolve(o);
        // deferred.resolve("Self-test執行中");
      }
    }).then(null, function(err){
      console.log(err);
      deferred.reject(err);
    });
  }else if(params["machine_status"].indexOf("AutoFlowRun") > -1){
    ST.find({'OVENID' : separate_oven_id(params['query']['OVENID'])})
    .sort({'DATETIME' : -1})
    .limit(5)
    .exec()
    .then(function(info){
      // console.log(info)
      var o={}
      if(info.length>0){
        var d=new Date(info[0]['DATETIME'])
        var c=new Date();
        o['isExpired'] = ((c-d) / 86400000)>7 ? true:false;
      }
      if(info.length == 5){
        STOvenConfigue.findOne({'OVENID' : separate_oven_id(params['query']['OVENID'])})
        .then(function(info2){
          
          // console.log("!!!!!!!!!!!!!", separate_oven_id(params['query']['OVENID']) , "   ", info2)
          if(info2 != null){
            var st_success_count = 0;
            _.forEach(info, function(one_st){
              var oven_success_count = 0;
              _.forEach(one_st["RESULTS"], function(one_result){
                if(one_result["PFDRV"] == 1){
                  oven_success_count++;
                }
              });
              if(oven_success_count.toString() == info2["SLOT"].toString()){
                st_success_count++;
              }
            });
            if(st_success_count == 5){
              o['statusStr']="Self-test執行完成";
              deferred.resolve(o);
            }else{
              o['statusStr']="Self-test 執行Fail";
              deferred.resolve(o);
            }
          }else{
            o['statusStr']="無 Oven Config";
            deferred.resolve(o);
          }
        }).then(null, function(err){
          o['statusStr']="Configue查詢失敗";
            deferred.resolve(o);
        });
      }else{
        o['statusStr']="Self-test 執行Fail";
        deferred.resolve(o);
      }
      
    }).then(null, function(err){
      console.log(err);
      deferred.reject(err);
    });
  }
  
  // else{
  //   
  // }
  return deferred.promise;
};

exports.getOneOvenStStatus = getOneOvenStStatus;


var getOneOvenStInfo = function(params, utils){
    var df = Q.defer();
    try {
      var query = params;
      var match = {}
      match["OVENID"] = separate_oven_id(params['query']['OVENID']);
      var pipe = [{'$match': match}];
      pipe.push({'$unwind': "$RESULTS"});
      pipe.push(
          { '$group': {
            '_id': { 
              'DATETIME': '$DATETIME', 
              'TESTITEM': '$TESTITEM',
              'OVENID': '$OVENID'
            },
            'TOTAL': { '$sum': '$RESULTS.TESTDRV' },
            'PASS': { '$sum': '$RESULTS.PFDRV' }
          }}
        );
      pipe.push({
        '$project': {
          '_id': 0,
          'DATETIME': {
            '$dateToString': { 'format': '%Y-%m-%d %H:%M', 'date': '$_id.DATETIME' }
          },
          'TESTITEM': '$_id.TESTITEM',
          'OVENID': '$_id.OVENID',
          'PASS': '$PASS',
          'TOTAL': '$TOTAL' 
        }});
      pipe.push({'$sort': { 'DATETIME' : -1 }});
      pipe.push({'$limit': 5});
      // console.log(pipe) 
      var doQuery = utils.ctrls.mongoQuery.massiveQuery(ST, pipe)
      .then(function(result){
        try {
            df.resolve(result);
        } catch (e) {
            df.reject(e.stack);
            console.log(e)
        }
      })
    } catch (err){
      df.reject(err.stack);
      console.log(err)
    }

    return df.promise;
  };
exports.getOneOvenStInfo = getOneOvenStInfo;