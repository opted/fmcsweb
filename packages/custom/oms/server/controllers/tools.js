'use strict';
var transOvenId = function(ss) {
  ss = ss || '';
  return ss.replace(/[-]*([0-9]{2})/g, '-$1').toUpperCase();
};
exports.transOvenId = transOvenId;
