describe('PasswordController', function() {
  beforeEach(module('app'));

  var $controller;
  var $loc;

  beforeEach(inject(function(_$controller_, _$location_){
    $controller = _$controller_;
    $loc = _$location_;
  }));

  describe('$scope.grade', function() {
    var $scope, controller;

    beforeEach(function() {
      $scope = {};
      controller = $controller('PasswordController', {$scope: $scope});
    });

    it('sets the strength to "strong" if the password length is >8 chars', function() {
      $scope.password = 'longerthaneightchars';
      $scope.grade();
      expect($scope.strength).toEqual('strong');
    });

    it('sets the strength to "weak" if the password length <3 chars', function() {
      $scope.password = 'a';
      $scope.grade();
      expect($scope.strength).toEqual('weak');
    });

    it('should navigate away from the current page', function() {
      $loc.path('/here');
      $scope.navigate();
      expect($loc.path()).toEqual('/some/where/else');
    });
    
  });
});
