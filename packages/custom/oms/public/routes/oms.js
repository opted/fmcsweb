(function() {
  'use strict';
  function Oms($stateProvider) {
    $stateProvider.state('oms main page', {
      url: '/oms',
      templateUrl: 'oms/views/index.html',
      controller:OmsController,
      resolve:{
        loggedin:function(MeanUser){
        return MeanUser.checkLoggedIn();
        }
      }
    })
  }

  angular
    .module('mean.oms')
    .config(Oms);

  Oms.$inject = ['$stateProvider'];
})();
