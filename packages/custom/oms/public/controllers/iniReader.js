﻿(function (undefined) {
  var root = this;
  var isNode = false;
  var INIParser = {};
  var eol = '\r\n';
  if (typeof exports !== 'undefined') {
    if (typeof module !== 'undefined' && module.exports) {
      exports = module.exports = INIParser;
      isNode = true;
    }
    exports.vINIParser = INIParser;

  } else {
    root.INIParser = INIParser;
  }

  if (typeof _ === 'undefined') {
    console.log('Lodash.js is required. Get it from http://lodash.com/');
    return;
  }
  var regex = {
    section: /^\s*\[\s*([^\]]*)\s*\]\s*$/,
    param: /^\s*([\w\.\-\_]+)\s*=\s*(.*?)\s*$/,
    comment: /^\s*;.*$/
  };

  function isQuoted(val) {
    return (val.charAt(0) === '"' && val.slice(-1) === '"') ||
    (val.charAt(0) === "'" && val.slice(-1) === "'");
  }

  function safe(val) {
    return (typeof val !== 'string' ||
        val.match(/[=\r\n]/) ||
        val.match(/^\[/) ||
        (val.length > 1 &&
          isQuoted(val)) ||
        val !== val.trim()) ?
      JSON.stringify(val) :
      val.replace(/;/g, '\\;').replace(/#/g, '\\#');
  }

  function unsafe(val, doUnesc) {
    val = (val || '').trim();
    if (isQuoted(val)) {
      // remove the single quotes before calling JSON.parse
      if (val.charAt(0) === "'") {
        val = val.substr(1, val.length - 2);
      }
      try { val = JSON.parse(val) } catch (_) { }
    } else {
      // walk the val to find the first not-escaped ; character
      var esc = false;
      var unesc = '';
      for (var i = 0, l = val.length; i < l; i++) {
        var c = val.charAt(i);
        if (esc) {
          if ('\\;#'.indexOf(c) !== -1) {
            unesc += c;
          } else {
            unesc += '\\' + c;
          }
          esc = false;
        } else if (';#'.indexOf(c) !== -1) {
          break;
        } else if (c === '\\') {
          esc = true;
        } else {
          unesc += c;
        }
      }
      if (esc) {
        unesc += '\\';
      }
      return unesc;
    }
    return val;
  }

  function sectionEncode(str) {
    return '[' + safe(str) + ']';
  }

  function paramEncode(key, val, separator) {
    return safe(key) + separator + safe(val);
  }

  function encode(obj, opt) {
    var out = [];

    if (typeof opt === 'string') {
      opt = {
        section: opt,
        whitespace: false
      }
    } else {
      opt = opt || {};
      opt.whitespace = opt.whitespace === true;
    }

    var separator = opt.whitespace ? ' = ' : '=';
    //var sections = [];
    _.forOwn(obj, function (value, key) {
      out.push(sectionEncode(key));
      if (value && _.isArray(value)) {
        _.forEachRight(value, function (item, idx) {
          out.push(paramEncode(idx,item,separator));
        });
      } else if (value && _.isObject(value)) {
        _.forOwnRight(value, function (v, k) {
          out.push(paramEncode(k, v, separator));
        });
      } else {
        out.push(paramEncode(key, separator, value));
      }
      out.push('');
    });

    return out.join(eol);
  }

  function parse(data) {
    var value = {};
    var lines = data.split(/\r\n|\r|\n/);
    var section = null;
    lines.forEach(function (line) {
      if (regex.comment.test(line)) {
        return;
      } else if (regex.param.test(line)) {
        var match = line.match(regex.param);
        if (section) {
          value[section][match[1]] = match[2];
        } else {
          value[match[1]] = match[2];
        }
      } else if (regex.section.test(line)) {
        var match = line.match(regex.section);
        value[match[1]] = {};
        section = match[1];
      } else if (line.length == 0 && section) {
        section = null;
      };
    });
    return value;
  }

  INIParser.parse = parse;
  INIParser.encode = encode;
}).call(this);
