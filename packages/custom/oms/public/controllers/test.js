//angular.module('app', [])
//.controller('PasswordController', function PasswordController($scope) {
function PasswordController($scope, $location) {
  $scope.password = '';
  $scope.grade = function() {
    var size = $scope.password.length;
    if (size > 8) {
      $scope.strength = 'strong';
    } else if (size > 3) {
      $scope.strength = 'medium';
    } else {
      $scope.strength = 'weak';
    }
  };
  $scope.navigate = function() {
    $location.path('/some/where/else');
  };
};
angular
  .module('app', [])
  .controller('PasswordController', PasswordController);

PasswordController.$inject = ['$scope', '$location'];
