'use strict';
function OmsController($scope, OvenInfoEdit, OvenStatus, OvenColors, Modeladjust, d3, machineParser, OmsSocket, MeanUser, $http, $timeout, _, $interval){
  $scope.data = {
    rectmodel: [],    //畫面矩形資料
    objxy: null,      //當前選擇的矩形座標
    display: false,   //顯示機台資訊
    name: null,       //機台名稱
  };
  $scope.stDisplay = false;
  $scope.st = {
    time: new Date(),
    total: "",
    pass: "",
    fail: "",
    yield: ""
  }
  $scope.showBakData = false;
  $scope.isLoading = false;       
  $scope.isReloadPage = true;
  $scope.searchedOvenList = [];

  //accodion panel
  $scope.accod = {
    isOpenOvenPanel: false,
    isOpenDCPanel: false,
    isOpenBakPanel: false,
    isOpenErrorPanel: false,
    isOpenBinPanel: false
  };

  $scope.showCategories = [{'name': '所有機種', 'types': ['所有型號']}];
  $scope.selectedShowCategory = $scope.showCategories[0];
  $scope.selectedShowType = $scope.selectedShowCategory.types[0];
  $scope.info = {};
  $scope.machineStatus = []; //機台狀態
  $scope.data.objxy = {
    x: null,
    y: null
  };
  $scope.statusTest = {
    "ALARM": /^ALARM/i,
    "LALARM": /^L-ALARM/i,
    "ARun": /^A-Run/i,
    "AEnd": /^A-End/i,
    "MRun": /^M-Run/i,
    "MEnd": /^M-End/i,
    "IDLE": /^IDLE/i,
    "NoData": /^NoData/i
  }; 
  resetColorsum();
  function resetColorsum(){    
    $scope.statusSum = { //機台計數
      "ALARM": 0,
      "all_escape": 0,
      "all_zone_escape": 0,
      "all_board_escape": 0,
      "abnormal_temperature": 0,
      "LALARM": 0,
      "ARun": 0,
      "AEnd": 0,
      "MRun": 0,
      "MEnd": 0,
      "IDLE": 0,
      "NoData":0,      
      'ALARMList': [],
      "all_escapeList": [],
      "all_zone_escapeList": [],
      "all_board_escapeList": [],
      "abnormal_temperatureList": [],
      "LALARMList": [],
      "ARunList": [],
      "AEndList": [],
      "MRunList": [],
      "MEndList": [],
      "IDLEList": [],
      "NoDataList":[]
    };
  };

  $scope.user = {};
  MeanUser.loadUserInfo(MeanUser,function(){
      $scope.user.id = MeanUser.userInfo().local.name || "";
      $scope.user.editable = MeanUser.userInfo().local.omsAuthority || false;
  });

  function rateCalculate(){
    var alarmSet = [
      'all_escape',
      'all_zone_escape',
      'all_board_escape',
      'abnormal_temperature'
    ];
    var totalSet = [
      'ALARM',
      'LALARM',
      'ARun',
      'AEnd'
    ];
    var alarmCount = 0;
    var extraCount = 0;
    _.forEach(alarmSet, function(item){
      alarmCount += $scope.statusSum[item];
    });
    _.forEach(totalSet, function(item){
      extraCount += $scope.statusSum[item];
    });
    var rate = alarmCount / (alarmCount + extraCount);
    $scope.alarm_rate = alarmCount +  '/' + (alarmCount + extraCount) + '=' + _.round(rate*100, 2) + '%';
  };

  // 啟用編輯
  $scope.startEditing = function (item) {
    if ($scope.selfSigned) {
      $scope.editing = true;
      $("#nValue").focus();
    };
  };

  // 結束編輯
  $scope.doneEditing = function (item) {
    if ($scope.data.name) {
      $scope.editing = false;
    };
    if ($scope.data.objxy.x) {
      $scope.editing = false;
    };
  };  

  // 更新機台狀態
  $scope.initOvenInfo = function() {
    OvenInfoEdit.query(function (info) {
      $scope.data.rectmodel = info;
      $scope.showCategories.splice(1);
      var cates = _.uniq(_.map(info, 'equipmentCategory'));
      var allTypes = ['所有型號'];
      _.forEach(cates, function(c){
        if (_.isUndefined(c) === false) {
          var types = _.uniq(_.map(_.filter(info, {'equipmentCategory': c}), 'equipmentType'));
          allTypes = _.union(allTypes, types);
          types.splice(0,0,'所有型號');
          $scope.showCategories.push({'name': c, 'types': types});
        };
      });
      $scope.showCategories[0].types = allTypes;
      $scope.isReloadPage = true; 
      OvenStatus.query(function (statusData) {
        $scope.isReloadPage = false; 
        $scope.data.rectmodel = updateMachineStatus($scope.data.rectmodel, statusData);
        makeAllDiagram();
      });
    }); 
  };
  $scope.initOvenInfo();

  OmsSocket.on('updateStatus', function (data) {
    $scope.data.rectmodel = updateMachineStatus($scope.data.rectmodel, data);
    updateMachineInfo()
    $scope.lastUpdateTime = Date.now();
  });

  // 新增機台
  $scope.addMachine = function(){
    $scope.helpblock2 = '';
    if (!$scope.machineName) {
      $scope.helpblock2 = '不可為空白';
      return;
    };
    $("#addModal").modal('hide');
    var newModel = {x: 9000, y: 200, name: $scope.machineName};
    $scope.data.rectmodel.push(newModel);
    $scope.isLoading = true;
    OvenInfoEdit.create(newModel, function(){
      OvenStatus.query(function (statusData) {
        $scope.data.rectmodel = updateMachineStatus($scope.data.rectmodel, statusData);
        makeDiagram(newModel, $scope.data.rectmodel.length-1, true)
        $scope.machineName = null; 
        rateCalculate();
        $scope.lastUpdateTime = Date.now();
        $scope.isLoading = false;
      });
    });
  };

  $scope.change = function(){
    $scope.helpblock2 = '';
    if (!$scope.machineName) {
      $scope.helpblock2 = '不可為空白';
    };
  };

  // 匯入機台
  $scope.iniFile = null;
  $scope.rowData = null;
  $scope.FILE_NAME = null;
  $scope.fileChanged = function (files) {
    $scope.iniFile = files[0];
  };
  $scope.preView = function () {
    var promise = machineParser.read($scope.iniFile, $scope);
    promise.then(function(data) {
      $('#importModal').modal('hide');
      $scope.rowData = data;
      $scope.data.rectmodel = updateMachineStatus(data, false);
      svg.selectAll("*").remove();
      $scope.data.rectmodel.forEach(function (val,index) {
        makeDiagram(val, index, 0);
      });
      rateCalculate();
      $scope.lastUpdateTime = Date.now();
    });
  };

  // 匯出機台
  $scope.exportFile = function() {
    var len = $scope.data.rectmodel.length;
    machineParser.write($scope.data.rectmodel, { 'EQP_Count': len });
  };

  // 儲存機台
  $scope.save = function() {
    var updatedata = _.cloneDeep($scope.data.rectmodel);
    updatedata.forEach(function (val) {
      delete val["status"];
    });
    OvenInfoEdit.update(
      updatedata, 
      function success() { alert('儲存成功！') },
      function err() {alert('連線失敗，請重新操作！')}
    ); 
  };
  // SocketIO設定
  $scope.selfSigned = false;  //this client 未登入
  $scope.logOut = function(){
    $scope.selfSigned = false;
    $http.post('/api/oms/oven/editorLogout',{'id': $scope.user.id})
      .then(function(res){
        console.log(res.data);
      })
      .then(null, function(err){
        console.log(err);
      });
  };
  $scope.signin = function() {
    $http.post('/api/oms/oven/editorLogin',{'id': $scope.user.id})
      .then(function(res){
        if (res.data.isLogin === true) {
          $scope.selfSigned = true;
        } else {
          var alertMessage = '管理者' + res.data.user + '使用中！';
          alert(alertMessage);
        };
      })
      .then(null, function(err){
        console.log(err);
      });
  };
  OmsSocket.on('checkEditor', function(res){
    if ($scope.selfSigned === true) {
      $scope.signin();
    };
  });
  OmsSocket.on('disconnect', function(){
    console.log('socket disconnect');
    $scope.selfSigned = false;
  });

  // ZOOM設定
  var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 1000 - margin.left - margin.right,
    height = 550 - margin.top - margin.bottom;

  var x = d3.scale.linear()
    .domain([-width / 2, width / 2])
    .range([0, width]);

  var y = d3.scale.linear()
    .domain([-height / 2, height / 2])
    .range([height, 0]);

  var p = d3.select("#monitoringSystem");
  var zoom = d3.behavior.zoom()
    .x(x)
    .xExtent([-2000,2000])
    .y(y)
    .yExtent([-1500,1500])
    .scale([0.25])
    .scaleExtent([0.25, 2])
    .on("zoom", function () {
       svg.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")")
    });

  // 機台列表SVG
  var svg = p.selectAll("svg").data(["there can be only one"])
    .enter().append('svg').attr('width', "100%").attr('height',550)
    .call(zoom)
    .on("dblclick.zoom", null)
    .append("g");

  // for 1336*768 setting
  svg.attr("transform", "translate(322.5, 62.5)" + " scale(0.25)");

  // 建立與設定矩形內容
  function makeDiagram(machineInfo, count, isNew){
    var x = Modeladjust.Model.ref(machineInfo, 'x')
              .clamped(0, 25000)
              .multiply(1 / 13)
    var y = Modeladjust.Model.ref(machineInfo, 'y')
              .clamped(0, 15000)
              .multiply(1 / 9)

    var model = {
      get: function() { return [x.get(), y.get()]; },
      set: function(p) { x.set(p[0]); y.set(p[1]); }
    };

    var g = d3.select('svg').select('g').append('g').attr('class', "draggable").attr('id', machineInfo.name);

    g.on("mousedown", function() {
      d3.event.stopPropagation();//停用平移
      setmachineInfo(machineInfo);
      var blinkList = getBlinkList();
      d3.select(this).classed('blink', true); // 賦予當前選擇機台閃爍狀態
      d3.select(this).select("*").attr('style', "stroke-width: 10px; stroke-opacity:1; stroke: red")
      this.parentNode.appendChild(this);
      $scope.data.objxy['x'] = machineInfo.x;
      $scope.data.objxy['y'] = machineInfo.y;

      //如果為登入狀態，顯示的機台名稱為輸入的機台名稱(如果為空則回復原名)
      if ($scope.selfSigned) {
        d3.select("#nValue").on("input", function () {
          if (this.value) {
            d3.select("#m" + count).text(this.value);
            $scope.data.rectmodel[count].name = this.value;
          };
        });
      };

      //當前機台點擊刪除
      $scope.deletemodel = function() {
        var rectmodelname = d3.select("#m" + count).text();
        if (confirm("確定刪除" + rectmodelname + "?")) {
          $scope.data.display = false;
          $scope.data.rectmodel.splice(count,1);
          OvenInfoEdit.remove({id: rectmodelname}, function(){
            makeAllDiagram();
          });
        };
      }
    });

    //建立新增機台初始狀態
    if (isNew) {
      g.attr('class', 'draggable blink');
      isNew = false;
    };

    $scope.machineInfoStatus = {
      color:'#aaaaaa',
      statusName:'NoData'
    };

    if (machineInfo.errStatus && _.trimEnd(machineInfo.status, '*') !== 'L-ALARM') {
      $scope.machineInfoStatus = OvenColors.setColor(machineInfo.errStatus.type);
      $scope.statusSum[_.trimEnd(machineInfo.errStatus.type, '*')]++;
      $scope.statusSum[_.trimEnd(machineInfo.errStatus.type, '*') + 'List'].push(machineInfo.name)
      if (_.trimEnd(machineInfo.errStatus.type, '*') !== 'all_zone_escape') { 
        g.attr('class', 'draggable blink');
      };
    } else {
      $scope.machineInfoStatus = OvenColors.setColor(machineInfo.status);   
      for (var key in $scope.statusTest) {
        if ($scope.statusTest.hasOwnProperty(key) && $scope.statusTest[key].test($scope.machineInfoStatus.statusName)) {
          $scope.statusSum[key]++;
          $scope.statusSum[key + 'List'].push(machineInfo.name)
          break;
        };
      };
    };
    g.append('rect').attr('class', "shadow").attr('height', 40).attr('width', 50);
    g.append('rect').attr('style', "fill:" + $scope.machineInfoStatus.color).attr('height', 40).attr('width', 50).attr("id","r"+count);
    var t = g.append("text");
    t.append("tspan").attr("x", "2px").attr("dy", "1em").text(machineInfo.name).style('font-weight', 'bold').attr("id","m"+count);
    t.append("tspan").attr("x", "2px").attr("dy", "1em").text($scope.machineInfoStatus.statusName).style('font-size', '90%').attr("id","s"+count);
    t.append("tspan").attr("x", "2px").attr("dy", "1em").text(machineInfo.st_status).style('font-size', '47%').attr("id","st"+count);
    //BUG: 擊點後位置會位移
    d3.behavior.drag()
      .on('drag', on_drag)
      .on('dragstart', function () {
        $scope.data.display = true;
        d3.event.sourceEvent.stopPropagation();
        this.parentNode.appendChild(this);
        //d3.selectAll('g').classed('blink', false);
        //d3.select(this).classed('blink', true);
        d3.select(this).classed('dragging', true);
      })
      .on('dragend', function () {
        d3.select(this).classed('dragging', false);
      })
      .call(g);

    redraw();

    function on_drag() {
      if($scope.selfSigned){
        model.set([d3.event.x, d3.event.y]);
        redraw();
      }
    };
    function redraw() {
      g.attr('transform', "translate(" + model.get() + ")");
    };
    var stStatusParams = {
      'query': {"OVENID": machineInfo.name},
      'machine_status': machineInfo.status,
      'lotnum' : machineInfo.lotnum ,
      'start_time': machineInfo.start_time
    }
  };

  function makeAllDiagram() {
    // console.log(_.clone($scope.data.rectmodel));
    resetColorsum();
    svg.selectAll("*").remove();
    $scope.data.rectmodel.forEach(function (val,index) {
      val.isShowDiagram = 
        ($scope.selectedShowCategory.name === '所有機種' || $scope.selectedShowCategory.name === val.equipmentCategory) &&
        ($scope.selectedShowType === '所有型號' || $scope.selectedShowType === val.equipmentType)      
      if (val.isShowDiagram) {
        makeDiagram(val, index, false);
      };
    });
    rateCalculate();
    $scope.lastUpdateTime = Date.now();
    $scope.summayBtClass = 'blink';
  };

  function formatFloat(num, pos){
    var size = Math.pow(10, pos);
    return Math.round(num * size) / size;
  }

  function setmachineInfo(machineInfo) {
    $scope.isLoading = true;
    $scope.data.display = true;
    if ($scope.accod === undefined || $scope.accod.isOpenOvenPanel === false) {
      $scope.accod.isOpenOvenPanel = true;
      $scope.accod.isOpenDCPanel = false;
      $scope.accod.isOpenBakPanel = false;
      $scope.accod.isOpenErrorPanel = false;
      $scope.accod.isOpenBinPanel = false;      
    };
    $scope.editing = false; //初始化編輯名稱狀態
    $scope.info = {};
    d3.selectAll('g').select("*").attr('style', ""); //取消其他機台紅框
    trunOffBlink();
    $scope.data.name = machineInfo.name;
    OvenStatus.get({id: machineInfo.name, lot: machineInfo.lotnum}, function(res){
      if (res.status == 'IDLE') {
        $scope.data.display = false; 
        $scope.isLoading = false;
        return;
      };
      if (_.isUndefined(res.oven_temperature_last_lotno)) {
        $scope.showBakData = false;
      } else {
        var diffBetweenLots = _.difference(res.oven_temperature_last_lotno, res.lotnum);
        if (_.isEmpty(diffBetweenLots)) {
          $scope.showBakData = true;
          if (_.every(_.map(res.oven_temperature, function(x){return x == 327.67}, true))) {
            $scope.showBakData = false;
          };
        } else {
          $scope.showBakData = false;
        };
      };
      //機台資訊
      if (res.errStatus && _.trimEnd(res.status, '*') !== 'L-ALARM') { //機台狀態
        $scope.info["status"] = res.errStatus.name;
        _.find($scope.data.rectmodel, {name: machineInfo.name}).errStatus = res.errStatus;
      } else {
        $scope.info["status"] = res.status;
      };
      _.find($scope.data.rectmodel, {name: machineInfo.name}).status = res.status;
      updateMachineInfo();
      $scope.info["abnormal_slot"] = res.abnormal_slot;//異常slot數
      $scope.info["total_slot"] = res.total_slot;//Total slot數
      $scope.info["lotnum"] = res.lotnum.join(',');//京元批號
      $scope.info["lotnums"] = res.lotnum;
      $scope.info["update_time"] = res.update_time;//更新時間
      $scope.info["stateChangeTime"] = res.stateChangeTime //狀態更新時間
      $scope.info["version"] = res.version;//版本
      $scope.info["program"] = res.program;//程式名稱
      $scope.info["equipmentType"] = machineInfo.equipmentType || 'N/A';         //機型
      $scope.info["equipmentCategory"] = machineInfo.equipmentCategory || 'N/A'; //機種
      $scope.info["start_time"] = res.start_time;//BI開始時間
      $scope.info["expect_time"] = res.expect_time; //預計出爐時間

      //DCLOG資訊
      $scope.info["pc_log"] = res.pc_log;//DC log
      $scope.info["pc_log_last_record"] = res.pc_log_last_record;//DC log最後紀錄
      $scope.info["ZONE0_vamp"] = res.zone[0]['vamp'];//ZONE0電壓vamp
      $scope.info["ZONE0_v12"] = res.zone[0]['v_12'].join(', ');//ZONE0電壓(DPS1,2)
      $scope.info["ZONE0_v34"] = res.zone[0]['v_34'].join(', ');//ZONE0電壓(DPS3,4)
      $scope.info["ZONE0_a12"] = res.zone[0]['a_12'].join(', ');//ZONE0電流(DPS1,2)
      $scope.info["ZONE1_vamp"] = res.zone[1]['vamp'];//ZONE1電壓vamp
      $scope.info["ZONE1_v12"] = res.zone[1]['v_12'].join(', ');//ZONE1電壓(DPS1,2)
      $scope.info["ZONE1_v34"] = res.zone[1]['v_34'].join(', ');//ZONE1電壓(DPS3,4)
      $scope.info["ZONE1_a12"] = res.zone[1]['a_12'].join(', ');//ZONE1電流(DPS1,2)
      $scope.info["ZONE2_vamp"] = res.zone[2]['vamp'];//ZONE2電壓vamp
      $scope.info["ZONE2_v12"] = res.zone[2]['v_12'].join(', ');//ZONE2電壓(DPS1,2)
      $scope.info["ZONE2_v34"] = res.zone[2]['v_34'].join(', ');//ZONE2電壓(DPS3,4)
      $scope.info["ZONE2_a12"] = res.zone[2]['a_12'].join(', ');//ZONE2電流(DPS1,2)
      $scope.info["ZONE3_vamp"] = res.zone[3]['vamp'];//ZONE3電壓vamp
      $scope.info["ZONE3_v12"] = res.zone[3]['v_12'].join(', ');//ZONE3電壓(DPS1,2)
      $scope.info["ZONE3_v34"] = res.zone[3]['v_34'].join(', ');//ZONE3電壓(DPS3,4)
      $scope.info["ZONE3_a12"] = res.zone[3]['a_12'].join(', '); //ZONE3電流(DPS1,2)

      //爐溫資訊
      $scope.info["oven_temperature_last_record"] = res.oven_temperature_last_record;//爐溫最後紀錄
      $scope.info["oven_temperature_last_lotno"] = res.oven_temperature_last_lotno;//爐溫最後紀錄批號
      $scope.info["oven_temperature1"] = res.oven_temperature[0];//第一點之爐溫
      $scope.info["oven_temperature2"] = res.oven_temperature[1];//第二點之爐溫
      $scope.info["oven_temperature3"] = res.oven_temperature[2];//第三點之爐溫
      $scope.info["oven_temperature4"] = res.oven_temperature[3];//第四點之爐溫
      $scope.info["oven_temperature5"] = res.oven_temperature[4];//第五點之爐溫
      $scope.info["oven_temperature6"] = res.oven_temperature[5];//第六點之爐溫

      //錯誤資訊
      $scope.info["all_escape"] = res.all_escape ? res.all_escape: '否' ;//整爐跳脫
      $scope.info["all_zone_escape"] = res.all_zone_escape ? res.all_zone_escape : '否';//整zone跳脫
      $scope.info["abnormal_temperature"] = res.abnormal_temperature ? res.abnormal_temperature : '否' //爐溫異常
      $scope.info["all_board_escape"] = res.abnormal_slot > 0 ? '是': '否';//整板跳脫
      $scope.info["alert_count"] = res.abnormal_slot; //跳脫版
      
      //BIN資訊
      $scope.info["hasBinData"] = _.isArray(res.binData);
      if ($scope.info["hasBinData"] === true) {
        $scope.info["binData"] = res.binData;
      };
      $scope.isLoading = false;
    });

    $http.post('/api/oms/oven/oneStInfo',{'query': {"OVENID": machineInfo.name}})
      .then(function(res){
        var oneStInfo = res["data"];
        
        if(oneStInfo.length > 0){
          $scope.stDisplay = true;
          var nowTotal = 1000;
          var nowPass = 1000;
          for(var i = 0 ; i < oneStInfo.length ; i++){
            if( nowTotal > oneStInfo[i]['TOTAL']){
              nowTotal = oneStInfo[i]['TOTAL'];
            }
            if( nowPass > oneStInfo[i]['PASS']){
              nowPass = oneStInfo[i]['PASS'];
            }
          }
          $scope.st.time = oneStInfo[0]['DATETIME']
          $scope.st.total = nowTotal
          $scope.st.pass = nowPass
          $scope.st.fail = nowTotal - nowPass
          $scope.st.yield = formatFloat((nowPass/nowTotal)*100, 2) + "%"
        }else{
          $scope.stDisplay = false;
        }
      })
      .then(null, function(err){
        console.log(err);
        $scope.stDisplay = false;
      });

  };

  $scope.changeShowCategory = function(c){
    $scope.selectedShowCategory = c;
    makeAllDiagram();
  };

  $scope.changeShowType = function(t){
    $scope.selectedShowType = t;
    makeAllDiagram();
  };

  $scope.clickStatusSumButton = function(type){
    var map = {
      'all_escape': '整爐跳脫',
      'all_zone_escape': '整zone跳脫',
      'all_board_escape': '整板跳脫',
      'abnormal_temperature': '爐溫異常',
      'A-Run': 'AutoFlowRun',
      'A-End': 'AutoFlowEnd',
      'M-Run': 'ManualFlowRun',
      'M-End': 'ManualFlowEnd',
    };
    $scope.statusModalType = map[type] || type;
    $scope.statusModalContent = $scope.statusSum[type.replace(/-/, '') + 'List'];
  };

  var hasCloseMember = function(){
    var hasCloseMember = false;
    _.forOwn($scope.accod, function(value, key){
      if (value === false) {
        hasCloseMember = true;
      };
    });
    $scope.hasCloseMember =  hasCloseMember;
  };

  $scope.toggoleAccodPanel = function(){
    var keys = _.keys($scope.accod);
    if ($scope.hasCloseMember) { 
      _.forEach(keys, function(key){
        $scope.accod[key] = true;
      });
    } else {
      _.forEach(keys, function(key){
        $scope.accod[key] = false;
      });
    }
    hasCloseMember();
  };

  $scope.isOvenExists = function(){
    if (_.find($scope.data.rectmodel, {name: $scope.inputOven}) === undefined){
      $scope.helpblockSearch = "OvenId: " + $scope.inputOven + "不存在";
    } else {
      $scope.helpblockSearch = "";
    }
  };

  $scope.searchOven = function(){
    var g = d3.select('svg').select('g').select('#' + $scope.inputOven);
    g.classed('blink', true);
    g.select("*").attr('style', "stroke-width: 10px; stroke-opacity:1; stroke: red") 
    $scope.searchedOvenList.push($scope.inputOven);
    $('#searchModal').modal('hide');
  };

  function updateMachineStatus(machineData, newStatus){
    if (newStatus) {
      $scope.machineStatus = newStatus;
    };
    machineData.forEach(function (rect) {
      rect.status = 'NoData';
      $scope.machineStatus.forEach(function (stat) {
        if (stat.id == rect.name) {
          rect.status = stat.status;
          rect.lotnum = stat.lotnum;
          rect.start_time = stat.start_time;
          rect.st_status = stat.st_status;
          if (stat.errStatus) {
            rect.errStatus = stat.errStatus;
          } else {
            if (stat.id !== $scope.data.name) {
               trunBlinkByOvenId(stat.id, false);
            };
            delete rect.errStatus;
          };
        };
      });
    });
    console.log('L-ALARM count:')
    console.log(_.filter(machineData, {status: 'L-ALARM'}))
    console.log(_.filter(machineData, {status: 'L-ALARM*'}))
    return machineData;
  };

  function getBlinkList() {
    var blinkTypes = [
      'abnormal_temperature',
      'abnormal_temperature*',
      'all_escape',
      'all_escape*',
      'all_board_escape',
      'all_board_escape*'
    ];
    var blinkList = [];
    _.forEach(blinkTypes, function(type){
      blinkList.push.apply(blinkList, _.map(_.filter($scope.data.rectmodel, {'errStatus': {'type': type}}), 'name'));
    });
    return blinkList;
  };

  function trunOffBlink() {
    var blinkList = getBlinkList();
    _.forEach($scope.searchedOvenList, function(ovenId){
      if (blinkList.indexOf(ovenId) < 0) {
        var g = d3.select('svg').select('g').select('#' + ovenId);
        g.classed('blink', false);
      };
    });
    $scope.searchedOvenList = [];
    if (blinkList.indexOf($scope.data.name) < 0) {
      var g = d3.select('svg').select('g').select('#' + $scope.data.name);
      g.classed('blink', false);
    } else {
      _.forEach(blinkList, function(ovenId){
        var g = d3.select('svg').select('g').select('#' + ovenId);
        g.classed('blink', false);
        $scope.summayBtClass = '';
        $timeout(function(){
          g.classed('blink', true);
          $scope.summayBtClass = 'blink';
        }, 100);
      });
    }
  }

  function updateMachineInfo() {
    resetColorsum();
    $scope.data.rectmodel.forEach(function (val,index) {
      if (val.isShowDiagram === false) {
        return;
      };
      $scope.setmachineInfoStatus = {
        color:'#aaaaaa',
        statusName:'NoData'
      };
      if (val.errStatus && _.trimEnd(val.status, '*') !== 'L-ALARM') {
        $scope.setmachineInfoStatus = OvenColors.setColor(val.errStatus.type);
        $scope.statusSum[_.trimEnd(val.errStatus.type, '*')]++;
        $scope.statusSum[_.trimEnd(val.errStatus.type, '*') + 'List'].push(val.name)
        if (_.trimEnd(val.errStatus.type, '*') !== 'all_zone_escape') {
          trunBlinkByOvenId(val.name, true);
        };
      } else {
        $scope.setmachineInfoStatus = OvenColors.setColor(val.status);
        for (var key in $scope.statusTest) {
          if ($scope.statusTest.hasOwnProperty(key) && $scope.statusTest[key].test($scope.setmachineInfoStatus.statusName)) {
            $scope.statusSum[key]++;
            $scope.statusSum[key + 'List'].push(val.name);
            break;
          };
        };
      };
      d3.select("#s" + index).text( $scope.setmachineInfoStatus.statusName);
      d3.select("#r" + index).attr('style', "fill:" +  $scope.setmachineInfoStatus.color);
      d3.select("#st" + index).text(val.st_status);
    });
    rateCalculate();
  };

  function trunBlinkByOvenId(ovenId, disable) {
    var g = d3.select('svg').select('g').select('#' + ovenId);
    g.classed('blink', disable);
  };
  //檢查超過兩分鐘沒有更新
  var checkUpdate = $interval(function() {
    if ( (new Date() - $scope.lastUpdateTime) > 2*60*1000 ) {
      console.log('over')
      $scope.isNoUpdate = true;
    } else { 
      $scope.isNoUpdate = false;
    }
  }, 15000);
};

angular
  .module('mean.oms', [])
  .controller('OmsController', OmsController);

OmsController.$inject = ['$scope', 'OvenInfoEdit', 'OvenStatus', 'OvenColors', 'Modeladjust', 'd3', 'machineParser',
                         'OmsSocket', 'MeanUser', '$http', '$timeout', '_', '$interval'];
