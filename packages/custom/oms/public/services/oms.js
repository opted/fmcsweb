'use strict';
angular.module('mean.oms')
  .factory('OvenInfoEdit', function($resource) {
    return $resource('/api/oms/oven/info', {}, {
      update: { method: 'PUT' },
      query: { method: 'GET', isArray: true },
      create: { method: 'POST' },
      remove: { method: 'DELETE' }
    });
  })
  .factory("OvenStatus", function ($resource) {
    //TODO: change ':id' to use req.query get parameters
    return $resource("/api/oms/oven/status/:id", {}, {
      get: { method: 'GET' },
      query: { method: "GET", isArray: true }
    });
  })
  .factory("Modeladjust", function () {
    var Model = function Model(init) {
      this.get = init.get;
      this.set = init.set;
    };
    Model.ref = function(obj, prop) {
      return new Model({
        get: function() { return obj[prop]; },
        set: function(v) { obj[prop] = v; }
      });
    };
    Model.constant = function(v) {
      return new Model({
        get: function() { return v; },
        set: function(_) { }
      });
    };
    Model.prototype.clamped = function(lo, hi) {
      var m = this;
      return new Model({
        get: function() { return m.get(); },
        set: function(v) { m.set(Math.min(hi, Math.max(lo, v))); }
      });
    };
    Model.prototype.add = function(a) {
      var m = this;
      return new Model({
        get: function() { return m.get() + a; },
        set: function(v) { m.set(v - a); }
      });
    };
    Model.prototype.multiply = function(k) {
      var m = this;
      return new Model({
        get: function() { return m.get() * k; },
        set: function(v) { m.set(v / k); }
      });
    };
    Model.prototype.rounded = function() {
      var m = this;
      return new Model({
        get: function() { return m.get(); },
        set: function(v) { m.set(Math.round(v)); }
      });
    };
    return {
      Model : Model
    };
  })
  .factory("FileSaver", function() {
    var s2ab = function(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      var i;
      for (i = 0; i != s.length; i+=1) {
        view[i] = s.charCodeAt(i) & 0xFF;
      };
      return buf;
    };
    var writeFile = function(obj, fileName) {
      saveAs(new Blob([s2ab(obj)], { type: "application/octet-stream" }), fileName);
    };
    return {
      writeFile: writeFile
    };
  })
  .factory('FileReader', ['$q', '$window', function ($q, $window) {
    var onLoad = function(reader, deferred, scope) {
      return function () {
        scope.$apply(function () {
          deferred.resolve(reader.result);
        });
      };
    };
    var onError = function (reader, deferred, scope) {
      return function () {
        scope.$apply(function () {
          deferred.reject(reader.result);
        });
      };
    };
    var onProgress = function(reader, scope) {
      return function (event) {
        scope.$broadcast('fileProgress', {
          total: event.total,
          loaded: event.loaded
        });
      };
    };
    var getReader = function(deferred, scope) {
      var reader = new $window.FileReader();
      reader.onload = onLoad(reader, deferred, scope);
      reader.onerror = onError(reader, deferred, scope);
      reader.onprogress = onProgress(reader, scope);
      return reader;
    };
    var readAsDataURL = function (file, scope) {
      var deferred = $q.defer();

      var reader = getReader(deferred, scope);
      reader.readAsDataURL(file);

      return deferred.promise;
    };
    var readAsText = function(file, encoding, scope) {
      var deferred = $q.defer();

      var reader = getReader(deferred, scope);
      reader.readAsText(file, encoding);

      return deferred.promise;
    };
    var readAsBinaryString = function (file, scope) {
      var deferred = $q.defer();

      var reader = getReader(deferred, scope);
      reader.readAsBinaryString(file);

      return deferred.promise;
    };

    return {
      readAsDataURL: readAsDataURL,
      readAsBinaryString: readAsBinaryString,
      readAsText: readAsText
    };
  }])
  .factory("INIReaderService", ['$q', 'FileReader', function ($q, FileReader) {
    var iniParser = INIParser;
    var read = function (file, scope) {
      var deferred = $q.defer();
      FileReader.readAsText(file, 'utf8', scope)
        .then(function (resp) {
          var result = iniParser.parse(resp);
          deferred.resolve(result);
        }, function (err) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    var parse = function (str, scope) {
      var deferred = $q.defer();
      var result = iniParser.parse(str);
      scope.$apply(function () {
        deferred.resolve(result);
      });
      return deferred.promise;
    }
    var encode = iniParser.encode;

    return {
      read: read,
      parse: parse,
      encode: encode
    };
  }])
  .factory("machineParser", ['$q', 'INIReaderService', 'FileSaver', function ($q, INIReaderService, FileSaver) {
    function convert(data, idx, result) {
      result.unshift({
        name: data.EQP_Name[idx],
        x: +data.EQP_Left[idx],
        y: +data.EQP_Top[idx]
      });
      return --idx < 1 ? result : convert(data, idx, result);
    }
    var read = function(file, scope) {
      var deferred = $q.defer();
      INIReaderService.read(file, scope)
        .then(function (data) {
          var result = [];
          var len = +data.Common.EQP_Count || 0;
          len > 0 ? deferred.resolve(convert(data, len, result)) : deferred.reject('no data or data format error !');
        }, function(err) {
          deferred.reject(err);
        });
      return deferred.promise;
    };

    var write = function (arr, opt) {
      var valueArr = [];
      var obj = {
        'EQP_Top': {},
        'EQP_Left': {},
        'EQP_Name' : {},
        'Common' : {}
      };
      (!_.isUndefined(opt.EQP_Count)) && (obj.Common.EQP_Count = opt.EQP_Count);
      (!_.isUndefined(opt.EQP_FilePath)) && (obj.Common.EQP_FilePath = opt.EQP_FilePath);
      (!_.isUndefined(opt.Floor_IMG)) && (obj.Common.Floor_IMG = opt.Floor_IMG);
      _.forEach(arr, function (item, idx) {
        obj.EQP_Top[idx + 1] = item.y;
        obj.EQP_Left[idx + 1] = item.x;
        obj.EQP_Name[idx + 1] = item.name;
      });
      var iniStr = INIReaderService.encode(obj, opt);
      FileSaver.writeFile(iniStr, 'EQPMgr.ini');
    };
    return {
      read : read,
      write : write,
      convert : convert
    };
  }])
  .factory("OvenColors", function () {
    var nameColorMap = {
      'AutoFlowRun' : {
        statusName : 'A-Run',
        color : '#aaffbb'
      },
      'AutoFlowRun*': {
        statusName : 'A-Run*',
        color : '#aaffbb'
      },
      'ALARM':{
        statusName : 'ALARM',
        color : '#ffaa33'
      },
      'ALARM*':{
        statusName : 'ALARM*',
        color : '#ffaa33'
      },
      'L-ALARM': {
        statusName: 'L-ALARM',
        color: 'pink'
      },
      'L-ALARM*': {
        statusName: 'L-ALARM*',
        color: 'pink'
      },
      'AutoFlowEnd':{
        statusName : 'A-End',
        color : '#77aaff'
      },
      'AutoFlowEnd*':{
        statusName : 'A-End*',
        color : '#77aaff'
      },
      'ManualFlowRun': {
        statusName : 'M-Run',
        color : '#aaffff'
      },
      'ManualFlowRun*': {
        statusName : 'M-Run*',
        color : '#aaffff'
      },
      'ManualFlowEnd':{
        statusName : 'M-End',
        color : '#ddbbff'
      },
      'ManualFlowEnd*':{
        statusName : 'M-End*',
        color : '#ddbbff'
      },'IDLE':{
        statusName : 'IDLE',
        color : '#ffffbb'
      },
      'default' :{
        statusName : 'NoData',
        color : '#aaaaaa'
      },
      'all_escape' : {
        statusName : '整爐跳脫',
        color: '#ff6666'
      },
      'all_escape*' : {
        statusName : '整爐跳脫*',
        color: '#ff6666'
      },
      'abnormal_temperature' : {
        statusName : '爐溫異常',
        color: '#ff6666'
      },
      'abnormal_temperature*' : {
        statusName : '爐溫異常*',
        color: '#ff6666'
      },
      'all_zone_escape' : {
        statusName : '整Z跳脫',
        color: '#ff6666'
      },
      'all_zone_escape*' : {
        statusName : '整Z跳脫*',
        color: '#ff6666'
      },
      'all_board_escape' : {
        statusName : '整板跳脫',
        color: '#FF00FF'
      },
      'all_board_escape*' : {
        statusName : '整板跳脫*',
        color: '#FF00FF'
      }
    },
    setColor = function(status){
      return status && nameColorMap[status] ? {
        statusName : nameColorMap[status].statusName,
        color : nameColorMap[status].color
      } :  {
        statusName : nameColorMap['default'].statusName,
        color : nameColorMap['default'].color
      };
    };
    return {
        setColor: setColor
    };
  })
  .factory('OmsSocket', function($rootScope, $location) {
    var baseUrl = 'http://'+$location.host()+':'+$location.port()+'/omsSocket';
    var socket = io.connect(baseUrl);
    return {
      on: function(eventName, callback) {
        socket.on(eventName, function() {
          var args = arguments;
          $rootScope.$apply(function() {
            callback.apply(socket, args);
          });
        });
      },
      emit: function(eventName, data, callback) {
        socket.emit(eventName, data, function() {
          console.log('event:', eventName);
          var args = arguments;
          $rootScope.$apply(function() {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        });
      }
    };
  });
