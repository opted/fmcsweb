'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var ChannelTest = new Module('ctest');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
ChannelTest.register(function(app, auth, database, utils) {
  
  ChannelTest.aggregateAsset('css', '../css/ctest.css');
  //We enable routing. By default the Package Object is passed to the routes
  ChannelTest.routes(app, auth, database, utils);
  return ChannelTest;
});
