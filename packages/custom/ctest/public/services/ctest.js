(function() {
'use strict';
  function ChannelTest($http,$q,_,Rebi) {
    var groupByNameMapping = {oven_ID:'爐號',driver_id:'DRIVERID',board_id:'BoardID',BID:'BID',DAY:'時間(DAY)',flowname:'FLOWNAME',BlockName:'BLOCKNAME',OZS:'O\Z\S',bipgm:'BIPGM'};
    var groupByOvenMapping = {};
    var groupList = _.keys(groupByNameMapping);
    var groupOvenList = [];
    var CtestService = Object.create(Rebi); // Channel Test Service inheritance from Rebi Service
    // Defined new function
    CtestService.hisOvenIDgroupBy = function(rawdata){
      groupByOvenMapping = {};
      var ovenlist = _.map(rawdata,'oven_ID');
      ovenlist.forEach(function(ovenid){
        groupByOvenMapping[ovenid] = ovenid;
      });
      groupOvenList = _.keys(groupByOvenMapping);
      return groupByOvenMapping;
    };
    CtestService.initCurChartParams = function(rawdata){
      return this.getChartParams(0,rawdata,groupList,'cur');
    };
    CtestService.initHisChartParams = function(rawdata){
      return this.getChartParams(0,rawdata,groupOvenList,'his');
    };
    CtestService.getDataGridColumnDefs = function(){
      var fieldList = ['ID','oven_ID','board_id','lotnum','Zone_ID','Slot_ID','xsock','ysock','TestQty','FailQty','binmap1',
      'binmap2','log_time','bipgm','flowname','BlockName','BoardStatus',
      'driver_id','BID','CustomerLotNo','socketmap1','socketmap2'];
      var ColumnDefs = [];
      fieldList.forEach(function(key){
        ColumnDefs.push({headerName: key, field: key, filter: 'text', width: 100});
      });
      return ColumnDefs;
    };
    CtestService.getGroupGridColumnDefs = function(){
      var fieldList = ['TestQty','FailQty','ID','board_id','lotnum','Zone_ID','Slot_ID','xsock','ysock','binmap1',
      'binmap2','log_time','bipgm','flowname','BlockName','BoardStatus',
      'driver_id','BID','CustomerLotNo','socketmap1','socketmap2'];
      var ColumnDefs = [{headerName: 'oven_ID', field: 'oven_ID', width: 100,comparator: agGrid.defaultGroupComparator, cellRenderer: 'group',rowGroupIndex:0}];
      fieldList.forEach(function(key){
        ColumnDefs.push({headerName: key, field: key, filter: 'text', width: 100});
      });
      return ColumnDefs;
    };
    // Override function
    CtestService.groupByMapping = function(){
      return groupByNameMapping;
    };
    CtestService.getChartData = function(chartParams){
      var rawdata = chartParams.data;
      var selected = chartParams.groupby.selected;
      var ctype = chartParams.ctype;
      var chartData = [];
      if(ctype=='cur'){
        // group by selected
        var groupMap = _.groupBy(rawdata,selected), maxvalue=8;
        // calculate by selected
        for(var key in groupMap){
          var groupData = groupMap[key];
          var sdata = {key:key};
          // 2.1 cal testqty, passrate or failrate
          var testqty =_.sumBy(groupData,'TestQty');
          var failqty =_.sumBy(groupData,'FailQty');
          sdata['yield']=(testqty-failqty)/testqty;
          sdata['failcount']=failqty;
          maxvalue = Math.max(failqty,maxvalue);
          for(var key in sdata)
            if(key=='yield')
              sdata[key] = _.round((sdata[key]*100),2);
          chartData.push(sdata);
        }
        chartData.forEach(function(cdata){
          cdata['maxvalue'] = maxvalue;
        });
      }
      else if(ctype=='his'){
        // filter by selected ovenid
        var filterData = _.filter(rawdata,function(o){return o['oven_ID']==selected;});
        var groupMap = _.groupBy(filterData,'log_time'), maxvalue=100;
        // calculate by selected
        for(var key in groupMap){
          var groupData = groupMap[key];
          var sdata = {key:key};
          // 2.1 cal testqty, passrate or failrate
          var testqty_sa =_.sumBy(groupData,function(o){
            return (o['flowname']=='summary' && o['BlockName']=='auto')?o['TestQty']:0;
          });
          var failqty_sa =_.sumBy(groupData,function(o){
            return (o['flowname']=='summary' && o['BlockName']=='auto')?o['FailQty']:0;
          });
          sdata['yield_sa']=(testqty_sa-failqty_sa)/testqty_sa;
          var testqty_sm =_.sumBy(groupData,function(o){
            return (o['flowname']=='summary' && o['BlockName']=='manual')?o['TestQty']:0;
          });
          var failqty_sm =_.sumBy(groupData,function(o){
            return (o['flowname']=='summary' && o['BlockName']=='manual')?o['FailQty']:0;
          });
          sdata['yield_sm']=(testqty_sm-failqty_sm)/testqty_sm;
          for(var key in sdata)
            if(key.startsWith('yield'))
              sdata[key] = _.round((sdata[key]*100),2);
          sdata['maxvalue'] = maxvalue;
          chartData.push(sdata);
        }
      }
      chartParams.allshowdata = chartData;
      chartParams.allshowdata = this.sortingAllShowData(chartParams);
      var limitselected = chartParams.limit.selected;
      var newChartData = [];
      // handle limit item part
      if(limitselected=='all' || limitselected>=chartParams.allshowdata.length)
        newChartData = chartParams.allshowdata;
      else
        for(var i=0;i<limitselected;i++)
          newChartData.push(chartParams.allshowdata[i]);
      return newChartData;
    };
    CtestService.getChartParams = function(level,rawdata,groupbylist,chartType){
      // Iinitial status was defined as following
      var chartParams = Rebi.initBasicChartParams(level,rawdata,groupbylist,chartType);
      chartParams.showdata = this.getChartData(chartParams);
      chartParams.tmpgroupresult[chartParams.groupby.selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
      return chartParams;
    };
    CtestService.getDrillDownParams = function(chartParams,category){
      if(chartParams.groupby.itemlist.length==1){
        alert('No more dimension for drill down!');
        return undefined;
      }
      var key = _.keys(category)[0];
      // 1. get new rawdata
      var rawdata = _.filter(chartParams.data,function(o){
        if(o[key]==category[key])
          return true;
        else
          return false;
      });
      // 2. get new groupbylist
      // drill down level 
      var groupbylist = _.cloneDeep(chartParams.groupby.itemlist);
      _.remove(groupbylist,function(o){return o==chartParams.groupby.selected;});
      var ddChartParams = this.getChartParams(chartParams.level+1,rawdata,groupbylist,chartParams.ctype);
      // 4. set drillpath
      ddChartParams.drillpath = _.cloneDeep(chartParams.drillpath);
      ddChartParams.drillpath.push(category);
      return ddChartParams;
    };
    CtestService.changeGroup = function(chartParams,selected){
      chartParams.groupby.selected = selected;
      if(chartParams.tmpgroupresult[selected]){ //defined
        var tmpresult = chartParams.tmpgroupresult[selected];
        chartParams.showdata = tmpresult.showdata;
        chartParams.allshowdata = tmpresult.allshowdata;
        chartParams.sorting = tmpresult.sorting;
        chartParams.limit = tmpresult.limit;
      }
      else{ // undefined
        chartParams.showdata = this.getChartData(chartParams);
        chartParams.sorting = _.cloneDeep(chartParams.sorting);
        chartParams.limit = _.cloneDeep(chartParams.limit);
        // 暫存 groupby 資料, 這部分資料在切換 groupby, limit, sorting 時要確認是否確實置換
        chartParams.tmpgroupresult[selected] = {showdata:chartParams.showdata,allshowdata:chartParams.allshowdata,sorting:chartParams.sorting,limit:chartParams.limit};
      }
      return chartParams;
    };
    CtestService.chartChangeGroup = function(scope,chart,item){
      if(scope.drillDownTriggerChangeGroup){
        // drill down cilck will make new groupby tabset and trigger this function, 
        // do not trigger at this time.
        scope.drillDownTriggerChangeGroup = false;
        return;
      }
      scope.curChartParams = this.changeGroup(scope.curChartParams,item);
      chart.dataProvider = scope.curChartParams.showdata;
      chart.validateData();
      chart.validateNow();
    };
    CtestService.chartDrillDown = function(scope,chart,graphItemCategory){
      // get new chart params
      var category = {};
      category[scope.curChartParams.groupby.selected] = graphItemCategory;
      var newChartParams = this.getDrillDownParams(scope.curChartParams,category);
      if(newChartParams){
        Rebi.setDrillDownTab(scope,newChartParams);
        Rebi.chartDrillDownAutoChangeTab(scope);
      }
      else
        return;
    };
    CtestService.setSortinField = function(chartParams){
      if(chartParams.ctype=='cur'){
        if(chartParams.sorting.selected.indexOf('yield')>=0)
          chartParams.sorting.field = 'yield';
        else if(chartParams.sorting.selected.indexOf('failcount')>=0)
          chartParams.sorting.field = 'failcount';
        else
          chartParams.sorting.field = undefined;
      }
      else if(chartParams.ctype=='his'){
        if(chartParams.sorting.selected.indexOf('yield summary/auto')>=0)
          chartParams.sorting.field = 'yield_sa';
        else if(chartParams.sorting.selected.indexOf('yield summary/manual')>=0)
          chartParams.sorting.field = 'yield_sm';
        else
          chartParams.sorting.field = undefined;
      }
    };
    CtestService.sortingAllShowData = function(chartParams){
      this.setSortinField(chartParams);
      return Rebi.sortingAllShowData(chartParams);
    };
    CtestService.chartChangeSorting = function(scope,chart,changeItem){
      this.setSortinField(scope.curChartParams);
      return Rebi.chartChangeSorting(scope,chart,changeItem);
    };
    return CtestService;
  }
  angular
      .module('mean.ctest')
      .factory('ChannelTest', ChannelTest);
  ChannelTest.$inject = ['$http','$q','_','Rebi'];
})();