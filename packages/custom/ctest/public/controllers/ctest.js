'use strict';
var openFile = undefined;
var daySeconds = 24*60*60*1000;
function ChannelTestController($scope, Global, $stateParams, $window, $http, utilsSrchOpt, common, _) {
  /*
    search panel
  */
  $scope.selected = {datetype:'day'};
  $scope.mbifields = [
    utilsSrchOpt.genField("爐號(OvenID)", "OVEN_ID", "string", true, true),
    utilsSrchOpt.genField("內批(Lotnum)", "LOTNUM", "string", true, true),
    utilsSrchOpt.genField("BID", "BID", "string", true, true),
    utilsSrchOpt.genField("BoardID", "BOARD_ID", "string", true, true),
    utilsSrchOpt.genField("BIProgram", "BIPGM", "string", true, true),
    utilsSrchOpt.genField("DriverID", "DRIVER_ID", "string", true, true)
  ];
  $scope.mbifsURL = '/api/mbisql/getsearchfield';
  $scope.filterExtraMatch = {datetime:$scope.selected};
  $scope.resetTime = function(){
    var curtime = new Date();
    curtime.setHours(23,59,59,999);
    $scope.selected.from = new Date(common.getToday().getTime()-daySeconds);
    $scope.selected.to = curtime;
  };
  $scope.resetTime();
  // select zone & slot
  $scope.zoneSlotOpts = {zone:{selected:[],list:[0,1,2,3]},slot:{selected:[],list:[]}};
  for(var i=0;i<14;i++)
    $scope.zoneSlotOpts.slot.list.push(i);
  $scope.queryParams = {type:0};
  $scope.changeType = function(type){
    return;
  };
  /*
   query summary information
  */
  $scope.requestStatus = undefined;
  $scope.rowNum = undefined;
  $scope.waitingResponse = false;
  var startQuery = function(){
    $scope.waitingResponse = true;
    $scope.requestStatus = undefined;
    $scope.rowNum = undefined;
  };
  var endQuery = function(err,length){
    $scope.waitingResponse = false;
    if(err){
      $scope.requestStatus = '查詢失敗, '+err;
    }
    else{
      $scope.rowNum = length;
      $scope.requestStatus = '查詢完成';
    }
  };
  /*
   query function
  */
  $scope.queryData = function(){
    
    var params = {};
    params.time = {from:$scope.selected.from,to:$scope.selected.to};
    // test parameters
    //params.match = {OVEN_ID:'OD57'};
    params.match = utilsSrchOpt.getMatch($scope.mbifields);
    //append zoneid and slotid
    if(!_.isEmpty($scope.zoneSlotOpts.zone.selected))
      params.match['ZONE_ID'] = $scope.zoneSlotOpts.zone.selected.toString();
    if(!_.isEmpty($scope.zoneSlotOpts.slot.selected))
      params.match['SLOT_ID'] = $scope.zoneSlotOpts.slot.selected.toString();
    // replace OVEN_ID starts with 'O' to 'M'
    if(params.match['OVEN_ID'])
      params.match['OVEN_ID'] = 'M'+params.match['OVEN_ID'].slice(1);
    startQuery();
    var startTime = new Date();
    $scope.queryTime = "";
    var query_url = '/api/ctest/querydata/';
    if($scope.queryParams.type==0){
      $scope.queryType = '現況分析';
      query_url+='cur';
      //  搜尋從 time.to 開始往前推算三個月內的最新 Channel Test 資訊
      var cur_fromtime = new Date(params.time.to);
      // 取 fromtime 為 totime 前三個月當月1日0時0分0秒
      cur_fromtime = new Date(new Date(new Date(cur_fromtime.setMonth((cur_fromtime.getMonth()-3))).setDate(1)).setHours(0,0,0,0));
      params.time.from = cur_fromtime;
      $scope.curAnalysisTime = {from:cur_fromtime,to:params.time.to};
    }
    else if($scope.queryParams.type==1){
      $scope.queryType = '歷史分析';
      query_url+='his';
    }
    $http.post(query_url,params)
    .then(function(response){
      if($scope.queryParams.type==0){
         endQuery(undefined,response.data.cur.length);
         modifiedTimeFormat(response.data.cur);
        $scope.curChartData = response.data.cur;
        $scope.noctovens = response.data.noctovens;
      }
      else{
         endQuery(undefined,response.data.his.length);
         modifiedTimeFormat(response.data.his);
        $scope.hisChartData = response.data.his;
      }
      $scope.queryTime = new Date() - startTime;
    },
    function(err){
      endQuery(err);
      $scope.queryTime = new Date() - startTime;
    });
  };
  var modifiedTimeFormat = function(docs){
    docs.forEach(function(o){
      o['log_time'] = o['log_time'].replace('T',' ').replace('.000Z','');
    });
    return docs;
  };
}

angular
    .module('mean.ctest')
    .controller('ChannelTestController', ChannelTestController);

ChannelTestController.$inject = ['$scope', 'Global', '$stateParams', '$window', '$http', 'utilsSrchOpt', 'common','_'];