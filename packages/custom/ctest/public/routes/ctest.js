(function() {
    'use strict';

    function ChannelTest($stateProvider) {
        $stateProvider.state('ctest main page', {
            url: '/ctest',
            templateUrl: 'ctest/views/index.html',
            controller:ChannelTestController,
            resolve:{
              loggedin:function(MeanUser){
                return MeanUser.checkLoggedIn();
              }
            }
        });
    }

    angular
        .module('mean.ctest')
        .config(ChannelTest);

    ChannelTest.$inject = ['$stateProvider'];

})();
