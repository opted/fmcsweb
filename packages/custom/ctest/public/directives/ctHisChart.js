
(function(){
	'use strict';
	function ctHisChart(ChannelTest,common,_){
    var chartName = "ct-his-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=' // Data {data:rawdata,ftype:{rebi:selectedtype,yield:selectedtype}}
			},
			template: 
      '<div ng-show="display">'+
      '<h3>歷史分析</h3>'+
      // grouping button
      // render all pill tabset and hide unneeded tabset to fix a bug
      '<uib-tabset class="rebi-groupby" type="pills">\
      <uib-tab ng-repeat="item in curChartParams.groupby.itemlist" heading="{{GroupByMapping[item]}}" select="changeGroup(item)">\
      </uib-tab>\
      </uib-tabset>'+
      // tab title
      '<h3>{{ curChartParams.tabtitle }}</h3>'+
      // using hint
      '<div class="row col-sm-12">\
      <label class="text-primary control-label vcenter">* 點擊一次 Drill down</label>'+
      // sorting select
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">排序</label>\
        <select vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglist" ng-change="changeSorting(0)">\
        </select>\
        <div class="btn-group vcenter">\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="0" ng-click="changeSorting(1)">降序</label>\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="1" ng-click="changeSorting(2)">升序</label>\
        </div>\
      </div>'+
       // amchart
      '<div id="'+chartName+'" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      '<div class="row" style="padding-left:20px;"><div style="height:180px;" class="ag-blue" ag-grid="ChartDataGrid"></div>\
      <button class="btn btn-success" ng-click="exportAgGrid(\'xls\')">下載Excel</button></div>'+
       // drilldown tab
      '<div class="col-sm-12" ng-show="drillTablist.length>0">\
       <hr>\
       <h3>Drill Down 分析</h3>\
       <uib-tabset active="drillDownTabActive">\
        <uib-tab ng-repeat="tab in drillTablist" index="tab.id">\
        <uib-tab-heading>\
          {{tab.title}}<button type="button" class="close" aria-label="Close" style="font-size:medium" ng-click="closeTab($index)">\
          <span aria-hidden="true">&times;</span>\
          </button>\
        </uib-tab-heading>\
          <div class="col-sm-12" style="padding-top:10px;">\
            <ct-cur-chart data="tab.data" chartid="his-{{tab.id}}"></ct-cur-chart>\
          </div>\
        </uib-tab>\
        </div>\
      </uib-tabset>',
			link: function (scope, element, attrs) {
        scope.display = false;
        /**
          Chart parameters, use to drilldown, sorting, limit or change group by
        */
        /*
          Chart params structure
          {
          level:,//drill down 等級，初始等級為0
          groupby:{itemlist:["LOTNO","ROOTLOT",...],selected:"LOTNO",active:0}, // 分群用項目
          sorting:{selected:'X軸字典排序',type:0},//可用的排序規則記在外部
          limit:{selected:'X軸字典排序',type:0}, //圖表呈現上限
          drillpath:"",
          rawdata:[],
          showdata:[]
          }
        */
        scope.curChartParams = undefined;
        scope.changeGroup = function(item){
          ChannelTest.chartChangeGroup(scope,chart,item);
          scope.ChartDataGrid.api.setRowData(scope.curChartParams.allshowdata);
        };
        // limit selected
        scope.changeLimit = function(){
          ChannelTest.chartChangeLimit(scope,chart);
        };
        // sorting selected
        scope.sortinglist = ['X軸字典排序','yield summary/auto數值排序','yield summary/manual數值排序'];
        scope.changeSorting = function(changeItem){
          ChannelTest.chartChangeSorting(scope,chart,changeItem);
        };
        /**
          Drill down control
        */
        scope.drillDownTriggerChangeGroup=false;
        scope.drillTablist = [];
        scope.drillDown = function(graphItemCategory){
          ChannelTest.chartDrillDown(scope,chart,graphItemCategory);
        };
        scope.triggerCloseTab = false;
        scope.selectedTabItemIndex = 0;
        scope.closeTab = function(tabindex){
          // tabindex: index of tab, drillDownTabActive: id of tab
          scope.triggerCloseTab = true; // click close tab button will trigger closeTab function first, and then changeDrillDownTab
          setTimeout(function(){ // don't set time out will route to home page...tricky bug
            scope.drillTablist.splice(tabindex,1);
            scope.drillDownTabActive=scope.drillTablist[0]?scope.drillTablist[0].id:0;
            scope.triggerCloseTab=false;
            scope.$apply();
          },250);
        };
        scope.showDutMap = function(){
          if(scope.curChartParams)
            return scope.curChartParams.dutMap?true:false;
          else
            return false;
        }
        /**
          Defined amchart options
        */
        var ballonFunction = common.amChartCustomizeBallonFunction();
        var chartOptions = {
          "type": "serial",
          "addClassNames": true,
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
            "useGraphSettings": true,
            "markerSize":12,
            "valueWidth":0,
            "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [ 
          {
            "id": "v1",
            "title": "summary/auto良率",
            "position": "left",
            "maximum":100,
            "labelFunction": function(value) {
              return value+"%";
            }
          },{
            "id": "v1",
            "title": "summary/manual良率",
            "position": "right",
            "maximum":100,
            "labelFunction": function(value) {
              return value+"%";
            }
          }],
          "startDuration": 1,
          "graphs": [{
            "id": "yield_sa",
            "balloonFunction": ballonFunction.percent,
            "lineColor":"#119828",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "summary/auto良率",
            "valueField": "yield_sa",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },{
            "id": "yield_sm",
            "balloonFunction": ballonFunction.percent,
            "lineColor":"#78B3ED",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "summary/manual良率",
            "valueField": "yield_sm",
            "valueAxis": "v2",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },{
            "clustered": false,
            "visibleInLegend": false,
            "fillAlphas": 0,
            "lineAlpha": 0,
            "showBalloon": false,
            "type": "column",
            "valueField": "maxvalue"
          }// invisible graph, using to trigger click event more easier
          ],
          "categoryField": "key",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":5
          },
          "export": {
            "enabled": true
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
          "chartScrollbar": {
          }
        };

				var chart = AmCharts.makeChart(chartName, chartOptions);
        var zoomChart = function(){
          try{
            chart.zoomToIndexes(0,15);
          }
          catch(e){} // data size less than 20
        }
        chart.addListener("dataUpdated", zoomChart);
        /**
          Add click event listener
        */
        var setClickListener = function(chart){
          function clickHandler(event) {
            var clickItem = scope.GroupByMapping[scope.curChartParams.groupby.selected]+': '+event.item.category;
            var ok = confirm('Drill down from '+clickItem);
            if(ok){
              scope.ddpath = '爐號: '+scope.GroupByMapping[scope.curChartParams.groupby.selected]+', 時間: '+event.item.category;
              scope.drillChartData = _.filter(scope.rawData,function(o){
                return (o['oven_ID']==scope.curChartParams.groupby.selected && o['log_time']==event.item.category);
              });
              scope.drillTablist.push({title:scope.ddpath,data:scope.drillChartData,id:new Date().getTime()}); 
              setTimeout(function(){
                scope.$apply();
                window.scrollTo(0,document.body.scrollHeight);
                setTimeout(function(){
                  scope.drillDownTabActive = scope.drillTablist[scope.drillTablist.length-1].id;
                  scope.$apply();
                },250);
              },100);
            }
          };
          chart.addListener("clickGraphItem", clickHandler);
        };
        setClickListener(chart);
        /**
          Whating the data link of outer controller
        */
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined)
            return;
          var rawdata = scope.data;
          scope.rawData = scope.data;
          if(_.isArray(rawdata)){
            if(rawdata.length>0){
              scope.GroupByMapping = ChannelTest.hisOvenIDgroupBy(rawdata);
              scope.display = true;
              scope.curChartParams = ChannelTest.initHisChartParams(rawdata);
              scope.drillTablist = [];
              chart.dataProvider = scope.curChartParams.showdata;
              chart.validateData();
              chart.validateNow();
              scope.ChartDataGrid.api.setRowData(scope.curChartParams.allshowdata);
              setTimeout(function(){
                scope.drillDownTabActive = 0;
                scope.$apply();
              },100);
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
				chart.validateData();
				chart.validateNow();
			},//link
    controller:['$scope',function($scope){
      /**
        Chart Data Grid
      */
      var chartDataGridColumnDefs = [
        {headerName: 'Time', field: 'key', filter: 'text', width: 150},
        {headerName: 'summary/auto良率(%)', field: 'yield_sa', filter: 'text', width: 180},
        {headerName: 'summary/manual良率(%)', field: 'yield_sm', filter: 'text', width: 180}
      ];
      $scope.ChartDataGrid = {
        columnDefs:chartDataGridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true
      };

      $scope.exportAgGrid = function(fileType){
        common.exportAgGrid('chartdata',fileType,$scope.ChartDataGrid);
      };
    }]
		};
	};

	angular.module('mean.ctest').directive('ctHisChart', ctHisChart);
	ctHisChart.$inject = ['ChannelTest','common','_'];
})();
'use strict';