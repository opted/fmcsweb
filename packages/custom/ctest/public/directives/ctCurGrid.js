
(function(){
	'use strict';
	function ctCurGrid(ChannelTest,common,_){
    var chartName = "ct-cur-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=' // Data {data:rawdata,ftype:{rebi:selectedtype,yield:selectedtype}}
			},
			template: '<div class="row" style="padding-left:20px;"><div style="height:350px;" class="ag-blue" ag-grid="curDataGrid"></div>\
      <button class="btn btn-success" ng-click="exportAgGrid(\'xls\')">下載Excel</button></div>',
			link: function (scope, element, attrs) {
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined)
            return;
          var rawdata = scope.data;
          if(_.isEmpty(rawdata)!=true){
            scope.display = true;
            // set ag-grid rowdata
            scope.rawdata = rawdata;
            scope.curDataGrid.api.setRowData(rawdata);
          }
          else
            scope.display = false;
				});
			},//link
    controller:['$scope',function($scope){
      var aggFun = function(nodes) {
        var result = {
          FailQty:0,
          TestQty:0
        };
        nodes.forEach( function(node) {
          var data = node.data;
          result.FailQty+=data.FailQty;
          result.TestQty+=data.TestQty;
        });
        return result;
      };
      $scope.curDataGrid = {
        columnDefs:ChannelTest.getGroupGridColumnDefs(),
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true,
        groupSuppressAutoColumn: true,
        groupRowAggNodes: aggFun
      };
      $scope.exportAgGrid = function(fileType){
        common.exportAgGrid('current_rawdata',fileType,$scope.curDataGrid);
      };
    }]
		};
	};

	angular.module('mean.ctest').directive('ctCurGrid', ctCurGrid);
	ctCurGrid.$inject = ['ChannelTest','common','_'];
})();
'use strict';