
(function(){
	'use strict';
	function ctCurChart(ChannelTest,common,_){
    var chartName = "ct-cur-chart";
		return {
			restrict: 'E',
			scope: {
				data: '=', // Data {data:rawdata,ftype:{rebi:selectedtype,yield:selectedtype}}
        chartid: '@?'
			},
			template: 
      // drilldown tab list
      '<div ng-show="display"><uib-tabset active="drillDownTabActive">\
        <uib-tab ng-repeat="tab in drillTablist" ng-click="changeDrillDownTab($index)" index="tab.id">\
        <uib-tab-heading>\
          {{tab.title}}<button type="button" class="close" aria-label="Close" style="font-size:medium" ng-click="closeTab($index)" ng-show="$index>0">\
          <span aria-hidden="true">&times;</span>\
          </button>\
        </uib-tab-heading>\
        </uib-tab>\
      </uib-tabset>'+
      // tab title
      '<h3>{{ curChartParams.tabtitle }}</h3>'+
      // grouping button
      // render all pill tabset and hide unneeded tabset to fix a bug
      '<div ng-repeat="tab in drillTablist"><uib-tabset class="rebi-groupby" type="pills" ng-show="selectedTabItemIndex==$index">\
      <uib-tab index="$index" ng-repeat="item in tab.chartparams.groupby.itemlist" heading="{{GroupByMapping[item]}}" select="changeGroup(item)">\
      </uib-tab>\
      </uib-tabset></div>'+
      // using hint
      '<div class="row">\
      <label class="text-primary control-label col-sm-12 vcenter">* 點擊一次 Drill down</label>\
      <label class="text-primary control-label col-sm-12 vcenter">* 點擊兩次顯示Dut疊圖</label></div>'+
      // sorting select
      '<div class="form-inline" style="margin-top:5px;margin-left:10px;">\
        <label class="text-default">排序</label>\
        <select vcenter" class="form-control" ng-model="curChartParams.sorting.selected" ng-options="opt for opt in sortinglist" ng-change="changeSorting(0)">\
        </select>\
        <div class="btn-group vcenter">\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="0" ng-click="changeSorting(1)">降序</label>\
        <label class="btn btn-primary" ng-model="curChartParams.sorting.type" uib-btn-radio="1" ng-click="changeSorting(2)">升序</label>\
        </div>\
      </div>'+
      // amchart
      '<div id="{{chartName}}" style="width:100%;height: 300px; margin:20px;"><div></div></div>'+
      '<div class="row" style="padding-left:20px;"><div style="height:180px;" class="ag-blue" ag-grid="ChartDataGrid"></div>\
      <button class="btn btn-success" ng-click="exportAgGrid(\'chartdata\',\'xls\')">下載Excel</button></div>'+
      '<div class="stackmap" ng-show="showDutMap()"><hr><h3>疊圖分析</h3>\
      <label class="text-default">Stack By: {{curChartParams.dutMap.by}}</label>\
      <dut-map rawdata="curChartParams.dutMap.data"></dut-map>\
      <hr><h3>疊圖 Raw Data</h3>\
      <div class="row" style="padding-left:20px;"><div style="height:300px;" class="ag-blue" ag-grid="RawDataGrid"></div>\
        <button class="btn btn-success" ng-click="exportAgGrid(\'rawdata\',\'xls\')">下載Excel</button></div>',
			link: function (scope, element, attrs) {
        scope.display = false;
        scope.chartName = scope.chartid?(chartName+'-'+scope.chartid):chartName;
        /**
          Chart parameters, use to drilldown, sorting, limit or change group by
        */
        /*
          Chart params structure
          {
          level:,//drill down 等級，初始等級為0
          groupby:{itemlist:["LOTNO","ROOTLOT",...],selected:"LOTNO",active:0}, // 分群用項目
          sorting:{selected:'X軸字典排序',type:0},//可用的排序規則記在外部
          limit:{selected:'X軸字典排序',type:0}, //圖表呈現上限
          drillpath:"",
          rawdata:[],
          showdata:[]
          }
        */
        scope.curChartParams = undefined;
        scope.GroupByMapping = ChannelTest.groupByMapping();
        scope.changeGroup = function(item){
          ChannelTest.chartChangeGroup(scope,chart,item);
          scope.ChartDataGrid.api.setRowData(scope.curChartParams.allshowdata);
        };
        // limit selected
        scope.changeLimit = function(){
          ChannelTest.chartChangeLimit(scope,chart);
        };
        // sorting selected
        scope.sortinglist = ['X軸字典排序','yield 數值排序','failcount 數值排序'];
        scope.changeSorting = function(changeItem){
          ChannelTest.chartChangeSorting(scope,chart,changeItem);
        };
        /**
          Drill down control
        */
        scope.drillDownTriggerChangeGroup=false;
        scope.drillTablist = [];
        scope.drillDown = function(graphItemCategory){
          ChannelTest.chartDrillDown(scope,chart,graphItemCategory);
        };
        scope.triggerCloseTab = false;
        scope.selectedTabItemIndex = 0;
        scope.changeDrillDownTab = function(index){
          ChannelTest.chartChangeDrillDownTab(scope,chart,index);
          if(scope.curChartParams.dutMap){
            scope.DutMapGrid.api.setRowData(scope.curChartParams.dutMap.data);
          }
        };
        scope.closeTab = function(tabindex){
          ChannelTest.chartCloseTab(scope,tabindex);
        };
        scope.showDutMap = function(){
          if(scope.curChartParams)
            return scope.curChartParams.dutMap?true:false;
          else
            return false;
        }
        /**
          Defined amchart options
        */
        var ballonFunction = common.amChartCustomizeBallonFunction();
        var chartOptions = {
          "type": "serial",
          "addClassNames": true,
          "balloon": {
            "adjustBorderColor": false,
            "horizontalPadding": 10,
            "verticalPadding": 8,
            "color": "#ffffff"
          },
          "legend": {
            "useGraphSettings": true,
            "markerSize":12,
            "valueWidth":0,
            "verticalGap":0
          },
          "dataProvider": [],
          "valueAxes": [ {
            "id": "v1",
            "title": "錯誤總量",
            "position": "left"
          },
          {
            "id": "v2",
            "title": "良率",
            "position": "right",
            "maximum":100,
            "labelFunction": function(value) {
              return value+"%";
            }
          }],
          "startDuration": 1,
          "graphs": [{
            "clustered": false,
            "visibleInLegend": false,
            "fillAlphas": 0,
            "lineAlpha": 0,
            "showBalloon": false,
            "type": "column",
            "valueField": "maxvalue"
          },// invisible graph, using to trigger click event more easier
          {
            "id":"failcount",
            "balloonFunction": ballonFunction.basic,
            "lineColor":"#FFA07A",
            "fillAlphas": 1,
            "title": "錯誤總量",
            "type": "column",
            "valueField": "failcount",
            "valueAxis": "v1",
            "dashLengthField": "dashLengthColumn",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          },
          {
            "id": "yield",
            "balloonFunction": ballonFunction.percent,
            "lineColor":"#119828",
            "bullet": "round",
            "lineThickness": 3,
            "bulletSize": 7,
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "useLineColorForBulletBorder": true,
            "bulletBorderThickness": 3,
            "fillAlphas": 0,
            "lineAlpha": 1,
            "title": "良率",
            "valueField": "yield",
            "valueAxis": "v2",
            "dashLengthField": "dashLengthLine",
            "labelText": "[[value]]",
            "showAllValueLabels":true
          }
          ],
          "categoryField": "key",
          "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "tickLength": 5,
            "autoRotateAngle":-8,
            "autoRotateCount":10
          },
          "export": {
            "enabled": true
          },
          "chartCursor": {
            "categoryBalloonDateFormat": "DD",
            "cursorAlpha": 0.1,
            "cursorColor":"#000000",
            "fullWidth":true,
            "valueBalloonsEnabled": false,
            "zoomable": false
          },
          "chartScrollbar": {
          }
        };

        var zoomChart = function(){
          try{
            chart.zoomToIndexes(0,20);
          }
          catch(e){} // data size less than 20
        }
        /**
          Add double click handler for amchart listener
        */
        var setClickListener = function(chart){
          chart.clickTimeout = 0;
          chart.lastClick = 0;
          chart.doubleClickDuration = 300;
          function clickHandler(event) {
            var clickItem = scope.GroupByMapping[scope.curChartParams.groupby.selected]+': '+event.item.category;
            var ts = (new Date()).getTime();
            // [[ Double click ]], get raw data
            if ((ts - chart.lastClick) < chart.doubleClickDuration) {
              if (chart.clickTimeout) {
                clearTimeout(chart.clickTimeout);
              }
              chart.lastClick = 0; // reset last click
              var ok = confirm('Get raw data from '+clickItem);
              if(ok){
                // Get dut stack map for click board id
                var drillpath = scope.curChartParams.drillpath;
                var filterCondition = {};
                drillpath.forEach(function(pathItem){
                  for(var key in pathItem){
                    filterCondition[key]=pathItem[key];
                  }
                });
                filterCondition[scope.curChartParams.groupby.selected] = event.item.category;
                var stackmapdata = _.filter(scope.rawData,filterCondition);
                scope.curChartParams.dutMap = {'data':_.filter(scope.rawData,filterCondition),'by':clickItem};
                // set dut map grid rawdata
                // set ag-grid rowdata
                scope.RawDataGrid.api.setRowData(stackmapdata);
                setTimeout(function(){
                  scope.$apply();
                  window.scrollTo(0,document.body.scrollHeight);
                },100);
              }
            }
            // [[ Single click ]], drill down
            else {
              chart.clickTimeout = setTimeout(function(){
                // trigger drill down
                var ok = confirm('Drill down from '+clickItem);
                if(ok){
                  scope.drillDownTriggerChangeGroup = true;
                  scope.drillDown(event.item.category);
                }
              }, chart.doubleClickDuration);
            }
            chart.lastClick = ts;
          };
          chart.addListener("clickGraphItem", clickHandler);
        };
        var chart = undefined;
        /**
          Whating the data link of outer controller
        */
				scope.$watchGroup(['data'], function(){ // Change of scope.data, Only trigger by http query
          if(scope.data==undefined)
            return;
          var rawdata = scope.data;
          scope.rawData = scope.data;
          if(_.isArray(rawdata)){
            if(rawdata.length>0){
              scope.display = true;
              chart = AmCharts.makeChart(scope.chartName, chartOptions);
              chart.addListener("dataUpdated", zoomChart);
              setClickListener(chart);
              scope.curChartParams = ChannelTest.initCurChartParams(rawdata);
              scope.drillTablist = [];
              scope.drillTablist.push(ChannelTest.getInitDrillTab(scope.curChartParams)); 
              chart.dataProvider = scope.curChartParams.showdata;
              chart.validateData();
              chart.validateNow();
              scope.ChartDataGrid.api.setRowData(scope.curChartParams.allshowdata);
              setTimeout(function(){
                scope.drillDownTabActive = scope.drillTablist[0].id;;
                scope.$apply();
              },100);
            }
            else
              scope.display = false;
          }
          else
            scope.display = false;
				});
        if(chart){
          chart.validateData();
          chart.validateNow();
        }
			},//link
    controller:['$scope',function($scope){
      /**
        Chart Data Grid
      */
      var chartDataGridColumnDefs = [
        {headerName: 'KEY', field: 'key', filter: 'text', width: 150},
        {headerName: '良率(%)', field: 'yield', filter: 'text', width: 100},
        {headerName: '錯誤數量', field: 'failcount', filter: 'text', width: 100}
      ];
      $scope.ChartDataGrid = {
        columnDefs:chartDataGridColumnDefs,
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true,
        groupSuppressAutoColumn: true
      };
      $scope.RawDataGrid = {
        columnDefs:ChannelTest.getDataGridColumnDefs(),
        rowData:[],
        groupUseEntireRow: false,
        enableSorting: true,
        enableColResize: true,
        enableRangeSelection: true,
        groupSuppressAutoColumn: true
      };
      $scope.exportAgGrid = function(fileName,fileType){
        if(fileName=='chartdata')
          common.exportAgGrid(fileName,fileType,$scope.ChartDataGrid);
        else if(fileName=='rawdata')
          common.exportAgGrid(fileName,fileType,$scope.RawDataGrid);
      };
    }]
		};
	};

	angular.module('mean.ctest').directive('ctCurChart', ctCurChart);
	ctCurChart.$inject = ['ChannelTest','common','_'];
})();
'use strict';