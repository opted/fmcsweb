'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Q = require('q'),
    _ = require('lodash');
var mbisqlCtrl = require('../../../../custom/mbisql/server/controllers/mbisql.js'),
    oqtool = require('../../../../custom/rebi/server/controllers/odbQueryTool.js');
var mbiconn = undefined, mesconn=undefined;//new mesclass();

var getMbiTableList = function(from,to){
  return mbisqlCtrl.getMBITableList(from,to);
}

var setExtraColumn = function(result){
  result.forEach(function(o){
    // set missing column
    o['BID'] = o['board_id'].slice(1,4);
    o['oven_ID'] = 'O'+o['oven_ID'].slice(1);
    o['OZS'] = o['oven_ID']+'/Z'+o['Zone_ID']+'/S'+o['Slot_ID'];
    o['lotnum'] = _.trimEnd(o['lotnum']);
    o['DAY'] = o['log_time'].toISOString().slice(0,10);
    o['TestQty']=0;
    o['FailQty']=0;
    var binmap = o['binmap1']+o['binmap2'];
    // Fill Dut
    for(var i=0;i<binmap.length;i++){
      if(binmap[i]==1)
        o['TestQty']+=1;
      else if(binmap[i]>1){
        o['TestQty']+=1;
        o['FailQty']+=1;
      }
    }
  });
};

exports.initMbiConnection = function(utils){
  mbiconn = utils.ctrls.mbi;
  mesconn = utils.ctrls.mes;
};

var getQuery = function(req, res, next, type){
  req.setTimeout(180000); // 30 mins
  var match = req.body.match;
  var time = req.body.time;
  time.to = new Date(time.to);
  time.to = new Date(time.to.setSeconds(59,0));
  time.from = new Date(time.from);
  // Change Time Zone, mbi table store UTC+8 time to UTC+0
  time.to = new Date(time.to.setHours(time.to.getHours()+8));
  time.from = new Date(time.from.setHours(time.from.getHours()+8));
  var qcmdlist = [];
  // Channel Test 只會在 OD, OG, OI, OC 開頭的爐號進行
  var regexp_like_ooven = "(oven_ID like 'OC%' or oven_ID like 'OD%' or oven_ID like 'OG%' or oven_ID like 'OI%')";
  var matchStr = '';
  // find lotno by field CustomerName and CustomerLotNo from MES Table
  var leader_tablename='';
  if(type=='cur')
    leader_tablename='A.';
  for(var key in match){
    if(matchStr.length>0)
      matchStr+=" and";
    if(key.startsWith('ZONE') || key.startsWith('SLOT'))
      matchStr+=" "+leader_tablename+key+" in ("+match[key]+")";
    else if(key.startsWith('BID'))
      matchStr+=" SUBSTRING("+leader_tablename+"board_id,2,3)='"+match[key]+"'";
    else
      matchStr+=" "+leader_tablename+key+"='"+match[key]+"'";
  }
  if(type=='cur'){
    if(matchStr.length>0)
      matchStr=' where'+matchStr;
  }
  else{
    if(matchStr.length>0)
      matchStr=' and'+matchStr;
  }
  
  if(type=='cur'){
    // 現況分析搜尋
    // time.to 三個月內最新 Chanel Test 資料
    // 取 time.from 應當為 time.to 前三個月當月1日0時0分0秒
    var mbiTableList = getMbiTableList(time.from,time.to);
    mbiTableList.forEach(function(table){
      var qcmd = "select A.* from "+table+" A inner join (select oven_ID,max(log_time) log_time from "+table+" where oven_ID like 'M%' and flowname='summary' and (BlockName='manual' or BlockName='auto') group by oven_ID) B on (A.oven_ID=B.oven_ID and A.log_time=B.log_time)"+matchStr;
      qcmdlist.push(mbiconn.exec(qcmd,{}));
    });
    Q.all(qcmdlist).then(function(cur_result){
      cur_result = _.flatten(cur_result);
      // Aggregate by ovenid + driverid and pick last one (取得某爐某driverid在固定區間內的最新一筆資料)
      var new_cur_result = [];
      var group_cur_result = _.groupBy(cur_result,function(o){return o.oven_ID+o.driver_id})
      for(var key in group_cur_result){
        new_cur_result.push(_.orderBy(group_cur_result[key],['log_time'],['desc'])[0]);
      }
      cur_result = new_cur_result;
      // no channel test 搜尋
      var qcmdlist2 = [];
      mbiTableList.forEach(function(table){
        var qcmd = "select distinct oven_ID from "+table+" where "+regexp_like_ooven+" or oven_ID like 'M%'";
        qcmdlist2.push(mbiconn.exec(qcmd,{}));
      });
      Q.all(qcmdlist2).then(function(noctoven_result){
        noctoven_result = _.uniq(_.map(_.flatten(noctoven_result),'oven_ID'));
        var oovenlist = _.filter(noctoven_result,function(o){return o.startsWith('O')});
        var movenlist = _.filter(noctoven_result,function(o){return o.startsWith('M')});
        var noctovenlist = [], count=0;
        oovenlist = _.orderBy(oovenlist);
        oovenlist.forEach(function(ooven){
          var moven = 'M'+ooven.slice(1);
          //save item into n*10 2d array
          if(movenlist.indexOf(moven)<0){
            if(count%10==0)
              noctovenlist.push([]);
            noctovenlist[Math.floor(count/10)].push(ooven);
            count+=1;
          }
        });
        setExtraColumn(cur_result);
        return res.json({cur:cur_result,noctovens:noctovenlist});
      })
      .catch(function(err){
        console.log(err.stack);
        return res.status(400).json('noctoven query error! '+err);
      });
    })
    .catch(function(err){
      console.log(err.stack);
      return res.status(400).json('cur query error! '+err);
    });
  }
  else if(type=='his'){
    // 歷史分析
    var params = {to:new Date(time.to),from:new Date(time.from)};
    var mbiTableList = getMbiTableList(time.from,time.to);
    mbiTableList.forEach(function(table){
      var qcmd = "select * from "+table+" where log_time between @from and @to and oven_ID like 'M%' and flowname='summary' and (BlockName='manual' or BlockName='auto')"+matchStr;
      qcmdlist.push(mbiconn.exec(qcmd,params));
    });
    Q.all(qcmdlist).then(function(his_result){
      his_result = _.flatten(his_result);
      setExtraColumn(his_result);
      return res.json({his:his_result});
    })
    .catch(function(err){
      console.log(err.stack);
      return res.status(400).json('history query error! '+err);
    });
  }
};

exports.curQuery = function(req, res, next) {
  return getQuery(req,res,next,'cur');
};

exports.hisQuery = function(req, res, next) {
  return getQuery(req,res,next,'his');
};