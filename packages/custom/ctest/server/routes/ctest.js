(function() {
  'use strict';
  /* jshint -W098 */
  var ctestCtrl = require('../controllers/ctest');
  module.exports = function(MBISQL, app, auth, database,utils) {
    ctestCtrl .initMbiConnection(utils);
    app.post('/api/ctest/querydata/cur', ctestCtrl.curQuery);
    app.post('/api/ctest/querydata/his', ctestCtrl.hisQuery);
  };
})();
