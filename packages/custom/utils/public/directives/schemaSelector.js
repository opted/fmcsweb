(function(){
  'use strict';

  function schemaSelector(){
    return {
      restrict: 'E',
      scope: {
        schema: "="
      },
      template: `
        <div class="utils-schema">
          <form class="form-inline">
            <label uib-popover="切換必須重新查詢" popover-trigger="'mouseenter'"> 資料源: </label>
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-default active" ng-click="schema = 'HISMFT'">
                <input type="radio" name="options"> MFT
              </label>
              <label class="btn btn-default" ng-click="schema = 'HISLFT'">
                <input type="radio" name="options"> LFT
              </label>
            </div>
          </form>
        </div>
      `
    };
  };
  
  angular.module('mean.utils').directive('schemaSelector', schemaSelector);
  schemaSelector.$inject = [];
})();