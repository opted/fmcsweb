(function(){
  'use strict';
  function searchDateSwitchV1(utilsSrchOpt){
    return {
      restrict: 'E',
      scope: {
        selected: "="
      },
      template: 
        `<form class="form-inline" role="form" style="margin-bottom: 15px;">
          <div ng-show="dateType.selected=='日'" class="form-group">
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 起始時間: </label>
              <input class="form-control" ng-model="selected.from" type="date" ng-blur="checkErr()" ng-required="require == true">\
            </div>
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 結束時間: </label>
              <input class="form-control" ng-model="selected.to" type="date" ng-blur="checkErr()" ng-required="require == true">
            </div>
          </div>
          <div ng-show="dateType.selected=='週'" class="form-group">
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 起始時間: </label>
              <input class="form-control" ng-model="selected.from" type="week" ng-blur="checkErr()" ng-required="require == true">\
            </div>
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 結束時間: </label>
              <input class="form-control" ng-model="selected.to" type="week" ng-blur="checkErr()" ng-required="require == true">
            </div>
          </div>
          <div ng-show="dateType.selected=='月'" class="form-group">
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 起始時間: </label>
              <input class="form-control" ng-model="selected.from" type="month" ng-blur="checkErr()" ng-required="require == true">\
            </div>
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 結束時間: </label>
              <input class="form-control" ng-model="selected.to" type="month" ng-blur="checkErr()" ng-required="require == true">
            </div>
          </div>
          <select class="form-control" ng-model="dateType.selected" ng-change="changeType()" ng-options="opt for opt in dateType.itemlist">
          </select>
        </form>`
      ,
      link: function(scope, element, attrs){
        var preFrom = scope.selected.from;
        var preTo = scope.selected.to;
        var setCompleteInterval = function(){
          // set accurate whole week and whole month
          if(scope.dateType.selected=='週'){
            var from = scope.selected.from, fromday=scope.selected.from.getDay();
            var to = scope.selected.to, today=scope.selected.to.getDay();
            var fromDiff = from.getDate() - fromday,
              toDiff = to.getDate() + 6 - today;
            var prefromyear = preFrom.getFullYear(), pretoyear = preTo.getFullYear();
            from = new Date(scope.selected.from.setDate(fromDiff)); // set day to sunday
            to = new Date(scope.selected.to.setDate(toDiff)); // set day to saturday
            if(from.getFullYear()!=prefromyear)
              from = new Date(new Date(prefromyear.toString()).setHours(0,0,0));
            if(to.getFullYear()!=pretoyear)
              to = new Date(new Date(pretoyear.toString()+'-31').setHours(23,59,59));
            scope.selected.from = from; // set day to sunday, or a year first day
            scope.selected.to = to; // set day to saturday, or a year last day
          }
          else if(scope.dateType.selected=='月'){
            // set date to 1
            scope.selected.from = new Date(scope.selected.from.setDate(1));
            var tomonth = scope.selected.to.getMonth();
            // set date to last day
            scope.selected.to = new Date(new Date(scope.selected.to.setMonth(tomonth+1)).setDate(0));
          }
        };
        scope.dateType = {selected:'日',itemlist:['日','週','月'],itemmap:{'日':'day','週':'week','月':'month'}};
        scope.changeType = function(){
          if(scope.selected.datetype)
            scope.selected.datetype = scope.dateType.itemmap[scope.dateType.selected];
          setCompleteInterval();
        };
        scope.checkErr = function(){
          if (new Date(scope.selected.from) > new Date(scope.selected.to)){
            alert("起始時間 > 結束時間，請重新設定...");
            scope.selected.from = preFrom;
            scope.selected.to = preTo;
          } else {
            preFrom = scope.selected.from;
            preTo = scope.selected.to;
            setCompleteInterval();
          }
        }
      }
    };
  };
  
  angular.module('mean.utils').directive('searchDateSwitchV1', searchDateSwitchV1);
  searchDateSwitchV1.$inject = ['utilsSrchOpt'];
})();