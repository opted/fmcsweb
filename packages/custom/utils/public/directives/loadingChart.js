(function(){
  'use strict';

  function LoadingChart(){
    return {
      restrict: 'C',
      scope: {
        cid: '@',
        isLoading: '=',
        loadingText: '@?'
      },
      transclude: true,
      template: `
        <div class="loading-cht-container" ng-style="sty">
          <div id="{{cid}}" class="cht-content"></div>
          <div class="curtain cht-content" ng-show="isLoading"><span class="loading-blink">{{loadingText}}</span></div>
        </div>
      `,
      link: function(scope, element, attrs){
        if(scope.loadingText==undefined)
          scope.loadingText = 'Chart is loading...';
      }
    };
  };
  
  angular.module('mean.utils').directive('utilsLoadingChart', LoadingChart);
  LoadingChart.$inject = [];
})();