(function(){
  'use strict';

  function selector(){
    return {
      restrict: 'E',
      scope: {
        pick: '=',
        list: '=',
        key: '='
      },
      transclude: true,
      template: `
        <div class="form-group" style="margin-left: 10px;margin-top: 5px; margin-buttom: 5px; margin-right: 10px;">
          <label for="repeatSelect" ng-transclude></label>
          <select name="repeatSelect" ng-model="pick" class="form-control" ng-options="g[key] for g in list">
          </select>
        </div>
      `,
    };
  };
  
  angular.module('mean.utils').directive('utilsSimpleSelector', selector);
  selector.$inject = [];
})();