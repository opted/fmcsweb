(function(){
  'use strict';

  function hintModal(utilsSrchOpt,$uibModal){
    return {
      restrict: 'E',
      scope: {
        // @ for binding text, = two-ways binding, & binding function
        hintText: '=', // hint text can be html style write in string
        title: '@?',//? symbol for optional
        btnText: '@?',
        mdSize: '@?',
        btnClass: '@?',
        okFunction: '=?',
      },
      template: `<button type="button" class="{{btnClass}}" ng-click="callHintModal()">{{btnText}}</button>`,
      link: function(scope, element, attrs){
        //defined title
        if(scope.title==undefined)
          scope.title = 'Hint';
        if(scope.btnText==undefined)
          scope.btnText = 'Hint';
        var mdSize='md';
        if(scope.mdSize=='sm' || scope.mdSize=='lg')
          mdSize = scope.mdSize;
        if(scope.btnClass==undefined)
          scope.btnClass = 'btn btn-primary';
        scope.callHintModal = function(){
          scope.modalInstance = $uibModal.open({
            templateUrl: 'utils/views/hintModal.html',
            size: mdSize,
            scope: scope
          });
        };
        scope.close = function () {
          scope.modalInstance.dismiss('cancel');
        };
        // defined ok function
        if(scope.okFunction==undefined)
          scope.ok = scope.close;
        else
          scope.ok = function(){
          scope.okFunction();
          scope.close();
        };
      }
    };
  };
  
  angular.module('mean.utils').directive('hintModal', hintModal);
  hintModal.$inject = ['utilsSrchOpt','$uibModal'];
})();