(function(){
  'use strict';

  function searchFilter(_, $http, $window, utilsSrchOpt){
    return {
      restrict: 'E',
      scope: {
        field: "=",
        fields: "=",
        queryUrl: "=",
        extraMatch: "=?" // additional field query which not in fields. p.s. need to convert datetime from string to date at bakend
      },
      templateUrl: 'utils/views/search/filter.html',
      link: function(scope, element, attrs){
        scope.getField = function($select){
          var searchStr = $select.search;
          scope.field.selected = searchStr;
          scope.field.searchField = $select; // keep field's info object

          if (!scope.field.needSearch){
            return;
          }

          if (!_.isEmpty(searchStr)) {
            if (searchStr.slice(-1) != "*" && searchStr.search(",") == -1) { // Query if without * and comma
              var match = utilsSrchOpt.getMatch(scope.fields);
              $http.post(scope.queryUrl, {
                'field': scope.field.name,
                'regex': searchStr,
                'match': match,
                'exmatch': scope.extraMatch
              }).then(function(res) {
                // set field selections
                scope.field.filteredData = res.data;
              }, function(err){
                $window.alert("Server failed...");
              });
            }
          } else {
            scope.field.filteredData = null; // clear the drop down list
          }
        };
        
        scope.syncSearchField = function ($select) {
          $select.search = scope.field.selected;
        };
      }
    };
  };
  
  angular.module('mean.utils').directive('searchFilter', searchFilter);
  searchFilter.$inject = ['_', '$http', '$window', 'utilsSrchOpt'];
})();