/*
mananger: jon-yi
email: jhong-yi.chen@alpha-info.com.tw
*/

(function(){
  'use strict';

  function sheetImport(_){
    return {
      scope: {
        data: "=",
        oneCol: "=",
      },
      templateUrl: 'utils/views/sheetImport.html',
      link: function(scope, element, attrs){
        /*
          [功能] 
            頁面上傳單一 excel 並讀取第一個 sheet 資料

          [使用方式]
            1. 將 data binding 視為 ag-grid 的 gridOption 來輸出
               ex:
                 <sheet-import data="gridOption"></sheet-import>

            2. 若僅讀 excel 的第一 column 取得資料，oneCol 設成 true
               ex: 
                 <sheet-import data="data" one-col="true"></sheet-import>

          [Ref] https://github.com/SheetJS/js-xlsx#parsing-options
        */
        element.on('change', function (changeEvent) {
          var reader = new FileReader();

          reader.onload = function (e) {
            /* read workbook */
            var bstr = e.target.result;
            var wb = XLSX.read(bstr, {type:'binary'});

            /* grab first sheet */
            var wsname = wb.SheetNames[0];
            var ws = wb.Sheets[wsname];

            var aoa = XLSX.utils.sheet_to_json(ws, {header:1, raw:false});

            scope.$apply(function() {              
              if (scope.oneCol == true) {
                scope.data = _.map(aoa, a => a[0]);
              } else {
                var cols = _.map(aoa[0], function(c){return {field: c};});
                scope.data = typeof scope.data !== 'undefined' ? scope.data : {};
                scope.data.columnDefs = cols;
                scope.data.data = _.map(aoa.slice(1, aoa.length), a => _.zipObject(_.map(cols, "field"), a));
              }
            });
          };

          reader.readAsBinaryString(changeEvent.target.files[0]);
        });

        scope.clear = function(){
          element.find(`#sheetinput_${scope.$id}`).val('');
          scope.data = {};
        }

        scope.setDefault = function(){
          element.find(`#sheetinput_${scope.$id}`).val('');
        }
      }
    };
  };
  
  angular.module('mean.utils').directive('sheetImport', sheetImport);
  sheetImport.$inject = ['_']
})();