(function(){
  'use strict';

  function searchDatetime(utilsSrchOpt){
    return {
      restrict: 'E',
      scope: {
        selected: "="
      },
      template: `
        <form class="form-inline" role="form" style="margin-bottom: 15px;">
          <div class="form-group" style="margin-right: 20px;">
            <label class="text-primary"> 起始時間: </label>
            <input class="form-control" ng-model="selected.from" type="datetime-local" ng-blur='checkErr()'>
          </div>
          <div class="form-group" style="margin-right: 20px;">
            <label class="text-primary"> 結束時間: </label>
            <input class="form-control" ng-model="selected.to" type="datetime-local" ng-blur='checkErr()'>
          </div>
        </form>
      `,
      link: function(scope, element, attrs){
        var preFrom = scope.selected.from;;
        var preTo = scope.selected.to;

        scope.checkErr = function(){
          if (new Date(scope.selected.from) > new Date(scope.selected.to)){
            alert("起始時間 > 結束時間，請重新設定...");
            scope.selected.from = preFrom;
            scope.selected.to = preTo;
          } else {
            preFrom = scope.selected.from;
            preTo = scope.selected.to;
          }
        }
      }
    };
  };
  
  angular.module('mean.utils').directive('searchDatetime', searchDatetime);
  searchDatetime.$inject = ['utilsSrchOpt'];
})();
