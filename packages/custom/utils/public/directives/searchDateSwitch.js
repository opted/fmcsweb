(function(){
  'use strict';
  function searchDateSwitch(utilsSrchOpt,common,_){
    return {
      restrict: 'E',
      scope: {
        selected: "="
      },
      template: 
        `<form class="form-inline" role="form" style="margin-bottom: 15px;">
          <div class="form-group">
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 起始時間: </label>
              <input class="form-control" ng-model="selected.from" type="date" ng-blur="checkErr()" ng-required="require == true">\
            </div>
            <div class="form-group" style="margin-right: 20px;">
              <label class="text-primary"> 結束時間: </label>
              <input class="form-control" ng-model="selected.to" type="date" ng-blur="checkErr()" ng-required="require == true">
            </div>
          </div>
          <label class="text-primary"> 推移圖單位: </label>
          <select class="form-control" ng-model="dateType.selected" ng-change="changeType()" ng-options="opt for opt in dateType.itemlist" style="margin-right: 20px;">
          </select>
          <label class="text-primary" ng-show="timerange"> 時間區間: {{timerange}} </label>
        </form>`
      ,
      link: function(scope, element, attrs){
        scope.timerange = undefined;
        var preFrom = scope.selected.from;
        var preTo = scope.selected.to;
        var setCompleteInterval = function(){
          // set accurate whole week and whole month
          if(scope.dateType.selected=='週'){
            scope.timerange = scope.selected.from.getFullYear()+'年第'+_.padStart(common.getWeek(scope.selected.from),2,'0')+'週 ~ '+
              scope.selected.to.getFullYear()+'年第'+_.padStart(common.getWeek(scope.selected.to),2,'0')+'週';
          }
          else if(scope.dateType.selected=='月'){
            scope.timerange = scope.selected.from.getFullYear()+'年'+_.padStart(scope.selected.from.getMonth()+1,2,'0')+'月 ~ '+
              scope.selected.to.getFullYear()+'年'+_.padStart(scope.selected.to.getMonth()+1,2,'0')+'月';
          }
          else{
            scope.timerange = undefined;
          }
        };
        scope.dateType = {selected:'日',itemlist:['日','週','月'],itemmap:{'日':'day','週':'week','月':'month'}};
        scope.changeType = function(){
          if(scope.selected.datetype)
            scope.selected.datetype = scope.dateType.itemmap[scope.dateType.selected];
          setCompleteInterval();
        };
        scope.checkErr = function(){
          if (new Date(scope.selected.from) > new Date(scope.selected.to)){
            alert("起始時間 > 結束時間，請重新設定...");
            scope.selected.from = preFrom;
            scope.selected.to = preTo;
          } else {
            preFrom = scope.selected.from;
            preTo = scope.selected.to;
            setCompleteInterval();
          }
        }
      }
    };
  };
  
  angular.module('mean.utils').directive('searchDateSwitch', searchDateSwitch);
  searchDateSwitch.$inject = ['utilsSrchOpt','common','_'];
})();