(function(){
  'use strict';

  function searchDate(utilsSrchOpt){
    return {
      restrict: 'E',
      scope: {
        selected: "=",
        need: "=?"
      },
      template: `
        <form class="form-inline" role="form">
          <div class="form-group utils-srch-date">
            <label class="text-primary"> 起始時間: </label>
            <input class="form-control" ng-model="selected.from" type="date" ng-blur='checkErr()' ng-required="need == true">
          </div>
          <div class="form-group utils-srch-date">
            <label class="text-primary"> 結束時間: </label>
            <input class="form-control" ng-model="selected.to" type="date" ng-blur='checkErr()' ng-required="need == true">
          </div>
        </form>
      `,
      link: function(scope, element, attrs){
        var preFrom = scope.selected.from;;
        var preTo = scope.selected.to;

        scope.checkErr = function(){
          if (new Date(scope.selected.from) > new Date(scope.selected.to)){
            alert("起始時間 > 結束時間，請重新設定...");
            scope.selected.from = preFrom;
            scope.selected.to = preTo;
          } else {
            preFrom = scope.selected.from;
            preTo = scope.selected.to;
          }
        }
      }
    };
  };
  
  angular.module('mean.utils').directive('searchDate', searchDate);
  searchDate.$inject = ['utilsSrchOpt'];
})();