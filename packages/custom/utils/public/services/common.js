(function(){
  'use strict';
  function common(_){
    var setBallonText = function(graphDataItem,postfixSymbol){
      var value = graphDataItem.values.value;
      if(!isNaN(Number(value)))
        if(_.isNumber(Number(value)))
          value = Number(value).toLocaleString();
      var title = graphDataItem.graph.title;
      var category = graphDataItem.category;
      return "<span style='font-size:12px;'>"+title+" in "+category+":<br><span style='font-size:20px;'>"+value+postfixSymbol+"</span>[[additional]]</span>";
    };
    var getWeek = function(date,janone){
      return Math.ceil((((date - janone) / 86400000) - 1 + janone.getDay()) / 7);
    }
    return {
      secondsReadable: function(seconds){ // turn second to read able time, ex: turn 3650 seconds to 1 h 0 m 50 s
        var h=0, m=0, s=0;
        if(seconds>=3600){
          h=Math.floor(seconds/3600);
          seconds-=h*3600;
        }
        if(seconds>=60){
          m=Math.floor(seconds/60);
          seconds-=m*60;
        }
        s=seconds;
        if(h>0)
         return h+' h '+m+' m '+s+' s';
        else if(m>0)
         return m+' m '+s+' s';
        else
         return s+' s';
      },
      getToday: function(){
        var cur = new Date();  // Get current time
        cur.setHours(0, 0, 0, 0);  // Set to the start of day
        return cur;
      },
      exportAgGrid:function(fliename,type,agGrid){
        if (agGrid.api.getModel().getRowCount() < 1){
          alert("無資料可供下載");
          return;
        }
        var params = {
          fileName: fliename+'.'+type
        };
        if(type=='xls')
          agGrid.api.exportDataAsExcel(params);
        else if(type='csv')
          agGrid.api.exportDataAsCsv(params);
      },
      getChromeVersion(){
        var raw = navigator.userAgent.match(/Chrom(e|ium)\/([0-9]+)\./);
        return raw ? parseInt(raw[2], 10) : false;
      },
      getWeek: function(date) {
        // 星期一 00:00:00 會被視為上一週的最後一秒，強制調整秒數為 1
        date  = new Date(new Date(date).setSeconds(1));
        var janone = new Date(date.getFullYear(), 0, 1);
        var addnum = 0;
        // week range: Mon ~ Sun
        // 若當年的1月1日算成 week 0，需要自動加1
        if(getWeek(new Date(date.getFullYear().toString()),janone)==0)
          addnum=1;
        return getWeek(date,janone)+addnum;
      },
      setAmChartClickHandler: function(chart,params){ // params = {singleClick:{confirmText:,clickFunction:},doubleClick:{...},clickItemPrefixText:''}
        chart.clickTimeout = 0;
        chart.lastClick = 0;
        chart.doubleClickDuration = 300;
        function clickHandler(event) {
          var clickItem = event.item.category;
          var ts = (new Date()).getTime();
          // [[ Double click ]], get raw data
          if ((ts - chart.lastClick) < chart.doubleClickDuration) {
            if (chart.clickTimeout) {
              clearTimeout(chart.clickTimeout);
            }
            chart.lastClick = 0; // reset last click
            var ok = confirm(params.doubleClick.confirmText+clickItem);
            if(ok){
              params.doubleClick.clickFunction(event);
            }
          } 
          // [[ Single click ]], drill down
          else {
            chart.clickTimeout = setTimeout(function(){
              // trigger drill down
              var ok = confirm(params.singleClick.confirmText+clickItem);
              if(ok){
                params.singleClick.clickFunction(event);
              }
            }, chart.doubleClickDuration);
          }
          chart.lastClick = ts;
        };
        chart.addListener("clickGraphItem", clickHandler);
      },
      setWeekDay: function(date,weekday){
        // weekday: 0: sun, 1: mon, 2: tue, 3: wen, 4:thu, 5:fri, 6:sat
        var day=date.getDay();
        var diff = date.getDate() + weekday - day;
        return new Date(date.setDate(diff)); // set day to sunday
      },
      shiftDate: function(date,number,unit){
        var shiftdate = new Date(date);
        var oneDayMs = 24*60*60*1000;
        if(unit=='day'){ // 當日0時0分0秒往前推數日
          shiftdate = new Date(shiftdate.setHours(0,0,0));
          return new Date(shiftdate.getTime()-number*oneDayMs);
        }
        else if(unit=='week'){ // 當週日往前推數週
          shiftdate = this.setWeekDay(new Date(shiftdate.setHours(0,0,0)),0);
          return new Date(shiftdate.getTime()-number*oneDayMs*7);
        }
        if(unit=='month'){ // 當月1日往前推數月
          shiftdate = new Date(new Date(new Date(shiftdate.setDate(1)).setHours(0,0,0)).setMonth(shiftdate.getMonth()-number));
          return shiftdate;
        }
      },
      amChartCustomizeBallonFunction: function(){
        var basicBallonFunction = function(graphDataItem, graph){
          return setBallonText(graphDataItem,'');
        };
        var percentBallonFunction = function(graphDataItem, graph){
          return setBallonText(graphDataItem,'%');
        };
        return {
          'basic':basicBallonFunction,
          'percent':percentBallonFunction
        };
      },
	    agGridcellRendererAddPercent:function(params){
        return params.value+'%';
      },
      //處理混合數字的字串排序。使用此函式升序排列時，'OG29'會排在'OG1001'前，普通的字典排序輸出結果相反。
      //Arguments:
      //  input (Object)
      //  key (key by sorting)
      //  orders (order of sorting 'desc' or 'asc')
      //Example:
      //  common.sortingBy([{key:'OG23'},{key:'OG101'}],'key','desc');
      sortingBy:function(input,key,orders){
        let startswithNumber = false;
        if(input[0])
          if(!isNaN(Number(input[0][key][0]))) // isNumber
            startswithNumber = true;
            
        input.forEach((o)=>{
          if(startswithNumber){
            o[key+'p1'] = o[key].match(/\d+/)[0];
          }
          else{
            try{
              o[key+'p1'] = o[key].match(/[A-Z|a-z]+/)[0];
              o[key+'p2'] = Number(o[key].match(/\d+/)[0]);
            }
            catch(e){
             o[key+'p1'] = o[key];
            };
          }
        });
        input = _.orderBy(input,[key+'p1',key+'p2'],[orders,orders]);
        input.forEach((o)=>{
          delete o[key+'p1'];
          delete o[key+'p2'];
        });
        return input;
      }
    };
  };
  angular.module('mean.utils').factory('common', common);
  common.$inject = ['_'];
})();
