(function(){
    'use strict';
    function utilsSrchOpt(_){
        return {
            genField: function(label, name, type, isMain, needSearch, isShow, width){
                type = typeof type !== 'undefined' ? type : 'string';
                isMain = typeof isMain !== 'undefined' ? isMain : true;
                needSearch = typeof needSearch !== 'undefined' ? needSearch : true;
                isShow = typeof isShow !== 'undefined' ? isShow : true;
                width = typeof width !== 'undefined' ? width : 2;

                return {
                    "label": label,
                    "name": name,
                    "type": type,
                    "width": width,
                    "isShow": isShow,
                    "searchField": null,
                    "filteredData": null,
                    "selected": null,
                    "isMain": isMain,
                    "needSearch": needSearch
                };
            },
            getMatch: function(fields){
                return _.reduce(fields, (rslt, obj) => !_.isEmpty(obj.selected) ? _.set(rslt, obj.name, obj.selected) : rslt, {});
            }
        };
    };

    angular.module('mean.utils').factory('utilsSrchOpt', utilsSrchOpt);
    utilsSrchOpt.$inject = ['_'];
})();