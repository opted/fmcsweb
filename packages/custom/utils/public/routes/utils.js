(function() {
    'use strict';

    function Utils($stateProvider) {
        $stateProvider.state('utils main page', {
            url: '/utils',
            templateUrl: 'utils/views/index.html',
            controller:UtilsController,
            // resolve:{
            //   loggedin:function(MeanUser){
            //     return MeanUser.checkLoggedIn();
            //   }
            // }
        })
            .state('utils circles example', {
            url: '/utils/example/:circle',
            templateUrl: 'utils/views/example.html'
        });
    }

    angular
        .module('mean.utils')
        .config(Utils);

    Utils.$inject = ['$stateProvider'];

})();
