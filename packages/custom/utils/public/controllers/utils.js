'use strict';

/* jshint -W098 */
function UtilsController($scope, $window, $http) {
  
}

angular
  .module('mean.utils')
  .controller('UtilsController', UtilsController);

UtilsController.$inject = ['$scope', '$window', '$http'];
