'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module;

var Utils = new Module('utils');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
Utils.register(function(app, auth, database) {

  //We enable routing. By default the Package Object is passed to the routes
  Utils.routes(app, auth, database);

  Utils.aggregateAsset('css', 'utils.css');
  Utils.aggregateAsset('js', 'amcharts/amcharts.js', {weight: 1});
  Utils.aggregateAsset('js', 'amcharts/serial.js', {weight: 2});
  Utils.aggregateAsset('js', 'amcharts/pie.js', {weight: 5});
  Utils.aggregateAsset('js', 'amcharts/plugins/export/export.js', {weight: 3});
  Utils.aggregateAsset('css', '../js/amcharts/plugins/export/export.css', {weight: 4});


  //We are adding a link to the main menu for all authenticated users
  /*
  Utils.menus.add({
    title: 'utils example page',
    link: 'utils example page',
    roles: ['authenticated'],
    menu: 'main'
  });
  */
  
  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    Utils.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    Utils.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    Utils.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  Utils.angularDependencies(['agGrid']);
  return Utils;
});
