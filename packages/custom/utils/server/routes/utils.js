(function() {
    'use strict';
    // The Package is past automatically as first parameter
    var MongoQuery = require('../controllers/mongoQuery');
    var Mes = require('../controllers/mesConnection');
    var Mbi = require('../controllers/mbiConnection');
    var DateTool = require('../controllers/datetool');
    var BatchPromise = require('../controllers/batchPromise');

    module.exports = function(Utils, app, auth, database) {
        Utils.ctrls = {
            mongoQuery: new MongoQuery(),
            mes: new Mes(),
            mbi: new Mbi(),
            dateTool: new DateTool(),
            batch: new BatchPromise()
        };
    };
})();
