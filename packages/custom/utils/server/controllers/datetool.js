// Auth: jhong-yi.chen@alpha-info.com.tw
var _ = require('lodash');
var moment = require('moment');

function getPreDay(d, num) {
  return moment(new Date(d)).subtract(num, 'd').startOf('day')._d;
}

// 取得該 Date 所屬週的星期一
function getPreNWeekMonday(d, num) {
  return moment(new Date(d)).subtract(num, 'w').startOf('isoWeek')._d;
}

// 取得該 Date 所屬月的一號
function getFirstDayOfPreNMonth(d, num) {
  return moment(new Date(d)).subtract(num, 'M').startOf('month')._d;;
}

class DateTool {
  constructor () {}

  getPeriodSetting(from, to, period, groupBy, dayPrd, weekPrd, mthPrd){
    var range;
    var prd;
    switch (groupBy) {
      case "DAY":
        from = getPreDay(to, period);
        range = _.map(_.range(1, period), r => getPreDay(to, r));
        prd = dayPrd;
        break;
      case "WEEK":
        from = getPreNWeekMonday(to, (period - 1));
        range = _.map(_.range(period), r => getPreNWeekMonday(to, r));
        prd = weekPrd;
        break;
      case "MONTH":
        from = getFirstDayOfPreNMonth(to, (period - 1));
        range = _.map(_.range(period), r => {
          var d = getFirstDayOfPreNMonth(to, r);
          return moment(d).format('YYYY-MM');
        });
        prd = mthPrd;
        break;
      default:
        throw Error("No such Date Period... " + groupBy);
    }
    return [from, new Date(to), range, prd];
  }
}

module.exports = DateTool;