'use strict'

var _ = require('lodash');
var mongoose = require('mongoose');
var Q = require('q');

class MongoQuery {
  constructor () {}

  transformRegexStr (str, isStrict=true){
    str = str.replace(/\+/g, '.');
    if(/.+\*$/g.test(str)){ // 'ABC*'
      return new RegExp('^'+str.slice(0, -1), 'i');
    } else if (/^\*.+/g.test(str)) { //'*abc'
      return new RegExp(str.slice(1)+"$", 'i');
    } else if (/.+\*.+/g.test(str)) { //'abc*dd'
      return new RegExp('^'+str.replace('*', '.*')+'$', 'i');
    } else {
      return isStrict ? str : new RegExp('^' + str , 'i');
    }
  };

  /*
  *  Create Field Matching with subpath
  *  ex:
  *    in:  { SITENO: 'nVIDIA', 'BID': '00C0' } 
  *    out: { SITENO: /^nVIDIA/i, 'ZONES.BOARDS.BID': /^00C0/i }
  */
  genFieldMatch (query, pathMapping, isStrict){
    var subQuerys = _(query)
      .pickBy(_.isString)
      .map((v, k) => _.has(pathMapping, k) ? [pathMapping[k], this.transformRegexStr(v, isStrict)] : undefined)
      .reject(_.isUndefined)  // [1, undefined, 2]  =>  [1, 2]
      .unzip()  // [['a', 1], ['b', 2]]  =>  [['a', 'b'], [1, 2]]
      .value();
    return _.zipObject(subQuerys[0], subQuerys[1]);  // [['a', 'b'], [1, 2]]  =>  {'a': 1, 'b': 2}
  };

  /*
  * [Large Size Mongo Query]
  */
  massiveQuery (coll, pipe) {
    var df = Q.defer();
    var results = [];
    var cr = coll.aggregate(pipe).cursor({batchSize: 1000}).allowDiskUse(true).exec();
    cr.on('data', function(r){
      try {
        results.push(r);
      } catch (err) {
        df.reject(err.stack);
      }
    });

    cr.on('end', function(){
      try {
        df.resolve(results);
      } catch (err) {
        df.reject(err.stack);
      }
    })

    cr.on('error', function(e){
      df.reject(e);
    });

    return df.promise;
  };


  insertQuery (coll, data) {
    var df = Q.defer();
    var results = [];
    coll.collection.insert((err, data) => {
            if (err) {
                df.reject(err.stack);
            }
            df.resolve(data);
        });
    // cr.on('data', function(r){
    //   try {
    //     results.push(r);
    //   } catch (err) {
    //     df.reject(err.stack);
    //   }
    // });

    // cr.on('end', function(){
    //   try {
    //     df.resolve(results);
    //   } catch (err) {
    //     df.reject(err.stack);
    //   }
    // })

    // cr.on('error', function(e){
    //   df.reject(e);
    // });

    return df.promise;
  };

  /*
  *  [Search Field Filter]
  *  Use for real time filter searching, work with:
  *    - utils/public/directives/searchFilter.js
  *    - utils/public/services/searchOption.js
  */
  getSearchField (coll, params, pathMapping, arrayFields, limit=100) {
    var df = Q.defer();
    try {
      var match = _.assign({}, this.genFieldMatch(params.match, pathMapping)); // Other fields
      var regField = pathMapping[params.field]; // Current search field
      match[regField] = this.transformRegexStr(params.regex, false);

      var pipe = [];
      // Unwind if path has list
      var path = regField.split('.');
      _.forEach(path, function(s, i){
        if (_.includes(arrayFields, s)) {
          pipe.push({'$unwind': '$' + _.slice(path, 0, i+1).join('.')});
        }
      });
      if(params.exmatch){
        if(params.exmatch['$and']){
          params.exmatch['$and'].push(match);
          match = params.exmatch;
        } else {
          _.forEach(params.exmatch, function(value, key){
            if (_.isObject(value)) {
              _.forEach(value, function(subValue, subKey){
                var dateFormat = new Date(subValue)
                if (_.isDate(dateFormat)) {
                  value[subKey] = dateFormat;
                };
              });
            };
          });
          _.assign(match,params.exmatch);
        }
      }

      pipe.push({'$match': match});
      pipe.push({'$group': {'_id': '$' + regField}});
      pipe.push({'$limit': limit});
      pipe.push({'$project': {'_id': 1}});
      
      this.massiveQuery(coll, pipe)
      .then(function(result){

        try {
          var rslt = _.map(result, '_id');
          df.resolve(rslt);
        } catch (e) {
          console.log(e.stack);
          df.reject(e.stack);
        }
      });
    } catch (err) {
      console.log(err.stack);
      df.reject(err.stack);
    }
    return df.promise;
  };
}

module.exports = MongoQuery;
