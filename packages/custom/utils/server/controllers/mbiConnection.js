// Auth: jhong-yi.chen@alpha-info.com.tw
var mssql = require('mssql');
var _ = require('lodash');
var Q = require('q');

function getSqlType(obj){
  switch(obj.constructor){
    case String :
      return mssql.NVarChar;
      break;
    case Number :
      return mssql.Int;
      break;
    case Boolean :
      return mssql.Bit;
      break;
    case Date :
      return mssql.DateTime;
      break;
    case Buffer:
      return mssql.VarBinary;
      break;
    case mssql.Table:
      return mssql.TVP;
      break;
    default:
      throw Exception("No such sql types...");
  }
}

class Mbi {
  constructor (user, pwd, url) {
    // [參考] https://stackoverflow.com/questions/30356148/how-can-i-use-a-single-mssql-connection-pool-across-several-routes-in-an-express/30356582#30356582
    this.config = {
      user: 'Alpha-info-r',
      password: '6rKIEvjdo',
      server: process.env.MS_ADDR || 'c2sql10.kyec.com.tw',
      database: 'BIB',
      connectionTimeout: 30*60000,
      requestTimeout: 30*60000,
      pool: {
        max: 30,
        min: 0,
        idleTimeoutMillis: 30*60000
      }
    };

    this.cp = new mssql.ConnectionPool(this.config);
    this.cp.connect();
  }

  exec(sqlStr, bindvars) {
    var req = new mssql.Request(this.cp);
    if (typeof bindvars === 'object') {
      _.forEach(bindvars, (v, k) => req.input(k, v));
    }
    return req.query(sqlStr).then(result => result.recordset);
  }
}

module.exports = Mbi;
