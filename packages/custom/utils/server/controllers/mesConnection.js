// Auth: jhong-yi.chen@alpha-info.com.tw
var oracledb = require('oracledb');
var _ = require('lodash');
var Q = require('q');

var schema_list = ["HISMFT", "HISLFT"];
class Mes {
  constructor (user, pwd, url) {
    this.attrs = {
      user: 'EDAAP',
      password: 'wpfbaxd3d',
      connectString: 'c2db005-raclb.kyec.com.tw:1801/CHDMPD3',
      poolMax: 64,
      poolTimeout: 30*60,
      queueTimeout: 30*60000
    };
  }

  exec(sqlStr, bindvars) {
    bindvars = typeof bindvars === 'undefined' ? {} : bindvars;
    return oracledb.getConnection(this.attrs)
    .then(conn => conn.execute(sqlStr, bindvars, {resultSet: true})  // resultSet 用來取得未知數量的 row, 不然預設僅 100 
        .then(function(result) {
            var cols = _.map(result.metaData, 'name');
            var rows = [];
            return fetchRowFromRS(conn, result.resultSet, rows)
            .then(rr => _.map(rr, r => _.zipObject(cols, r)));
        })
    );
  }

  checkSchema(req, res, next) {
    var schema = req.body.schema;
    if (!_.includes(schema_list, schema)) {
      var str = "[Wrong Schema!] " + schema + ", must be " + schema_list;
      console.log(str);
      res.status(500).send({'error': str});
    } else {
      return next();
    }
  }
}

function fetchRowFromRS(connection, resultSet, rows)
{
  return resultSet.getRow()
    .then(function(row){
      if (!row) {
        doCloseResultSet(resultSet, connection)
        return rows;
      } else {
        rows.push(row);
        return fetchRowFromRS(connection, resultSet, rows);  // get next row
      }
    })
    .catch(function(err){
      doCloseResultSet(resultSet, connection);
      return err;
    });
}

function doCloseResultSet(resultSet, connection){
  resultSet.close(function(err) {
    if (err) {
      console.log("ResultSet close failed:", err);
    }
    connection.release(function(err){
      if (err) {
        console.log("Connection release failed:", err);
      }
    });
  });
}

module.exports = Mes;
