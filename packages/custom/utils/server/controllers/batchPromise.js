var Q = require('q');

class BatchPromises {
  constructor () {
  	this.attrs = {
      batchSize: 10,
	  retry: true
    };
    this.resultArr=[];
  }

  doBatch(items) {
	var results = [];
	var index = (this.attrs.batchSize - 1);

	function handler(item) {
		return Q.delay(800).then(function() {
			// if (Math.random() > 0.5) {
			// 	throw new Error('A random error occured');
			// }
			return item;
		});
	}

	function getNextItem() {
		index++;
		if (items.length > index) {                
			var nextItem = items[index];
			return getCurrentItem(nextItem);
		} 
	}

	function getCurrentItem(item) {
		// console.log(item, 'Starting');
		return handler(item)
			.then(function(result) {
				// console.log(item, 'Success');
				results.push(result);
				return getNextItem();
			})
			.catch(function(err) {
				// console.log(item);
				// console.log(err);
				// return this.attrs.retry ? getCurrentItem(item) : getNextItem();
				return getNextItem(item);
			});
	};  

	var promises = items.slice(0, this.attrs.batchSize).map(function(item) {		
		return getCurrentItem(item);
	});
	return Q.all(promises).then(function() {
		return results;
	});
  } 
}


module.exports = BatchPromises;