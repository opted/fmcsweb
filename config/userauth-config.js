exports.requestLog = {
    notRecordUrls:[/^localhost/,/^192.168/],
    ingnoreApis:['/api/datalog/getmyuploadfile','/api/logincheckout','/api/loaduserprofile'],
    ingonreSubString:['/getsearchfield']
  };
  
exports.role = {
    highadmin:{admin_wr:3,user_wr:3,chisname:'最高管理者',level:0},
    admin:{admin_wr:1,user_wr:3,chisname:'管理者',level:1},
    regularuser:{admin_wr:0,user_wr:0,chisname:'一般用戶',level:10}
};
  
exports.authIgnoreApi = {
    get:['/api/get-public-config'],
    getStartsWith:[],
    post:['/api/login']
};
