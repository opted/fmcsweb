# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw

import os, datetime, xlsxwriter #, cx_Oracle
from DBOperator import decodeOracleData as decode
import Property

class ExcelGenerator:
    def __init__(self, book_name):
    	conf = Property.getProps()
    	path = conf.get('XLSX')["path"]
    	if not os.path.exists(path):
    		os.mkdir(path)
        self.workbook = xlsxwriter.Workbook(path + book_name) # Create xlsx file

    # trun index into "A1", "AB2" for cell location
    def cellStr(self, row, col):
        row = str(row)
        div = col
        col_str = ""
        temp = 0
        while div > 0:
            module = (div - 1) % 26
            col_str = chr(65 + module) + col_str
            div = int((div - module) / 26)
        return col_str + row

    def cellStrNoRows(self, col):
        div = col
        col_str = ""
        temp = 0
        while div > 0:
            module = (div - 1) % 26
            col_str = chr(65 + module) + col_str
            div = int((div - module) / 26)
        return col_str

    def dataToSheet(self, sheet_name, data , has_chart,show_temp):
        row_num = 1
        data_counts = len(data)
        worksheet = self.workbook.add_worksheet(sheet_name)
        for i, row in enumerate(data):
            for j, cell in enumerate(row):
                if (j+1) <= len(data[0]) and (j+1) <= 56:
                    worksheet.write(self.cellStr(row_num, j+1), cell)
                if(i == 0):
                    if j < 1:
                        col_width=25
                    else:
                        col_width=10
                    worksheet.set_column(j, j, col_width)
            row_num += 1

        if has_chart:
            count = data_counts-1 # reduce 1 row cause of header
            header_length= len(data[0])
            if header_length > 56:
                header_length=56
            # Date and Ovens trendline
            chart= self.workbook.add_chart({'type':'line'})
            # vars or boards trendline
            chart_2 = self.workbook.add_chart({'type':'line'})
            chart_location = self.cellStr(2, header_length+2)

            data_start_idx=0
            if show_temp:
                data_start_idx=2
            for idx, header in enumerate(data[0]):
                if show_temp and (idx == 1 or idx == 2):
                    chart_2.add_series({'name':[sheet_name,0,int(idx)],
                                  'categories':[sheet_name,1,0,count,0],
                                  'line':   {'dash_type': 'dash'},
                                  'values':[sheet_name, 1,int(idx),count,int(idx)],
                                  'y2_axis': True})
                # skip Datetime and left,right Oven
                if idx>data_start_idx and idx<=56:
                    chart.add_series({'name':[sheet_name,0,int(idx)],
                                  'categories':[sheet_name,1,0,count,0],
                                  'values':[sheet_name, 1,int(idx),count,int(idx)]})
            if show_temp:
                chart.combine(chart_2)
                # chart.set_y_axis({ 'name': 'Data Value'})
                chart.set_y2_axis({ 'name': 'Temp(C)'})
                
            chart.set_y_axis({ 'name': 'Data Value'})
            chart.set_x_axis({ 'name': 'Date Time','label_position':'low'})
            chart.set_title ({'name': sheet_name})
            worksheet.insert_chart(chart_location,chart,{'x_offset': 25, 'y_offset': 25})


    def dataDrawListChart(self, all_sheet_list, judge_type, has_chart):
        bold = self.workbook.add_format({'bold': 1})
        for sheet_obj in all_sheet_list:
            sheet_name = sheet_obj["sheet_name"]
            data_counts = len(sheet_obj["result"][0][sheet_obj["result"][0].keys()[0]])
            worksheet = self.workbook.add_worksheet(sheet_name)
            worksheet.write_row('A1', sheet_obj["header"], bold)
            if judge_type == "board":
                worksheet.write_column(1, 0, [sheet_obj["zone_board_id"]])
            elif judge_type == "dut":
                worksheet.write_column(1, 0, [sheet_obj["dut_id"]])
            worksheet.write_column(1, 1, sheet_obj["datetime"])
            for idx, sheet_obj_result in enumerate(sheet_obj["result"]):
                worksheet.write_column(1, idx + 2, sheet_obj_result[sheet_obj["header"][idx + 2]])
            if sheet_name != 'ERRCODE':
                chart = self.workbook.add_chart({'type': 'line'})
                chart_location = self.cellStr(2, len(sheet_obj["header"]) + 2) 
                for idx, header in enumerate(sheet_obj["header"]):
                    if idx == 0 or idx == 1:
                        continue
                    chart.add_series({'name': [sheet_name, 0, int(idx)],
                                      'categories': [sheet_name, 1, 1, data_counts, 1],
                                      'values': [sheet_name, 1, int(idx), data_counts, int(idx)]})
                chart.set_style(10)
                chart.set_title({'name': sheet_name})
                worksheet.insert_chart(chart_location, chart, {'x_offset': 25, 'y_offset': 25})


    def close(self):
        self.workbook.close()