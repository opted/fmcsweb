# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw

import datetime, os, json, re, os.path #,cx_Oracle
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError, OperationFailure
import Property

class DBOperator:
	def __init__(self):
		conf = Property.getProps()

		# Set the language and territory used by the client application and the database. 
		# Also sets the character set for entering and displaying data by a client program, such as SQL*Plus
		os.environ['NLS_LANG'] = conf.get('NLS_LANG')

		# Set Global vars
		self.ORACLE = conf.get('ORACLE')
		self.MONGO  = conf.get('MONGO')
		self.TABLES = conf.get('TABLES')
		
		# self.orcl_db = cx_Oracle.connect(self.ORACLE['user'], self.ORACLE['pwd'],
		# 	self.ORACLE['host']+":"+str(self.ORACLE['port'])+"/"+self.ORACLE['sid'])
		
		self.mongo_db = MongoClient('mongodb://'+self.MONGO['host']+':'+ str(self.MONGO['port']))[self.MONGO['db']]

	def getOracleDB(self):
		if self.orcl_db == None:
			self.__init__()
		return self.orcl_db

	def getMongoDB(self):
		if self.mongo_db == None:
			self.__init__()
		return self.mongo_db

	def close(self):
		try:
			if self.orcl_db != None:
				self.orcl_db.close()
			if self.mongo_db != None:
				self.mongo_db.client.close()
		except:
			raise Exception('Failed to close DB ...')
		
def decodeOracleData(val):
	return val.decode('utf-8') if type(val) == str else val