# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw

import os, datetime, logging
import Property

def createLogger(file):
	log = logging.getLogger(file)
	conf = Property.getProps()

	LOGGER = conf.get('LOGGER')

	# Initial logger
	init(log, file,
		level=logging.getLevelName(LOGGER['level']), 
		name=LOGGER["name"], 
		format_str=LOGGER['format'],
		path=LOGGER['path'])
	return log

def init(log, file,
		level=logging.DEBUG, 
		name="logs", 
		format_str='%(asctime)s   [%(levelname)s] %(name)s    %(message)s',
		path="log/"):
	log.setLevel(level)
	
	# Produce formatter
	formatter = logging.Formatter(format_str)
	
	# Setup StreamHandler 
	streamLogger = logging.StreamHandler()
	streamLogger.setLevel(logging.DEBUG)
	streamLogger.setFormatter(formatter)
	log.addHandler(streamLogger)

	# Setup FileHandler
	basepath = os.path.dirname(file)
	trg_path = os.path.abspath(os.path.join(basepath, path))

	if not os.path.exists(trg_path):
		os.makedirs(trg_path)
	logfile = os.path.abspath(os.path.join(trg_path, datetime.datetime.now().strftime(name)))
	fileLogger = logging.FileHandler(logfile)
	fileLogger.setLevel(logging.DEBUG)
	fileLogger.setFormatter(formatter)
	log.addHandler(fileLogger)

