# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw

import sys,os, datetime, logging
from ConfigParser import SafeConfigParser


def create_logger(file, module_name):
    log = logging.getLogger(file)
    conf = SafeConfigParser()
    #sys.path.append('python/conf')
    conf.read('python/conf/properties')

    # Initial logger

    init(log, file,
         level=logging.getLevelName(conf.get('LOGGER', 'level')),
         name=module_name + '_' + conf.get('LOGGER', 'name'),
         format_str=conf.get('LOGGER', 'format'),
         path=conf.get('LOGGER', 'path'))
    return log


def init(log, file,
         level=logging.DEBUG,
         name="logs",
         format_str='%(asctime)s   [%(levelname)s] %(name)s    %(message)s',
         path="log/"):
    log.setLevel(level)

    # Produce formatter
    formatter = logging.Formatter(format_str)

    # Setup StreamHandler
    stream_logger = logging.StreamHandler()
    stream_logger.setLevel(logging.DEBUG)
    stream_logger.setFormatter(formatter)
    log.addHandler(stream_logger)

    try:
        log_dir = os.environ['PYTHONPATH']
    except:
        log_dir = file

    # Setup FileHandler
    basepath = os.path.dirname(log_dir)
    trg_path = os.path.abspath(os.path.join(basepath, path))

    if not os.path.exists(trg_path):
        os.makedirs(trg_path)
    logfile = os.path.abspath(os.path.join(trg_path, datetime.datetime.now().strftime(name)))
    file_logger = logging.FileHandler(logfile)
    file_logger.setLevel(logging.DEBUG)
    file_logger.setFormatter(formatter)
    log.addHandler(file_logger)
