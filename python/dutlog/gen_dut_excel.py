# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw
import sys
sys.path.append('python/lib')
import Logger
#from DBOperator import DBOperator
#from DBOperator import decodeOracleData as decode
from ExcelGenerator import ExcelGenerator
#import Property
#import pymssql
#import pymssql
import pymongo
import time
from pprint import pprint
from pymongo import MongoClient
from datetime import datetime, timedelta
from dateutil import tz
from dateutil.relativedelta import relativedelta
import dateutil.parser
import json

reload(sys)
sys.setdefaultencoding('utf8')


def init():
    print 'Init'
    connect_db()
    # from ConfigParser import ConfigParser
    # conf = ConfigParser()
    # conf.read('../conf/properties')
    global log
    log = Logger.create_logger(__file__, 'DUT_logs')


def connect_db():
    client = MongoClient('10.5.135.34', 27017)
    global db
    db = client['BIDATA']
    global col
    col = db['DUT_logs']


def createReport(param):
    oid = 'OI-16'
    var_name = 'DPS4CUR'
    var_list = ['DPS3CUR','DPS4CUR']#var_list

    oven_board_list = []
    result = []
    header = []
    # print(col.find_one())
    cr = col.aggregate(gen_pipeline(param), allowDiskUse=True)

    all_sheet_list = get_exel_data(cr, param)
    judge_type = param["slotChooseListSelected"]
    exGen = ExcelGenerator("dut.xlsx")
    exGen.dataDrawListChart(all_sheet_list,judge_type , True)
    exGen.close()

def get_exel_data(cursor, param):
    all_sheet_list = []
    result = []
    header = []
    date_time_list = []
    judge_type = ""
    if param["slotChooseListSelected"] == "board":
        header.append("zone_board_id")
        header.append("datetime")
        cursor_list = list(cursor)
        # log.info(len(cursor_list))
        judge_dutlist = cursor_list[0]["DUTLIST"]
        judge_dut = judge_dutlist[0]
        now_zone_id = param["slotOptionListSelected"][param["slotOptionListSelected"].find("ZONE")+4:param["slotOptionListSelected"].find("ZONE")+6]
        now_board_id = param["slotOptionListSelected"][param["slotOptionListSelected"].find("BOARD")+5:param["slotOptionListSelected"].find("BOARD")+7]
        
        # print("now_zone_id", now_zone_id)
        # print("now_board_id", now_board_id)
        for sheet_name in judge_dut.keys():
            if "DUT" != sheet_name:
                sheetObj = {}
                sheetObj["sheet_name"] = sheet_name
                all_sheet_list.append(sheetObj)
        for one_judge_dut in judge_dutlist:
            header.append("DUT" + one_judge_dut["DUT"])
            # dut_obj = dict()
            # dut_list = list()
            # dut_obj["DUT" + one_judge_dut["DUT"]] = list(dut_list)
            # result.append(dut_obj.copy())
            # result.append({"DUT" + one_judge_dut["DUT"]: []})
        for in_sheet_obj in all_sheet_list:
            result_list = []
            for one_judge_dut in judge_dutlist:
                dut_obj = {}
                dut_list = []
                dut_obj["DUT" + one_judge_dut["DUT"]] = dut_list
                result_list.append(dut_obj)
            in_sheet_obj["result"] = result_list

            in_sheet_obj["zone_board_id"] = param["slotOptionListSelected"]
            in_sheet_obj["datetime"] = list(date_time_list)
            in_sheet_obj["header"] = header
        for data in cursor_list:
            if data["ZONE"] != now_zone_id or data["BOARD"] != now_board_id:
                continue
            dutlist = data["DUTLIST"]
            # log.info(data)
            # break
            for in_sheet_obj in all_sheet_list:
                in_sheet_obj["datetime"].append(data["DATETIME"])
            is_dut_repeat = [False, ""]
            for dut in dutlist:
                if is_dut_repeat[0]:
                    if is_dut_repeat[1] == dut["DUT"]:
                        break
                else:
                    is_dut_repeat[0] = True
                    is_dut_repeat[1] = dut["DUT"]
                     
                now_dut_id = "DUT" + dut["DUT"]
                for in_sheet_obj in all_sheet_list:
                    now_sheet_name = in_sheet_obj["sheet_name"]
                    for in_sheet_obj_result in in_sheet_obj["result"]:
                        if now_dut_id in in_sheet_obj_result.keys():
                            in_sheet_obj_result[now_dut_id].append(dut[now_sheet_name])
                            break
        # log.info(all_sheet_list)
        # log.info(len(all_sheet_list[0]["result"][0]["DUT00"]))
    elif param["slotChooseListSelected"] == "dut":
        header.append("DUT ID")
        header.append("datetime")
        cursor_list = list(cursor)
        now_dut_id = param["slotOptionListSelected"][param["slotOptionListSelected"].find("DUT")+3:param["slotOptionListSelected"].find("DUT")+5]
        judge_dutlist = cursor_list[0]["DUTLIST"]
        judge_dut = judge_dutlist[0]
        for sheet_name in judge_dut.keys():
            if "DUT" != sheet_name:
                sheetObj = {}
                sheetObj["sheet_name"] = sheet_name
                all_sheet_list.append(sheetObj)
        judge_zoneboard = ""
        for data in cursor_list:
            if judge_zoneboard == "":
                header.append("ZONE" + data["ZONE"] + "BOARD" + data["BOARD"])
                judge_zoneboard = data["DATETIME"]
            else:
                if judge_zoneboard == data["DATETIME"]:
                    header.append("ZONE" + data["ZONE"] + "BOARD" + data["BOARD"])
                else:
                    break
        for in_sheet_obj in all_sheet_list:
            result_list = []
            for idx, one_header in enumerate(header):
                if idx == 0 or idx == 1:
                    continue
                result_obj = {}
                result_obj[one_header] = []
                result_list.append(result_obj)
            in_sheet_obj["result"] = result_list
            in_sheet_obj["dut_id"] = param["slotOptionListSelected"]
            in_sheet_obj["datetime"] = date_time_list
            in_sheet_obj["header"] = header
        judge_datetime = ""
        for data in cursor_list:
            dutlist = data["DUTLIST"]
            if judge_datetime == "":
                date_time_list.append(data["DATETIME"])
                judge_datetime = data["DATETIME"]
            else:
                if judge_datetime != data["DATETIME"]:
                    date_time_list.append(data["DATETIME"])
                    judge_datetime = data["DATETIME"]
            for in_sheet_obj in all_sheet_list:
                now_sheet_name = in_sheet_obj["sheet_name"]
                is_dut_repeat = [False, ""]
                for dut_obj in dutlist:
                    if is_dut_repeat[0]:
                        if is_dut_repeat[1] == dut_obj["DUT"]:
                            break
                    else:
                        is_dut_repeat[0] = True
                        is_dut_repeat[1] = dut_obj["DUT"]
                    # print("now_dut_id ",now_dut_id)
                    if now_dut_id == dut_obj["DUT"]:
                        for in_sheet_obj_result in in_sheet_obj["result"]:
                            compare_zone_board_id = "ZONE" + data["ZONE"] + "BOARD" + data["BOARD"]
                            # print("!!!!!!!   ", compare_zone_board_id)
                            if compare_zone_board_id in in_sheet_obj_result.keys():
                                # print("!!!!!!!   ", compare_zone_board_id)
                                in_sheet_obj_result[compare_zone_board_id].append(dut_obj[now_sheet_name])
                                break
                        break
        # print(len(all_sheet_list[0]["result"]))
        # print("header  ", header)
    return all_sheet_list


def get_vars_result(collection, pipeline):
    return 0


def gen_pipeline(param):
    # today_now = time.strftime("%Y/%m/%d_%H:%M:%S")  #%Y/%m/%d_%H:%M:%S
    # dateutil.parser.parse('2008-09-03T20:56:35.450686Z')
    # date_cur = datetime.strptime(query["from"], "%Y-%m-%d'T'%H:%M:%S'Z")
    date_cur = dateutil.parser.parse(param["from"])
    # date_cur = datetime.strptime(today_now, "%Y/%m/%d_%H:%M:%S")
    # print("date_curddddddddd  ", date_cur)
    # date_pre = datetime.strptime(query["to"], "%Y-%m-%d'T'%H:%M:%S'Z")
    date_pre = dateutil.parser.parse(param["to"])

    # date_pre = date_cur - relativedelta(days=2)
    # _.assign(match, this.utils.ctrls.mongoQuery.genFieldMatch(_.pick(query, [ 'CUSTOMERLOTNO', 'LOTNO']), this.pathMapping));
    # match = {'CREATETIME': {'$gte': date_pre, '$lt': date_cur}}
    match = {'CREATETIME': {'$gte': date_cur, '$lt': date_pre}}
    if "CUSTOMERLOTNO" in param:
        match["CUSTOMERLOTNO"] = param["CUSTOMERLOTNO"]
    if "LOTNO" in param:
        match["LOTNO"] = param["LOTNO"]

    pipe = [{'$match': match}]
    pipe.append({'$unwind': "$SLOTINFO"})
    pipe.append({'$unwind': "$SLOTINFO.DUT_LIST"})

    pushObj = {}
    pushObj["DUT"] = "$SLOTINFO.DUT_LIST.DUT"
    for varObj in param["VARS"]:
        searchName = varObj["name"]
        pushObj[searchName] = "$SLOTINFO.DUT_LIST." + searchName

    pipe.append({'$group':
                    {
                        '_id':
                            {
                                'DATETIME': {'$dateToString': {'format': "%Y-%m-%d %H:%M:%S", 'date': "$SLOTINFO.DATETIME" }},
                                'OVEN': "$OVENID",
                                'ZONE': "$SLOTINFO.ZONE",
                                'BOARD': "$SLOTINFO.BOARD"
                            },
                        'DUTLIST':
                            {
                                '$push': pushObj
                            },
                        'FILEPATH':
                            {
                                '$first': "$FILEPATH"
                            },
                        'OVENID':
                            {
                                '$first': "$OVENID"
                            }
                    }
                })
    pipe.append({'$project': {
        '_id': 0,
        'FILEPATH': "$FILEPATH",
        'OVENID': "$OVENID",
        'LOTNO': 1,
        'DATETIME': "$_id.DATETIME",
        'ZONE': '$_id.ZONE',
        'BOARD': '$_id.BOARD',
        'DUT': '$_id.DUT_LIST.DUTS',
        'DUTLIST': 1
    }})
    pipe.append({'$sort': {"DATETIME": 1}})
    # print(pipe)
    return pipe


def get_sql_result(cr, query):
    """Exec SQL query & return dict result"""
    today = time.strftime("%Y/%m/%d")
    date_cur = datetime.strptime(today, "%Y/%m/%d")
    date_pre = date_cur - relativedelta(days=1)
    date_nxt = date_cur + relativedelta(days=1)

    cr.execute(query, (date_cur, date_nxt))
    cols = [c[0] for c in cr.description]
    return [dict(zip(cols, list(r))) for r in cr]


def reportManager(param):
    
    try:
        log.info("START TO CREATE REPORT")
        createReport(param)
        log.info('DONE !')
    except Exception as e:
        log.exception("Got exception on main handler")
        raise


if __name__ == "__main__":
# Input Parameters
    init()
    # var_list = sys.argv[1]
    # log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!555 ")
    # log.info(sys.argv[1])
    # param = sys.argv[1]
    # print(param)
    # reportManager(var_list,param)
    # gen_pipeline()
    # param = {"from": '2017-08-21T16:00:00.000Z',
    #           "to": '2017-08-23T16:00:00.000Z',
    #           "VARS": 
    #            [ { "name": 'TJ' },
    #              { "name": 'CAL_TJ' },
    #              { "name": 'TC' },
    #              { "name": 'DUTV' },
    #              { "name": 'DUTA' },
    #              { "name": 'ERRCODE' } ],
    #           "CUSTOMERLOTNO": 'APU3206400',
    #           "slotChooseListSelected": 'board',
    #           "slotOptionListSelected": 'ZONE00BOARD00' }
    # param = {"from": '2017-08-21T16:00:00.000Z',
    #           "to": '2017-08-22T16:00:00.000Z',
    #           "VARS": 
    #            [ { "name": 'TJ' },
    #              { "name": 'CAL_TJ' },
    #              { "name": 'TC' },
    #              { "name": 'DUTV' },
    #              { "name": 'DUTA' },
    #              { "name": 'ERRCODE' } ],
    #           "CUSTOMERLOTNO": 'APU3206400',
    #           "slotChooseListSelected": 'dut',
    #           "slotOptionListSelected": 'DUT00' }
    param = json.loads(sys.argv[1]) 
    reportManager(param)
