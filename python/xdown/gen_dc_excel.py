# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw


import sys
sys.path.append('../lib/')
import Logger
#from DBOperator import DBOperator
#from DBOperator import decodeOracleData as decode
from ExcelGenerator import ExcelGenerator
#import Property
#import pymssql
#import pymssql
import pymongo
import time
from pprint import pprint
from pymongo import MongoClient
from datetime import datetime, timedelta
from dateutil import tz
from dateutil.relativedelta import relativedelta

reload(sys)  
sys.setdefaultencoding('utf8')

def init():
    print 'Init'
    connect_db()
    from ConfigParser import ConfigParser
    conf = ConfigParser()
    conf.read('../conf/properties')
    global log
    log = Logger.create_logger(__file__,'DC_logs')

def connect_db():
    client=MongoClient('192.168.0.170',27017)
    global db
    db=client['BIDATA_All_ver1']
    global col
    col=db['DC_logs_new']

def createReport(var_list,param):
    oid = 'OI-16'
    var_name = 'DPS4CUR'
    var_list =var_list

    oven_board_list=[]
    result=[]
    header=[]
    
    pipe = gen_pipeline(oid)

    exGen = ExcelGenerator("dc.xlsx")
    cr_list=[]
    for v in var_list:
        cr=col.aggregate(gen_pipeline(v,param))
        pro_data = process_cursor(cr)
        exGen.dataToSheet(v, pro_data, True)

    # cr = col.aggregate(pipe)
    # for doc in cr:
    #     data=[]
    #     for key in sorted(doc.keys()):
    #         if key!='OVENDATA' and key != '_id' and key!='OVENID':
    #             if is_first_row:
    #                 header.append(key)
    #             data.append(doc[key])

    #     for zb in doc['OVENDATA']:
    #         bno='Zone'+ str(zb['ZONE'])+'Board'+ str(zb['BOARD'])
    #         if is_first_row:
    #             oven_board_list.append(bno)
    #         data.append(zb['VARS'])
    #     if is_first_row:
    #         header_row = header+oven_board_list
    #         result.append(header_row)
    #         is_first_row=False

    #     result.append(data)

    # cr = db.command('aggregate','DC_logs_new', pipeline=pipe, allowDiskUse=True)

    
    # Daily Shut Down Data Sheet
    # cr = orcldb.cursor()
    # cr.execute(query_shut_down, {'date_pre': from_date, 'datenxt': to_date})
    # a=[]
    # r={}
    # for key in cr:
    #     if key == 'result':
    #         for row in cr[key]:
    #             a.append(row)

    # r = a[0]
    # pprint(cols)
        #shut_down_data.append([decode(val) for val in row])  # Append Row Data
        #dt = relativedelta(row[4],row[3]) #row[9],row[8]
        #dif_second = dt.hours*3600 + dt.minutes*60

       #row[5] = product family = C.Family

    # exGen.dataToSheet(var_name, result, True)  # Insert to sheet
    exGen.close()

def process_cursor(cursor):
    oven_board_list=[]
    result=[]
    header=[]
    is_first_row=True
    for doc in cursor:
        data=[]
        for key in sorted(doc.keys()):
            if key!='OVENDATA' and key != '_id' and key!='OVENID':
                if is_first_row:
                    header.append(key)
                data.append(doc[key])

        for zb in doc['OVENDATA']:
            bno='Zone'+ str(zb['ZONE'])+'Board'+ str(zb['BOARD'])
            if is_first_row:
                oven_board_list.append(bno)
            data.append(zb['VARS'])
        if is_first_row:
            header_row = header+oven_board_list
            result.append(header_row)
            is_first_row=False

        result.append(data)

    return result

def get_vars_result(collection, pipeline):
    return 0

def gen_pipeline(v,param):

    today_now = time.strftime("%Y/%m/%d_%H:%M:%S")  #%Y/%m/%d_%H:%M:%S
    #date_cur = datetime.strptime(param.from,"%Y/%m/%d_%H:%M:%S")
    date_cur = datetime.strptime(today_now, "%Y/%m/%d_%H:%M:%S")
    #date_pre = datetime.strptime(param.to,"%Y/%m/%d_%H:%M:%S")
    date_pre = date_cur - relativedelta(days=1)

    #from_date = datetime.strftime(date_pre, "%Y/%m/%d_%H:%M:%S")
    #to_date = datetime.strftime(date_cur, "%Y/%m/%d_%H:%M:%S")
    #oid=param.OVENID
    oid='OI-16'
    #var_name=param.VARS
    var_name='DPS4CUR'
    
    match = {'OVENID':oid, 'DATETIME':{'$gte':date_pre, '$lt': date_cur}}
    pipe = [{'$match': match}];
    pipe.append({'$sort': {'DATETIME': -1}});
    #pipe = [{'$match': match}];
    pipe.append({'$unwind': '$ZONES'});
    pipe.append({'$unwind': '$ZONES.BOARDS'});
    #pipe.append({'$match': genSubZoneBoardQuery(_.pick(query, ['ZONE', 'BOARD']))});
    pipe.append({'$group':
    { '_id': { 'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': '$DATETIME' }}
        ,'OVEN': '$OVENID'},
        'OVENDATA':{ '$push': {'VARS': '$ZONES.BOARDS.' + var_name,
            'ZONE':'$ZONES.ZONE',
            'BOARD':'$ZONES.BOARDS.BOARD'}},
        'Right_Oven':{'$first':'$Right_Oven'},
        'Left_Oven':{'$first':'$Left_Oven'}
    }
    })
    pipe.append({'$project': {
        'DATETIME':  '$_id.DATETIME' ,
        'OVENID':  '$_id.OVEN',
        'Right_Oven': '$Right_Oven',
        'Left_Oven': '$Left_Oven',
        'OVENDATA' : 1
    }});
    pipe.append({'$sort': {"DATETIME": 1}});

    return pipe

def get_sql_result(cr, query):
    """Exec SQL query & return dict result"""
    today = time.strftime("%Y/%m/%d")
    date_cur = datetime.strptime(today, "%Y/%m/%d")
    date_pre = date_cur - relativedelta(days=1)
    date_nxt = date_cur + relativedelta(days=1)

    cr.execute(query,(date_cur,date_nxt))
    cols = [c[0] for c in cr.description]
    return [dict(zip(cols, list(r))) for r in cr]

def reportManager(var_list,param):
    init()
    try:
        log.info("START TO CREATE REPORT")
        createReport(var_list,param)
        log.info('DONE !')
    except Exception as e:
        log.exception("Got exception on main handler")
        raise
    
if __name__ == "__main__":
# Input Parameters
    var_list = sys.argv[1]
    param = sys.argv[2]
    reportManager(var_list,param)
