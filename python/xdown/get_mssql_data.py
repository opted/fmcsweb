import sys
sys.path.append('lib/')
import Property
import Logger
import pymssql
import time
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

reload(sys)  
sys.setdefaultencoding('utf8')

def init():
    conf = Property.getProps()
    # global ORACLE
    # ORACLE = conf.get('ORACLE')
    global log
    log = Logger.createLogger(__file__)

def get_sql_result(cr, query):
    """Exec SQL query & return dict result"""
    today = time.strftime("%Y/%m/%d")
    date_cur = datetime.strptime(today, "%Y/%m/%d")
    date_pre = date_cur - relativedelta(days=1)
    date_nxt = date_cur + relativedelta(days=1)

    cr.execute(query,(date_cur,date_nxt))
    cols = [c[0] for c in cr.description]
    return [dict(zip(cols, list(r))) for r in cr]

def getData():


    MSSQL_INFO = {"server": "egServer70","user": "Alpha-info-r","pwd": "6rKIEvjdo"}

    data_dict={}
 
    with pymssql.connect(MSSQL_INFO['server'], MSSQL_INFO['user'], MSSQL_INFO['pwd']) as mssql:
		cr = mssql.cursor()
        try:
            query = """
                SELECT
                    LotNum,
                    MsgText,
                    Log_Time
                FROM
                    [BIB].[dbo].[OvenErrorMsgALL]
                WHERE
                     Log_Time >= %s
                     AND Log_Time < %s
            """
            for r in get_sql_result(cr, query):
				err_msg = r['MsgText'].strip()
				msg = datetime.strftime(r['Log_Time'], "%Y/%m/%d_%H:%M:%S") +' '+ err_msg
				if r['LotNum'] not in data_dict:
					data_dict[r['LotNum']] = msg
				elif err_msg not in data_dict[r['LotNum']]:
					data_dict[r['LotNum']] = data_dict[r['LotNum']] + ', '+msg

        except pymssql.ProgrammingError:
        	print('MSSQL error')
            # Table may not be exist

	#for  data in data_list:
		#print data['LotNum'],data['MsgText']
	#distinct_lot_list= list(set(lot_list))
	#print distinct_lot_list[1]

	return data_dict


def getMSSQLData():
    init()
    try:
        log.info("START TO CONNECT MSSQL")
        data=getData()
        for key,value in data.items():
        	print key, value
        log.info('DONE !')
    except Exception as e:
        log.exception("Got exception on main handler")
        raise

if __name__ == "__main__":
    getMSSQLData()