# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw


import sys
# path start at /biweb_phase2
sys.path.append('python/lib')
import Logger
from ExcelGenerator import ExcelGenerator
import pymongo
import time
from pprint import pprint
from pymongo import MongoClient
from datetime import datetime, timedelta
from dateutil import tz
from dateutil.relativedelta import relativedelta
import json
reload(sys)  
sys.setdefaultencoding('utf8')

def init():
    print 'Init'
    connect_db()
    global log
    log = Logger.create_logger(__file__,'DC_logs')

def connect_db():
    client=MongoClient('10.5.135.34',27017) # 10.5.21.114:27017
    global db
    db=client['BIDATA'] #BIDATA_All_ver1
    global col
    col=db['DC_logs'] #DC_logs_new

#var_list,from_t,to_t,oven_id
def createReport(param):
    #search_opt={'from':param['from'] ,'to':param['to'],'oven_id':param['OVENID']}
    exGen = ExcelGenerator("dc.xlsx")
    if param['testType']=='vars':
        for v in param['VARSLIST']:
            cr=col.aggregate(gen_pipeline(param,v))
            #process each row to be an array
            pro_data = process_cursor(cr,param['testType'],param['VARSLIST'],param['showTemp'])
            # Insert to sheet
            exGen.dataToSheet(v, pro_data, True,param['showTemp'])
    else:
        for zb in param['ZBLIST']:
            cr=col.aggregate(gen_pipeline(param,zb))
            pro_data = process_cursor(cr,param['testType'],param['VARSLIST'],param['showTemp'])
            exGen.dataToSheet(zb, pro_data, True,param['showTemp'])


    exGen.close()

def process_cursor(cursor,test_type,vars_list,show_temp):
    oven_board_list=[]
    result=[]
    header=[]
    default_header = []
    if show_temp==True:
        default_header=['DATETIME','Left_Oven','Right_Oven']
    else:
        default_header=['DATETIME']

    is_first_row=True
    for doc in cursor:
        data=[]
        for key in sorted(doc.keys()):
            if test_type=='vars':
                if key in default_header: #key!='OVENDATA' and key != '_id' and key!='OVENID'
                    if is_first_row:
                        header.append(key)
                    if key =='DATETIME':
                        # set date format
                        data.append(iso_date_to_locale(doc[key]))
                    elif show_temp == True:
                        try:
                            data.append(float(doc[key]))
                        except Exception as e:
                            data.append(doc[key])
                        # data.append(doc[key])
                    # else:
                    #     data.append(doc[key])
            else:
                if key in default_header:
                    if is_first_row:
                        header.append(key)
                    if key =='DATETIME':
                        data.append(iso_date_to_locale(doc[key]))
                    elif show_temp == True:
                        try:
                            data.append(float(doc[key]))
                        except Exception as e:
                            data.append(doc[key])

        for zb in doc['OVENDATA']:
            if test_type=='vars':
                bno='Zone'+ str(zb['ZONE'])+'Board'+ str(zb['BOARD'])
                if is_first_row:
                    oven_board_list.append(bno)
                data.append(zb['VARS'])
            else:
                for v_name in vars_list:
                    if is_first_row:
                        oven_board_list.append(v_name)
                    data.append(zb['ALLVARS'][v_name])

        if is_first_row:
            header_row = header+oven_board_list
            result.append(header_row)
            is_first_row=False

        result.append(data)

    return result

def iso_date_to_locale(datetime_obj):
    iso_date = datetime.strptime(datetime_obj,"%Y-%m-%d %H:%M:%S")
    local_date = iso_date # + relativedelta(hours=8)
    local_date_str = datetime.strftime(local_date, "%Y-%m-%d %H:%M:%S")

    return local_date_str

def gen_pipeline(param,cur_var_or_tab):

    date_cur=datetime.strptime(param['from'],"%Y-%m-%dT%H:%M:%S.%fZ")
    date_pre=datetime.strptime(param['to'],"%Y-%m-%dT%H:%M:%S.%fZ")
    oid=param['OVENID']
    test_type=param['testType']
    
    match = {'OVENID':oid, 'DATETIME':{'$gte':date_cur, '$lt': date_pre}}
    pipe = [{'$match': match}];
    pipe.append({'$sort': {'DATETIME': -1}});
    pipe.append({'$unwind': '$ZONES'});
    pipe.append({'$unwind': '$ZONES.BOARDS'});
    if param['isAllZone'] ==False:
        if test_type == 'vars':
            pipe.append({'$match':{ 'ZONES.ZONE': { '$in': param['ZONE'] },
                                'ZONES.BOARDS.BOARD': { '$in': param['BOARD'] }}
                    })
        else:
            cur_zone= int(cur_var_or_tab[1])
            cur_board= int(cur_var_or_tab[3:])
            pipe.append({'$match':{ 'ZONES.ZONE': { '$in': [cur_zone]},
                                'ZONES.BOARDS.BOARD': { '$in': [cur_board] }}
                    })


    if test_type=='vars':
        var_name=cur_var_or_tab
        pipe.append({'$group':
        { '_id': { 'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }}
            ,'OVEN': "$OVENID"},
            'OVENDATA':{ '$push': {'VARS': "$ZONES.BOARDS." + var_name,
                'ZONE':"$ZONES.ZONE",
                'BOARD':"$ZONES.BOARDS.BOARD"}},
            'Right_Oven':{'$first':"$Right_Oven"},
            'Left_Oven':{'$first':"$Left_Oven"},
        }
        })
        pipe.append({'$project': {
            'DATETIME':  "$_id.DATETIME" ,
            'Right_Oven': '$Right_Oven',
            'Left_Oven': '$Left_Oven',
            'OVENDATA' : 1
        }})
    
    else:
        pipe.append({'$group':
        { '_id': { 'DATETIME':{'$dateToString': { 'format': "%Y-%m-%d %H:%M:%S", 'date': "$DATETIME" }}
            ,'OVEN': "$OVENID"},
            'OVENDATA':{ '$push': {'ALLVARS': "$ZONES.BOARDS",
                'ZONE':"$ZONES.ZONE",
                'BOARD':"$ZONES.BOARDS.BOARD"}},
            'Right_Oven':{'$first':"$Right_Oven"},
            'Left_Oven':{'$first':"$Left_Oven"},
        }
        })
        
        project_dict = {
            'DATETIME':  "$_id.DATETIME" ,
            'Right_Oven': "$Right_Oven",
            'Left_Oven': "$Left_Oven",
            'OVENDATA':1
        }
        # for v_name in param['VARSLIST']:
        #     project_dict[v_name]= "$OVENDATA.ALLVARS."+v_name
        pipe.append({'$project': project_dict}) 


    pipe.append({'$sort': {"DATETIME": 1}});

    # log.info(pipe)

    return pipe

def reportManager(param):
    init()
    try:
        log.info("START TO CREATE REPORT")
        createReport(param)
        log.info('DONE !')
    except Exception as e:
        log.exception("Got exception on main handler")
        raise
    
if __name__ == "__main__":
	# Input Parameters
    param = json.loads(sys.argv[1]) 
    reportManager(param)
