# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw


import sys
sys.path.append('../lib/')
import Logger
from DBOperator import DBOperator
from DBOperator import decodeOracleData as decode
from ExcelGenerator import ExcelGenerator
import Property
import pymssql
import pymssql
import time
from datetime import datetime, timedelta
from dateutil import tz
from dateutil.relativedelta import relativedelta

reload(sys)  
sys.setdefaultencoding('utf8')

def init():
    from ConfigParser import ConfigParser
    conf = ConfigParser()
    conf.read('../conf/properties')
    global log
    log = Logger.create_logger(__file__)

def createReport():
    dbOpt = DBOperator()
    orcldb = dbOpt.getOracleDB()
#MAINSTATUSNO,EQUIPMENTNO,LOTNO,EVENTSTARTTIME,EVENTENDTIME
    query_shut_down ="""
        SELECT A.MAINSTATUSNO,A.EQUIPMENTNO,A.LOTNO,A.EVENTSTARTTIME,A.EVENTENDTIME, C.Family,D.CUSTOMERCHISNAME
        FROM 
            HISMFT.DTBLWIPEQUIPRTCONTENT A,
            HISMFT.TblWipLotData B,
            HISMFT.Tblengproductdata C,
            Masterdm.DIM_Customer D
        WHERE 
            A.EVENTENDTIME >= TO_DATE(:datepre, 'YYYY/MM/DD HH24:MI:SS')
            AND A.EVENTSTARTTIME < TO_DATE(:datenxt, 'YYYY/MM/DD HH24:MI:SS')
            AND A.LOTNO IS NOT NULL
            AND A.LOTNO = B.LOTNO
            AND B.PRODUCTNO = C.PRODUCTNO
            AND B.CustomerNo = D.CustNo 
            ORDER BY A.EVENTENDTIME DESC
"""
#
    today_now = time.strftime("%Y/%m/%d_%H:%M:%S")  #%Y/%m/%d_%H:%M:%S
    datecur = datetime.strptime(today_now, "%Y/%m/%d_%H:%M:%S")
    datepre = datecur - relativedelta(days=1)

    from_date = datetime.strftime(datepre, "%Y/%m/%d_%H:%M:%S")
    to_date = datetime.strftime(datecur, "%Y/%m/%d_%H:%M:%S")

    log.info("START TO CONNECT MSSQL")
    mssql_data = getData()
    log.info("GET MSSQL DATA")

    exGen = ExcelGenerator("xdown_shut_down.xlsx")

    # Daily Shut Down Data Sheet
    cr = orcldb.cursor()
    cr.execute(query_shut_down, {'datepre': from_date, 'datenxt': to_date})

    
    shut_down_data = [[c[0] for c in cr.description]] # Column Name as 1st row
    output_data =[] 
    output_data.append(["Customer","Product Family","xRun","xDown",'ErrMsg'])

    dict_groupby_eq= {}

    for row in cr:
        shut_down_data.append([decode(val) for val in row])  # Append Row Data
        dt = relativedelta(row[4],row[3]) #row[9],row[8]
        dif_second = dt.hours*3600 + dt.minutes*60

        # row[5] = product family = C.Family
        if row[5] not in dict_groupby_eq:
            # init
            dict_groupby_eq[row[5]] = {'run_time':0,'down_time': 0,'xRun':'0.0%','xDown':'0.0%','family':0,'customer':row[6],'lot_list':[],'msg_list':""}
        # row[0] = machine status ; '1' means Run ; '2' means Down
        if row[0] == '1':
            dict_groupby_eq[row[5]].update(family= dict_groupby_eq[row[5]]['family']+1)
            update_rtime = dict_groupby_eq[row[5]]['run_time']+dif_second
            xRun = "%.2f%%" % ((float(update_rtime)/86400)/dict_groupby_eq[row[5]]['family']*100)
            dict_groupby_eq[row[5]].update(run_time= update_rtime,xRun=xRun)
        elif row[0] == '2':
            dict_groupby_eq[row[5]].update(family= dict_groupby_eq[row[5]]['family']+1)
            update_dtime = dict_groupby_eq[row[5]]['down_time']+dif_second
            xDown = "%.2f%%" % ((float(update_dtime)/86400)/dict_groupby_eq[row[5]]['family']*100)
            dict_groupby_eq[row[5]].update(down_time=update_dtime,xDown=xDown )
        dict_groupby_eq[row[5]]['lot_list'].append(row[2])


    for eq, eq_info in dict_groupby_eq.items():
        for lot in mssql_data:
            if lot in eq_info['lot_list']:
                eq_info['msg_list'] += mssql_data[lot]['MsgText']

        output_data.append([eq_info['customer'],eq,eq_info['xRun'],eq_info['xDown'],eq_info['msg_list']])



    exGen.dataToSheet("xDownDaily", output_data)  # Insert to sheet


    exGen.close()
    dbOpt.close()

def get_sql_result(cr, query):
    """Exec SQL query & return dict result"""
    today = time.strftime("%Y/%m/%d")
    date_cur = datetime.strptime(today, "%Y/%m/%d")
    date_pre = date_cur - relativedelta(days=1)
    date_nxt = date_cur + relativedelta(days=1)

    cr.execute(query,(date_cur,date_nxt))
    cols = [c[0] for c in cr.description]
    return [dict(zip(cols, list(r))) for r in cr]

def getData():

    MSSQL_INFO = {"server": "egServer70","user": "Alpha-info-r","pwd": "6rKIEvjdo"}
    data_dict={}
    err_record={}
    err_text = ' ErrCount: '
 
    with pymssql.connect(MSSQL_INFO['server'], MSSQL_INFO['user'], MSSQL_INFO['pwd']) as mssql:
        cr = mssql.cursor()
        try:
            query = """
                SELECT
                    LotNum,
                    MsgText,
                    Log_Time
                FROM
                    [BIB].[dbo].[OvenErrorMsgALL]
                WHERE
                     Log_Time >= %s
                     AND Log_Time < %s
                     AND ErrorCode_1 != 0
            """
            for r in get_sql_result(cr, query):
                lot = r['LotNum']
                err_msg = r['MsgText'].strip()
                #msg = lot +' '+ err_msg # datetime.strftime(r['Log_Time'], "%Y/%m/%d_%H:%M:%S")
                temp = {}
                if lot not in err_record:
                    err_record[lot] = {err_msg: 1,'err_list':[]}
                    err_record[lot]['err_list'].append(err_msg)
                    #data_dict[lot] = { "AllMsgText" : msg + err_text+ str(err_count_dict[lot])}
                    #temp['AllMsgText'] = msg + err_text+ str(err_count_dict[lot])
                elif err_msg not in err_record[lot]['err_list']:
                    err_record[lot][err_msg] = 1
                    err_record[lot]['err_list'].append(err_msg)
                else:
                    err_record[lot][err_msg] +=1

            for lot in err_record:
                err_record[lot]['MsgText']= lot+': '
                for err_msg in err_record[lot]['err_list']:
                    err_record[lot]['MsgText'] += err_msg+' ErrCounts: '+ str(err_record[lot][err_msg])+' '

                

        except pymssql.ProgrammingError:
            print('MSSQL error')
            # Table may not be exist
    #for  data in data_list:
        #print data['LotNum'],data['MsgText']
    #distinct_lot_list= list(set(lot_list))
    #print distinct_lot_list[1]

    return err_record

def reportManager():
    init()
    try:
        log.info("START TO CREATE REPORT")
        createReport()
        log.info('DONE !')
    except Exception as e:
        log.exception("Got exception on main handler")
        raise
    
if __name__ == "__main__":
    reportManager()
