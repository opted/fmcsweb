# Author: Jhong-Yi
# Email: jhong-yi.chen@alpha-info.com.tw

import os, json, re, os.path, datetime
CONF_FILE = '../conf/properties.json'

def getProps():
	global CONF_FILE
	filepath = os.path.join(os.path.dirname(__file__), CONF_FILE)
	with open(filepath) as file:
		return json.load(file, object_hook=datetime_parser)

def datetime_parser(dct):
	for k,v in dct.items():
		if isinstance(v, basestring) and re.search("\ UTC", v):
			try:
				dct[k] = datetime.datetime.strptime(v, "%Y/%m/%d %H:%M:%S %Z")
			except:
				print "[ERROR] Fail to parse time format: %s!" %v
	return dct
