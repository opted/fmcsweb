import xlsxwriter
import codecs
import math
import sys
sys.path.append('python/lib')
import Logger
log = Logger.create_logger(__file__, 'RebiReportWriter_logs')

def setValue(value):
    try:
       return float(value)
    except ValueError:
       return value

def writeExcel(infile,outfile):
    f = codecs.open(infile, 'r', 'utf-8')
    lines = f.readlines()
    sheetMap = {}
    sheetOrder = []
    curSheet = ''
    f.close()
    # Sorting Sheet data
    for line in lines:
        line=line.rstrip('\n')
        if line.startswith('######'):
            curSheet = line.replace('######','')
            sheetOrder.append(curSheet)
            sheetMap[curSheet]=[]
        else:
            sheetMap[curSheet].append(line)
    
    # Writing Data to Excel
    workbook = xlsxwriter.Workbook(outfile,{'nan_inf_to_errors': True})
    format1 = workbook.add_format()
    format1.set_num_format('0.00%')
    hformat = workbook.add_format()
    hformat.set_pattern(1)  # This is optional when using a solid fill.
    hformat.set_bg_color('#FFFF99')
    hformat.set_border(1)
    bformat = workbook.add_format()
    bformat.set_border(1)
    
    def setColumnWidth(worksheet):
        widthlist = [15,15,15,15,8,15,35,25,25,15,20,20,10,20,20]
        for i in xrange(0,len(widthlist)):
            worksheet.set_column(i, i, widthlist[i])
        for i in xrange(15,49):
            worksheet.set_column(i, i, 8)
        worksheet.set_column(49, 49, 20)
        worksheet.set_column(50, 50, 25)
        worksheet.set_column(4, 4, 25)
        worksheet.set_column(5, 5, 35)
        worksheet.set_column(43, 43, 25)
        
    for key in sheetOrder:
        log.info('Write Sheet: %s'%(key))
        worksheet = workbook.add_worksheet(key)
        rows = sheetMap[key]
        rownum=1
        for row in rows:
            rowsplit = []
            row = row.rstrip('\t')
            for item in row.split('\t'):
                rowsplit.append(setValue(item))
            if key!='RawDataChartAnalysis':
                setColumnWidth(worksheet)
                if rowsplit[0].startswith('CustomerName'):
                    worksheet.write_row('A'+str(rownum),rowsplit,hformat)
                else:
                    worksheet.write_row('A'+str(rownum),rowsplit,bformat)
                rownum+=1
            else:
                if rownum==1:
                    worksheet.write_row('A'+str(rownum),rowsplit)
                else:
                    worksheet.write_row('A'+str(rownum),rowsplit[0:2])
                    for i in xrange(2,len(rowsplit)):
                        value = rowsplit[i]
                        if math.isnan(value):
                            continue
                        else:
                            worksheet.write_number(rownum-1,i,value,format1)
                    #worksheet.write_row('C'+str(rownum),rowsplit[2:],format1)
                rownum+=1
        if key=='RawDataChartAnalysis':
            # Create a Chart object.
            # Use bar chart be primary chart
            bar_chart = workbook.add_chart({'type': 'column'})
            # Configure the charts. In simplest case we just add some data series.
            bar_chart.add_series({
                'name': 'TestQty',
                'categories': '=%s!$A$%s:$A$%s'%(key,2,rownum-1),
                'values': '=%s!$B$%s:$B$%s'%(key,2,rownum-1)
            })
            # Define line chart
            line_chart = workbook.add_chart({'type': 'line'})
            line_chart.add_series({
                'name': 'FinalYield',
                'categories': '=%s!$A$%s:$A$%s'%(key,2,rownum-1),
                'values': '=%s!$C$%s:$C$%s'%(key,2,rownum-1),
                'marker': {'type': 'automatic'},
                'y2_axis': True
            })
            line_chart.add_series({
                'name': 'FirstYield',
                'categories': '=%s!$A$%s:$A$%s'%(key,2,rownum-1),
                'values': '=%s!$D$%s:$D$%s'%(key,2,rownum-1),
                'marker': {'type': 'automatic'},
                'y2_axis': True
            })
            # Combine two chart into one
            bar_chart.combine(line_chart)

            # Add a chart title and some axis labels.
            bar_chart.set_title({  'name': 'Yield Analysis'})
            bar_chart.set_x_axis({ 'name': 'ProductFamily'})
            bar_chart.set_y_axis({ 'name': 'TestQty'})

            # Note: the y2 properties are on the secondary chart.
            line_chart.set_y2_axis({'name': 'Yield'})

            # Insert the chart into the worksheet.
            bar_chart.set_size({'x_scale': 2, 'y_scale': 1.5})
            worksheet.insert_chart('B'+str(rownum+1), bar_chart)

    workbook.close()

'''
    Usage: python rebiReportWriter.py [inputFile] [outputFile]
    Example: python rebiReportWriter.py test.tsv test.xlsx
'''
if __name__ == '__main__':
    if len(sys.argv)<2:
        print 'python rebiReportWriter.py [inputFile] [outputFile]'
        sys.exit()

    infile = sys.argv[1]
    outfile = sys.argv[2]
    log.info("START TO CREATE REPORT")
    log.info('InputFile: %s, OutputFile: %s'%(infile,outfile))
    writeExcel(infile,outfile)
    log.info("DONE!")